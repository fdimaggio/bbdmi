![BBDMI logo](source/images/logo-bbdmi.png "Amazing BBDMI logo")

# BBDMI repository

This the code repository of the [BBDMI](https://bbdmi.nakala.fr/) project. It currently includes:

* [Max repository](https://gitlab.huma-num.fr/bbdmi/bbdmi/-/tree/main/max) containing Max/MSP patches, bpatchers,  abstractions.

* [Faust repository](https://gitlab.huma-num.fr/bbdmi/bbdmi/-/tree/main/faust) containing DSP files for the modules of sound synthesis and patches to include on the [EAVI board](https://research.gold.ac.uk/id/eprint/26476/).

* [EAVI repository](https://gitlab.huma-num.fr/bbdmi/bbdmi/-/tree/main/EAVI) containing EAVI board firmware versions and DSP patches specifics for the board.

![Partner logos](source/images/logo-partners.png "Partner logos")
