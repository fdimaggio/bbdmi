# BBDMI Pd repository (work in progress)

## About

This part of the repository contains Pd patches developed in [BBDMI](https://bbdmi.nakala.fr/) project for the purpose of creating a Brain Body Digital Music Instrument. The repository is organized in a directory structure that roughly followes a Brain Computer Music Interface (BCMI) data flow. Directories can be seen as modules sharing a common goal and operation within the BCMI data flow. Each patch is located in subsequent subdirectories that include a README.MD explaining their function, user cases and examples.

- The [Sound synthesis](https://gitlab.huma-num.fr/bbdmi/bbdmi/-/tree/main/pd/sound_synthesis) module contains patches developed by the BBDMI project that deal with the production and modulations of sound.