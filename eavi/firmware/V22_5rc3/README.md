# BBDMI EAVI firmware v22.5rc3 repository

## About

This folder contains the V22.5rc3 firmware version for the EAVI board. 

# Copyright and license
Code and documentation is copyright (c) 2021–2022 by [BBDMI](https://bbdmi.nakala.fr/), released for free under the [MIT License](https://gitlab.huma-num.fr/bbdmi/bbdmi/-/blob/main/LICENSE).