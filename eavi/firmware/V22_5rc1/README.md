# BBDMI EAVI firmware v22.5rc1 repository

## About

This folder contains the V22.5rc1 firmware version for the EAVI board. This version streams 7 channels containing 4 EMG channels and 3 accelerometer values. The sample rate of the output signals is 8Khz.

# Copyright and license
Code and documentation is copyright (c) 2021–2022 by [BBDMI](https://bbdmi.nakala.fr/), released for free under the [MIT License](https://gitlab.huma-num.fr/bbdmi/bbdmi/-/blob/main/LICENSE).
