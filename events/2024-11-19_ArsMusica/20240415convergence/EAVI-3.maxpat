{
	"patcher" : 	{
		"fileversion" : 1,
		"appversion" : 		{
			"major" : 8,
			"minor" : 5,
			"revision" : 6,
			"architecture" : "x64",
			"modernui" : 1
		}
,
		"classnamespace" : "box",
		"rect" : [ 187.0, 88.0, 1249.0, 773.0 ],
		"openrect" : [ 0.0, 0.0, 1249.0, 773.0 ],
		"bglocked" : 0,
		"openinpresentation" : 1,
		"default_fontsize" : 12.0,
		"default_fontface" : 0,
		"default_fontname" : "Arial",
		"gridonopen" : 1,
		"gridsize" : [ 15.0, 15.0 ],
		"gridsnaponopen" : 1,
		"objectsnaponopen" : 1,
		"statusbarvisible" : 2,
		"toolbarvisible" : 1,
		"lefttoolbarpinned" : 0,
		"toptoolbarpinned" : 0,
		"righttoolbarpinned" : 0,
		"bottomtoolbarpinned" : 0,
		"toolbars_unpinned_last_save" : 3,
		"tallnewobj" : 0,
		"boxanimatetime" : 200,
		"enablehscroll" : 1,
		"enablevscroll" : 1,
		"devicewidth" : 0.0,
		"description" : "",
		"digest" : "",
		"tags" : "",
		"style" : "",
		"subpatcher_template" : "",
		"assistshowspatchername" : 0,
		"boxes" : [ 			{
				"box" : 				{
					"id" : "obj-63",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 335.204225361347198, 255.0, 67.0, 22.0 ],
					"text" : "delay 4000"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-69",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 335.204225361347198, 281.0, 52.0, 22.0 ],
					"presentation_linecount" : 2,
					"text" : "freeze 1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-70",
					"linecount" : 2,
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 349.0, 216.827678571428578, 150.0, 33.0 ],
					"text" : "to undo bug in calibrate default"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-68",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 266.204225361347198, 255.0, 67.0, 22.0 ],
					"text" : "delay 5000"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-66",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 266.204225361347198, 222.327678571428578, 58.0, 22.0 ],
					"text" : "loadbang"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-65",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 266.204225361347198, 281.0, 41.0, 22.0 ],
					"text" : "time 5"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-9",
					"lastchannelcount" : 2,
					"maxclass" : "mc.live.gain~",
					"numinlets" : 1,
					"numoutlets" : 4,
					"orientation" : 1,
					"outlettype" : [ "multichannelsignal", "", "float", "list" ],
					"parameter_enable" : 1,
					"patching_rect" : [ 1160.0, 775.042305663654361, 139.0, 36.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1043.0, 379.454464285714153, 199.0, 36.0 ],
					"saved_attribute_attributes" : 					{
						"valueof" : 						{
							"parameter_initial" : [ -70.0 ],
							"parameter_initial_enable" : 1,
							"parameter_longname" : "live.gain~[3]",
							"parameter_mmax" : 6.0,
							"parameter_mmin" : -70.0,
							"parameter_shortname" : "leloup~",
							"parameter_type" : 0,
							"parameter_unitstyle" : 4
						}

					}
,
					"showname" : 0,
					"varname" : "live.gain~[3]"
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.058823529411765, 0.058823529411765, 0.058823529411765, 1.0 ],
					"fgcolor" : [ 0.403, 1.0, 0.2, 1.0 ],
					"gridcolor" : [ 0.137254901960784, 0.137254901960784, 0.137254901960784, 1.0 ],
					"id" : "obj-41",
					"maxclass" : "scope~",
					"numinlets" : 2,
					"numoutlets" : 0,
					"patching_rect" : [ 1160.0, 678.811160714286189, 139.0, 84.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1043.0, 263.449404761904361, 199.0, 113.0 ]
				}

			}
, 			{
				"box" : 				{
					"annotation" : "",
					"args" : [ 2, "@mode", 1, "@cutoff", 100.0 ],
					"bgmode" : 0,
					"border" : 1,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"hint" : "",
					"id" : "obj-42",
					"lockeddragscroll" : 0,
					"lockedsize" : 0,
					"maxclass" : "bpatcher",
					"name" : "bbdmi.onepole~.maxpat",
					"numinlets" : 2,
					"numoutlets" : 2,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "multichannelsignal", "" ],
					"patching_rect" : [ 1160.0, 506.263392857143344, 198.0, 160.999999999999972 ],
					"presentation" : 1,
					"presentation_rect" : [ 1043.0, 139.0, 198.0, 121.44434523809457 ],
					"varname" : "bbdmi.onepole~[2]",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-47",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "float", "float" ],
					"patching_rect" : [ 946.0, 1205.0, 74.0, 22.0 ],
					"text" : "unpack 0. 0."
				}

			}
, 			{
				"box" : 				{
					"args" : [ "piano2" ],
					"bgmode" : 0,
					"border" : 1,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-48",
					"lockeddragscroll" : 0,
					"lockedsize" : 0,
					"maxclass" : "bpatcher",
					"name" : "bbdmi.send.maxpat",
					"numinlets" : 1,
					"numoutlets" : 0,
					"offset" : [ 0.0, 0.0 ],
					"patching_rect" : [ 946.0, 1315.0, 199.0, 62.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1043.0, 702.300428000000011, 199.0, 64.000000000000014 ],
					"varname" : "bbdmi.send[5]",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-49",
					"lockeddragscroll" : 0,
					"lockedsize" : 0,
					"maxclass" : "bpatcher",
					"name" : "bbdmi.scale.maxpat",
					"numinlets" : 1,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "" ],
					"patching_rect" : [ 946.0, 1077.0, 198.0, 119.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 838.0, 653.0, 198.0, 119.0 ],
					"varname" : "bbdmi.scale[2]",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"args" : [ "piano1" ],
					"bgmode" : 0,
					"border" : 1,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-50",
					"lockeddragscroll" : 0,
					"lockedsize" : 0,
					"maxclass" : "bpatcher",
					"name" : "bbdmi.send.maxpat",
					"numinlets" : 1,
					"numoutlets" : 0,
					"offset" : [ 0.0, 0.0 ],
					"patching_rect" : [ 946.0, 1237.0, 199.0, 62.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1043.0, 636.300428000000011, 199.0, 64.000000000000014 ],
					"varname" : "bbdmi.send[6]",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-51",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 946.5, 1059.189285714285234, 200.0, 22.0 ],
					"text" : "0. 0."
				}

			}
, 			{
				"box" : 				{
					"annotation" : "",
					"args" : [ 2 ],
					"bgcolor" : [ 0.180392156862745, 0.423529411764706, 0.776470588235294, 1.0 ],
					"bgmode" : 0,
					"border" : 1,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"hint" : "",
					"id" : "obj-52",
					"lockeddragscroll" : 0,
					"lockedsize" : 0,
					"maxclass" : "bpatcher",
					"name" : "bbdmi.smooth.maxpat",
					"numinlets" : 2,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "float" ],
					"patching_rect" : [ 946.0, 742.335714285714857, 199.0, 63.439285714284779 ],
					"presentation" : 1,
					"presentation_rect" : [ 838.0, 468.459523809523944, 198.0, 63.439285714284779 ],
					"varname" : "bbdmi.smooth[3]",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"annotation" : "",
					"args" : [ 4, 2 ],
					"bgcolor" : [ 0.180392156862745, 0.423529411764706, 0.776470588235294, 1.0 ],
					"bgmode" : 0,
					"border" : 1,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"hint" : "",
					"id" : "obj-54",
					"lockeddragscroll" : 0,
					"lockedsize" : 0,
					"maxclass" : "bpatcher",
					"name" : "bbdmi.crosspatch.maxpat",
					"numinlets" : 2,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "" ],
					"patching_rect" : [ 946.0, 152.608035714286189, 199.0, 161.439285714284779 ],
					"presentation" : 1,
					"presentation_rect" : [ 838.0, 99.005059523809791, 198.0, 161.439285714284779 ],
					"varname" : "bbdmi.crosspatch[2]",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"args" : [ 2 ],
					"bgcolor" : [ 0.772549019607843, 0.831372549019608, 0.294117647058824, 1.0 ],
					"bgmode" : 0,
					"border" : 1,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-55",
					"lockeddragscroll" : 0,
					"lockedsize" : 0,
					"maxclass" : "bpatcher",
					"name" : "bbdmi.list2~.maxpat",
					"numinlets" : 2,
					"numoutlets" : 2,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "multichannelsignal", "" ],
					"patching_rect" : [ 946.0, 437.655357142857156, 199.0, 35.0 ],
					"varname" : "bbdmi.list2~[2]",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"args" : [ 2 ],
					"bgcolor" : [ 0.694117647058824, 0.035294117647059, 0.0, 1.0 ],
					"bgmode" : 0,
					"border" : 1,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-56",
					"lockeddragscroll" : 0,
					"lockedsize" : 0,
					"maxclass" : "bpatcher",
					"name" : "bbdmi.eavi.maxpat",
					"numinlets" : 1,
					"numoutlets" : 2,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 946.0, 31.0, 198.0, 88.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 838.0, 8.0, 198.0, 88.0 ],
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"annotation" : "",
					"args" : [ 2 ],
					"bgcolor" : [ 0.772549019607843, 0.831372549019608, 0.294117647058824, 1.0 ],
					"bgmode" : 0,
					"border" : 1,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"hint" : "",
					"id" : "obj-57",
					"lockeddragscroll" : 0,
					"lockedsize" : 0,
					"maxclass" : "bpatcher",
					"name" : "bbdmi.calibrate.maxpat",
					"numinlets" : 2,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "" ],
					"patching_rect" : [ 946.0, 613.871428571429533, 198.0, 113.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 838.0, 263.449404761904361, 198.0, 113.0 ],
					"varname" : "bbdmi.calibrate[2]",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"annotation" : "",
					"args" : [ 2 ],
					"bgcolor" : [ 0.180392156862745, 0.423529411764706, 0.776470588235294, 1.0 ],
					"bgmode" : 0,
					"border" : 1,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"hint" : "",
					"id" : "obj-58",
					"lockeddragscroll" : 0,
					"lockedsize" : 0,
					"maxclass" : "bpatcher",
					"name" : "bbdmi.multislider.maxpat",
					"numinlets" : 2,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "" ],
					"patching_rect" : [ 946.5, 934.75, 198.0, 114.439285714285234 ],
					"presentation" : 1,
					"presentation_rect" : [ 838.0, 534.903869047618514, 198.0, 113.719642857142617 ],
					"varname" : "bbdmi.multislider[2]",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"candycane" : 8,
					"id" : "obj-60",
					"maxclass" : "multislider",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1009.0, 347.475000000000364, 140.0, 63.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1043.0, 8.0, 199.0, 88.0 ],
					"setstyle" : 3,
					"size" : 2
				}

			}
, 			{
				"box" : 				{
					"candycane" : 8,
					"id" : "obj-61",
					"maxclass" : "multislider",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1012.5, 839.75, 140.0, 63.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1043.0, 419.903869047618514, 199.0, 113.0 ],
					"setminmax" : [ 0.0, 1.0 ],
					"setstyle" : 3,
					"size" : 2
				}

			}
, 			{
				"box" : 				{
					"args" : [ 2 ],
					"bgcolor" : [ 0.772549019607843, 0.831372549019608, 0.294117647058824, 1.0 ],
					"bgmode" : 0,
					"border" : 1,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-62",
					"lockeddragscroll" : 0,
					"lockedsize" : 0,
					"maxclass" : "bpatcher",
					"name" : "bbdmi.rms~.maxpat",
					"numinlets" : 2,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "" ],
					"patching_rect" : [ 946.0, 506.263392857143344, 198.0, 87.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 838.0, 379.454464285714153, 198.296296296296418, 86.0 ],
					"varname" : "bbdmi.rms~[2]",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-45",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 336.0, 860.25, 41.0, 22.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 315.0, 541.224702380952294, 41.0, 22.0 ],
					"text" : "r osc2"
				}

			}
, 			{
				"box" : 				{
					"format" : 6,
					"id" : "obj-46",
					"maxclass" : "flonum",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 336.0, 890.0, 50.0, 22.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 315.0, 565.224702380952181, 50.0, 22.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-44",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 278.0, 860.25, 41.0, 22.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 212.0, 541.224702380952294, 41.0, 22.0 ],
					"text" : "r osc1"
				}

			}
, 			{
				"box" : 				{
					"format" : 6,
					"id" : "obj-43",
					"maxclass" : "flonum",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 278.0, 890.0, 50.0, 22.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 212.0, 565.224702380952181, 50.0, 22.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-40",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 336.0, 920.0, 43.0, 22.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 367.0, 565.224702380952181, 43.0, 22.0 ],
					"text" : "cycle~"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-39",
					"lastchannelcount" : 0,
					"maxclass" : "live.gain~",
					"numinlets" : 2,
					"numoutlets" : 5,
					"orientation" : 1,
					"outlettype" : [ "signal", "signal", "", "float", "list" ],
					"parameter_enable" : 1,
					"patching_rect" : [ 278.0, 967.0, 136.0, 47.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 244.0, 589.224702380952181, 136.0, 47.0 ],
					"saved_attribute_attributes" : 					{
						"valueof" : 						{
							"parameter_initial" : [ -70.0 ],
							"parameter_initial_enable" : 1,
							"parameter_longname" : "live.gain~[2]",
							"parameter_mmax" : 6.0,
							"parameter_mmin" : -70.0,
							"parameter_shortname" : "live.gain~[2]",
							"parameter_type" : 0,
							"parameter_unitstyle" : 4
						}

					}
,
					"varname" : "live.gain~[2]"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-38",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 278.0, 920.0, 43.0, 22.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 264.0, 565.224702380952181, 43.0, 22.0 ],
					"text" : "cycle~"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-15",
					"lastchannelcount" : 2,
					"maxclass" : "mc.live.gain~",
					"numinlets" : 1,
					"numoutlets" : 4,
					"orientation" : 1,
					"outlettype" : [ "multichannelsignal", "", "float", "list" ],
					"parameter_enable" : 1,
					"patching_rect" : [ 715.0, 775.042305663654361, 139.0, 36.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 626.0, 379.454464285714153, 199.0, 36.0 ],
					"saved_attribute_attributes" : 					{
						"valueof" : 						{
							"parameter_initial" : [ -70.0 ],
							"parameter_initial_enable" : 1,
							"parameter_longname" : "live.gain~[1]",
							"parameter_mmax" : 6.0,
							"parameter_mmin" : -70.0,
							"parameter_shortname" : "leloup~",
							"parameter_type" : 0,
							"parameter_unitstyle" : 4
						}

					}
,
					"showname" : 0,
					"varname" : "live.gain~[1]"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-14",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 445.0, 839.75, 54.0, 22.0 ],
					"text" : "mc.dac~"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-67",
					"lastchannelcount" : 2,
					"maxclass" : "mc.live.gain~",
					"numinlets" : 1,
					"numoutlets" : 4,
					"orientation" : 1,
					"outlettype" : [ "multichannelsignal", "", "float", "list" ],
					"parameter_enable" : 1,
					"patching_rect" : [ 258.0, 775.042305663654361, 139.0, 36.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 212.0, 379.454464285714153, 199.0, 36.0 ],
					"saved_attribute_attributes" : 					{
						"valueof" : 						{
							"parameter_initial" : [ -70.0 ],
							"parameter_initial_enable" : 1,
							"parameter_longname" : "live.gain~",
							"parameter_mmax" : 6.0,
							"parameter_mmin" : -70.0,
							"parameter_shortname" : "leloup~",
							"parameter_type" : 0,
							"parameter_unitstyle" : 4
						}

					}
,
					"showname" : 0,
					"varname" : "live.gain~"
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.058823529411765, 0.058823529411765, 0.058823529411765, 1.0 ],
					"fgcolor" : [ 0.403, 1.0, 0.2, 1.0 ],
					"gridcolor" : [ 0.137254901960784, 0.137254901960784, 0.137254901960784, 1.0 ],
					"id" : "obj-11",
					"maxclass" : "scope~",
					"numinlets" : 2,
					"numoutlets" : 0,
					"patching_rect" : [ 715.0, 678.811160714286189, 139.0, 84.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 626.0, 263.449404761904361, 199.0, 113.0 ]
				}

			}
, 			{
				"box" : 				{
					"annotation" : "",
					"args" : [ 2, "@mode", 1, "@cutoff", 100.0 ],
					"bgmode" : 0,
					"border" : 1,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"hint" : "",
					"id" : "obj-13",
					"lockeddragscroll" : 0,
					"lockedsize" : 0,
					"maxclass" : "bpatcher",
					"name" : "bbdmi.onepole~.maxpat",
					"numinlets" : 2,
					"numoutlets" : 2,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "multichannelsignal", "" ],
					"patching_rect" : [ 715.0, 506.263392857143344, 198.0, 160.999999999999972 ],
					"presentation" : 1,
					"presentation_rect" : [ 626.0, 139.0, 198.0, 121.44434523809457 ],
					"varname" : "bbdmi.onepole~[1]",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.058823529411765, 0.058823529411765, 0.058823529411765, 1.0 ],
					"fgcolor" : [ 0.403, 1.0, 0.2, 1.0 ],
					"gridcolor" : [ 0.137254901960784, 0.137254901960784, 0.137254901960784, 1.0 ],
					"id" : "obj-7",
					"maxclass" : "scope~",
					"numinlets" : 2,
					"numoutlets" : 0,
					"patching_rect" : [ 258.0, 678.811160714286189, 139.0, 84.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 212.0, 263.449404761904361, 199.0, 113.0 ]
				}

			}
, 			{
				"box" : 				{
					"annotation" : "",
					"args" : [ 2, "@mode", 1, "@cutoff", 100.0 ],
					"bgmode" : 0,
					"border" : 1,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"hint" : "",
					"id" : "obj-175",
					"lockeddragscroll" : 0,
					"lockedsize" : 0,
					"maxclass" : "bpatcher",
					"name" : "bbdmi.onepole~.maxpat",
					"numinlets" : 2,
					"numoutlets" : 2,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "multichannelsignal", "" ],
					"patching_rect" : [ 258.0, 506.263392857143344, 198.0, 160.999999999999972 ],
					"presentation" : 1,
					"presentation_rect" : [ 212.0, 139.0, 198.0, 121.44434523809457 ],
					"varname" : "bbdmi.onepole~",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-37",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "float", "float" ],
					"patching_rect" : [ 501.0, 1205.0, 74.0, 22.0 ],
					"text" : "unpack 0. 0."
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-36",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "float", "float" ],
					"patching_rect" : [ 32.5, 1205.0, 74.0, 22.0 ],
					"text" : "unpack 0. 0."
				}

			}
, 			{
				"box" : 				{
					"args" : [ "bassclar2" ],
					"bgmode" : 0,
					"border" : 1,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-33",
					"lockeddragscroll" : 0,
					"lockedsize" : 0,
					"maxclass" : "bpatcher",
					"name" : "bbdmi.send.maxpat",
					"numinlets" : 1,
					"numoutlets" : 0,
					"offset" : [ 0.0, 0.0 ],
					"patching_rect" : [ 501.0, 1315.0, 199.0, 62.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 626.0, 702.300428000000011, 199.0, 64.000000000000014 ],
					"varname" : "bbdmi.send[3]",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-34",
					"lockeddragscroll" : 0,
					"lockedsize" : 0,
					"maxclass" : "bpatcher",
					"name" : "bbdmi.scale.maxpat",
					"numinlets" : 1,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "" ],
					"patching_rect" : [ 501.0, 1077.0, 198.0, 119.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 421.0, 653.0, 198.0, 119.0 ],
					"varname" : "bbdmi.scale[1]",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"args" : [ "bassclar1" ],
					"bgmode" : 0,
					"border" : 1,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-35",
					"lockeddragscroll" : 0,
					"lockedsize" : 0,
					"maxclass" : "bpatcher",
					"name" : "bbdmi.send.maxpat",
					"numinlets" : 1,
					"numoutlets" : 0,
					"offset" : [ 0.0, 0.0 ],
					"patching_rect" : [ 501.0, 1237.0, 199.0, 62.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 626.0, 636.300428000000011, 199.0, 64.000000000000014 ],
					"varname" : "bbdmi.send[4]",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"args" : [ "viola2" ],
					"bgmode" : 0,
					"border" : 1,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-32",
					"lockeddragscroll" : 0,
					"lockedsize" : 0,
					"maxclass" : "bpatcher",
					"name" : "bbdmi.send.maxpat",
					"numinlets" : 1,
					"numoutlets" : 0,
					"offset" : [ 0.0, 0.0 ],
					"patching_rect" : [ 32.5, 1310.0, 199.0, 62.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 212.0, 702.300428000000011, 199.0, 64.000000000000014 ],
					"varname" : "bbdmi.send[2]",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-31",
					"lockeddragscroll" : 0,
					"lockedsize" : 0,
					"maxclass" : "bpatcher",
					"name" : "bbdmi.scale.maxpat",
					"numinlets" : 1,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "" ],
					"patching_rect" : [ 32.5, 1077.0, 198.0, 119.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 7.0, 653.0, 198.0, 119.0 ],
					"varname" : "bbdmi.scale",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"args" : [ "viola1" ],
					"bgmode" : 0,
					"border" : 1,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-64",
					"lockeddragscroll" : 0,
					"lockedsize" : 0,
					"maxclass" : "bpatcher",
					"name" : "bbdmi.send.maxpat",
					"numinlets" : 1,
					"numoutlets" : 0,
					"offset" : [ 0.0, 0.0 ],
					"patching_rect" : [ 32.5, 1237.0, 199.0, 62.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 212.0, 636.300428000000011, 199.0, 64.000000000000014 ],
					"varname" : "bbdmi.send[1]",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-18",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 501.5, 1059.189285714285234, 200.0, 22.0 ],
					"text" : "0. 0."
				}

			}
, 			{
				"box" : 				{
					"annotation" : "",
					"args" : [ 2 ],
					"bgcolor" : [ 0.180392156862745, 0.423529411764706, 0.776470588235294, 1.0 ],
					"bgmode" : 0,
					"border" : 1,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"hint" : "",
					"id" : "obj-20",
					"lockeddragscroll" : 0,
					"lockedsize" : 0,
					"maxclass" : "bpatcher",
					"name" : "bbdmi.smooth.maxpat",
					"numinlets" : 2,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "float" ],
					"patching_rect" : [ 501.0, 742.335714285714857, 199.0, 63.439285714284779 ],
					"presentation" : 1,
					"presentation_rect" : [ 421.0, 468.459523809523944, 198.0, 63.439285714284779 ],
					"varname" : "bbdmi.smooth[2]",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"annotation" : "",
					"args" : [ 4, 2 ],
					"bgcolor" : [ 0.180392156862745, 0.423529411764706, 0.776470588235294, 1.0 ],
					"bgmode" : 0,
					"border" : 1,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"hint" : "",
					"id" : "obj-21",
					"lockeddragscroll" : 0,
					"lockedsize" : 0,
					"maxclass" : "bpatcher",
					"name" : "bbdmi.crosspatch.maxpat",
					"numinlets" : 2,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "" ],
					"patching_rect" : [ 501.0, 152.608035714286189, 199.0, 161.439285714284779 ],
					"presentation" : 1,
					"presentation_rect" : [ 421.0, 99.005059523809791, 198.0, 161.439285714284779 ],
					"varname" : "bbdmi.crosspatch[1]",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"args" : [ 2 ],
					"bgcolor" : [ 0.772549019607843, 0.831372549019608, 0.294117647058824, 1.0 ],
					"bgmode" : 0,
					"border" : 1,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-22",
					"lockeddragscroll" : 0,
					"lockedsize" : 0,
					"maxclass" : "bpatcher",
					"name" : "bbdmi.list2~.maxpat",
					"numinlets" : 2,
					"numoutlets" : 2,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "multichannelsignal", "" ],
					"patching_rect" : [ 501.0, 437.655357142857156, 199.0, 35.0 ],
					"varname" : "bbdmi.list2~[1]",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"args" : [ 2 ],
					"bgcolor" : [ 0.694117647058824, 0.035294117647059, 0.0, 1.0 ],
					"bgmode" : 0,
					"border" : 1,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-23",
					"lockeddragscroll" : 0,
					"lockedsize" : 0,
					"maxclass" : "bpatcher",
					"name" : "bbdmi.eavi.maxpat",
					"numinlets" : 1,
					"numoutlets" : 2,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 501.0, 31.0, 198.0, 88.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 421.0, 8.0, 198.0, 88.0 ],
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"annotation" : "",
					"args" : [ 2 ],
					"bgcolor" : [ 0.772549019607843, 0.831372549019608, 0.294117647058824, 1.0 ],
					"bgmode" : 0,
					"border" : 1,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"hint" : "",
					"id" : "obj-24",
					"lockeddragscroll" : 0,
					"lockedsize" : 0,
					"maxclass" : "bpatcher",
					"name" : "bbdmi.calibrate.maxpat",
					"numinlets" : 2,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "" ],
					"patching_rect" : [ 501.0, 613.871428571429533, 198.0, 113.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 421.0, 263.449404761904361, 198.0, 113.0 ],
					"varname" : "bbdmi.calibrate[1]",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"annotation" : "",
					"args" : [ 2 ],
					"bgcolor" : [ 0.180392156862745, 0.423529411764706, 0.776470588235294, 1.0 ],
					"bgmode" : 0,
					"border" : 1,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"hint" : "",
					"id" : "obj-25",
					"lockeddragscroll" : 0,
					"lockedsize" : 0,
					"maxclass" : "bpatcher",
					"name" : "bbdmi.multislider.maxpat",
					"numinlets" : 2,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "" ],
					"patching_rect" : [ 501.5, 934.75, 198.0, 114.439285714285234 ],
					"presentation" : 1,
					"presentation_rect" : [ 421.0, 534.903869047618514, 198.0, 113.719642857142617 ],
					"varname" : "bbdmi.multislider[1]",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"candycane" : 8,
					"id" : "obj-26",
					"maxclass" : "multislider",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 564.0, 347.475000000000364, 140.0, 63.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 626.0, 8.0, 199.0, 88.0 ],
					"setstyle" : 3,
					"size" : 2
				}

			}
, 			{
				"box" : 				{
					"candycane" : 8,
					"id" : "obj-27",
					"maxclass" : "multislider",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 567.5, 839.75, 140.0, 63.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 626.0, 419.903869047618514, 199.0, 113.0 ],
					"setminmax" : [ 0.0, 1.0 ],
					"setstyle" : 3,
					"size" : 2
				}

			}
, 			{
				"box" : 				{
					"args" : [ 2 ],
					"bgcolor" : [ 0.772549019607843, 0.831372549019608, 0.294117647058824, 1.0 ],
					"bgmode" : 0,
					"border" : 1,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-28",
					"lockeddragscroll" : 0,
					"lockedsize" : 0,
					"maxclass" : "bpatcher",
					"name" : "bbdmi.rms~.maxpat",
					"numinlets" : 2,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "" ],
					"patching_rect" : [ 501.0, 506.263392857143344, 198.0, 87.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 421.0, 379.454464285714153, 198.296296296296418, 86.0 ],
					"varname" : "bbdmi.rms~[1]",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-16",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 31.5, 1053.0, 200.0, 22.0 ],
					"text" : "0. 0."
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-1",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 354.0, 45.75, 54.0, 22.0 ],
					"text" : "deferlow"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-4",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 295.678726178205579, 12.274999999999636, 51.0, 20.0 ],
					"text" : "pcontrol"
				}

			}
, 			{
				"box" : 				{
					"comment" : "",
					"id" : "obj-10",
					"index" : 0,
					"maxclass" : "inlet",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 257.977111839843474, 7.274999999999636, 30.0, 30.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-12",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patcher" : 					{
						"fileversion" : 1,
						"appversion" : 						{
							"major" : 8,
							"minor" : 5,
							"revision" : 6,
							"architecture" : "x64",
							"modernui" : 1
						}
,
						"classnamespace" : "box",
						"rect" : [ 34.0, 87.0, 173.0, 257.0 ],
						"bglocked" : 0,
						"openinpresentation" : 0,
						"default_fontsize" : 12.0,
						"default_fontface" : 0,
						"default_fontname" : "Arial",
						"gridonopen" : 1,
						"gridsize" : [ 15.0, 15.0 ],
						"gridsnaponopen" : 1,
						"objectsnaponopen" : 1,
						"statusbarvisible" : 2,
						"toolbarvisible" : 1,
						"lefttoolbarpinned" : 0,
						"toptoolbarpinned" : 0,
						"righttoolbarpinned" : 0,
						"bottomtoolbarpinned" : 0,
						"toolbars_unpinned_last_save" : 0,
						"tallnewobj" : 0,
						"boxanimatetime" : 200,
						"enablehscroll" : 1,
						"enablevscroll" : 1,
						"devicewidth" : 0.0,
						"description" : "",
						"digest" : "",
						"tags" : "",
						"style" : "",
						"subpatcher_template" : "Default Max 7",
						"assistshowspatchername" : 0,
						"boxes" : [ 							{
								"box" : 								{
									"id" : "obj-85",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 33.25, 170.000012222222267, 120.0, 22.0 ],
									"text" : "prepend pattrstorage"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-78",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "", "" ],
									"patching_rect" : [ 14.0, 60.500012222222267, 63.0, 22.0 ],
									"text" : "route read"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 12.0,
									"id" : "obj-66",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 5,
									"outlettype" : [ "", "", "", "", "" ],
									"patching_rect" : [ 14.0, 133.500012222222267, 96.0, 22.0 ],
									"text" : "regexp (.+)\\\\..+$"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 12.0,
									"id" : "obj-72",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "int" ],
									"patching_rect" : [ 14.0, 97.000012222222267, 57.0, 22.0 ],
									"text" : "strippath"
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-86",
									"index" : 1,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 14.000000023437501, 16.000012222222267, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-87",
									"index" : 1,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 33.25, 206.500012222222267, 30.0, 30.0 ]
								}

							}
 ],
						"lines" : [ 							{
								"patchline" : 								{
									"destination" : [ "obj-85", 0 ],
									"midpoints" : [ 42.75, 156.0, 42.75, 156.0 ],
									"source" : [ "obj-66", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-66", 0 ],
									"midpoints" : [ 23.5, 120.0, 23.5, 120.0 ],
									"source" : [ "obj-72", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-72", 0 ],
									"midpoints" : [ 23.5, 84.0, 23.5, 84.0 ],
									"source" : [ "obj-78", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-87", 0 ],
									"midpoints" : [ 42.75, 195.0, 42.75, 195.0 ],
									"source" : [ "obj-85", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-78", 0 ],
									"midpoints" : [ 23.500000023437501, 48.0, 23.5, 48.0 ],
									"source" : [ "obj-86", 0 ]
								}

							}
 ]
					}
,
					"patching_rect" : [ 266.204225361347198, 133.059048033611703, 52.0, 22.0 ],
					"saved_object_attributes" : 					{
						"description" : "",
						"digest" : "",
						"globalpatchername" : "",
						"tags" : ""
					}
,
					"text" : "p preset"
				}

			}
, 			{
				"box" : 				{
					"annotation" : "",
					"args" : [ 2 ],
					"bgcolor" : [ 0.180392156862745, 0.423529411764706, 0.776470588235294, 1.0 ],
					"bgmode" : 0,
					"border" : 1,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"hint" : "",
					"id" : "obj-2",
					"lockeddragscroll" : 0,
					"lockedsize" : 0,
					"maxclass" : "bpatcher",
					"name" : "bbdmi.smooth.maxpat",
					"numinlets" : 2,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "float" ],
					"patching_rect" : [ 32.0, 742.335714285714857, 199.0, 63.439285714284779 ],
					"presentation" : 1,
					"presentation_rect" : [ 7.0, 468.459523809523944, 198.0, 63.439285714284779 ],
					"varname" : "bbdmi.smooth[1]",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"annotation" : "",
					"args" : [ 4, 2 ],
					"bgcolor" : [ 0.180392156862745, 0.423529411764706, 0.776470588235294, 1.0 ],
					"bgmode" : 0,
					"border" : 1,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"hint" : "",
					"id" : "obj-8",
					"lockeddragscroll" : 0,
					"lockedsize" : 0,
					"maxclass" : "bpatcher",
					"name" : "bbdmi.crosspatch.maxpat",
					"numinlets" : 2,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "" ],
					"patching_rect" : [ 32.0, 152.608035714286189, 199.0, 161.439285714284779 ],
					"presentation" : 1,
					"presentation_rect" : [ 7.0, 99.005059523809791, 198.0, 161.439285714284779 ],
					"varname" : "bbdmi.crosspatch",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"args" : [ 2 ],
					"bgcolor" : [ 0.772549019607843, 0.831372549019608, 0.294117647058824, 1.0 ],
					"bgmode" : 0,
					"border" : 1,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-53",
					"lockeddragscroll" : 0,
					"lockedsize" : 0,
					"maxclass" : "bpatcher",
					"name" : "bbdmi.list2~.maxpat",
					"numinlets" : 2,
					"numoutlets" : 2,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "multichannelsignal", "" ],
					"patching_rect" : [ 32.0, 437.655357142857156, 199.0, 35.0 ],
					"varname" : "bbdmi.list2~",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-6",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 354.0, 15.274999999999636, 102.0, 22.0 ],
					"text" : "loadmess recall 1"
				}

			}
, 			{
				"box" : 				{
					"args" : [ 2 ],
					"bgcolor" : [ 0.694117647058824, 0.035294117647059, 0.0, 1.0 ],
					"bgmode" : 0,
					"border" : 1,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-30",
					"lockeddragscroll" : 0,
					"lockedsize" : 0,
					"maxclass" : "bpatcher",
					"name" : "bbdmi.eavi.maxpat",
					"numinlets" : 1,
					"numoutlets" : 2,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 32.0, 31.0, 198.0, 88.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 7.0, 8.0, 198.0, 88.0 ],
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"annotation" : "",
					"args" : [ 2 ],
					"bgcolor" : [ 0.772549019607843, 0.831372549019608, 0.294117647058824, 1.0 ],
					"bgmode" : 0,
					"border" : 1,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"hint" : "",
					"id" : "obj-93",
					"lockeddragscroll" : 0,
					"lockedsize" : 0,
					"maxclass" : "bpatcher",
					"name" : "bbdmi.calibrate.maxpat",
					"numinlets" : 2,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "" ],
					"patching_rect" : [ 32.0, 613.871428571429533, 198.0, 113.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 7.0, 263.449404761904361, 198.0, 113.0 ],
					"varname" : "bbdmi.calibrate",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"annotation" : "",
					"args" : [ 2 ],
					"bgcolor" : [ 0.180392156862745, 0.423529411764706, 0.776470588235294, 1.0 ],
					"bgmode" : 0,
					"border" : 1,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"hint" : "",
					"id" : "obj-59",
					"lockeddragscroll" : 0,
					"lockedsize" : 0,
					"maxclass" : "bpatcher",
					"name" : "bbdmi.multislider.maxpat",
					"numinlets" : 2,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "" ],
					"patching_rect" : [ 32.5, 934.75, 198.0, 114.439285714285234 ],
					"presentation" : 1,
					"presentation_rect" : [ 7.0, 534.903869047618514, 198.0, 113.719642857142617 ],
					"varname" : "bbdmi.multislider",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"candycane" : 8,
					"id" : "obj-29",
					"maxclass" : "multislider",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 95.0, 347.475000000000364, 140.0, 63.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 212.0, 8.0, 199.0, 88.0 ],
					"setstyle" : 3,
					"size" : 2
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-19",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 265.5, 45.75, 63.0, 22.0 ],
					"text" : "writeagain"
				}

			}
, 			{
				"box" : 				{
					"bubblesize" : 20,
					"id" : "obj-17",
					"maxclass" : "preset",
					"numinlets" : 1,
					"numoutlets" : 5,
					"outlettype" : [ "preset", "int", "preset", "int", "" ],
					"patching_rect" : [ 266.204225361347198, 163.047321428570967, 101.0, 29.0 ],
					"pattrstorage" : "RD-perf-a",
					"presentation" : 1,
					"presentation_rect" : [ 755.000000000000227, 545.224702380952181, 53.25, 29.0 ]
				}

			}
, 			{
				"box" : 				{
					"autorestore" : "RD-perf-a.json",
					"id" : "obj-3",
					"linecount" : 2,
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 265.5, 93.774999999999636, 233.5, 35.0 ],
					"priority" : 					{
						"bbdmi.multislider::Multislider" : 1,
						"bbdmi.crosspatch::Crosspatch" : 1,
						"bbdmi.multislider[1]::Multislider" : 1,
						"bbdmi.crosspatch[1]::Crosspatch" : 1,
						"bbdmi.multislider[2]::Multislider" : 1,
						"bbdmi.crosspatch[2]::Crosspatch" : 1
					}
,
					"saved_object_attributes" : 					{
						"client_rect" : [ 100, 100, 500, 600 ],
						"parameter_enable" : 0,
						"parameter_mappable" : 0,
						"storage_rect" : [ 200, 200, 800, 500 ]
					}
,
					"text" : "pattrstorage RD-perf-a @savemode 1 @autorestore 1 @changemode 1",
					"varname" : "RD-perf-a"
				}

			}
, 			{
				"box" : 				{
					"candycane" : 8,
					"id" : "obj-87",
					"maxclass" : "multislider",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 98.5, 839.75, 140.0, 63.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 212.0, 419.903869047618514, 199.0, 113.0 ],
					"setminmax" : [ 0.0, 1.0 ],
					"setstyle" : 3,
					"size" : 2
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-213",
					"local" : 1,
					"maxclass" : "ezdac~",
					"numinlets" : 2,
					"numoutlets" : 0,
					"patching_rect" : [ 278.0, 1029.683928571427714, 54.5, 54.5 ],
					"presentation" : 1,
					"presentation_rect" : [ 755.000000000000227, 576.224702380952181, 53.25, 53.25 ]
				}

			}
, 			{
				"box" : 				{
					"args" : [ 2 ],
					"bgcolor" : [ 0.772549019607843, 0.831372549019608, 0.294117647058824, 1.0 ],
					"bgmode" : 0,
					"border" : 1,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-5",
					"lockeddragscroll" : 0,
					"lockedsize" : 0,
					"maxclass" : "bpatcher",
					"name" : "bbdmi.rms~.maxpat",
					"numinlets" : 2,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "" ],
					"patching_rect" : [ 32.0, 506.263392857143344, 198.0, 87.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 7.0, 379.454464285714153, 198.296296296296418, 86.0 ],
					"varname" : "bbdmi.rms~",
					"viewvisibility" : 1
				}

			}
 ],
		"lines" : [ 			{
				"patchline" : 				{
					"destination" : [ "obj-3", 0 ],
					"midpoints" : [ 363.5, 79.0, 275.0, 79.0 ],
					"source" : [ "obj-1", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-17", 0 ],
					"midpoints" : [ 275.704225361347198, 151.0, 275.704225361347198, 151.0 ],
					"source" : [ "obj-12", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-11", 0 ],
					"midpoints" : [ 724.5, 683.667857142857656, 724.5, 683.667857142857656 ],
					"order" : 1,
					"source" : [ "obj-13", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-15", 0 ],
					"order" : 0,
					"source" : [ "obj-13", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-14", 0 ],
					"source" : [ "obj-15", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-67", 0 ],
					"order" : 0,
					"source" : [ "obj-175", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-7", 0 ],
					"midpoints" : [ 267.5, 683.667857142857656, 267.5, 683.667857142857656 ],
					"order" : 1,
					"source" : [ "obj-175", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-3", 0 ],
					"midpoints" : [ 275.0, 70.0, 275.0, 70.0 ],
					"source" : [ "obj-19", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-59", 0 ],
					"midpoints" : [ 41.5, 930.0, 42.0, 930.0 ],
					"order" : 1,
					"source" : [ "obj-2", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-87", 0 ],
					"midpoints" : [ 41.5, 825.0, 108.0, 825.0 ],
					"order" : 0,
					"source" : [ "obj-2", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-25", 0 ],
					"midpoints" : [ 510.5, 930.0, 511.0, 930.0 ],
					"order" : 1,
					"source" : [ "obj-20", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-27", 0 ],
					"midpoints" : [ 510.5, 825.0, 577.0, 825.0 ],
					"order" : 0,
					"source" : [ "obj-20", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-22", 0 ],
					"midpoints" : [ 510.5, 315.0, 510.5, 315.0 ],
					"order" : 1,
					"source" : [ "obj-21", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-26", 0 ],
					"midpoints" : [ 510.5, 333.0, 573.5, 333.0 ],
					"order" : 0,
					"source" : [ "obj-21", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-13", 0 ],
					"order" : 1,
					"source" : [ "obj-22", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-28", 0 ],
					"midpoints" : [ 510.5, 474.0, 510.5, 474.0 ],
					"order" : 0,
					"source" : [ "obj-22", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-21", 0 ],
					"midpoints" : [ 510.5, 120.0, 510.5, 120.0 ],
					"source" : [ "obj-23", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-20", 0 ],
					"midpoints" : [ 510.5, 729.0, 510.5, 729.0 ],
					"source" : [ "obj-24", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-18", 1 ],
					"order" : 0,
					"source" : [ "obj-25", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-34", 0 ],
					"order" : 1,
					"source" : [ "obj-25", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-24", 0 ],
					"midpoints" : [ 510.5, 594.0, 510.5, 594.0 ],
					"source" : [ "obj-28", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-12", 0 ],
					"midpoints" : [ 275.0, 118.0, 275.704225361347198, 118.0 ],
					"source" : [ "obj-3", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-8", 0 ],
					"midpoints" : [ 41.5, 120.0, 41.5, 120.0 ],
					"source" : [ "obj-30", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-36", 0 ],
					"source" : [ "obj-31", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-37", 0 ],
					"source" : [ "obj-34", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-32", 0 ],
					"source" : [ "obj-36", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-64", 0 ],
					"source" : [ "obj-36", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-33", 0 ],
					"source" : [ "obj-37", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-35", 0 ],
					"source" : [ "obj-37", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-39", 0 ],
					"source" : [ "obj-38", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-213", 1 ],
					"source" : [ "obj-39", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-213", 0 ],
					"source" : [ "obj-39", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-39", 1 ],
					"source" : [ "obj-40", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-41", 0 ],
					"midpoints" : [ 1169.5, 683.667857142857656, 1169.5, 683.667857142857656 ],
					"order" : 1,
					"source" : [ "obj-42", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-9", 0 ],
					"order" : 0,
					"source" : [ "obj-42", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-38", 0 ],
					"source" : [ "obj-43", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-43", 0 ],
					"source" : [ "obj-44", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-46", 0 ],
					"source" : [ "obj-45", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-40", 0 ],
					"source" : [ "obj-46", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-48", 0 ],
					"source" : [ "obj-47", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-50", 0 ],
					"source" : [ "obj-47", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-47", 0 ],
					"source" : [ "obj-49", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-93", 0 ],
					"midpoints" : [ 41.5, 594.0, 41.5, 594.0 ],
					"source" : [ "obj-5", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-58", 0 ],
					"midpoints" : [ 955.5, 930.0, 956.0, 930.0 ],
					"order" : 1,
					"source" : [ "obj-52", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-61", 0 ],
					"midpoints" : [ 955.5, 825.0, 1022.0, 825.0 ],
					"order" : 0,
					"source" : [ "obj-52", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-175", 0 ],
					"order" : 1,
					"source" : [ "obj-53", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-5", 0 ],
					"midpoints" : [ 41.5, 474.0, 41.5, 474.0 ],
					"order" : 0,
					"source" : [ "obj-53", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-55", 0 ],
					"midpoints" : [ 955.5, 315.0, 955.5, 315.0 ],
					"order" : 1,
					"source" : [ "obj-54", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-60", 0 ],
					"midpoints" : [ 955.5, 333.0, 1018.5, 333.0 ],
					"order" : 0,
					"source" : [ "obj-54", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-42", 0 ],
					"order" : 1,
					"source" : [ "obj-55", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-62", 0 ],
					"midpoints" : [ 955.5, 474.0, 955.5, 474.0 ],
					"order" : 0,
					"source" : [ "obj-55", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-54", 0 ],
					"midpoints" : [ 955.5, 120.0, 955.5, 120.0 ],
					"source" : [ "obj-56", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-52", 0 ],
					"midpoints" : [ 955.5, 729.0, 955.5, 729.0 ],
					"source" : [ "obj-57", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-49", 0 ],
					"order" : 1,
					"source" : [ "obj-58", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-51", 1 ],
					"order" : 0,
					"source" : [ "obj-58", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-16", 1 ],
					"order" : 0,
					"source" : [ "obj-59", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-31", 0 ],
					"order" : 1,
					"source" : [ "obj-59", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-1", 0 ],
					"midpoints" : [ 363.5, 40.0, 363.5, 40.0 ],
					"source" : [ "obj-6", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-57", 0 ],
					"midpoints" : [ 955.5, 594.0, 955.5, 594.0 ],
					"source" : [ "obj-62", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-69", 0 ],
					"source" : [ "obj-63", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-24", 1 ],
					"order" : 2,
					"source" : [ "obj-65", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-57", 1 ],
					"order" : 1,
					"source" : [ "obj-65", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-93", 1 ],
					"order" : 0,
					"source" : [ "obj-65", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-63", 0 ],
					"order" : 0,
					"source" : [ "obj-66", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-68", 0 ],
					"order" : 1,
					"source" : [ "obj-66", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-14", 0 ],
					"source" : [ "obj-67", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-65", 0 ],
					"source" : [ "obj-68", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-24", 1 ],
					"order" : 1,
					"source" : [ "obj-69", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-57", 1 ],
					"order" : 0,
					"source" : [ "obj-69", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-93", 1 ],
					"order" : 2,
					"source" : [ "obj-69", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-29", 0 ],
					"midpoints" : [ 41.5, 333.0, 104.5, 333.0 ],
					"order" : 0,
					"source" : [ "obj-8", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-53", 0 ],
					"midpoints" : [ 41.5, 315.0, 41.5, 315.0 ],
					"order" : 1,
					"source" : [ "obj-8", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-2", 0 ],
					"midpoints" : [ 41.5, 729.0, 41.5, 729.0 ],
					"source" : [ "obj-93", 0 ]
				}

			}
 ],
		"parameters" : 		{
			"obj-13::obj-107::obj-33" : [ "tab[80]", "tab[1]", 0 ],
			"obj-13::obj-123::obj-33" : [ "tab[78]", "tab[1]", 0 ],
			"obj-13::obj-36::obj-33" : [ "tab[82]", "tab[1]", 0 ],
			"obj-13::obj-40::obj-33" : [ "tab[83]", "tab[1]", 0 ],
			"obj-13::obj-41::obj-33" : [ "tab[84]", "tab[1]", 0 ],
			"obj-13::obj-44::obj-33" : [ "tab[87]", "tab[1]", 0 ],
			"obj-13::obj-45::obj-33" : [ "tab[88]", "tab[1]", 0 ],
			"obj-13::obj-46::obj-33" : [ "tab[89]", "tab[1]", 0 ],
			"obj-13::obj-47::obj-33" : [ "tab[90]", "tab[1]", 0 ],
			"obj-13::obj-48::obj-33" : [ "tab[91]", "tab[1]", 0 ],
			"obj-13::obj-49::obj-33" : [ "tab[92]", "tab[1]", 0 ],
			"obj-13::obj-50::obj-33" : [ "tab[93]", "tab[1]", 0 ],
			"obj-13::obj-6::obj-33" : [ "tab[86]", "tab[1]", 0 ],
			"obj-13::obj-74::obj-33" : [ "tab[79]", "tab[1]", 0 ],
			"obj-13::obj-8::obj-33" : [ "tab[85]", "tab[1]", 0 ],
			"obj-13::obj-9::obj-33" : [ "tab[81]", "tab[1]", 0 ],
			"obj-15" : [ "live.gain~[1]", "leloup~", 0 ],
			"obj-175::obj-107::obj-33" : [ "tab[64]", "tab[1]", 0 ],
			"obj-175::obj-123::obj-33" : [ "tab[125]", "tab[1]", 0 ],
			"obj-175::obj-36::obj-33" : [ "tab[66]", "tab[1]", 0 ],
			"obj-175::obj-40::obj-33" : [ "tab[67]", "tab[1]", 0 ],
			"obj-175::obj-41::obj-33" : [ "tab[68]", "tab[1]", 0 ],
			"obj-175::obj-44::obj-33" : [ "tab[71]", "tab[1]", 0 ],
			"obj-175::obj-45::obj-33" : [ "tab[72]", "tab[1]", 0 ],
			"obj-175::obj-46::obj-33" : [ "tab[73]", "tab[1]", 0 ],
			"obj-175::obj-47::obj-33" : [ "tab[74]", "tab[1]", 0 ],
			"obj-175::obj-48::obj-33" : [ "tab[75]", "tab[1]", 0 ],
			"obj-175::obj-49::obj-33" : [ "tab[76]", "tab[1]", 0 ],
			"obj-175::obj-50::obj-33" : [ "tab[77]", "tab[1]", 0 ],
			"obj-175::obj-6::obj-33" : [ "tab[70]", "tab[1]", 0 ],
			"obj-175::obj-74::obj-33" : [ "tab[63]", "tab[1]", 0 ],
			"obj-175::obj-8::obj-33" : [ "tab[69]", "tab[1]", 0 ],
			"obj-175::obj-9::obj-33" : [ "tab[65]", "tab[1]", 0 ],
			"obj-20::obj-122" : [ "number[3]", "number[1]", 0 ],
			"obj-20::obj-16" : [ "number[4]", "number[1]", 0 ],
			"obj-24::obj-107::obj-33" : [ "tab[18]", "tab[1]", 0 ],
			"obj-24::obj-123::obj-33" : [ "tab[16]", "tab[1]", 0 ],
			"obj-24::obj-34::obj-33" : [ "tab[19]", "tab[1]", 0 ],
			"obj-24::obj-36::obj-33" : [ "tab[20]", "tab[1]", 0 ],
			"obj-24::obj-40::obj-33" : [ "tab[21]", "tab[1]", 0 ],
			"obj-24::obj-41::obj-33" : [ "tab[22]", "tab[1]", 0 ],
			"obj-24::obj-42::obj-33" : [ "tab[23]", "tab[1]", 0 ],
			"obj-24::obj-43::obj-33" : [ "tab[24]", "tab[1]", 0 ],
			"obj-24::obj-44::obj-33" : [ "tab[25]", "tab[1]", 0 ],
			"obj-24::obj-45::obj-33" : [ "tab[26]", "tab[1]", 0 ],
			"obj-24::obj-46::obj-33" : [ "tab[27]", "tab[1]", 0 ],
			"obj-24::obj-47::obj-33" : [ "tab[28]", "tab[1]", 0 ],
			"obj-24::obj-48::obj-33" : [ "tab[29]", "tab[1]", 0 ],
			"obj-24::obj-49::obj-33" : [ "tab[30]", "tab[1]", 0 ],
			"obj-24::obj-50::obj-33" : [ "tab[31]", "tab[1]", 0 ],
			"obj-24::obj-74::obj-33" : [ "tab[17]", "tab[1]", 0 ],
			"obj-2::obj-122" : [ "number[2]", "number[1]", 0 ],
			"obj-2::obj-16" : [ "number[1]", "number[1]", 0 ],
			"obj-31::obj-107::obj-33" : [ "tab[33]", "tab[1]", 0 ],
			"obj-31::obj-123::obj-33" : [ "tab[116]", "tab[1]", 0 ],
			"obj-31::obj-34::obj-33" : [ "tab[34]", "tab[1]", 0 ],
			"obj-31::obj-36::obj-33" : [ "tab[35]", "tab[1]", 0 ],
			"obj-31::obj-40::obj-33" : [ "tab[36]", "tab[1]", 0 ],
			"obj-31::obj-41::obj-33" : [ "tab[37]", "tab[1]", 0 ],
			"obj-31::obj-42::obj-33" : [ "tab[38]", "tab[1]", 0 ],
			"obj-31::obj-43::obj-33" : [ "tab[39]", "tab[1]", 0 ],
			"obj-31::obj-44::obj-33" : [ "tab[40]", "tab[1]", 0 ],
			"obj-31::obj-45::obj-33" : [ "tab[41]", "tab[1]", 0 ],
			"obj-31::obj-46::obj-33" : [ "tab[42]", "tab[1]", 0 ],
			"obj-31::obj-47::obj-33" : [ "tab[43]", "tab[1]", 0 ],
			"obj-31::obj-48::obj-33" : [ "tab[44]", "tab[1]", 0 ],
			"obj-31::obj-49::obj-33" : [ "tab[45]", "tab[1]", 0 ],
			"obj-31::obj-50::obj-33" : [ "tab[46]", "tab[1]", 0 ],
			"obj-31::obj-74::obj-33" : [ "tab[32]", "tab[1]", 0 ],
			"obj-34::obj-107::obj-33" : [ "tab[49]", "tab[1]", 0 ],
			"obj-34::obj-123::obj-33" : [ "tab[47]", "tab[1]", 0 ],
			"obj-34::obj-34::obj-33" : [ "tab[50]", "tab[1]", 0 ],
			"obj-34::obj-36::obj-33" : [ "tab[51]", "tab[1]", 0 ],
			"obj-34::obj-40::obj-33" : [ "tab[52]", "tab[1]", 0 ],
			"obj-34::obj-41::obj-33" : [ "tab[53]", "tab[1]", 0 ],
			"obj-34::obj-42::obj-33" : [ "tab[54]", "tab[1]", 0 ],
			"obj-34::obj-43::obj-33" : [ "tab[55]", "tab[1]", 0 ],
			"obj-34::obj-44::obj-33" : [ "tab[56]", "tab[1]", 0 ],
			"obj-34::obj-45::obj-33" : [ "tab[57]", "tab[1]", 0 ],
			"obj-34::obj-46::obj-33" : [ "tab[58]", "tab[1]", 0 ],
			"obj-34::obj-47::obj-33" : [ "tab[59]", "tab[1]", 0 ],
			"obj-34::obj-48::obj-33" : [ "tab[60]", "tab[1]", 0 ],
			"obj-34::obj-49::obj-33" : [ "tab[61]", "tab[1]", 0 ],
			"obj-34::obj-50::obj-33" : [ "tab[62]", "tab[1]", 0 ],
			"obj-34::obj-74::obj-33" : [ "tab[48]", "tab[1]", 0 ],
			"obj-39" : [ "live.gain~[2]", "live.gain~[2]", 0 ],
			"obj-42::obj-107::obj-33" : [ "tab[130]", "tab[1]", 0 ],
			"obj-42::obj-123::obj-33" : [ "tab[123]", "tab[1]", 0 ],
			"obj-42::obj-36::obj-33" : [ "tab[132]", "tab[1]", 0 ],
			"obj-42::obj-40::obj-33" : [ "tab[133]", "tab[1]", 0 ],
			"obj-42::obj-41::obj-33" : [ "tab[134]", "tab[1]", 0 ],
			"obj-42::obj-44::obj-33" : [ "tab[137]", "tab[1]", 0 ],
			"obj-42::obj-45::obj-33" : [ "tab[138]", "tab[1]", 0 ],
			"obj-42::obj-46::obj-33" : [ "tab[139]", "tab[1]", 0 ],
			"obj-42::obj-47::obj-33" : [ "tab[140]", "tab[1]", 0 ],
			"obj-42::obj-48::obj-33" : [ "tab[141]", "tab[1]", 0 ],
			"obj-42::obj-49::obj-33" : [ "tab[142]", "tab[1]", 0 ],
			"obj-42::obj-50::obj-33" : [ "tab[143]", "tab[1]", 0 ],
			"obj-42::obj-6::obj-33" : [ "tab[136]", "tab[1]", 0 ],
			"obj-42::obj-74::obj-33" : [ "tab[124]", "tab[1]", 0 ],
			"obj-42::obj-8::obj-33" : [ "tab[135]", "tab[1]", 0 ],
			"obj-42::obj-9::obj-33" : [ "tab[131]", "tab[1]", 0 ],
			"obj-49::obj-107::obj-33" : [ "tab[112]", "tab[1]", 0 ],
			"obj-49::obj-123::obj-33" : [ "tab[110]", "tab[1]", 0 ],
			"obj-49::obj-34::obj-33" : [ "tab[117]", "tab[1]", 0 ],
			"obj-49::obj-36::obj-33" : [ "tab[118]", "tab[1]", 0 ],
			"obj-49::obj-40::obj-33" : [ "tab[113]", "tab[1]", 0 ],
			"obj-49::obj-41::obj-33" : [ "tab[114]", "tab[1]", 0 ],
			"obj-49::obj-42::obj-33" : [ "tab[115]", "tab[1]", 0 ],
			"obj-49::obj-43::obj-33" : [ "tab[119]", "tab[1]", 0 ],
			"obj-49::obj-44::obj-33" : [ "tab[126]", "tab[1]", 0 ],
			"obj-49::obj-45::obj-33" : [ "tab[127]", "tab[1]", 0 ],
			"obj-49::obj-46::obj-33" : [ "tab[128]", "tab[1]", 0 ],
			"obj-49::obj-47::obj-33" : [ "tab[129]", "tab[1]", 0 ],
			"obj-49::obj-48::obj-33" : [ "tab[120]", "tab[1]", 0 ],
			"obj-49::obj-49::obj-33" : [ "tab[121]", "tab[1]", 0 ],
			"obj-49::obj-50::obj-33" : [ "tab[122]", "tab[1]", 0 ],
			"obj-49::obj-74::obj-33" : [ "tab[111]", "tab[1]", 0 ],
			"obj-52::obj-122" : [ "number[6]", "number[1]", 0 ],
			"obj-52::obj-16" : [ "number[5]", "number[1]", 0 ],
			"obj-57::obj-107::obj-33" : [ "tab[96]", "tab[1]", 0 ],
			"obj-57::obj-123::obj-33" : [ "tab[94]", "tab[1]", 0 ],
			"obj-57::obj-34::obj-33" : [ "tab[97]", "tab[1]", 0 ],
			"obj-57::obj-36::obj-33" : [ "tab[98]", "tab[1]", 0 ],
			"obj-57::obj-40::obj-33" : [ "tab[99]", "tab[1]", 0 ],
			"obj-57::obj-41::obj-33" : [ "tab[100]", "tab[1]", 0 ],
			"obj-57::obj-42::obj-33" : [ "tab[101]", "tab[1]", 0 ],
			"obj-57::obj-43::obj-33" : [ "tab[102]", "tab[1]", 0 ],
			"obj-57::obj-44::obj-33" : [ "tab[103]", "tab[1]", 0 ],
			"obj-57::obj-45::obj-33" : [ "tab[104]", "tab[1]", 0 ],
			"obj-57::obj-46::obj-33" : [ "tab[105]", "tab[1]", 0 ],
			"obj-57::obj-47::obj-33" : [ "tab[106]", "tab[1]", 0 ],
			"obj-57::obj-48::obj-33" : [ "tab[107]", "tab[1]", 0 ],
			"obj-57::obj-49::obj-33" : [ "tab[108]", "tab[1]", 0 ],
			"obj-57::obj-50::obj-33" : [ "tab[109]", "tab[1]", 0 ],
			"obj-57::obj-74::obj-33" : [ "tab[95]", "tab[1]", 0 ],
			"obj-67" : [ "live.gain~", "leloup~", 0 ],
			"obj-9" : [ "live.gain~[3]", "leloup~", 0 ],
			"obj-93::obj-107::obj-33" : [ "tab[2]", "tab[1]", 0 ],
			"obj-93::obj-123::obj-33" : [ "tab[182]", "tab[1]", 0 ],
			"obj-93::obj-34::obj-33" : [ "tab[3]", "tab[1]", 0 ],
			"obj-93::obj-36::obj-33" : [ "tab[4]", "tab[1]", 0 ],
			"obj-93::obj-40::obj-33" : [ "tab[5]", "tab[1]", 0 ],
			"obj-93::obj-41::obj-33" : [ "tab[6]", "tab[1]", 0 ],
			"obj-93::obj-42::obj-33" : [ "tab[7]", "tab[1]", 0 ],
			"obj-93::obj-43::obj-33" : [ "tab[8]", "tab[1]", 0 ],
			"obj-93::obj-44::obj-33" : [ "tab[9]", "tab[1]", 0 ],
			"obj-93::obj-45::obj-33" : [ "tab[10]", "tab[1]", 0 ],
			"obj-93::obj-46::obj-33" : [ "tab[11]", "tab[1]", 0 ],
			"obj-93::obj-47::obj-33" : [ "tab[12]", "tab[1]", 0 ],
			"obj-93::obj-48::obj-33" : [ "tab[13]", "tab[1]", 0 ],
			"obj-93::obj-49::obj-33" : [ "tab[14]", "tab[1]", 0 ],
			"obj-93::obj-50::obj-33" : [ "tab[15]", "tab[1]", 0 ],
			"obj-93::obj-74::obj-33" : [ "tab[1]", "tab[1]", 0 ],
			"parameterbanks" : 			{
				"0" : 				{
					"index" : 0,
					"name" : "",
					"parameters" : [ "-", "-", "-", "-", "-", "-", "-", "-" ]
				}

			}
,
			"inherited_shortname" : 1
		}
,
		"dependency_cache" : [ 			{
				"name" : "RD-perf-a.json",
				"bootpath" : "~/Documents/GitHub/bbdmi/events/2023-11-24_Athenor/RD/preset",
				"patcherrelativepath" : "../../2023-11-24_Athenor/RD/preset",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bbdmi.calibrate.maxpat",
				"bootpath" : "~/Documents/GitHub/bbdmi/max/control_processing/calibrate",
				"patcherrelativepath" : "../../../max/control_processing/calibrate",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bbdmi.crosspatch.maxpat",
				"bootpath" : "~/Documents/GitHub/bbdmi/max/utilities/crosspatch",
				"patcherrelativepath" : "../../../max/utilities/crosspatch",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bbdmi.eavi.maxpat",
				"bootpath" : "~/Documents/GitHub/bbdmi/max/input/eavi",
				"patcherrelativepath" : "../../../max/input/eavi",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bbdmi.list2~.maxpat",
				"bootpath" : "~/Documents/GitHub/bbdmi/max/utilities/list2~",
				"patcherrelativepath" : "../../../max/utilities/list2~",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bbdmi.multislider.maxpat",
				"bootpath" : "~/Documents/GitHub/bbdmi/max/utilities/multislider",
				"patcherrelativepath" : "../../../max/utilities/multislider",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bbdmi.onepole~.maxpat",
				"bootpath" : "~/Documents/GitHub/bbdmi/max/utilities/onepole~",
				"patcherrelativepath" : "../../../max/utilities/onepole~",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bbdmi.rms~.maxpat",
				"bootpath" : "~/Documents/GitHub/bbdmi/max/feature_extraction/rms~",
				"patcherrelativepath" : "../../../max/feature_extraction/rms~",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bbdmi.scale.maxpat",
				"bootpath" : "~/Documents/GitHub/bbdmi/max/control_processing/scale",
				"patcherrelativepath" : "../../../max/control_processing/scale",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bbdmi.send.maxpat",
				"bootpath" : "~/Documents/GitHub/bbdmi/max/utilities/send",
				"patcherrelativepath" : "../../../max/utilities/send",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bbdmi.smooth.maxpat",
				"bootpath" : "~/Documents/GitHub/bbdmi/max/control_processing/smooth",
				"patcherrelativepath" : "../../../max/control_processing/smooth",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "name.js",
				"bootpath" : "~/Documents/GitHub/bbdmi/max/source/js",
				"patcherrelativepath" : "../../../max/source/js",
				"type" : "TEXT",
				"implicit" : 1
			}
, 			{
				"name" : "p.calibrate.maxpat",
				"bootpath" : "~/Documents/GitHub/bbdmi/max/control_processing/calibrate",
				"patcherrelativepath" : "../../../max/control_processing/calibrate",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "p.exposer.maxpat",
				"bootpath" : "~/Documents/GitHub/bbdmi/max/source/patchers",
				"patcherrelativepath" : "../../../max/source/patchers",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "p.onepole~.maxpat",
				"bootpath" : "~/Documents/GitHub/bbdmi/max/utilities/onepole~",
				"patcherrelativepath" : "../../../max/utilities/onepole~",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "p.scale.maxpat",
				"bootpath" : "~/Documents/GitHub/bbdmi/max/control_processing/scale",
				"patcherrelativepath" : "../../../max/control_processing/scale",
				"type" : "JSON",
				"implicit" : 1
			}
 ],
		"autosave" : 0,
		"bgcolor" : [ 0.898039215686275, 0.898039215686275, 0.898039215686275, 1.0 ],
		"editing_bgcolor" : [ 0.898039215686275, 0.898039215686275, 0.898039215686275, 1.0 ]
	}

}
