{
	"patcher" : 	{
		"fileversion" : 1,
		"appversion" : 		{
			"major" : 8,
			"minor" : 5,
			"revision" : 6,
			"architecture" : "x64",
			"modernui" : 1
		}
,
		"classnamespace" : "box",
		"rect" : [ 34.0, 101.0, 1115.0, 823.0 ],
		"bglocked" : 0,
		"openinpresentation" : 0,
		"default_fontsize" : 12.0,
		"default_fontface" : 0,
		"default_fontname" : "Arial",
		"gridonopen" : 1,
		"gridsize" : [ 15.0, 15.0 ],
		"gridsnaponopen" : 1,
		"objectsnaponopen" : 1,
		"statusbarvisible" : 2,
		"toolbarvisible" : 1,
		"lefttoolbarpinned" : 0,
		"toptoolbarpinned" : 0,
		"righttoolbarpinned" : 0,
		"bottomtoolbarpinned" : 0,
		"toolbars_unpinned_last_save" : 0,
		"tallnewobj" : 0,
		"boxanimatetime" : 200,
		"enablehscroll" : 1,
		"enablevscroll" : 1,
		"devicewidth" : 0.0,
		"description" : "",
		"digest" : "",
		"tags" : "",
		"style" : "",
		"subpatcher_template" : "",
		"assistshowspatchername" : 0,
		"boxes" : [ 			{
				"box" : 				{
					"fontface" : 3,
					"id" : "obj-26",
					"linecount" : 3,
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 3.0, 237.333328664302826, 165.0, 47.0 ],
					"text" : "need to specify instrument and gran parameter in SEND b-patchers",
					"textcolor" : [ 0.941176470588235, 0.219607843137255, 0.219607843137255, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-25",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 3.0, 180.0, 150.0, 20.0 ],
					"text" : "EAVI input and mapping"
				}

			}
, 			{
				"box" : 				{
					"color" : [ 1.0, 0.694117647058824, 0.0, 1.0 ],
					"fontface" : 1,
					"fontsize" : 18.0,
					"id" : "obj-24",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 10.0, 207.333328664302826, 65.0, 29.0 ],
					"text" : "EAVI-2",
					"textcolor" : [ 0.0, 0.996078431372549, 0.098039215686275, 1.0 ],
					"varname" : "EAVI-2"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-22",
					"linecount" : 3,
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 771.470131926238537, 4.333328664302826, 266.0, 47.0 ],
					"text" : "ADC 1/2 = micro bass clar\nADC 3 = clavier elctronique piano\nADC 4 = micro guitar electrique préparée"
				}

			}
, 			{
				"box" : 				{
					"fontface" : 1,
					"id" : "obj-21",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 947.102598944678903, 727.835540967701036, 160.0, 20.0 ],
					"text" : "piano parameters here",
					"textcolor" : [ 0.941176470588235, 0.219607843137255, 0.219607843137255, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontface" : 1,
					"id" : "obj-19",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 541.102598944678903, 727.835540967701036, 160.0, 20.0 ],
					"text" : "bass clar parameters here",
					"textcolor" : [ 0.941176470588235, 0.219607843137255, 0.219607843137255, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-13",
					"linecount" : 2,
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 865.980087950825691, 55.0, 150.0, 33.0 ],
					"text" : "Piano recording\npiano-lo-hit-44.wav"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-11",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 527.427762925624847, 57.0, 49.0, 20.0 ],
					"text" : "Dry/wet"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-10",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 457.0, 55.0, 49.0, 20.0 ],
					"text" : "Dry/wet"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-8",
					"linecount" : 3,
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 637.0, 212.333328664302826, 42.0, 47.0 ],
					"text" : "Send to Gran"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-9",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 587.0, 231.333328664302826, 27.0, 20.0 ],
					"text" : "Dry"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-7",
					"linecount" : 3,
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 238.0, 228.333328664302826, 42.0, 47.0 ],
					"text" : "Send to Gran"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-6",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 188.0, 247.333328664302826, 27.0, 20.0 ],
					"text" : "Dry"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-4",
					"linecount" : 2,
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 40.509956024587154, 50.5, 150.0, 33.0 ],
					"text" : "Bass clarinet recording\nbassclar-01.wav"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-50",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patcher" : 					{
						"fileversion" : 1,
						"appversion" : 						{
							"major" : 8,
							"minor" : 5,
							"revision" : 6,
							"architecture" : "x64",
							"modernui" : 1
						}
,
						"classnamespace" : "box",
						"rect" : [ 59.0, 106.0, 640.0, 480.0 ],
						"bglocked" : 0,
						"openinpresentation" : 0,
						"default_fontsize" : 12.0,
						"default_fontface" : 0,
						"default_fontname" : "Arial",
						"gridonopen" : 1,
						"gridsize" : [ 15.0, 15.0 ],
						"gridsnaponopen" : 1,
						"objectsnaponopen" : 1,
						"statusbarvisible" : 2,
						"toolbarvisible" : 1,
						"lefttoolbarpinned" : 0,
						"toptoolbarpinned" : 0,
						"righttoolbarpinned" : 0,
						"bottomtoolbarpinned" : 0,
						"toolbars_unpinned_last_save" : 0,
						"tallnewobj" : 0,
						"boxanimatetime" : 200,
						"enablehscroll" : 1,
						"enablevscroll" : 1,
						"devicewidth" : 0.0,
						"description" : "",
						"digest" : "",
						"tags" : "",
						"style" : "",
						"subpatcher_template" : "",
						"assistshowspatchername" : 0,
						"boxes" : [ 							{
								"box" : 								{
									"id" : "obj-48",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 50.0, 129.0, 29.5, 22.0 ],
									"text" : "0"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-49",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 80.75, 129.0, 29.5, 22.0 ],
									"text" : "-70"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-46",
									"maxclass" : "newobj",
									"numinlets" : 3,
									"numoutlets" : 3,
									"outlettype" : [ "bang", "bang", "" ],
									"patching_rect" : [ 77.75, 100.0, 60.0, 22.0 ],
									"text" : "select 0 1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-41",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 145.75, 129.0, 29.5, 22.0 ],
									"text" : "0"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-39",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 116.75, 129.0, 29.5, 22.0 ],
									"text" : "-70"
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-50",
									"index" : 1,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"patching_rect" : [ 77.75, 40.0, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-51",
									"index" : 1,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 77.375, 211.0, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-53",
									"index" : 2,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 112.375, 211.0, 30.0, 30.0 ]
								}

							}
 ],
						"lines" : [ 							{
								"patchline" : 								{
									"destination" : [ "obj-51", 0 ],
									"source" : [ "obj-39", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-53", 0 ],
									"source" : [ "obj-41", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-39", 0 ],
									"order" : 1,
									"source" : [ "obj-46", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-41", 0 ],
									"order" : 0,
									"source" : [ "obj-46", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-48", 0 ],
									"order" : 1,
									"source" : [ "obj-46", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-49", 0 ],
									"order" : 0,
									"source" : [ "obj-46", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-51", 0 ],
									"source" : [ "obj-48", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-53", 0 ],
									"source" : [ "obj-49", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-46", 0 ],
									"source" : [ "obj-50", 0 ]
								}

							}
 ]
					}
,
					"patching_rect" : [ 501.427762925624847, 84.835540967701036, 34.25, 22.0 ],
					"saved_object_attributes" : 					{
						"description" : "",
						"digest" : "",
						"globalpatchername" : "",
						"tags" : ""
					}
,
					"text" : "p"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-51",
					"maxclass" : "toggle",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 501.427762925624847, 57.0, 24.0, 24.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-48",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patcher" : 					{
						"fileversion" : 1,
						"appversion" : 						{
							"major" : 8,
							"minor" : 5,
							"revision" : 6,
							"architecture" : "x64",
							"modernui" : 1
						}
,
						"classnamespace" : "box",
						"rect" : [ 59.0, 106.0, 640.0, 480.0 ],
						"bglocked" : 0,
						"openinpresentation" : 0,
						"default_fontsize" : 12.0,
						"default_fontface" : 0,
						"default_fontname" : "Arial",
						"gridonopen" : 1,
						"gridsize" : [ 15.0, 15.0 ],
						"gridsnaponopen" : 1,
						"objectsnaponopen" : 1,
						"statusbarvisible" : 2,
						"toolbarvisible" : 1,
						"lefttoolbarpinned" : 0,
						"toptoolbarpinned" : 0,
						"righttoolbarpinned" : 0,
						"bottomtoolbarpinned" : 0,
						"toolbars_unpinned_last_save" : 0,
						"tallnewobj" : 0,
						"boxanimatetime" : 200,
						"enablehscroll" : 1,
						"enablevscroll" : 1,
						"devicewidth" : 0.0,
						"description" : "",
						"digest" : "",
						"tags" : "",
						"style" : "",
						"subpatcher_template" : "",
						"assistshowspatchername" : 0,
						"boxes" : [ 							{
								"box" : 								{
									"id" : "obj-48",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 50.0, 129.0, 29.5, 22.0 ],
									"text" : "0"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-49",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 80.75, 129.0, 29.5, 22.0 ],
									"text" : "-70"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-46",
									"maxclass" : "newobj",
									"numinlets" : 3,
									"numoutlets" : 3,
									"outlettype" : [ "bang", "bang", "" ],
									"patching_rect" : [ 77.75, 100.0, 60.0, 22.0 ],
									"text" : "select 0 1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-41",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 145.75, 129.0, 29.5, 22.0 ],
									"text" : "0"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-39",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 116.75, 129.0, 29.5, 22.0 ],
									"text" : "-70"
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-50",
									"index" : 1,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"patching_rect" : [ 77.75, 40.0, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-51",
									"index" : 1,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 77.375, 211.0, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-53",
									"index" : 2,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 112.375, 211.0, 30.0, 30.0 ]
								}

							}
 ],
						"lines" : [ 							{
								"patchline" : 								{
									"destination" : [ "obj-51", 0 ],
									"source" : [ "obj-39", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-53", 0 ],
									"source" : [ "obj-41", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-39", 0 ],
									"order" : 1,
									"source" : [ "obj-46", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-41", 0 ],
									"order" : 0,
									"source" : [ "obj-46", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-48", 0 ],
									"order" : 1,
									"source" : [ "obj-46", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-49", 0 ],
									"order" : 0,
									"source" : [ "obj-46", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-51", 0 ],
									"source" : [ "obj-48", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-53", 0 ],
									"source" : [ "obj-49", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-46", 0 ],
									"source" : [ "obj-50", 0 ]
								}

							}
 ]
					}
,
					"patching_rect" : [ 431.0, 82.835540967701036, 34.25, 22.0 ],
					"saved_object_attributes" : 					{
						"description" : "",
						"digest" : "",
						"globalpatchername" : "",
						"tags" : ""
					}
,
					"text" : "p"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-49",
					"maxclass" : "toggle",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 431.0, 55.0, 24.0, 24.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-15",
					"lastchannelcount" : 0,
					"maxclass" : "live.gain~",
					"numinlets" : 2,
					"numoutlets" : 5,
					"outlettype" : [ "signal", "signal", "", "float", "list" ],
					"parameter_enable" : 1,
					"patching_rect" : [ 657.102598944678903, 135.333328664302826, 47.0, 136.0 ],
					"saved_attribute_attributes" : 					{
						"valueof" : 						{
							"parameter_longname" : "live.gain~[8]",
							"parameter_mmax" : 6.0,
							"parameter_mmin" : -70.0,
							"parameter_shortname" : "live.gain~",
							"parameter_type" : 0,
							"parameter_unitstyle" : 4
						}

					}
,
					"varname" : "live.gain~[8]"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-31",
					"lastchannelcount" : 0,
					"maxclass" : "live.gain~",
					"numinlets" : 2,
					"numoutlets" : 5,
					"outlettype" : [ "signal", "signal", "", "float", "list" ],
					"parameter_enable" : 1,
					"patching_rect" : [ 739.0, 813.333328664302826, 47.0, 136.0 ],
					"saved_attribute_attributes" : 					{
						"valueof" : 						{
							"parameter_longname" : "live.gain~[6]",
							"parameter_mmax" : 6.0,
							"parameter_mmin" : -70.0,
							"parameter_shortname" : "live.gain~",
							"parameter_type" : 0,
							"parameter_unitstyle" : 4
						}

					}
,
					"varname" : "live.gain~[6]"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-33",
					"lastchannelcount" : 0,
					"maxclass" : "live.gain~",
					"numinlets" : 2,
					"numoutlets" : 5,
					"outlettype" : [ "signal", "signal", "", "float", "list" ],
					"parameter_enable" : 1,
					"patching_rect" : [ 601.102598944678903, 135.333328664302826, 47.0, 136.0 ],
					"saved_attribute_attributes" : 					{
						"valueof" : 						{
							"parameter_longname" : "live.gain~[7]",
							"parameter_mmax" : 6.0,
							"parameter_mmin" : -70.0,
							"parameter_shortname" : "live.gain~",
							"parameter_type" : 0,
							"parameter_unitstyle" : 4
						}

					}
,
					"varname" : "live.gain~[7]"
				}

			}
, 			{
				"box" : 				{
					"color" : [ 1.0, 0.694117647058824, 0.0, 1.0 ],
					"fontface" : 1,
					"fontsize" : 16.0,
					"id" : "obj-34",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patcher" : 					{
						"fileversion" : 1,
						"appversion" : 						{
							"major" : 8,
							"minor" : 5,
							"revision" : 6,
							"architecture" : "x64",
							"modernui" : 1
						}
,
						"classnamespace" : "box",
						"rect" : [ 166.0, 129.0, 1284.0, 804.0 ],
						"bglocked" : 0,
						"openinpresentation" : 0,
						"default_fontsize" : 12.0,
						"default_fontface" : 0,
						"default_fontname" : "Arial",
						"gridonopen" : 1,
						"gridsize" : [ 15.0, 15.0 ],
						"gridsnaponopen" : 1,
						"objectsnaponopen" : 1,
						"statusbarvisible" : 2,
						"toolbarvisible" : 1,
						"lefttoolbarpinned" : 0,
						"toptoolbarpinned" : 0,
						"righttoolbarpinned" : 0,
						"bottomtoolbarpinned" : 0,
						"toolbars_unpinned_last_save" : 0,
						"tallnewobj" : 0,
						"boxanimatetime" : 200,
						"enablehscroll" : 1,
						"enablevscroll" : 1,
						"devicewidth" : 0.0,
						"description" : "",
						"digest" : "",
						"tags" : "",
						"style" : "",
						"subpatcher_template" : "",
						"assistshowspatchername" : 0,
						"boxes" : [ 							{
								"box" : 								{
									"id" : "obj-14",
									"maxclass" : "newobj",
									"numinlets" : 0,
									"numoutlets" : 0,
									"patcher" : 									{
										"fileversion" : 1,
										"appversion" : 										{
											"major" : 8,
											"minor" : 5,
											"revision" : 6,
											"architecture" : "x64",
											"modernui" : 1
										}
,
										"classnamespace" : "box",
										"rect" : [ 59.0, 118.0, 640.0, 480.0 ],
										"bglocked" : 0,
										"openinpresentation" : 0,
										"default_fontsize" : 12.0,
										"default_fontface" : 0,
										"default_fontname" : "Arial",
										"gridonopen" : 1,
										"gridsize" : [ 15.0, 15.0 ],
										"gridsnaponopen" : 1,
										"objectsnaponopen" : 1,
										"statusbarvisible" : 2,
										"toolbarvisible" : 1,
										"lefttoolbarpinned" : 0,
										"toptoolbarpinned" : 0,
										"righttoolbarpinned" : 0,
										"bottomtoolbarpinned" : 0,
										"toolbars_unpinned_last_save" : 0,
										"tallnewobj" : 0,
										"boxanimatetime" : 200,
										"enablehscroll" : 1,
										"enablevscroll" : 1,
										"devicewidth" : 0.0,
										"description" : "",
										"digest" : "",
										"tags" : "",
										"style" : "",
										"subpatcher_template" : "",
										"assistshowspatchername" : 0,
										"boxes" : [ 											{
												"box" : 												{
													"id" : "obj-13",
													"linecount" : 18,
													"maxclass" : "message",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 308.105525851249695, 410.643398344516868, 226.894474148750305, 263.0 ],
													"text" : ";\rinstr transpout 0.;\rinstr feedback 0.2;\rinstr gain 1.;\rinstr variability 0.3;\rinstr transpgrain 0.;\rinstr modfreqmod 0.5;\rinstr modmorph 0.;\rinstr modfreq 500.;\rinstr modfactor 0.;\rinstr spacing 80;\rinstr grainoffset 40;\rinstr grainsize 40;\rinstr maxdelay 2000.;\rinstr lpffreq 20000.;\rinstr hpffreq 20.;\rinstr grainenvmorph 1.;\rinstr indexdistr 0;\r"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-11",
													"maxclass" : "comment",
													"numinlets" : 1,
													"numoutlets" : 0,
													"patching_rect" : [ 571.0, 392.0, 150.0, 20.0 ],
													"text" : "voice 11 feb"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-124",
													"linecount" : 18,
													"maxclass" : "message",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 574.105525851249695, 418.643398344516868, 226.894474148750305, 263.0 ],
													"text" : ";\rinstr transpout 10.;\rinstr feedback 0.991;\rinstr gain 1.;\rinstr variability 0.44;\rinstr transpgrain 5.;\rinstr modfreqmod 0.9;\rinstr modmorph 0.098;\rinstr modfreq 199.;\rinstr modfactor 0.41;\rinstr spacing 190;\rinstr grainoffset 0;\rinstr grainsize 211;\rinstr maxdelay 2000.;\rinstr lpffreq 20000.;\rinstr hpffreq 20.;\rinstr grainenvmorph 0.445;\rinstr indexdistr 1;\r"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-10",
													"maxclass" : "comment",
													"numinlets" : 1,
													"numoutlets" : 0,
													"patching_rect" : [ 72.0, 682.0, 150.0, 20.0 ],
													"text" : "piano transpose 1"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-9",
													"linecount" : 18,
													"maxclass" : "message",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 72.0, 708.0, 226.894474148750305, 263.0 ],
													"text" : ";\rinstr transpout 0.;\rinstr feedback 0.;\rinstr gain 1.;\rinstr variability 0.;\rinstr transpgrain 1.;\rinstr modfreqmod 0.;\rinstr modmorph 0.;\rinstr modfreq 0.;\rinstr modfactor 0.;\rinstr spacing 1;\rinstr grainoffset 0;\rinstr grainsize 250;\rinstr maxdelay 500.;\rinstr lpffreq 20000.;\rinstr hpffreq 20.;\rinstr grainenvmorph 1.;\rinstr indexdistr 0;\r"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-8",
													"maxclass" : "comment",
													"numinlets" : 1,
													"numoutlets" : 0,
													"patching_rect" : [ 322.0, 682.0, 150.0, 20.0 ],
													"text" : "piano transpose 2"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-7",
													"linecount" : 18,
													"maxclass" : "message",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 326.0, 704.0, 226.894474148750305, 263.0 ],
													"text" : ";\rinstr transpout 6.;\rinstr feedback 0.7;\rinstr gain 1.;\rinstr variability 0.;\rinstr transpgrain 0.;\rinstr modfreqmod 0.;\rinstr modmorph 0.;\rinstr modfreq 0.;\rinstr modfactor 0.;\rinstr spacing 1;\rinstr grainoffset 0;\rinstr grainsize 250;\rinstr maxdelay 500.;\rinstr lpffreq 20000.;\rinstr hpffreq 20.;\rinstr grainenvmorph 1.;\rinstr indexdistr 0;\r"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-5",
													"linecount" : 18,
													"maxclass" : "message",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 68.0, 407.0, 226.894474148750305, 263.0 ],
													"text" : ";\rinstr transpout 0.;\rinstr feedback 0.;\rinstr gain 1.;\rinstr variability 0.5;\rinstr transpgrain 0.;\rinstr modfreqmod 0.;\rinstr modmorph 0.;\rinstr modfreq 0.;\rinstr modfactor 0.;\rinstr spacing 1;\rinstr grainoffset 50;\rinstr grainsize 250;\rinstr maxdelay 500.;\rinstr lpffreq 20000.;\rinstr hpffreq 20.;\rinstr grainenvmorph 1.;\rinstr indexdistr 0;\r"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-2",
													"maxclass" : "comment",
													"numinlets" : 1,
													"numoutlets" : 0,
													"patching_rect" : [ 68.0, 379.0, 150.0, 20.0 ],
													"text" : "clarinet"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-1",
													"maxclass" : "comment",
													"numinlets" : 1,
													"numoutlets" : 0,
													"patching_rect" : [ 68.0, 36.0, 150.0, 20.0 ],
													"text" : "viola"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-34",
													"linecount" : 18,
													"maxclass" : "message",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 296.447237074375153, 66.0, 226.894474148750305, 263.0 ],
													"text" : ";\rinstr transpout 0.;\rinstr feedback 0.5;\rinstr gain 1.;\rinstr variability 0.;\rinstr transpgrain 0.;\rinstr modfreqmod 0.;\rinstr modmorph 0.;\rinstr modfreq 2.9;\rinstr modfactor 0.7;\rinstr spacing 17;\rinstr grainoffset 40;\rinstr grainsize 50;\rinstr maxdelay 300.;\rinstr lpffreq 20000.;\rinstr hpffreq 20.;\rinstr grainenvmorph 1.;\rinstr indexdistr 0;\r"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-29",
													"linecount" : 18,
													"maxclass" : "message",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 57.0, 66.0, 226.894474148750305, 263.0 ],
													"text" : ";\rinstr transpout 0.;\rinstr feedback 0.;\rinstr gain 1.;\rinstr variability 0.;\rinstr transpgrain 0.;\rinstr modfreqmod 0.5;\rinstr modmorph 0.;\rinstr modfreq 1000.;\rinstr modfactor 0.;\rinstr spacing 16;\rinstr grainoffset 0;\rinstr grainsize 219;\rinstr maxdelay 2000.;\rinstr lpffreq 20000.;\rinstr hpffreq 20.;\rinstr grainenvmorph 1.;\rinstr indexdistr 0;\r"
												}

											}
 ],
										"lines" : [  ]
									}
,
									"patching_rect" : [ 95.0, 45.0, 35.0, 22.0 ],
									"saved_object_attributes" : 									{
										"description" : "",
										"digest" : "",
										"globalpatchername" : "",
										"tags" : ""
									}
,
									"text" : "p old"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-74",
									"linecount" : 22,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1065.0, 442.0, 158.0, 317.0 ],
									"text" : ";\rpiano transpout 11.044334;\rpiano feedback 0.4;\rpiano gain 1.;\rpiano variability 0.234717;\rpiano transpgrain 9.041521;\rpiano modfreqmod 0.395239;\rpiano modmorph 1.888272;\rpiano modfreq 5181.219287;\rpiano modfactor 0.59711;\rpiano spacing 257;\rpiano grainoffset 243;\rpiano grainsize 200;\rpiano maxdelay 3477.948085;\rpiano lpffreq 17645.617938;\rpiano hpffreq 136.927723;\rpiano grainenvmorph 0.251452;\rpiano indexdistr 17;\r"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-73",
									"linecount" : 23,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 998.105525851249695, 380.00221230339821, 157.894474148750305, 330.0 ],
									"text" : ";\rpiano transpout 42.7;\rpiano feedback 0.833806;\rpiano gain 1.;\rpiano variability 0.103467;\rpiano transpgrain -42.353346;\rpiano modfreqmod 0.19328;\rpiano modmorph 2.926464;\rpiano modfreq 1355.294321;\rpiano modfactor 0.024476;\rpiano spacing 87;\rpiano grainoffset 546;\rpiano grainsize 200;\rpiano maxdelay 4888.035099;\rpiano lpffreq 9899.430395;\rpiano hpffreq 849.965203;\rpiano grainenvmorph 0.245914;\rpiano indexdistr 6;\r"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-72",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 936.0, 354.666671335697174, 150.0, 20.0 ],
									"text" : "guitar"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-67",
									"linecount" : 18,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 858.0, 380.00221230339821, 157.894474148750305, 263.0 ],
									"text" : ";\rpiano transpout 0.;\rpiano feedback 0.2;\rpiano gain 1.;\rpiano variability 0.8;\rpiano transpgrain 0.;\rpiano modfreqmod 0.;\rpiano modmorph 0.8;\rpiano modfreq 0.5;\rpiano modfactor 0.8;\rpiano spacing 83;\rpiano grainoffset 50;\rpiano grainsize 165;\rpiano maxdelay 500.;\rpiano lpffreq 7000.;\rpiano hpffreq 200.;\rpiano grainenvmorph 0.;\rpiano indexdistr 3;\r"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-66",
									"linecount" : 18,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 733.105525851249695, 108.168881672142106, 157.894474148750305, 263.0 ],
									"text" : ";\rpiano transpout 2.;\rpiano feedback 0.2;\rpiano gain 1.;\rpiano variability 0.8;\rpiano transpgrain 2.;\rpiano modfreqmod 0.;\rpiano modmorph 0.5;\rpiano modfreq 1.;\rpiano modfactor 0.5;\rpiano spacing 121;\rpiano grainoffset 50;\rpiano grainsize 441;\rpiano maxdelay 2000.;\rpiano lpffreq 5000.;\rpiano hpffreq 20.;\rpiano grainenvmorph 0.;\rpiano indexdistr 2;\r"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-65",
									"linecount" : 18,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 667.105525851249695, 375.0, 157.894474148750305, 263.0 ],
									"text" : ";\rpiano transpout -1.;\rpiano feedback 0.4;\rpiano gain 1.;\rpiano variability 0.8;\rpiano transpgrain -0.5;\rpiano modfreqmod 0.;\rpiano modmorph 0.;\rpiano modfreq 500.;\rpiano modfactor 0.;\rpiano spacing 128;\rpiano grainoffset 151;\rpiano grainsize 445;\rpiano maxdelay 2000.;\rpiano lpffreq 5000.;\rpiano hpffreq 20.;\rpiano grainenvmorph 0.;\rpiano indexdistr 1;\r"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-63",
									"linecount" : 18,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 613.105525851249695, 319.0, 157.894474148750305, 263.0 ],
									"text" : ";\rpiano transpout 10.;\rpiano feedback 0.8;\rpiano gain 1.;\rpiano variability 0.3;\rpiano transpgrain 0.;\rpiano modfreqmod 0.;\rpiano modmorph 0.;\rpiano modfreq 500.;\rpiano modfactor 0.;\rpiano spacing 80;\rpiano grainoffset 40;\rpiano grainsize 40;\rpiano maxdelay 2000.;\rpiano lpffreq 20000.;\rpiano hpffreq 20.;\rpiano grainenvmorph 0.5;\rpiano indexdistr 0;\r"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-43",
									"linecount" : 22,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 555.105525851249695, 41.0, 158.0, 317.0 ],
									"text" : ";\rpiano transpout 11.044334;\rpiano feedback 0.4;\rpiano gain 1.;\rpiano variability 0.234717;\rpiano transpgrain 9.041521;\rpiano modfreqmod 0.395239;\rpiano modmorph 1.888272;\rpiano modfreq 5181.219287;\rpiano modfactor 0.59711;\rpiano spacing 257;\rpiano grainoffset 243;\rpiano grainsize 200;\rpiano maxdelay 3477.948085;\rpiano lpffreq 17645.617938;\rpiano hpffreq 136.927723;\rpiano grainenvmorph 0.251452;\rpiano indexdistr 17;\r"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-6",
									"linecount" : 18,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 264.0, 104.0, 138.0, 263.0 ],
									"text" : ";\rpiano transpout 0.;\rpiano feedback 0.;\rpiano gain 1.;\rpiano variability 0.;\rpiano transpgrain 0.;\rpiano modfreqmod 0.;\rpiano modmorph 0.;\rpiano modfreq 0.;\rpiano modfactor 0.;\rpiano spacing 1;\rpiano grainoffset 0;\rpiano grainsize 250;\rpiano maxdelay 500.;\rpiano lpffreq 20000.;\rpiano hpffreq 20.;\rpiano grainenvmorph 1.;\rpiano indexdistr 0;\r"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-4",
									"linecount" : 18,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 6.0, 104.0, 226.894474148750305, 263.0 ],
									"text" : ";\rpiano transpout 11.044334;\rpiano feedback 0.4;\rpiano gain 1.;\rpiano variability 0.234717;\rpiano transpgrain 9.041521;\rpiano modfreqmod 0.395239;\rpiano modmorph 1.888272;\rpiano modfreq 5181.219287;\rpiano modfactor 0.59711;\rpiano spacing 257;\rpiano grainoffset 243;\rpiano grainsize 200;\rpiano maxdelay 3477.948085;\rpiano lpffreq 17645.617938;\rpiano hpffreq 136.927723;\rpiano grainenvmorph 0.251452;\rpiano indexdistr 17;\r"
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-3",
									"index" : 1,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 213.894474148750305, 41.0, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-20",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 270.0, 74.0, 150.0, 20.0 ],
									"text" : "reset"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-12",
									"linecount" : 18,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 264.0, 387.0, 138.0, 263.0 ],
									"text" : ";\rpiano transpout 0.;\rpiano feedback 0.2;\rpiano gain 1.;\rpiano variability 0.3;\rpiano transpgrain 0.;\rpiano modfreqmod 0.5;\rpiano modmorph 0.;\rpiano modfreq 500.;\rpiano modfactor 0.;\rpiano spacing 80;\rpiano grainoffset 40;\rpiano grainsize 40;\rpiano maxdelay 2000.;\rpiano lpffreq 20000.;\rpiano hpffreq 20.;\rpiano grainenvmorph 1.;\rpiano indexdistr 0;\r"
								}

							}
 ],
						"lines" : [ 							{
								"patchline" : 								{
									"destination" : [ "obj-4", 1 ],
									"source" : [ "obj-3", 0 ]
								}

							}
 ]
					}
,
					"patching_rect" : [ 919.0, 748.33332622051239, 80.0, 26.0 ],
					"saved_object_attributes" : 					{
						"description" : "",
						"digest" : "",
						"globalpatchername" : "",
						"tags" : ""
					}
,
					"text" : "p presets",
					"textcolor" : [ 0.0, 0.996078431372549, 0.098039215686275, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-35",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "signal" ],
					"patching_rect" : [ 739.0, 777.0, 74.0, 22.0 ],
					"text" : "mc.unpack~"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-38",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "multichannelsignal" ],
					"patching_rect" : [ 739.0, 748.33332622051239, 92.0, 22.0 ],
					"text" : "mc.mixdown~ 2"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-39",
					"maxclass" : "newobj",
					"numinlets" : 4,
					"numoutlets" : 1,
					"outlettype" : [ "multichannelsignal" ],
					"patching_rect" : [ 739.0, 135.333328664302826, 70.0, 22.0 ],
					"text" : "mc.pack~ 4"
				}

			}
, 			{
				"box" : 				{
					"basictuning" : 440,
					"clipheight" : 28.0,
					"data" : 					{
						"clips" : [ 							{
								"absolutepath" : "piano-lo-hit-44.wav",
								"filename" : "piano-lo-hit-44.wav",
								"filekind" : "audiofile",
								"id" : "u518011897",
								"selection" : [ 0.126623376623377, 0.574675324675325 ],
								"loop" : 1,
								"content_state" : 								{
									"loop" : 1
								}

							}
 ]
					}
,
					"followglobaltempo" : 0,
					"formantcorrection" : 0,
					"id" : "obj-40",
					"maxclass" : "playlist~",
					"mode" : "basic",
					"numinlets" : 1,
					"numoutlets" : 5,
					"originallength" : [ 0.0, "ticks" ],
					"originaltempo" : 120.0,
					"outlettype" : [ "signal", "signal", "signal", "", "dictionary" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 865.980087950825691, 86.99778769660179, 181.490043975412846, 29.0 ],
					"pitchcorrection" : 0,
					"quality" : "basic",
					"timestretch" : [ 0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-41",
					"maxclass" : "meter~",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "float" ],
					"patching_rect" : [ 835.0, 748.33332622051239, 59.0, 62.0 ]
				}

			}
, 			{
				"box" : 				{
					"args" : [ "piano", "@Channels", 7 ],
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-44",
					"lockeddragscroll" : 0,
					"lockedsize" : 0,
					"maxclass" : "bpatcher",
					"name" : "bbdmi_live_granulator~.maxpat",
					"numinlets" : 2,
					"numoutlets" : 3,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "multichannelsignal", "", "" ],
					"patching_rect" : [ 739.0, 161.835540967701036, 199.0, 564.0 ],
					"varname" : "bbdmi_live.granulator~[3]",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-12",
					"lastchannelcount" : 0,
					"maxclass" : "live.gain~",
					"numinlets" : 2,
					"numoutlets" : 5,
					"outlettype" : [ "signal", "signal", "", "float", "list" ],
					"parameter_enable" : 1,
					"patching_rect" : [ 335.0, 813.333328664302826, 47.0, 136.0 ],
					"saved_attribute_attributes" : 					{
						"valueof" : 						{
							"parameter_longname" : "live.gain~[4]",
							"parameter_mmax" : 6.0,
							"parameter_mmin" : -70.0,
							"parameter_shortname" : "live.gain~",
							"parameter_type" : 0,
							"parameter_unitstyle" : 4
						}

					}
,
					"varname" : "live.gain~[4]"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-14",
					"lastchannelcount" : 0,
					"maxclass" : "live.gain~",
					"numinlets" : 2,
					"numoutlets" : 5,
					"outlettype" : [ "signal", "signal", "", "float", "list" ],
					"parameter_enable" : 1,
					"patching_rect" : [ 202.102598944678903, 148.333328664302826, 47.0, 136.0 ],
					"saved_attribute_attributes" : 					{
						"valueof" : 						{
							"parameter_longname" : "live.gain~[5]",
							"parameter_mmax" : 6.0,
							"parameter_mmin" : -70.0,
							"parameter_shortname" : "live.gain~",
							"parameter_type" : 0,
							"parameter_unitstyle" : 4
						}

					}
,
					"varname" : "live.gain~[5]"
				}

			}
, 			{
				"box" : 				{
					"color" : [ 1.0, 0.694117647058824, 0.0, 1.0 ],
					"fontface" : 1,
					"fontsize" : 16.0,
					"id" : "obj-16",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patcher" : 					{
						"fileversion" : 1,
						"appversion" : 						{
							"major" : 8,
							"minor" : 5,
							"revision" : 6,
							"architecture" : "x64",
							"modernui" : 1
						}
,
						"classnamespace" : "box",
						"rect" : [ 697.0, 840.0, 1284.0, 804.0 ],
						"bglocked" : 0,
						"openinpresentation" : 0,
						"default_fontsize" : 12.0,
						"default_fontface" : 0,
						"default_fontname" : "Arial",
						"gridonopen" : 1,
						"gridsize" : [ 15.0, 15.0 ],
						"gridsnaponopen" : 1,
						"objectsnaponopen" : 1,
						"statusbarvisible" : 2,
						"toolbarvisible" : 1,
						"lefttoolbarpinned" : 0,
						"toptoolbarpinned" : 0,
						"righttoolbarpinned" : 0,
						"bottomtoolbarpinned" : 0,
						"toolbars_unpinned_last_save" : 0,
						"tallnewobj" : 0,
						"boxanimatetime" : 200,
						"enablehscroll" : 1,
						"enablevscroll" : 1,
						"devicewidth" : 0.0,
						"description" : "",
						"digest" : "",
						"tags" : "",
						"style" : "",
						"subpatcher_template" : "",
						"assistshowspatchername" : 0,
						"visible" : 1,
						"boxes" : [ 							{
								"box" : 								{
									"id" : "obj-98",
									"linecount" : 21,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 684.399651885032654, 54.078430593013763, 174.894474148750305, 303.0 ],
									"text" : ";\rbassclar transpout -1.567927;\rbassclar feedback 0.217766;\rbassclar gain 1.;\rbassclar variability 0.420905;\rbassclar transpgrain -0.30263;\rbassclar modfreqmod 0.451733;\rbassclar modmorph 0.449987;\rbassclar modfreq 2022.356999;\rbassclar modfactor 0.644277;\rbassclar spacing 69;\rbassclar grainoffset 599;\rbassclar grainsize 35;\rbassclar maxdelay 5433.37;\rbassclar lpffreq 14539.336625;\rbassclar hpffreq 1311.302966;\rbassclar grainenvmorph 0.890082;\rbassclar indexdistr 4;\r"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-97",
									"linecount" : 22,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 872.634957671165466, 611.668891864536363, 174.894474148750305, 317.0 ],
									"text" : ";\rbassclar transpout 13.740772;\rbassclar feedback 0.813036;\rbassclar gain 1.;\rbassclar variability 0.519368;\rbassclar transpgrain -17.093793;\rbassclar modfreqmod 0.079739;\rbassclar modmorph 2.652866;\rbassclar modfreq 391.3611;\rbassclar modfactor 0.457532;\rbassclar spacing 79;\rbassclar grainoffset 44;\rbassclar grainsize 36;\rbassclar maxdelay 636.551476;\rbassclar lpffreq 5734.015345;\rbassclar hpffreq 651.065726;\rbassclar grainenvmorph 0.480679;\rbassclar indexdistr 14;\r"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-96",
									"linecount" : 18,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 753.027107119560242, 545.254931628704071, 174.894474148750305, 263.0 ],
									"text" : ";\rbassclar transpout 0.;\rbassclar feedback 0.7;\rbassclar gain 1.;\rbassclar variability 0.8;\rbassclar transpgrain 0.2;\rbassclar modfreqmod 0.5;\rbassclar modmorph 0.1;\rbassclar modfreq 10.;\rbassclar modfactor 0.5;\rbassclar spacing 187;\rbassclar grainoffset 10;\rbassclar grainsize 233;\rbassclar maxdelay 2000.;\rbassclar lpffreq 12000.;\rbassclar hpffreq 20.;\rbassclar grainenvmorph 0.5;\rbassclar indexdistr 0;\r"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-95",
									"linecount" : 18,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 545.183956980705261, 589.372581422328949, 174.894474148750305, 263.0 ],
									"text" : ";\rbassclar transpout 2.;\rbassclar feedback 0.7;\rbassclar gain 1.;\rbassclar variability 0.8;\rbassclar transpgrain 0.2;\rbassclar modfreqmod 0.5;\rbassclar modmorph 0.1;\rbassclar modfreq 10.;\rbassclar modfactor 0.5;\rbassclar spacing 130;\rbassclar grainoffset 200;\rbassclar grainsize 48;\rbassclar maxdelay 500.;\rbassclar lpffreq 12000.;\rbassclar hpffreq 20.;\rbassclar grainenvmorph 0.5;\rbassclar indexdistr 0;\r"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-64",
									"linecount" : 18,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 709.105525851249695, 404.333328664302826, 174.894474148750305, 263.0 ],
									"text" : ";\rbassclar transpout -2.;\rbassclar feedback 0.8;\rbassclar gain 1.;\rbassclar variability 0.3;\rbassclar transpgrain 1.;\rbassclar modfreqmod 0.5;\rbassclar modmorph 0.1;\rbassclar modfreq 10.;\rbassclar modfactor 0.5;\rbassclar spacing 87;\rbassclar grainoffset 10;\rbassclar grainsize 42;\rbassclar maxdelay 100.;\rbassclar lpffreq 2000.;\rbassclar hpffreq 20.;\rbassclar grainenvmorph 0.5;\rbassclar indexdistr 0;\r"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-62",
									"linecount" : 18,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 730.105525851249695, 244.333328664302826, 174.894474148750305, 263.0 ],
									"text" : ";\rbassclar transpout 1.;\rbassclar feedback 0.4;\rbassclar gain 1.;\rbassclar variability 0.8;\rbassclar transpgrain 0.2;\rbassclar modfreqmod 0.5;\rbassclar modmorph 0.1;\rbassclar modfreq 10.;\rbassclar modfactor 0.5;\rbassclar spacing 80;\rbassclar grainoffset 200;\rbassclar grainsize 36;\rbassclar maxdelay 100.;\rbassclar lpffreq 6000.;\rbassclar hpffreq 20.;\rbassclar grainenvmorph 0.5;\rbassclar indexdistr 0;\r"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-61",
									"linecount" : 18,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 550.0, 254.333328664302826, 174.894474148750305, 263.0 ],
									"text" : ";\rbassclar transpout -20.;\rbassclar feedback 0.5;\rbassclar gain 1.;\rbassclar variability 0.3;\rbassclar transpgrain -5.;\rbassclar modfreqmod 0.5;\rbassclar modmorph 0.;\rbassclar modfreq 500.;\rbassclar modfactor 0.;\rbassclar spacing 40;\rbassclar grainoffset 20;\rbassclar grainsize 100;\rbassclar maxdelay 2000.;\rbassclar lpffreq 20000.;\rbassclar hpffreq 20.;\rbassclar grainenvmorph 0.5;\rbassclar indexdistr 0;\r"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-29",
									"linecount" : 21,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 534.105525851249695, -23.666671335697174, 174.894474148750305, 303.0 ],
									"text" : ";\rbassclar transpout -1.567927;\rbassclar feedback 0.217766;\rbassclar gain 1.;\rbassclar variability 0.420905;\rbassclar transpgrain -0.30263;\rbassclar modfreqmod 0.451733;\rbassclar modmorph 0.449987;\rbassclar modfreq 2022.356999;\rbassclar modfactor 0.644277;\rbassclar spacing 69;\rbassclar grainoffset 599;\rbassclar grainsize 35;\rbassclar maxdelay 5433.37;\rbassclar lpffreq 14539.336625;\rbassclar hpffreq 1311.302966;\rbassclar grainenvmorph 0.890082;\rbassclar indexdistr 4;\r"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-14",
									"maxclass" : "newobj",
									"numinlets" : 0,
									"numoutlets" : 0,
									"patcher" : 									{
										"fileversion" : 1,
										"appversion" : 										{
											"major" : 8,
											"minor" : 5,
											"revision" : 6,
											"architecture" : "x64",
											"modernui" : 1
										}
,
										"classnamespace" : "box",
										"rect" : [ 59.0, 118.0, 868.0, 655.0 ],
										"bglocked" : 0,
										"openinpresentation" : 0,
										"default_fontsize" : 12.0,
										"default_fontface" : 0,
										"default_fontname" : "Arial",
										"gridonopen" : 1,
										"gridsize" : [ 15.0, 15.0 ],
										"gridsnaponopen" : 1,
										"objectsnaponopen" : 1,
										"statusbarvisible" : 2,
										"toolbarvisible" : 1,
										"lefttoolbarpinned" : 0,
										"toptoolbarpinned" : 0,
										"righttoolbarpinned" : 0,
										"bottomtoolbarpinned" : 0,
										"toolbars_unpinned_last_save" : 0,
										"tallnewobj" : 0,
										"boxanimatetime" : 200,
										"enablehscroll" : 1,
										"enablevscroll" : 1,
										"devicewidth" : 0.0,
										"description" : "",
										"digest" : "",
										"tags" : "",
										"style" : "",
										"subpatcher_template" : "",
										"assistshowspatchername" : 0,
										"boxes" : [ 											{
												"box" : 												{
													"id" : "obj-13",
													"linecount" : 18,
													"maxclass" : "message",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 530.105525851249695, 389.643398344516868, 226.894474148750305, 263.0 ],
													"text" : ";\rinstr transpout 0.;\rinstr feedback 0.2;\rinstr gain 1.;\rinstr variability 0.3;\rinstr transpgrain 0.;\rinstr modfreqmod 0.5;\rinstr modmorph 0.;\rinstr modfreq 500.;\rinstr modfactor 0.;\rinstr spacing 80;\rinstr grainoffset 40;\rinstr grainsize 40;\rinstr maxdelay 2000.;\rinstr lpffreq 20000.;\rinstr hpffreq 20.;\rinstr grainenvmorph 1.;\rinstr indexdistr 0;\r"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-11",
													"maxclass" : "comment",
													"numinlets" : 1,
													"numoutlets" : 0,
													"patching_rect" : [ 793.0, 371.0, 150.0, 20.0 ],
													"text" : "voice 11 feb"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-124",
													"linecount" : 18,
													"maxclass" : "message",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 796.105525851249695, 397.643398344516868, 226.894474148750305, 263.0 ],
													"text" : ";\rinstr transpout 10.;\rinstr feedback 0.991;\rinstr gain 1.;\rinstr variability 0.44;\rinstr transpgrain 5.;\rinstr modfreqmod 0.9;\rinstr modmorph 0.098;\rinstr modfreq 199.;\rinstr modfactor 0.41;\rinstr spacing 190;\rinstr grainoffset 0;\rinstr grainsize 211;\rinstr maxdelay 2000.;\rinstr lpffreq 20000.;\rinstr hpffreq 20.;\rinstr grainenvmorph 0.445;\rinstr indexdistr 1;\r"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-10",
													"maxclass" : "comment",
													"numinlets" : 1,
													"numoutlets" : 0,
													"patching_rect" : [ 294.0, 661.0, 150.0, 20.0 ],
													"text" : "piano transpose 1"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-9",
													"linecount" : 18,
													"maxclass" : "message",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 294.0, 687.0, 226.894474148750305, 263.0 ],
													"text" : ";\rinstr transpout 0.;\rinstr feedback 0.;\rinstr gain 1.;\rinstr variability 0.;\rinstr transpgrain 1.;\rinstr modfreqmod 0.;\rinstr modmorph 0.;\rinstr modfreq 0.;\rinstr modfactor 0.;\rinstr spacing 1;\rinstr grainoffset 0;\rinstr grainsize 250;\rinstr maxdelay 500.;\rinstr lpffreq 20000.;\rinstr hpffreq 20.;\rinstr grainenvmorph 1.;\rinstr indexdistr 0;\r"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-8",
													"maxclass" : "comment",
													"numinlets" : 1,
													"numoutlets" : 0,
													"patching_rect" : [ 544.0, 661.0, 150.0, 20.0 ],
													"text" : "piano transpose 2"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-7",
													"linecount" : 18,
													"maxclass" : "message",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 548.0, 683.0, 226.894474148750305, 263.0 ],
													"text" : ";\rinstr transpout 6.;\rinstr feedback 0.7;\rinstr gain 1.;\rinstr variability 0.;\rinstr transpgrain 0.;\rinstr modfreqmod 0.;\rinstr modmorph 0.;\rinstr modfreq 0.;\rinstr modfactor 0.;\rinstr spacing 1;\rinstr grainoffset 0;\rinstr grainsize 250;\rinstr maxdelay 500.;\rinstr lpffreq 20000.;\rinstr hpffreq 20.;\rinstr grainenvmorph 1.;\rinstr indexdistr 0;\r"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-5",
													"linecount" : 18,
													"maxclass" : "message",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 290.0, 386.0, 226.894474148750305, 263.0 ],
													"text" : ";\rinstr transpout 0.;\rinstr feedback 0.;\rinstr gain 1.;\rinstr variability 0.5;\rinstr transpgrain 0.;\rinstr modfreqmod 0.;\rinstr modmorph 0.;\rinstr modfreq 0.;\rinstr modfactor 0.;\rinstr spacing 1;\rinstr grainoffset 50;\rinstr grainsize 250;\rinstr maxdelay 500.;\rinstr lpffreq 20000.;\rinstr hpffreq 20.;\rinstr grainenvmorph 1.;\rinstr indexdistr 0;\r"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-2",
													"maxclass" : "comment",
													"numinlets" : 1,
													"numoutlets" : 0,
													"patching_rect" : [ 290.0, 358.0, 150.0, 20.0 ],
													"text" : "clarinet"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-1",
													"maxclass" : "comment",
													"numinlets" : 1,
													"numoutlets" : 0,
													"patching_rect" : [ 290.0, 15.0, 150.0, 20.0 ],
													"text" : "viola"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-34",
													"linecount" : 18,
													"maxclass" : "message",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 518.447237074375153, 45.0, 226.894474148750305, 263.0 ],
													"text" : ";\rinstr transpout 0.;\rinstr feedback 0.5;\rinstr gain 1.;\rinstr variability 0.;\rinstr transpgrain 0.;\rinstr modfreqmod 0.;\rinstr modmorph 0.;\rinstr modfreq 2.9;\rinstr modfactor 0.7;\rinstr spacing 17;\rinstr grainoffset 40;\rinstr grainsize 50;\rinstr maxdelay 300.;\rinstr lpffreq 20000.;\rinstr hpffreq 20.;\rinstr grainenvmorph 1.;\rinstr indexdistr 0;\r"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-29",
													"linecount" : 18,
													"maxclass" : "message",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 279.0, 45.0, 226.894474148750305, 263.0 ],
													"text" : ";\rinstr transpout 0.;\rinstr feedback 0.;\rinstr gain 1.;\rinstr variability 0.;\rinstr transpgrain 0.;\rinstr modfreqmod 0.5;\rinstr modmorph 0.;\rinstr modfreq 1000.;\rinstr modfactor 0.;\rinstr spacing 16;\rinstr grainoffset 0;\rinstr grainsize 219;\rinstr maxdelay 2000.;\rinstr lpffreq 20000.;\rinstr hpffreq 20.;\rinstr grainenvmorph 1.;\rinstr indexdistr 0;\r"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-12",
													"linecount" : 18,
													"maxclass" : "message",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 8.0, 336.0, 226.894474148750305, 263.0 ],
													"text" : ";\rinstr transpout 0.;\rinstr feedback 0.2;\rinstr gain 1.;\rinstr variability 0.3;\rinstr transpgrain 0.;\rinstr modfreqmod 0.5;\rinstr modmorph 0.;\rinstr modfreq 500.;\rinstr modfactor 0.;\rinstr spacing 80;\rinstr grainoffset 40;\rinstr grainsize 40;\rinstr maxdelay 2000.;\rinstr lpffreq 20000.;\rinstr hpffreq 20.;\rinstr grainenvmorph 1.;\rinstr indexdistr 0;\r"
												}

											}
 ],
										"lines" : [  ]
									}
,
									"patching_rect" : [ 71.0, 37.0, 35.0, 22.0 ],
									"saved_object_attributes" : 									{
										"description" : "",
										"digest" : "",
										"globalpatchername" : "",
										"tags" : ""
									}
,
									"text" : "p old"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-6",
									"linecount" : 18,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 264.0, 104.0, 153.0, 263.0 ],
									"text" : ";\rbassclar transpout 0.;\rbassclar feedback 0.;\rbassclar gain 1.;\rbassclar variability 0.;\rbassclar transpgrain 0.;\rbassclar modfreqmod 0.;\rbassclar modmorph 0.;\rbassclar modfreq 0.;\rbassclar modfactor 0.;\rbassclar spacing 1;\rbassclar grainoffset 0;\rbassclar grainsize 250;\rbassclar maxdelay 500.;\rbassclar lpffreq 20000.;\rbassclar hpffreq 20.;\rbassclar grainenvmorph 1.;\rbassclar indexdistr 0;\r"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-4",
									"linecount" : 18,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 6.0, 104.0, 226.894474148750305, 263.0 ],
									"text" : ";\rbassclar transpout -1.567927;\rbassclar feedback 0.217766;\rbassclar gain 1.;\rbassclar variability 0.420905;\rbassclar transpgrain -0.30263;\rbassclar modfreqmod 0.451733;\rbassclar modmorph 0.449987;\rbassclar modfreq 2022.356999;\rbassclar modfactor 0.644277;\rbassclar spacing 69;\rbassclar grainoffset 599;\rbassclar grainsize 35;\rbassclar maxdelay 5433.37;\rbassclar lpffreq 14539.336625;\rbassclar hpffreq 1311.302966;\rbassclar grainenvmorph 0.890082;\rbassclar indexdistr 4;\r"
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-3",
									"index" : 1,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 213.894474148750305, 41.0, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-20",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 270.0, 74.0, 150.0, 20.0 ],
									"text" : "reset"
								}

							}
 ],
						"lines" : [ 							{
								"patchline" : 								{
									"destination" : [ "obj-4", 1 ],
									"source" : [ "obj-3", 0 ]
								}

							}
 ]
					}
,
					"patching_rect" : [ 515.0, 748.33332622051239, 80.0, 26.0 ],
					"saved_object_attributes" : 					{
						"description" : "",
						"digest" : "",
						"globalpatchername" : "",
						"tags" : ""
					}
,
					"text" : "p presets",
					"textcolor" : [ 0.0, 0.996078431372549, 0.098039215686275, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-18",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "signal" ],
					"patching_rect" : [ 335.0, 777.0, 74.0, 22.0 ],
					"text" : "mc.unpack~"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-20",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "multichannelsignal" ],
					"patching_rect" : [ 335.0, 748.33332622051239, 92.0, 22.0 ],
					"text" : "mc.mixdown~ 2"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-23",
					"maxclass" : "newobj",
					"numinlets" : 4,
					"numoutlets" : 1,
					"outlettype" : [ "multichannelsignal" ],
					"patching_rect" : [ 335.0, 135.333328664302826, 70.0, 22.0 ],
					"text" : "mc.pack~ 4"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-28",
					"maxclass" : "meter~",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "float" ],
					"patching_rect" : [ 431.0, 748.33332622051239, 59.0, 62.0 ]
				}

			}
, 			{
				"box" : 				{
					"args" : [ "bassclar", "@Channels", 7 ],
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-30",
					"lockeddragscroll" : 0,
					"lockedsize" : 0,
					"maxclass" : "bpatcher",
					"name" : "bbdmi_live_granulator~.maxpat",
					"numinlets" : 2,
					"numoutlets" : 3,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "multichannelsignal", "", "" ],
					"patching_rect" : [ 335.0, 161.835540967701036, 199.0, 564.0 ],
					"varname" : "bbdmi_live.granulator~[2]",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-3",
					"lastchannelcount" : 0,
					"maxclass" : "live.gain~",
					"numinlets" : 2,
					"numoutlets" : 5,
					"outlettype" : [ "signal", "signal", "", "float", "list" ],
					"parameter_enable" : 1,
					"patching_rect" : [ 261.102598944678903, 148.333328664302826, 47.0, 136.0 ],
					"saved_attribute_attributes" : 					{
						"valueof" : 						{
							"parameter_longname" : "live.gain~[2]",
							"parameter_mmax" : 6.0,
							"parameter_mmin" : -70.0,
							"parameter_shortname" : "live.gain~",
							"parameter_type" : 0,
							"parameter_unitstyle" : 4
						}

					}
,
					"varname" : "live.gain~[2]"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-32",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 0,
					"patching_rect" : [ 593.102598944678903, 992.671081935402071, 55.0, 22.0 ],
					"text" : "dac~ 1 2"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-1",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 4,
					"outlettype" : [ "signal", "signal", "signal", "signal" ],
					"patching_rect" : [ 213.0, 10.333328664302826, 554.0, 22.0 ],
					"text" : "adc~ 1 2 3 4"
				}

			}
, 			{
				"box" : 				{
					"basictuning" : 440,
					"clipheight" : 31.162246728900755,
					"data" : 					{
						"clips" : [ 							{
								"absolutepath" : "bassclar-01.wav",
								"filename" : "bassclar-01.wav",
								"filekind" : "audiofile",
								"id" : "u532004044",
								"selection" : [ 0.314935064935065, 0.454545454545455 ],
								"loop" : 1,
								"content_state" : 								{
									"loop" : 1
								}

							}
 ]
					}
,
					"followglobaltempo" : 0,
					"formantcorrection" : 0,
					"id" : "obj-5",
					"maxclass" : "playlist~",
					"mode" : "basic",
					"numinlets" : 1,
					"numoutlets" : 5,
					"originallength" : [ 0.0, "ticks" ],
					"originaltempo" : 120.0,
					"outlettype" : [ "signal", "signal", "signal", "", "dictionary" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 40.509956024587154, 84.835540967701036, 181.490043975412846, 32.162246728900755 ],
					"pitchcorrection" : 0,
					"quality" : "basic",
					"timestretch" : [ 0 ]
				}

			}
 ],
		"lines" : [ 			{
				"patchline" : 				{
					"destination" : [ "obj-14", 0 ],
					"order" : 1,
					"source" : [ "obj-1", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-14", 0 ],
					"order" : 1,
					"source" : [ "obj-1", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-15", 1 ],
					"order" : 0,
					"source" : [ "obj-1", 3 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-15", 0 ],
					"order" : 0,
					"source" : [ "obj-1", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-3", 0 ],
					"order" : 0,
					"source" : [ "obj-1", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-3", 0 ],
					"order" : 0,
					"source" : [ "obj-1", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-33", 1 ],
					"order" : 1,
					"source" : [ "obj-1", 3 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-33", 0 ],
					"order" : 1,
					"source" : [ "obj-1", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-32", 1 ],
					"source" : [ "obj-12", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-32", 0 ],
					"source" : [ "obj-12", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-32", 1 ],
					"order" : 0,
					"source" : [ "obj-14", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-32", 0 ],
					"order" : 1,
					"source" : [ "obj-14", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-39", 3 ],
					"order" : 0,
					"source" : [ "obj-15", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-39", 1 ],
					"order" : 1,
					"source" : [ "obj-15", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-39", 2 ],
					"order" : 0,
					"source" : [ "obj-15", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-39", 0 ],
					"order" : 1,
					"source" : [ "obj-15", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-12", 1 ],
					"source" : [ "obj-18", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-12", 0 ],
					"source" : [ "obj-18", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-18", 0 ],
					"source" : [ "obj-20", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-30", 0 ],
					"source" : [ "obj-23", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-23", 3 ],
					"order" : 0,
					"source" : [ "obj-3", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-23", 2 ],
					"order" : 1,
					"source" : [ "obj-3", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-23", 1 ],
					"order" : 2,
					"source" : [ "obj-3", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-23", 0 ],
					"order" : 3,
					"source" : [ "obj-3", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-16", 0 ],
					"source" : [ "obj-30", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-20", 0 ],
					"order" : 1,
					"source" : [ "obj-30", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-28", 0 ],
					"order" : 0,
					"source" : [ "obj-30", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-32", 1 ],
					"source" : [ "obj-31", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-32", 0 ],
					"source" : [ "obj-31", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-32", 1 ],
					"source" : [ "obj-33", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-32", 0 ],
					"source" : [ "obj-33", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-31", 1 ],
					"source" : [ "obj-35", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-31", 0 ],
					"source" : [ "obj-35", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-35", 0 ],
					"source" : [ "obj-38", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-44", 0 ],
					"source" : [ "obj-39", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-15", 1 ],
					"order" : 0,
					"source" : [ "obj-40", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-15", 0 ],
					"order" : 1,
					"source" : [ "obj-40", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-33", 1 ],
					"order" : 2,
					"source" : [ "obj-40", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-33", 0 ],
					"order" : 3,
					"source" : [ "obj-40", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-34", 0 ],
					"source" : [ "obj-44", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-38", 0 ],
					"order" : 1,
					"source" : [ "obj-44", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-41", 0 ],
					"order" : 0,
					"source" : [ "obj-44", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-14", 0 ],
					"source" : [ "obj-48", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-3", 0 ],
					"source" : [ "obj-48", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-48", 0 ],
					"source" : [ "obj-49", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-14", 1 ],
					"order" : 1,
					"source" : [ "obj-5", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-14", 0 ],
					"order" : 1,
					"source" : [ "obj-5", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-3", 0 ],
					"order" : 0,
					"source" : [ "obj-5", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-3", 0 ],
					"order" : 0,
					"source" : [ "obj-5", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-15", 0 ],
					"source" : [ "obj-50", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-33", 0 ],
					"source" : [ "obj-50", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-50", 0 ],
					"source" : [ "obj-51", 0 ]
				}

			}
 ],
		"parameters" : 		{
			"obj-12" : [ "live.gain~[4]", "live.gain~", 0 ],
			"obj-14" : [ "live.gain~[5]", "live.gain~", 0 ],
			"obj-15" : [ "live.gain~[8]", "live.gain~", 0 ],
			"obj-24::obj-13::obj-107::obj-33" : [ "tab[80]", "tab[1]", 0 ],
			"obj-24::obj-13::obj-123::obj-33" : [ "tab[78]", "tab[1]", 0 ],
			"obj-24::obj-13::obj-36::obj-33" : [ "tab[82]", "tab[1]", 0 ],
			"obj-24::obj-13::obj-40::obj-33" : [ "tab[83]", "tab[1]", 0 ],
			"obj-24::obj-13::obj-41::obj-33" : [ "tab[84]", "tab[1]", 0 ],
			"obj-24::obj-13::obj-44::obj-33" : [ "tab[87]", "tab[1]", 0 ],
			"obj-24::obj-13::obj-45::obj-33" : [ "tab[88]", "tab[1]", 0 ],
			"obj-24::obj-13::obj-46::obj-33" : [ "tab[89]", "tab[1]", 0 ],
			"obj-24::obj-13::obj-47::obj-33" : [ "tab[90]", "tab[1]", 0 ],
			"obj-24::obj-13::obj-48::obj-33" : [ "tab[91]", "tab[1]", 0 ],
			"obj-24::obj-13::obj-49::obj-33" : [ "tab[92]", "tab[1]", 0 ],
			"obj-24::obj-13::obj-50::obj-33" : [ "tab[93]", "tab[1]", 0 ],
			"obj-24::obj-13::obj-6::obj-33" : [ "tab[86]", "tab[1]", 0 ],
			"obj-24::obj-13::obj-74::obj-33" : [ "tab[79]", "tab[1]", 0 ],
			"obj-24::obj-13::obj-8::obj-33" : [ "tab[85]", "tab[1]", 0 ],
			"obj-24::obj-13::obj-9::obj-33" : [ "tab[81]", "tab[1]", 0 ],
			"obj-24::obj-15" : [ "live.gain~[1]", "leloup~", 0 ],
			"obj-24::obj-175::obj-107::obj-33" : [ "tab[64]", "tab[1]", 0 ],
			"obj-24::obj-175::obj-123::obj-33" : [ "tab[125]", "tab[1]", 0 ],
			"obj-24::obj-175::obj-36::obj-33" : [ "tab[66]", "tab[1]", 0 ],
			"obj-24::obj-175::obj-40::obj-33" : [ "tab[67]", "tab[1]", 0 ],
			"obj-24::obj-175::obj-41::obj-33" : [ "tab[68]", "tab[1]", 0 ],
			"obj-24::obj-175::obj-44::obj-33" : [ "tab[71]", "tab[1]", 0 ],
			"obj-24::obj-175::obj-45::obj-33" : [ "tab[72]", "tab[1]", 0 ],
			"obj-24::obj-175::obj-46::obj-33" : [ "tab[73]", "tab[1]", 0 ],
			"obj-24::obj-175::obj-47::obj-33" : [ "tab[74]", "tab[1]", 0 ],
			"obj-24::obj-175::obj-48::obj-33" : [ "tab[75]", "tab[1]", 0 ],
			"obj-24::obj-175::obj-49::obj-33" : [ "tab[76]", "tab[1]", 0 ],
			"obj-24::obj-175::obj-50::obj-33" : [ "tab[77]", "tab[1]", 0 ],
			"obj-24::obj-175::obj-6::obj-33" : [ "tab[70]", "tab[1]", 0 ],
			"obj-24::obj-175::obj-74::obj-33" : [ "tab[63]", "tab[1]", 0 ],
			"obj-24::obj-175::obj-8::obj-33" : [ "tab[69]", "tab[1]", 0 ],
			"obj-24::obj-175::obj-9::obj-33" : [ "tab[65]", "tab[1]", 0 ],
			"obj-24::obj-20::obj-122" : [ "number[49]", "number[1]", 0 ],
			"obj-24::obj-20::obj-16" : [ "number[48]", "number[1]", 0 ],
			"obj-24::obj-24::obj-107::obj-33" : [ "tab[18]", "tab[1]", 0 ],
			"obj-24::obj-24::obj-123::obj-33" : [ "tab[16]", "tab[1]", 0 ],
			"obj-24::obj-24::obj-34::obj-33" : [ "tab[19]", "tab[1]", 0 ],
			"obj-24::obj-24::obj-36::obj-33" : [ "tab[20]", "tab[1]", 0 ],
			"obj-24::obj-24::obj-40::obj-33" : [ "tab[21]", "tab[1]", 0 ],
			"obj-24::obj-24::obj-41::obj-33" : [ "tab[22]", "tab[1]", 0 ],
			"obj-24::obj-24::obj-42::obj-33" : [ "tab[23]", "tab[1]", 0 ],
			"obj-24::obj-24::obj-43::obj-33" : [ "tab[24]", "tab[1]", 0 ],
			"obj-24::obj-24::obj-44::obj-33" : [ "tab[25]", "tab[1]", 0 ],
			"obj-24::obj-24::obj-45::obj-33" : [ "tab[26]", "tab[1]", 0 ],
			"obj-24::obj-24::obj-46::obj-33" : [ "tab[27]", "tab[1]", 0 ],
			"obj-24::obj-24::obj-47::obj-33" : [ "tab[28]", "tab[1]", 0 ],
			"obj-24::obj-24::obj-48::obj-33" : [ "tab[29]", "tab[1]", 0 ],
			"obj-24::obj-24::obj-49::obj-33" : [ "tab[30]", "tab[1]", 0 ],
			"obj-24::obj-24::obj-50::obj-33" : [ "tab[31]", "tab[1]", 0 ],
			"obj-24::obj-24::obj-74::obj-33" : [ "tab[17]", "tab[1]", 0 ],
			"obj-24::obj-2::obj-122" : [ "number[2]", "number[1]", 0 ],
			"obj-24::obj-2::obj-16" : [ "number[1]", "number[1]", 0 ],
			"obj-24::obj-31::obj-107::obj-33" : [ "tab[33]", "tab[1]", 0 ],
			"obj-24::obj-31::obj-123::obj-33" : [ "tab[116]", "tab[1]", 0 ],
			"obj-24::obj-31::obj-34::obj-33" : [ "tab[34]", "tab[1]", 0 ],
			"obj-24::obj-31::obj-36::obj-33" : [ "tab[35]", "tab[1]", 0 ],
			"obj-24::obj-31::obj-40::obj-33" : [ "tab[36]", "tab[1]", 0 ],
			"obj-24::obj-31::obj-41::obj-33" : [ "tab[37]", "tab[1]", 0 ],
			"obj-24::obj-31::obj-42::obj-33" : [ "tab[38]", "tab[1]", 0 ],
			"obj-24::obj-31::obj-43::obj-33" : [ "tab[39]", "tab[1]", 0 ],
			"obj-24::obj-31::obj-44::obj-33" : [ "tab[40]", "tab[1]", 0 ],
			"obj-24::obj-31::obj-45::obj-33" : [ "tab[41]", "tab[1]", 0 ],
			"obj-24::obj-31::obj-46::obj-33" : [ "tab[42]", "tab[1]", 0 ],
			"obj-24::obj-31::obj-47::obj-33" : [ "tab[43]", "tab[1]", 0 ],
			"obj-24::obj-31::obj-48::obj-33" : [ "tab[44]", "tab[1]", 0 ],
			"obj-24::obj-31::obj-49::obj-33" : [ "tab[45]", "tab[1]", 0 ],
			"obj-24::obj-31::obj-50::obj-33" : [ "tab[46]", "tab[1]", 0 ],
			"obj-24::obj-31::obj-74::obj-33" : [ "tab[32]", "tab[1]", 0 ],
			"obj-24::obj-34::obj-107::obj-33" : [ "tab[49]", "tab[1]", 0 ],
			"obj-24::obj-34::obj-123::obj-33" : [ "tab[47]", "tab[1]", 0 ],
			"obj-24::obj-34::obj-34::obj-33" : [ "tab[50]", "tab[1]", 0 ],
			"obj-24::obj-34::obj-36::obj-33" : [ "tab[51]", "tab[1]", 0 ],
			"obj-24::obj-34::obj-40::obj-33" : [ "tab[52]", "tab[1]", 0 ],
			"obj-24::obj-34::obj-41::obj-33" : [ "tab[53]", "tab[1]", 0 ],
			"obj-24::obj-34::obj-42::obj-33" : [ "tab[54]", "tab[1]", 0 ],
			"obj-24::obj-34::obj-43::obj-33" : [ "tab[55]", "tab[1]", 0 ],
			"obj-24::obj-34::obj-44::obj-33" : [ "tab[56]", "tab[1]", 0 ],
			"obj-24::obj-34::obj-45::obj-33" : [ "tab[57]", "tab[1]", 0 ],
			"obj-24::obj-34::obj-46::obj-33" : [ "tab[58]", "tab[1]", 0 ],
			"obj-24::obj-34::obj-47::obj-33" : [ "tab[59]", "tab[1]", 0 ],
			"obj-24::obj-34::obj-48::obj-33" : [ "tab[60]", "tab[1]", 0 ],
			"obj-24::obj-34::obj-49::obj-33" : [ "tab[61]", "tab[1]", 0 ],
			"obj-24::obj-34::obj-50::obj-33" : [ "tab[62]", "tab[1]", 0 ],
			"obj-24::obj-34::obj-74::obj-33" : [ "tab[48]", "tab[1]", 0 ],
			"obj-24::obj-39" : [ "live.gain~[9]", "live.gain~[2]", 0 ],
			"obj-24::obj-67" : [ "live.gain~", "leloup~", 0 ],
			"obj-24::obj-93::obj-107::obj-33" : [ "tab[2]", "tab[1]", 0 ],
			"obj-24::obj-93::obj-123::obj-33" : [ "tab[182]", "tab[1]", 0 ],
			"obj-24::obj-93::obj-34::obj-33" : [ "tab[3]", "tab[1]", 0 ],
			"obj-24::obj-93::obj-36::obj-33" : [ "tab[4]", "tab[1]", 0 ],
			"obj-24::obj-93::obj-40::obj-33" : [ "tab[5]", "tab[1]", 0 ],
			"obj-24::obj-93::obj-41::obj-33" : [ "tab[6]", "tab[1]", 0 ],
			"obj-24::obj-93::obj-42::obj-33" : [ "tab[7]", "tab[1]", 0 ],
			"obj-24::obj-93::obj-43::obj-33" : [ "tab[8]", "tab[1]", 0 ],
			"obj-24::obj-93::obj-44::obj-33" : [ "tab[9]", "tab[1]", 0 ],
			"obj-24::obj-93::obj-45::obj-33" : [ "tab[10]", "tab[1]", 0 ],
			"obj-24::obj-93::obj-46::obj-33" : [ "tab[11]", "tab[1]", 0 ],
			"obj-24::obj-93::obj-47::obj-33" : [ "tab[12]", "tab[1]", 0 ],
			"obj-24::obj-93::obj-48::obj-33" : [ "tab[13]", "tab[1]", 0 ],
			"obj-24::obj-93::obj-49::obj-33" : [ "tab[14]", "tab[1]", 0 ],
			"obj-24::obj-93::obj-50::obj-33" : [ "tab[15]", "tab[1]", 0 ],
			"obj-24::obj-93::obj-74::obj-33" : [ "tab[1]", "tab[1]", 0 ],
			"obj-3" : [ "live.gain~[2]", "live.gain~", 0 ],
			"obj-30::obj-101" : [ "number[23]", "number[7]", 0 ],
			"obj-30::obj-104" : [ "number[21]", "number[5]", 0 ],
			"obj-30::obj-109" : [ "number[30]", "number[6]", 0 ],
			"obj-30::obj-113" : [ "number[17]", "number[9]", 0 ],
			"obj-30::obj-118" : [ "number[27]", "number[2]", 0 ],
			"obj-30::obj-123" : [ "number[19]", "number[3]", 0 ],
			"obj-30::obj-28" : [ "number[25]", "number[12]", 0 ],
			"obj-30::obj-43" : [ "number[29]", "number[4]", 0 ],
			"obj-30::obj-44" : [ "number[28]", "number[13]", 0 ],
			"obj-30::obj-61" : [ "number[24]", "number[9]", 0 ],
			"obj-30::obj-68" : [ "number[20]", "number[4]", 0 ],
			"obj-30::obj-72" : [ "number[18]", "number[11]", 0 ],
			"obj-30::obj-77" : [ "number[16]", "number[10]", 0 ],
			"obj-30::obj-90" : [ "number[26]", "number", 0 ],
			"obj-30::obj-93" : [ "number[22]", "number[8]", 0 ],
			"obj-30::obj-97" : [ "number[31]", "number[1]", 0 ],
			"obj-31" : [ "live.gain~[6]", "live.gain~", 0 ],
			"obj-33" : [ "live.gain~[7]", "live.gain~", 0 ],
			"obj-44::obj-101" : [ "number[45]", "number[7]", 0 ],
			"obj-44::obj-104" : [ "number[46]", "number[5]", 0 ],
			"obj-44::obj-109" : [ "number[47]", "number[6]", 0 ],
			"obj-44::obj-113" : [ "number[44]", "number[9]", 0 ],
			"obj-44::obj-118" : [ "number[33]", "number[2]", 0 ],
			"obj-44::obj-123" : [ "number[42]", "number[3]", 0 ],
			"obj-44::obj-28" : [ "number[32]", "number[12]", 0 ],
			"obj-44::obj-43" : [ "number[39]", "number[4]", 0 ],
			"obj-44::obj-44" : [ "number[41]", "number[13]", 0 ],
			"obj-44::obj-61" : [ "number[38]", "number[9]", 0 ],
			"obj-44::obj-68" : [ "number[34]", "number[4]", 0 ],
			"obj-44::obj-72" : [ "number[35]", "number[11]", 0 ],
			"obj-44::obj-77" : [ "number[40]", "number[10]", 0 ],
			"obj-44::obj-90" : [ "number[43]", "number", 0 ],
			"obj-44::obj-93" : [ "number[37]", "number[8]", 0 ],
			"obj-44::obj-97" : [ "number[36]", "number[1]", 0 ],
			"parameterbanks" : 			{
				"0" : 				{
					"index" : 0,
					"name" : "",
					"parameters" : [ "-", "-", "-", "-", "-", "-", "-", "-" ]
				}

			}
,
			"parameter_overrides" : 			{
				"obj-24::obj-39" : 				{
					"parameter_longname" : "live.gain~[9]"
				}

			}
,
			"inherited_shortname" : 1
		}
,
		"parameter_map" : 		{
			"midi" : 			{
				"number" : 				{
					"srcname" : "2.ctrl.0.chan.midi",
					"min" : 0.0,
					"max" : 1.0,
					"flags" : 2
				}
,
				"number[1]" : 				{
					"srcname" : "3.ctrl.0.chan.midi",
					"min" : 0.0,
					"max" : 1.0,
					"flags" : 2
				}
,
				"number[2]" : 				{
					"srcname" : "4.ctrl.0.chan.midi",
					"min" : 20.0,
					"max" : 20000.0,
					"flags" : 2
				}
,
				"number[3]" : 				{
					"srcname" : "5.ctrl.0.chan.midi",
					"min" : 20.0,
					"max" : 20000.0,
					"flags" : 2
				}
,
				"number[4]" : 				{
					"srcname" : "6.ctrl.0.chan.midi",
					"min" : 0.0,
					"max" : 10000.0,
					"flags" : 2
				}
,
				"number[5]" : 				{
					"srcname" : "8.ctrl.0.chan.midi",
					"min" : 1.0,
					"max" : 2000.0,
					"flags" : 2
				}
,
				"number[6]" : 				{
					"srcname" : "9.ctrl.0.chan.midi",
					"min" : 0.0,
					"max" : 1000.0,
					"flags" : 2
				}
,
				"number[7]" : 				{
					"srcname" : "12.ctrl.0.chan.midi",
					"min" : 1.0,
					"max" : 10000.0,
					"flags" : 2
				}
,
				"number[8]" : 				{
					"srcname" : "13.ctrl.0.chan.midi",
					"min" : 0.0,
					"max" : 1.0,
					"flags" : 2
				}
,
				"number[9]" : 				{
					"srcname" : "14.ctrl.0.chan.midi",
					"min" : -48.0,
					"max" : 48.0,
					"flags" : 2
				}
,
				"number[10]" : 				{
					"srcname" : "15.ctrl.0.chan.midi",
					"min" : 0.0,
					"max" : 1.0,
					"flags" : 2
				}
,
				"number[11]" : 				{
					"srcname" : "16.ctrl.0.chan.midi",
					"min" : 0.0,
					"max" : 15000.0,
					"flags" : 2
				}
,
				"number[12]" : 				{
					"srcname" : "17.ctrl.0.chan.midi",
					"min" : 0.0,
					"max" : 3.0,
					"flags" : 2
				}
,
				"number[13]" : 				{
					"srcname" : "18.ctrl.0.chan.midi",
					"min" : -48.0,
					"max" : 48.0,
					"flags" : 2
				}

			}

		}
,
		"dependency_cache" : [ 			{
				"name" : "EAVI-2.maxpat",
				"bootpath" : "~/Documents/GitHub/bbdmi/events/2024-11-19_ArsMusica",
				"patcherrelativepath" : ".",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "RD-perf-a.json",
				"bootpath" : "~/Documents/GitHub/bbdmi/events/2023-11-24_Athenor/RD/preset",
				"patcherrelativepath" : "../2023-11-24_Athenor/RD/preset",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bassclar-01.wav",
				"bootpath" : "~/Documents/GitHub/bbdmi/events/2024-11-19_ArsMusica",
				"patcherrelativepath" : ".",
				"type" : "WAVE",
				"implicit" : 1
			}
, 			{
				"name" : "bbdmi.calibrate.maxpat",
				"bootpath" : "~/Documents/GitHub/bbdmi/max/control_processing/calibrate",
				"patcherrelativepath" : "../../max/control_processing/calibrate",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bbdmi.crosspatch.maxpat",
				"bootpath" : "~/Documents/GitHub/bbdmi/max/utilities/crosspatch",
				"patcherrelativepath" : "../../max/utilities/crosspatch",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bbdmi.eavi.maxpat",
				"bootpath" : "~/Documents/GitHub/bbdmi/max/input/eavi",
				"patcherrelativepath" : "../../max/input/eavi",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bbdmi.list2~.maxpat",
				"bootpath" : "~/Documents/GitHub/bbdmi/max/utilities/list2~",
				"patcherrelativepath" : "../../max/utilities/list2~",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bbdmi.multislider.maxpat",
				"bootpath" : "~/Documents/GitHub/bbdmi/max/utilities/multislider",
				"patcherrelativepath" : "../../max/utilities/multislider",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bbdmi.onepole~.maxpat",
				"bootpath" : "~/Documents/GitHub/bbdmi/max/utilities/onepole~",
				"patcherrelativepath" : "../../max/utilities/onepole~",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bbdmi.rms~.maxpat",
				"bootpath" : "~/Documents/GitHub/bbdmi/max/feature_extraction/rms~",
				"patcherrelativepath" : "../../max/feature_extraction/rms~",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bbdmi.scale.maxpat",
				"bootpath" : "~/Documents/GitHub/bbdmi/max/control_processing/scale",
				"patcherrelativepath" : "../../max/control_processing/scale",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bbdmi.send.maxpat",
				"bootpath" : "~/Documents/GitHub/bbdmi/max/utilities/send",
				"patcherrelativepath" : "../../max/utilities/send",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bbdmi.smooth.maxpat",
				"bootpath" : "~/Documents/GitHub/bbdmi/max/control_processing/smooth",
				"patcherrelativepath" : "../../max/control_processing/smooth",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bbdmi_live_granulator~.maxpat",
				"bootpath" : "~/Documents/GitHub/bbdmi/max/sound_synthesis/live_granulator~",
				"patcherrelativepath" : "../../max/sound_synthesis/live_granulator~",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bbdmi_multi_granulator11~.mxo",
				"type" : "iLaX"
			}
, 			{
				"name" : "name.js",
				"bootpath" : "~/Documents/GitHub/bbdmi/max/source/js",
				"patcherrelativepath" : "../../max/source/js",
				"type" : "TEXT",
				"implicit" : 1
			}
, 			{
				"name" : "p.calibrate.maxpat",
				"bootpath" : "~/Documents/GitHub/bbdmi/max/control_processing/calibrate",
				"patcherrelativepath" : "../../max/control_processing/calibrate",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "p.exposer.maxpat",
				"bootpath" : "~/Documents/GitHub/bbdmi/max/source/patchers",
				"patcherrelativepath" : "../../max/source/patchers",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "p.onepole~.maxpat",
				"bootpath" : "~/Documents/GitHub/bbdmi/max/utilities/onepole~",
				"patcherrelativepath" : "../../max/utilities/onepole~",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "p.scale.maxpat",
				"bootpath" : "~/Documents/GitHub/bbdmi/max/control_processing/scale",
				"patcherrelativepath" : "../../max/control_processing/scale",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "piano-lo-hit-44.wav",
				"bootpath" : "~/Documents/GitHub/bbdmi/events/2024-11-19_ArsMusica",
				"patcherrelativepath" : ".",
				"type" : "WAVE",
				"implicit" : 1
			}
 ],
		"autosave" : 0
	}

}
