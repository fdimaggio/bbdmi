{
	"patcher" : 	{
		"fileversion" : 1,
		"appversion" : 		{
			"major" : 8,
			"minor" : 5,
			"revision" : 6,
			"architecture" : "x64",
			"modernui" : 1
		}
,
		"classnamespace" : "box",
		"rect" : [ 97.0, 87.0, 1538.0, 1182.0 ],
		"bglocked" : 0,
		"openinpresentation" : 0,
		"default_fontsize" : 12.0,
		"default_fontface" : 0,
		"default_fontname" : "Arial",
		"gridonopen" : 1,
		"gridsize" : [ 15.0, 15.0 ],
		"gridsnaponopen" : 1,
		"objectsnaponopen" : 1,
		"statusbarvisible" : 2,
		"toolbarvisible" : 1,
		"lefttoolbarpinned" : 0,
		"toptoolbarpinned" : 0,
		"righttoolbarpinned" : 0,
		"bottomtoolbarpinned" : 0,
		"toolbars_unpinned_last_save" : 0,
		"tallnewobj" : 0,
		"boxanimatetime" : 200,
		"enablehscroll" : 1,
		"enablevscroll" : 1,
		"devicewidth" : 0.0,
		"description" : "",
		"digest" : "",
		"tags" : "",
		"style" : "",
		"subpatcher_template" : "",
		"assistshowspatchername" : 0,
		"boxes" : [ 			{
				"box" : 				{
					"id" : "obj-12",
					"lastchannelcount" : 0,
					"maxclass" : "live.gain~",
					"numinlets" : 2,
					"numoutlets" : 5,
					"outlettype" : [ "signal", "signal", "", "float", "list" ],
					"parameter_enable" : 1,
					"patching_rect" : [ 905.0, 790.333328664302826, 47.0, 136.0 ],
					"saved_attribute_attributes" : 					{
						"valueof" : 						{
							"parameter_longname" : "live.gain~[4]",
							"parameter_mmax" : 6.0,
							"parameter_mmin" : -70.0,
							"parameter_shortname" : "live.gain~",
							"parameter_type" : 0,
							"parameter_unitstyle" : 4
						}

					}
,
					"varname" : "live.gain~[4]"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-14",
					"lastchannelcount" : 0,
					"maxclass" : "live.gain~",
					"numinlets" : 2,
					"numoutlets" : 5,
					"outlettype" : [ "signal", "signal", "", "float", "list" ],
					"parameter_enable" : 1,
					"patching_rect" : [ 666.102598944678903, 112.333328664302826, 47.0, 136.0 ],
					"saved_attribute_attributes" : 					{
						"valueof" : 						{
							"parameter_longname" : "live.gain~[5]",
							"parameter_mmax" : 6.0,
							"parameter_mmin" : -70.0,
							"parameter_shortname" : "live.gain~",
							"parameter_type" : 0,
							"parameter_unitstyle" : 4
						}

					}
,
					"varname" : "live.gain~[5]"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-16",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patcher" : 					{
						"fileversion" : 1,
						"appversion" : 						{
							"major" : 8,
							"minor" : 5,
							"revision" : 6,
							"architecture" : "x64",
							"modernui" : 1
						}
,
						"classnamespace" : "box",
						"rect" : [ 85.0, 602.0, 1284.0, 804.0 ],
						"bglocked" : 0,
						"openinpresentation" : 0,
						"default_fontsize" : 12.0,
						"default_fontface" : 0,
						"default_fontname" : "Arial",
						"gridonopen" : 1,
						"gridsize" : [ 15.0, 15.0 ],
						"gridsnaponopen" : 1,
						"objectsnaponopen" : 1,
						"statusbarvisible" : 2,
						"toolbarvisible" : 1,
						"lefttoolbarpinned" : 0,
						"toptoolbarpinned" : 0,
						"righttoolbarpinned" : 0,
						"bottomtoolbarpinned" : 0,
						"toolbars_unpinned_last_save" : 0,
						"tallnewobj" : 0,
						"boxanimatetime" : 200,
						"enablehscroll" : 1,
						"enablevscroll" : 1,
						"devicewidth" : 0.0,
						"description" : "",
						"digest" : "",
						"tags" : "",
						"style" : "",
						"subpatcher_template" : "",
						"assistshowspatchername" : 0,
						"boxes" : [ 							{
								"box" : 								{
									"id" : "obj-13",
									"linecount" : 18,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 786.105525851249695, 440.643398344516868, 226.894474148750305, 263.0 ],
									"text" : ";\rinstr transpout 0.;\rinstr feedback 0.2;\rinstr gain 1.;\rinstr variability 0.3;\rinstr transpgrain 0.;\rinstr modfreqmod 0.5;\rinstr modmorph 0.;\rinstr modfreq 500.;\rinstr modfactor 0.;\rinstr spacing 80;\rinstr grainoffset 40;\rinstr grainsize 40;\rinstr maxdelay 2000.;\rinstr lpffreq 20000.;\rinstr hpffreq 20.;\rinstr grainenvmorph 1.;\rinstr indexdistr 0;\r"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-11",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 1049.0, 422.0, 150.0, 20.0 ],
									"text" : "voice 11 feb"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-124",
									"linecount" : 18,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1052.105525851249695, 448.643398344516868, 226.894474148750305, 263.0 ],
									"text" : ";\rinstr transpout 10.;\rinstr feedback 0.991;\rinstr gain 1.;\rinstr variability 0.44;\rinstr transpgrain 5.;\rinstr modfreqmod 0.9;\rinstr modmorph 0.098;\rinstr modfreq 199.;\rinstr modfactor 0.41;\rinstr spacing 190;\rinstr grainoffset 0;\rinstr grainsize 211;\rinstr maxdelay 2000.;\rinstr lpffreq 20000.;\rinstr hpffreq 20.;\rinstr grainenvmorph 0.445;\rinstr indexdistr 1;\r"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-10",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 550.0, 712.0, 150.0, 20.0 ],
									"text" : "piano transpose 1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-9",
									"linecount" : 18,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 550.0, 738.0, 226.894474148750305, 263.0 ],
									"text" : ";\rinstr transpout 0.;\rinstr feedback 0.;\rinstr gain 1.;\rinstr variability 0.;\rinstr transpgrain 1.;\rinstr modfreqmod 0.;\rinstr modmorph 0.;\rinstr modfreq 0.;\rinstr modfactor 0.;\rinstr spacing 1;\rinstr grainoffset 0;\rinstr grainsize 250;\rinstr maxdelay 500.;\rinstr lpffreq 20000.;\rinstr hpffreq 20.;\rinstr grainenvmorph 1.;\rinstr indexdistr 0;\r"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-8",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 800.0, 712.0, 150.0, 20.0 ],
									"text" : "piano transpose 2"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-7",
									"linecount" : 18,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 804.0, 734.0, 226.894474148750305, 263.0 ],
									"text" : ";\rinstr transpout 6.;\rinstr feedback 0.7;\rinstr gain 1.;\rinstr variability 0.;\rinstr transpgrain 0.;\rinstr modfreqmod 0.;\rinstr modmorph 0.;\rinstr modfreq 0.;\rinstr modfactor 0.;\rinstr spacing 1;\rinstr grainoffset 0;\rinstr grainsize 250;\rinstr maxdelay 500.;\rinstr lpffreq 20000.;\rinstr hpffreq 20.;\rinstr grainenvmorph 1.;\rinstr indexdistr 0;\r"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-6",
									"linecount" : 18,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 264.0, 104.0, 226.894474148750305, 263.0 ],
									"text" : ";\rinstr transpout 0.;\rinstr feedback 0.;\rinstr gain 1.;\rinstr variability 0.;\rinstr transpgrain 0.;\rinstr modfreqmod 0.;\rinstr modmorph 0.;\rinstr modfreq 0.;\rinstr modfactor 0.;\rinstr spacing 1;\rinstr grainoffset 0;\rinstr grainsize 250;\rinstr maxdelay 500.;\rinstr lpffreq 20000.;\rinstr hpffreq 20.;\rinstr grainenvmorph 1.;\rinstr indexdistr 0;\r"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-5",
									"linecount" : 18,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 546.0, 437.0, 226.894474148750305, 263.0 ],
									"text" : ";\rinstr transpout 0.;\rinstr feedback 0.;\rinstr gain 1.;\rinstr variability 0.5;\rinstr transpgrain 0.;\rinstr modfreqmod 0.;\rinstr modmorph 0.;\rinstr modfreq 0.;\rinstr modfactor 0.;\rinstr spacing 1;\rinstr grainoffset 50;\rinstr grainsize 250;\rinstr maxdelay 500.;\rinstr lpffreq 20000.;\rinstr hpffreq 20.;\rinstr grainenvmorph 1.;\rinstr indexdistr 0;\r"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-4",
									"linecount" : 18,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 6.0, 104.0, 226.894474148750305, 263.0 ],
									"text" : ";\rviola transpout 0.;\rviola feedback 0.2;\rviola gain 1.;\rviola variability 0.3;\rviola transpgrain 0.;\rviola modfreqmod 0.5;\rviola modmorph 0.;\rviola modfreq 500.;\rviola modfactor 0.;\rviola spacing 80;\rviola grainoffset 40;\rviola grainsize 40;\rviola maxdelay 2000.;\rviola lpffreq 20000.;\rviola hpffreq 20.;\rviola grainenvmorph 0.5;\rviola indexdistr 0;\r"
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-3",
									"index" : 1,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 213.894474148750305, 41.0, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-2",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 546.0, 409.0, 150.0, 20.0 ],
									"text" : "clarinet"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-1",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 546.0, 66.0, 150.0, 20.0 ],
									"text" : "viola"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-20",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 270.0, 74.0, 150.0, 20.0 ],
									"text" : "reset"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-34",
									"linecount" : 18,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 774.447237074375153, 96.0, 226.894474148750305, 263.0 ],
									"text" : ";\rinstr transpout 0.;\rinstr feedback 0.5;\rinstr gain 1.;\rinstr variability 0.;\rinstr transpgrain 0.;\rinstr modfreqmod 0.;\rinstr modmorph 0.;\rinstr modfreq 2.9;\rinstr modfactor 0.7;\rinstr spacing 17;\rinstr grainoffset 40;\rinstr grainsize 50;\rinstr maxdelay 300.;\rinstr lpffreq 20000.;\rinstr hpffreq 20.;\rinstr grainenvmorph 1.;\rinstr indexdistr 0;\r"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-29",
									"linecount" : 18,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 535.0, 96.0, 226.894474148750305, 263.0 ],
									"text" : ";\rinstr transpout 0.;\rinstr feedback 0.;\rinstr gain 1.;\rinstr variability 0.;\rinstr transpgrain 0.;\rinstr modfreqmod 0.5;\rinstr modmorph 0.;\rinstr modfreq 1000.;\rinstr modfactor 0.;\rinstr spacing 16;\rinstr grainoffset 0;\rinstr grainsize 219;\rinstr maxdelay 2000.;\rinstr lpffreq 20000.;\rinstr hpffreq 20.;\rinstr grainenvmorph 1.;\rinstr indexdistr 0;\r"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-12",
									"linecount" : 18,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 264.0, 387.0, 226.894474148750305, 263.0 ],
									"text" : ";\rinstr transpout 0.;\rinstr feedback 0.2;\rinstr gain 1.;\rinstr variability 0.3;\rinstr transpgrain 0.;\rinstr modfreqmod 0.5;\rinstr modmorph 0.;\rinstr modfreq 500.;\rinstr modfactor 0.;\rinstr spacing 80;\rinstr grainoffset 40;\rinstr grainsize 40;\rinstr maxdelay 2000.;\rinstr lpffreq 20000.;\rinstr hpffreq 20.;\rinstr grainenvmorph 1.;\rinstr indexdistr 0;\r"
								}

							}
 ],
						"lines" : [ 							{
								"patchline" : 								{
									"destination" : [ "obj-4", 1 ],
									"source" : [ "obj-3", 0 ]
								}

							}
 ]
					}
,
					"patching_rect" : [ 1085.0, 725.33332622051239, 58.0, 22.0 ],
					"saved_object_attributes" : 					{
						"description" : "",
						"digest" : "",
						"globalpatchername" : "",
						"tags" : ""
					}
,
					"text" : "p presets"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-18",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "signal" ],
					"patching_rect" : [ 905.0, 754.0, 74.0, 22.0 ],
					"text" : "mc.unpack~"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-20",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "multichannelsignal" ],
					"patching_rect" : [ 905.0, 725.33332622051239, 92.0, 22.0 ],
					"text" : "mc.mixdown~ 2"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-23",
					"maxclass" : "newobj",
					"numinlets" : 4,
					"numoutlets" : 1,
					"outlettype" : [ "multichannelsignal" ],
					"patching_rect" : [ 905.0, 112.333328664302826, 70.0, 22.0 ],
					"text" : "mc.pack~ 4"
				}

			}
, 			{
				"box" : 				{
					"basictuning" : 440,
					"clipheight" : 55.0,
					"data" : 					{
						"clips" : [ 							{
								"absolutepath" : "/Users/atau/Library/CloudStorage/Dropbox/projmusic30/eramaa/audio/nov23/viola-bounce-ed-44.wav",
								"filename" : "viola-bounce-ed-44.wav",
								"filekind" : "audiofile",
								"id" : "u642009816",
								"selection" : [ 0.616883116883117, 0.840909090909091 ],
								"loop" : 1,
								"content_state" : 								{
									"loop" : 1
								}

							}
 ]
					}
,
					"followglobaltempo" : 0,
					"formantcorrection" : 0,
					"id" : "obj-25",
					"maxclass" : "playlist~",
					"mode" : "basic",
					"numinlets" : 1,
					"numoutlets" : 5,
					"originallength" : [ 0.0, "ticks" ],
					"originaltempo" : 120.0,
					"outlettype" : [ "signal", "signal", "signal", "", "dictionary" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 672.980087950825691, 20.833328664302826, 363.0, 55.0 ],
					"pitchcorrection" : 0,
					"quality" : "basic",
					"timestretch" : [ 0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-28",
					"maxclass" : "meter~",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "float" ],
					"patching_rect" : [ 1001.0, 725.33332622051239, 59.0, 62.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-29",
					"linecount" : 18,
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 985.105525851249695, 790.333328664302826, 157.894474148750305, 263.0 ],
					"text" : ";\rviola transpout 0.;\rviola feedback 0.2;\rviola gain 1.;\rviola variability 0.3;\rviola transpgrain 0.;\rviola modfreqmod 0.5;\rviola modmorph 0.;\rviola modfreq 500.;\rviola modfactor 0.;\rviola spacing 80;\rviola grainoffset 40;\rviola grainsize 40;\rviola maxdelay 2000.;\rviola lpffreq 20000.;\rviola hpffreq 20.;\rviola grainenvmorph 0.5;\rviola indexdistr 0;\r"
				}

			}
, 			{
				"box" : 				{
					"args" : [ "viola", "@Channels", 7 ],
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-30",
					"lockeddragscroll" : 0,
					"lockedsize" : 0,
					"maxclass" : "bpatcher",
					"name" : "bbdmi_live_granulator~.maxpat",
					"numinlets" : 2,
					"numoutlets" : 3,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "multichannelsignal", "", "" ],
					"patching_rect" : [ 905.0, 138.835540967701036, 199.0, 564.0 ],
					"varname" : "bbdmi_live.granulator~[2]",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-4",
					"lastchannelcount" : 0,
					"maxclass" : "live.gain~",
					"numinlets" : 2,
					"numoutlets" : 5,
					"outlettype" : [ "signal", "signal", "", "float", "list" ],
					"parameter_enable" : 1,
					"patching_rect" : [ 392.0, 795.333328664302826, 47.0, 136.0 ],
					"saved_attribute_attributes" : 					{
						"valueof" : 						{
							"parameter_longname" : "live.gain~[3]",
							"parameter_mmax" : 6.0,
							"parameter_mmin" : -70.0,
							"parameter_shortname" : "live.gain~",
							"parameter_type" : 0,
							"parameter_unitstyle" : 4
						}

					}
,
					"varname" : "live.gain~[3]"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-3",
					"lastchannelcount" : 0,
					"maxclass" : "live.gain~",
					"numinlets" : 2,
					"numoutlets" : 5,
					"outlettype" : [ "signal", "signal", "", "float", "list" ],
					"parameter_enable" : 1,
					"patching_rect" : [ 153.102598944678903, 117.333328664302826, 47.0, 136.0 ],
					"saved_attribute_attributes" : 					{
						"valueof" : 						{
							"parameter_longname" : "live.gain~[2]",
							"parameter_mmax" : 6.0,
							"parameter_mmin" : -70.0,
							"parameter_shortname" : "live.gain~",
							"parameter_type" : 0,
							"parameter_unitstyle" : 4
						}

					}
,
					"varname" : "live.gain~[2]"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-55",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patcher" : 					{
						"fileversion" : 1,
						"appversion" : 						{
							"major" : 8,
							"minor" : 5,
							"revision" : 6,
							"architecture" : "x64",
							"modernui" : 1
						}
,
						"classnamespace" : "box",
						"rect" : [ 85.0, 602.0, 1284.0, 804.0 ],
						"bglocked" : 0,
						"openinpresentation" : 0,
						"default_fontsize" : 12.0,
						"default_fontface" : 0,
						"default_fontname" : "Arial",
						"gridonopen" : 1,
						"gridsize" : [ 15.0, 15.0 ],
						"gridsnaponopen" : 1,
						"objectsnaponopen" : 1,
						"statusbarvisible" : 2,
						"toolbarvisible" : 1,
						"lefttoolbarpinned" : 0,
						"toptoolbarpinned" : 0,
						"righttoolbarpinned" : 0,
						"bottomtoolbarpinned" : 0,
						"toolbars_unpinned_last_save" : 0,
						"tallnewobj" : 0,
						"boxanimatetime" : 200,
						"enablehscroll" : 1,
						"enablevscroll" : 1,
						"devicewidth" : 0.0,
						"description" : "",
						"digest" : "",
						"tags" : "",
						"style" : "",
						"subpatcher_template" : "",
						"assistshowspatchername" : 0,
						"boxes" : [ 							{
								"box" : 								{
									"id" : "obj-13",
									"linecount" : 18,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 786.105525851249695, 440.643398344516868, 226.894474148750305, 263.0 ],
									"text" : ";\rinstr transpout 0.;\rinstr feedback 0.2;\rinstr gain 1.;\rinstr variability 0.3;\rinstr transpgrain 0.;\rinstr modfreqmod 0.5;\rinstr modmorph 0.;\rinstr modfreq 500.;\rinstr modfactor 0.;\rinstr spacing 80;\rinstr grainoffset 40;\rinstr grainsize 40;\rinstr maxdelay 2000.;\rinstr lpffreq 20000.;\rinstr hpffreq 20.;\rinstr grainenvmorph 1.;\rinstr indexdistr 0;\r"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-11",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 1049.0, 422.0, 150.0, 20.0 ],
									"text" : "voice 11 feb"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-124",
									"linecount" : 18,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1052.105525851249695, 448.643398344516868, 226.894474148750305, 263.0 ],
									"text" : ";\rinstr transpout 10.;\rinstr feedback 0.991;\rinstr gain 1.;\rinstr variability 0.44;\rinstr transpgrain 5.;\rinstr modfreqmod 0.9;\rinstr modmorph 0.098;\rinstr modfreq 199.;\rinstr modfactor 0.41;\rinstr spacing 190;\rinstr grainoffset 0;\rinstr grainsize 211;\rinstr maxdelay 2000.;\rinstr lpffreq 20000.;\rinstr hpffreq 20.;\rinstr grainenvmorph 0.445;\rinstr indexdistr 1;\r"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-10",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 550.0, 712.0, 150.0, 20.0 ],
									"text" : "piano transpose 1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-9",
									"linecount" : 18,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 550.0, 738.0, 226.894474148750305, 263.0 ],
									"text" : ";\rinstr transpout 0.;\rinstr feedback 0.;\rinstr gain 1.;\rinstr variability 0.;\rinstr transpgrain 1.;\rinstr modfreqmod 0.;\rinstr modmorph 0.;\rinstr modfreq 0.;\rinstr modfactor 0.;\rinstr spacing 1;\rinstr grainoffset 0;\rinstr grainsize 250;\rinstr maxdelay 500.;\rinstr lpffreq 20000.;\rinstr hpffreq 20.;\rinstr grainenvmorph 1.;\rinstr indexdistr 0;\r"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-8",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 800.0, 712.0, 150.0, 20.0 ],
									"text" : "piano transpose 2"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-7",
									"linecount" : 18,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 804.0, 734.0, 226.894474148750305, 263.0 ],
									"text" : ";\rinstr transpout 6.;\rinstr feedback 0.7;\rinstr gain 1.;\rinstr variability 0.;\rinstr transpgrain 0.;\rinstr modfreqmod 0.;\rinstr modmorph 0.;\rinstr modfreq 0.;\rinstr modfactor 0.;\rinstr spacing 1;\rinstr grainoffset 0;\rinstr grainsize 250;\rinstr maxdelay 500.;\rinstr lpffreq 20000.;\rinstr hpffreq 20.;\rinstr grainenvmorph 1.;\rinstr indexdistr 0;\r"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-6",
									"linecount" : 18,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 264.0, 104.0, 226.894474148750305, 263.0 ],
									"text" : ";\rinstr transpout 0.;\rinstr feedback 0.;\rinstr gain 1.;\rinstr variability 0.;\rinstr transpgrain 0.;\rinstr modfreqmod 0.;\rinstr modmorph 0.;\rinstr modfreq 0.;\rinstr modfactor 0.;\rinstr spacing 1;\rinstr grainoffset 0;\rinstr grainsize 250;\rinstr maxdelay 500.;\rinstr lpffreq 20000.;\rinstr hpffreq 20.;\rinstr grainenvmorph 1.;\rinstr indexdistr 0;\r"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-5",
									"linecount" : 18,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 546.0, 437.0, 226.894474148750305, 263.0 ],
									"text" : ";\rinstr transpout 0.;\rinstr feedback 0.;\rinstr gain 1.;\rinstr variability 0.5;\rinstr transpgrain 0.;\rinstr modfreqmod 0.;\rinstr modmorph 0.;\rinstr modfreq 0.;\rinstr modfactor 0.;\rinstr spacing 1;\rinstr grainoffset 50;\rinstr grainsize 250;\rinstr maxdelay 500.;\rinstr lpffreq 20000.;\rinstr hpffreq 20.;\rinstr grainenvmorph 1.;\rinstr indexdistr 0;\r"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-4",
									"linecount" : 18,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 6.0, 104.0, 226.894474148750305, 263.0 ],
									"text" : ";\rbassclar transpout 0.;\rbassclar feedback 0.2;\rbassclar gain 1.;\rbassclar variability 0.3;\rbassclar transpgrain 0.;\rbassclar modfreqmod 0.5;\rbassclar modmorph 0.;\rbassclar modfreq 500.;\rbassclar modfactor 0.;\rbassclar spacing 80;\rbassclar grainoffset 40;\rbassclar grainsize 40;\rbassclar maxdelay 2000.;\rbassclar lpffreq 20000.;\rbassclar hpffreq 20.;\rbassclar grainenvmorph 0.5;\rbassclar indexdistr 0;\r"
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-3",
									"index" : 1,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 213.894474148750305, 41.0, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-2",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 546.0, 409.0, 150.0, 20.0 ],
									"text" : "clarinet"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-1",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 546.0, 66.0, 150.0, 20.0 ],
									"text" : "viola"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-20",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 270.0, 74.0, 150.0, 20.0 ],
									"text" : "reset"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-34",
									"linecount" : 18,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 774.447237074375153, 96.0, 226.894474148750305, 263.0 ],
									"text" : ";\rinstr transpout 0.;\rinstr feedback 0.5;\rinstr gain 1.;\rinstr variability 0.;\rinstr transpgrain 0.;\rinstr modfreqmod 0.;\rinstr modmorph 0.;\rinstr modfreq 2.9;\rinstr modfactor 0.7;\rinstr spacing 17;\rinstr grainoffset 40;\rinstr grainsize 50;\rinstr maxdelay 300.;\rinstr lpffreq 20000.;\rinstr hpffreq 20.;\rinstr grainenvmorph 1.;\rinstr indexdistr 0;\r"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-29",
									"linecount" : 18,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 535.0, 96.0, 226.894474148750305, 263.0 ],
									"text" : ";\rinstr transpout 0.;\rinstr feedback 0.;\rinstr gain 1.;\rinstr variability 0.;\rinstr transpgrain 0.;\rinstr modfreqmod 0.5;\rinstr modmorph 0.;\rinstr modfreq 1000.;\rinstr modfactor 0.;\rinstr spacing 16;\rinstr grainoffset 0;\rinstr grainsize 219;\rinstr maxdelay 2000.;\rinstr lpffreq 20000.;\rinstr hpffreq 20.;\rinstr grainenvmorph 1.;\rinstr indexdistr 0;\r"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-12",
									"linecount" : 18,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 264.0, 387.0, 226.894474148750305, 263.0 ],
									"text" : ";\rinstr transpout 0.;\rinstr feedback 0.2;\rinstr gain 1.;\rinstr variability 0.3;\rinstr transpgrain 0.;\rinstr modfreqmod 0.5;\rinstr modmorph 0.;\rinstr modfreq 500.;\rinstr modfactor 0.;\rinstr spacing 80;\rinstr grainoffset 40;\rinstr grainsize 40;\rinstr maxdelay 2000.;\rinstr lpffreq 20000.;\rinstr hpffreq 20.;\rinstr grainenvmorph 1.;\rinstr indexdistr 0;\r"
								}

							}
 ],
						"lines" : [ 							{
								"patchline" : 								{
									"destination" : [ "obj-4", 1 ],
									"source" : [ "obj-3", 0 ]
								}

							}
 ]
					}
,
					"patching_rect" : [ 572.0, 730.33332622051239, 58.0, 22.0 ],
					"saved_object_attributes" : 					{
						"description" : "",
						"digest" : "",
						"globalpatchername" : "",
						"tags" : ""
					}
,
					"text" : "p presets"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-54",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patcher" : 					{
						"fileversion" : 1,
						"appversion" : 						{
							"major" : 8,
							"minor" : 5,
							"revision" : 6,
							"architecture" : "x64",
							"modernui" : 1
						}
,
						"classnamespace" : "box",
						"rect" : [ 0.0, 0.0, 640.0, 480.0 ],
						"bglocked" : 0,
						"openinpresentation" : 0,
						"default_fontsize" : 12.0,
						"default_fontface" : 0,
						"default_fontname" : "Arial",
						"gridonopen" : 1,
						"gridsize" : [ 15.0, 15.0 ],
						"gridsnaponopen" : 1,
						"objectsnaponopen" : 1,
						"statusbarvisible" : 2,
						"toolbarvisible" : 1,
						"lefttoolbarpinned" : 0,
						"toptoolbarpinned" : 0,
						"righttoolbarpinned" : 0,
						"bottomtoolbarpinned" : 0,
						"toolbars_unpinned_last_save" : 0,
						"tallnewobj" : 0,
						"boxanimatetime" : 200,
						"enablehscroll" : 1,
						"enablevscroll" : 1,
						"devicewidth" : 0.0,
						"description" : "",
						"digest" : "",
						"tags" : "",
						"style" : "",
						"subpatcher_template" : "",
						"assistshowspatchername" : 0,
						"boxes" : [ 							{
								"box" : 								{
									"id" : "obj-48",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 50.0, 129.0, 29.5, 22.0 ],
									"text" : "0"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-49",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 80.75, 129.0, 29.5, 22.0 ],
									"text" : "-70"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-46",
									"maxclass" : "newobj",
									"numinlets" : 3,
									"numoutlets" : 3,
									"outlettype" : [ "bang", "bang", "" ],
									"patching_rect" : [ 77.75, 100.0, 60.0, 22.0 ],
									"text" : "select 0 1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-41",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 145.75, 129.0, 29.5, 22.0 ],
									"text" : "0"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-39",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 116.75, 129.0, 29.5, 22.0 ],
									"text" : "-70"
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-50",
									"index" : 1,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"patching_rect" : [ 77.75, 40.0, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-51",
									"index" : 1,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 77.375, 211.0, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-53",
									"index" : 2,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 112.375, 211.0, 30.0, 30.0 ]
								}

							}
 ],
						"lines" : [ 							{
								"patchline" : 								{
									"destination" : [ "obj-51", 0 ],
									"source" : [ "obj-39", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-53", 0 ],
									"source" : [ "obj-41", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-39", 0 ],
									"order" : 1,
									"source" : [ "obj-46", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-41", 0 ],
									"order" : 0,
									"source" : [ "obj-46", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-48", 0 ],
									"order" : 1,
									"source" : [ "obj-46", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-49", 0 ],
									"order" : 0,
									"source" : [ "obj-46", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-51", 0 ],
									"source" : [ "obj-48", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-53", 0 ],
									"source" : [ "obj-49", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-46", 0 ],
									"source" : [ "obj-50", 0 ]
								}

							}
 ]
					}
,
					"patching_rect" : [ 105.714285714285722, 69.168869632003862, 34.25, 22.0 ],
					"saved_object_attributes" : 					{
						"description" : "",
						"digest" : "",
						"globalpatchername" : "",
						"tags" : ""
					}
,
					"text" : "p"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-45",
					"maxclass" : "toggle",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 105.714285714285722, 41.333328664302826, 24.0, 24.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-36",
					"lastchannelcount" : 0,
					"maxclass" : "live.gain~",
					"numinlets" : 2,
					"numoutlets" : 5,
					"outlettype" : [ "signal", "signal", "", "float", "list" ],
					"parameter_enable" : 1,
					"patching_rect" : [ 32.0, 117.333328664302826, 47.0, 136.0 ],
					"saved_attribute_attributes" : 					{
						"valueof" : 						{
							"parameter_longname" : "live.gain~[1]",
							"parameter_mmax" : 6.0,
							"parameter_mmin" : -70.0,
							"parameter_shortname" : "live.gain~",
							"parameter_type" : 0,
							"parameter_unitstyle" : 4
						}

					}
,
					"varname" : "live.gain~[1]"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-32",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 0,
					"patching_rect" : [ 666.0, 1097.33332622051239, 55.0, 22.0 ],
					"text" : "dac~ 1 2"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-17",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "signal" ],
					"patching_rect" : [ 392.0, 759.0, 74.0, 22.0 ],
					"text" : "mc.unpack~"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-2",
					"lastchannelcount" : 0,
					"maxclass" : "live.gain~",
					"numinlets" : 2,
					"numoutlets" : 5,
					"outlettype" : [ "signal", "signal", "", "float", "list" ],
					"parameter_enable" : 1,
					"patching_rect" : [ 81.0, 117.333328664302826, 47.0, 136.0 ],
					"saved_attribute_attributes" : 					{
						"valueof" : 						{
							"parameter_longname" : "live.gain~",
							"parameter_mmax" : 6.0,
							"parameter_mmin" : -70.0,
							"parameter_shortname" : "live.gain~",
							"parameter_type" : 0,
							"parameter_unitstyle" : 4
						}

					}
,
					"varname" : "live.gain~"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-1",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "signal" ],
					"patching_rect" : [ 41.0, 50.333328664302826, 55.0, 22.0 ],
					"text" : "adc~ 1 2"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-15",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1550.0, 231.158070654236212, 106.0, 22.0 ],
					"text" : "loadmess autofit 1"
				}

			}
, 			{
				"box" : 				{
					"autofit" : 1,
					"id" : "obj-94",
					"maxclass" : "fpic",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "jit_matrix" ],
					"patching_rect" : [ 1559.0, 258.158070654236212, 63.0, 281.0 ],
					"pic" : "Macintosh HD:/Users/davidfierro/Documents/Presentation_Granulator/images/indexdistr.png"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-13",
					"linecount" : 3,
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 56.124190639704466, 784.335540967701036, 178.751618720591068, 47.0 ],
					"text" : "Click on the arrows to test a 3 minutes path between different configurations of the granulator:"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-201",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 110.165045998990536, 984.002210336444932, 67.0, 20.0 ],
					"text" : "OutputsML"
				}

			}
, 			{
				"box" : 				{
					"candycane" : 10,
					"contdata" : 1,
					"id" : "obj-213",
					"maxclass" : "multislider",
					"numinlets" : 1,
					"numoutlets" : 2,
					"orientation" : 0,
					"outlettype" : [ "", "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 47.228469230234623, 1006.002210336444932, 197.0, 211.0 ],
					"setminmax" : [ 0.0, 1.0 ],
					"signed" : 1,
					"size" : 16,
					"slidercolor" : [ 1.0, 1.0, 1.0, 1.0 ],
					"spacing" : 4,
					"thickness" : 4
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-85",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "float", "" ],
					"patcher" : 					{
						"fileversion" : 1,
						"appversion" : 						{
							"major" : 8,
							"minor" : 5,
							"revision" : 6,
							"architecture" : "x64",
							"modernui" : 1
						}
,
						"classnamespace" : "box",
						"rect" : [ 59.0, 119.0, 1419.0, 829.0 ],
						"bglocked" : 0,
						"openinpresentation" : 0,
						"default_fontsize" : 12.0,
						"default_fontface" : 0,
						"default_fontname" : "Arial",
						"gridonopen" : 1,
						"gridsize" : [ 15.0, 15.0 ],
						"gridsnaponopen" : 1,
						"objectsnaponopen" : 1,
						"statusbarvisible" : 2,
						"toolbarvisible" : 1,
						"lefttoolbarpinned" : 0,
						"toptoolbarpinned" : 0,
						"righttoolbarpinned" : 0,
						"bottomtoolbarpinned" : 0,
						"toolbars_unpinned_last_save" : 0,
						"tallnewobj" : 0,
						"boxanimatetime" : 200,
						"enablehscroll" : 1,
						"enablevscroll" : 1,
						"devicewidth" : 0.0,
						"description" : "",
						"digest" : "",
						"tags" : "",
						"style" : "",
						"subpatcher_template" : "",
						"assistshowspatchername" : 0,
						"boxes" : [ 							{
								"box" : 								{
									"id" : "obj-15",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1016.0, 461.0, 50.0, 22.0 ],
									"text" : "bang"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-275",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 699.469821825623512, 325.551912132554207, 55.0, 22.0 ],
									"text" : "pipe 100"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-274",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"patching_rect" : [ 685.469821825623512, 298.151910988145119, 58.0, 22.0 ],
									"text" : "loadbang"
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-1",
									"index" : 2,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 105.000015049174294, 508.920112244015286, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"args" : [ 13 ],
									"bgmode" : 0,
									"border" : 1,
									"clickthrough" : 0,
									"enablehscroll" : 0,
									"enablevscroll" : 0,
									"id" : "obj-162",
									"lockeddragscroll" : 0,
									"lockedsize" : 0,
									"maxclass" : "bpatcher",
									"name" : "bbdmi.2max.maxpat",
									"numinlets" : 1,
									"numoutlets" : 0,
									"offset" : [ 0.0, 0.0 ],
									"patching_rect" : [ 448.033341312004268, 258.168177830215654, 195.533333301544189, 445.766659235954194 ],
									"varname" : "bbdmi.2max",
									"viewvisibility" : 1
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-72",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "float" ],
									"patching_rect" : [ 50.0, 473.985275546364392, 40.0, 22.0 ],
									"text" : "* 128."
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-31",
									"maxclass" : "newobj",
									"numinlets" : 3,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"patching_rect" : [ 50.0, 149.865264463424637, 41.0, 22.0 ],
									"text" : "line 0."
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-33",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 114.25, 100.0, 62.0, 22.0 ],
									"text" : "1. 360000"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-35",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 50.25, 100.0, 62.0, 22.0 ],
									"text" : "0. 360000"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-83",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"patching_rect" : [ 229.266662418842316, 153.551912132554207, 58.0, 22.0 ],
									"text" : "loadbang"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-30",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"patching_rect" : [ 369.936576768755913, 195.629464593219382, 61.0, 22.0 ],
									"text" : "delay 200"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-28",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 434.60327635705471, 195.629464593219382, 29.5, 22.0 ],
									"text" : "run"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-113",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 229.266662418842316, 195.629464593219382, 132.0, 22.0 ],
									"text" : "read modelDefault.json"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-296",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 99.25, 216.384840106006777, 54.0, 20.0 ],
									"text" : "ML input"
								}

							}
, 							{
								"box" : 								{
									"format" : 6,
									"id" : "obj-169",
									"maxclass" : "flonum",
									"maximum" : 1.0,
									"minimum" : 0.0,
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 50.0, 182.29613086252175, 50.0, 22.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-151",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 310.766674607992172, 236.168177830215654, 77.0, 20.0 ],
									"text" : "ML OUTPUT"
								}

							}
, 							{
								"box" : 								{
									"annotation" : "",
									"args" : [ 13 ],
									"bgmode" : 0,
									"border" : 1,
									"clickthrough" : 0,
									"enablehscroll" : 0,
									"enablevscroll" : 0,
									"hint" : "",
									"id" : "obj-163",
									"lockeddragscroll" : 0,
									"lockedsize" : 0,
									"maxclass" : "bpatcher",
									"name" : "bbdmi.scale.maxpat",
									"numinlets" : 1,
									"numoutlets" : 1,
									"offset" : [ 0.0, 0.0 ],
									"outlettype" : [ "" ],
									"patching_rect" : [ 248.599999964237213, 258.168177830215654, 196.199999988079071, 445.766659235954194 ],
									"varname" : "bbdmi.scale",
									"viewvisibility" : 1
								}

							}
, 							{
								"box" : 								{
									"annotation" : "",
									"bgmode" : 0,
									"border" : 1,
									"clickthrough" : 0,
									"enablehscroll" : 0,
									"enablevscroll" : 0,
									"hint" : "",
									"id" : "obj-165",
									"lockeddragscroll" : 0,
									"lockedsize" : 0,
									"maxclass" : "bpatcher",
									"name" : "bbdmi.regress.maxpat",
									"numinlets" : 3,
									"numoutlets" : 2,
									"offset" : [ 0.0, 0.0 ],
									"outlettype" : [ "", "" ],
									"patching_rect" : [ 50.0, 258.168177830215654, 196.599999964237213, 199.799998104572296 ],
									"varname" : "bbdmi.regress[1]",
									"viewvisibility" : 1
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-4",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 50.0, 216.384840106006777, 47.0, 22.0 ],
									"text" : "pack f f"
								}

							}
, 							{
								"box" : 								{
									"fontface" : 0,
									"fontname" : "Arial",
									"fontsize" : 14.0,
									"id" : "obj-171",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 685.469821825623512, 361.485275546364846, 65.0, 22.0 ],
									"text" : "PRESET"
								}

							}
, 							{
								"box" : 								{
									"bgcolor" : [ 0.137254901960784, 0.137254901960784, 0.137254901960784, 1.0 ],
									"fontname" : "Arial",
									"fontsize" : 12.0,
									"id" : "obj-173",
									"maxclass" : "textbutton",
									"numinlets" : 1,
									"numoutlets" : 3,
									"outlettype" : [ "", "", "int" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 727.330234423279762, 395.393971244148815, 39.0, 21.0 ],
									"text" : "store",
									"texton" : "store",
									"textoncolor" : [ 1.0, 1.0, 1.0, 1.0 ]
								}

							}
, 							{
								"box" : 								{
									"bgcolor" : [ 0.137254901960784, 0.137254901960784, 0.137254901960784, 1.0 ],
									"fontname" : "Arial",
									"fontsize" : 12.0,
									"id" : "obj-225",
									"maxclass" : "textbutton",
									"numinlets" : 1,
									"numoutlets" : 3,
									"outlettype" : [ "", "", "int" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 685.469821825623512, 395.393971244148815, 39.0, 21.0 ],
									"text" : "recall",
									"texton" : "recall",
									"textoncolor" : [ 1.0, 1.0, 1.0, 1.0 ]
								}

							}
, 							{
								"box" : 								{
									"bgcolor" : [ 0.137254901960784, 0.137254901960784, 0.137254901960784, 1.0 ],
									"fontname" : "Arial",
									"fontsize" : 12.0,
									"id" : "obj-175",
									"maxclass" : "textbutton",
									"numinlets" : 1,
									"numoutlets" : 3,
									"outlettype" : [ "", "", "int" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 801.969821825623512, 505.811362544350686, 66.0, 21.0 ],
									"text" : "writeagain",
									"texton" : "writeagain",
									"textoncolor" : [ 1.0, 1.0, 1.0, 1.0 ]
								}

							}
, 							{
								"box" : 								{
									"bgcolor" : [ 0.137254901960784, 0.137254901960784, 0.137254901960784, 1.0 ],
									"fontname" : "Arial",
									"fontsize" : 12.0,
									"id" : "obj-226",
									"maxclass" : "textbutton",
									"numinlets" : 1,
									"numoutlets" : 3,
									"outlettype" : [ "", "", "int" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 757.219821825623512, 505.811362544350686, 39.0, 21.0 ],
									"text" : "write",
									"texton" : "write",
									"textoncolor" : [ 1.0, 1.0, 1.0, 1.0 ]
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 12.0,
									"id" : "obj-177",
									"maxclass" : "newobj",
									"numinlets" : 3,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patcher" : 									{
										"fileversion" : 1,
										"appversion" : 										{
											"major" : 8,
											"minor" : 5,
											"revision" : 6,
											"architecture" : "x64",
											"modernui" : 1
										}
,
										"classnamespace" : "box",
										"rect" : [ 36.0, 100.0, 228.0, 264.0 ],
										"bglocked" : 0,
										"openinpresentation" : 0,
										"default_fontsize" : 12.0,
										"default_fontface" : 0,
										"default_fontname" : "Arial",
										"gridonopen" : 1,
										"gridsize" : [ 15.0, 15.0 ],
										"gridsnaponopen" : 1,
										"objectsnaponopen" : 1,
										"statusbarvisible" : 2,
										"toolbarvisible" : 1,
										"lefttoolbarpinned" : 0,
										"toptoolbarpinned" : 0,
										"righttoolbarpinned" : 0,
										"bottomtoolbarpinned" : 0,
										"toolbars_unpinned_last_save" : 0,
										"tallnewobj" : 0,
										"boxanimatetime" : 200,
										"enablehscroll" : 1,
										"enablevscroll" : 1,
										"devicewidth" : 0.0,
										"description" : "",
										"digest" : "",
										"tags" : "",
										"style" : "",
										"subpatcher_template" : "Default Max 7",
										"assistshowspatchername" : 0,
										"boxes" : [ 											{
												"box" : 												{
													"comment" : "",
													"id" : "obj-24",
													"index" : 3,
													"maxclass" : "inlet",
													"numinlets" : 0,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 114.357142857142833, 12.5, 30.0, 30.0 ]
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-23",
													"maxclass" : "button",
													"numinlets" : 1,
													"numoutlets" : 1,
													"outlettype" : [ "bang" ],
													"parameter_enable" : 0,
													"patching_rect" : [ 69.357142857142833, 66.5, 24.0, 24.0 ]
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-21",
													"maxclass" : "button",
													"numinlets" : 1,
													"numoutlets" : 1,
													"outlettype" : [ "bang" ],
													"parameter_enable" : 0,
													"patching_rect" : [ 11.357142857142833, 66.5, 24.0, 24.0 ]
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-19",
													"maxclass" : "message",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 11.357142857142833, 143.0, 54.0, 22.0 ],
													"text" : "recall $1"
												}

											}
, 											{
												"box" : 												{
													"color" : [ 1.0, 0.61176472902298, 0.61176472902298, 1.0 ],
													"fontface" : 0,
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-59",
													"maxclass" : "newobj",
													"numinlets" : 1,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 147.799525669642833, 67.5, 70.0, 22.0 ],
													"text" : "loadmess 1"
												}

											}
, 											{
												"box" : 												{
													"fontface" : 0,
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-64",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "int" ],
													"patching_rect" : [ 11.357142857142833, 114.5, 39.0, 22.0 ],
													"text" : "i"
												}

											}
, 											{
												"box" : 												{
													"fontface" : 0,
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-66",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "int" ],
													"patching_rect" : [ 69.357142857142833, 114.5, 40.5, 22.0 ],
													"text" : "i"
												}

											}
, 											{
												"box" : 												{
													"fontface" : 0,
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-68",
													"maxclass" : "message",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 69.357142857142833, 143.0, 52.0, 22.0 ],
													"text" : "store $1"
												}

											}
, 											{
												"box" : 												{
													"comment" : "",
													"id" : "obj-17",
													"index" : 1,
													"maxclass" : "outlet",
													"numinlets" : 1,
													"numoutlets" : 0,
													"patching_rect" : [ 10.5, 218.0, 30.0, 30.0 ]
												}

											}
, 											{
												"box" : 												{
													"comment" : "",
													"id" : "obj-16",
													"index" : 2,
													"maxclass" : "inlet",
													"numinlets" : 0,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 69.357142857142833, 12.5, 30.0, 30.0 ]
												}

											}
, 											{
												"box" : 												{
													"comment" : "",
													"id" : "obj-14",
													"index" : 1,
													"maxclass" : "inlet",
													"numinlets" : 0,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 11.357142857142833, 12.5, 30.0, 30.0 ]
												}

											}
 ],
										"lines" : [ 											{
												"patchline" : 												{
													"destination" : [ "obj-21", 0 ],
													"midpoints" : [ 20.857142857142833, 45.0, 20.857142857142833, 45.0 ],
													"source" : [ "obj-14", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-23", 0 ],
													"midpoints" : [ 78.857142857142833, 45.0, 78.857142857142833, 45.0 ],
													"source" : [ "obj-16", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-17", 0 ],
													"midpoints" : [ 20.857142857142833, 204.0, 20.0, 204.0 ],
													"source" : [ "obj-19", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-64", 0 ],
													"midpoints" : [ 20.857142857142833, 93.0, 20.857142857142833, 93.0 ],
													"source" : [ "obj-21", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-66", 0 ],
													"midpoints" : [ 78.857142857142833, 93.0, 78.857142857142833, 93.0 ],
													"source" : [ "obj-23", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-64", 1 ],
													"midpoints" : [ 123.857142857142833, 99.0, 40.857142857142833, 99.0 ],
													"order" : 1,
													"source" : [ "obj-24", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-66", 1 ],
													"midpoints" : [ 123.857142857142833, 99.0, 100.357142857142833, 99.0 ],
													"order" : 0,
													"source" : [ "obj-24", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-64", 1 ],
													"midpoints" : [ 157.299525669642833, 99.0, 40.857142857142833, 99.0 ],
													"order" : 1,
													"source" : [ "obj-59", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-66", 1 ],
													"midpoints" : [ 157.299525669642833, 99.0, 100.357142857142833, 99.0 ],
													"order" : 0,
													"source" : [ "obj-59", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-19", 0 ],
													"midpoints" : [ 20.857142857142833, 138.0, 20.857142857142833, 138.0 ],
													"source" : [ "obj-64", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-68", 0 ],
													"midpoints" : [ 78.857142857142833, 138.0, 78.857142857142833, 138.0 ],
													"source" : [ "obj-66", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-17", 0 ],
													"midpoints" : [ 78.857142857142833, 204.0, 20.0, 204.0 ],
													"source" : [ "obj-68", 0 ]
												}

											}
 ]
									}
,
									"patching_rect" : [ 685.469821825623512, 473.985275546364392, 101.0, 22.0 ],
									"saved_object_attributes" : 									{
										"description" : "",
										"digest" : "",
										"globalpatchername" : "",
										"tags" : ""
									}
,
									"text" : "p settings"
								}

							}
, 							{
								"box" : 								{
									"bgcolor" : [ 0.137254901960784, 0.137254901960784, 0.137254901960784, 1.0 ],
									"fontname" : "Arial",
									"fontsize" : 12.0,
									"id" : "obj-179",
									"maxclass" : "textbutton",
									"numinlets" : 1,
									"numoutlets" : 3,
									"outlettype" : [ "", "", "int" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 881.469821825623512, 505.811362544350686, 39.0, 21.0 ],
									"text" : "clear",
									"texton" : "clear",
									"textoncolor" : [ 1.0, 1.0, 1.0, 1.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-227",
									"maxclass" : "incdec",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "float" ],
									"parameter_enable" : 1,
									"patching_rect" : [ 768.330234423279762, 395.393971244148815, 41.0, 21.0 ],
									"saved_attribute_attributes" : 									{
										"valueof" : 										{
											"parameter_initial" : [ 1 ],
											"parameter_initial_enable" : 1,
											"parameter_invisible" : 1,
											"parameter_longname" : "incdec",
											"parameter_shortname" : "incdec",
											"parameter_type" : 3
										}

									}
,
									"varname" : "incdec[1]"
								}

							}
, 							{
								"box" : 								{
									"fontface" : 0,
									"fontname" : "Arial",
									"fontsize" : 12.0,
									"id" : "obj-228",
									"maxclass" : "number",
									"minimum" : 1,
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 768.330234423279762, 424.372231999687756, 40.0, 22.0 ],
									"triangle" : 0,
									"varname" : "number[1]"
								}

							}
, 							{
								"box" : 								{
									"bgcolor" : [ 0.137254901960784, 0.137254901960784, 0.137254901960784, 1.0 ],
									"fontname" : "Arial",
									"fontsize" : 12.0,
									"id" : "obj-182",
									"maxclass" : "textbutton",
									"numinlets" : 1,
									"numoutlets" : 3,
									"outlettype" : [ "", "", "int" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 712.469821825623512, 505.811362544350686, 39.0, 21.0 ],
									"text" : "read",
									"texton" : "read",
									"textoncolor" : [ 1.0, 1.0, 1.0, 1.0 ]
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 12.0,
									"id" : "obj-229",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patcher" : 									{
										"fileversion" : 1,
										"appversion" : 										{
											"major" : 8,
											"minor" : 5,
											"revision" : 6,
											"architecture" : "x64",
											"modernui" : 1
										}
,
										"classnamespace" : "box",
										"rect" : [ 36.0, 79.0, 160.0, 238.0 ],
										"bglocked" : 0,
										"openinpresentation" : 0,
										"default_fontsize" : 12.0,
										"default_fontface" : 0,
										"default_fontname" : "Arial",
										"gridonopen" : 1,
										"gridsize" : [ 15.0, 15.0 ],
										"gridsnaponopen" : 1,
										"objectsnaponopen" : 1,
										"statusbarvisible" : 2,
										"toolbarvisible" : 1,
										"lefttoolbarpinned" : 0,
										"toptoolbarpinned" : 0,
										"righttoolbarpinned" : 0,
										"bottomtoolbarpinned" : 0,
										"toolbars_unpinned_last_save" : 0,
										"tallnewobj" : 0,
										"boxanimatetime" : 200,
										"enablehscroll" : 1,
										"enablevscroll" : 1,
										"devicewidth" : 0.0,
										"description" : "",
										"digest" : "",
										"tags" : "",
										"style" : "",
										"subpatcher_template" : "Default Max 7",
										"assistshowspatchername" : 0,
										"boxes" : [ 											{
												"box" : 												{
													"id" : "obj-85",
													"maxclass" : "newobj",
													"numinlets" : 1,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 33.25, 152.5, 120.0, 22.0 ],
													"text" : "prepend pattrstorage"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-78",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 2,
													"outlettype" : [ "", "" ],
													"patching_rect" : [ 14.0, 76.0, 63.0, 22.0 ],
													"text" : "route read"
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-66",
													"maxclass" : "newobj",
													"numinlets" : 1,
													"numoutlets" : 5,
													"outlettype" : [ "", "", "", "", "" ],
													"patching_rect" : [ 14.0, 125.384386777777763, 96.0, 22.0 ],
													"text" : "regexp (.+)\\\\..+$"
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-72",
													"maxclass" : "newobj",
													"numinlets" : 1,
													"numoutlets" : 2,
													"outlettype" : [ "", "int" ],
													"patching_rect" : [ 14.0, 101.384386777777763, 57.0, 22.0 ],
													"text" : "strippath"
												}

											}
, 											{
												"box" : 												{
													"comment" : "",
													"id" : "obj-86",
													"index" : 1,
													"maxclass" : "inlet",
													"numinlets" : 0,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 14.000000023437501, 16.000012222222267, 30.0, 30.0 ]
												}

											}
, 											{
												"box" : 												{
													"comment" : "",
													"id" : "obj-87",
													"index" : 1,
													"maxclass" : "outlet",
													"numinlets" : 1,
													"numoutlets" : 0,
													"patching_rect" : [ 33.250000023437494, 192.500012222222267, 30.0, 30.0 ]
												}

											}
 ],
										"lines" : [ 											{
												"patchline" : 												{
													"destination" : [ "obj-85", 0 ],
													"source" : [ "obj-66", 1 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-66", 0 ],
													"source" : [ "obj-72", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-72", 0 ],
													"source" : [ "obj-78", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-87", 0 ],
													"source" : [ "obj-85", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-78", 0 ],
													"source" : [ "obj-86", 0 ]
												}

											}
 ]
									}
,
									"patching_rect" : [ 685.469821825623512, 593.189623395256604, 52.0, 22.0 ],
									"saved_object_attributes" : 									{
										"description" : "",
										"digest" : "",
										"globalpatchername" : "",
										"tags" : ""
									}
,
									"text" : "p preset"
								}

							}
, 							{
								"box" : 								{
									"bubblesize" : 20,
									"embed" : 0,
									"id" : "obj-230",
									"maxclass" : "preset",
									"numinlets" : 1,
									"numoutlets" : 5,
									"outlettype" : [ "preset", "int", "preset", "int", "" ],
									"patching_rect" : [ 685.469821849061191, 621.985275546364392, 196.999999976562549, 29.000000000000114 ]
								}

							}
, 							{
								"box" : 								{
									"autorestore" : "preset.json",
									"id" : "obj-231",
									"linecount" : 2,
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 685.469821825623512, 550.485275546364392, 235.000000000000114, 35.0 ],
									"saved_object_attributes" : 									{
										"client_rect" : [ 100, 100, 500, 600 ],
										"parameter_enable" : 0,
										"parameter_mappable" : 0,
										"storage_rect" : [ 200, 200, 800, 500 ]
									}
,
									"text" : "pattrstorage amelia @savemode 1 @autorestore 1 @changemode 1",
									"varname" : "amelia"
								}

							}
, 							{
								"box" : 								{
									"angle" : 270.0,
									"bgcolor" : [ 0.2, 0.2, 0.2, 0.0 ],
									"border" : 1,
									"bordercolor" : [ 0.0, 0.0, 0.0, 1.0 ],
									"id" : "obj-207",
									"maxclass" : "panel",
									"mode" : 0,
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 676.969821837342352, 352.985275546364846, 256.999999976562549, 305.0 ],
									"proportion" : 0.5,
									"rounded" : 0
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-79",
									"index" : 1,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"patching_rect" : [ 50.250015049174294, 39.999999697650907, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-80",
									"index" : 2,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"patching_rect" : [ 114.250015049174294, 39.999999697650907, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-81",
									"index" : 1,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 50.0, 508.920112244015286, 30.0, 30.0 ]
								}

							}
 ],
						"lines" : [ 							{
								"patchline" : 								{
									"destination" : [ "obj-165", 2 ],
									"midpoints" : [ 238.766662418842316, 222.73412427162566, 237.099999964237213, 222.73412427162566 ],
									"source" : [ "obj-113", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-162", 0 ],
									"source" : [ "obj-163", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-1", 0 ],
									"order" : 0,
									"source" : [ "obj-165", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-163", 0 ],
									"order" : 1,
									"source" : [ "obj-165", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-4", 1 ],
									"order" : 0,
									"source" : [ "obj-169", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-4", 0 ],
									"order" : 2,
									"source" : [ "obj-169", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-72", 0 ],
									"order" : 1,
									"source" : [ "obj-169", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-177", 1 ],
									"midpoints" : [ 736.830234423279762, 417.985275546364392, 735.969821825623512, 417.985275546364392 ],
									"source" : [ "obj-173", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-231", 0 ],
									"midpoints" : [ 834.969821825623512, 528.985275546364846, 694.969821825623512, 528.985275546364846 ],
									"source" : [ "obj-175", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-231", 0 ],
									"midpoints" : [ 694.969821825623512, 495.985275546364846, 694.969821825623512, 495.985275546364846 ],
									"source" : [ "obj-177", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-231", 0 ],
									"midpoints" : [ 900.969821825623512, 528.985275546364846, 694.969821825623512, 528.985275546364846 ],
									"source" : [ "obj-179", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-231", 0 ],
									"midpoints" : [ 731.969821825623512, 528.985275546364846, 694.969821825623512, 528.985275546364846 ],
									"source" : [ "obj-182", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-15", 1 ],
									"order" : 0,
									"source" : [ "obj-225", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-177", 0 ],
									"midpoints" : [ 694.969821825623512, 417.985275546364392, 694.969821825623512, 417.985275546364392 ],
									"order" : 1,
									"source" : [ "obj-225", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-231", 0 ],
									"midpoints" : [ 776.719821825623512, 528.985275546364846, 694.969821825623512, 528.985275546364846 ],
									"source" : [ "obj-226", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-228", 0 ],
									"midpoints" : [ 777.830234423279762, 417.985275546364392, 777.830234423279762, 417.985275546364392 ],
									"source" : [ "obj-227", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-177", 2 ],
									"midpoints" : [ 777.830234423279762, 468.985275546364846, 776.969821825623512, 468.985275546364846 ],
									"order" : 0,
									"source" : [ "obj-228", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-227", 0 ],
									"midpoints" : [ 777.830234423279762, 456.985275546364846, 819.469821802185834, 456.985275546364846, 819.469821802185834, 381.985275546364846, 777.830234423279762, 381.985275546364846 ],
									"order" : 1,
									"source" : [ "obj-228", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-230", 0 ],
									"midpoints" : [ 694.969821825623512, 615.985275546364846, 694.969821849061191, 615.985275546364846 ],
									"source" : [ "obj-229", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-229", 0 ],
									"midpoints" : [ 694.969821825623512, 585.985275546364846, 694.969821825623512, 585.985275546364846 ],
									"source" : [ "obj-231", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-225", 0 ],
									"order" : 1,
									"source" : [ "obj-274", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-275", 0 ],
									"order" : 0,
									"source" : [ "obj-274", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-225", 0 ],
									"source" : [ "obj-275", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-165", 2 ],
									"source" : [ "obj-28", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-28", 0 ],
									"source" : [ "obj-30", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-169", 0 ],
									"source" : [ "obj-31", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-31", 0 ],
									"source" : [ "obj-33", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-31", 0 ],
									"source" : [ "obj-35", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-165", 0 ],
									"source" : [ "obj-4", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-81", 0 ],
									"source" : [ "obj-72", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-35", 0 ],
									"source" : [ "obj-79", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-33", 0 ],
									"source" : [ "obj-80", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-113", 0 ],
									"order" : 1,
									"source" : [ "obj-83", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-30", 0 ],
									"order" : 0,
									"source" : [ "obj-83", 0 ]
								}

							}
 ]
					}
,
					"patching_rect" : [ 69.980087950825691, 930.335540967701036, 156.0, 22.0 ],
					"saved_object_attributes" : 					{
						"description" : "",
						"digest" : "",
						"globalpatchername" : "",
						"tags" : ""
					}
,
					"text" : "p MachineLearning_Control",
					"varname" : "patcher"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-76",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 108.0, 838.406969539129591, 61.0, 20.0 ],
					"text" : "3 minutes"
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.482352941176471, 0.482352941176471, 0.482352941176471, 1.0 ],
					"elementcolor" : [ 0.831372549019608, 0.831372549019608, 0.831372549019608, 1.0 ],
					"id" : "obj-69",
					"knobcolor" : [ 0.266666666666667, 0.552941176470588, 0.831372549019608, 1.0 ],
					"maxclass" : "slider",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 69.980087950825691, 954.671081935402071, 151.496762558817863, 28.0 ]
				}

			}
, 			{
				"box" : 				{
					"downarrow" : 0,
					"id" : "obj-52",
					"leftarrow" : 0,
					"maxclass" : "live.arrows",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 159.980087950825691, 860.406969539129591, 33.245021987706423, 28.928571428571427 ],
					"uparrow" : 0
				}

			}
, 			{
				"box" : 				{
					"downarrow" : 0,
					"id" : "obj-47",
					"maxclass" : "live.arrows",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 91.35757695697248, 860.406969539129591, 33.245021987706423, 28.928571428571427 ],
					"rightarrow" : 0,
					"uparrow" : 0
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-24",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "multichannelsignal" ],
					"patching_rect" : [ 392.0, 730.33332622051239, 92.0, 22.0 ],
					"text" : "mc.mixdown~ 2"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-8",
					"linecount" : 2,
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 279.5, 243.835540967701036, 116.0, 33.0 ],
					"text" : "Test different states of each variable:"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-42",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 159.980087950825691, 897.233108069747686, 24.0, 24.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-37",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 95.980087950825691, 897.233108069747686, 24.0, 24.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-11",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 572.0, 79.0, 58.0, 22.0 ],
					"text" : "loadbang"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-10",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 572.0, 110.0, 67.0, 22.0 ],
					"text" : "channels 4"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-6",
					"maxclass" : "newobj",
					"numinlets" : 4,
					"numoutlets" : 1,
					"outlettype" : [ "multichannelsignal" ],
					"patching_rect" : [ 392.0, 117.333328664302826, 70.0, 22.0 ],
					"text" : "mc.pack~ 4"
				}

			}
, 			{
				"box" : 				{
					"basictuning" : 440,
					"clipheight" : 55.0,
					"data" : 					{
						"clips" : [ 							{
								"absolutepath" : "/Users/atau/Library/CloudStorage/Dropbox/projmusic30/eramaa/audio/nov23/bassclar-01.wav",
								"filename" : "bassclar-01.wav",
								"filekind" : "audiofile",
								"id" : "u532004044",
								"selection" : [ 0.314935064935065, 0.454545454545455 ],
								"loop" : 1,
								"content_state" : 								{
									"loop" : 1
								}

							}
 ]
					}
,
					"followglobaltempo" : 0,
					"formantcorrection" : 0,
					"id" : "obj-5",
					"maxclass" : "playlist~",
					"mode" : "basic",
					"numinlets" : 1,
					"numoutlets" : 5,
					"originallength" : [ 0.0, "ticks" ],
					"originaltempo" : 120.0,
					"outlettype" : [ "signal", "signal", "signal", "", "dictionary" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 159.980087950825691, 25.833328664302826, 363.0, 55.0 ],
					"pitchcorrection" : 0,
					"quality" : "basic",
					"timestretch" : [ 0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-239",
					"maxclass" : "newobj",
					"numinlets" : 33,
					"numoutlets" : 0,
					"patcher" : 					{
						"fileversion" : 1,
						"appversion" : 						{
							"major" : 8,
							"minor" : 5,
							"revision" : 6,
							"architecture" : "x64",
							"modernui" : 1
						}
,
						"classnamespace" : "box",
						"rect" : [ 34.0, 100.0, 1444.0, 848.0 ],
						"bglocked" : 0,
						"openinpresentation" : 0,
						"default_fontsize" : 12.0,
						"default_fontface" : 0,
						"default_fontname" : "Arial",
						"gridonopen" : 1,
						"gridsize" : [ 15.0, 15.0 ],
						"gridsnaponopen" : 1,
						"objectsnaponopen" : 1,
						"statusbarvisible" : 2,
						"toolbarvisible" : 1,
						"lefttoolbarpinned" : 0,
						"toptoolbarpinned" : 0,
						"righttoolbarpinned" : 0,
						"bottomtoolbarpinned" : 0,
						"toolbars_unpinned_last_save" : 0,
						"tallnewobj" : 0,
						"boxanimatetime" : 200,
						"enablehscroll" : 1,
						"enablevscroll" : 1,
						"devicewidth" : 0.0,
						"description" : "",
						"digest" : "",
						"tags" : "",
						"style" : "",
						"subpatcher_template" : "",
						"assistshowspatchername" : 0,
						"boxes" : [ 							{
								"box" : 								{
									"id" : "obj-155",
									"linecount" : 18,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 385.0, 3481.130757234626799, 131.0, 263.0 ],
									"text" : ";\rinstr transpout 10.;\rinstr feedback 1.;\rinstr gain 1.;\rinstr variability 0.7;\rinstr transpgrain 0.;\rinstr modfreqmod 0.5;\rinstr modmorph 0.;\rinstr modfreq 976.;\rinstr modfactor 0.5;\rinstr spacing 19;\rinstr grainoffset 85;\rinstr grainsize 53;\rinstr maxdelay 2000.;\rinstr lpffreq 20000.;\rinstr hpffreq 20.;\rinstr grainenvmorph 1.;\rinstr indexdistr 0;\r"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-154",
									"linecount" : 18,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 217.0, 3481.130757234626799, 131.0, 263.0 ],
									"text" : ";\rinstr transpout -10.;\rinstr feedback 1.;\rinstr gain 1.;\rinstr variability 0.7;\rinstr transpgrain 0.;\rinstr modfreqmod 0.5;\rinstr modmorph 0.;\rinstr modfreq 976.;\rinstr modfactor 0.5;\rinstr spacing 19;\rinstr grainoffset 85;\rinstr grainsize 53;\rinstr maxdelay 2000.;\rinstr lpffreq 20000.;\rinstr hpffreq 20.;\rinstr grainenvmorph 1.;\rinstr indexdistr 0;\r"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-150",
									"linecount" : 18,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 50.0, 3481.130757234626799, 131.0, 263.0 ],
									"text" : ";\rinstr transpout 0.;\rinstr feedback 1.;\rinstr gain 1.;\rinstr variability 0.7;\rinstr transpgrain 0.;\rinstr modfreqmod 0.5;\rinstr modmorph 0.;\rinstr modfreq 976.;\rinstr modfactor 0.5;\rinstr spacing 19;\rinstr grainoffset 85;\rinstr grainsize 53;\rinstr maxdelay 2000.;\rinstr lpffreq 20000.;\rinstr hpffreq 20.;\rinstr grainenvmorph 1.;\rinstr indexdistr 0;\r"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-139",
									"linecount" : 18,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 215.0, 3193.97676032781601, 131.0, 263.0 ],
									"text" : ";\rinstr transpout 0.;\rinstr feedback 1.;\rinstr gain 1.;\rinstr variability 0.7;\rinstr transpgrain 0.;\rinstr modfreqmod 0.5;\rinstr modmorph 0.;\rinstr modfreq 976.;\rinstr modfactor 0.5;\rinstr spacing 19;\rinstr grainoffset 85;\rinstr grainsize 53;\rinstr maxdelay 2000.;\rinstr lpffreq 20000.;\rinstr hpffreq 20.;\rinstr grainenvmorph 1.;\rinstr indexdistr 0;\r"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-138",
									"linecount" : 18,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 50.0, 3193.97676032781601, 131.0, 263.0 ],
									"text" : ";\rinstr transpout 0.;\rinstr feedback 0.;\rinstr gain 1.;\rinstr variability 0.7;\rinstr transpgrain 0.;\rinstr modfreqmod 0.5;\rinstr modmorph 0.;\rinstr modfreq 976.;\rinstr modfactor 0.5;\rinstr spacing 19;\rinstr grainoffset 85;\rinstr grainsize 53;\rinstr maxdelay 2000.;\rinstr lpffreq 20000.;\rinstr hpffreq 20.;\rinstr grainenvmorph 1.;\rinstr indexdistr 0;\r"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-133",
									"linecount" : 18,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 216.614235069389679, 2918.5, 131.0, 263.0 ],
									"text" : ";\rinstr transpout 0.;\rinstr feedback 0.;\rinstr gain 1.;\rinstr variability 0.7;\rinstr transpgrain -10.;\rinstr modfreqmod 0.5;\rinstr modmorph 0.;\rinstr modfreq 976.;\rinstr modfactor 0.5;\rinstr spacing 19;\rinstr grainoffset 85;\rinstr grainsize 53;\rinstr maxdelay 2000.;\rinstr lpffreq 20000.;\rinstr hpffreq 20.;\rinstr grainenvmorph 1.;\rinstr indexdistr 0;\r"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-132",
									"linecount" : 18,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 50.0, 2918.5, 131.0, 263.0 ],
									"text" : ";\rinstr transpout 0.;\rinstr feedback 0.;\rinstr gain 1.;\rinstr variability 0.7;\rinstr transpgrain 0.;\rinstr modfreqmod 0.5;\rinstr modmorph 0.;\rinstr modfreq 976.;\rinstr modfactor 0.5;\rinstr spacing 19;\rinstr grainoffset 85;\rinstr grainsize 53;\rinstr maxdelay 2000.;\rinstr lpffreq 20000.;\rinstr hpffreq 20.;\rinstr grainenvmorph 1.;\rinstr indexdistr 0;\r"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-131",
									"linecount" : 18,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 378.414306219454147, 2918.653996906810789, 131.0, 263.0 ],
									"text" : ";\rinstr transpout 0.;\rinstr feedback 0.;\rinstr gain 1.;\rinstr variability 0.7;\rinstr transpgrain 10.;\rinstr modfreqmod 0.5;\rinstr modmorph 0.;\rinstr modfreq 976.;\rinstr modfactor 0.5;\rinstr spacing 19;\rinstr grainoffset 85;\rinstr grainsize 53;\rinstr maxdelay 2000.;\rinstr lpffreq 20000.;\rinstr hpffreq 20.;\rinstr grainenvmorph 1.;\rinstr indexdistr 0;\r"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-117",
									"linecount" : 18,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 390.0, 2641.0, 131.0, 263.0 ],
									"text" : ";\rinstr transpout 0.;\rinstr feedback 0.;\rinstr gain 1.;\rinstr variability 0.7;\rinstr transpgrain 0.;\rinstr modfreqmod 1.;\rinstr modmorph 0.;\rinstr modfreq 976.;\rinstr modfactor 1.;\rinstr spacing 19;\rinstr grainoffset 85;\rinstr grainsize 53;\rinstr maxdelay 2000.;\rinstr lpffreq 20000.;\rinstr hpffreq 20.;\rinstr grainenvmorph 1.;\rinstr indexdistr 0;\r"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-115",
									"linecount" : 18,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 223.0, 2641.0, 131.0, 263.0 ],
									"text" : ";\rinstr transpout 0.;\rinstr feedback 0.;\rinstr gain 1.;\rinstr variability 0.7;\rinstr transpgrain 0.;\rinstr modfreqmod 0.;\rinstr modmorph 0.;\rinstr modfreq 976.;\rinstr modfactor 1.;\rinstr spacing 19;\rinstr grainoffset 85;\rinstr grainsize 53;\rinstr maxdelay 2000.;\rinstr lpffreq 20000.;\rinstr hpffreq 20.;\rinstr grainenvmorph 1.;\rinstr indexdistr 0;\r"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-111",
									"linecount" : 18,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 50.0, 2641.0, 131.0, 263.0 ],
									"text" : ";\rinstr transpout 0.;\rinstr feedback 0.;\rinstr gain 1.;\rinstr variability 0.7;\rinstr transpgrain 0.;\rinstr modfreqmod 0.5;\rinstr modmorph 0.;\rinstr modfreq 976.;\rinstr modfactor 1.;\rinstr spacing 19;\rinstr grainoffset 85;\rinstr grainsize 53;\rinstr maxdelay 2000.;\rinstr lpffreq 20000.;\rinstr hpffreq 20.;\rinstr grainenvmorph 1.;\rinstr indexdistr 0;\r"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-101",
									"linecount" : 18,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 369.0, 2359.97676032781601, 131.0, 263.0 ],
									"text" : ";\rinstr transpout 0.;\rinstr feedback 0.;\rinstr gain 1.;\rinstr variability 0.7;\rinstr transpgrain 0.;\rinstr modfreqmod 0.5;\rinstr modmorph 2.;\rinstr modfreq 976.;\rinstr modfactor 1.;\rinstr spacing 19;\rinstr grainoffset 85;\rinstr grainsize 53;\rinstr maxdelay 2000.;\rinstr lpffreq 20000.;\rinstr hpffreq 20.;\rinstr grainenvmorph 1.;\rinstr indexdistr 0;\r"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-96",
									"linecount" : 18,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 211.0, 2359.97676032781601, 131.0, 263.0 ],
									"text" : ";\rinstr transpout 0.;\rinstr feedback 0.;\rinstr gain 1.;\rinstr variability 0.7;\rinstr transpgrain 0.;\rinstr modfreqmod 0.5;\rinstr modmorph 1.;\rinstr modfreq 976.;\rinstr modfactor 1.;\rinstr spacing 19;\rinstr grainoffset 85;\rinstr grainsize 53;\rinstr maxdelay 2000.;\rinstr lpffreq 20000.;\rinstr hpffreq 20.;\rinstr grainenvmorph 1.;\rinstr indexdistr 0;\r"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-95",
									"linecount" : 18,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 50.0, 2359.97676032781601, 131.0, 263.0 ],
									"text" : ";\rinstr transpout 0.;\rinstr feedback 0.;\rinstr gain 1.;\rinstr variability 0.7;\rinstr transpgrain 0.;\rinstr modfreqmod 0.5;\rinstr modmorph 0.;\rinstr modfreq 976.;\rinstr modfactor 1.;\rinstr spacing 19;\rinstr grainoffset 85;\rinstr grainsize 53;\rinstr maxdelay 2000.;\rinstr lpffreq 20000.;\rinstr hpffreq 20.;\rinstr grainenvmorph 1.;\rinstr indexdistr 0;\r"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-86",
									"linecount" : 18,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 706.0, 2083.0, 131.0, 263.0 ],
									"text" : ";\rinstr transpout 0.;\rinstr feedback 0.;\rinstr gain 1.;\rinstr variability 0.7;\rinstr transpgrain 0.;\rinstr modfreqmod 0.5;\rinstr modmorph 0.;\rinstr modfreq 9108.;\rinstr modfactor 1.;\rinstr spacing 19;\rinstr grainoffset 85;\rinstr grainsize 53;\rinstr maxdelay 2000.;\rinstr lpffreq 20000.;\rinstr hpffreq 20.;\rinstr grainenvmorph 1.;\rinstr indexdistr 0;\r"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-84",
									"linecount" : 18,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 544.411071371074058, 2083.0, 131.0, 263.0 ],
									"text" : ";\rinstr transpout 0.;\rinstr feedback 0.;\rinstr gain 1.;\rinstr variability 0.7;\rinstr transpgrain 0.;\rinstr modfreqmod 0.5;\rinstr modmorph 0.;\rinstr modfreq 1889.;\rinstr modfactor 1.;\rinstr spacing 19;\rinstr grainoffset 85;\rinstr grainsize 53;\rinstr maxdelay 2000.;\rinstr lpffreq 20000.;\rinstr hpffreq 20.;\rinstr grainenvmorph 1.;\rinstr indexdistr 0;\r"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-82",
									"linecount" : 18,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 376.0, 2083.0, 131.0, 263.0 ],
									"text" : ";\rinstr transpout 0.;\rinstr feedback 0.;\rinstr gain 1.;\rinstr variability 0.7;\rinstr transpgrain 0.;\rinstr modfreqmod 0.5;\rinstr modmorph 0.;\rinstr modfreq 976.;\rinstr modfactor 1.;\rinstr spacing 19;\rinstr grainoffset 85;\rinstr grainsize 53;\rinstr maxdelay 2000.;\rinstr lpffreq 20000.;\rinstr hpffreq 20.;\rinstr grainenvmorph 1.;\rinstr indexdistr 0;\r"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-81",
									"linecount" : 18,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 210.0, 2083.0, 131.0, 263.0 ],
									"text" : ";\rinstr transpout 0.;\rinstr feedback 0.;\rinstr gain 1.;\rinstr variability 0.7;\rinstr transpgrain 0.;\rinstr modfreqmod 0.5;\rinstr modmorph 0.;\rinstr modfreq 206.;\rinstr modfactor 1.;\rinstr spacing 19;\rinstr grainoffset 85;\rinstr grainsize 53;\rinstr maxdelay 2000.;\rinstr lpffreq 20000.;\rinstr hpffreq 20.;\rinstr grainenvmorph 1.;\rinstr indexdistr 0;\r"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-80",
									"linecount" : 18,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 50.0, 2083.0, 131.0, 263.0 ],
									"text" : ";\rinstr transpout 0.;\rinstr feedback 0.;\rinstr gain 1.;\rinstr variability 0.7;\rinstr transpgrain 0.;\rinstr modfreqmod 0.5;\rinstr modmorph 0.;\rinstr modfreq 31.;\rinstr modfactor 1.;\rinstr spacing 19;\rinstr grainoffset 85;\rinstr grainsize 53;\rinstr maxdelay 2000.;\rinstr lpffreq 20000.;\rinstr hpffreq 20.;\rinstr grainenvmorph 1.;\rinstr indexdistr 0;\r"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-76",
									"linecount" : 18,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 239.811071406836845, 1806.97676032781601, 131.0, 263.0 ],
									"text" : ";\rinstr transpout 0.;\rinstr feedback 0.;\rinstr gain 1.;\rinstr variability 0.7;\rinstr transpgrain 0.;\rinstr modfreqmod 0.5;\rinstr modmorph 0.;\rinstr modfreq 1000.;\rinstr modfactor 1.;\rinstr spacing 19;\rinstr grainoffset 85;\rinstr grainsize 53;\rinstr maxdelay 2000.;\rinstr lpffreq 20000.;\rinstr hpffreq 20.;\rinstr grainenvmorph 1.;\rinstr indexdistr 0;\r"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-75",
									"linecount" : 18,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 50.0, 1806.97676032781601, 131.0, 263.0 ],
									"text" : ";\rinstr transpout 0.;\rinstr feedback 0.;\rinstr gain 1.;\rinstr variability 0.7;\rinstr transpgrain 0.;\rinstr modfreqmod 0.5;\rinstr modmorph 0.;\rinstr modfreq 1000.;\rinstr modfactor 0.;\rinstr spacing 19;\rinstr grainoffset 85;\rinstr grainsize 53;\rinstr maxdelay 2000.;\rinstr lpffreq 20000.;\rinstr hpffreq 20.;\rinstr grainenvmorph 1.;\rinstr indexdistr 0;\r"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-74",
									"linecount" : 18,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 208.614235069389679, 1220.97676032781601, 131.0, 263.0 ],
									"text" : ";\rinstr transpout 0.;\rinstr feedback 0.;\rinstr gain 1.;\rinstr variability 1.;\rinstr transpgrain 0.;\rinstr modfreqmod 0.5;\rinstr modmorph 0.;\rinstr modfreq 1000.;\rinstr modfactor 0.;\rinstr spacing 40;\rinstr grainoffset 55;\rinstr grainsize 1;\rinstr maxdelay 2000.;\rinstr lpffreq 20000.;\rinstr hpffreq 20.;\rinstr grainenvmorph 1.;\rinstr indexdistr 0;\r"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-73",
									"linecount" : 18,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 50.0, 1220.97676032781601, 131.0, 263.0 ],
									"text" : ";\rinstr transpout 0.;\rinstr feedback 0.;\rinstr gain 1.;\rinstr variability 1.;\rinstr transpgrain 0.;\rinstr modfreqmod 0.5;\rinstr modmorph 0.;\rinstr modfreq 1000.;\rinstr modfactor 0.;\rinstr spacing 40;\rinstr grainoffset 0;\rinstr grainsize 1;\rinstr maxdelay 2000.;\rinstr lpffreq 20000.;\rinstr hpffreq 20.;\rinstr grainenvmorph 1.;\rinstr indexdistr 0;\r"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-68",
									"linecount" : 18,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 359.41427093350444, 1526.0, 131.0, 263.0 ],
									"text" : ";\rinstr transpout 0.;\rinstr feedback 0.;\rinstr gain 1.;\rinstr variability 0.;\rinstr transpgrain 0.;\rinstr modfreqmod 0.5;\rinstr modmorph 0.;\rinstr modfreq 1000.;\rinstr modfactor 0.;\rinstr spacing 703;\rinstr grainoffset 0;\rinstr grainsize 80;\rinstr maxdelay 2000.;\rinstr lpffreq 20000.;\rinstr hpffreq 20.;\rinstr grainenvmorph 1.;\rinstr indexdistr 0;\r"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-67",
									"linecount" : 18,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 208.0, 1526.0, 131.0, 263.0 ],
									"text" : ";\rinstr transpout 0.;\rinstr feedback 0.;\rinstr gain 1.;\rinstr variability 0.;\rinstr transpgrain 0.;\rinstr modfreqmod 0.5;\rinstr modmorph 0.;\rinstr modfreq 1000.;\rinstr modfactor 0.;\rinstr spacing 160;\rinstr grainoffset 0;\rinstr grainsize 80;\rinstr maxdelay 2000.;\rinstr lpffreq 20000.;\rinstr hpffreq 20.;\rinstr grainenvmorph 1.;\rinstr indexdistr 0;\r"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-66",
									"linecount" : 18,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 50.0, 1526.0, 131.0, 263.0 ],
									"text" : ";\rinstr transpout 0.;\rinstr feedback 0.;\rinstr gain 1.;\rinstr variability 0.;\rinstr transpgrain 0.;\rinstr modfreqmod 0.5;\rinstr modmorph 0.;\rinstr modfreq 1000.;\rinstr modfactor 0.;\rinstr spacing 1;\rinstr grainoffset 0;\rinstr grainsize 80;\rinstr maxdelay 2000.;\rinstr lpffreq 20000.;\rinstr hpffreq 20.;\rinstr grainenvmorph 1.;\rinstr indexdistr 0;\r"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-37",
									"linecount" : 18,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 355.0, 938.0, 131.0, 263.0 ],
									"text" : ";\rinstr transpout 0.;\rinstr feedback 0.;\rinstr gain 1.;\rinstr variability 0.;\rinstr transpgrain 0.;\rinstr modfreqmod 0.5;\rinstr modmorph 0.;\rinstr modfreq 1000.;\rinstr modfactor 0.;\rinstr spacing 40;\rinstr grainoffset 0;\rinstr grainsize 786;\rinstr maxdelay 2000.;\rinstr lpffreq 20000.;\rinstr hpffreq 20.;\rinstr grainenvmorph 1.;\rinstr indexdistr 0;\r"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-28",
									"linecount" : 18,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 200.0, 938.0, 131.0, 263.0 ],
									"text" : ";\rinstr transpout 0.;\rinstr feedback 0.;\rinstr gain 1.;\rinstr variability 0.;\rinstr transpgrain 0.;\rinstr modfreqmod 0.5;\rinstr modmorph 0.;\rinstr modfreq 1000.;\rinstr modfactor 0.;\rinstr spacing 40;\rinstr grainoffset 0;\rinstr grainsize 80;\rinstr maxdelay 2000.;\rinstr lpffreq 20000.;\rinstr hpffreq 20.;\rinstr grainenvmorph 1.;\rinstr indexdistr 0;\r"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-24",
									"linecount" : 18,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 50.0, 938.0, 131.0, 263.0 ],
									"text" : ";\rinstr transpout 0.;\rinstr feedback 0.;\rinstr gain 1.;\rinstr variability 0.;\rinstr transpgrain 0.;\rinstr modfreqmod 0.5;\rinstr modmorph 0.;\rinstr modfreq 1000.;\rinstr modfactor 0.;\rinstr spacing 40;\rinstr grainoffset 0;\rinstr grainsize 2;\rinstr maxdelay 2000.;\rinstr lpffreq 20000.;\rinstr hpffreq 20.;\rinstr grainenvmorph 1.;\rinstr indexdistr 0;\r"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-20",
									"linecount" : 18,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 50.0, 662.5, 131.0, 263.0 ],
									"text" : ";\rinstr transpout 0.;\rinstr feedback 0.;\rinstr gain 1.;\rinstr variability 0.;\rinstr transpgrain 0.;\rinstr modfreqmod 0.5;\rinstr modmorph 0.;\rinstr modfreq 1000.;\rinstr modfactor 0.;\rinstr spacing 40;\rinstr grainoffset 0;\rinstr grainsize 20;\rinstr maxdelay 2000.;\rinstr lpffreq 20000.;\rinstr hpffreq 20.;\rinstr grainenvmorph 1.;\rinstr indexdistr 0;\r"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-18",
									"linecount" : 18,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 50.0, 382.97676032781601, 131.0, 263.0 ],
									"text" : ";\rinstr transpout 0.;\rinstr feedback 0.;\rinstr gain 1.;\rinstr variability 0.;\rinstr transpgrain 0.;\rinstr modfreqmod 0.5;\rinstr modmorph 0.;\rinstr modfreq 1000.;\rinstr modfactor 0.;\rinstr spacing 40;\rinstr grainoffset 0;\rinstr grainsize 20;\rinstr maxdelay 0.;\rinstr lpffreq 7161.;\rinstr hpffreq 2993.;\rinstr grainenvmorph 1.;\rinstr indexdistr 0;\r"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-2",
									"linecount" : 18,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 201.5, 100.0, 137.0, 263.0 ],
									"text" : ";\rinstr transpout 0.;\rinstr feedback 0.;\rinstr gain 1.;\rinstr variability 0.;\rinstr transpgrain 0.;\rinstr modfreqmod 0.5;\rinstr modmorph 0.;\rinstr modfreq 1000.;\rinstr modfactor 0.;\rinstr spacing 40;\rinstr grainoffset 0;\rinstr grainsize 20;\rinstr maxdelay 0.;\rinstr lpffreq 20000.;\rinstr hpffreq 20.;\rinstr grainenvmorph 1.;\rinstr indexdistr 0;\r"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-1",
									"linecount" : 18,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 55.5, 100.0, 137.0, 263.0 ],
									"text" : ";\rinstr transpout 0.;\rinstr feedback 0.;\rinstr gain 1.;\rinstr variability 0.;\rinstr transpgrain 0.;\rinstr modfreqmod 0.5;\rinstr modmorph 0.;\rinstr modfreq 1000.;\rinstr modfactor 0.;\rinstr spacing 40;\rinstr grainoffset 0;\rinstr grainsize 20;\rinstr maxdelay 0.;\rinstr lpffreq 20000.;\rinstr hpffreq 20.;\rinstr grainenvmorph 0.;\rinstr indexdistr 0;\r"
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-168",
									"index" : 1,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"patching_rect" : [ 49.999999933504441, 39.999987983299206, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-170",
									"index" : 2,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"patching_rect" : [ 84.999999933504441, 39.999987983299206, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-172",
									"index" : 3,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"patching_rect" : [ 119.999999933504441, 39.999987983299206, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-181",
									"index" : 4,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"patching_rect" : [ 154.999999933504455, 39.999987983299206, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-183",
									"index" : 5,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"patching_rect" : [ 189.999999933504455, 39.999987983299206, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-184",
									"index" : 6,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"patching_rect" : [ 224.999999933504455, 39.999987983299206, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-185",
									"index" : 7,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"patching_rect" : [ 259.999999933504455, 39.999987983299206, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-186",
									"index" : 8,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"patching_rect" : [ 294.999999933504455, 39.999987983299206, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-187",
									"index" : 9,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"patching_rect" : [ 329.999999933504455, 39.999987983299206, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-188",
									"index" : 10,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"patching_rect" : [ 364.999999933504455, 39.999987983299206, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-189",
									"index" : 11,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"patching_rect" : [ 399.999999933504455, 39.999987983299206, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-190",
									"index" : 12,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"patching_rect" : [ 434.999999933504455, 39.999987983299206, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-191",
									"index" : 13,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"patching_rect" : [ 469.999999933504455, 39.999987983299206, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-192",
									"index" : 14,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"patching_rect" : [ 504.999999933504455, 39.999987983299206, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-193",
									"index" : 15,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"patching_rect" : [ 539.999999933504455, 39.999987983299206, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-194",
									"index" : 16,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"patching_rect" : [ 574.999999933504455, 39.999987983299206, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-195",
									"index" : 17,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"patching_rect" : [ 609.999999933504455, 39.999987983299206, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-196",
									"index" : 18,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"patching_rect" : [ 644.999999933504455, 39.999987983299206, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-198",
									"index" : 19,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"patching_rect" : [ 679.999999933504455, 39.999987983299206, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-204",
									"index" : 20,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"patching_rect" : [ 714.999999933504455, 39.999987983299206, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-212",
									"index" : 21,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"patching_rect" : [ 749.999999933504455, 39.999987983299206, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-215",
									"index" : 22,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"patching_rect" : [ 784.999999933504455, 39.999987983299206, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-216",
									"index" : 23,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"patching_rect" : [ 819.999999933504455, 39.999987983299206, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-217",
									"index" : 24,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"patching_rect" : [ 854.999999933504455, 39.999987983299206, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-218",
									"index" : 25,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"patching_rect" : [ 889.999999933504455, 39.999987983299206, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-219",
									"index" : 26,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"patching_rect" : [ 924.999999933504455, 39.999987983299206, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-221",
									"index" : 27,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"patching_rect" : [ 959.999999933504455, 39.999987983299206, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-222",
									"index" : 28,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"patching_rect" : [ 994.999999933504455, 39.999987983299206, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-223",
									"index" : 29,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"patching_rect" : [ 1029.999999933504569, 39.999987983299206, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-224",
									"index" : 30,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"patching_rect" : [ 1064.999999933504569, 39.999987983299206, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-232",
									"index" : 31,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"patching_rect" : [ 1099.999999933504569, 39.999987983299206, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-237",
									"index" : 32,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"patching_rect" : [ 1134.999999933504569, 39.999987983299206, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-238",
									"index" : 33,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"patching_rect" : [ 1169.999999933504569, 39.999987983299206, 30.0, 30.0 ]
								}

							}
 ],
						"lines" : [ 							{
								"patchline" : 								{
									"destination" : [ "obj-18", 0 ],
									"source" : [ "obj-168", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-20", 0 ],
									"source" : [ "obj-170", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-24", 0 ],
									"source" : [ "obj-172", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-73", 0 ],
									"source" : [ "obj-181", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-66", 0 ],
									"source" : [ "obj-183", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-75", 0 ],
									"source" : [ "obj-184", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-80", 0 ],
									"source" : [ "obj-185", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-95", 0 ],
									"source" : [ "obj-186", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-111", 0 ],
									"source" : [ "obj-187", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-132", 0 ],
									"source" : [ "obj-188", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-138", 0 ],
									"source" : [ "obj-189", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-150", 0 ],
									"source" : [ "obj-190", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-1", 0 ],
									"source" : [ "obj-191", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-28", 0 ],
									"source" : [ "obj-192", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-2", 0 ],
									"source" : [ "obj-193", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-67", 0 ],
									"source" : [ "obj-194", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-74", 0 ],
									"source" : [ "obj-195", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-81", 0 ],
									"source" : [ "obj-196", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-96", 0 ],
									"source" : [ "obj-198", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-139", 0 ],
									"source" : [ "obj-204", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-133", 0 ],
									"source" : [ "obj-212", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-154", 0 ],
									"source" : [ "obj-215", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-115", 0 ],
									"source" : [ "obj-216", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-76", 0 ],
									"source" : [ "obj-217", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-37", 0 ],
									"source" : [ "obj-218", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-68", 0 ],
									"source" : [ "obj-219", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-101", 0 ],
									"source" : [ "obj-221", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-82", 0 ],
									"source" : [ "obj-222", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-131", 0 ],
									"source" : [ "obj-223", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-155", 0 ],
									"source" : [ "obj-224", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-117", 0 ],
									"source" : [ "obj-232", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-84", 0 ],
									"source" : [ "obj-237", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-86", 0 ],
									"source" : [ "obj-238", 0 ]
								}

							}
 ]
					}
,
					"patching_rect" : [ 321.714285714285722, 692.835540967701263, 66.285714285714292, 22.0 ],
					"saved_object_attributes" : 					{
						"description" : "",
						"digest" : "",
						"globalpatchername" : "",
						"tags" : ""
					}
,
					"text" : "p states"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-156",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 364.0, 644.835540967701036, 24.0, 24.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-166",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 338.0, 644.835540967701036, 24.0, 24.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-167",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 312.0, 645.835540967701036, 24.0, 24.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-142",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 334.0, 617.835540967701036, 24.0, 24.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-140",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 364.0, 617.835540967701036, 24.0, 24.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-134",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 364.0, 557.835540967701036, 24.0, 24.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-136",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 338.0, 557.835540967701036, 24.0, 24.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-137",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 312.0, 558.835540967701036, 24.0, 24.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-120",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 364.0, 529.835540967701036, 24.0, 24.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-122",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 338.0, 529.835540967701036, 24.0, 24.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-123",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 313.0, 529.835540967701036, 24.0, 24.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-103",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 364.0, 503.835540967701036, 24.0, 24.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-105",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 338.0, 503.835540967701036, 24.0, 24.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-107",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 313.0, 503.835540967701036, 24.0, 24.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-91",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 313.0, 478.835540967701036, 24.0, 24.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-92",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 287.0, 478.835540967701036, 24.0, 24.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-93",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 257.0, 478.835540967701036, 24.0, 24.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-88",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 364.0, 478.835540967701036, 24.0, 24.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-90",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 339.0, 478.835540967701036, 24.0, 24.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-77",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 364.0, 454.835540967701036, 24.0, 24.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-78",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 338.0, 454.835540967701036, 24.0, 24.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-70",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 364.0, 403.835540967701036, 24.0, 24.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-71",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 338.0, 403.835540967701036, 24.0, 24.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-56",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 364.0, 429.835540967701036, 24.0, 24.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-59",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 338.0, 429.835540967701036, 24.0, 24.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-60",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 312.0, 429.835540967701036, 24.0, 24.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-27",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 364.0, 377.835540967701036, 24.0, 24.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-26",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 338.0, 377.835540967701036, 24.0, 24.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-22",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 312.0, 377.835540967701036, 24.0, 24.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-21",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 364.0, 350.835540967701036, 24.0, 24.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-19",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 364.0, 316.835540967701036, 24.0, 24.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-9",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 364.0, 281.835540967701036, 24.0, 24.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-7",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 335.0, 281.835540967701036, 24.0, 24.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-159",
					"maxclass" : "meter~",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "float" ],
					"patching_rect" : [ 488.0, 730.33332622051239, 59.0, 62.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-124",
					"linecount" : 19,
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 472.105525851249695, 795.333328664302826, 157.894474148750305, 277.0 ],
					"text" : ";\rbassclar transpout 0.;\rbassclar feedback 0.2;\rbassclar gain 1.;\rbassclar variability 0.3;\rbassclar transpgrain 0.;\rbassclar modfreqmod 0.5;\rbassclar modmorph 0.;\rbassclar modfreq 500.;\rbassclar modfactor 0.;\rbassclar spacing 80;\rbassclar grainoffset 40;\rbassclar grainsize 40;\rbassclar maxdelay 2000.;\rbassclar lpffreq 20000.;\rbassclar hpffreq 20.;\rbassclar grainenvmorph 0.5;\rbassclar indexdistr 0;\r"
				}

			}
, 			{
				"box" : 				{
					"args" : [ "bassclar", "@Channels", 7 ],
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-160",
					"lockeddragscroll" : 0,
					"lockedsize" : 0,
					"maxclass" : "bpatcher",
					"name" : "bbdmi_live_granulator~.maxpat",
					"numinlets" : 2,
					"numoutlets" : 3,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "multichannelsignal", "", "" ],
					"patching_rect" : [ 392.0, 143.835540967701036, 199.0, 564.0 ],
					"varname" : "bbdmi_live.granulator~[1]",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"angle" : 270.0,
					"bgcolor" : [ 0.972549019607843, 0.807843137254902, 1.0, 1.0 ],
					"id" : "obj-86",
					"maxclass" : "panel",
					"mode" : 0,
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 40.0, 833.335540967701036, 211.0, 391.0 ],
					"proportion" : 0.5
				}

			}
 ],
		"lines" : [ 			{
				"patchline" : 				{
					"destination" : [ "obj-2", 1 ],
					"order" : 0,
					"source" : [ "obj-1", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-2", 0 ],
					"order" : 0,
					"source" : [ "obj-1", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-36", 1 ],
					"order" : 1,
					"source" : [ "obj-1", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-36", 0 ],
					"order" : 1,
					"source" : [ "obj-1", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-160", 1 ],
					"source" : [ "obj-10", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-239", 26 ],
					"source" : [ "obj-103", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-239", 18 ],
					"source" : [ "obj-105", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-239", 7 ],
					"source" : [ "obj-107", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-10", 0 ],
					"source" : [ "obj-11", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-32", 1 ],
					"source" : [ "obj-12", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-32", 0 ],
					"source" : [ "obj-12", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-239", 30 ],
					"source" : [ "obj-120", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-239", 22 ],
					"source" : [ "obj-122", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-239", 8 ],
					"source" : [ "obj-123", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-239", 28 ],
					"source" : [ "obj-134", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-239", 20 ],
					"source" : [ "obj-136", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-239", 9 ],
					"source" : [ "obj-137", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-32", 1 ],
					"order" : 0,
					"source" : [ "obj-14", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-32", 0 ],
					"order" : 1,
					"source" : [ "obj-14", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-239", 19 ],
					"source" : [ "obj-140", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-239", 10 ],
					"source" : [ "obj-142", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-94", 0 ],
					"midpoints" : [ 1559.5, 251.579035327118106, 1568.5, 251.579035327118106 ],
					"source" : [ "obj-15", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-239", 29 ],
					"source" : [ "obj-156", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-124", 1 ],
					"order" : 0,
					"source" : [ "obj-160", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-159", 0 ],
					"order" : 0,
					"source" : [ "obj-160", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-24", 0 ],
					"order" : 1,
					"source" : [ "obj-160", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-55", 0 ],
					"order" : 1,
					"source" : [ "obj-160", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-239", 21 ],
					"source" : [ "obj-166", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-239", 11 ],
					"source" : [ "obj-167", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-4", 1 ],
					"source" : [ "obj-17", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-4", 0 ],
					"source" : [ "obj-17", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-12", 1 ],
					"source" : [ "obj-18", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-12", 0 ],
					"source" : [ "obj-18", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-239", 0 ],
					"source" : [ "obj-19", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-6", 3 ],
					"order" : 0,
					"source" : [ "obj-2", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-6", 2 ],
					"order" : 1,
					"source" : [ "obj-2", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-6", 1 ],
					"order" : 0,
					"source" : [ "obj-2", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-6", 0 ],
					"order" : 1,
					"source" : [ "obj-2", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-18", 0 ],
					"source" : [ "obj-20", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-239", 1 ],
					"source" : [ "obj-21", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-239", 2 ],
					"source" : [ "obj-22", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-30", 0 ],
					"source" : [ "obj-23", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-17", 0 ],
					"source" : [ "obj-24", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-14", 1 ],
					"source" : [ "obj-25", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-14", 0 ],
					"order" : 4,
					"source" : [ "obj-25", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-23", 3 ],
					"order" : 0,
					"source" : [ "obj-25", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-23", 2 ],
					"order" : 1,
					"source" : [ "obj-25", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-23", 1 ],
					"order" : 2,
					"source" : [ "obj-25", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-23", 0 ],
					"order" : 3,
					"source" : [ "obj-25", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-239", 13 ],
					"source" : [ "obj-26", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-239", 24 ],
					"source" : [ "obj-27", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-32", 1 ],
					"source" : [ "obj-3", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-32", 0 ],
					"source" : [ "obj-3", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-16", 0 ],
					"order" : 1,
					"source" : [ "obj-30", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-20", 0 ],
					"order" : 1,
					"source" : [ "obj-30", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-28", 0 ],
					"order" : 0,
					"source" : [ "obj-30", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-29", 1 ],
					"order" : 0,
					"source" : [ "obj-30", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-32", 1 ],
					"source" : [ "obj-36", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-32", 0 ],
					"source" : [ "obj-36", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-85", 0 ],
					"source" : [ "obj-37", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-32", 1 ],
					"source" : [ "obj-4", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-32", 0 ],
					"source" : [ "obj-4", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-85", 1 ],
					"source" : [ "obj-42", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-54", 0 ],
					"source" : [ "obj-45", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-37", 0 ],
					"source" : [ "obj-47", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-3", 1 ],
					"source" : [ "obj-5", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-3", 0 ],
					"order" : 4,
					"source" : [ "obj-5", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-6", 3 ],
					"order" : 0,
					"source" : [ "obj-5", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-6", 2 ],
					"order" : 1,
					"source" : [ "obj-5", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-6", 1 ],
					"order" : 2,
					"source" : [ "obj-5", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-6", 0 ],
					"order" : 3,
					"source" : [ "obj-5", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-42", 0 ],
					"source" : [ "obj-52", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-2", 0 ],
					"source" : [ "obj-54", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-36", 0 ],
					"source" : [ "obj-54", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-239", 25 ],
					"source" : [ "obj-56", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-239", 15 ],
					"source" : [ "obj-59", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-160", 0 ],
					"source" : [ "obj-6", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-239", 4 ],
					"source" : [ "obj-60", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-239", 12 ],
					"source" : [ "obj-7", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-239", 16 ],
					"source" : [ "obj-70", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-239", 3 ],
					"source" : [ "obj-71", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-239", 23 ],
					"source" : [ "obj-77", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-239", 5 ],
					"source" : [ "obj-78", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-213", 0 ],
					"source" : [ "obj-85", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-69", 0 ],
					"source" : [ "obj-85", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-239", 32 ],
					"source" : [ "obj-88", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-239", 14 ],
					"source" : [ "obj-9", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-239", 31 ],
					"source" : [ "obj-90", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-239", 27 ],
					"source" : [ "obj-91", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-239", 17 ],
					"source" : [ "obj-92", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-239", 6 ],
					"source" : [ "obj-93", 0 ]
				}

			}
 ],
		"parameters" : 		{
			"obj-12" : [ "live.gain~[4]", "live.gain~", 0 ],
			"obj-14" : [ "live.gain~[5]", "live.gain~", 0 ],
			"obj-160::obj-101" : [ "number[7]", "number[7]", 0 ],
			"obj-160::obj-104" : [ "number[5]", "number[5]", 0 ],
			"obj-160::obj-109" : [ "number[6]", "number[6]", 0 ],
			"obj-160::obj-113" : [ "number[9]", "number[9]", 0 ],
			"obj-160::obj-118" : [ "number[2]", "number[2]", 0 ],
			"obj-160::obj-123" : [ "number[3]", "number[3]", 0 ],
			"obj-160::obj-28" : [ "number[12]", "number[12]", 0 ],
			"obj-160::obj-43" : [ "number[14]", "number[4]", 0 ],
			"obj-160::obj-44" : [ "number[13]", "number[13]", 0 ],
			"obj-160::obj-61" : [ "number[15]", "number[9]", 0 ],
			"obj-160::obj-68" : [ "number[4]", "number[4]", 0 ],
			"obj-160::obj-72" : [ "number[11]", "number[11]", 0 ],
			"obj-160::obj-77" : [ "number[10]", "number[10]", 0 ],
			"obj-160::obj-90" : [ "number", "number", 0 ],
			"obj-160::obj-93" : [ "number[8]", "number[8]", 0 ],
			"obj-160::obj-97" : [ "number[1]", "number[1]", 0 ],
			"obj-2" : [ "live.gain~", "live.gain~", 0 ],
			"obj-3" : [ "live.gain~[2]", "live.gain~", 0 ],
			"obj-30::obj-101" : [ "number[23]", "number[7]", 0 ],
			"obj-30::obj-104" : [ "number[21]", "number[5]", 0 ],
			"obj-30::obj-109" : [ "number[30]", "number[6]", 0 ],
			"obj-30::obj-113" : [ "number[17]", "number[9]", 0 ],
			"obj-30::obj-118" : [ "number[27]", "number[2]", 0 ],
			"obj-30::obj-123" : [ "number[19]", "number[3]", 0 ],
			"obj-30::obj-28" : [ "number[25]", "number[12]", 0 ],
			"obj-30::obj-43" : [ "number[29]", "number[4]", 0 ],
			"obj-30::obj-44" : [ "number[28]", "number[13]", 0 ],
			"obj-30::obj-61" : [ "number[24]", "number[9]", 0 ],
			"obj-30::obj-68" : [ "number[20]", "number[4]", 0 ],
			"obj-30::obj-72" : [ "number[18]", "number[11]", 0 ],
			"obj-30::obj-77" : [ "number[16]", "number[10]", 0 ],
			"obj-30::obj-90" : [ "number[26]", "number", 0 ],
			"obj-30::obj-93" : [ "number[22]", "number[8]", 0 ],
			"obj-30::obj-97" : [ "number[31]", "number[1]", 0 ],
			"obj-36" : [ "live.gain~[1]", "live.gain~", 0 ],
			"obj-4" : [ "live.gain~[3]", "live.gain~", 0 ],
			"obj-85::obj-162::obj-107::obj-27::obj-18" : [ "toggle[33]", "toggle", 0 ],
			"obj-85::obj-162::obj-107::obj-48" : [ "SendTo-TXT[51]", "SendTo-TXT", 0 ],
			"obj-85::obj-162::obj-107::obj-8" : [ "tab[18]", "tab[1]", 0 ],
			"obj-85::obj-162::obj-123::obj-27::obj-18" : [ "toggle[31]", "toggle", 0 ],
			"obj-85::obj-162::obj-123::obj-48" : [ "SendTo-TXT[49]", "SendTo-TXT", 0 ],
			"obj-85::obj-162::obj-123::obj-8" : [ "tab[16]", "tab[1]", 0 ],
			"obj-85::obj-162::obj-34::obj-27::obj-18" : [ "toggle[34]", "toggle", 0 ],
			"obj-85::obj-162::obj-34::obj-48" : [ "SendTo-TXT[52]", "SendTo-TXT", 0 ],
			"obj-85::obj-162::obj-34::obj-8" : [ "tab[19]", "tab[1]", 0 ],
			"obj-85::obj-162::obj-36::obj-27::obj-18" : [ "toggle[35]", "toggle", 0 ],
			"obj-85::obj-162::obj-36::obj-48" : [ "SendTo-TXT[53]", "SendTo-TXT", 0 ],
			"obj-85::obj-162::obj-36::obj-8" : [ "tab[20]", "tab[1]", 0 ],
			"obj-85::obj-162::obj-40::obj-27::obj-18" : [ "toggle[36]", "toggle", 0 ],
			"obj-85::obj-162::obj-40::obj-48" : [ "SendTo-TXT[54]", "SendTo-TXT", 0 ],
			"obj-85::obj-162::obj-40::obj-8" : [ "tab[21]", "tab[1]", 0 ],
			"obj-85::obj-162::obj-41::obj-27::obj-18" : [ "toggle[37]", "toggle", 0 ],
			"obj-85::obj-162::obj-41::obj-48" : [ "SendTo-TXT[55]", "SendTo-TXT", 0 ],
			"obj-85::obj-162::obj-41::obj-8" : [ "tab[22]", "tab[1]", 0 ],
			"obj-85::obj-162::obj-42::obj-27::obj-18" : [ "toggle[38]", "toggle", 0 ],
			"obj-85::obj-162::obj-42::obj-48" : [ "SendTo-TXT[56]", "SendTo-TXT", 0 ],
			"obj-85::obj-162::obj-42::obj-8" : [ "tab[23]", "tab[1]", 0 ],
			"obj-85::obj-162::obj-43::obj-27::obj-18" : [ "toggle[39]", "toggle", 0 ],
			"obj-85::obj-162::obj-43::obj-48" : [ "SendTo-TXT[57]", "SendTo-TXT", 0 ],
			"obj-85::obj-162::obj-43::obj-8" : [ "tab[24]", "tab[1]", 0 ],
			"obj-85::obj-162::obj-44::obj-27::obj-18" : [ "toggle[40]", "toggle", 0 ],
			"obj-85::obj-162::obj-44::obj-48" : [ "SendTo-TXT[58]", "SendTo-TXT", 0 ],
			"obj-85::obj-162::obj-44::obj-8" : [ "tab[25]", "tab[1]", 0 ],
			"obj-85::obj-162::obj-45::obj-27::obj-18" : [ "toggle[41]", "toggle", 0 ],
			"obj-85::obj-162::obj-45::obj-48" : [ "SendTo-TXT[1]", "SendTo-TXT", 0 ],
			"obj-85::obj-162::obj-45::obj-8" : [ "tab[26]", "tab[1]", 0 ],
			"obj-85::obj-162::obj-46::obj-27::obj-18" : [ "toggle[42]", "toggle", 0 ],
			"obj-85::obj-162::obj-46::obj-48" : [ "SendTo-TXT[2]", "SendTo-TXT", 0 ],
			"obj-85::obj-162::obj-46::obj-8" : [ "tab[27]", "tab[1]", 0 ],
			"obj-85::obj-162::obj-47::obj-27::obj-18" : [ "toggle[43]", "toggle", 0 ],
			"obj-85::obj-162::obj-47::obj-48" : [ "SendTo-TXT[26]", "SendTo-TXT", 0 ],
			"obj-85::obj-162::obj-47::obj-8" : [ "tab[28]", "tab[1]", 0 ],
			"obj-85::obj-162::obj-48::obj-27::obj-18" : [ "toggle[44]", "toggle", 0 ],
			"obj-85::obj-162::obj-48::obj-48" : [ "SendTo-TXT[27]", "SendTo-TXT", 0 ],
			"obj-85::obj-162::obj-48::obj-8" : [ "tab[29]", "tab[1]", 0 ],
			"obj-85::obj-162::obj-49::obj-27::obj-18" : [ "toggle[65]", "toggle", 0 ],
			"obj-85::obj-162::obj-49::obj-48" : [ "SendTo-TXT[28]", "SendTo-TXT", 0 ],
			"obj-85::obj-162::obj-49::obj-8" : [ "tab[30]", "tab[1]", 0 ],
			"obj-85::obj-162::obj-50::obj-27::obj-18" : [ "toggle[66]", "toggle", 0 ],
			"obj-85::obj-162::obj-50::obj-48" : [ "SendTo-TXT[29]", "SendTo-TXT", 0 ],
			"obj-85::obj-162::obj-50::obj-8" : [ "tab[31]", "tab[1]", 0 ],
			"obj-85::obj-162::obj-74::obj-27::obj-18" : [ "toggle[32]", "toggle", 0 ],
			"obj-85::obj-162::obj-74::obj-48" : [ "SendTo-TXT[50]", "SendTo-TXT", 0 ],
			"obj-85::obj-162::obj-74::obj-8" : [ "tab[17]", "tab[1]", 0 ],
			"obj-85::obj-163::obj-107::obj-33" : [ "tab[2]", "tab[1]", 0 ],
			"obj-85::obj-163::obj-123::obj-33" : [ "tab[116]", "tab[1]", 0 ],
			"obj-85::obj-163::obj-34::obj-33" : [ "tab[3]", "tab[1]", 0 ],
			"obj-85::obj-163::obj-36::obj-33" : [ "tab[4]", "tab[1]", 0 ],
			"obj-85::obj-163::obj-40::obj-33" : [ "tab[5]", "tab[1]", 0 ],
			"obj-85::obj-163::obj-41::obj-33" : [ "tab[6]", "tab[1]", 0 ],
			"obj-85::obj-163::obj-42::obj-33" : [ "tab[7]", "tab[1]", 0 ],
			"obj-85::obj-163::obj-43::obj-33" : [ "tab[8]", "tab[1]", 0 ],
			"obj-85::obj-163::obj-44::obj-33" : [ "tab[9]", "tab[1]", 0 ],
			"obj-85::obj-163::obj-45::obj-33" : [ "tab[10]", "tab[1]", 0 ],
			"obj-85::obj-163::obj-46::obj-33" : [ "tab[11]", "tab[1]", 0 ],
			"obj-85::obj-163::obj-47::obj-33" : [ "tab[12]", "tab[1]", 0 ],
			"obj-85::obj-163::obj-48::obj-33" : [ "tab[13]", "tab[1]", 0 ],
			"obj-85::obj-163::obj-49::obj-33" : [ "tab[14]", "tab[1]", 0 ],
			"obj-85::obj-163::obj-50::obj-33" : [ "tab[15]", "tab[1]", 0 ],
			"obj-85::obj-163::obj-74::obj-33" : [ "tab[1]", "tab[1]", 0 ],
			"obj-85::obj-165::obj-25" : [ "live.numbox[2]", "live.numbox", 0 ],
			"obj-85::obj-165::obj-96" : [ "live.numbox[6]", "live.numbox", 0 ],
			"obj-85::obj-227" : [ "incdec", "incdec", 0 ],
			"parameterbanks" : 			{
				"0" : 				{
					"index" : 0,
					"name" : "",
					"parameters" : [ "-", "-", "-", "-", "-", "-", "-", "-" ]
				}

			}
,
			"inherited_shortname" : 1
		}
,
		"parameter_map" : 		{
			"midi" : 			{
				"number" : 				{
					"srcname" : "2.ctrl.0.chan.midi",
					"min" : 0.0,
					"max" : 1.0,
					"flags" : 2
				}
,
				"number[1]" : 				{
					"srcname" : "3.ctrl.0.chan.midi",
					"min" : 0.0,
					"max" : 1.0,
					"flags" : 2
				}
,
				"number[2]" : 				{
					"srcname" : "4.ctrl.0.chan.midi",
					"min" : 20.0,
					"max" : 20000.0,
					"flags" : 2
				}
,
				"number[3]" : 				{
					"srcname" : "5.ctrl.0.chan.midi",
					"min" : 20.0,
					"max" : 20000.0,
					"flags" : 2
				}
,
				"number[4]" : 				{
					"srcname" : "6.ctrl.0.chan.midi",
					"min" : 0.0,
					"max" : 10000.0,
					"flags" : 2
				}
,
				"number[5]" : 				{
					"srcname" : "8.ctrl.0.chan.midi",
					"min" : 1.0,
					"max" : 2000.0,
					"flags" : 2
				}
,
				"number[6]" : 				{
					"srcname" : "9.ctrl.0.chan.midi",
					"min" : 0.0,
					"max" : 1000.0,
					"flags" : 2
				}
,
				"number[7]" : 				{
					"srcname" : "12.ctrl.0.chan.midi",
					"min" : 1.0,
					"max" : 10000.0,
					"flags" : 2
				}
,
				"number[8]" : 				{
					"srcname" : "13.ctrl.0.chan.midi",
					"min" : 0.0,
					"max" : 1.0,
					"flags" : 2
				}
,
				"number[9]" : 				{
					"srcname" : "14.ctrl.0.chan.midi",
					"min" : -48.0,
					"max" : 48.0,
					"flags" : 2
				}
,
				"number[10]" : 				{
					"srcname" : "15.ctrl.0.chan.midi",
					"min" : 0.0,
					"max" : 1.0,
					"flags" : 2
				}
,
				"number[11]" : 				{
					"srcname" : "16.ctrl.0.chan.midi",
					"min" : 0.0,
					"max" : 15000.0,
					"flags" : 2
				}
,
				"number[12]" : 				{
					"srcname" : "17.ctrl.0.chan.midi",
					"min" : 0.0,
					"max" : 3.0,
					"flags" : 2
				}
,
				"number[13]" : 				{
					"srcname" : "18.ctrl.0.chan.midi",
					"min" : -48.0,
					"max" : 48.0,
					"flags" : 2
				}

			}

		}
,
		"dependency_cache" : [ 			{
				"name" : "bassclar-01.wav",
				"bootpath" : "~/Library/CloudStorage/Dropbox/projmusic30/eramaa/audio/nov23",
				"patcherrelativepath" : "../../../../../Library/CloudStorage/Dropbox/projmusic30/eramaa/audio/nov23",
				"type" : "WAVE",
				"implicit" : 1
			}
, 			{
				"name" : "bbdmi.2max.maxpat",
				"bootpath" : "~/Documents/GitHub/bbdmi/max/output/2max",
				"patcherrelativepath" : "../../max/output/2max",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bbdmi.regress.maxpat",
				"bootpath" : "~/Documents/GitHub/bbdmi/max/control_processing/regress",
				"patcherrelativepath" : "../../max/control_processing/regress",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bbdmi.scale.maxpat",
				"bootpath" : "~/Documents/GitHub/bbdmi/max/control_processing/scale",
				"patcherrelativepath" : "../../max/control_processing/scale",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bbdmi_live_granulator~.maxpat",
				"bootpath" : "~/Documents/GitHub/bbdmi/max/sound_synthesis/live_granulator~",
				"patcherrelativepath" : "../../max/sound_synthesis/live_granulator~",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bbdmi_multi_granulator11~.mxo",
				"type" : "iLaX"
			}
, 			{
				"name" : "bbdmi_multi_granulator4~.mxo",
				"type" : "iLaX"
			}
, 			{
				"name" : "name.js",
				"bootpath" : "~/Documents/GitHub/bbdmi/max/source/js",
				"patcherrelativepath" : "../../max/source/js",
				"type" : "TEXT",
				"implicit" : 1
			}
, 			{
				"name" : "p.2max.maxpat",
				"bootpath" : "~/Documents/GitHub/bbdmi/max/output/2max",
				"patcherrelativepath" : "../../max/output/2max",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "p.exposer.maxpat",
				"bootpath" : "~/Documents/GitHub/bbdmi/max/source/patchers",
				"patcherrelativepath" : "../../max/source/patchers",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "p.scale.maxpat",
				"bootpath" : "~/Documents/GitHub/bbdmi/max/control_processing/scale",
				"patcherrelativepath" : "../../max/control_processing/scale",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "preset.json",
				"bootpath" : "~/Documents/GitHub/bbdmi/max/instruments/spatial_granulator/presets",
				"patcherrelativepath" : "../../max/instruments/spatial_granulator/presets",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "viola-bounce-ed-44.wav",
				"bootpath" : "~/Library/CloudStorage/Dropbox/projmusic30/eramaa/audio/nov23",
				"patcherrelativepath" : "../../../../../Library/CloudStorage/Dropbox/projmusic30/eramaa/audio/nov23",
				"type" : "WAVE",
				"implicit" : 1
			}
 ],
		"autosave" : 0
	}

}
