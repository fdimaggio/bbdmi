{
	"patcher" : 	{
		"fileversion" : 1,
		"appversion" : 		{
			"major" : 8,
			"minor" : 5,
			"revision" : 5,
			"architecture" : "x64",
			"modernui" : 1
		}
,
		"classnamespace" : "box",
		"rect" : [ 34.0, 100.0, 1046.0, 561.0 ],
		"bglocked" : 0,
		"openinpresentation" : 1,
		"default_fontsize" : 12.0,
		"default_fontface" : 0,
		"default_fontname" : "Arial",
		"gridonopen" : 1,
		"gridsize" : [ 15.0, 15.0 ],
		"gridsnaponopen" : 1,
		"objectsnaponopen" : 1,
		"statusbarvisible" : 2,
		"toolbarvisible" : 1,
		"lefttoolbarpinned" : 0,
		"toptoolbarpinned" : 0,
		"righttoolbarpinned" : 0,
		"bottomtoolbarpinned" : 0,
		"toolbars_unpinned_last_save" : 0,
		"tallnewobj" : 0,
		"boxanimatetime" : 200,
		"enablehscroll" : 1,
		"enablevscroll" : 1,
		"devicewidth" : 0.0,
		"description" : "",
		"digest" : "",
		"tags" : "",
		"style" : "",
		"subpatcher_template" : "",
		"assistshowspatchername" : 0,
		"boxes" : [ 			{
				"box" : 				{
					"id" : "obj-1",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 188.32983771959951, 871.0, 55.0, 22.0 ],
					"text" : "zl.slice 1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-24",
					"maxclass" : "newobj",
					"numinlets" : 3,
					"numoutlets" : 1,
					"outlettype" : [ "float" ],
					"patching_rect" : [ 207.32983771959951, 943.0, 67.0, 22.0 ],
					"text" : "slide 20 20"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-14",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 188.32983771959951, 1014.45449740156846, 47.0, 22.0 ],
					"text" : "midiout"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-15",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"patching_rect" : [ 188.32983771959951, 982.257610385071985, 58.0, 22.0 ],
					"text" : "xbendout"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-16",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 188.32983771959951, 902.0, 121.0, 22.0 ],
					"text" : "expr int($f1 * 16383.)"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-11",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 599.5, 484.257653061224801, 87.0, 22.0 ],
					"text" : "prepend setlist"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-9",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 812.5, 202.515306122450056, 87.0, 22.0 ],
					"text" : "prepend setlist"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-2",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 599.5, 233.515306122450056, 54.0, 22.0 ],
					"text" : "deferlow"
				}

			}
, 			{
				"box" : 				{
					"candycane" : 8,
					"id" : "obj-13",
					"maxclass" : "multislider",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 812.5, 265.954591836734835, 139.0, 84.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 12.5, 163.876850198412455, 198.0, 91.0 ],
					"setminmax" : [ 0.0, 1.0 ],
					"setstyle" : 3,
					"size" : 4
				}

			}
, 			{
				"box" : 				{
					"annotation" : "",
					"args" : [ 4, 4 ],
					"bgcolor" : [ 0.180392156862745, 0.423529411764706, 0.776470588235294, 1.0 ],
					"bgmode" : 0,
					"border" : 1,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"hint" : "",
					"id" : "obj-65",
					"lockeddragscroll" : 0,
					"lockedsize" : 0,
					"maxclass" : "bpatcher",
					"name" : "bbdmi.crosspatch.maxpat",
					"numinlets" : 2,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "" ],
					"patching_rect" : [ 343.5, 484.257653061224801, 199.0, 162.439285714284892 ],
					"presentation" : 1,
					"presentation_rect" : [ 423.333333333333371, 163.154231150793294, 199.0, 162.196428571428896 ],
					"varname" : "bbdmi.crosspatch",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"args" : [ 4 ],
					"bgmode" : 0,
					"border" : 1,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-4",
					"lockeddragscroll" : 0,
					"lockedsize" : 0,
					"maxclass" : "bpatcher",
					"name" : "bbdmi.recorder.maxpat",
					"numinlets" : 2,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "" ],
					"patching_rect" : [ 343.5, 31.257653061225028, 199.0, 139.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 12.5, 13.714945436508515, 198.0, 141.0 ],
					"varname" : "bbdmi.recorder",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"annotation" : "",
					"args" : [ 4 ],
					"bgcolor" : [ 0.180392156862745, 0.423529411764706, 0.776470588235294, 1.0 ],
					"bgmode" : 0,
					"border" : 1,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"hint" : "",
					"id" : "obj-45",
					"lockeddragscroll" : 0,
					"lockedsize" : 0,
					"maxclass" : "bpatcher",
					"name" : "bbdmi.smooth.maxpat",
					"numinlets" : 2,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "" ],
					"patching_rect" : [ 343.5, 392.886479591837542, 199.0, 63.439285714284779 ],
					"presentation" : 1,
					"presentation_rect" : [ 12.5, 262.63399305555663, 198.0, 63.439285714284779 ],
					"varname" : "bbdmi.smooth",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"bubble" : 1,
					"id" : "obj-103",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 694.0, 310.704591836734608, 77.0, 24.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 380.791666666666174, 71.714945436508515, 77.0, 24.0 ],
					"text" : "start DSP"
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 1.0, 0.788235, 0.470588, 1.0 ],
					"fontname" : "Arial Bold",
					"hint" : "",
					"id" : "obj-90",
					"ignoreclick" : 1,
					"legacytextcolor" : 1,
					"maxclass" : "textbutton",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "", "", "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 666.5, 312.704591836734608, 20.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 351.666666666666629, 74.214945436508515, 20.0, 20.0 ],
					"rounded" : 60.0,
					"text" : "1",
					"textcolor" : [ 0.34902, 0.34902, 0.34902, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"args" : [ 4 ],
					"bgmode" : 0,
					"border" : 1,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-86",
					"lockeddragscroll" : 0,
					"lockedsize" : 0,
					"maxclass" : "bpatcher",
					"name" : "bbdmi.2xbend.maxpat",
					"numinlets" : 2,
					"numoutlets" : 0,
					"offset" : [ 0.0, 0.0 ],
					"patching_rect" : [ 343.5, 865.0, 199.0, 162.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 835.166666666666742, 162.911374007936956, 199.0, 162.439285714285688 ],
					"varname" : "bbdmi.2xbend",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"annotation" : "",
					"args" : [ 4 ],
					"bgcolor" : [ 0.180392156862745, 0.423529411764706, 0.776470588235294, 1.0 ],
					"bgmode" : 0,
					"border" : 1,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"hint" : "",
					"id" : "obj-83",
					"lockeddragscroll" : 0,
					"lockedsize" : 0,
					"maxclass" : "bpatcher",
					"name" : "bbdmi.multislider.maxpat",
					"numinlets" : 2,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "" ],
					"patching_rect" : [ 343.5, 674.628826530612287, 199.0, 162.439285714285234 ],
					"presentation" : 1,
					"presentation_rect" : [ 629.25, 162.911374007936956, 199.0, 162.439285714285688 ],
					"varname" : "bbdmi.multislider",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 1.0, 0.788235, 0.470588, 1.0 ],
					"fontname" : "Arial Bold",
					"hint" : "",
					"id" : "obj-38",
					"ignoreclick" : 1,
					"legacytextcolor" : 1,
					"maxclass" : "textbutton",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "", "", "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 605.450000000000045, 867.0, 20.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 351.666666666666629, 23.105124007937093, 20.0, 20.0 ],
					"rounded" : 60.0,
					"text" : "2",
					"textcolor" : [ 0.34902, 0.34902, 0.34902, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"bubble" : 1,
					"id" : "obj-69",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 633.0, 865.0, 147.0, 24.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 380.791666666666174, 21.105124007937093, 147.0, 24.0 ],
					"text" : "Endorphines CARGO4"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-71",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 571.0, 865.0, 24.0, 24.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 321.166666666666629, 21.605124007937206, 23.0, 23.0 ]
				}

			}
, 			{
				"box" : 				{
					"candycane" : 8,
					"id" : "obj-41",
					"maxclass" : "multislider",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 599.5, 523.47729591836719, 139.0, 84.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 12.5, 333.154231150793294, 403.0, 212.0 ],
					"setminmax" : [ 0.0, 1.0 ],
					"setstyle" : 3,
					"size" : 4
				}

			}
, 			{
				"box" : 				{
					"annotation" : "",
					"args" : [ 4, "@time", 30 ],
					"bgcolor" : [ 0.180392156862745, 0.423529411764706, 0.776470588235294, 1.0 ],
					"bgmode" : 0,
					"border" : 1,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"hint" : "",
					"id" : "obj-40",
					"lockeddragscroll" : 0,
					"lockedsize" : 0,
					"maxclass" : "bpatcher",
					"name" : "bbdmi.calibrate.maxpat",
					"numinlets" : 2,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "" ],
					"patching_rect" : [ 343.5, 202.515306122450056, 199.0, 162.439285714284892 ],
					"presentation" : 1,
					"presentation_rect" : [ 217.416666666666686, 163.876850198412455, 199.0, 162.196428571428896 ],
					"varname" : "bbdmi.calibrate",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-6",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 599.5, 202.515306122450056, 70.0, 22.0 ],
					"text" : "loadmess 1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-28",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 343.5, 1050.033928571428078, 150.0, 20.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-213",
					"local" : 1,
					"maxclass" : "ezdac~",
					"numinlets" : 2,
					"numoutlets" : 0,
					"patching_rect" : [ 599.5, 295.454591836734835, 54.5, 54.5 ],
					"presentation" : 1,
					"presentation_rect" : [ 289.666666666666629, 56.964945436508515, 54.5, 54.5 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-7",
					"linecount" : 3,
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 571.0, 909.0, 209.0, 49.0 ],
					"text" : ";\rmax launchbrowser https://cargo4.endorphin.es/index.php"
				}

			}
 ],
		"lines" : [ 			{
				"patchline" : 				{
					"destination" : [ "obj-16", 0 ],
					"source" : [ "obj-1", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-41", 0 ],
					"midpoints" : [ 609.0, 507.0, 609.0, 507.0 ],
					"source" : [ "obj-11", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-14", 0 ],
					"midpoints" : [ 197.82983771959951, 1005.0, 197.82983771959951, 1005.0 ],
					"source" : [ "obj-15", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-15", 0 ],
					"midpoints" : [ 197.82983771959951, 963.0, 197.82983771959951, 963.0 ],
					"source" : [ "obj-16", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-213", 0 ],
					"midpoints" : [ 609.0, 258.0, 609.0, 258.0 ],
					"source" : [ "obj-2", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-40", 0 ],
					"midpoints" : [ 353.0, 171.0, 353.0, 171.0 ],
					"order" : 1,
					"source" : [ "obj-4", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-9", 0 ],
					"midpoints" : [ 353.0, 189.0, 822.0, 189.0 ],
					"order" : 0,
					"source" : [ "obj-4", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-45", 0 ],
					"midpoints" : [ 353.0, 366.0, 353.0, 366.0 ],
					"source" : [ "obj-40", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-11", 0 ],
					"midpoints" : [ 353.0, 471.0, 609.0, 471.0 ],
					"order" : 0,
					"source" : [ "obj-45", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-65", 0 ],
					"midpoints" : [ 353.0, 459.0, 353.0, 459.0 ],
					"order" : 1,
					"source" : [ "obj-45", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-2", 0 ],
					"midpoints" : [ 609.0, 225.0, 609.0, 225.0 ],
					"source" : [ "obj-6", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-83", 0 ],
					"midpoints" : [ 353.0, 648.0, 353.0, 648.0 ],
					"source" : [ "obj-65", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-7", 0 ],
					"midpoints" : [ 580.5, 891.0, 580.5, 891.0 ],
					"source" : [ "obj-71", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-86", 0 ],
					"midpoints" : [ 353.0, 840.0, 353.0, 840.0 ],
					"source" : [ "obj-83", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-13", 0 ],
					"midpoints" : [ 822.0, 225.0, 822.0, 225.0 ],
					"source" : [ "obj-9", 0 ]
				}

			}
 ],
		"parameters" : 		{
			"obj-40::obj-107::obj-33" : [ "tab[51]", "tab[1]", 0 ],
			"obj-40::obj-123::obj-33" : [ "tab[26]", "tab[1]", 0 ],
			"obj-40::obj-34::obj-33" : [ "tab[52]", "tab[1]", 0 ],
			"obj-40::obj-36::obj-33" : [ "tab[53]", "tab[1]", 0 ],
			"obj-40::obj-40::obj-33" : [ "tab[54]", "tab[1]", 0 ],
			"obj-40::obj-41::obj-33" : [ "tab[55]", "tab[1]", 0 ],
			"obj-40::obj-42::obj-33" : [ "tab[21]", "tab[1]", 0 ],
			"obj-40::obj-43::obj-33" : [ "tab[24]", "tab[1]", 0 ],
			"obj-40::obj-44::obj-33" : [ "tab[27]", "tab[1]", 0 ],
			"obj-40::obj-45::obj-33" : [ "tab[48]", "tab[1]", 0 ],
			"obj-40::obj-46::obj-33" : [ "tab[56]", "tab[1]", 0 ],
			"obj-40::obj-47::obj-33" : [ "tab[25]", "tab[1]", 0 ],
			"obj-40::obj-48::obj-33" : [ "tab[28]", "tab[1]", 0 ],
			"obj-40::obj-49::obj-33" : [ "tab[57]", "tab[1]", 0 ],
			"obj-40::obj-50::obj-33" : [ "tab[22]", "tab[1]", 0 ],
			"obj-40::obj-74::obj-33" : [ "tab[49]", "tab[1]", 0 ],
			"obj-45::obj-122" : [ "number[1]", "number[1]", 0 ],
			"obj-45::obj-16" : [ "number[3]", "number[1]", 0 ],
			"obj-86::obj-107::obj-27::obj-18" : [ "toggle[14]", "toggle", 0 ],
			"obj-86::obj-107::obj-8" : [ "tab[66]", "tab[1]", 0 ],
			"obj-86::obj-123::obj-27::obj-18" : [ "toggle[22]", "toggle", 0 ],
			"obj-86::obj-123::obj-8" : [ "tab[150]", "tab[1]", 0 ],
			"obj-86::obj-34::obj-27::obj-18" : [ "toggle[15]", "toggle", 0 ],
			"obj-86::obj-34::obj-8" : [ "tab[67]", "tab[1]", 0 ],
			"obj-86::obj-37::obj-27::obj-18" : [ "toggle[26]", "toggle", 0 ],
			"obj-86::obj-37::obj-8" : [ "tab[72]", "tab[1]", 0 ],
			"obj-86::obj-38::obj-27::obj-18" : [ "toggle[25]", "toggle", 0 ],
			"obj-86::obj-38::obj-8" : [ "tab[71]", "tab[1]", 0 ],
			"obj-86::obj-39::obj-27::obj-18" : [ "toggle[16]", "toggle", 0 ],
			"obj-86::obj-39::obj-8" : [ "tab[68]", "tab[1]", 0 ],
			"obj-86::obj-40::obj-27::obj-18" : [ "toggle[23]", "toggle", 0 ],
			"obj-86::obj-40::obj-8" : [ "tab[69]", "tab[1]", 0 ],
			"obj-86::obj-41::obj-27::obj-18" : [ "toggle[24]", "toggle", 0 ],
			"obj-86::obj-41::obj-8" : [ "tab[70]", "tab[1]", 0 ],
			"obj-86::obj-44::obj-27::obj-18" : [ "toggle[27]", "toggle", 0 ],
			"obj-86::obj-44::obj-8" : [ "tab[73]", "tab[1]", 0 ],
			"obj-86::obj-45::obj-27::obj-18" : [ "toggle[28]", "toggle", 0 ],
			"obj-86::obj-45::obj-8" : [ "tab[74]", "tab[1]", 0 ],
			"obj-86::obj-46::obj-27::obj-18" : [ "toggle[29]", "toggle", 0 ],
			"obj-86::obj-46::obj-8" : [ "tab[75]", "tab[1]", 0 ],
			"obj-86::obj-47::obj-27::obj-18" : [ "toggle[30]", "toggle", 0 ],
			"obj-86::obj-47::obj-8" : [ "tab[76]", "tab[1]", 0 ],
			"obj-86::obj-48::obj-27::obj-18" : [ "toggle[31]", "toggle", 0 ],
			"obj-86::obj-48::obj-8" : [ "tab[77]", "tab[1]", 0 ],
			"obj-86::obj-49::obj-27::obj-18" : [ "toggle[32]", "toggle", 0 ],
			"obj-86::obj-49::obj-8" : [ "tab[78]", "tab[1]", 0 ],
			"obj-86::obj-50::obj-27::obj-18" : [ "toggle[33]", "toggle", 0 ],
			"obj-86::obj-50::obj-8" : [ "tab[79]", "tab[1]", 0 ],
			"obj-86::obj-74::obj-27::obj-18" : [ "toggle[11]", "toggle", 0 ],
			"obj-86::obj-74::obj-8" : [ "tab[65]", "tab[1]", 0 ],
			"parameterbanks" : 			{
				"0" : 				{
					"index" : 0,
					"name" : "",
					"parameters" : [ "-", "-", "-", "-", "-", "-", "-", "-" ]
				}

			}
,
			"inherited_shortname" : 1
		}
,
		"dependency_cache" : [ 			{
				"name" : "bbdmi.2xbend.maxpat",
				"bootpath" : "~/Documents/GitHub/bbdmi/max/output/2xbend",
				"patcherrelativepath" : "../../../max/output/2xbend",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bbdmi.calibrate.maxpat",
				"bootpath" : "~/Documents/GitHub/bbdmi/max/control_processing/calibrate",
				"patcherrelativepath" : "../../../max/control_processing/calibrate",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bbdmi.crosspatch.maxpat",
				"bootpath" : "~/Documents/GitHub/bbdmi/max/utilities/crosspatch",
				"patcherrelativepath" : "../../../max/utilities/crosspatch",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bbdmi.multislider.maxpat",
				"bootpath" : "~/Documents/GitHub/bbdmi/max/utilities/multislider",
				"patcherrelativepath" : "../../../max/utilities/multislider",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bbdmi.recorder.maxpat",
				"bootpath" : "~/Documents/GitHub/bbdmi/max/utilities/recorder",
				"patcherrelativepath" : "../../../max/utilities/recorder",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bbdmi.smooth.maxpat",
				"bootpath" : "~/Documents/GitHub/bbdmi/max/control_processing/smooth",
				"patcherrelativepath" : "../../../max/control_processing/smooth",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "name.js",
				"bootpath" : "~/Documents/GitHub/bbdmi/max/source/js",
				"patcherrelativepath" : "../../../max/source/js",
				"type" : "TEXT",
				"implicit" : 1
			}
, 			{
				"name" : "p.2xbend.maxpat",
				"bootpath" : "~/Documents/GitHub/bbdmi/max/output/2xbend",
				"patcherrelativepath" : "../../../max/output/2xbend",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "p.calibrate.maxpat",
				"bootpath" : "~/Documents/GitHub/bbdmi/max/control_processing/calibrate",
				"patcherrelativepath" : "../../../max/control_processing/calibrate",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "p.exposer.maxpat",
				"bootpath" : "~/Documents/GitHub/bbdmi/max/source/patchers",
				"patcherrelativepath" : "../../../max/source/patchers",
				"type" : "JSON",
				"implicit" : 1
			}
 ],
		"autosave" : 0
	}

}
