{
	"patcher" : 	{
		"fileversion" : 1,
		"appversion" : 		{
			"major" : 8,
			"minor" : 5,
			"revision" : 6,
			"architecture" : "x64",
			"modernui" : 1
		}
,
		"classnamespace" : "box",
		"rect" : [ 34.0, 87.0, 1242.0, 686.0 ],
		"bglocked" : 0,
		"openinpresentation" : 1,
		"default_fontsize" : 12.0,
		"default_fontface" : 0,
		"default_fontname" : "Arial",
		"gridonopen" : 1,
		"gridsize" : [ 15.0, 15.0 ],
		"gridsnaponopen" : 1,
		"objectsnaponopen" : 1,
		"statusbarvisible" : 2,
		"toolbarvisible" : 1,
		"lefttoolbarpinned" : 0,
		"toptoolbarpinned" : 0,
		"righttoolbarpinned" : 0,
		"bottomtoolbarpinned" : 0,
		"toolbars_unpinned_last_save" : 0,
		"tallnewobj" : 0,
		"boxanimatetime" : 200,
		"enablehscroll" : 1,
		"enablevscroll" : 1,
		"devicewidth" : 0.0,
		"description" : "",
		"digest" : "",
		"tags" : "",
		"style" : "",
		"subpatcher_template" : "",
		"assistshowspatchername" : 0,
		"boxes" : [ 			{
				"box" : 				{
					"fontsize" : 50.0,
					"id" : "obj-39",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 243.0, 29.0, 122.0, 62.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 866.133333333333439, 6.0, 122.0, 62.0 ],
					"text" : "EMG"
				}

			}
, 			{
				"box" : 				{
					"fontsize" : 50.0,
					"id" : "obj-37",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 764.0, 29.0, 114.0, 62.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 459.566666666666606, 218.0, 114.0, 62.0 ],
					"text" : "EEG"
				}

			}
, 			{
				"box" : 				{
					"arrows" : 2,
					"border" : 2.0,
					"id" : "obj-35",
					"linecolor" : [ 0.0, 0.0, 0.0, 1.0 ],
					"maxclass" : "live.line",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 334.0, 276.0, 5.0, 100.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 6.0, 197.0, 1227.516666666666879, 11.126319444445471 ],
					"saved_attribute_attributes" : 					{
						"linecolor" : 						{
							"expression" : "themecolor.live_control_fg"
						}

					}

				}

			}
, 			{
				"box" : 				{
					"id" : "obj-21",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 142.0, 932.060714285714312, 64.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1066.491666666666788, 77.694642857143492, 64.0, 20.0 ],
					"text" : "ROUTING"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-22",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 312.5, 750.060714285714312, 74.0, 22.0 ],
					"text" : "crosspatch~"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-23",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 163.5, 788.060714285714312, 168.0, 22.0 ],
					"text" : "combine bbdmi. s @triggers 1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-25",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 163.5, 818.535714285714675, 77.0, 22.0 ],
					"text" : "subscribe $1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-26",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 15.0, 818.535714285714675, 63.0, 22.0 ],
					"text" : "writeagain"
				}

			}
, 			{
				"box" : 				{
					"bubblesize" : 20,
					"id" : "obj-27",
					"maxclass" : "preset",
					"numinlets" : 1,
					"numoutlets" : 5,
					"outlettype" : [ "preset", "int", "preset", "int", "" ],
					"patching_rect" : [ 15.0, 915.560714285714312, 101.0, 53.0 ],
					"pattrstorage" : "jim-at-d",
					"presentation" : 1,
					"presentation_rect" : [ 1047.991666666666788, 104.437499999999829, 100.999999999999886, 29.0 ]
				}

			}
, 			{
				"box" : 				{
					"autorestore" : "jim-at-d.json",
					"id" : "obj-31",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 15.0, 869.310714285714312, 485.0, 22.0 ],
					"saved_object_attributes" : 					{
						"client_rect" : [ 100, 100, 500, 600 ],
						"parameter_enable" : 0,
						"parameter_mappable" : 0,
						"storage_rect" : [ 200, 200, 800, 500 ]
					}
,
					"subscribe" : [ "bbdmi.crosspatch~" ],
					"text" : "pattrstorage jim-at-d @savemode 3 @autorestore 1 @changemode 1 @subscribemode 1",
					"varname" : "jim-at-d"
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.058823529411765, 0.058823529411765, 0.058823529411765, 1.0 ],
					"fgcolor" : [ 0.403, 1.0, 0.2, 1.0 ],
					"gridcolor" : [ 0.137254901960784, 0.137254901960784, 0.137254901960784, 1.0 ],
					"id" : "obj-8",
					"maxclass" : "scope~",
					"numinlets" : 2,
					"numoutlets" : 0,
					"patching_rect" : [ 238.5, 415.643303571428589, 139.0, 84.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 623.450000000000273, 6.0, 199.0, 160.999999999999943 ]
				}

			}
, 			{
				"box" : 				{
					"args" : [ 4, "@dB", -6 ],
					"bgmode" : 0,
					"border" : 1,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-10",
					"lockeddragscroll" : 0,
					"lockedsize" : 0,
					"maxclass" : "bpatcher",
					"name" : "bbdmi.dac~.maxpat",
					"numinlets" : 2,
					"numoutlets" : 0,
					"offset" : [ 0.0, 0.0 ],
					"patching_rect" : [ 15.0, 615.191071428571377, 198.0, 97.125000000000227 ],
					"presentation" : 1,
					"presentation_rect" : [ 828.133333333333439, 70.874999999999716, 198.0, 96.125000000000227 ],
					"varname" : "bbdmi.dac~",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"annotation" : "",
					"args" : [ 4, 4 ],
					"bgmode" : 0,
					"border" : 1,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"hint" : "",
					"id" : "obj-201",
					"lockeddragscroll" : 0,
					"lockedsize" : 0,
					"maxclass" : "bpatcher",
					"name" : "bbdmi.crosspatch~.maxpat",
					"numinlets" : 2,
					"numoutlets" : 2,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "multichannelsignal", "" ],
					"patching_rect" : [ 15.0, 415.643303571428589, 198.0, 160.999999999999943 ],
					"presentation" : 1,
					"presentation_rect" : [ 417.491666666666788, 6.0, 198.0, 160.999999999999943 ],
					"varname" : "bbdmi.crosspatch~",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"annotation" : "",
					"args" : [ 4, "@cutoff", 100.0 ],
					"bgmode" : 0,
					"border" : 1,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"hint" : "",
					"id" : "obj-175",
					"lockeddragscroll" : 0,
					"lockedsize" : 0,
					"maxclass" : "bpatcher",
					"name" : "bbdmi.onepole~.maxpat",
					"numinlets" : 2,
					"numoutlets" : 2,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "multichannelsignal", "" ],
					"patching_rect" : [ 15.0, 216.095535714285745, 198.0, 160.999999999999972 ],
					"presentation" : 1,
					"presentation_rect" : [ 211.533333333333331, 6.0, 198.0, 160.999999999999972 ],
					"varname" : "bbdmi.onepole~",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"annotation" : "",
					"args" : [ 7, 7 ],
					"bgcolor" : [ 0.180392156862745, 0.423529411764706, 0.776470588235294, 1.0 ],
					"bgmode" : 0,
					"border" : 1,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"hint" : "",
					"id" : "obj-65",
					"lockeddragscroll" : 0,
					"lockedsize" : 0,
					"maxclass" : "bpatcher",
					"name" : "bbdmi.crosspatch.maxpat",
					"numinlets" : 2,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "" ],
					"patching_rect" : [ 541.5, 1195.0, 199.0, 234.439285714284779 ],
					"presentation" : 1,
					"presentation_rect" : [ 623.450000000000273, 297.714945436508515, 199.0, 234.439285714285234 ],
					"varname" : "bbdmi.crosspatch",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"args" : [ 7 ],
					"bgmode" : 0,
					"border" : 1,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-4",
					"lockeddragscroll" : 0,
					"lockedsize" : 0,
					"maxclass" : "bpatcher",
					"name" : "bbdmi.recorder.maxpat",
					"numinlets" : 2,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "" ],
					"patching_rect" : [ 593.5, 323.475000000000364, 199.0, 139.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 6.0, 538.689285714285688, 199.0, 139.0 ],
					"varname" : "bbdmi.recorder",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"annotation" : "",
					"args" : [ 7 ],
					"bgcolor" : [ 0.180392156862745, 0.423529411764706, 0.776470588235294, 1.0 ],
					"bgmode" : 0,
					"border" : 1,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"hint" : "",
					"id" : "obj-45",
					"lockeddragscroll" : 0,
					"lockedsize" : 0,
					"maxclass" : "bpatcher",
					"name" : "bbdmi.smooth.maxpat",
					"numinlets" : 2,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "float" ],
					"patching_rect" : [ 541.5, 926.335714285714857, 199.0, 63.439285714284779 ],
					"presentation" : 1,
					"presentation_rect" : [ 211.958333333333485, 538.689285714285688, 199.0, 63.439285714284779 ],
					"varname" : "bbdmi.smooth",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-32",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1187.0, 529.310714285714312, 48.0, 22.0 ],
					"text" : "smooth"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-115",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 997.0, 714.274999999999636, 31.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 295.533333333333303, 229.126319444445471, 31.0, 20.0 ],
					"text" : "INIT"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-114",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 990.5, 425.975000000000364, 138.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 36.4500000000005, 229.126319444445471, 138.0, 20.0 ],
					"text" : "BAND POWER MATRIX"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-104",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 976.5, 1257.75, 64.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 690.950000000000387, 229.369176587301808, 64.0, 20.0 ],
					"text" : "ROUTING"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-105",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1147.0, 1075.75, 67.0, 22.0 ],
					"text" : "crosspatch"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-109",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 998.0, 1113.75, 168.0, 22.0 ],
					"text" : "combine bbdmi. s @triggers 1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-110",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 998.0, 1144.225000000000364, 77.0, 22.0 ],
					"text" : "subscribe $1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-111",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 849.5, 1144.225000000000364, 63.0, 22.0 ],
					"text" : "writeagain"
				}

			}
, 			{
				"box" : 				{
					"bubblesize" : 20,
					"id" : "obj-112",
					"maxclass" : "preset",
					"numinlets" : 1,
					"numoutlets" : 5,
					"outlettype" : [ "preset", "int", "preset", "int", "" ],
					"patching_rect" : [ 849.5, 1241.25, 101.0, 53.0 ],
					"pattrstorage" : "jim-at-e",
					"presentation" : 1,
					"presentation_rect" : [ 672.450000000000387, 256.112033730158146, 100.999999999999886, 29.0 ]
				}

			}
, 			{
				"box" : 				{
					"autorestore" : "jim-at-e.json",
					"id" : "obj-113",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 849.5, 1195.0, 485.0, 22.0 ],
					"priority" : 					{
						"bbdmi.crosspatch::Crosspatch" : 1
					}
,
					"saved_object_attributes" : 					{
						"client_rect" : [ 100, 100, 500, 600 ],
						"parameter_enable" : 0,
						"parameter_mappable" : 0,
						"storage_rect" : [ 200, 200, 800, 500 ]
					}
,
					"subscribe" : [ "bbdmi.crosspatch" ],
					"text" : "pattrstorage jim-at-e @savemode 3 @autorestore 1 @changemode 1 @subscribemode 1",
					"varname" : "jim-at-e"
				}

			}
, 			{
				"box" : 				{
					"bubble" : 1,
					"id" : "obj-103",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 964.5, 778.999999999999773, 77.0, 24.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1086.516666666666879, 609.261607142855837, 77.0, 24.0 ],
					"text" : "start DSP"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-92",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 976.5, 1523.492857142856337, 58.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 899.483333333333462, 229.369176587301808, 58.0, 20.0 ],
					"text" : "OUTPUT"
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 1.0, 0.788235, 0.470588, 1.0 ],
					"fontname" : "Arial Bold",
					"hint" : "",
					"id" : "obj-90",
					"ignoreclick" : 1,
					"legacytextcolor" : 1,
					"maxclass" : "textbutton",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "", "", "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 937.0, 780.999999999999773, 20.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1057.391666666667334, 611.761607142855837, 20.0, 20.0 ],
					"rounded" : 60.0,
					"text" : "1",
					"textcolor" : [ 0.34902, 0.34902, 0.34902, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"args" : [ 7 ],
					"bgmode" : 0,
					"border" : 1,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-86",
					"lockeddragscroll" : 0,
					"lockedsize" : 0,
					"maxclass" : "bpatcher",
					"name" : "bbdmi.2xbend.maxpat",
					"numinlets" : 2,
					"numoutlets" : 0,
					"offset" : [ 0.0, 0.0 ],
					"patching_rect" : [ 541.5, 1738.0, 199.0, 234.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1034.516666666666879, 297.714945436508515, 199.0, 234.439285714285688 ],
					"varname" : "bbdmi.2xbend",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"annotation" : "",
					"args" : [ 7 ],
					"bgcolor" : [ 0.180392156862745, 0.423529411764706, 0.776470588235294, 1.0 ],
					"bgmode" : 0,
					"border" : 1,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"hint" : "",
					"id" : "obj-83",
					"lockeddragscroll" : 0,
					"lockedsize" : 0,
					"maxclass" : "bpatcher",
					"name" : "bbdmi.multislider.maxpat",
					"numinlets" : 2,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "" ],
					"patching_rect" : [ 541.5, 1457.742857142856337, 199.000000000000057, 234.439285714285234 ],
					"presentation" : 1,
					"presentation_rect" : [ 828.98333333333369, 297.714945436508515, 199.0, 234.439285714285688 ],
					"varname" : "bbdmi.multislider",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-63",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1139.5, 255.0, 48.0, 22.0 ],
					"text" : "unicorn"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-64",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 990.5, 293.0, 168.0, 22.0 ],
					"text" : "combine bbdmi. s @triggers 1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-66",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 990.5, 323.475000000000364, 77.0, 22.0 ],
					"text" : "subscribe $1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-70",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 842.0, 323.475000000000364, 63.0, 22.0 ],
					"text" : "writeagain"
				}

			}
, 			{
				"box" : 				{
					"bubblesize" : 20,
					"id" : "obj-72",
					"maxclass" : "preset",
					"numinlets" : 1,
					"numoutlets" : 5,
					"outlettype" : [ "preset", "int", "preset", "int", "" ],
					"patching_rect" : [ 842.0, 409.475000000000364, 101.0, 53.0 ],
					"pattrstorage" : "jim-at-c",
					"presentation" : 1,
					"presentation_rect" : [ 54.9500000000005, 255.869176587301808, 101.0, 29.0 ]
				}

			}
, 			{
				"box" : 				{
					"autorestore" : "jim-at-c.json",
					"id" : "obj-73",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 842.0, 374.249999999999773, 484.0, 22.0 ],
					"saved_object_attributes" : 					{
						"client_rect" : [ 100, 100, 500, 600 ],
						"parameter_enable" : 0,
						"parameter_mappable" : 0,
						"storage_rect" : [ 200, 200, 800, 500 ]
					}
,
					"subscribe" : [ "bbdmi.unicorn" ],
					"text" : "pattrstorage jim-at-c @savemode 3 @autorestore 1 @changemode 1 @subscribemode 1",
					"varname" : "jim-at-c"
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 1.0, 0.788235, 0.470588, 1.0 ],
					"fontname" : "Arial Bold",
					"hint" : "",
					"id" : "obj-38",
					"ignoreclick" : 1,
					"legacytextcolor" : 1,
					"maxclass" : "textbutton",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "", "", "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 325.450000000000045, 1696.0, 20.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1057.391666666667334, 560.651785714284415, 20.0, 20.0 ],
					"rounded" : 60.0,
					"text" : "2",
					"textcolor" : [ 0.34902, 0.34902, 0.34902, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"bubble" : 1,
					"id" : "obj-69",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 353.0, 1694.0, 147.0, 24.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1086.516666666666879, 558.651785714284415, 147.0, 24.0 ],
					"text" : "Endorphines CARGO4"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-71",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 291.0, 1694.0, 24.0, 24.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1026.891666666667334, 559.151785714284529, 23.0, 23.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-56",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 1117.5, 478.055357142857247, 58.0, 22.0 ],
					"text" : "loadbang"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-54",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1117.5, 529.310714285714312, 54.0, 22.0 ],
					"text" : "calibrate"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-33",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 968.5, 578.274999999999636, 168.0, 22.0 ],
					"text" : "combine bbdmi. s @triggers 1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-34",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 968.5, 608.75, 77.0, 22.0 ],
					"text" : "subscribe $1"
				}

			}
, 			{
				"box" : 				{
					"candycane" : 8,
					"id" : "obj-41",
					"maxclass" : "multislider",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 797.5, 1020.75, 139.0, 84.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 417.066666666666663, 297.714945436508515, 199.0, 234.0 ],
					"setminmax" : [ 0.0, 1.0 ],
					"setstyle" : 3,
					"size" : 7
				}

			}
, 			{
				"box" : 				{
					"annotation" : "",
					"args" : [ 7, "@time", 30 ],
					"bgcolor" : [ 0.180392156862745, 0.423529411764706, 0.776470588235294, 1.0 ],
					"bgmode" : 0,
					"border" : 1,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"hint" : "",
					"id" : "obj-40",
					"lockeddragscroll" : 0,
					"lockedsize" : 0,
					"maxclass" : "bpatcher",
					"name" : "bbdmi.calibrate.maxpat",
					"numinlets" : 2,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "" ],
					"patching_rect" : [ 541.5, 656.871428571429533, 199.0, 234.439285714284892 ],
					"presentation" : 1,
					"presentation_rect" : [ 211.533333333333331, 297.714945436508515, 199.0, 234.439285714284892 ],
					"varname" : "bbdmi.calibrate",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-15",
					"linecount" : 7,
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 764.0, 150.0, 128.0, 100.0 ],
					"presentation" : 1,
					"presentation_linecount" : 7,
					"presentation_rect" : [ 452.566666666666606, 538.689285714285688, 128.0, 100.0 ],
					"text" : "Delta (1-4 Hz)\nTheta (4-8 Hz)\nAlpha (8-12 Hz)\nBeta Low (12-16 Hz) \nBeta Mid (16-20 Hz)\nBeta High (20- 30 Hz) \nGamma (30-50 Hz)"
				}

			}
, 			{
				"box" : 				{
					"bgmode" : 0,
					"border" : 1,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-46",
					"lockeddragscroll" : 0,
					"lockedsize" : 0,
					"maxclass" : "bpatcher",
					"name" : "bbdmi.unicorn.maxpat",
					"numinlets" : 1,
					"numoutlets" : 3,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "", "", "" ],
					"patching_rect" : [ 541.5, 16.0, 198.0, 234.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 6.0, 297.714945436508515, 199.0, 234.439285714284892 ],
					"varname" : "bbdmi.unicorn",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"args" : [ 4 ],
					"bgcolor" : [ 0.772549019607843, 0.831372549019608, 0.294117647058824, 1.0 ],
					"bgmode" : 0,
					"border" : 1,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-53",
					"lockeddragscroll" : 0,
					"lockedsize" : 0,
					"maxclass" : "bpatcher",
					"name" : "bbdmi.list2~.maxpat",
					"numinlets" : 2,
					"numoutlets" : 2,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "multichannelsignal", "" ],
					"patching_rect" : [ 15.0, 142.547767857142873, 199.0, 35.0 ],
					"varname" : "bbdmi.list2~",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-24",
					"linecount" : 2,
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 779.5, 763.75, 55.0, 35.0 ],
					"text" : ";\rdsp start"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-14",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 779.5, 656.774999999999636, 54.0, 22.0 ],
					"text" : "deferlow"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-6",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 779.5, 608.75, 70.0, 22.0 ],
					"text" : "loadmess 1"
				}

			}
, 			{
				"box" : 				{
					"args" : [ 2 ],
					"bgcolor" : [ 0.694117647058824, 0.035294117647059, 0.0, 1.0 ],
					"bgmode" : 0,
					"border" : 1,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-30",
					"lockeddragscroll" : 0,
					"lockedsize" : 0,
					"maxclass" : "bpatcher",
					"name" : "bbdmi.eavi.maxpat",
					"numinlets" : 1,
					"numoutlets" : 2,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 15.0, 16.0, 198.0, 88.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 6.0, 6.0, 199.0, 88.0 ],
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-130",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1147.0, 1338.492857142856565, 63.0, 22.0 ],
					"text" : "multislider"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-131",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 998.0, 1376.492857142856565, 168.0, 22.0 ],
					"text" : "combine bbdmi. s @triggers 1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-132",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 998.0, 1406.967857142856928, 77.0, 22.0 ],
					"text" : "subscribe $1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-106",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 849.5, 1406.967857142856928, 63.0, 22.0 ],
					"text" : "writeagain"
				}

			}
, 			{
				"box" : 				{
					"bubblesize" : 20,
					"id" : "obj-107",
					"maxclass" : "preset",
					"numinlets" : 1,
					"numoutlets" : 5,
					"outlettype" : [ "preset", "int", "preset", "int", "" ],
					"patching_rect" : [ 849.5, 1506.992857142856565, 101.0, 53.0 ],
					"pattrstorage" : "jim-at-b",
					"presentation" : 1,
					"presentation_rect" : [ 877.983333333333462, 256.112033730158146, 101.0, 29.0 ]
				}

			}
, 			{
				"box" : 				{
					"autorestore" : "jim-at-b.json",
					"id" : "obj-108",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 849.5, 1457.742857142856337, 485.0, 22.0 ],
					"priority" : 					{
						"bbdmi.multislider::Multislider" : 1
					}
,
					"saved_object_attributes" : 					{
						"client_rect" : [ 100, 100, 500, 600 ],
						"parameter_enable" : 0,
						"parameter_mappable" : 0,
						"storage_rect" : [ 200, 200, 800, 500 ]
					}
,
					"subscribe" : [ "bbdmi.multislider", "bbdmi.multislider[1]" ],
					"text" : "pattrstorage jim-at-b @savemode 3 @autorestore 1 @changemode 1 @subscribemode 1",
					"varname" : "jim-at-b"
				}

			}
, 			{
				"box" : 				{
					"candycane" : 8,
					"id" : "obj-29",
					"maxclass" : "multislider",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 238.5, 142.547767857142873, 139.0, 84.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 6.0, 99.0, 199.0, 68.0 ],
					"setstyle" : 3,
					"size" : 4
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-28",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 541.5, 1986.033928571428078, 150.0, 20.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-19",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 870.0, 608.75, 63.0, 22.0 ],
					"text" : "writeagain"
				}

			}
, 			{
				"box" : 				{
					"bubblesize" : 20,
					"id" : "obj-17",
					"maxclass" : "preset",
					"numinlets" : 1,
					"numoutlets" : 5,
					"outlettype" : [ "preset", "int", "preset", "int", "" ],
					"patching_rect" : [ 870.0, 709.774999999999636, 101.0, 29.0 ],
					"pattrstorage" : "jim-at-a",
					"presentation" : 1,
					"presentation_rect" : [ 260.533333333333303, 255.869176587301808, 101.0, 29.0 ]
				}

			}
, 			{
				"box" : 				{
					"autorestore" : "jim-at-a.json",
					"id" : "obj-3",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 870.0, 656.774999999999636, 485.0, 22.0 ],
					"saved_object_attributes" : 					{
						"client_rect" : [ 100, 100, 500, 600 ],
						"parameter_enable" : 0,
						"parameter_mappable" : 0,
						"storage_rect" : [ 200, 200, 800, 500 ]
					}
,
					"subscribe" : [ "bbdmi.calibrate", "bbdmi.smooth" ],
					"text" : "pattrstorage jim-at-a @savemode 3 @autorestore 1 @changemode 1 @subscribemode 1",
					"varname" : "jim-at-a"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-213",
					"local" : 1,
					"maxclass" : "ezdac~",
					"numinlets" : 2,
					"numoutlets" : 0,
					"patching_rect" : [ 870.0, 763.75, 54.5, 54.5 ],
					"presentation" : 1,
					"presentation_rect" : [ 995.39166666666722, 594.511607142855837, 54.5, 54.5 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-7",
					"linecount" : 3,
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 291.0, 1738.0, 209.0, 49.0 ],
					"text" : ";\rmax launchbrowser https://cargo4.endorphin.es/index.php"
				}

			}
 ],
		"lines" : [ 			{
				"patchline" : 				{
					"destination" : [ "obj-109", 1 ],
					"midpoints" : [ 1156.5, 1098.0, 1156.5, 1098.0 ],
					"source" : [ "obj-105", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-108", 0 ],
					"midpoints" : [ 859.0, 1431.0, 859.0, 1431.0 ],
					"source" : [ "obj-106", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-110", 0 ],
					"midpoints" : [ 1007.5, 1137.0, 1007.5, 1137.0 ],
					"source" : [ "obj-109", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-113", 0 ],
					"midpoints" : [ 1007.5, 1182.0, 859.0, 1182.0 ],
					"source" : [ "obj-110", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-113", 0 ],
					"midpoints" : [ 859.0, 1167.0, 859.0, 1167.0 ],
					"source" : [ "obj-111", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-131", 1 ],
					"midpoints" : [ 1156.5, 1362.0, 1156.5, 1362.0 ],
					"source" : [ "obj-130", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-132", 0 ],
					"midpoints" : [ 1007.5, 1401.0, 1007.5, 1401.0 ],
					"source" : [ "obj-131", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-108", 0 ],
					"midpoints" : [ 1007.5, 1443.0, 859.0, 1443.0 ],
					"source" : [ "obj-132", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-17", 0 ],
					"midpoints" : [ 789.0, 696.0, 879.5, 696.0 ],
					"order" : 0,
					"source" : [ "obj-14", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-24", 0 ],
					"midpoints" : [ 789.0, 681.0, 789.0, 681.0 ],
					"order" : 1,
					"source" : [ "obj-14", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-201", 0 ],
					"midpoints" : [ 24.5, 378.0, 24.5, 378.0 ],
					"order" : 1,
					"source" : [ "obj-175", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-8", 0 ],
					"midpoints" : [ 24.5, 402.0, 248.0, 402.0 ],
					"order" : 0,
					"source" : [ "obj-175", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-3", 0 ],
					"midpoints" : [ 879.5, 633.0, 879.5, 633.0 ],
					"source" : [ "obj-19", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-10", 0 ],
					"midpoints" : [ 24.5, 579.0, 24.5, 579.0 ],
					"source" : [ "obj-201", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-23", 1 ],
					"midpoints" : [ 322.0, 774.0, 322.0, 774.0 ],
					"source" : [ "obj-22", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-25", 0 ],
					"midpoints" : [ 173.0, 813.0, 173.0, 813.0 ],
					"source" : [ "obj-23", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-31", 0 ],
					"midpoints" : [ 173.0, 855.0, 24.5, 855.0 ],
					"source" : [ "obj-25", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-31", 0 ],
					"midpoints" : [ 24.5, 843.0, 24.5, 843.0 ],
					"source" : [ "obj-26", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-29", 0 ],
					"midpoints" : [ 24.5, 129.0, 248.0, 129.0 ],
					"order" : 0,
					"source" : [ "obj-30", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-53", 0 ],
					"midpoints" : [ 24.5, 105.0, 24.5, 105.0 ],
					"order" : 1,
					"source" : [ "obj-30", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-33", 1 ],
					"midpoints" : [ 1196.5, 564.0, 1127.0, 564.0 ],
					"source" : [ "obj-32", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-34", 0 ],
					"midpoints" : [ 978.0, 603.0, 978.0, 603.0 ],
					"source" : [ "obj-33", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-3", 0 ],
					"midpoints" : [ 978.0, 642.0, 879.5, 642.0 ],
					"source" : [ "obj-34", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-40", 0 ],
					"midpoints" : [ 603.0, 642.0, 551.0, 642.0 ],
					"source" : [ "obj-4", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-45", 0 ],
					"midpoints" : [ 551.0, 894.0, 551.0, 894.0 ],
					"source" : [ "obj-40", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-41", 0 ],
					"midpoints" : [ 551.0, 1005.0, 807.0, 1005.0 ],
					"order" : 0,
					"source" : [ "obj-45", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-65", 0 ],
					"midpoints" : [ 551.0, 990.0, 551.0, 990.0 ],
					"order" : 1,
					"source" : [ "obj-45", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-4", 0 ],
					"midpoints" : [ 551.0, 309.0, 603.0, 309.0 ],
					"order" : 1,
					"source" : [ "obj-46", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-40", 0 ],
					"midpoints" : [ 551.0, 252.0, 551.0, 252.0 ],
					"order" : 0,
					"source" : [ "obj-46", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-175", 0 ],
					"midpoints" : [ 24.5, 180.0, 24.5, 180.0 ],
					"source" : [ "obj-53", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-33", 1 ],
					"midpoints" : [ 1127.0, 552.0, 1127.0, 552.0 ],
					"source" : [ "obj-54", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-32", 0 ],
					"midpoints" : [ 1127.0, 516.0, 1196.5, 516.0 ],
					"order" : 0,
					"source" : [ "obj-56", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-54", 0 ],
					"midpoints" : [ 1127.0, 501.0, 1127.0, 501.0 ],
					"order" : 1,
					"source" : [ "obj-56", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-14", 0 ],
					"midpoints" : [ 789.0, 633.0, 789.0, 633.0 ],
					"source" : [ "obj-6", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-64", 1 ],
					"midpoints" : [ 1149.0, 279.0, 1149.0, 279.0 ],
					"source" : [ "obj-63", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-66", 0 ],
					"midpoints" : [ 1000.0, 318.0, 1000.0, 318.0 ],
					"source" : [ "obj-64", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-83", 0 ],
					"midpoints" : [ 551.0, 1431.0, 551.0, 1431.0 ],
					"source" : [ "obj-65", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-73", 0 ],
					"midpoints" : [ 1000.0, 360.0, 851.5, 360.0 ],
					"source" : [ "obj-66", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-73", 0 ],
					"midpoints" : [ 851.5, 348.0, 851.5, 348.0 ],
					"source" : [ "obj-70", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-7", 0 ],
					"midpoints" : [ 300.5, 1719.0, 300.5, 1719.0 ],
					"source" : [ "obj-71", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-86", 0 ],
					"midpoints" : [ 551.0, 1695.0, 551.0, 1695.0 ],
					"source" : [ "obj-83", 0 ]
				}

			}
 ],
		"parameters" : 		{
			"obj-10::obj-45" : [ "live.gain~[2]", "live.gain~", 0 ],
			"obj-175::obj-107::obj-33" : [ "tab[29]", "tab[1]", 0 ],
			"obj-175::obj-123::obj-33" : [ "tab[125]", "tab[1]", 0 ],
			"obj-175::obj-36::obj-33" : [ "tab[15]", "tab[1]", 0 ],
			"obj-175::obj-40::obj-33" : [ "tab[16]", "tab[1]", 0 ],
			"obj-175::obj-41::obj-33" : [ "tab[17]", "tab[1]", 0 ],
			"obj-175::obj-44::obj-33" : [ "tab[20]", "tab[1]", 0 ],
			"obj-175::obj-45::obj-33" : [ "tab[31]", "tab[1]", 0 ],
			"obj-175::obj-46::obj-33" : [ "tab[32]", "tab[1]", 0 ],
			"obj-175::obj-47::obj-33" : [ "tab[33]", "tab[1]", 0 ],
			"obj-175::obj-48::obj-33" : [ "tab[34]", "tab[1]", 0 ],
			"obj-175::obj-49::obj-33" : [ "tab[35]", "tab[1]", 0 ],
			"obj-175::obj-50::obj-33" : [ "tab[36]", "tab[1]", 0 ],
			"obj-175::obj-6::obj-33" : [ "tab[30]", "tab[1]", 0 ],
			"obj-175::obj-74::obj-33" : [ "tab[18]", "tab[1]", 0 ],
			"obj-175::obj-8::obj-33" : [ "tab[19]", "tab[1]", 0 ],
			"obj-175::obj-9::obj-33" : [ "tab[10]", "tab[1]", 0 ],
			"obj-40::obj-107::obj-33" : [ "tab[51]", "tab[1]", 0 ],
			"obj-40::obj-123::obj-33" : [ "tab[26]", "tab[1]", 0 ],
			"obj-40::obj-34::obj-33" : [ "tab[52]", "tab[1]", 0 ],
			"obj-40::obj-36::obj-33" : [ "tab[53]", "tab[1]", 0 ],
			"obj-40::obj-40::obj-33" : [ "tab[54]", "tab[1]", 0 ],
			"obj-40::obj-41::obj-33" : [ "tab[55]", "tab[1]", 0 ],
			"obj-40::obj-42::obj-33" : [ "tab[21]", "tab[1]", 0 ],
			"obj-40::obj-43::obj-33" : [ "tab[24]", "tab[1]", 0 ],
			"obj-40::obj-44::obj-33" : [ "tab[27]", "tab[1]", 0 ],
			"obj-40::obj-45::obj-33" : [ "tab[48]", "tab[1]", 0 ],
			"obj-40::obj-46::obj-33" : [ "tab[56]", "tab[1]", 0 ],
			"obj-40::obj-47::obj-33" : [ "tab[25]", "tab[1]", 0 ],
			"obj-40::obj-48::obj-33" : [ "tab[28]", "tab[1]", 0 ],
			"obj-40::obj-49::obj-33" : [ "tab[57]", "tab[1]", 0 ],
			"obj-40::obj-50::obj-33" : [ "tab[22]", "tab[1]", 0 ],
			"obj-40::obj-74::obj-33" : [ "tab[49]", "tab[1]", 0 ],
			"obj-45::obj-122" : [ "number[1]", "number[1]", 0 ],
			"obj-45::obj-16" : [ "number[3]", "number[1]", 0 ],
			"obj-46::obj-95" : [ "number[162]", "number", 0 ],
			"obj-86::obj-107::obj-27::obj-18" : [ "toggle[14]", "toggle", 0 ],
			"obj-86::obj-107::obj-8" : [ "tab[66]", "tab[1]", 0 ],
			"obj-86::obj-123::obj-27::obj-18" : [ "toggle[22]", "toggle", 0 ],
			"obj-86::obj-123::obj-8" : [ "tab[150]", "tab[1]", 0 ],
			"obj-86::obj-34::obj-27::obj-18" : [ "toggle[15]", "toggle", 0 ],
			"obj-86::obj-34::obj-8" : [ "tab[67]", "tab[1]", 0 ],
			"obj-86::obj-37::obj-27::obj-18" : [ "toggle[26]", "toggle", 0 ],
			"obj-86::obj-37::obj-8" : [ "tab[72]", "tab[1]", 0 ],
			"obj-86::obj-38::obj-27::obj-18" : [ "toggle[25]", "toggle", 0 ],
			"obj-86::obj-38::obj-8" : [ "tab[71]", "tab[1]", 0 ],
			"obj-86::obj-39::obj-27::obj-18" : [ "toggle[16]", "toggle", 0 ],
			"obj-86::obj-39::obj-8" : [ "tab[68]", "tab[1]", 0 ],
			"obj-86::obj-40::obj-27::obj-18" : [ "toggle[23]", "toggle", 0 ],
			"obj-86::obj-40::obj-8" : [ "tab[69]", "tab[1]", 0 ],
			"obj-86::obj-41::obj-27::obj-18" : [ "toggle[24]", "toggle", 0 ],
			"obj-86::obj-41::obj-8" : [ "tab[70]", "tab[1]", 0 ],
			"obj-86::obj-44::obj-27::obj-18" : [ "toggle[27]", "toggle", 0 ],
			"obj-86::obj-44::obj-8" : [ "tab[73]", "tab[1]", 0 ],
			"obj-86::obj-45::obj-27::obj-18" : [ "toggle[28]", "toggle", 0 ],
			"obj-86::obj-45::obj-8" : [ "tab[74]", "tab[1]", 0 ],
			"obj-86::obj-46::obj-27::obj-18" : [ "toggle[29]", "toggle", 0 ],
			"obj-86::obj-46::obj-8" : [ "tab[75]", "tab[1]", 0 ],
			"obj-86::obj-47::obj-27::obj-18" : [ "toggle[30]", "toggle", 0 ],
			"obj-86::obj-47::obj-8" : [ "tab[76]", "tab[1]", 0 ],
			"obj-86::obj-48::obj-27::obj-18" : [ "toggle[31]", "toggle", 0 ],
			"obj-86::obj-48::obj-8" : [ "tab[77]", "tab[1]", 0 ],
			"obj-86::obj-49::obj-27::obj-18" : [ "toggle[32]", "toggle", 0 ],
			"obj-86::obj-49::obj-8" : [ "tab[78]", "tab[1]", 0 ],
			"obj-86::obj-50::obj-27::obj-18" : [ "toggle[33]", "toggle", 0 ],
			"obj-86::obj-50::obj-8" : [ "tab[79]", "tab[1]", 0 ],
			"obj-86::obj-74::obj-27::obj-18" : [ "toggle[11]", "toggle", 0 ],
			"obj-86::obj-74::obj-8" : [ "tab[65]", "tab[1]", 0 ],
			"parameterbanks" : 			{
				"0" : 				{
					"index" : 0,
					"name" : "",
					"parameters" : [ "-", "-", "-", "-", "-", "-", "-", "-" ]
				}

			}
,
			"parameter_overrides" : 			{
				"obj-10::obj-45" : 				{
					"parameter_longname" : "live.gain~[2]"
				}

			}
,
			"inherited_shortname" : 1
		}
,
		"dependency_cache" : [ 			{
				"name" : "bbdmi.2xbend.maxpat",
				"bootpath" : "~/Documents/Max 8/Packages/bbdmi/max/output/2xbend",
				"patcherrelativepath" : "../../max/output/2xbend",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bbdmi.calibrate.maxpat",
				"bootpath" : "~/Documents/Max 8/Packages/bbdmi/max/control_processing/calibrate",
				"patcherrelativepath" : "../../max/control_processing/calibrate",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bbdmi.crosspatch.maxpat",
				"bootpath" : "~/Documents/Max 8/Packages/bbdmi/max/utilities/crosspatch",
				"patcherrelativepath" : "../../max/utilities/crosspatch",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bbdmi.crosspatch~.maxpat",
				"bootpath" : "~/Documents/Max 8/Packages/bbdmi/max/utilities/crosspatch~",
				"patcherrelativepath" : "../../max/utilities/crosspatch~",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bbdmi.dac~.maxpat",
				"bootpath" : "~/Documents/Max 8/Packages/bbdmi/max/utilities/dac~",
				"patcherrelativepath" : "../../max/utilities/dac~",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bbdmi.eavi.maxpat",
				"bootpath" : "~/Documents/Max 8/Packages/bbdmi/max/input/eavi",
				"patcherrelativepath" : "../../max/input/eavi",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bbdmi.list2~.maxpat",
				"bootpath" : "~/Documents/Max 8/Packages/bbdmi/max/utilities/list2~",
				"patcherrelativepath" : "../../max/utilities/list2~",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bbdmi.multislider.maxpat",
				"bootpath" : "~/Documents/Max 8/Packages/bbdmi/max/utilities/multislider",
				"patcherrelativepath" : "../../max/utilities/multislider",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bbdmi.onepole~.maxpat",
				"bootpath" : "~/Documents/Max 8/Packages/bbdmi/max/utilities/onepole~",
				"patcherrelativepath" : "../../max/utilities/onepole~",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bbdmi.recorder.maxpat",
				"bootpath" : "~/Documents/Max 8/Packages/bbdmi/max/utilities/recorder",
				"patcherrelativepath" : "../../max/utilities/recorder",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bbdmi.smooth.maxpat",
				"bootpath" : "~/Documents/Max 8/Packages/bbdmi/max/control_processing/smooth",
				"patcherrelativepath" : "../../max/control_processing/smooth",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bbdmi.unicorn.maxpat",
				"bootpath" : "~/Documents/Max 8/Packages/bbdmi/max/input/unicorn",
				"patcherrelativepath" : "../../max/input/unicorn",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "jim-at-a.json",
				"bootpath" : "~/Documents/Max 8/Packages/bbdmi/events/2023-05-24_JIM/preset",
				"patcherrelativepath" : "./preset",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "jim-at-b.json",
				"bootpath" : "~/Documents/Max 8/Packages/bbdmi/events/2023-05-24_JIM/preset",
				"patcherrelativepath" : "./preset",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "jim-at-c.json",
				"bootpath" : "~/Documents/Max 8/Packages/bbdmi/events/2023-05-24_JIM/preset",
				"patcherrelativepath" : "./preset",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "jim-at-d.json",
				"bootpath" : "~/Documents/Max 8/Packages/bbdmi/events/2023-05-24_JIM/preset",
				"patcherrelativepath" : "./preset",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "jim-at-e.json",
				"bootpath" : "~/Documents/Max 8/Packages/bbdmi/events/2023-05-24_JIM/preset",
				"patcherrelativepath" : "./preset",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "mxj.mxo",
				"type" : "iLaX"
			}
, 			{
				"name" : "name.js",
				"bootpath" : "~/Documents/Max 8/Packages/bbdmi/max/source/js",
				"patcherrelativepath" : "../../max/source/js",
				"type" : "TEXT",
				"implicit" : 1
			}
, 			{
				"name" : "p.2xbend.maxpat",
				"bootpath" : "~/Documents/Max 8/Packages/bbdmi/max/output/2xbend",
				"patcherrelativepath" : "../../max/output/2xbend",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "p.calibrate.maxpat",
				"bootpath" : "~/Documents/Max 8/Packages/bbdmi/max/control_processing/calibrate",
				"patcherrelativepath" : "../../max/control_processing/calibrate",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "p.exposer.maxpat",
				"bootpath" : "~/Documents/Max 8/Packages/bbdmi/max/source/patchers",
				"patcherrelativepath" : "../../max/source/patchers",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "p.onepole~.maxpat",
				"bootpath" : "~/Documents/Max 8/Packages/bbdmi/max/utilities/onepole~",
				"patcherrelativepath" : "../../max/utilities/onepole~",
				"type" : "JSON",
				"implicit" : 1
			}
 ],
		"autosave" : 0
	}

}
