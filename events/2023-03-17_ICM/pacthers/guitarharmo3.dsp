import("stdfaust.lib");
n = 8;
coeff = (1, 4/3, 2/3, 4/5, 4/7, 4/9, 3/4, 1/2);
//string length multipliers are:
//1 for the fundamental
//4/3 for the fourth below
//2/3 for the perfect fifth above
//4/5 for the perfect third above
//4/7 for the seventh above
//4/9 for the ninth above
//3/4 for the perfect fourth above
//1/2 for the octave above
length = 1.023; //E note
lengthChange = pow(2, (64 - nentry("[0]reference", 64, 0, 127, 1)) / 48);
factor = hslider("[1]factor", 1, 0.01, 50, 0.01);
pluckPosition = hslider("[2]pluckPosition", 0.5, 0, 1, 0.01);
gain = hslider("[3]gain", 0.2, 0, 1, 0.01) : si.smoo;
stereoShift = nentry("[4]stereoShift", 0, 0, n-1, 1);
toStereo(n, p) = par(i, n, sp.panner(1 - fmod(i+p, n) / (n-1))) :> (_, _);
process = par(i, n, (no.sparse_noise(factor*(1+i*0.01)) : pm.guitar(length*ba.take(i+1, coeff)*lengthChange, pluckPosition, gain))) : toStereo(n, stereoShift);