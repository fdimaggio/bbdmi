{
	"patcher" : 	{
		"fileversion" : 1,
		"appversion" : 		{
			"major" : 8,
			"minor" : 5,
			"revision" : 3,
			"architecture" : "x64",
			"modernui" : 1
		}
,
		"classnamespace" : "box",
		"rect" : [ 34.0, 100.0, 1444.0, 848.0 ],
		"bglocked" : 0,
		"openinpresentation" : 0,
		"default_fontsize" : 12.0,
		"default_fontface" : 0,
		"default_fontname" : "Arial",
		"gridonopen" : 1,
		"gridsize" : [ 15.0, 15.0 ],
		"gridsnaponopen" : 1,
		"objectsnaponopen" : 1,
		"statusbarvisible" : 2,
		"toolbarvisible" : 1,
		"lefttoolbarpinned" : 0,
		"toptoolbarpinned" : 0,
		"righttoolbarpinned" : 0,
		"bottomtoolbarpinned" : 0,
		"toolbars_unpinned_last_save" : 0,
		"tallnewobj" : 0,
		"boxanimatetime" : 200,
		"enablehscroll" : 1,
		"enablevscroll" : 1,
		"devicewidth" : 0.0,
		"description" : "",
		"digest" : "",
		"tags" : "",
		"style" : "",
		"subpatcher_template" : "",
		"assistshowspatchername" : 0,
		"boxes" : [ 			{
				"box" : 				{
					"id" : "obj-39",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1026.666697263717651, 860.000025629997253, 150.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 845.833332657814026, 567.36252490234142, 93.0, 20.0 ],
					"text" : "BBDMI GITLAB"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-44",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 151.800001800060272, 72.215223937748675, 159.0, 22.0 ],
					"text" : "OSC-route /EEGsynth/alpha"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-38",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 151.800001800060272, 41.096590912342194, 97.0, 22.0 ],
					"text" : "udpreceive 8000"
				}

			}
, 			{
				"box" : 				{
					"fontface" : 0,
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-34",
					"items" : [ "off", ",", "Unicorn", ",", "Mentalab" ],
					"maxclass" : "umenu",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "int", "", "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 45.20000159740448, 72.215223937748675, 100.0, 23.0 ],
					"pattrmode" : 1,
					"presentation" : 1,
					"presentation_rect" : [ -59.000005424022675, 67.588183790923239, 100.0, 23.0 ],
					"textcolor" : [ 1.0, 1.0, 1.0, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-32",
					"maxclass" : "newobj",
					"numinlets" : 3,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 45.20000159740448, 130.615225499390363, 52.0, 22.0 ],
					"text" : "switch 2"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-29",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 980.199999928474426, 1212.432955164195391, 50.0, 22.0 ],
					"text" : "-127."
				}

			}
, 			{
				"box" : 				{
					"format" : 6,
					"id" : "obj-52",
					"maxclass" : "flonum",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 558.199999928474426, 584.000000953674316, 50.0, 22.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-47",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 590.399928778409958, 538.000000953674316, 39.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 829.283261507749557, 863.333327531814575, 39.0, 20.0 ],
					"text" : "on-off"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-43",
					"maxclass" : "toggle",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 558.199999928474426, 523.000000953674316, 24.0, 24.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 836.783261507749557, 885.333327531814575, 24.0, 24.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-40",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 558.199999928474426, 560.000000953674316, 32.0, 22.0 ],
					"text" : "gate"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-33",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 601.149928778409958, 1346.000000953674316, 59.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 897.333332657814026, 863.333327531814575, 59.0, 20.0 ],
					"text" : "MODELS"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-10",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 783.199999928474426, 554.000000953674316, 54.0, 22.0 ],
					"text" : "deferlow"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-6",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 783.199999928474426, 524.434772235199034, 58.0, 22.0 ],
					"text" : "loadbang"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-121",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 601.149928778409958, 524.434772235199034, 135.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 829.283261507749557, 656.230281195762245, 135.0, 20.0 ],
					"text" : "ALPHA - State Detector"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-118",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 690.149928778409958, 778.000000953674316, 79.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 887.333332657814026, 834.480281195762245, 79.0, 20.0 ],
					"text" : "model trigger"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-116",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 712.149928778409958, 555.000000953674316, 35.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 881.283261507749557, 701.480281195762245, 35.0, 20.0 ],
					"text" : "reset"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-114",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 682.196765080094451, 1476.562007306151827, 128.0, 22.0 ],
					"text" : "read modelAbove.json"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-113",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 539.196765080094451, 1476.562007306151827, 132.0, 22.0 ],
					"text" : "read modelDefault.json"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-112",
					"maxclass" : "number",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 509.199999928474426, 1374.215047898200737, 50.0, 22.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-110",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 509.199999928474426, 1346.000000953674316, 87.0, 22.0 ],
					"text" : "r chooseModel"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-108",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 569.250071078538895, 1434.430094842727158, 24.0, 24.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 942.333332657814026, 885.333327531814575, 24.0, 24.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-106",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 539.250071078538895, 1434.430094842727158, 24.0, 24.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 914.833332657814026, 885.333327531814575, 24.0, 24.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-104",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 509.250071078538895, 1434.430094842727158, 24.0, 24.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 887.833332657814026, 885.333327531814575, 24.0, 24.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-100",
					"maxclass" : "newobj",
					"numinlets" : 4,
					"numoutlets" : 4,
					"outlettype" : [ "bang", "bang", "bang", "" ],
					"patching_rect" : [ 509.199999928474426, 1402.430094842727158, 109.150213450193291, 22.0 ],
					"text" : "sel -1 0 1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-109",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 601.149928778409958, 812.000000953674316, 89.0, 22.0 ],
					"text" : "s chooseModel"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-102",
					"maxclass" : "number",
					"maximum" : 1,
					"minimum" : -1,
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 601.149928778409958, 778.000000953674316, 50.0, 22.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 829.283261507749557, 833.480281195762245, 50.0, 22.0 ],
					"textcolor" : [ 1.0, 1.0, 1.0, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-99",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 749.149928778409958, 665.000000953674316, 75.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 881.283261507749557, 737.480281195762245, 75.0, 20.0 ],
					"text" : "threshold (s)"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-98",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 737.899928766691346, 633.750000953674316, 41.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 905.783261507749557, 772.230281195762245, 41.0, 20.0 ],
					"text" : "above"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-97",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 556.399928778409958, 633.750000953674316, 40.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 834.283261507749557, 772.230281195762245, 40.0, 20.0 ],
					"text" : "below"
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 1.0, 0.392156862745098, 0.392156862745098, 1.0 ],
					"id" : "obj-94",
					"maxclass" : "number",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 697.149928778409958, 664.000000953674316, 50.0, 22.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 829.283261507749557, 736.480281195762245, 50.0, 22.0 ],
					"textcolor" : [ 1.0, 1.0, 1.0, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-89",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patcher" : 					{
						"fileversion" : 1,
						"appversion" : 						{
							"major" : 8,
							"minor" : 5,
							"revision" : 3,
							"architecture" : "x64",
							"modernui" : 1
						}
,
						"classnamespace" : "box",
						"rect" : [ 59.0, 119.0, 352.0, 480.0 ],
						"bglocked" : 0,
						"openinpresentation" : 0,
						"default_fontsize" : 12.0,
						"default_fontface" : 0,
						"default_fontname" : "Arial",
						"gridonopen" : 1,
						"gridsize" : [ 15.0, 15.0 ],
						"gridsnaponopen" : 1,
						"objectsnaponopen" : 1,
						"statusbarvisible" : 2,
						"toolbarvisible" : 1,
						"lefttoolbarpinned" : 0,
						"toptoolbarpinned" : 0,
						"righttoolbarpinned" : 0,
						"bottomtoolbarpinned" : 0,
						"toolbars_unpinned_last_save" : 0,
						"tallnewobj" : 0,
						"boxanimatetime" : 200,
						"enablehscroll" : 1,
						"enablevscroll" : 1,
						"devicewidth" : 0.0,
						"description" : "",
						"digest" : "",
						"tags" : "",
						"style" : "",
						"subpatcher_template" : "",
						"assistshowspatchername" : 0,
						"boxes" : [ 							{
								"box" : 								{
									"id" : "obj-7",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 3,
									"outlettype" : [ "", "int", "int" ],
									"patching_rect" : [ 114.0, 393.0, 48.0, 22.0 ],
									"text" : "change"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-6",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"patching_rect" : [ 114.0, 357.0, 29.5, 22.0 ],
									"text" : "i"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-5",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 114.0, 308.5, 29.5, 22.0 ],
									"text" : "0"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-3",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "int", "int" ],
									"patching_rect" : [ 196.0, 139.0, 29.5, 22.0 ],
									"text" : "t i i"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-1",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "int", "int" ],
									"patching_rect" : [ 114.0, 139.0, 29.5, 22.0 ],
									"text" : "t i i"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-66",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "bang", "int" ],
									"patching_rect" : [ 196.0, 189.5, 29.5, 22.0 ],
									"text" : "t b i"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-60",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 302.54295742225213, 214.5, 29.5, 22.0 ],
									"text" : "1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-63",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "bang", "" ],
									"patching_rect" : [ 302.492886272187661, 179.5, 34.0, 22.0 ],
									"text" : "sel 1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-58",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 50.050071150064468, 169.5, 29.5, 22.0 ],
									"text" : "-1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-53",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "bang", "" ],
									"patching_rect" : [ 50.0, 134.5, 34.0, 22.0 ],
									"text" : "sel 1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-52",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 114.0, 268.5, 184.0, 22.0 ],
									"text" : "if $i1 == 0 && $i2 == 0 then bang"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-45",
									"maxclass" : "toggle",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 196.0, 100.0, 24.0, 24.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-41",
									"maxclass" : "toggle",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 114.0, 100.0, 24.0, 24.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-84",
									"index" : 1,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"patching_rect" : [ 114.000022150064524, 40.0, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-86",
									"index" : 2,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"patching_rect" : [ 196.000022150064524, 40.0, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-88",
									"index" : 1,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 114.0, 428.5, 30.0, 30.0 ]
								}

							}
 ],
						"lines" : [ 							{
								"patchline" : 								{
									"destination" : [ "obj-52", 0 ],
									"source" : [ "obj-1", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-53", 0 ],
									"source" : [ "obj-1", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-63", 0 ],
									"source" : [ "obj-3", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-66", 0 ],
									"source" : [ "obj-3", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-1", 0 ],
									"source" : [ "obj-41", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-3", 0 ],
									"source" : [ "obj-45", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-6", 0 ],
									"source" : [ "obj-5", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-5", 0 ],
									"source" : [ "obj-52", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-58", 0 ],
									"source" : [ "obj-53", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-6", 0 ],
									"source" : [ "obj-58", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-7", 0 ],
									"source" : [ "obj-6", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-6", 0 ],
									"source" : [ "obj-60", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-60", 0 ],
									"source" : [ "obj-63", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-52", 1 ],
									"source" : [ "obj-66", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-52", 0 ],
									"source" : [ "obj-66", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-88", 0 ],
									"source" : [ "obj-7", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-41", 0 ],
									"source" : [ "obj-84", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-45", 0 ],
									"source" : [ "obj-86", 0 ]
								}

							}
 ]
					}
,
					"patching_rect" : [ 601.149928778409958, 746.500000953674316, 101.0, 22.0 ],
					"saved_object_attributes" : 					{
						"description" : "",
						"digest" : "",
						"globalpatchername" : "",
						"tags" : ""
					}
,
					"text" : "p condition"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-79",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 683.149928778409958, 553.000000953674316, 24.0, 24.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 829.283261507749557, 682.277054909110348, 49.203226286651898, 49.203226286651898 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-25",
					"maxclass" : "number",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 683.149928778409958, 631.750000953674316, 50.0, 22.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 901.283261507749557, 794.230281195762245, 50.0, 22.0 ],
					"textcolor" : [ 1.0, 1.0, 1.0, 1.0 ],
					"triangle" : 0
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-31",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"patching_rect" : [ 683.149928778409958, 710.000000953674316, 33.0, 22.0 ],
					"text" : ">= 8"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-23",
					"maxclass" : "number",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 601.149928778409958, 631.750000953674316, 50.0, 22.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 829.283261507749557, 794.230281195762245, 50.0, 22.0 ],
					"textcolor" : [ 1.0, 1.0, 1.0, 1.0 ],
					"triangle" : 0
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-11",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"patching_rect" : [ 601.149928778409958, 710.000000953674316, 33.0, 22.0 ],
					"text" : ">= 8"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-85",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 396.196765080094451, 1476.562007306151827, 126.0, 22.0 ],
					"text" : "read modelBelow.json"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-83",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 628.463439717888946, 1402.430094842727158, 58.0, 22.0 ],
					"text" : "loadbang"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-82",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "float" ],
					"patching_rect" : [ 1226.199999928474426, 375.000000953674316, 31.0, 22.0 ],
					"text" : "* -1."
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-5",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "int", "int" ],
					"patching_rect" : [ 601.149928778409958, 604.000000953674316, 101.0, 22.0 ],
					"text" : "bbdmi.timethresh"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-300",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 113.366668283939362, 873.000000953674316, 71.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 455.6333367228508, 220.796771269557667, 71.0, 20.0 ],
					"text" : "To mapping"
				}

			}
, 			{
				"box" : 				{
					"format" : 6,
					"id" : "obj-298",
					"maxclass" : "flonum",
					"maximum" : 1.0,
					"minimum" : 0.0,
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 123.866668283939362, 898.000000953674316, 50.0, 22.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 466.1333367228508, 245.796771269557667, 50.0, 22.0 ],
					"textcolor" : [ 1.0, 1.0, 1.0, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-296",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 278.199999928474426, 1476.562007306151827, 54.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 207.666667699813843, 585.333327531814575, 54.0, 20.0 ],
					"text" : "ML input"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-292",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patcher" : 					{
						"fileversion" : 1,
						"appversion" : 						{
							"major" : 8,
							"minor" : 5,
							"revision" : 3,
							"architecture" : "x64",
							"modernui" : 1
						}
,
						"classnamespace" : "box",
						"rect" : [ 59.0, 106.0, 640.0, 480.0 ],
						"bglocked" : 0,
						"openinpresentation" : 0,
						"default_fontsize" : 12.0,
						"default_fontface" : 0,
						"default_fontname" : "Arial",
						"gridonopen" : 1,
						"gridsize" : [ 15.0, 15.0 ],
						"gridsnaponopen" : 1,
						"objectsnaponopen" : 1,
						"statusbarvisible" : 2,
						"toolbarvisible" : 1,
						"lefttoolbarpinned" : 0,
						"toptoolbarpinned" : 0,
						"righttoolbarpinned" : 0,
						"bottomtoolbarpinned" : 0,
						"toolbars_unpinned_last_save" : 0,
						"tallnewobj" : 0,
						"boxanimatetime" : 200,
						"enablehscroll" : 1,
						"enablevscroll" : 1,
						"devicewidth" : 0.0,
						"description" : "",
						"digest" : "",
						"tags" : "",
						"style" : "",
						"subpatcher_template" : "",
						"assistshowspatchername" : 0,
						"boxes" : [ 							{
								"box" : 								{
									"id" : "obj-29",
									"maxclass" : "newobj",
									"numinlets" : 5,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 50.0, 310.113972431900152, 98.0, 22.0 ],
									"text" : "zmap 0. 30. 0. 1."
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-39",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 120.0, 277.313971943142064, 38.0, 20.0 ],
									"text" : "alpha"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-38",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 89.0, 207.0, 50.0, 22.0 ],
									"text" : "70"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-40",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "", "" ],
									"patching_rect" : [ 120.0, 177.0, 37.0, 22.0 ],
									"text" : "zl.len"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-41",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 50.0, 140.0, 143.0, 22.0 ],
									"text" : "fromsymbol @separator \\,"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-10",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "", "" ],
									"patching_rect" : [ 50.0, 277.313971943142064, 54.0, 22.0 ],
									"text" : "zl.nth 59"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 12.0,
									"id" : "obj-52",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "" ],
									"patching_rect" : [ 50.0, 100.0, 163.0, 22.0 ],
									"text" : "mxj net.udp.recv @port 8888"
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-291",
									"index" : 1,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 50.0, 392.113983000000019, 30.0, 30.0 ]
								}

							}
 ],
						"lines" : [ 							{
								"patchline" : 								{
									"destination" : [ "obj-29", 0 ],
									"source" : [ "obj-10", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-291", 0 ],
									"source" : [ "obj-29", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-38", 1 ],
									"source" : [ "obj-40", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-10", 0 ],
									"order" : 1,
									"source" : [ "obj-41", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-40", 0 ],
									"order" : 0,
									"source" : [ "obj-41", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-41", 0 ],
									"source" : [ "obj-52", 0 ]
								}

							}
 ]
					}
,
					"patching_rect" : [ 61.600006604194618, 42.516643261910218, 84.0, 22.0 ],
					"saved_object_attributes" : 					{
						"description" : "",
						"digest" : "",
						"globalpatchername" : "",
						"tags" : ""
					}
,
					"text" : "p fromUnicorn"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-289",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 311.642815050597619, 763.500000953674316, 80.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 928.917504192427259, 935.666694343090057, 80.0, 20.0 ],
					"text" : "EEG Joystick"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-288",
					"maxclass" : "newobj",
					"numinlets" : 10,
					"numoutlets" : 1,
					"outlettype" : [ "float" ],
					"patcher" : 					{
						"fileversion" : 1,
						"appversion" : 						{
							"major" : 8,
							"minor" : 5,
							"revision" : 3,
							"architecture" : "x64",
							"modernui" : 1
						}
,
						"classnamespace" : "box",
						"rect" : [ 0.0, 0.0, 640.0, 480.0 ],
						"bglocked" : 0,
						"openinpresentation" : 0,
						"default_fontsize" : 12.0,
						"default_fontface" : 0,
						"default_fontname" : "Arial",
						"gridonopen" : 1,
						"gridsize" : [ 15.0, 15.0 ],
						"gridsnaponopen" : 1,
						"objectsnaponopen" : 1,
						"statusbarvisible" : 2,
						"toolbarvisible" : 1,
						"lefttoolbarpinned" : 0,
						"toptoolbarpinned" : 0,
						"righttoolbarpinned" : 0,
						"bottomtoolbarpinned" : 0,
						"toolbars_unpinned_last_save" : 0,
						"tallnewobj" : 0,
						"boxanimatetime" : 200,
						"enablehscroll" : 1,
						"enablevscroll" : 1,
						"devicewidth" : 0.0,
						"description" : "",
						"digest" : "",
						"tags" : "",
						"style" : "",
						"subpatcher_template" : "",
						"assistshowspatchername" : 0,
						"boxes" : [ 							{
								"box" : 								{
									"id" : "obj-193",
									"maxclass" : "newobj",
									"numinlets" : 10,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patcher" : 									{
										"fileversion" : 1,
										"appversion" : 										{
											"major" : 8,
											"minor" : 5,
											"revision" : 3,
											"architecture" : "x64",
											"modernui" : 1
										}
,
										"classnamespace" : "box",
										"rect" : [ 0.0, 0.0, 640.0, 480.0 ],
										"bglocked" : 0,
										"openinpresentation" : 0,
										"default_fontsize" : 12.0,
										"default_fontface" : 0,
										"default_fontname" : "Arial",
										"gridonopen" : 1,
										"gridsize" : [ 15.0, 15.0 ],
										"gridsnaponopen" : 1,
										"objectsnaponopen" : 1,
										"statusbarvisible" : 2,
										"toolbarvisible" : 1,
										"lefttoolbarpinned" : 0,
										"toptoolbarpinned" : 0,
										"righttoolbarpinned" : 0,
										"bottomtoolbarpinned" : 0,
										"toolbars_unpinned_last_save" : 0,
										"tallnewobj" : 0,
										"boxanimatetime" : 200,
										"enablehscroll" : 1,
										"enablevscroll" : 1,
										"devicewidth" : 0.0,
										"description" : "",
										"digest" : "",
										"tags" : "",
										"style" : "",
										"subpatcher_template" : "",
										"assistshowspatchername" : 0,
										"boxes" : [ 											{
												"box" : 												{
													"id" : "obj-55",
													"maxclass" : "message",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 50.0, 137.0, 42.0, 22.0 ],
													"text" : "127. 0"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-9",
													"maxclass" : "message",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 270.25, 134.0, 75.0, 22.0 ],
													"text" : "127. 360000"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-10",
													"maxclass" : "message",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 270.25, 107.0, 62.0, 22.0 ],
													"text" : "0. 360000"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-3",
													"maxclass" : "message",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 50.0, 100.0, 29.5, 22.0 ],
													"text" : "0. 0"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-109",
													"maxclass" : "message",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 389.583333611488342, 134.0, 75.0, 22.0 ],
													"text" : "127. 600000"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-110",
													"maxclass" : "message",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 389.583333611488342, 107.0, 62.0, 22.0 ],
													"text" : "0. 600000"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-107",
													"maxclass" : "message",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 180.25, 134.0, 75.0, 22.0 ],
													"text" : "127. 120000"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-108",
													"maxclass" : "message",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 180.25, 107.0, 62.0, 22.0 ],
													"text" : "0. 120000"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-85",
													"maxclass" : "message",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 104.25, 134.0, 69.0, 22.0 ],
													"text" : "127. 60000"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-84",
													"maxclass" : "message",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 104.25, 107.0, 55.0, 22.0 ],
													"text" : "0. 60000"
												}

											}
, 											{
												"box" : 												{
													"comment" : "",
													"id" : "obj-119",
													"index" : 1,
													"maxclass" : "inlet",
													"numinlets" : 0,
													"numoutlets" : 1,
													"outlettype" : [ "bang" ],
													"patching_rect" : [ 50.0, 39.999999231979331, 30.0, 30.0 ]
												}

											}
, 											{
												"box" : 												{
													"comment" : "",
													"id" : "obj-120",
													"index" : 2,
													"maxclass" : "inlet",
													"numinlets" : 0,
													"numoutlets" : 1,
													"outlettype" : [ "bang" ],
													"patching_rect" : [ 85.0, 39.999999231979331, 30.0, 30.0 ]
												}

											}
, 											{
												"box" : 												{
													"comment" : "",
													"id" : "obj-121",
													"index" : 3,
													"maxclass" : "inlet",
													"numinlets" : 0,
													"numoutlets" : 1,
													"outlettype" : [ "bang" ],
													"patching_rect" : [ 120.0, 39.999999231979331, 30.0, 30.0 ]
												}

											}
, 											{
												"box" : 												{
													"comment" : "",
													"id" : "obj-122",
													"index" : 4,
													"maxclass" : "inlet",
													"numinlets" : 0,
													"numoutlets" : 1,
													"outlettype" : [ "bang" ],
													"patching_rect" : [ 155.0, 39.999999231979331, 30.0, 30.0 ]
												}

											}
, 											{
												"box" : 												{
													"comment" : "",
													"id" : "obj-123",
													"index" : 5,
													"maxclass" : "inlet",
													"numinlets" : 0,
													"numoutlets" : 1,
													"outlettype" : [ "bang" ],
													"patching_rect" : [ 190.0, 39.999999231979331, 30.0, 30.0 ]
												}

											}
, 											{
												"box" : 												{
													"comment" : "",
													"id" : "obj-124",
													"index" : 6,
													"maxclass" : "inlet",
													"numinlets" : 0,
													"numoutlets" : 1,
													"outlettype" : [ "bang" ],
													"patching_rect" : [ 225.0, 39.999999231979331, 30.0, 30.0 ]
												}

											}
, 											{
												"box" : 												{
													"comment" : "",
													"id" : "obj-125",
													"index" : 7,
													"maxclass" : "inlet",
													"numinlets" : 0,
													"numoutlets" : 1,
													"outlettype" : [ "bang" ],
													"patching_rect" : [ 270.25, 39.999999231979331, 30.0, 30.0 ]
												}

											}
, 											{
												"box" : 												{
													"comment" : "",
													"id" : "obj-126",
													"index" : 8,
													"maxclass" : "inlet",
													"numinlets" : 0,
													"numoutlets" : 1,
													"outlettype" : [ "bang" ],
													"patching_rect" : [ 305.25, 39.999999231979331, 30.0, 30.0 ]
												}

											}
, 											{
												"box" : 												{
													"comment" : "",
													"id" : "obj-127",
													"index" : 9,
													"maxclass" : "inlet",
													"numinlets" : 0,
													"numoutlets" : 1,
													"outlettype" : [ "bang" ],
													"patching_rect" : [ 389.583344000000011, 39.999999231979331, 30.0, 30.0 ]
												}

											}
, 											{
												"box" : 												{
													"comment" : "",
													"id" : "obj-128",
													"index" : 10,
													"maxclass" : "inlet",
													"numinlets" : 0,
													"numoutlets" : 1,
													"outlettype" : [ "bang" ],
													"patching_rect" : [ 424.583344000000011, 39.999999231979331, 30.0, 30.0 ]
												}

											}
, 											{
												"box" : 												{
													"comment" : "",
													"id" : "obj-129",
													"index" : 1,
													"maxclass" : "outlet",
													"numinlets" : 1,
													"numoutlets" : 0,
													"patching_rect" : [ 209.407409999999999, 218.999999231979331, 30.0, 30.0 ]
												}

											}
 ],
										"lines" : [ 											{
												"patchline" : 												{
													"destination" : [ "obj-129", 0 ],
													"source" : [ "obj-10", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-129", 0 ],
													"source" : [ "obj-107", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-129", 0 ],
													"source" : [ "obj-108", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-129", 0 ],
													"source" : [ "obj-109", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-129", 0 ],
													"source" : [ "obj-110", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-3", 0 ],
													"source" : [ "obj-119", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-55", 0 ],
													"source" : [ "obj-120", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-84", 0 ],
													"source" : [ "obj-121", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-85", 0 ],
													"source" : [ "obj-122", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-108", 0 ],
													"source" : [ "obj-123", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-107", 0 ],
													"source" : [ "obj-124", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-10", 0 ],
													"source" : [ "obj-125", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-9", 0 ],
													"source" : [ "obj-126", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-110", 0 ],
													"source" : [ "obj-127", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-109", 0 ],
													"source" : [ "obj-128", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-129", 0 ],
													"source" : [ "obj-3", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-129", 0 ],
													"source" : [ "obj-84", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-129", 0 ],
													"source" : [ "obj-85", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-129", 0 ],
													"source" : [ "obj-9", 0 ]
												}

											}
 ]
									}
,
									"patching_rect" : [ 50.0, 100.0, 113.5, 22.0 ],
									"saved_object_attributes" : 									{
										"description" : "",
										"digest" : "",
										"globalpatchername" : "",
										"tags" : ""
									}
,
									"text" : "p lines"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-219",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "float" ],
									"patching_rect" : [ 50.0, 167.198597180843308, 39.0, 22.0 ],
									"text" : "/ 127."
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-237",
									"maxclass" : "newobj",
									"numinlets" : 3,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"patching_rect" : [ 50.0, 137.865264463424637, 67.0, 22.0 ],
									"text" : "line 20000."
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-277",
									"index" : 1,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"patching_rect" : [ 49.999969667026562, 40.000013018795016, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-278",
									"index" : 2,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"patching_rect" : [ 84.999969667026562, 40.000013018795016, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-279",
									"index" : 3,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"patching_rect" : [ 119.999969667026562, 40.000013018795016, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-280",
									"index" : 4,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"patching_rect" : [ 154.999969667026562, 40.000013018795016, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-281",
									"index" : 5,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"patching_rect" : [ 189.999969667026562, 40.000013018795016, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-282",
									"index" : 6,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"patching_rect" : [ 224.999969667026562, 40.000013018795016, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-283",
									"index" : 7,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"patching_rect" : [ 259.999969667026562, 40.000013018795016, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-284",
									"index" : 8,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"patching_rect" : [ 294.999969667026562, 40.000013018795016, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-285",
									"index" : 9,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"patching_rect" : [ 329.999969667026562, 40.000013018795016, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-286",
									"index" : 10,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"patching_rect" : [ 364.999969667026562, 40.000013018795016, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-287",
									"index" : 1,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 49.999969667026562, 249.198621018794938, 30.0, 30.0 ]
								}

							}
 ],
						"lines" : [ 							{
								"patchline" : 								{
									"destination" : [ "obj-237", 0 ],
									"midpoints" : [ 59.5, 123.715974676608994, 59.5, 123.715974676608994 ],
									"source" : [ "obj-193", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-287", 0 ],
									"source" : [ "obj-219", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-219", 0 ],
									"midpoints" : [ 59.5, 162.715974676608994, 59.5, 162.715974676608994 ],
									"source" : [ "obj-237", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-193", 0 ],
									"source" : [ "obj-277", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-193", 1 ],
									"source" : [ "obj-278", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-193", 2 ],
									"source" : [ "obj-279", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-193", 3 ],
									"source" : [ "obj-280", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-193", 4 ],
									"source" : [ "obj-281", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-193", 5 ],
									"source" : [ "obj-282", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-193", 6 ],
									"source" : [ "obj-283", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-193", 7 ],
									"source" : [ "obj-284", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-193", 8 ],
									"source" : [ "obj-285", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-193", 9 ],
									"source" : [ "obj-286", 0 ]
								}

							}
 ]
					}
,
					"patching_rect" : [ 231.899928778410072, 1349.430094842727158, 113.5, 22.0 ],
					"saved_object_attributes" : 					{
						"description" : "",
						"digest" : "",
						"globalpatchername" : "",
						"tags" : ""
					}
,
					"text" : "p TimeLines"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-269",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 109.399928778410072, 1414.430094842727158, 70.0, 22.0 ],
					"text" : "loadmess 2"
				}

			}
, 			{
				"box" : 				{
					"fontface" : 0,
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-271",
					"items" : [ "off", ",", "Time", ",", "EEG" ],
					"maxclass" : "umenu",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "int", "", "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 109.399928778410072, 1441.430094842727158, 100.0, 23.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 179.466667652130127, 554.36252490234142, 100.0, 23.0 ],
					"textcolor" : [ 1.0, 1.0, 1.0, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 11.595186999999999,
					"id" : "obj-272",
					"maxclass" : "newobj",
					"numinlets" : 3,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 216.399928778410072, 1441.430094842727158, 50.0, 21.0 ],
					"text" : "switch 2"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-276",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 211.399928778410072, 1373.430094842727158, 81.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 188.966667652130127, 529.36252490234142, 81.0, 20.0 ],
					"text" : "Time/Joystick"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-261",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 16.866668283939362, 775.500000953674316, 70.0, 22.0 ],
					"text" : "loadmess 2"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-260",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 139.366668283939362, 730.817384130241408, 93.0, 22.0 ],
					"text" : "r EEGcalibrated"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-259",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 45.20000159740448, 490.752155411766125, 95.0, 22.0 ],
					"text" : "s EEGcalibrated"
				}

			}
, 			{
				"box" : 				{
					"fontface" : 0,
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-252",
					"items" : [ "off", ",", "Calibrated", ",", "Joystick" ],
					"maxclass" : "umenu",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "int", "", "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 16.866668283939362, 802.500000953674316, 100.0, 23.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 441.1333367228508, 198.999997556209564, 100.0, 23.0 ],
					"textcolor" : [ 1.0, 1.0, 1.0, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 11.595186999999999,
					"id" : "obj-26",
					"maxclass" : "newobj",
					"numinlets" : 3,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 123.866668283939362, 802.500000953674316, 50.0, 21.0 ],
					"text" : "switch 2"
				}

			}
, 			{
				"box" : 				{
					"format" : 6,
					"id" : "obj-250",
					"maxclass" : "flonum",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 45.20000159740448, 340.701420253994002, 50.0, 22.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-248",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "float" ],
					"patcher" : 					{
						"fileversion" : 1,
						"appversion" : 						{
							"major" : 8,
							"minor" : 5,
							"revision" : 3,
							"architecture" : "x64",
							"modernui" : 1
						}
,
						"classnamespace" : "box",
						"rect" : [ 0.0, 0.0, 640.0, 480.0 ],
						"bglocked" : 0,
						"openinpresentation" : 0,
						"default_fontsize" : 12.0,
						"default_fontface" : 0,
						"default_fontname" : "Arial",
						"gridonopen" : 1,
						"gridsize" : [ 15.0, 15.0 ],
						"gridsnaponopen" : 1,
						"objectsnaponopen" : 1,
						"statusbarvisible" : 2,
						"toolbarvisible" : 1,
						"lefttoolbarpinned" : 0,
						"toptoolbarpinned" : 0,
						"righttoolbarpinned" : 0,
						"bottomtoolbarpinned" : 0,
						"toolbars_unpinned_last_save" : 0,
						"tallnewobj" : 0,
						"boxanimatetime" : 200,
						"enablehscroll" : 1,
						"enablevscroll" : 1,
						"devicewidth" : 0.0,
						"description" : "",
						"digest" : "",
						"tags" : "",
						"style" : "",
						"subpatcher_template" : "",
						"assistshowspatchername" : 0,
						"boxes" : [ 							{
								"box" : 								{
									"id" : "obj-155",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 395.833364963531494, 153.706809508800461, 80.0, 22.0 ],
									"text" : "loadmess 0.1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-140",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 281.333364963531494, 153.706809508800461, 107.0, 22.0 ],
									"text" : "loadmess 0.00632"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-139",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 186.333364963531494, 153.706809508800461, 77.0, 22.0 ],
									"text" : "loadmess 10"
								}

							}
, 							{
								"box" : 								{
									"format" : 6,
									"id" : "obj-88",
									"maxclass" : "flonum",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 50.0, 100.0, 50.0, 22.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-170",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 88.0, 153.706809508800461, 80.0, 22.0 ],
									"text" : "loadmess 0.5"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-53",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 88.0, 182.512198847053241, 45.0, 20.0 ],
									"text" : "restVal"
								}

							}
, 							{
								"box" : 								{
									"fontsize" : 13.0,
									"format" : 6,
									"id" : "obj-67",
									"maxclass" : "flonum",
									"maximum" : 1.0,
									"minimum" : 0.0,
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 88.0, 210.212208228824352, 80.0, 23.0 ],
									"triscale" : 0.9
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 9.0,
									"id" : "obj-68",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 88.0, 242.942132788417666, 48.0, 19.0 ],
									"text" : "restVal $1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-69",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 381.833364963531494, 182.512198847053241, 57.0, 20.0 ],
									"text" : "restDelta"
								}

							}
, 							{
								"box" : 								{
									"fontsize" : 13.0,
									"format" : 6,
									"id" : "obj-71",
									"maxclass" : "flonum",
									"maximum" : 0.5,
									"minimum" : 0.0,
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 370.333364963531494, 210.212208228824352, 80.0, 23.0 ],
									"triscale" : 0.9
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 9.0,
									"id" : "obj-72",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 370.333364963531494, 242.942132788417666, 93.0, 19.0 ],
									"text" : "restDelta $1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-73",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 281.333364963531494, 182.512198847053241, 92.0, 20.0 ],
									"text" : "derivativeDepth"
								}

							}
, 							{
								"box" : 								{
									"fontsize" : 13.0,
									"format" : 6,
									"id" : "obj-75",
									"maxclass" : "flonum",
									"maximum" : 100.0,
									"minimum" : 0.0,
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 281.333364963531494, 210.212208228824352, 80.0, 23.0 ],
									"triscale" : 0.9
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 9.0,
									"id" : "obj-76",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 281.333364963531494, 245.43299627304043, 84.0, 19.0 ],
									"text" : "derivativeDepth $1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-77",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 186.333364963531494, 182.512198847053241, 19.0, 20.0 ],
									"text" : "dt"
								}

							}
, 							{
								"box" : 								{
									"fontsize" : 13.0,
									"format" : 6,
									"id" : "obj-89",
									"maxclass" : "flonum",
									"maximum" : 1000.0,
									"minimum" : 1.0,
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 186.333364963531494, 210.212208228824352, 80.0, 23.0 ],
									"triscale" : 0.9
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 9.0,
									"id" : "obj-90",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 186.333364963531494, 242.942132788417666, 29.5, 19.0 ],
									"text" : "dt $1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-60",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "float" ],
									"patching_rect" : [ 88.0, 331.31220043253677, 81.0, 22.0 ],
									"text" : "snapshot~ 10"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-93",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 50.0, 129.706809508800347, 31.0, 22.0 ],
									"text" : "sig~"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 12.0,
									"id" : "obj-449",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "signal", "list" ],
									"patching_rect" : [ 88.0, 303.71220995735905, 92.0, 22.0 ],
									"text" : "AlainJoystick2~"
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-246",
									"index" : 1,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 49.999990963531445, 40.000022508800498, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-247",
									"index" : 1,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 87.999990963531445, 413.312156508800399, 30.0, 30.0 ]
								}

							}
 ],
						"lines" : [ 							{
								"patchline" : 								{
									"destination" : [ "obj-89", 0 ],
									"source" : [ "obj-139", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-75", 0 ],
									"source" : [ "obj-140", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-71", 0 ],
									"source" : [ "obj-155", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-67", 0 ],
									"source" : [ "obj-170", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-88", 0 ],
									"source" : [ "obj-246", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-60", 0 ],
									"source" : [ "obj-449", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-247", 0 ],
									"source" : [ "obj-60", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-68", 0 ],
									"hidden" : 1,
									"source" : [ "obj-67", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-449", 0 ],
									"hidden" : 1,
									"source" : [ "obj-68", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-72", 0 ],
									"hidden" : 1,
									"source" : [ "obj-71", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-449", 0 ],
									"hidden" : 1,
									"source" : [ "obj-72", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-76", 0 ],
									"hidden" : 1,
									"source" : [ "obj-75", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-449", 0 ],
									"hidden" : 1,
									"source" : [ "obj-76", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-93", 0 ],
									"source" : [ "obj-88", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-90", 0 ],
									"hidden" : 1,
									"source" : [ "obj-89", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-449", 0 ],
									"hidden" : 1,
									"source" : [ "obj-90", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-449", 0 ],
									"source" : [ "obj-93", 0 ]
								}

							}
 ]
					}
,
					"patching_rect" : [ 154.866668283939362, 763.500000953674316, 87.0, 22.0 ],
					"saved_object_attributes" : 					{
						"description" : "",
						"digest" : "",
						"globalpatchername" : "",
						"tags" : ""
					}
,
					"text" : "p AlainJoystick"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-241",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 176.700001597404366, 150.000000417232513, 64.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 157.0, 7.0, 79.0, 20.0 ],
					"text" : "EEG Input"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-197",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 470.507381111383552, 1301.16344682393219, 24.0, 24.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 582.574119985103607, 567.36252490234142, 24.0, 24.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-199",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 470.507381111383552, 1272.16344682393219, 24.0, 24.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 582.574119985103607, 538.36252490234142, 24.0, 24.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-200",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 406.399928778410072, 1301.16344682393219, 24.0, 24.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 518.466667652130127, 567.36252490234142, 24.0, 24.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-202",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 406.399928778410072, 1272.16344682393219, 24.0, 24.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 518.466667652130127, 538.36252490234142, 24.0, 24.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-203",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 343.399928778410072, 1301.16344682393219, 24.0, 24.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 455.466667652130127, 567.36252490234142, 24.0, 24.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-205",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 343.399928778410072, 1272.16344682393219, 24.0, 24.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 455.466667652130127, 538.36252490234142, 24.0, 24.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-206",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 283.399928778410072, 1301.16344682393219, 24.0, 24.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 395.466667652130127, 567.36252490234142, 24.0, 24.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-208",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 283.399928778410072, 1272.16344682393219, 24.0, 24.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 395.466667652130127, 538.36252490234142, 24.0, 24.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-209",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 231.899928778410072, 1301.16344682393219, 24.0, 24.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 343.966667652130127, 567.36252490234142, 24.0, 24.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-210",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 206.649928778410072, 1303.16344682393219, 19.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 318.716667652130127, 569.36252490234142, 19.0, 20.0 ],
					"text" : "1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-211",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 206.649928778410072, 1274.16344682393219, 19.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 318.716667652130127, 540.36252490234142, 19.0, 20.0 ],
					"text" : "0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-214",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 231.899928778410072, 1272.16344682393219, 24.0, 24.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 343.966667652130127, 538.36252490234142, 24.0, 24.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-220",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 221.899928778410072, 1243.16344682393219, 44.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 333.966667652130127, 509.36252490234142, 44.0, 20.0 ],
					"text" : "instant"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-233",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 449.007381111383552, 1243.16344682393219, 67.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 561.074119985103607, 509.36252490234142, 67.0, 20.0 ],
					"text" : "10 minutes"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-234",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 387.899928778410072, 1243.16344682393219, 61.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 499.966667652130127, 509.36252490234142, 61.0, 20.0 ],
					"text" : "3 minutes"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-235",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 324.899928778410072, 1243.16344682393219, 61.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 436.966667652130127, 509.36252490234142, 61.0, 20.0 ],
					"text" : "2 minutes"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-236",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 267.899928778410072, 1243.16344682393219, 55.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 379.966667652130127, 509.36252490234142, 55.0, 20.0 ],
					"text" : "1 minute"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-184",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1476.280934866304733, 105.698582404373155, 77.0, 22.0 ],
					"text" : "r pluckGuitar"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-172",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 1387.280934866304733, 315.698582404373155, 40.0, 22.0 ],
					"text" : "*~ 0.5"
				}

			}
, 			{
				"box" : 				{
					"format" : 6,
					"id" : "obj-169",
					"maxclass" : "flonum",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 216.399928778410072, 1476.562007306151827, 50.0, 22.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 207.666667699813843, 607.333327531814575, 50.0, 22.0 ],
					"textcolor" : [ 1.0, 1.0, 1.0, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-166",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 247.399928778410072, 1402.430094842727158, 32.0, 22.0 ],
					"text" : "r ML"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-161",
					"linecount" : 18,
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1681.866655945777893, 828.000000953674316, 214.0, 263.0 ],
					"text" : ";\rinstr transpout 12.079097;\rinstr feedback 0.803479;\rinstr gain 1.;\rinstr variability 0.057971;\rinstr transpgrain 2.;\rinstr modfreqmod 0.138644;\rinstr modmorph 1.25175;\rinstr modfreq 6303.972739;\rinstr modfactor 0.903244;\rinstr spacing 668;\rinstr grainoffset 468;\rinstr grainsize 688;\rinstr maxdelay 1626.286065;\rinstr lpffreq 17798.063684;\rinstr hpffreq 676.093274;\rinstr grainenvmorph 0.035929;\rinstr indexdistr 6;\r"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-159",
					"maxclass" : "meter~",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "float" ],
					"patching_rect" : [ 1496.199999928474426, 1110.000000953674316, 78.59999942779541, 97.800000011920929 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-158",
					"linecount" : 18,
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2136.199999928474426, 828.000000953674316, 214.0, 263.0 ],
					"text" : ";\rinstr transpout -3.659462;\rinstr feedback 0.378896;\rinstr gain 1.;\rinstr variability 0.971125;\rinstr transpgrain 1.547342;\rinstr modfreqmod 0.229642;\rinstr modmorph 1.739886;\rinstr modfreq 886.140406;\rinstr modfactor 0.074165;\rinstr spacing 26;\rinstr grainoffset 390;\rinstr grainsize 80;\rinstr maxdelay 1908.8943;\rinstr lpffreq 20000.;\rinstr hpffreq 20.;\rinstr grainenvmorph 0.97429;\rinstr indexdistr 5;\r"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-137",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "multichannelsignal" ],
					"patching_rect" : [ 1405.614268199637991, 1079.000000953674316, 53.0, 22.0 ],
					"text" : "mc.*~ 2."
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-135",
					"linecount" : 18,
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1905.533340096473694, 828.000000953674316, 214.0, 263.0 ],
					"text" : ";\rinstr transpout -0.935548;\rinstr feedback 0.378165;\rinstr gain 1.;\rinstr variability 0.715379;\rinstr transpgrain 24.248604;\rinstr modfreqmod 0.303656;\rinstr modmorph 2.406309;\rinstr modfreq 10260.421883;\rinstr modfactor 0.117533;\rinstr spacing 773;\rinstr grainoffset 842;\rinstr grainsize 383;\rinstr maxdelay 490.;\rinstr lpffreq 19143.983002;\rinstr hpffreq 1011.509643;\rinstr grainenvmorph 0.604853;\rinstr indexdistr 6;\r"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-124",
					"linecount" : 18,
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1676.199999928474426, 1106.200713748218277, 214.0, 263.0 ],
					"text" : ";\rinstr transpout -32.404112;\rinstr feedback 0.77;\rinstr gain 1.;\rinstr variability 0.706869;\rinstr transpgrain -22.594593;\rinstr modfreqmod 0.732305;\rinstr modmorph 0.641584;\rinstr modfreq 5449.36574;\rinstr modfactor 0.163719;\rinstr spacing 891;\rinstr grainoffset 356;\rinstr grainsize 270;\rinstr maxdelay 9121.055892;\rinstr lpffreq 13283.696337;\rinstr hpffreq 1552.110202;\rinstr grainenvmorph 0.466103;\rinstr indexdistr 12;\r"
				}

			}
, 			{
				"box" : 				{
					"args" : [ "@time", 15 ],
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-59",
					"lockeddragscroll" : 0,
					"lockedsize" : 0,
					"maxclass" : "bpatcher",
					"name" : "bbdmi.calibrate.maxpat",
					"numinlets" : 2,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "" ],
					"patching_rect" : [ 45.20000159740448, 373.018803430561093, 198.0, 113.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 385.000001668930054, 51.698581450698839, 198.0, 113.0 ],
					"varname" : "bbdmi.calibrate[1]",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-28",
					"maxclass" : "multislider",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 45.20000159740448, 524.434772235199034, 352.0, 159.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 595.163439795374984, 32.5, 352.0, 159.0 ],
					"setminmax" : [ 0.0, 1.0 ],
					"setstyle" : 3
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-81",
					"maxclass" : "multislider",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 212.642815050597619, 790.000000953674316, 278.0, 186.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 794.417504192427259, 961.166694343090057, 349.0, 158.0 ],
					"setminmax" : [ 0.0, 1.0 ],
					"setstyle" : 3
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-70",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 51.800006949901558, 20.511813967942118, 103.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ -59.000005424022675, 40.400000751018524, 103.0, 20.0 ],
					"text" : "Unicorn/Mentalab"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-56",
					"maxclass" : "multislider",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 45.20000159740448, 182.400001704692841, 320.0, 133.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 49.0, 41.698581450698839, 320.0, 133.0 ],
					"setminmax" : [ 0.0, 1.0 ],
					"setstyle" : 3
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-240",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1689.280934866304733, 98.817384130241408, 72.0, 22.0 ],
					"text" : "r noteGuitar"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-239",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1313.199999928474426, 178.698582404373155, 69.0, 22.0 ],
					"text" : "r rateGuitar"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-216",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1038.199999928474426, 1212.432955164195391, 48.0, 22.0 ],
					"text" : "gain $1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-212",
					"maxclass" : "newobj",
					"numinlets" : 5,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1038.199999928474426, 1172.432955164195391, 109.0, 22.0 ],
					"text" : "zmap 0 127 -127 0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-198",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"patching_rect" : [ 1038.199999928474426, 1139.432955164195391, 57.0, 22.0 ],
					"text" : "ctlin 14 1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-168",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "multichannelsignal" ],
					"patcher" : 					{
						"fileversion" : 1,
						"appversion" : 						{
							"major" : 8,
							"minor" : 5,
							"revision" : 3,
							"architecture" : "x64",
							"modernui" : 1
						}
,
						"classnamespace" : "box",
						"rect" : [ 59.0, 119.0, 640.0, 480.0 ],
						"bglocked" : 0,
						"openinpresentation" : 0,
						"default_fontsize" : 12.0,
						"default_fontface" : 0,
						"default_fontname" : "Arial",
						"gridonopen" : 1,
						"gridsize" : [ 15.0, 15.0 ],
						"gridsnaponopen" : 1,
						"objectsnaponopen" : 1,
						"statusbarvisible" : 2,
						"toolbarvisible" : 1,
						"lefttoolbarpinned" : 0,
						"toptoolbarpinned" : 0,
						"righttoolbarpinned" : 0,
						"bottomtoolbarpinned" : 0,
						"toolbars_unpinned_last_save" : 0,
						"tallnewobj" : 0,
						"boxanimatetime" : 200,
						"enablehscroll" : 1,
						"enablevscroll" : 1,
						"devicewidth" : 0.0,
						"description" : "",
						"digest" : "",
						"tags" : "",
						"style" : "",
						"subpatcher_template" : "",
						"assistshowspatchername" : 0,
						"boxes" : [ 							{
								"box" : 								{
									"id" : "obj-169",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 20.0, 215.0, 106.0, 22.0 ],
									"text" : "loadmess speed 1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-77",
									"maxclass" : "number",
									"maximum" : 1000,
									"minimum" : 0,
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 175.428571428571502, 81.999997322116826, 50.0, 22.0 ]
								}

							}
, 							{
								"box" : 								{
									"bgcolor" : [ 1.0, 1.0, 1.0, 1.0 ],
									"id" : "obj-80",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 227.428571428571502, 81.999997322116826, 63.0, 20.0 ],
									"text" : "(msec)"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-83",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 175.428571428571502, 40.999997322116826, 77.0, 22.0 ],
									"text" : "loadmess 20"
								}

							}
, 							{
								"box" : 								{
									"bgcolor" : [ 0.301961, 0.301961, 0.301961, 1.0 ],
									"bgcolor2" : [ 0.301961, 0.301961, 0.301961, 1.0 ],
									"bgfillcolor_angle" : 270.0,
									"bgfillcolor_autogradient" : 0.0,
									"bgfillcolor_color" : [ 0.498039215686275, 0.498039215686275, 0.498039215686275, 1.0 ],
									"bgfillcolor_color1" : [ 0.301961, 0.301961, 0.301961, 1.0 ],
									"bgfillcolor_color2" : [ 0.2, 0.2, 0.2, 1.0 ],
									"bgfillcolor_proportion" : 0.5,
									"bgfillcolor_type" : "color",
									"gradient" : 1,
									"id" : "obj-84",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 175.428571428571502, 116.999997322116826, 79.0, 22.0 ],
									"text" : "returntime $1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-7",
									"maxclass" : "newobj",
									"numinlets" : 7,
									"numoutlets" : 1,
									"outlettype" : [ "multichannelsignal" ],
									"patching_rect" : [ 134.0, 273.333328664302826, 118.428571428571502, 22.0 ],
									"text" : "mc.pack~ 7"
								}

							}
, 							{
								"box" : 								{
									"fontface" : 1,
									"fontsize" : 14.0,
									"id" : "obj-38",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 8,
									"outlettype" : [ "signal", "signal", "signal", "signal", "signal", "signal", "signal", "list" ],
									"patching_rect" : [ 134.000000000000114, 215.0, 135.0, 24.0 ],
									"text" : "abc_2d_encoder3~",
									"textcolor" : [ 0.968627450980392, 0.968627450980392, 0.968627450980392, 1.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-23",
									"index" : 1,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 133.999992716514612, 36.999997322116826, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-26",
									"index" : 1,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 133.999992716514612, 355.333317322116841, 30.0, 30.0 ]
								}

							}
 ],
						"lines" : [ 							{
								"patchline" : 								{
									"destination" : [ "obj-38", 0 ],
									"source" : [ "obj-169", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-38", 0 ],
									"source" : [ "obj-23", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-7", 6 ],
									"source" : [ "obj-38", 6 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-7", 5 ],
									"source" : [ "obj-38", 5 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-7", 4 ],
									"source" : [ "obj-38", 4 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-7", 3 ],
									"source" : [ "obj-38", 3 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-7", 2 ],
									"source" : [ "obj-38", 2 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-7", 1 ],
									"source" : [ "obj-38", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-7", 0 ],
									"source" : [ "obj-38", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-26", 0 ],
									"source" : [ "obj-7", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-84", 0 ],
									"source" : [ "obj-77", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-77", 0 ],
									"source" : [ "obj-83", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-38", 0 ],
									"source" : [ "obj-84", 0 ]
								}

							}
 ]
					}
,
					"patching_rect" : [ 1405.614268199637991, 463.544983531001776, 67.0, 22.0 ],
					"saved_object_attributes" : 					{
						"description" : "",
						"digest" : "",
						"globalpatchername" : "",
						"tags" : ""
					}
,
					"text" : "p encoding"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-20",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 102.866668283939362, 703.817384130241408, 116.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 433.1333367228508, 175.317380732776655, 116.0, 20.0 ],
					"text" : "Calibrated / Joystick"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-30",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 807.630106427861392, 956.000000953674316, 51.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 718.1333367228508, 300.999997556209564, 51.0, 20.0 ],
					"text" : "Outputs"
				}

			}
, 			{
				"box" : 				{
					"candycane" : 10,
					"contdata" : 1,
					"id" : "obj-35",
					"maxclass" : "multislider",
					"numinlets" : 1,
					"numoutlets" : 2,
					"orientation" : 0,
					"outlettype" : [ "", "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 744.642815050597619, 986.931927614925371, 198.0, 233.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 657.609508481736611, 330.796771269557667, 191.0, 165.0 ],
					"setminmax" : [ 0.0, 1.0 ],
					"signed" : 1,
					"size" : 7,
					"slidercolor" : [ 1.0, 1.0, 1.0, 1.0 ],
					"spacing" : 4,
					"thickness" : 4
				}

			}
, 			{
				"box" : 				{
					"args" : [ 13 ],
					"bgmode" : 0,
					"border" : 1,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-36",
					"lockeddragscroll" : 0,
					"lockedsize" : 0,
					"maxclass" : "bpatcher",
					"name" : "bbdmi.2max.maxpat",
					"numinlets" : 1,
					"numoutlets" : 0,
					"offset" : [ 0.0, 0.0 ],
					"patching_rect" : [ 539.99997495968455, 986.931927614925371, 197.533333301544189, 232.664822733401934 ],
					"presentation" : 1,
					"presentation_rect" : [ 452.966668390823543, 269.796771269557667, 197.533333301544189, 232.664822733401934 ],
					"varname" : "bbdmi.2max[1]",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"annotation" : "",
					"args" : [ 13 ],
					"bgmode" : 0,
					"border" : 1,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"hint" : "",
					"id" : "obj-12",
					"lockeddragscroll" : 0,
					"lockedsize" : 0,
					"maxclass" : "bpatcher",
					"name" : "bbdmi.scale.maxpat",
					"numinlets" : 1,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "" ],
					"patching_rect" : [ 332.83330824971199, 986.931927614925371, 198.199999988079071, 232.664822733401934 ],
					"presentation" : 1,
					"presentation_rect" : [ 245.800001680850983, 269.796771269557667, 198.199999988079071, 232.664822733401934 ],
					"varname" : "bbdmi.scale[1]",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"args" : [ 5 ],
					"bgmode" : 0,
					"border" : 1,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-37",
					"lockeddragscroll" : 0,
					"lockedsize" : 0,
					"maxclass" : "bpatcher",
					"name" : "bbdmi.crosspatch.maxpat",
					"numinlets" : 2,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "" ],
					"patching_rect" : [ 123.866668283939362, 986.931927614925371, 194.666666626930237, 225.864847052097048 ],
					"presentation" : 1,
					"presentation_rect" : [ 42.133335053920746, 269.796771269557667, 194.666666626930237, 225.864847052097048 ],
					"varname" : "bbdmi.crosspatch[1]",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-136",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 176.700001597404366, 490.752155411766125, 89.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 726.66343979537487, -1.182616823432909, 89.0, 20.0 ],
					"text" : "EEG calibrated"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-151",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 479.36343972384941, 1538.434054512264311, 77.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 459.833342343568802, 643.767381090404569, 77.0, 20.0 ],
					"text" : "ML OUTPUT"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-152",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 59.399928778410072, 1538.434054512264311, 120.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 39.869831398129463, 643.767381090404569, 120.0, 20.0 ],
					"text" : "ML desired OUTPUT"
				}

			}
, 			{
				"box" : 				{
					"args" : [ 13 ],
					"bgmode" : 0,
					"border" : 1,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-153",
					"lockeddragscroll" : 0,
					"lockedsize" : 0,
					"maxclass" : "bpatcher",
					"name" : "bbdmi.multislider.maxpat",
					"numinlets" : 2,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "" ],
					"patching_rect" : [ 417.196765080094451, 1560.434054512264311, 197.199999988079071, 445.766659235954194 ],
					"presentation" : 1,
					"presentation_rect" : [ 397.666667699813843, 665.767381090404569, 197.199999988079071, 445.766659235954194 ],
					"varname" : "bbdmi.multislider[2]",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"args" : [ 13 ],
					"bgmode" : 0,
					"border" : 1,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-162",
					"lockeddragscroll" : 0,
					"lockedsize" : 0,
					"maxclass" : "bpatcher",
					"name" : "bbdmi.2max.maxpat",
					"numinlets" : 1,
					"numoutlets" : 0,
					"offset" : [ 0.0, 0.0 ],
					"patching_rect" : [ 815.630106427861392, 1560.434054512264311, 195.533333301544189, 445.766659235954194 ],
					"varname" : "bbdmi.2max",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-201",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 283.596765115857238, 1765.434054512264311, 67.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 264.06666773557663, 870.767381090404569, 67.0, 20.0 ],
					"text" : "OutputsML"
				}

			}
, 			{
				"box" : 				{
					"candycane" : 10,
					"contdata" : 1,
					"id" : "obj-213",
					"maxclass" : "multislider",
					"numinlets" : 1,
					"numoutlets" : 2,
					"orientation" : 0,
					"outlettype" : [ "", "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 218.596765115857238, 1788.469228797733649, 197.0, 211.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 199.06666773557663, 893.802555375873908, 197.0, 211.0 ],
					"setminmax" : [ 0.0, 1.0 ],
					"signed" : 1,
					"size" : 16,
					"slidercolor" : [ 1.0, 1.0, 1.0, 1.0 ],
					"spacing" : 4,
					"thickness" : 4
				}

			}
, 			{
				"box" : 				{
					"annotation" : "",
					"args" : [ 13 ],
					"bgmode" : 0,
					"border" : 1,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"hint" : "",
					"id" : "obj-163",
					"lockeddragscroll" : 0,
					"lockedsize" : 0,
					"maxclass" : "bpatcher",
					"name" : "bbdmi.scale.maxpat",
					"numinlets" : 1,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "" ],
					"patching_rect" : [ 608.463439717888946, 1560.434054512264311, 196.199999988079071, 445.766659235954194 ],
					"varname" : "bbdmi.scale",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"args" : [ 13 ],
					"bgmode" : 0,
					"border" : 1,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-164",
					"lockeddragscroll" : 0,
					"lockedsize" : 0,
					"maxclass" : "bpatcher",
					"name" : "bbdmi.multislider.maxpat",
					"numinlets" : 2,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "" ],
					"patching_rect" : [ 22.203092440962905, 1560.668094111446408, 194.393672674894333, 442.766659235954194 ],
					"presentation" : 1,
					"presentation_rect" : [ 2.672995060682297, 666.001420689586666, 194.393672674894333, 442.766659235954194 ],
					"varname" : "bbdmi.multislider[3]",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"annotation" : "",
					"bgmode" : 0,
					"border" : 1,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"hint" : "",
					"id" : "obj-165",
					"lockeddragscroll" : 0,
					"lockedsize" : 0,
					"maxclass" : "bpatcher",
					"name" : "bbdmi.regress.maxpat",
					"numinlets" : 3,
					"numoutlets" : 2,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "list", "" ],
					"patching_rect" : [ 218.596765115857238, 1560.434054512264311, 196.599999964237213, 199.799998104572296 ],
					"presentation" : 1,
					"presentation_rect" : [ 199.06666773557663, 665.767381090404569, 196.599999964237213, 199.799998104572296 ],
					"varname" : "bbdmi.regress[1]",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"angle" : 270.0,
					"bgcolor" : [ 0.450980392156863, 0.898039215686275, 1.0, 1.0 ],
					"id" : "obj-174",
					"maxclass" : "panel",
					"mode" : 0,
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 803.599998259544464, 182.400001704692841, 134.400000095367432, 27.199998497962952 ],
					"proportion" : 0.5
				}

			}
, 			{
				"box" : 				{
					"angle" : 270.0,
					"bgcolor" : [ 1.0, 0.596078431372549, 0.596078431372549, 1.0 ],
					"id" : "obj-176",
					"maxclass" : "panel",
					"mode" : 0,
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 803.599998259544464, 146.400001168251038, 134.400000095367432, 27.199998497962952 ],
					"proportion" : 0.5
				}

			}
, 			{
				"box" : 				{
					"angle" : 270.0,
					"bgcolor" : [ 0.913725490196078, 1.0, 0.592156862745098, 1.0 ],
					"id" : "obj-178",
					"maxclass" : "panel",
					"mode" : 0,
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 803.599998259544464, 110.696590960025901, 134.400000095367432, 27.199998497962952 ],
					"proportion" : 0.5
				}

			}
, 			{
				"box" : 				{
					"angle" : 270.0,
					"bgcolor" : [ 0.611764705882353, 1.0, 0.662745098039216, 1.0 ],
					"id" : "obj-180",
					"maxclass" : "panel",
					"mode" : 0,
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 803.599998259544464, 77.896590471267814, 134.400000095367432, 27.199998497962952 ],
					"proportion" : 0.5
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-4",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 218.596765115857238, 1518.650716788055433, 47.0, 22.0 ],
					"text" : "pack f f"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-16",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1795.28093486630496, 136.698582404373155, 70.0, 22.0 ],
					"text" : "loadmess 6"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-1",
					"maxclass" : "number",
					"maximum" : 7,
					"minimum" : 0,
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1795.28093486630496, 178.698582404373155, 50.0, 22.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-2",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1795.28093486630496, 217.698582404373155, 83.0, 22.0 ],
					"text" : "stereoShift $1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-22",
					"maxclass" : "number",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1689.280934866304733, 178.698582404373155, 50.0, 22.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-18",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1689.280934866304733, 136.698582404373155, 77.0, 22.0 ],
					"text" : "loadmess 64"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-3",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1689.280934866304733, 217.698582404373155, 76.0, 22.0 ],
					"text" : "reference $1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-7",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1592.280934866304733, 136.698582404373155, 70.0, 22.0 ],
					"text" : "loadmess 1"
				}

			}
, 			{
				"box" : 				{
					"format" : 6,
					"id" : "obj-13",
					"maxclass" : "flonum",
					"maximum" : 1.0,
					"minimum" : 0.0,
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1592.280934866304733, 178.698582404373155, 50.0, 22.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-14",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1592.280934866304733, 217.698582404373155, 48.0, 22.0 ],
					"text" : "gain $1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-9",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1476.280934866304733, 136.698582404373155, 80.0, 22.0 ],
					"text" : "loadmess 0.5"
				}

			}
, 			{
				"box" : 				{
					"format" : 6,
					"id" : "obj-8",
					"maxclass" : "flonum",
					"maximum" : 1.0,
					"minimum" : 0.0,
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1476.280934866304733, 178.698582404373155, 50.0, 22.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-17",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1476.280934866304733, 217.698582404373155, 96.0, 22.0 ],
					"text" : "pluckPosition $1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-19",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1387.280934866304733, 136.698582404373155, 73.0, 22.0 ],
					"text" : "loadmess 1."
				}

			}
, 			{
				"box" : 				{
					"format" : 6,
					"id" : "obj-21",
					"maxclass" : "flonum",
					"maximum" : 50.0,
					"minimum" : 0.01,
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1387.280934866304733, 178.698582404373155, 50.0, 22.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-24",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1387.280934866304733, 217.698582404373155, 55.0, 22.0 ],
					"text" : "factor $1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-27",
					"library_path0" : "/Users/alainbonardi/Documents/Max 8/Packages/faustgen/externals/msp/faustgen~.mxo/Contents/Resources/",
					"library_path1" : "/Users/davidfierro/Documents/Max 8/Packages/faustgen/externals/msp/faustgen~.mxo/Contents/Resources/",
					"machinecode" : "z/rt/gcAAAEDAAAAAQAAAAQAAAAIAgAAACAAAAAAAAAZAAAAiAEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAEeLAAAAAAAAKAIAAAAAAABHiwAAAAAAAAcAAAAHAAAABAAAAAAAAABfX3RleHQAAAAAAAAAAAAAX19URVhUAAAAAAAAAAAAAAAAAAAAAAAAFnQAAAAAAAAoAgAABAAAAHCNAABGAAAAAAQAgAAAAAAAAAAAAAAAAF9fbGl0ZXJhbDgAAAAAAABfX1RFWFQAAAAAAAAAAAAAGHQAAAAAAAAAAgAAAAAAAEB2AAADAAAAAAAAAAAAAAAEAAAAAAAAAAAAAAAAAAAAX19saXRlcmFsMTYAAAAAAF9fVEVYVAAAAAAAAAAAAAAgdgAAAAAAABAAAAAAAAAASHgAAAQAAAAAAAAAAAAAAA4AAAAAAAAAAAAAAAAAAABfX2NvbnN0AAAAAAAAAAAAX19URVhUAAAAAAAAAAAAADB2AAAAAAAAFxUAAAAAAABYeAAABAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADIAAAAYAAAAAQAAAAAADQAAAAAAAAAAAAIAAAAYAAAAoI8AAAwAAABgkAAAqBUAAAsAAABQAAAAAAAAAAEAAAABAAAABwAAAAgAAAAEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAw2YuDx+EAAAAAAAPH0QAAFVBV0FWQVVBVFNIg+woSYn8SMdHCAAAAADHRywAAAAASIPHOE2NvCQAIQAAD1fAQQ8RhCQQogAASceEJCCiAAAAAAAATY20JDCiAABBDxGEJEATAQBJx4QkUBMBAAAAAABJjawkYBMBAEEPEYQkcGQBAEnHhCSAZAEAAAAAAEmNnCSQZAEAQQ8RhCSg1QEASceEJLDVAQAAAAAASY2EJMDVAQBIiUQkCEEPEYQk0BYCAEnHhCTgFgIAAAAAAEmNhCTwFgIASIlEJBBBDxGEJABoAgBJx4QkEGgCAAAAAABJjYQkIGgCAEiJRCQYQQ8RhCQw2QIASceEJEDZAgAAAAAASY2EJFDZAgBIiUQkIEm9AAAAAAAAAAC+UCAAAEH/1Q9XwEEPEYQk2CAAAEEPEYQkyCAAAEEPEYQkuCAAAEEPEYQkqCAAAL4IgQAATIn/Qf/VvghxAABMifdB/9W+CFEAAEiJ70H/1b4IcQAASInfQf/VvghBAABIi3wkCEH/1b4IUQAASIt8JBBB/9W+CHEAAEiLfCQYQf/VvrAwAABIi3wkIEH/1UiDxChbQVxBXUFeQV9dw2YuDx+EAAAAAACJdxDyDyrOSLgYdAAAAAAAAPIPEBDyD1/KSLggdAAAAAAAAPIPXQhIuCh0AAAAAAAA8g8QAPIPWcHyDxFHFEi4MHQAAAAAAADyDxAA8g9ZwfIPEUcwZg8owvIPXsHyDxLYSLggdgAAAAAAAGYPWRhmDxGfiCAAAGYPFdvyD1zT8g8Rl6AgAABIuDh0AAAAAAAA8g9ZCPIPEY/oIAAA8g8Rh/AgAABIuEB0AAAAAAAA8g8QCPIPWcjyDxGPCKIAAEi4SHQAAAAAAADyDxAI8g9ZyPIPEY8oogAASLhQdAAAAAAAAPIPEAjyD1nI8g8RjzgTAQBIuFh0AAAAAAAA8g8QCPIPWcjyDxGPWBMBAEi4YHQAAAAAAADyDxAI8g9ZyPIPEY9oZAEASLhodAAAAAAAAPIPEAjyD1nI8g8Rj4hkAQBIuHB0AAAAAAAA8g8QCPIPWcjyDxGPmNUBAEi4eHQAAAAAAADyDxAI8g9ZyPIPEY+41QEASLiAdAAAAAAAAPIPEAjyD1nI8g8Rj8gWAgBIuIh0AAAAAAAA8g8QCPIPWcjyDxGP6BYCAEi4kHQAAAAAAADyDxAI8g9ZyPIPEY/4ZwIASLiYdAAAAAAAAPIPEAjyD1nI8g8RjxhoAgBIuKB0AAAAAAAA8g8QCPIPWcjyDxGPKNkCAEi4qHQAAAAAAADyD1kA8g8Rh0jZAgDDZpDDZi4PH4QAAAAAAA8fRAAAw2YuDx+EAAAAAAAPH0QAAEi4AAAAAAAAAADDDx9EAABVQVdBVkFVQVRTSIHsyAkAAIl0JEhIiftIiwFIiYQkwAkAAEiLQQhIiYQkuAkAAPIPEAfyDxFEJCBIvQAAAAAAAAAASLiwdAAAAAAAAPIPEAjyDxFMJAj/1fIPEYQkgAUAAPIPEEMU8g8RBCTyDxBLHPIPEUwkQEi4uHQAAAAAAADyDxAA8g8RhCRAAQAAZg8o0PIPEEMkSLjAdAAAAAAAAPIPWQDyD1zR8g8RVCQwSLjIdAAAAAAAAPIPWABIuAAAAAAAAAAA/9BIuNB0AAAAAAAA8g8QGPIPWdhmDyjg8g8RRCQ4SLjYdAAAAAAAAPIPEADyDxFEJBjyD1jY8g8QTCQw8g8QBCTyD1nI8g8RTCQw8g9Zy0i44HQAAAAAAADyDxAQ8g8RjCRYBAAA8g9YyvIPEVQkEPIPEYwkgAAAAPJEDyzhRTH/RYXkuAAAAABBD0nEiYQk0AMAAESJ4P/AQQ9Ix4mEJNgDAABEieCDwAJBD0jHiYQk6AMAAESJ4IPAA0EPSMeJhCTwAwAAQYPEBEUPSOfyD1lEJEDyDxEEJPIPWdjyDxGcJGAEAABmDyjD8g9YwvIPEYQksAMAAPJEDyzoRYXtuAAAAABBD0nFiYQk+AMAAESJ6P/AQQ9Ix4mEJLgDAABEieiDwAJBD0jHiYQkwAMAAESJ6IPAA0EPSMeJhCTIAwAAQYPFBEUPSO/yDxBDMPIPEYQkkAAAAPIPEIwkQAEAAPIPXszyDxFMJCjyDxCDiCAAAPIPWcFJvgAAAAAAAAAAQf/W8g8RhCSgAwAA8g8Qg5ggAADyDxCL6CAAAPIPEUwkYPIPWYOQIAAA8g8RhCSwCQAASLgwdQAAAAAAAPIPEADyDxFEJEDyDxCL+CAAAPIPEUwkWPIPEIPwIAAA8g9ZwfIPEYQkqAkAAPIPEEQkIPIPWIQkQAEAAPIPEEwkCP/VSLhAdQAAAAAAAPIPEAjyD1lMJDjyD1hMJBjyDxBUJDDyD1nR8g8RlCRIBAAA8g8QXCQQ8g9Y0/IPEZQkmAMAAPIPLMqFybgAAAAAD0nBiYQkMAEAAInI/8BBD0jHiYQkYAMAAInIg8ACQQ9Ix4mEJIgDAACJyIPAA0EPSMeJhCSQAwAAg8EEQQ9Iz4lMJHzyD1kMJPIPEYwkUAQAAPIPWMvyDxGMJGgDAADyDyzJhcm4AAAAAA9JwYmEJKgDAACJyP/AQQ9Ix4mEJHADAADyDxGEJHgFAACJyIPAAkEPSMeJhCR4AwAAiciDwANBD0jHiYQkgAMAAIPBBEEPSM+JjCQoAQAA8g8QgwiiAADyD1lEJChB/9byDxGEJCABAADyDxCDKKIAAPIPWUQkWPIPEYQkoAkAAPIPEEQkIPIPWEQkQPIPEEwkCP/VSLhQdQAAAAAAAPIPEAjyD1lMJDjyD1hMJBjyDxBUJDDyD1nR8g8RlCQ4BAAA8g8QXCQQ8g9Y0/IPEZQkSAMAAPIPLMKFwLkAAAAAD0nIiUwkeInB/8FBD0jPiUwkdInBg8ECQQ9Iz4mMJEADAACJwYPBA0EPSM+JjCRQAwAAg8AEQQ9Ix4mEJFgDAADyD1kMJPIPEYwkQAQAAPIPWMvyDxGMJCgDAADyDyzBhcC5AAAAAA9JyIlMJGyJwf/BQQ9Iz4mMJBgBAADyDxGEJHAFAACJwYPBAkEPSM+JjCQgAwAAicGDwQNBD0jPiYwkMAMAAIPABEEPSMeJhCQ4AwAA8g8QgzgTAQDyD1lEJChB/9byDxGEJBABAADyDxCDWBMBAPIPWUQkWPIPEYQkmAkAAEi4YHUAAAAAAADyDxAA8g9YRCQg8g8QTCQI/9VIuGh1AAAAAAAA8g8QCPIPWUwkOPIPWEwkGPIPEFQkMPIPWdHyDxGUJCgEAADyDxBcJBDyD1jT8g8RlCQAAwAA8g8swoXAuQAAAAAPSciJTCRwicH/wUEPSM+JjCQIAwAAicGDwQJBD0jPiYwkEAMAAInBg8EDQQ9Iz4mMJBgDAACDwARBD0jHiUQkaPIPWQwk8g8RjCQwBAAA8g9Yy/IPEYwk4AIAAPIPLMGFwLkAAAAAD0nIiYwk2AIAAInB/8FBD0jPiYwk6AIAAPIPEYQkaAUAAInBg8ECQQ9Iz4mMJPACAACJwYPBA0EPSM+JjCT4AgAAg8AEQQ9Ix4mEJAgBAADyDxCDaGQBAPIPWUQkKEH/1vIPEYQkwAIAAPIPEIOIZAEA8g9ZRCRY8g8RhCSQCQAASLh4dQAAAAAAAPIPEADyD1hEJCDyDxBMJAj/1Ui4gHUAAAAAAADyDxAI8g9ZTCQ48g9YTCQY8g8QVCQw8g9Z0fIPEZQkIAQAAPIPEFwkEPIPWNPyDxGUJMgBAADyDyzChcC5AAAAAA9JyImMJAABAACJwf/BQQ9Iz4mMJLgCAACJwYPBAkEPSM+JjCTQAAAAicGDwQNBD0jPiYwk2AAAAIPABEEPSMeJhCTIAgAA8g9ZDCTyDxGMJEAFAADyD1jL8g8RjCSYAgAA8g8swYXAuQAAAAAPSciJjCTQAgAAicH/wUEPSM+JjCSgAgAA8g8RhCRgBQAAicGDwQJBD0jPiYwkqAIAAInBg8EDQQ9Iz4mMJLACAACDwARBD0jHiYQk+AAAAPIPEIOY1QEA8g9ZRCQoQf/W8g8RhCSAAgAA8g8Qg7jVAQDyD1lEJFjyDxGEJIgJAABIuJB1AAAAAAAA8g8QAPIPWEQkIPIPEEwkCP/VSLiYdQAAAAAAAPIPEAjyD1lMJDjyD1hMJBjyDxBUJDDyD1nR8g8RlCQQBAAA8g8QXCQQ8g9Y0/IPEZQkeAIAAPIPLMKFwLkAAAAAD0nIiYwk8AAAAInB/8FBD0jPiYwk4AAAAInBg8ECQQ9Iz4mMJOgAAACJwYPBA0EPSM+JjCRwAgAAg8AEQQ9Ix4mEJIgCAADyD1kMJPIPEYwkGAQAAPIPWMvyDxGMJJgAAADyDyzBhcC5AAAAAA9JyImMJJACAACJwf/BQQ9Iz4mMJFgCAADyDxGEJFgFAACJwYPBAkEPSM+JjCRQAgAAicGDwQNBD0jPiYwkYAIAAIPABEEPSMeJhCRoAgAA8g8Qg8gWAgDyD1lEJChB/9byDxGEJKAAAADyDxCD6BYCAPIPWUQkWPIPEYQkgAkAAEi4qHUAAAAAAADyDxAA8g9YRCQg8g8QTCQI/9VIuLB1AAAAAAAA8g8QCPIPWUwkOPIPWEwkGPIPEFQkMPIPWdHyDxGUJAAEAADyDxBcJBDyD1jT8g8RlCQ4AgAA8g8swoXAuQAAAAAPSciJjCSoAAAAicH/wUEPSM+JjCQoAgAAicGDwQJBD0jPiYwkMAIAAInBg8EDQQ9Iz4mMJEACAACDwARBD0jHiYQkSAIAAPIPWQwk8g8RjCQIBAAA8g9Yy/IPEYwkEAIAAPIPLMGFwLkAAAAAD0nIiYwksAAAAInB/8FBD0jPiYwkuAAAAPIPEYQkUAUAAInBg8ECQQ9Iz4mMJAgCAACJwYPBA0EPSM+JjCQYAgAAg8AEQQ9Ix4mEJCACAADyDxCD+GcCAPIPWUQkKEH/1vIPEYQkSAEAAPIPEIMYaAIA8g9ZRCRY8g8RhCR4CQAASLjAdQAAAAAAAPIPEEQkIPIPWADyDxBMJAj/1Ui4yHUAAAAAAADyDxBMJDjyD1kI8g9YTCQY8g8QXCQw8g9Z2WYPKNHyDxFcJDBmDyjL8g8QXCQQ8g9Yy/IPEYwk6AEAAPIPLMGFwLkAAAAAD0nIiYwk4AEAAInB/8FBD0jPiYwk8AEAAInBg8ECQQ9Iz4mMJPgBAACJwYPBA0EPSM+JjCQAAgAAg8AEQQ9Ix4mEJFABAADyD1kUJPIPEVQkOGYPKMvyD1jK8g8RTCQQ8g8swYXAuQAAAAAPSciJjCTYAQAAicH/wUEPSM+JjCTgAwAA8g8RhCRIBQAAicGDwQJBD0jPiUwkGInBg8EDQQ9Iz4mMJIgAAACDwARBD0jHiUQkIPIPEIMo2QIA8g9ZRCQoQf/Wg3wkSAAPjidkAADyDxGEJCgFAAAPV8DyDyqEJNADAADyRA8QhCSQAAAAZkEPKMjyD13I8g8swYmEJMQBAAAPV8DyDyqEJNgDAABmQQ8oyPIPXcjyDyzBiYQkwAEAAPIPEIQkgAAAAA9XyWYPOgvICUi46HQAAAAAAADyDxAAZg8o2GZEDyjQ8g9c2fIPEKQkWAQAAPIPWNxIuPB0AAAAAAAA8g8QAGYPKNBmRA8o4PIPXNHyD1jUSLj4dAAAAAAAAPIPEABmDyjwZkQPKNjyD1zx8g9Y9Ei4AHUAAAAAAADyDxAAZg8o+GZEDyjI8g9c+fIPWPzyD1zh8g8RpCRYBAAAZg8o5vIPWedIuAh1AAAAAAAA8kQPEChmQQ8oxfIPWcPyD1nESLgQdQAAAAAAAPJEDxA4ZkEPKO/yD1nq8g9Z7PIPEawk2AMAAEi4GHUAAAAAAADyDxAIZg8o6fIPEYwkgAAAAPIPWevyD1ns8g8RrCTQAwAAD1fk8g8qpCToAwAAZkEPKOjyD13s8g8sxYmEJLwBAADyD1nC8g8RhCToAwAA8g9Z0w9X2/IPKpwk8AMAAGZBDyjg8g9d4/IPLMSJhCS4AQAAD1fb8kEPKtxmQQ8o4PIPXePyDyzEiYQktAEAAPJBD1n18kEPWf/yD1n68g8RvCTwAwAAD1fb8g8qnCT4AwAA8g9Z8vIPEbQk+AMAAGZBDyjQ8g9d0/IPLMKJhCQwBQAA8g8QhCSwAwAAD1fSZg86C9AJZkEPKNryD1za8g8QrCRgBAAA8g9Y3WZBDyjk8g9c4vIPWOVmQQ8ow/IPXMLyD1jFZkEPKPHyD1zy8g9Y9fIPXOryDxGsJGAEAAAPV9LyDyqUJLgDAABmQQ8o6PIPXeryDyzFiYQkzAAAAGYPKNDyD1nWZkEPKP3yD1n78g9Z+mZBDyjv8g9Z7PIPWeryDxGsJLADAABmDyjp8g9Z6/IPWeryDxGsJIgFAAAPV9LyDyqUJMADAABmQQ8o6PIPXeryDyzFiYQkyAAAAPIPWfzyDxG8JLgDAADyD1njD1fS8g8qlCTIAwAAZkEPKNjyD13a8g8sw4mEJMQAAADyQQ9ZxfJBD1n38g9Z9PIPEbQkwAMAAPIPWcTyDxGEJMgDAAAPV9LyQQ8q1WZBDyjY8g9d2vIPLMOJhCTAAAAA8kQPELQkQAEAAGZBDyje8g8QrCSgAwAA8g9e3Ui4IHUAAAAAAADyDxAA8g8RBCRmDyjj8g9Y4PIPWePyQQ9Y5mZBDyjG8g9exPIPEYQkcAkAAEi4KHUAAAAAAADyDxAI8g8RTCRQZg8ow/IPWMHyD1nD8g8RhCQ4BQAAD1fb8g8qnCQwAQAAZkEPKODyD13j8g8sxImEJLABAADyD1ntZkEPKMTyD17F8g8RhCQwAQAAD1fb8g8qnCRgAwAAZkEPKODyD13j8g8sxImEJKwBAAAPV9vyDyqcJIgDAABmQQ8o4PIPXePyDyzEiYQkqAEAAPIPEIQkmAMAAA9X7WYPOgvoCfJEDxFUJAhmQQ8o4vIPXOXyDxC0JEgEAADyD1jmZkEPKNzyD1zd8g9Y3mZBDyjD8g9cxfIPWMZmQQ8o+fIPXP3yD1j+8g9c9fIPEbQkSAQAAA9X7fIPKqwkkAMAAGZBDyjw8g9d9fIPLMaJhCSkAQAAZg8o6PIPWe9mQQ8ozfIPWczyD1nNZkEPKPfyD1nz8g9Z9fIPEbQkkAMAAPIPEJQkgAAAAGYPKPLyD1n08g9Z9fIPEbQkiAMAAA9X7fIPKmwkfGZBDyjw8g9d9fIPLMaJRCR8D1ft8g8qrCSoAwAAZkEPKPDyD1318g8sxomEJCAFAADyD1nL8g8RjCSYAwAA8g9Z3PJBD1nF8kEPWf/yD1n78g8RvCSgAwAA8g9Zw/IPEYQkqAMAAPIPEIQkaAMAAA9X22YPOgvYCWZBDyji8g9c4/IPELwkUAQAAPIPWOdmQQ8o7PIPXOvyD1jvZkUPKNPyRA9c0/JED1jXZkEPKMnyD1zL8g9Yzw9X9vIPKrQkcAMAAPIPXPvyDxG8JFAEAABmQQ8o2PIPXd7yDyzDiYQkGAUAAGZBDyja8g9Z2WZBDyj98g9Z/PIPWftmQQ8o9/IPWfXyD1nz8g8RtCRoAwAAZg8owvIPWcQPV/byDyq0JHgDAADyD1nD8g8RhCRgAwAAZkEPKNjyD13e8g8sw4mEJBAFAADyD1n98g8RvCRwAwAA8g9Z7A9X2/IPKpwkgAMAAGZBDyjg8g9d4/IPLMSJhCQIBQAA8kUPWdXyQQ9Zz/IPWc3yDxGMJHgDAAAPV9vyDyqcJCgBAADyRA9Z1fJEDxGUJIADAABmQQ8o4PIPXePyDyzEiYQkAAUAAGZBDyje8g8QjCQgAQAA8g9e2WYPKOPyD1gkJPIPWePyQQ9Y5mZBDyjG8g9exPIPEYQkaAkAAGYPKMPyD1hEJFDyD1nD8g8RhCQoAQAA8g9ZyWZFDyjUZkEPKMTyD17B8g8RhCQgAQAAD1fb8g8qXCR4ZkEPKODyD13j8g8sxIlEJHgPV9vyDypcJHRmQQ8o4PIPXePyDyzEiUQkdPIPEIQkSAMAAA9X22YPOgvYCfJEDxBkJAhmQQ8o5PIPXOPyDxC0JDgEAADyD1jmZkEPKOryD1zr8g9Y7mZBDyjD8g9cw/IPWMZmQQ8oyfJEDxGMJDgBAADyD1zL8g9YzvIPXPPyDxG0JDgEAAAPV9vyDyqcJEADAABmQQ8o8PIPXfPyDyzGiYQkoAEAAGYPKNjyD1nZZkEPKP3yD1n88g9Z+2ZBDyj38g9Z9fIPWfPyDxG0JEgDAABmDyjy8g9Z9PIPWfPyDxG0JEADAAAPV9vyDyqcJFADAABmQQ8o8PIPXfPyDyzGiYQknAEAAA9X2/IPKpwkWAMAAGZBDyjw8g9d8/IPLMaJhCSYAQAA8g9Z/fIPEbwkYAkAAPIPWezyQQ9ZxfJBD1nP8g9ZzfIPEYwkUAMAAPIPWcXyDxGEJFgDAAAPV9vyDypcJGxmQQ8o4PIPXePyDyzEiYQk+AQAAA9X2/IPKpwkGAEAAGZBDyjg8g9d4/IPLMSJhCTwBAAA8g8QhCQoAwAAD1fbZg86C9gJZkEPKOTyD1zj8g8QtCRABAAA8g9Y5mZBDyjq8g9c6/IPWO7yRA8RnCTQAQAAZkEPKMPyD1zD8g9YxmZBDyjJ8g9cy/IPWM7yD1zz8g8RtCRABAAAD1fb8g8qnCQgAwAAZkEPKPDyD13z8g8sxomEJOgEAABmDyjY8g9Z2WZBDyj98g9Z/PIPWftmQQ8o9/IPWfXyD1nz8g8RtCQoAwAAZg8o8mZEDyjK8g9Z9PIPWfPyDxG0JCADAAAPV9vyDyqcJDADAABmQQ8o8PIPXfPyDyzGiYQk4AQAAA9X2/IPKpwkOAMAAGZBDyjw8g9d8/IPLMaJhCTYBAAA8g9Z/fIPEbwkMAMAAPIPWezyQQ9ZxfJBD1nP8g9ZzfIPEYwkWAkAAPIPWcXyDxGEJDgDAABmQQ8o3vIPEIwkEAEAAPIPXtlmDyjj8g9YJCTyD1nj8kEPWOZmQQ8oxvIPXsTyDxGEJFAJAABmDyjD8g9YRCRQ8g9Zw/IPEYQkGAEAAPIPWclmQQ8owvIPXsHyDxGEJBABAAAPV9vyDypcJHBmQQ8o4PIPXePyDyzEiUQkcPIPEIQkAAMAAA9X7WYPOgvoCWZBDyjUZkEPKOTyD1zl8g8QtCQoBAAA8g9Y5mZBDyjaZkUPKOLyD1zd8g9Y3mZBDyjD8g9cxfIPWMbyRA8QlCQ4AQAAZkEPKMryD1zN8g9YzvIPXPXyDxG0JCgEAAAPV+3yDyqsJAgDAABmQQ8o8PIPXfXyDyzGiUQkbGYPKOjyD1npZkEPKP3yD1n88g9Z/WZBDyj38g9Z8/IPWfXyDxG0JAgDAABmQQ8o8fIPWfTyD1n18g8RtCQAAwAAD1ft8g8qrCQQAwAAZkEPKPDyD1318g8sxomEJJQBAAAPV+3yDyqsJBgDAABmQQ8o8PIPXfXyDyzGiYQkkAEAAPIPWfvyDxG8JEgJAADyD1nc8kEPWcXyQQ9Zz/IPWcvyDxGMJBADAADyD1nD8g8RhCQYAwAAD1fb8g8qXCRoZkEPKODyD13j8g8sxIlEJGgPV9vyDyqcJNgCAABmQQ8o4PIPXePyDyzEiYQk0AQAAPIPEIQk4AIAAA9X22YPOgvYCWYPKOLyD1zj8g8QtCQwBAAA8g9Y5mZBDyjs8g9c6/IPWO5mQQ8ow/IPXMPyD1jGZkEPKMryD1zL8g9YzvIPXPPyDxG0JDAEAAAPV9vyDyqcJOgCAABmQQ8o8PIPXfPyDyzGiYQkyAQAAGYPKNjyD1nZZkEPKP3yD1n88g9Z+2ZBDyj38g9Z9fIPWfPyDxG0JOACAABmRQ8o2WZBDyjx8g9Z9PIPWfPyDxG0JNgCAAAPV9vyDyqcJPACAABmQQ8o8PIPXfPyDyzGiYQkwAQAAA9X2/IPKpwk+AIAAGZBDyjw8g9d8/IPLMaJhCS4BAAA8g9Z/fIPEbwk6AIAAPIPWezyQQ9ZxfJBD1nP8g9ZzfIPEYwk8AIAAPIPWcXyDxGEJPgCAAAPV9vyDyqcJAgBAABmQQ8o4PIPXePyDyzEiYQksAQAAGZBDyje8g8QjCTAAgAA8g9e2WYPKOPyD1gkJPIPWePyQQ9Y5mZBDyjG8g9exPIPEYQkQAkAAGYPKMPyD1hEJFDyD1nD8g8RhCQIAQAA8g9ZyWZBDyjED1fb8g8qnCQAAQAA8g9ewfIPEYQkAAEAAGZBDyjg8g9d4/IPLMSJhCSMAQAA8g8QhCTIAQAAD1fkZg86C+AJZg8o6vIPXOzyDxC0JCAEAADyD1juZkEPKNzyD1zc8g9Y3vJEDxCMJNABAABmQQ8owfIPXMTyD1jGZkEPKMryD1zM8g9YzvIPXPTyDxG0JCAEAAAPV+TyDyqkJLgCAABmQQ8o8PIPXfTyDyzGiYQkyAEAAGYPKODyD1nhZkEPKP3yD1n98g9Z/GZBDyj38g9Z8/IPWfTyDxG0JMACAABmQQ8o8/IPWfXyD1n08g8RtCS4AgAAD1fk8g8qpCTQAAAAZkEPKPDyD1308g8sxomEJIgBAADyD1n78g8RvCQ4CQAA8g9Z3Q9X5PIPKqQk2AAAAGZBDyjo8g9d7PIPLMWJhCSEAQAAD1fk8g8qpCTIAgAAZkEPKOjyD13s8g8sxYmEJIABAADyQQ9ZxfJBD1nP8g9Zy/IPEYwkyAIAAA9X5PIPKqQk0AIAAPIPWcPyDxGEJNACAABmQQ8o2PIPXdzyDyzDiYQkqAQAAPIPEIQkmAIAAA9X22YPOgvYCWYPKOLyD1zj8g8QtCRABQAA8g9Y5mZBDyjs8g9c6/IPWO5mQQ8owfIPXMPyD1jG8kQPXNPyRA9Y1vIPXPPyDxG0JEAFAAAPV9vyDyqcJKACAABmQQ8o8PIPXfPyDyzGiYQkoAQAAGYPKNjyQQ9Z2mZBDyj98g9Z/PIPWftmQQ8o9/IPWfXyD1nz8g8RtCSgAgAAZkEPKPPyD1n08g9Z8/IPEbQkmAIAAA9X2/IPKpwkqAIAAGZBDyjw8g9d8/IPLMaJhCSYBAAA8g9Z/fIPEbwkqAIAAPIPWewPV9vyDyqcJLACAABmQQ8o4PIPXePyDyzEiYQkkAQAAPJBD1nF8kUPWdfyRA9Z1fJEDxGUJDAJAADyD1nF8g8RhCSwAgAAD1fb8g8qnCT4AAAAZkEPKODyD13j8g8sxImEJIgEAABmQQ8o3vIPEIwkgAIAAPIPXtlmDyjj8g9YJCTyD1nj8kEPWOZmQQ8oxvIPXsTyDxGEJCgJAABmDyjD8g9YRCRQ8g9Zw/IPEYQk+AAAAA9X2/IPKpwk8AAAAGZBDyjg8g9d4/IPLMSJhCR8AQAA8g9ZyWZBDyjE8g9ewfIPEYQk8AAAAA9X2/IPKpwk4AAAAGZBDyjg8g9d4/IPLMSJhCR4AQAAD1fb8g8qnCToAAAAZkEPKODyD13j8g8sxImEJHQBAADyDxCEJHgCAAAPV+1mDzoL6AlmRA8o0mYPKOLyD1zl8g8QtCQQBAAA8g9Y5mZBDyjc8g9c3fIPWN5mQQ8owfIPXMXyD1jG8kQPEJwkOAEAAGZBDyjL8g9czfIPWM7yD1z18g8RtCQQBAAAD1ft8g8qrCRwAgAAZkEPKPDyD1318g8sxomEJHABAABmDyjo8g9Z6WZBDyj98g9Z/PIPWf1mQQ8o9/IPWfPyD1n18g8RtCR4AgAA8g8QlCSAAAAAZg8o8vIPWfTyD1n18g8RtCRwAgAAD1ft8g8qrCSIAgAAZkEPKPDyD1318g8sxomEJGwBAAAPV+3yDyqsJJACAABmQQ8o8PIPXfXyDyzGiYQkgAQAAPIPWfvyDxG8JIACAADyD1nc8kEPWcXyQQ9Zz/IPWcvyDxGMJIgCAADyD1nD8g8RhCSQAgAA8g8QhCSYAAAAD1fbZg86C9gJZkEPKOLyD1zj8g8QvCQYBAAA8g9Y52ZFDyjUZkEPKOzyD1zr8g9Y72ZFDyjh8kQPXOPyRA9Y52ZBDyjL8g9cy/IPWM8PV/byDyq0JFgCAADyD1z78g8RvCQYBAAAZkEPKNjyD13e8g8sw4mEJHgEAABmQQ8o3PIPWdlmQQ8o/fIPWfzyD1n7ZkEPKPfyD1n18g9Z8/IPEbQkWAIAAPIPWdQPV/byDyq0JFACAADyD1nT8g8RlCRQAgAAZkEPKNjyD13e8g8sw4mEJHAEAADyD1n98g8RvCQgCQAA8g9Z7A9X2/IPKpwkYAIAAGZBDyjg8g9d4/IPLMSJhCRoBAAA8kUPWeXyQQ9Zz/IPWc3yDxGMJGACAAAPV9vyDyqcJGgCAADyRA9Z5fJEDxGkJGgCAABmQQ8o4PIPXePyDyzEiYQkmAAAAGZBDyje8g8QjCSgAAAA8g9e2WYPKOPyD1gkJPIPWePyQQ9Y5mZBDyjG8g9exPIPEYQkGAkAAGYPKMPyD1hEJFDyD1nD8g8RhCToAAAA8g9ZyWZBDyjC8g9ewfIPEYQk4AAAAA9X2/IPKpwkqAAAAGZBDyjg8g9d4/IPLMSJhCRoAQAAD1fb8g8qnCQoAgAAZkEPKODyD13j8g8sxImEJGQBAADyDxCEJDgCAAAPV9tmDzoL2AnyRA8QZCQIZkEPKOTyD1zj8g8QtCQABAAA8g9Y5mZBDyjq8g9c6/IPWO5mQQ8owfIPXMPyD1jGZkEPKMtmQQ8o0/IPXMvyD1jO8g9c8/IPEbQkAAQAAA9X2/IPKpwkMAIAAGZBDyjw8g9d8/IPLMaJhCRgAQAAZg8o2PIPWdlmQQ8o/fIPWfzyD1n7ZkEPKPfyD1n18g9Z8/IPEbQkMAIAAPJEDxCcJIAAAABmQQ8o8/IPWfTyD1nz8g8RtCQoAgAAD1fb8g8qnCRAAgAAZkEPKPDyD13z8g8sxomEJFwBAAAPV9vyDyqcJEgCAABmQQ8o8PIPXfPyDyzGiYQkWAEAAPIPWf3yDxG8JDgCAADyD1ns8kEPWcXyQQ9Zz/IPWc3yDxGMJEACAADyD1nF8g8RhCRIAgAAD1fb8g8qnCSwAAAAZkEPKODyD13j8g8sxImEJLAAAAAPV9vyDyqcJLgAAABmQQ8o4PIPXePyDyzEiYQkuAAAAPIPEIQkEAIAAA9X22YPOgvYCWZBDyjk8g9c4/IPELQkCAQAAPIPWOZmQQ8o6vIPXOvyD1ju8kQPXMvyRA9YzvIPXNPyD1jW8g9c8/IPEbQkCAQAAA9X2/IPKpwkCAIAAGZBDyjw8g9d8/IPLMaJhCSoAAAAZkEPKNnyD1naZkEPKP3yD1n88g9Z+2ZBDyj38g9Z9fIPWfPyDxG0JAgCAABmQQ8o8/IPWfTyD1nz8g8RtCQQCQAAD1fb8g8qnCQYAgAAZkEPKPDyD13z8g8sxomEJKAAAAAPV9vyDyqcJCACAABmQQ8o8PIPXfPyRA8s5vIPWf3yDxG8JBACAADyD1ns8kUPWc3yQQ9Z1/IPWdXyDxGUJBgCAADyRA9ZzfJEDxGMJCACAABmQQ8o3vIPEIwkSAEAAPIPXtlmDyjj8g9YJCTyD1nj8kEPWOZmQQ8oxvIPXsTyDxGEJAgJAABmDyjD8g9YRCRQ8g9Zw/IPEYQk2AAAAPIPWclmQQ8owvIPXsHyDxGEJNAAAAAPV9vyDyqcJOABAABmQQ8o4PIPXePyRA8s9PIPEIQk6AEAAA9X7WYPOgvoCWZFDyjMZkEPKOTyD1zl8g8QdCQw8g9Y5mZBDyjaZkUPKOLyD1zd8g9Y3vIPEJQk0AEAAGYPKMLyD1zF8g9YxvJEDxCUJDgBAABmQQ8oyvIPXM3yD1jO8g9c9fIPEXQkMA9X7fIPKqwk8AEAAGZBDyjw8g9d9fJEDyzWZg8o6PIPWelmQQ8o/fIPWfzyD1n9ZkEPKPfyD1nz8g9Z9fIPEbQk6AEAAGZBDyjz8g9Z9PIPWfXyDxG0JOABAAAPV+3yDyqsJPgBAABmQQ8o8PIPXfXyRA8szg9X7fIPKqwkAAIAAGZBDyjw8g9d9fJEDyzG8g9Z+/IPEbwk8AEAAPIPWdzyQQ9ZxfJBD1nP8g9Zy/IPEYwk+AEAAPIPWcPyDxGEJAACAAAPV9vyDyqcJFABAABmQQ8o4PIPXePyDyz8D1fb8g8qnCTYAQAAZkEPKODyD13j8g8s9PIPEEQkEA9X5GYPOgvgCfJED1zM8g8QbCQ48kQPWM1mQQ8o3PIPXNzyD1jd8g9c1PIPWNVmQQ8o8vIPXPTyD1j18g9c7PIPEWwkOA9X5PIPKqQk4AMAAGZBDyjo8g9d7PIPLNVmDyji8kEPWdXyD1nm8kUPWenyRA9Z7PJBD1n38kQPWfvyRA9Z/PJEDxG8JOADAADyRQ9Z2Q9XyfIPKkwkGPJED1nc8kQPEZwkgAAAAGZBDyjg8g9d4fIPLMzyRA9Z6/JEDxGsJNgBAADyQQ9Z2Q9XwPIPKoQkiAAAAGZBDyjI8g9dyPIPLMHyD1nz8g8RtCQ4AQAA8g9Z0/IPEZQk0AEAAA9XwPIPKkQkIPJED13A8kUPLNhmQQ8oxvIPEJwkKAUAAPIPXsPyDxAUJPIPWNDyD1nQ8kEPWNZmQQ8ozvIPXsryDxGMJAAJAADyRA8QRCRQ8kQPWMDyRA9ZwPIPWdvyRA9e40m92HUAAAAAAADyQQ8QTQDyDxCEJIAFAADyD1nBZkEPKNbyD1zQ8g8RlCTwCAAA8g8QhCR4BQAA8g9ZwWZBDyjW8g9c0PIPEZQk6AgAAPIPEIQkcAUAAPIPWcFmQQ8o1vIPXNDyDxGUJOAIAADyDxCEJGgFAADyD1nBZkEPKNbyD1zQ8g8RlCTYCAAA8g8QhCRgBQAA8g9ZwWZBDyjW8g9c0PIPEZQk0AgAAPIPEIQkWAUAAPIPWcFmQQ8o1vIPXNDyDxGUJMgIAADyDxCEJFAFAADyD1nBZkEPKNbyD1zQ8g8RlCTACAAASL04dQAAAAAAAPIPEHUA8g8QVCQo8g9Z8ki9SHUAAAAAAADyDxBtAPIPWepIvVh1AAAAAAAA8g8QZQDyD1niSL1wdQAAAAAAAPIPEF0A8g9Z2ki9iHUAAAAAAADyDxBFAPIPWcJIvaB1AAAAAAAA8kQPEF0A8kQPWdpIvbh1AAAAAAAA8kQPEE0A8kQPWcpIvdB1AAAAAAAA8g9ZVQDyDxC8JEgFAADyDxGMJPgIAADyD1n5ZkEPKM7yD1zP8g8RjCS4CAAA8kEPWPbyQQ9Y7vJBD1jm8kEPWN7yQQ9YxvJFD1je8kUPWM7yQQ9Y1vIPWdLyDxBMJGDyD1nR8kUPWcnyRA9ZyfJFD1nb8kQPWdnyD1nA8g9ZwfIPWdvyD1nZ8g9Z5PIPWeHyD1nt8g9Z6fIPWfbyD1nx8kEPX/ZmQQ8ozvIPXs7yDxGMJLAIAADyQQ9f7mZBDyjO8g9ezfIPEYwkqAgAAPJBD1/mZkEPKM7yD17M8g8RjCSgCAAA8kEPX95mQQ8ozvIPXsvyDxGMJJgIAADyQQ9fxmZBDyjO8g9eyPIPEYwkkAgAAPJFD1/eZkEPKM7yQQ9ey/IPEYwkiAgAAPJFD1/OZkEPKMbyQQ9ewfIPEYQkgAgAAPIPEIQkOAUAAPJBD1jG8g8RhCQ4BQAA8g8QhCQoAQAA8kEPWMbyDxGEJCgBAADyDxCEJBgBAADyQQ9YxvIPEYQkGAEAAPIPEIQkCAEAAPJBD1jG8g8RhCQIAQAA8g8QhCT4AAAA8kEPWMbyDxGEJPgAAADyDxCEJOgAAADyQQ9YxvIPEYQk6AAAAPIPEIQk2AAAAPJBD1jG8g8RhCTYAAAA8kUPWMbyRA8RRCRQ8kEPX9byRA9e8vJEDxG0JEABAACLrCTEAQAA99VIiawkeAgAAIusJMABAAD31UiJrCRwCAAAi6wkvAEAAPfVSImsJGgIAACLrCS4AQAA99VIiawkYAgAAIusJLQBAAD31UiJrCRYCAAA8g8QRCRA8g8QjCQwAQAA8g9YyPIPEYwkMAEAAL3+AwAARIusJDAFAABEKe1IiawkUAgAAESJ7ffVSImsJEgIAAC9/gMAAESLrCTMAAAARCntSImsJEAIAABEie331UiJrCQ4CAAAvf4DAABEi6wkyAAAAEQp7UiJrCQwCAAARInt99VIiawkKAgAAL3+AwAARIusJMQAAABEKe1IiawkIAgAAESJ7ffVSImsJBgIAAC9/gMAAESLrCTAAAAARCntSImsJBAIAABEie331UiJrCQICAAAi6wksAEAAPfVSImsJAAIAACLrCSsAQAA99VIiawk+AcAAIusJKgBAAD31UiJrCTwBwAAi6wkpAEAAPfVSImsJOgHAACLbCR899VIiawk4AcAAPIPEIwkIAEAAPIPWMjyDxGMJCABAAC9/gMAAESLrCQgBQAARCntSImsJNgHAABEie331UiJrCQgBQAAvf4DAABEi6wkGAUAAEQp7UiJrCTQBwAARInt99VIiawkGAUAAL3+AwAARIusJBAFAABEKe1IiawkyAcAAESJ7ffVSImsJBAFAAC9/gMAAESLrCQIBQAARCntSImsJMAHAABEie331UiJrCQIBQAAvf4DAABEi6wkAAUAAEQp7UiJrCS4BwAARInt99VIiawkAAUAAItsJHj31UiJrCSwBwAAi2wkdPfVSImsJKgHAACLrCSgAQAA99VIiawkoAcAAIusJJwBAAD31UiJrCSYBwAAi6wkmAEAAPfVSImsJJAHAADyDxCMJBABAADyD1jI8g8RjCQQAQAAvf4BAABEi6wk+AQAAEQp7UiJrCSIBwAARInt99VIiawk+AQAAL3+AQAARIusJPAEAABEKe1IiawkgAcAAESJ7ffVSImsJPAEAAC9/gEAAESLrCToBAAARCntSImsJHgHAABEie331UiJrCToBAAAvf4BAABEi6wk4AQAAEQp7UiJrCRwBwAARInt99VIiawk4AQAAL3+AQAARIusJNgEAABEKe1IiawkaAcAAESJ7ffVSImsJNgEAACLbCRw99VIiawkYAcAAItsJGz31UiJrCRYBwAAi6wklAEAAPfVSImsJFAHAACLrCSQAQAA99VIiawkSAcAAItsJGj31UiJrCRABwAA8g8QjCQAAQAA8g9YyPIPEYwkAAEAAL3+AwAARIusJNAEAABEKe1IiawkOAcAAESJ7ffVSImsJNAEAAC9/gMAAESLrCTIBAAARCntSImsJDAHAABEie331UiJrCTIBAAAvf4DAABEi6wkwAQAAEQp7UiJrCQoBwAARInt99VIiawkwAQAAL3+AwAARIusJLgEAABEKe1IiawkIAcAAESJ7ffVSImsJLgEAAC9/gMAAESLrCSwBAAARCntSImsJBgHAABEie331UiJrCSwBAAAi6wkjAEAAPfVSImsJBAHAACLrCTIAQAA99VIiawkCAcAAIusJIgBAAD31UiJrCQABwAAi6wkhAEAAPfVSImsJPgGAACLrCSAAQAA99VIiawk8AYAAPIPEIwk8AAAAPIPWMjyDxGMJPAAAAC9/gEAAESLrCSoBAAARCntSImsJOgGAABEie331UiJrCSoBAAAvf4BAABEi6wkoAQAAEQp7UiJrCTgBgAARInt99VIiawkoAQAAL3+AQAARIusJJgEAABEKe1Iiawk2AYAAESJ7ffVSImsJJgEAAC9/gEAAESLrCSQBAAARCntSImsJNAGAABEie331UiJrCSQBAAAvf4BAABEi6wkiAQAAEQp7UiJrCTIBgAARInt99VIiawkiAQAAIusJHwBAAD31UiJrCTABgAAi6wkeAEAAPfVSImsJLgGAACLrCR0AQAA99VIiawksAYAAIusJHABAAD31UiJrCSoBgAAi6wkbAEAAPfVSImsJKAGAADyDxCMJOAAAADyD1jI8g8RjCTgAAAAvf4BAABEi6wkgAQAAEQp7UiJrCSYBgAARInt99VIiawkgAQAAL3+AQAARIusJHgEAABEKe1IiawkkAYAAESJ7ffVSImsJHgEAAC9/gEAAESLrCRwBAAARCntSImsJIgGAABEie331UiJrCRwBAAAvf4BAABEi6wkaAQAAEQp7UiJrCSABgAARInt99VIiawkaAQAAL3+AQAARIusJJgAAABEKe1IiawkeAYAAESJ7ffVSImsJHAGAACLrCRoAQAA99VIiawkaAYAAIusJGQBAAD31UiJrCRgBgAAi6wkYAEAAPfVSImsJFgGAACLrCRcAQAA99VIiawkUAYAAIusJFgBAAD31UiJrCRIBgAA8g8QjCTQAAAA8g9YyPIPEYwk0AAAAL3+AwAARIusJLAAAABEKe1IiawkQAYAAESJ7ffVSImsJDgGAAC9/gMAAESLrCS4AAAARCntSImsJDAGAABEie331UiJrCQoBgAAvf4DAABEi6wkqAAAAEQp7UiJrCQgBgAARInt99VIiawkGAYAAL3+AwAARIusJKAAAABEKe1IiawkEAYAAESJ7ffVSImsJAgGAAC9/gMAAEQp5UiJrCQABgAARInl99VIiawk+AUAAESJtCTMAAAARIn199VIiawk8AUAAESJlCTIAAAARInV99VIiawk6AUAAESJjCTEAAAARInN99VIiawk4AUAAESJhCTAAAAARInF99VIiawk2AUAAIm8JCgFAAD310iJvCTQBQAA8kQPWODyRA8RpCQwBQAA8g8QRCRY8g9Zg0jZAgDyDxFEJFi//gEAACn3SIm8JMgFAAD31kiJtCTABQAAvv4BAAAp1kiJtCS4BQAA99JIiZQksAUAALr+AQAAKcpIiZQkqAUAAPfRSImMJKAFAAC5/gEAACnBSImMJJgFAABBicFB99FBuv4BAABFKdpB99OLRCRISImEJJAFAABJvuB1AAAAAAAASLnodQAAAAAAAEi68HUAAAAAAABIvvh1AAAAAAAASL8AdgAAAAAAAEi9CHYAAAAAAABIuBB2AAAAAAAARTHt8kEPEAbyDxGEJJAAAADyDxAB8g8RhCSIAAAA8g8QAvIPEUQkYPIPEAbyDxFEJBDyDxAH8g8RBCTyDxBFAPIPEUQkKPIPEADyDxFEJAhmDx+EAAAAAADHQwgAAAAAD1fA8g8qg5xhAADyDxGEJFABAADyRA8Qs4hhAADyRA8Qk5BhAADyRA9Yk4BhAACLewyLQyxIi4wkeAgAAAHBgeH/AwAA8g8QlMugYQAASIuMJHAIAAABwYHh/wMAAPIPEKTLoGEAAEiLjCRoCAAAAcGB4f8DAADyDxCsy6BhAADyD1mkJNgDAABIi4wkYAgAAAHBgeH/AwAA8g8QtMugYQAA8g9ZrCTQAwAA8g9ZtCTwAwAASIuMJFgIAAABwYHh/wMAAPIPWOzyDxCky6BhAADyD1mkJPgDAADyD1jm8g9ZlCToAwAA8g9Y5fIPWaQkWAQAAPIPWOLyDxFjOPIPEGNA8g9ZZCQQ8kQPEGtQ8kQPWSwk8kQPWOzyRA8Ra0jyDxCjqIEAAPIPEWNY8g8QY3DyD1hjYPIPEGtoQYnE8kQPEKQkkAAAAPJBD1ns8g9ZpCSIAAAA8g9Y5UGB5P8DAADyRA8QfCRg8kEPWedIi4wkUAgAAAHBgeH/AwAA8kIPEWTjePIPEGTLeEiLjCRACAAAAcGB4f8DAADyDxBsy3hIi4wkMAgAAAHBgeH/AwAA8g8QdMt48g9ZrCSwAwAASIuMJCAIAAABwYHh/wMAAPIPEHzLePIPWbQkiAUAAPIPWbwkwAMAAEiLjCQQCAAAAcGB4f8DAADyD1j18g8QbMt48g9ZrCTIAwAA8g9Y7/IPWaQkuAMAAPIPWO7yD1msJGAEAADyD1js8g8Rq3ggAADyRA8Qm7AgAADyRA9Zm6AgAADyDxCr2CAAAPJED1icJLAJAADyRA8Rm6ggAADyRA8RXCQgaYu8IAAAbU7GQYHBOTAAAPIPKsGJi7ggAADyDxGDwCAAAPIPEGQkKPIPWcTyRA8Qg+AgAABmQQ8o8GYPKP3yD1m0JDgFAADyD1m8JDABAADyD1j+8kQPEIwkcAkAAPJBD1n5Zg8o8PIPEUQkSPIPXPfyDxGz0CAAAPIPEJsIIQAAZg8o+/IPEJQkqAkAAPIPWPpmDyjLZg86C/8J8g9cz/IPWMryDxGLACEAAGYPKPnyD1z78g8RuxAhAABmD1fSZg8ukxghAABAD5PGZg8u+g+XwUAg8Q+28YDxAQ+2yQ9X//IPKvkPV9LyDyrW8g9ZuyghAADyD1nQ8g9Y1/IPEZMgIQAA8g8QRCQI8g9Z0PIPWNBmDy7KD5PBZg8u0/IPEJPIIAAA8g8RVCQYD5fCIMoPtsoPV8nyDyrJ8g9ZyvIPEYswIQAA8g9ZzPIPEJM4IQAA8g9Z1IuLRCEAADHShckPn8IByjHJZg8uyvIPWO1BD0fX8kEPWOgPl8HyRQ9Z9AHRD1fJ8g8qyYmLQCEAAPIPWYwksAgAAPIPWO7yDxBUJEDyD1zR8g9dymYPV8DyD1/I8kUPWdnyRA9Z3fIPELwkiAAAAPJED1nX8kQPWdnyDxCLgCAAAPJBD1jL8kUPWNbyDxGLSCEAAPIPEItQIQAARI1A/0SJxvJEDxBEJBDyQQ9ZyIHm/wMAAPIPEJTzWCEAAPIPEAQk8g9Z0PJFD1nXZkUPKPfyD1jRicIrlCTEAQAAgeL/AwAA8kIPEZTjWCEAAPIPEJzTWCEAAInCK5QkwAEAAIHi/wMAAPIPWZwk6AMAAPIPEIzTWCEAAInCK5QkvAEAAIHi/wMAAPIPWYwk2AMAAPIPEJTTWCEAAInCK5QkuAEAAIHi/wMAAPIPWZQk0AMAAPIPELTTWCEAAPIPWbQk8AMAAInCK5QktAEAAPIPWNGB4v8DAADyDxCs01ghAADyD1msJPgDAADyD1ju8kQPEatYQQAA8kQPWJtgQQAA8g9Y6vJGDxGc42hBAABIi4wkSAgAAI0UCIHi/wMAAPIPWawkWAQAAPIPEJTTaEEAAEiLjCQ4CAAAjRQIgeL/AwAA8g9ZlCS4AwAA8g8QjNNoQQAASIuMJCgIAACNFAiB4v8DAADyD1mMJLADAADyDxCk02hBAABIi4wkGAgAAI0UCIHi/wMAAPIPWaQkiAUAAPIPELTTaEEAAPIPWbQkwAMAAEiLjCQICAAAjRQI8g9Y64Hi/wMAAPIPEJzTaEEAAPIPWZwkyAMAAPIPWOHyD1je8g9Y3PIPWZwkYAQAAPJED1iUJFABAADyD1ja8g8Rq2hhAADyDxCLcGEAAPIPEJOAYQAA8g8RlCRQAQAA8g8Ri3hhAACJu5hhAADyRg8RlOOgYQAA8g8Rm6CBAADHg7CBAAAAAAAAi7u0gQAAD1fJ8g8qi8ziAADyDxGMJEgBAADyRA8Qu7jiAABIi4wkAAgAAI0UCIHi/wMAAPJEDxCLwOIAAPIPEIzT0OIAAEiLjCT4BwAAjRQIgeL/AwAA8g8QlNPQ4gAASIuMJPAHAACNFAiB4v8DAADyDxCc09DiAADyD1mUJJADAABIi4wk6AcAAI0UCIHi/wMAAPIPEKTT0OIAAPIPWZwkiAMAAPIPWaQkoAMAAEiLjCTgBwAAjRQIgeL/AwAA8g9Y2vIPEJTT0OIAAPIPWZQkqAMAAPIPWNTyD1mMJJgDAADyD1jT8g9ZlCRIBAAA8kQPWIuw4gAA8g9Y0fIPEZO4gQAA8g8Qi8CBAADyQQ9ZyPJEDxCb0IEAAPJED1nY8kQPWNnyRA8Rm8iBAADyDxCL2AIBAPIPEYvYgQAA8g8Qi+iBAABmRQ8oxPJBD1nM8g8Qk/CBAADyD1iT4IEAAPIPWdfyD1jRZkUPKO7yQQ9Z1kiLjCTYBwAAjRQB8kIPEZTj+IEAAIHi/wMAAPIPEIzT+IEAAEiLjCTQBwAAjRQB8g9ZjCRwAwAAgeL/AwAA8g8QlNP4gQAASIuMJMgHAACNFAHyD1mUJGgDAACB4v8DAADyDxCc0/iBAABIi4wkwAcAAI0UAfIPWZwkYAMAAIHi/wMAAPIPEKTT+IEAAEiLjCS4BwAAjRQB8g9ZpCR4AwAAgeL/AwAA8g8QrNP4gQAA8g9ZrCSAAwAA8g9Y2vIPWOzyD1jr8g9ZrCRQBAAA8g9Y6fIPEav4oQAA8g8QmxiiAADyRA8QkyCiAABmQQ8oymYPKNPyD1mMJCgBAADyD1mUJCABAADyD1jR8kQPEKQkaAkAAPJBD1nU8g8QZCRIZg8oxPIPXMLyDxGDEKIAAPIPEIs4ogAAZg8o0fIPELQkoAkAAPIPWNZmDyjpZg86C9IJ8g9c6vIPWO7yDxGrMKIAAGYPKNXyD1zR8g8Rk0CiAABmD1f2Zg8us0iiAABAD5PFZg8u1g+XwkAg6g+26oDyAQ+20g9X9vIPKvIPV9LyDyrV8g9Zs1iiAADyD1nU8g9Y1vIPEZNQogAA8g8QZCQI8g9Z1PIPWNTyD1jbZg8u6vJBD1jaD5PCZg8u0Q+XwSDRD7bJD1fJ8g8qyfIPWUwkGPIPEYtgogAA8kQPEHQkKPJBD1nO8g8Qk2iiAACLi3SiAADyQQ9Z1jHShckPn8IByjHJZg8uyvJFD1n4QQ9H1/JED1nP8kUPWM8Pl8FmRQ8o1fJFD1nNAdEPV8nyDyrJiYtwogAA8g9ZjCSoCAAA8g9Y2PIPEFQkQPIPXNHyD13KZg9XwPIPX8jyDxBkJCDyQQ9Z5PIPWePyD1nh8g8QiwCiAADyD1jM8g8Ri3iiAADyDxCLgKIAAPIPEHwkEPIPWc/yDxCU84iiAADyRA8QNCTyQQ9Z1vJED1iMJEgBAADyD1jRicErjCSwAQAAgeH/AwAA8kIPEZTjiKIAAPIPEIzLiKIAAInBK4wkrAEAAIHh/wMAAPIPWYwkmAMAAPIPEJTLiKIAAInBK4wkqAEAAIHh/wMAAPIPWZQkkAMAAPIPEJzLiKIAAInBK4wkpAEAAIHh/wMAAPIPWZwkiAMAAPIPEKzLiKIAAPIPWawkoAMAAInBK0wkfPIPWNqB4f8DAADyDxCUy4iiAADyD1mUJKgDAADyD1jV8kQPEZuIwgAA8g9Yo5DCAADyD1jT8kIPEaTjmMIAAEiLjCQgBQAAAcGB4f8DAADyD1mUJEgEAADyDxCcy5jCAABIi4wkGAUAAAHBgeH/AwAA8g9ZnCRwAwAA8g8QpMuYwgAASIuMJBAFAAABwYHh/wMAAPIPWaQkaAMAAPIPEKzLmMIAAEiLjCQIBQAAAcGB4f8DAADyD1msJGADAADyDxC0y5jCAADyD1m0JHgDAABIi4wkAAUAAAHB8g9Y0YHh/wMAAPIPEIzLmMIAAPIPWYwkgAMAAPIPWOzyD1jO8g9YzfIPWYwkUAQAAPIPWMvyDxGTmOIAAPIPEJOg4gAA8g8Qg7DiAADyDxGEJEgBAADyDxGTqOIAAIm7yOIAAPJGDxGM49DiAADyDxGL0AIBAMeD4AIBAAAAAABEi7PkAgEAD1fA8g8qg/wzAQDyDxGEJLgAAADyRA8Qo+gzAQDyRA8Qg/AzAQBIi4wksAcAAAHBgeH/AQAA8g8QlMsANAEASIuMJKgHAAABwYHh/wEAAPIPEJzLADQBAPIPWZwkSAMAAEiLjCSgBwAAAcGB4f8BAADyDxCkywA0AQDyD1mkJEADAABIi4wkmAcAAAHBgeH/AQAA8g8QrMsANAEA8g9ZrCRQAwAASIuMJJAHAAABwYHh/wEAAPIPELTLADQBAPIPWOPyD1m0JFgDAADyD1j18g9Y9PJEDxC8JGAJAADyQQ9Z1/IPWbQkOAQAAPIPWPLyRA9Yg+AzAQDyDxGz6AIBAPIPEJvwAgEA8kQPEKsAAwEA8g9Z3/JFD1nu8kQPWOvyRA8Rq/gCAQDyDxCbCEQBAPIPEZsIAwEAicfyDxCbGAMBAPIPEKMgAwEA8kQPEJwkkAAAAPJBD1nb8g9YoxADAQDyRA8QjCSIAAAA8kEPWeGB5/8BAADyD1jj8kEPWeJIi4wkiAcAAAHB8g8RpPsoAwEAgeH/AQAA8g8QnMsoAwEASIuMJIAHAAABwfIPWZwkMAMAAIHh/wEAAPIPEKTLKAMBAEiLjCR4BwAAAcHyD1mkJCgDAACB4f8BAADyDxCsyygDAQBIi4wkcAcAAAHB8g9ZrCQgAwAAgeH/AQAA8g8QtMsoAwEASIuMJGgHAAABwfJEDxC0JFgJAADyQQ9Z9oHh/wEAAPIPELzLKAMBAPIPWbwkOAMAAPIPWOzyD1j+8g9Y/fIPWbwkQAQAAPIPWPvyDxG7KBMBAPIPEJtIEwEA8g8Qo1ATAQBmDyjsZg8o8/IPWawkGAEAAPIPWbQkEAEAAPIPWPXyRA8QlCRQCQAA8kEPWfLyDxBsJEhmDyjF8g9cxvIPEYNAEwEA8g8Qi2gTAQBmDyj58g8QlCSYCQAA8g9Y+mYPKPFmDzoL/wnyD1z38g9Y8vIPEbNgEwEAZg8o/vIPXPnyDxG7cBMBAGYPV9JmDy6TeBMBAA+TwWYPLvoPl8Igyg+2yoDyAQ+20g9X//IPKvoPV9LyDyrR8g9Zu4gTAQDyD1nV8g9Y1/IPEZOAEwEA8g8QbCQI8g9Z1fIPWNVmDy7yD5PBZg8u0Q+XwiDKD7bKD1fJ8g8qyfIPWUwkGPIPEYuQEwEA8g8QbCQo8g9ZzfIPEJOYEwEA8g9Z1YuLpBMBADHShckPn8IByjHJZg8uykEPR9fyD1jb8g9Y3A+XwQHRD1fJ8g8qyfJFD1nj8kUPWcHyD1mMJKAIAADyD1jY8g8QVCRAiYugEwEA8g9c0fIPXcryDxBkJCDyQQ9Z4mYPV8DyD1/I8g9Z4/IPWeHyDxCLMBMBAPJFD1jE8g9YzPIPEYuoEwEA8g8Qi7ATAQDyRA8QVCQQ8kEPWcpBgeD/AQAA8kIPEJTDuBMBAPIPEDwk8g9Z1/JEDxBcJGDyRQ9Zw/IPWNGJwStMJHiB4f8BAADyDxGU+7gTAQDyDxCMy7gTAQCJwStMJHSB4f8BAADyQQ9Zz/IPEJTLuBMBAInBK4wkoAEAAIHh/wEAAPIPWZQkSAMAAPIPEKzLuBMBAInBK4wknAEAAIHh/wEAAPIPWawkQAMAAPIPELTLuBMBAPIPWbQkUAMAAInBK4wkmAEAAPIPWOqB4f8BAADyDxCcy7gTAQDyD1mcJFgDAADyD1je8kQPEau4IwEA8g9Yo8AjAQDyD1jd8g8RpPvIIwEASIuMJPgEAAABwYHh/wEAAPIPWZwkOAQAAPIPEJTLyCMBAEiLjCTwBAAAAcGB4f8BAADyD1mUJDADAADyDxCky8gjAQBIi4wk6AQAAAHBgeH/AQAA8g9ZpCQoAwAA8g8QrMvIIwEASIuMJOAEAAABwYHh/wEAAPIPWawkIAMAAPIPELTLyCMBAPJBD1n2SIuMJNgEAAABwfIPWNmB4f8BAADyDxCMy8gjAQDyD1mMJDgDAADyD1js8g9YzvIPWM3yD1mMJEAEAADyRA9YhCS4AAAA8g9YyvIPEZvIMwEA8g8Qk9AzAQDyDxCb4DMBAPIPEZwkuAAAAPIPEZPYMwEARImz+DMBAPJEDxGE+wA0AQDyDxGLAEQBAMeDEEQBAAAAAACLqxREAQDyDyqDLKUBAPIPEYQksAAAAPJEDxCzGKUBAEiLjCRgBwAAAcGB4f8DAADyRA8QgyClAQDyDxCEyzClAQBIi4wkWAcAAAHBgeH/AwAA8g8QnMswpQEASIuMJFAHAAABwYHh/wMAAPIPEKTLMKUBAPIPWZwkCAMAAEiLjCRIBwAAAcGB4f8DAADyDxCsyzClAQDyD1mkJAADAADyD1msJBADAABIi4wkQAcAAAHBgeH/AwAA8g9Y4/IPEJzLMKUBAPIPWZwkGAMAAPIPWN3yRA8QvCRICQAA8kEPWcfyD1jc8g9ZnCQoBAAA8kQPWIMQpQEA8g9Y2PIPEZsYRAEA8g8QgyBEAQDyQQ9ZwvJEDxCrMEQBAPJED1nv8kQPWOjyRA8RqyhEAQDyDxCDOMUBAPIPEYM4RAEA8g8Qg0hEAQDyRA8QpCSQAAAA8kEPWcTyDxCbUEQBAPIPWJtARAEA8kEPWdnyD1jY8kEPWdtIi4wkOAcAAAHB8kIPEZzjWEQBAIHh/wMAAPIPEITLWEQBAEiLjCQwBwAAAcHyD1mEJOgCAACB4f8DAADyDxCcy1hEAQBIi4wkKAcAAAHB8g9ZnCTgAgAAgeH/AwAA8g8QpMtYRAEASIuMJCAHAAABwfIPWaQk2AIAAIHh/wMAAPIPEKzLWEQBAEiLjCQYBwAAAcHyD1msJPACAACB4f8DAADyDxC8y1hEAQDyD1m8JPgCAADyD1jj8g9Y/fIPWPzyD1m8JDAEAADyD1j48g8Ru1hkAQDyDxCbeGQBAPIPELuAZAEAZg8ox2YPKOvyD1mEJAgBAADyD1msJAABAADyD1jo8kQPEJQkQAkAAPJBD1nq8g8QZCRIZg8o1PIPXNXyDxGTcGQBAPIPEIuYZAEAZg8owfIPELQkkAkAAPIPWMZmDyjpZg86C8AJ8g9c6PIPWO7yDxGrkGQBAGYPKMXyD1zB8g8Rg6BkAQBmD1f2Zg8us6hkAQAPk8FmDy7GD5fCIMoPtsqA8gEPttIPV/byDyry8g9Zs7hkAQAPV8DyDyrB8g9ZxPIPWMbyDxGDsGQBAPIPEGQkCPIPWcTyD1jEZg8u6A+TwfIPWNtmDy7B8g9Y3w+XwiDKD7bKD1fA8g8qwfIPWUQkGPIPEYPAZAEA8g8QZCQo8g9ZxPIPEIvIZAEA8g9ZzIuL1GQBADHShckPn8IByjHJZg8uwUEPR9dmRQ8o3PJFD1n08kUPWcEPl8HyRQ9YxgHRD1fA8g8qwYmL0GQBAPIPWYQkmAgAAPIPWNryDxBMJEDyD1zI8g9dwWYPV8nyD1/B8g8QZCQg8kEPWeLyD1nj8g9Z4PIPEINgZAEA8g9YxPIPEYPYZAEA8g8Qg+BkAQDyDxBUJBDyD1nC8g8QjPPoZAEA8g8QPCTyD1nP8kQPEEwkYPJFD1nB8g9YyInBK0wkcIHh/wMAAPJCDxGM4+hkAQDyDxCMy+hkAQCJwStMJGyB4f8DAADyQQ9Zz/IPEITL6GQBAInBK4wklAEAAIHh/wMAAPIPWYQkCAMAAPIPEKzL6GQBAInBK4wkkAEAAIHh/wMAAPIPWawkAAMAAPIPELTL6GQBAPIPWbQkEAMAAInBK0wkaPIPWOiB4f8DAADyDxCcy+hkAQDyD1mcJBgDAADyD1je8kQPEavohAEA8g9Yo/CEAQDyD1jd8kIPEaTj+IQBAEiLjCTQBAAAAcGB4f8DAADyD1mcJCgEAADyDxCEy/iEAQBIi4wkyAQAAAHBgeH/AwAA8g9ZhCToAgAA8g8QpMv4hAEASIuMJMAEAAABwYHh/wMAAPIPWaQk4AIAAPIPEKzL+IQBAEiLjCS4BAAAAcGB4f8DAADyD1msJNgCAADyDxC0y/iEAQDyD1m0JPACAABIi4wksAQAAAHB8g9Y2YHh/wMAAPIPEIzL+IQBAPIPWYwk+AIAAPIPWOzyD1jO8g9YzfIPWYwkMAQAAPJED1iEJLAAAADyD1jI8g8Rm/ikAQDyDxCDAKUBAPIPEJsQpQEA8g8RnCSwAAAA8g8RgwilAQCJqyilAQDyRg8RhOMwpQEA8g8RizDFAQDHg0DFAQAAAAAAi6tExQEAD1fA8g8qg1z2AQDyDxGEJKgAAADyRA8Qo0j2AQBIi4wkEAcAAAHBgeH/AQAA8kQPEINQ9gEA8g8QhMtg9gEASIuMJAgHAAABwYHh/wEAAPIPEJzLYPYBAEiLjCQABwAAAcGB4f8BAADyDxCky2D2AQDyD1mcJMACAABIi4wk+AYAAAHBgeH/AQAA8g8QrMtg9gEA8g9ZpCS4AgAA8g9ZrCTIAgAASIuMJPAGAAABwYHh/wEAAPIPWOPyDxCcy2D2AQDyD1mcJNACAADyD1jd8kQPELwkOAkAAPJBD1nH8g9Y3PIPWZwkIAQAAPJED1iDQPYBAPIPWNjyDxGbSMUBAPIPEINQxQEA8g9ZwvJEDxCzYMUBAPJED1n38kQPWPDyRA8Rs1jFAQDyDxCDaAYCAPIPEYNoxQEA8g8Qg3jFAQDyQQ9Zw/IPEJuAxQEA8g9Ym3DFAQDyRA8QlCSIAAAA8kEPWdryD1jY8kEPWdlIi4wk6AYAAAHB8g8RnPuIxQEAgeH/AQAA8g8QhMuIxQEASIuMJOAGAAABwfIPWYQkqAIAAIHh/wEAAPIPEJzLiMUBAEiLjCTYBgAAAcHyD1mcJKACAACB4f8BAADyDxCky4jFAQBIi4wk0AYAAAHB8g9ZpCSYAgAAgeH/AQAA8g8QrMuIxQEASIuMJMgGAAABwfJEDxCcJDAJAADyQQ9Z64Hh/wEAAPIPELzLiMUBAPIPWbwksAIAAPIPWOPyD1j98g9Y/PJEDxCsJEAFAADyQQ9Z/fIPWPjyDxG7iNUBAPIPEJuo1QEA8g8Qu7DVAQBmDyjHZg8o6/IPWYQk+AAAAPIPWawk8AAAAPIPWOjyRA8QjCQoCQAA8kEPWenyDxBkJEhmDyjU8g9c1fIPEZOg1QEA8g8Qi8jVAQBmDyjB8g8QtCSICQAA8g9YxmYPOgvACWYPKOnyD1zo8g9Y7vIPEavA1QEAZg8oxfIPXMHyDxGD0NUBAGYPV/ZmDy6z2NUBAA+TwWYPLsYPl8Igyg+2yoDyAQ+20g9X9vIPKvLyD1mz6NUBAA9XwPIPKsHyD1nE8g9YxvIPEYPg1QEA8g8QZCQI8g9ZxPIPWMRmDy7oD5PB8g9Y22YPLsHyD1jfD5fCIMoPtsoPV8DyDyrB8g9ZRCQY8g8Rg/DVAQDyDxBkJCjyD1nE8g8Qi/jVAQDyD1nMi4sE1gEAMdKFyQ+fwgHKMclmDy7BQQ9H1/IPELwkkAAAAPJED1nn8kUPWcIPl8HyRQ9YxAHRD1fA8g8qwYmLANYBAPIPWYQkkAgAAPIPWNryDxBMJEDyD1zI8g9dwWYPV8nyD1/B8g8QZCQg8kEPWeHyD1nj8g9Z4PIPEIOQ1QEA8g9YxPIPEYMI1gEA8g8QgxDWAQDyRA8QZCQQ8kEPWcTyQg8QjMMY1gEA8g8QFCTyD1nK8kQPEEwkYPJFD1nB8g9YyInBK4wkjAEAAIHh/wEAAPIPEYz7GNYBAPIPEIzLGNYBAInBK4wkyAEAAIHh/wEAAPJBD1nP8g8QhMsY1gEAicErjCSIAQAAgeH/AQAA8g9ZhCTAAgAA8g8QrMsY1gEAicErjCSEAQAAgeH/AQAA8g9ZrCS4AgAA8g8QtMsY1gEA8g9ZtCTIAgAAicErjCSAAQAA8g9Y6IHh/wEAAPIPEJzLGNYBAPIPWZwk0AIAAPIPWN7yRA8RsxjmAQDyD1ijIOYBAPIPWN3yDxGk+yjmAQBIi4wkqAQAAAHBgeH/AQAA8g9ZnCQgBAAA8g8QhMso5gEASIuMJKAEAAABwYHh/wEAAPIPWYQkqAIAAPIPEKTLKOYBAEiLjCSYBAAAAcGB4f8BAADyD1mkJKACAADyDxCsyyjmAQBIi4wkkAQAAAHBgeH/AQAA8g9ZrCSYAgAA8g8QtMso5gEA8kEPWfNIi4wkiAQAAAHB8g9Y2YHh/wEAAPIPEIzLKOYBAPIPWYwksAIAAPIPWOzyD1jO8g9YzfJBD1nN8kQPWIQkqAAAAPIPWMjyDxGbKPYBAPIPEIMw9gEA8g8Qm0D2AQDyDxGcJKgAAADyDxGDOPYBAImrWPYBAPJEDxGE+2D2AQDyDxGLYAYCAMeDcAYCAAAAAACLq3QGAgAPV8DyDyqDjDcCAPIPEYQkoAAAAPJEDxCreDcCAEiLjCTABgAAAcGB4f8BAADyRA8Qg4A3AgDyDxCcy5A3AgBIi4wkuAYAAAHBgeH/AQAA8g8QhMuQNwIASIuMJLAGAAABwYHh/wEAAPIPEKTLkDcCAPIPWYQkeAIAAEiLjCSoBgAAAcGB4f8BAADyDxCsy5A3AgDyD1mkJHACAADyD1msJIgCAABIi4wkoAYAAAHBgeH/AQAA8g9Y4PIPEITLkDcCAPIPWYQkkAIAAPIPWMXyD1mcJIACAADyD1jE8g9ZhCQQBAAA8kQPWINwNwIA8g9Yw/IPEYN4BgIA8g8Qg4AGAgBmRQ8o3PJBD1nE8kQPELOQBgIA8kQPWfLyRA9Y8PJEDxGziAYCAPIPEIOYRwIA8g8Rg5gGAgDyDxCDqAYCAPIPWcfyDxCbsAYCAPIPWJugBgIA8kEPWdryD1jY8kEPWdlIi4wkmAYAAAHB8g8RnPu4BgIAgeH/AQAASIuUJJAGAAABwoHi/wEAAPIPEJzLuAYCAPIPEITTuAYCAEiLjCSIBgAAAcGB4f8BAADyD1mEJFgCAADyDxCky7gGAgBIi4wkgAYAAAHBgeH/AQAA8g9ZpCRQAgAA8g8QrMu4BgIA8g9ZrCRgAgAASIuMJHgGAAABwfIPWOCB4f8BAADyDxCEy7gGAgDyD1mEJGgCAADyRA8QvCQgCQAA8kEPWd/yD1jF8g9YxPIPWYQkGAQAAPIPWMPyDxGDuBYCAPIPEIvYFgIA8g8Qq+AWAgBmDyjFZg8o2fIPWYQk6AAAAPIPWZwk4AAAAPIPWNjyRA8QjCQYCQAA8kEPWdnyDxBkJEhmDyjU8g9c0/IPEZPQFgIA8g8Qs/gWAgBmDyje8g8QvCSACQAA8g9Y32YPOgvbCWYPKMbyD1zD8g9Yx/IPEYPwFgIAZg8o2PIPXN7yDxGbABcCAGYPV/9mDy67CBcCAA+TwWYPLt9mD1f/D5fCIMoPtsqA8gEPttIPV9vyDyra8g9ZmxgXAgDyRA8q4fJED1nk8kQPWOPyRA8RoxAXAgDyDxBcJAjyRA9Z4/JED1jjZkEPLsQPk8FmRA8u5vIPWMnyD1jND5fCIMoPtsoPV8DyDyrB8g9ZRCQY8g8RgyAXAgDyDxBkJCjyD1nE8g8QmygXAgDyD1nci4s0FwIAMdKFyQ+fwgHKMclmDy7DQQ9H1/JEDxCkJJAAAADyRQ9Z7PJFD1nCD5fB8kUPWMUB0Q9XwPIPKsGJizAXAgDyD1mEJIgIAADyD1jK8g8QXCRA8g9c2PIPXcPyD1/H8g8QZCQg8kEPWeHyD1nh8g9Z4PIPEIPAFgIA8g9YxPIPEYM4FwIA8g8Qg0AXAgBmQQ8o+/JBD1nD8kIPEIzDSBcCAPJEDxAMJPJBD1nJ8kQPEFwkYPJFD1nD8g9YyInBK4wkfAEAAIHh/wEAAPIPEYz7SBcCAPIPEIzLSBcCAInBK4wkeAEAAIHh/wEAAPIPWYwkgAIAAPIPEITLSBcCAInBK4wkdAEAAIHh/wEAAPIPWYQkeAIAAPIPEKzLSBcCAInBK4wkcAEAAIHh/wEAAPIPWawkcAIAAPIPELTLSBcCAPIPWbQkiAIAAInBK4wkbAEAAPIPWOiB4f8BAADyDxCcy0gXAgDyD1mcJJACAADyD1je8kQPEbNIJwIA8g9Yo1AnAgDyD1jd8g8RpPtYJwIASIuMJIAEAAABwYHh/wEAAPIPWZwkEAQAAPIPEKTLWCcCAEiLjCR4BAAAAcGB4f8BAADyQQ9Z5/IPEITLWCcCAEiLjCRwBAAAAcGB4f8BAADyD1mEJFgCAADyDxCsy1gnAgBIi4wkaAQAAAHBgeH/AQAA8g9ZrCRQAgAA8g8QtMtYJwIA8g9ZtCRgAgAASIuMJHAGAAABwfIPWNmB4f8BAADyDxCMy1gnAgDyD1mMJGgCAADyD1jo8g9YzvIPWM3yD1mMJBgEAADyRA9YhCSgAAAA8g9YzPIPEZtYNwIA8g8Qm2A3AgDyDxCDcDcCAPIPEYQkoAAAAPIPEZtoNwIAiauINwIA8kQPEYT7kDcCAPIPEYuQRwIAx4OgRwIAAAAAAESLs6RHAgAPV8DyDyqDvKgCAPIPEYQkmAAAAPJEDxCzqKgCAEiLjCRoBgAAAcGB4f8DAADyRA8Qg7CoAgDyDxCUy8CoAgBIi4wkYAYAAAHBgeH/AwAA8g8QnMvAqAIASIuMJFgGAAABwYHh/wMAAPIPEKTLwKgCAPIPWZwkMAIAAEiLjCRQBgAAAcGB4f8DAADyDxCsy8CoAgDyD1mkJCgCAADyD1msJEACAABIi4wkSAYAAAHBgeH/AwAA8g9Y4/IPEJzLwKgCAPIPWZwkSAIAAPIPWN3yD1mUJDgCAADyD1jc8g9ZnCQABAAA8kQPWIOgqAIA8g9Y2vIPEZuoRwIA8g8Qk7BHAgDyD1nX8kQPEJPARwIA8kUPWdHyRA9Y0vJEDxGTuEcCAPIPEJPIyAIA8g8Rk8hHAgDyDxCT2EcCAPJBD1nU8g8Qm+BHAgDyD1ib0EcCAPJEDxCsJIgAAADyQQ9Z3fIPWNryQQ9Z20iLjCRABgAAjSwB8kIPEZzj6EcCAIHl/wMAAEiLjCQwBgAAAcGB4f8DAADyDxCUy+hHAgBIi4wkIAYAAAHBgeH/AwAA8g8QnMvoRwIA8g9ZlCQIAgAASIuMJBAGAAABwYHh/wMAAPIPEKTL6EcCAPJEDxC8JBAJAADyQQ9Z3/IPWaQkGAIAAEiLjCQABgAAAcGB4f8DAADyD1ja8g8QlMvoRwIA8g9ZlCQgAgAA8g9Y1PIPEKTr6EcCAPIPWaQkEAIAAPIPWNPyD1mUJAgEAADyD1jU8g8Rk+hnAgDyDxCjCGgCAPIPEKsQaAIAZg8o1fIPWZQk2AAAAGYPKNzyD1mcJNAAAADyD1ja8kQPEIwkCAkAAPJBD1nZ8g8QVCRIZg8oyvIPXMvyDxGLAGgCAPIPEIMoaAIAZg8o2PIPELwkeAkAAPIPWN9mDzoL2wlmDyjw8g9c8/IPWPfyDxGzIGgCAGYPKN7yD1zY8g8RmzBoAgBmD1f/Zg8uuzhoAgAPk8FmDy7fZg9X/w+XwiDKD7bKgPIBD7bSD1fb8g8q2vIPWZtIaAIA8kQPKtnyRA9Z2vJED1jb8kQPEZtAaAIA8g8QVCQI8kQPWdryRA9Y2mZBDy7zD5PBZkQPLtgPl8Igyg+2yg9XwPIPKsHyD1lEJBjyDxGDUGgCAPIPEFQkKPIPWcLyDxCbWGgCAPIPWdqLi2RoAgAx0oXJD5/CAcoxyWYPLsNBD0fXD5fBAdHyD1jk8g9Y5Q9XwPIPKsHyRQ9Z9PIPWYQkgAgAAPIPWOHyDxBUJECJi2BoAgDyD1zQ8g9dwvIPEGwkIPJBD1np8g9fx/IPWezyD1no8g8Qg/BnAgBmRQ8o3fJFD1nF8g9YxfIPEYNoaAIA8g8Qg3BoAgDyRA8QTCQQ8kEPWcHyDxCU83hoAgDyDxAMJPIPWdHyD1jQ8kUPWMbyQg8RlON4aAIAicErjCRoAQAAgeH/AwAAicLyDxCUy3hoAgArlCRkAQAAgeL/AwAAicErjCRgAQAA8g8QnNN4aAIAgeH/AwAA8g8QtMt4aAIAicErjCRcAQAAicKB4f8DAAArlCRYAQAAgeL/AwAA8g8QhMt4aAIA8g8QpNN4aAIA8kQPEZN4iAIA8g9ZnCQwAgAA8g9ZtCQoAgAA8g9Y8/IPEHwkYPJED1nH8g9ZlCQ4AgAA8g9ZhCRAAgAA8g9ZpCRIAgAA8g9Yq4CIAgDyQg8RrOOIiAIA8g9Y4EiLjCQ4BgAAAcGB4f8DAADyDxCcy4iIAgDyD1jmSIuMJCgGAAABwYHh/wMAAPIPEITLiIgCAPIPWaQkAAQAAEiLjCQYBgAAAcGB4f8DAADyDxCsy4iIAgDyD1mEJAgCAADyQQ9Z70iLjCQIBgAAAcGB4f8DAADyD1ji8g8QlMuIiAIA8g9ZlCQYAgAASIuMJPgFAAABwfIPWOiB4f8DAADyDxCEy4iIAgDyD1mEJCACAADyD1mcJBACAADyD1jC8g9YxfIPWYQkCAQAAPJED1iEJJgAAADyD1jD8g8Ro4ioAgDyDxCTkKgCAPJEDxCjoKgCAPIPEZOYqAIARImzuKgCAPJGDxGE48CoAgDyDxGDwMgCAMeD0MgCAAAAAACLq9TIAgAPV8DyDyqD7PkCAPIPEYQkmAAAAPJEDxCr2PkCAEiLjCTwBQAAjRQIgeL/AQAA8kQPELPg+QIASIuMJOgFAACNNAiB5v8BAADyDxCE8/D5AgDyD1mEJOgBAABIi4wk4AUAAI00CIHm/wEAAPIPEJTz8PkCAPIPWZQk4AEAAEiLjCTYBQAAjTQIgeb/AQAA8g8QpPPw+QIA8g9Y0EiLjCTQBQAAjTQIgeb/AQAA8g8QhPPw+QIA8g9ZpCT4AQAA8g9ZhCQAAgAA8g9YxPIPEKTT8PkCAPIPWaQk8AEAAPIPWMLyD1lEJDDyRA9Ys9D5AgDyD1jE8g8Rg9jIAgDyDxCD4MgCAPJBD1nB8kQPEJPwyAIA8kQPWdHyRA9Y0PJEDxGT6MgCAPIPEIP4CQMA8g8Rg/jIAgDyDxCDCMkCAPIPEJwkkAAAAPIPWcPyDxCjEMkCAPIPWKMAyQIA8kEPWePyD1jg8g9Z50iLjCS4BQAAjRQB8g8RpPsYyQIAgeL/AQAA8g8QhNMYyQIASIuMJKgFAACNFAHyD1mEJOADAACB4v8BAADyDxCk0xjJAgDyD1mkJIAAAADyD1jgSIuMJJgFAACNFAGB4v8BAADyDxCE0xjJAgDyRA8QvCQ4AQAA8kEPWcdBjRQCgeL/AQAA8g8QrNMYyQIA8kQPEJwk0AEAAPJBD1nr8g9Y6EiLjCTIBQAAjRQBgeL/AQAA8g9Y7PIPEITTGMkCAPIPWYQk2AEAAPIPWWwkOPIPWOjyDxGrGNkCAPIPEKtA2QIAZg8oxfIPWUQkUPIPEIs42QIAZg8o4fIPWaQkMAUAAPIPWODyRA8QhCQACQAA8kEPWeDyDxB0JEhmDyjW8g9c1PIPEZMw2QIA8kQPEItY2QIAZkEPKMHyDxB8JFjyD1jHZg86C8AJZkEPKOHyD1zg8g9Y5/IPEaNQ2QIAZg8oxPJBD1zB8g8Rg2DZAgBmD1f/Zg8uu2jZAgAPk8JmDy7HD5fBINEPttEPV8DyDyrC8g9ZxoDxAQ+2yQ9X//IPKvnyD1m7eNkCAPIPWMfyDxGDcNkCAPIPEHwkCPIPWcfyD1jHZg8u4A+TwWZBDy7BD5fCIMoPtsoPV8DyDyrB8g9ZRCQY8g8Rg4DZAgDyDxB0JCjyD1nG8g8Qo4jZAgDyD1nmi4uU2QIAMdKFyQ+fwgHKMclmDy7E8g9YyUEPR9fyD1jND5fBAdHyRA9Z6/JED1m0JIgAAADyRQ9Y9YmLkNkCAA9XwPIPKsHyD1mEJEABAADyD1jK8g8QXCRA8g9c2PIPXcPyDxBcJCDyQQ9Z2GYPV9LyD1/C8g9Z2fIPWdjyDxCDINkCAPJED1l0JGDyD1jDZg8o0/IPEYOY2QIA8g8Qg6DZAgDyD1lEJBDyQg8QjMOo2QIA8g9ZDCTyD1jIicHyDxGM+6jZAgArjCTMAAAAgeH/AQAA8g8QnMuo2QIAicHyRA9YtCSYAAAAK4wkyAAAAIHh/wEAAPIPEITLqNkCAInB8g9ZhCToAQAAK4wkxAAAAIHh/wEAAPIPEIzLqNkCAInB8g9ZjCTgAQAAK4wkwAAAAIHh/wEAAPIPEKzLqNkCAInB8g9YyCuMJCgFAACB4f8BAADyDxCky6jZAgDyD1msJPgBAADyD1mkJAACAADyD1jl8kQPEZOo6QIA8g9Y4fIPWJOw6QIASIuMJMAFAAABwfIPEZT7uOkCAIHh/wEAAPIPEIzLuOkCAEiLjCSwBQAAAcGB4f8BAADyDxCEy7jpAgBIi4wkoAUAAAHBgeH/AQAA8g8QlMu46QIAQo0MCIHh/wEAAPIPEKzLuOkCAPIPWZwk8AEAAPIPWWQkMEQB2CX/AQAA8g9Y4/IPEJzDuOkCAPIPWYQk4AMAAPIPWZQkgAAAAPIPWNDyQQ9Z7/JBD1nb8g9Y3fIPWNryDxGjuPkCAPIPEIPA+QIA8kQPEIwkUAEAAGZBDyjh8g9ZpCSABQAA8g8QvCRIAQAAZg8o7/IPEJPQ+QIA8g9ZrCR4BQAA8g9Y7PJEDxCEJLgAAABmQQ8o4PIPWaQkcAUAAPIPEYPI+QIA8g8QtCSwAAAAZg8oxvIPWYQkaAUAAPIPWMSJq+j5AgDyRA8RtPvw+QIA8g9ZjCTYAQAA8g9ZXCQ48g9YxfJEDxCcJKgAAABmQQ8o4/IPWNnyD1mkJGAFAADyDxCsJKAAAABmDyjN8g9ZjCRYBQAA8g9YzPIPEZvwCQMAZkEPKNzyD1mcJFAFAADyD1jZ8g9Y2GZBDyjB8g9ZhCTwCAAAZg8oz/IPWYwk6AgAAPIPWMhmQQ8owPIPWYQk4AgAAGYPKObyD1mkJNgIAADyD1jg8g9Y4WYPKMLyD1mEJEgFAADyD1jDZkEPKMvyD1mMJNAIAABmDyjd8g9ZnCTICAAA8g9Y2fJED1mkJMAIAADyRA9Y4/JED1jk8g9ZhCT4CAAASIuEJMAJAADyQg8RBOjyD1mUJLgIAADyQQ9Y1EiLhCS4CQAA8kIPERToi0MIiUMM/0Ms8g8QQzjyDxFDQPIPEENI8g8RQ1DyDxBDaPIPEUNwDxBDWA8RQ2DyDxCDeCAAAPIPEYOAIAAA8g8Qg6ggAADyDxGDsCAAAIuDuCAAAImDvCAAAPIPEIPAIAAA8g8Rg8ggAAAPEIPQIAAADxGD2CAAAPIPEIMAIQAA8g8RgwghAADyDxCDECEAAPIPEYMYIQAA8g8QgyAhAADyDxGDKCEAAPIPEIMwIQAA8g8RgzghAACLg0AhAACJg0QhAADyDxCDSCEAAPIPEYNQIQAA8g8Qg1hBAADyDxGDYEEAAPIPEINoYQAA8g8Rg3BhAADyDxCDiGEAAPIPEYOQYQAADxCDeGEAAA8Rg4BhAACLg5hhAACJg5xhAADyDxCDoIEAAPIPEYOogQAAi4OwgQAAiYO0gQAA8g8Qg7iBAADyDxGDwIEAAPIPEIPIgQAA8g8Rg9CBAADyDxCD6IEAAPIPEYPwgQAADxCD2IEAAA8Rg+CBAADyDxCD+KEAAPIPEYMAogAADxCDEKIAAA8RgxiiAADyDxCDMKIAAPIPEYM4ogAA8g8Qg0CiAADyDxGDSKIAAPIPEINQogAA8g8Rg1iiAADyDxCDYKIAAPIPEYNoogAAi4NwogAAiYN0ogAA8g8Qg3iiAADyDxGDgKIAAPIPEIOIwgAA8g8Rg5DCAADyDxCDmOIAAPIPEYOg4gAA8g8Qg7jiAADyDxGDwOIAAA8Qg6jiAAAPEYOw4gAAi4PI4gAAiYPM4gAA8g8Qg9ACAQDyDxGD2AIBAIuD4AIBAImD5AIBAPIPEIPoAgEA8g8Rg/ACAQDyDxCD+AIBAPIPEYMAAwEA8g8QgxgDAQDyDxGDIAMBAA8QgwgDAQAPEYMQAwEA8g8QgygTAQDyDxGDMBMBAA8Qg0ATAQAPEYNIEwEA8g8Qg2ATAQDyDxGDaBMBAPIPEINwEwEA8g8Rg3gTAQDyDxCDgBMBAPIPEYOIEwEA8g8Qg5ATAQDyDxGDmBMBAIuDoBMBAImDpBMBAPIPEIOoEwEA8g8Rg7ATAQDyDxCDuCMBAPIPEYPAIwEA8g8Qg8gzAQDyDxGD0DMBAPIPEIPoMwEA8g8Rg/AzAQAPEIPYMwEADxGD4DMBAIuD+DMBAImD/DMBAPIPEIMARAEA8g8RgwhEAQCLgxBEAQCJgxREAQDyDxCDGEQBAPIPEYMgRAEA8g8QgyhEAQDyDxGDMEQBAPIPEINIRAEA8g8Rg1BEAQAPEIM4RAEADxGDQEQBAPIPEINYZAEA8g8Rg2BkAQAPEINwZAEADxGDeGQBAPIPEIOQZAEA8g8Rg5hkAQDyDxCDoGQBAPIPEYOoZAEA8g8Qg7BkAQDyDxGDuGQBAPIPEIPAZAEA8g8Rg8hkAQCLg9BkAQCJg9RkAQDyDxCD2GQBAPIPEYPgZAEA8g8Qg+iEAQDyDxGD8IQBAPIPEIP4pAEA8g8RgwClAQDyDxCDGKUBAPIPEYMgpQEADxCDCKUBAA8RgxClAQCLgyilAQCJgyylAQDyDxCDMMUBAPIPEYM4xQEAi4NAxQEAiYNExQEA8g8Qg0jFAQDyDxGDUMUBAPIPEINYxQEA8g8Rg2DFAQDyDxCDeMUBAPIPEYOAxQEADxCDaMUBAA8Rg3DFAQDyDxCDiNUBAPIPEYOQ1QEADxCDoNUBAA8Rg6jVAQDyDxCDwNUBAPIPEYPI1QEA8g8Qg9DVAQDyDxGD2NUBAPIPEIPg1QEA8g8Rg+jVAQDyDxCD8NUBAPIPEYP41QEAi4MA1gEAiYME1gEA8g8QgwjWAQDyDxGDENYBAPIPEIMY5gEA8g8RgyDmAQDyDxCDKPYBAPIPEYMw9gEA8g8Qg0j2AQDyDxGDUPYBAA8Qgzj2AQAPEYNA9gEAi4NY9gEAiYNc9gEA8g8Qg2AGAgDyDxGDaAYCAIuDcAYCAImDdAYCAPIPEIN4BgIA8g8Rg4AGAgDyDxCDiAYCAPIPEYOQBgIA8g8Qg6gGAgDyDxGDsAYCAA8Qg5gGAgAPEYOgBgIA8g8Qg7gWAgDyDxGDwBYCAA8Qg9AWAgAPEYPYFgIA8g8Qg/AWAgDyDxGD+BYCAPIPEIMAFwIA8g8RgwgXAgDyDxCDEBcCAPIPEYMYFwIA8g8QgyAXAgDyDxGDKBcCAIuDMBcCAImDNBcCAPIPEIM4FwIA8g8Rg0AXAgDyDxCDSCcCAPIPEYNQJwIA8g8Qg1g3AgDyDxGDYDcCAPIPEIN4NwIA8g8Rg4A3AgAPEINoNwIADxGDcDcCAIuDiDcCAImDjDcCAPIPEIOQRwIA8g8Rg5hHAgCLg6BHAgCJg6RHAgDyDxCDqEcCAPIPEYOwRwIA8g8Qg7hHAgDyDxGDwEcCAPIPEIPYRwIA8g8Rg+BHAgAPEIPIRwIADxGD0EcCAPIPEIPoZwIA8g8Rg/BnAgAPEIMAaAIADxGDCGgCAPIPEIMgaAIA8g8RgyhoAgDyDxCDMGgCAPIPEYM4aAIA8g8Qg0BoAgDyDxGDSGgCAPIPEINQaAIA8g8Rg1hoAgCLg2BoAgCJg2RoAgDyDxCDaGgCAPIPEYNwaAIA8g8Qg3iIAgDyDxGDgIgCAPIPEIOIqAIA8g8Rg5CoAgDyDxCDqKgCAPIPEYOwqAIADxCDmKgCAA8Rg6CoAgCLg7ioAgCJg7yoAgDyDxCDwMgCAPIPEYPIyAIAi4PQyAIAiYPUyAIA8g8Qg9jIAgDyDxGD4MgCAPIPEIPoyAIA8g8Rg/DIAgDyDxCDCMkCAPIPEYMQyQIADxCD+MgCAA8RgwDJAgDyDxCDGNkCAPIPEYMg2QIADxCDMNkCAA8RgzjZAgDyDxCDUNkCAPIPEYNY2QIA8g8Qg2DZAgDyDxGDaNkCAPIPEINw2QIA8g8Rg3jZAgDyDxCDgNkCAPIPEYOI2QIAi4OQ2QIAiYOU2QIA8g8Qg5jZAgDyDxGDoNkCAPIPEIOo6QIA8g8Rg7DpAgDyDxCDuPkCAPIPEYPA+QIA8g8Qg9j5AgDyDxGD4PkCAA8Qg8j5AgAPEYPQ+QIAi4Po+QIAiYPs+QIA8g8Qg/AJAwDyDxGD+AkDAEn/xUw5rCSQBQAAD4XMw///SIHEyAkAAFtBXEFdQV5BX13DAAAAAAAAAADwPwAAAAAAcAdBGBgYGBgYWD8SEhISEhKCP/yp8dJNYmA/6FDUDfOWrkApXI/C9SjwP+hQ1A3zlr5AUrgehetR8D9qQ4aLyn25QHsUrkfhevA/MS8RyA3YwUCkcD0K16PwP648X0o28cZAzczMzMzM8D+V8koM2DC7QPYoXI/C9fA/8DU4CaJkxEAfhetRuB7xPwAAAAAAACBAAAAAAAAA8D9VVVVVVVWVv1VVVVVVVfU/ke18PzVe8D+amZmZmZm5v8ed0sH6//e/AAAAAAAA8L8AAAAAAAAAwAAAAAAAAAjAAAAAAAAAEMBVVVVVVVWlP1VVVVVVVcW/AAAAAAAA0D/MO39mnqD2P8w7f2aeoPa/AAAAAAAAAECycXFwbFy8v2zn+6nx0vU/RhVVVFFFtb9s5/up8dLlP0YVVVRRRcW/AAAAAAAACEAcfGEyVTDqPw/nRsbDucG/AAAAAAAAEEA4ffwjz7TiP3xDY+Le0Mi/AAAAAAAAFEDlifo3QhndP+mff/7558+/AAAAAAAAGEBaZDvfT43oP8xL9kpI6MK/AAAAAAAAHECR7Xw/NV7gP7JxcXBsXMy/kiRJkiRJwj9mZmZmZmbmPzMzMzMzM8M/C4Lz0FXu779mZmZmZmbuP5qZmZmZmak/AAAgAAAAAD4AAAAAAADgPwAAAAAAAAAA8DU4CaJktEDNzMzMzAxGQHsibmFtZSI6ICJmYXVzdGdlbi0xIiwiZmlsZW5hbWUiOiAiZmF1c3RnZW4tMSIsInZlcnNpb24iOiAiMi40MC4wIiwiY29tcGlsZV9vcHRpb25zIjogIi1sYW5nIGxsdm0gMTIuMC4xIC1lcyAxIC1tY2QgMTYgLWRvdWJsZSAtZnR6IDAiLCJsaWJyYXJ5X2xpc3QiOiBbIi9Vc2Vycy9kYXZpZGZpZXJyby9Eb2N1bWVudHMvTWF4IDgvUGFja2FnZXMvZmF1c3RnZW4vZXh0ZXJuYWxzL21zcC9mYXVzdGdlbn4ubXhvL0NvbnRlbnRzL1Jlc291cmNlcy9zdGRmYXVzdC5saWIiLCIvVXNlcnMvZGF2aWRmaWVycm8vRG9jdW1lbnRzL01heCA4L1BhY2thZ2VzL2ZhdXN0Z2VuL2V4dGVybmFscy9tc3AvZmF1c3RnZW5+Lm14by9Db250ZW50cy9SZXNvdXJjZXMvbm9pc2VzLmxpYiIsIi9Vc2Vycy9kYXZpZGZpZXJyby9Eb2N1bWVudHMvTWF4IDgvUGFja2FnZXMvZmF1c3RnZW4vZXh0ZXJuYWxzL21zcC9mYXVzdGdlbn4ubXhvL0NvbnRlbnRzL1Jlc291cmNlcy9vc2NpbGxhdG9ycy5saWIiLCIvVXNlcnMvZGF2aWRmaWVycm8vRG9jdW1lbnRzL01heCA4L1BhY2thZ2VzL2ZhdXN0Z2VuL2V4dGVybmFscy9tc3AvZmF1c3RnZW5+Lm14by9Db250ZW50cy9SZXNvdXJjZXMvbWF0aHMubGliIiwiL1VzZXJzL2RhdmlkZmllcnJvL0RvY3VtZW50cy9NYXggOC9QYWNrYWdlcy9mYXVzdGdlbi9leHRlcm5hbHMvbXNwL2ZhdXN0Z2Vufi5teG8vQ29udGVudHMvUmVzb3VyY2VzL3BsYXRmb3JtLmxpYiIsIi9Vc2Vycy9kYXZpZGZpZXJyby9Eb2N1bWVudHMvTWF4IDgvUGFja2FnZXMvZmF1c3RnZW4vZXh0ZXJuYWxzL21zcC9mYXVzdGdlbn4ubXhvL0NvbnRlbnRzL1Jlc291cmNlcy9iYXNpY3MubGliIiwiL1VzZXJzL2RhdmlkZmllcnJvL0RvY3VtZW50cy9NYXggOC9QYWNrYWdlcy9mYXVzdGdlbi9leHRlcm5hbHMvbXNwL2ZhdXN0Z2Vufi5teG8vQ29udGVudHMvUmVzb3VyY2VzL3BoeXNtb2RlbHMubGliIiwiL1VzZXJzL2RhdmlkZmllcnJvL0RvY3VtZW50cy9NYXggOC9QYWNrYWdlcy9mYXVzdGdlbi9leHRlcm5hbHMvbXNwL2ZhdXN0Z2Vufi5teG8vQ29udGVudHMvUmVzb3VyY2VzL3NpZ25hbHMubGliIiwiL1VzZXJzL2RhdmlkZmllcnJvL0RvY3VtZW50cy9NYXggOC9QYWNrYWdlcy9mYXVzdGdlbi9leHRlcm5hbHMvbXNwL2ZhdXN0Z2Vufi5teG8vQ29udGVudHMvUmVzb3VyY2VzL3NwYXRzLmxpYiIsIi9Vc2Vycy9kYXZpZGZpZXJyby9Eb2N1bWVudHMvTWF4IDgvUGFja2FnZXMvZmF1c3RnZW4vZXh0ZXJuYWxzL21zcC9mYXVzdGdlbn4ubXhvL0NvbnRlbnRzL1Jlc291cmNlcy9maWx0ZXJzLmxpYiIsIi9Vc2Vycy9kYXZpZGZpZXJyby9Eb2N1bWVudHMvTWF4IDgvUGFja2FnZXMvZmF1c3RnZW4vZXh0ZXJuYWxzL21zcC9mYXVzdGdlbn4ubXhvL0NvbnRlbnRzL1Jlc291cmNlcy9lbnZlbG9wZXMubGliIiwiL1VzZXJzL2RhdmlkZmllcnJvL0RvY3VtZW50cy9NYXggOC9QYWNrYWdlcy9mYXVzdGdlbi9leHRlcm5hbHMvbXNwL2ZhdXN0Z2Vufi5teG8vQ29udGVudHMvUmVzb3VyY2VzL3JvdXRlcy5saWIiLCIvVXNlcnMvZGF2aWRmaWVycm8vRG9jdW1lbnRzL01heCA4L1BhY2thZ2VzL2ZhdXN0Z2VuL2V4dGVybmFscy9tc3AvZmF1c3RnZW5+Lm14by9Db250ZW50cy9SZXNvdXJjZXMvZGVsYXlzLmxpYiJdLCJpbmNsdWRlX3BhdGhuYW1lcyI6IFsiL1VzZXJzL2RhdmlkZmllcnJvL0RvY3VtZW50cy9NYXggOC9QYWNrYWdlcy9mYXVzdGdlbi9leHRlcm5hbHMvbXNwL2ZhdXN0Z2Vufi5teG8vQ29udGVudHMvUmVzb3VyY2VzIiwiL3NoYXJlL2ZhdXN0IiwiL3Vzci9sb2NhbC9zaGFyZS9mYXVzdCIsIi91c3Ivc2hhcmUvZmF1c3QiLCIuIl0sInNpemUiOiAxOTkxNjgsImlucHV0cyI6IDAsIm91dHB1dHMiOiAyLCJzcl9pbmRleCI6IDE2LCJtZXRhIjogWyB7ICJiYXNpY3MubGliL25hbWUiOiAiRmF1c3QgQmFzaWMgRWxlbWVudCBMaWJyYXJ5IiB9LHsgImJhc2ljcy5saWIvdmVyc2lvbiI6ICIwLjUiIH0seyAiY29tcGlsZV9vcHRpb25zIjogIi1sYW5nIGxsdm0gMTIuMC4xIC1lcyAxIC1tY2QgMTYgLWRvdWJsZSAtZnR6IDAiIH0seyAiZGVsYXlzLmxpYi9uYW1lIjogIkZhdXN0IERlbGF5IExpYnJhcnkiIH0seyAiZGVsYXlzLmxpYi92ZXJzaW9uIjogIjAuMSIgfSx7ICJlbnZlbG9wZXMubGliL2FyOmF1dGhvciI6ICJZYW5uIE9ybGFyZXksIFN0w6lwaGFuZSBMZXR6IiB9LHsgImVudmVsb3Blcy5saWIvYXV0aG9yIjogIkdSQU1FIiB9LHsgImVudmVsb3Blcy5saWIvY29weXJpZ2h0IjogIkdSQU1FIiB9LHsgImVudmVsb3Blcy5saWIvbGljZW5zZSI6ICJMR1BMIHdpdGggZXhjZXB0aW9uIiB9LHsgImVudmVsb3Blcy5saWIvbmFtZSI6ICJGYXVzdCBFbnZlbG9wZSBMaWJyYXJ5IiB9LHsgImVudmVsb3Blcy5saWIvdmVyc2lvbiI6ICIwLjEiIH0seyAiZmlsZW5hbWUiOiAiZmF1c3RnZW4tMSIgfSx7ICJmaWx0ZXJzLmxpYi9maXI6YXV0aG9yIjogIkp1bGl1cyBPLiBTbWl0aCBJSUkiIH0seyAiZmlsdGVycy5saWIvZmlyOmNvcHlyaWdodCI6ICJDb3B5cmlnaHQgKEMpIDIwMDMtMjAxOSBieSBKdWxpdXMgTy4gU21pdGggSUlJIDxqb3NfY2NybWEuc3RhbmZvcmQuZWR1PiIgfSx7ICJmaWx0ZXJzLmxpYi9maXI6bGljZW5zZSI6ICJNSVQtc3R5bGUgU1RLLTQuMyBsaWNlbnNlIiB9LHsgImZpbHRlcnMubGliL2lpcjphdXRob3IiOiAiSnVsaXVzIE8uIFNtaXRoIElJSSIgfSx7ICJmaWx0ZXJzLmxpYi9paXI6Y29weXJpZ2h0IjogIkNvcHlyaWdodCAoQykgMjAwMy0yMDE5IGJ5IEp1bGl1cyBPLiBTbWl0aCBJSUkgPGpvc19jY3JtYS5zdGFuZm9yZC5lZHU+IiB9LHsgImZpbHRlcnMubGliL2lpcjpsaWNlbnNlIjogIk1JVC1zdHlsZSBTVEstNC4zIGxpY2Vuc2UiIH0seyAiZmlsdGVycy5saWIvbG93cGFzczBfaGlnaHBhc3MxIjogIk1JVC1zdHlsZSBTVEstNC4zIGxpY2Vuc2UiIH0seyAiZmlsdGVycy5saWIvbG93cGFzczBfaGlnaHBhc3MxOmF1dGhvciI6ICJKdWxpdXMgTy4gU21pdGggSUlJIiB9LHsgImZpbHRlcnMubGliL2xvd3Bhc3M6YXV0aG9yIjogIkp1bGl1cyBPLiBTbWl0aCBJSUkiIH0seyAiZmlsdGVycy5saWIvbG93cGFzczpjb3B5cmlnaHQiOiAiQ29weXJpZ2h0IChDKSAyMDAzLTIwMTkgYnkgSnVsaXVzIE8uIFNtaXRoIElJSSA8am9zX2Njcm1hLnN0YW5mb3JkLmVkdT4iIH0seyAiZmlsdGVycy5saWIvbG93cGFzczpsaWNlbnNlIjogIk1JVC1zdHlsZSBTVEstNC4zIGxpY2Vuc2UiIH0seyAiZmlsdGVycy5saWIvbmFtZSI6ICJGYXVzdCBGaWx0ZXJzIExpYnJhcnkiIH0seyAiZmlsdGVycy5saWIvdGYyOmF1dGhvciI6ICJKdWxpdXMgTy4gU21pdGggSUlJIiB9LHsgImZpbHRlcnMubGliL3RmMjpjb3B5cmlnaHQiOiAiQ29weXJpZ2h0IChDKSAyMDAzLTIwMTkgYnkgSnVsaXVzIE8uIFNtaXRoIElJSSA8am9zX2Njcm1hLnN0YW5mb3JkLmVkdT4iIH0seyAiZmlsdGVycy5saWIvdGYyOmxpY2Vuc2UiOiAiTUlULXN0eWxlIFNUSy00LjMgbGljZW5zZSIgfSx7ICJmaWx0ZXJzLmxpYi90ZjJzOmF1dGhvciI6ICJKdWxpdXMgTy4gU21pdGggSUlJIiB9LHsgImZpbHRlcnMubGliL3RmMnM6Y29weXJpZ2h0IjogIkNvcHlyaWdodCAoQykgMjAwMy0yMDE5IGJ5IEp1bGl1cyBPLiBTbWl0aCBJSUkgPGpvc19jY3JtYS5zdGFuZm9yZC5lZHU+IiB9LHsgImZpbHRlcnMubGliL3RmMnM6bGljZW5zZSI6ICJNSVQtc3R5bGUgU1RLLTQuMyBsaWNlbnNlIiB9LHsgImZpbHRlcnMubGliL3ZlcnNpb24iOiAiMC4zIiB9LHsgIm1hdGhzLmxpYi9hdXRob3IiOiAiR1JBTUUiIH0seyAibWF0aHMubGliL2NvcHlyaWdodCI6ICJHUkFNRSIgfSx7ICJtYXRocy5saWIvbGljZW5zZSI6ICJMR1BMIHdpdGggZXhjZXB0aW9uIiB9LHsgIm1hdGhzLmxpYi9uYW1lIjogIkZhdXN0IE1hdGggTGlicmFyeSIgfSx7ICJtYXRocy5saWIvdmVyc2lvbiI6ICIyLjUiIH0seyAibmFtZSI6ICJmYXVzdGdlbi0xIiB9LHsgIm5vaXNlcy5saWIvbmFtZSI6ICJGYXVzdCBOb2lzZSBHZW5lcmF0b3IgTGlicmFyeSIgfSx7ICJub2lzZXMubGliL3ZlcnNpb24iOiAiMC4zIiB9LHsgIm9zY2lsbGF0b3JzLmxpYi9uYW1lIjogIkZhdXN0IE9zY2lsbGF0b3IgTGlicmFyeSIgfSx7ICJvc2NpbGxhdG9ycy5saWIvdmVyc2lvbiI6ICIwLjMiIH0seyAicGh5c21vZGVscy5saWIvbmFtZSI6ICJGYXVzdCBQaHlzaWNhbCBNb2RlbHMgTGlicmFyeSIgfSx7ICJwaHlzbW9kZWxzLmxpYi92ZXJzaW9uIjogIjAuMSIgfSx7ICJwbGF0Zm9ybS5saWIvbmFtZSI6ICJHZW5lcmljIFBsYXRmb3JtIExpYnJhcnkiIH0seyAicGxhdGZvcm0ubGliL3ZlcnNpb24iOiAiMC4yIiB9LHsgInJvdXRlcy5saWIvbmFtZSI6ICJGYXVzdCBTaWduYWwgUm91dGluZyBMaWJyYXJ5IiB9LHsgInJvdXRlcy5saWIvdmVyc2lvbiI6ICIwLjIiIH0seyAic2lnbmFscy5saWIvbmFtZSI6ICJGYXVzdCBTaWduYWwgUm91dGluZyBMaWJyYXJ5IiB9LHsgInNpZ25hbHMubGliL3ZlcnNpb24iOiAiMC4xIiB9LHsgInNwYXRzLmxpYi9uYW1lIjogIkZhdXN0IFNwYXRpYWxpemF0aW9uIExpYnJhcnkiIH0seyAic3BhdHMubGliL3ZlcnNpb24iOiAiMC4wIiB9XSwidWkiOiBbIHsidHlwZSI6ICJ2Z3JvdXAiLCJsYWJlbCI6ICJmYXVzdGdlbi0xIiwiaXRlbXMiOiBbIHsidHlwZSI6ICJuZW50cnkiLCJsYWJlbCI6ICJyZWZlcmVuY2UiLCJhZGRyZXNzIjogIi9mYXVzdGdlbi0xL3JlZmVyZW5jZSIsImluZGV4IjogMzYsIm1ldGEiOiBbeyAiMCI6ICIiIH1dLCJpbml0IjogNjQsIm1pbiI6IDAsIm1heCI6IDEyNywic3RlcCI6IDF9LHsidHlwZSI6ICJoc2xpZGVyIiwibGFiZWwiOiAiZmFjdG9yIiwiYWRkcmVzcyI6ICIvZmF1c3RnZW4tMS9mYWN0b3IiLCJpbmRleCI6IDg0NDAsIm1ldGEiOiBbeyAiMSI6ICIiIH1dLCJpbml0IjogMSwibWluIjogMC4wMSwibWF4IjogNTAsInN0ZXAiOiAwLjAxfSx7InR5cGUiOiAiaHNsaWRlciIsImxhYmVsIjogInBsdWNrUG9zaXRpb24iLCJhZGRyZXNzIjogIi9mYXVzdGdlbi0xL3BsdWNrUG9zaXRpb24iLCJpbmRleCI6IDI4LCJtZXRhIjogW3sgIjIiOiAiIiB9XSwiaW5pdCI6IDAuNSwibWluIjogMCwibWF4IjogMSwic3RlcCI6IDAuMDF9LHsidHlwZSI6ICJoc2xpZGVyIiwibGFiZWwiOiAiZ2FpbiIsImFkZHJlc3MiOiAiL2ZhdXN0Z2VuLTEvZ2FpbiIsImluZGV4IjogODM0NCwibWV0YSI6IFt7ICIzIjogIiIgfV0sImluaXQiOiAwLjIsIm1pbiI6IDAsIm1heCI6IDEsInN0ZXAiOiAwLjAxfSx7InR5cGUiOiAibmVudHJ5IiwibGFiZWwiOiAic3RlcmVvU2hpZnQiLCJhZGRyZXNzIjogIi9mYXVzdGdlbi0xL3N0ZXJlb1NoaWZ0IiwiaW5kZXgiOiAwLCJtZXRhIjogW3sgIjQiOiAiIiB9XSwiaW5pdCI6IDAsIm1pbiI6IDAsIm1heCI6IDcsInN0ZXAiOiAxfV19XX0AAG83AAACAAAGZTcAAAIAAAZbNwAAAgAABlE3AAACAAAGRzcAAAIAAAY9NwAAAgAABjM3AAACAAAGPiwAAAIAAAYpLAAAAgAABhQsAAACAAAGASwAAAIAAAbuKwAAAgAABtsrAAACAAAGyCsAAAIAAAavKwAAAgAABsYqAAACAAAGmxMAAAIAAAZlEwAAAgAABh8RAAACAAAG+hAAAAIAAAbeEAAAAgAABqoQAAACAAAGixAAAAIAAAZsEAAAAgAABkQQAAACAAAGvg4AAAIAAAaiDgAAAgAABmkNAAACAAAGTQ0AAAIAAAYUDAAAAgAABvgLAAACAAAGvwoAAAIAAAajCgAAAgAABnAJAAACAAAGVAkAAAIAAAYkCAAAAgAABtoGAAACAAAGjAYAAAIAAAZPBgAACwAADigFAAACAAAG9wQAAAIAAAbbBAAAAgAABs8EAAAJAAAOwQQAAAIAAAapBAAAAgAABokEAAACAAAGVQQAAAIAAAZLBAAACgAADgIEAAAAAAAOyQMAAAIAAAavAwAAAgAABpUDAAACAAAGewMAAAIAAAZhAwAAAgAABkcDAAACAAAGLQMAAAIAAAYTAwAAAgAABvkCAAACAAAG3wIAAAIAAAbFAgAAAgAABqsCAAACAAAGkQIAAAIAAAZ3AgAAAgAABlkCAAACAAAGMwIAAAMAAAYQAgAAAgAABvkBAAACAAAG6wEAAAIAAAbZAQAAAgAABiABAAAIAAAOAQAAAA4EAAAwdgAAAAAAAHEVAAAPAQAA4AMAAAAAAAAnFQAADwEAAAAAAAAAAAAAYxUAAA8BAAAQBAAAAAAAABkVAAAPAQAA8AMAAAAAAACAFQAADwEAAAAEAAAAAAAATxUAAA8BAAAQAAAAAAAAADcVAAAPAQAA0AEAAAAAAACOFQAAAQAAAAAAAAAAAAAAohUAAAEAAAAAAAAAAAAAAJwVAAABAAAAAAAAAAAAAACXFQAAAQAAAAAAAAAAAAAAAF97Im5hbWUiOiAiZmF1c3RnZW4tMSIsImZpbGVuYW1lIjogImZhdXN0Z2VuLTEiLCJ2ZXJzaW9uIjogIjIuNDAuMCIsImNvbXBpbGVfb3B0aW9ucyI6ICItbGFuZyBsbHZtIDEyLjAuMSAtZXMgMSAtbWNkIDE2IC1kb3VibGUgLWZ0eiAwIiwibGlicmFyeV9saXN0IjogWyIvVXNlcnMvZGF2aWRmaWVycm8vRG9jdW1lbnRzL01heCA4L1BhY2thZ2VzL2ZhdXN0Z2VuL2V4dGVybmFscy9tc3AvZmF1c3RnZW5+Lm14by9Db250ZW50cy9SZXNvdXJjZXMvc3RkZmF1c3QubGliIiwiL1VzZXJzL2RhdmlkZmllcnJvL0RvY3VtZW50cy9NYXggOC9QYWNrYWdlcy9mYXVzdGdlbi9leHRlcm5hbHMvbXNwL2ZhdXN0Z2Vufi5teG8vQ29udGVudHMvUmVzb3VyY2VzL25vaXNlcy5saWIiLCIvVXNlcnMvZGF2aWRmaWVycm8vRG9jdW1lbnRzL01heCA4L1BhY2thZ2VzL2ZhdXN0Z2VuL2V4dGVybmFscy9tc3AvZmF1c3RnZW5+Lm14by9Db250ZW50cy9SZXNvdXJjZXMvb3NjaWxsYXRvcnMubGliIiwiL1VzZXJzL2RhdmlkZmllcnJvL0RvY3VtZW50cy9NYXggOC9QYWNrYWdlcy9mYXVzdGdlbi9leHRlcm5hbHMvbXNwL2ZhdXN0Z2Vufi5teG8vQ29udGVudHMvUmVzb3VyY2VzL21hdGhzLmxpYiIsIi9Vc2Vycy9kYXZpZGZpZXJyby9Eb2N1bWVudHMvTWF4IDgvUGFja2FnZXMvZmF1c3RnZW4vZXh0ZXJuYWxzL21zcC9mYXVzdGdlbn4ubXhvL0NvbnRlbnRzL1Jlc291cmNlcy9wbGF0Zm9ybS5saWIiLCIvVXNlcnMvZGF2aWRmaWVycm8vRG9jdW1lbnRzL01heCA4L1BhY2thZ2VzL2ZhdXN0Z2VuL2V4dGVybmFscy9tc3AvZmF1c3RnZW5+Lm14by9Db250ZW50cy9SZXNvdXJjZXMvYmFzaWNzLmxpYiIsIi9Vc2Vycy9kYXZpZGZpZXJyby9Eb2N1bWVudHMvTWF4IDgvUGFja2FnZXMvZmF1c3RnZW4vZXh0ZXJuYWxzL21zcC9mYXVzdGdlbn4ubXhvL0NvbnRlbnRzL1Jlc291cmNlcy9waHlzbW9kZWxzLmxpYiIsIi9Vc2Vycy9kYXZpZGZpZXJyby9Eb2N1bWVudHMvTWF4IDgvUGFja2FnZXMvZmF1c3RnZW4vZXh0ZXJuYWxzL21zcC9mYXVzdGdlbn4ubXhvL0NvbnRlbnRzL1Jlc291cmNlcy9zaWduYWxzLmxpYiIsIi9Vc2Vycy9kYXZpZGZpZXJyby9Eb2N1bWVudHMvTWF4IDgvUGFja2FnZXMvZmF1c3RnZW4vZXh0ZXJuYWxzL21zcC9mYXVzdGdlbn4ubXhvL0NvbnRlbnRzL1Jlc291cmNlcy9zcGF0cy5saWIiLCIvVXNlcnMvZGF2aWRmaWVycm8vRG9jdW1lbnRzL01heCA4L1BhY2thZ2VzL2ZhdXN0Z2VuL2V4dGVybmFscy9tc3AvZmF1c3RnZW5+Lm14by9Db250ZW50cy9SZXNvdXJjZXMvZmlsdGVycy5saWIiLCIvVXNlcnMvZGF2aWRmaWVycm8vRG9jdW1lbnRzL01heCA4L1BhY2thZ2VzL2ZhdXN0Z2VuL2V4dGVybmFscy9tc3AvZmF1c3RnZW5+Lm14by9Db250ZW50cy9SZXNvdXJjZXMvZW52ZWxvcGVzLmxpYiIsIi9Vc2Vycy9kYXZpZGZpZXJyby9Eb2N1bWVudHMvTWF4IDgvUGFja2FnZXMvZmF1c3RnZW4vZXh0ZXJuYWxzL21zcC9mYXVzdGdlbn4ubXhvL0NvbnRlbnRzL1Jlc291cmNlcy9yb3V0ZXMubGliIiwiL1VzZXJzL2RhdmlkZmllcnJvL0RvY3VtZW50cy9NYXggOC9QYWNrYWdlcy9mYXVzdGdlbi9leHRlcm5hbHMvbXNwL2ZhdXN0Z2Vufi5teG8vQ29udGVudHMvUmVzb3VyY2VzL2RlbGF5cy5saWIiXSwiaW5jbHVkZV9wYXRobmFtZXMiOiBbIi9Vc2Vycy9kYXZpZGZpZXJyby9Eb2N1bWVudHMvTWF4IDgvUGFja2FnZXMvZmF1c3RnZW4vZXh0ZXJuYWxzL21zcC9mYXVzdGdlbn4ubXhvL0NvbnRlbnRzL1Jlc291cmNlcyIsIi9zaGFyZS9mYXVzdCIsIi91c3IvbG9jYWwvc2hhcmUvZmF1c3QiLCIvdXNyL3NoYXJlL2ZhdXN0IiwiLiJdLCJzaXplIjogMTk5MTY4LCJpbnB1dHMiOiAwLCJvdXRwdXRzIjogMiwic3JfaW5kZXgiOiAxNiwibWV0YSI6IFsgeyAiYmFzaWNzLmxpYi9uYW1lIjogIkZhdXN0IEJhc2ljIEVsZW1lbnQgTGlicmFyeSIgfSx7ICJiYXNpY3MubGliL3ZlcnNpb24iOiAiMC41IiB9LHsgImNvbXBpbGVfb3B0aW9ucyI6ICItbGFuZyBsbHZtIDEyLjAuMSAtZXMgMSAtbWNkIDE2IC1kb3VibGUgLWZ0eiAwIiB9LHsgImRlbGF5cy5saWIvbmFtZSI6ICJGYXVzdCBEZWxheSBMaWJyYXJ5IiB9LHsgImRlbGF5cy5saWIvdmVyc2lvbiI6ICIwLjEiIH0seyAiZW52ZWxvcGVzLmxpYi9hcjphdXRob3IiOiAiWWFubiBPcmxhcmV5LCBTdMOpcGhhbmUgTGV0eiIgfSx7ICJlbnZlbG9wZXMubGliL2F1dGhvciI6ICJHUkFNRSIgfSx7ICJlbnZlbG9wZXMubGliL2NvcHlyaWdodCI6ICJHUkFNRSIgfSx7ICJlbnZlbG9wZXMubGliL2xpY2Vuc2UiOiAiTEdQTCB3aXRoIGV4Y2VwdGlvbiIgfSx7ICJlbnZlbG9wZXMubGliL25hbWUiOiAiRmF1c3QgRW52ZWxvcGUgTGlicmFyeSIgfSx7ICJlbnZlbG9wZXMubGliL3ZlcnNpb24iOiAiMC4xIiB9LHsgImZpbGVuYW1lIjogImZhdXN0Z2VuLTEiIH0seyAiZmlsdGVycy5saWIvZmlyOmF1dGhvciI6ICJKdWxpdXMgTy4gU21pdGggSUlJIiB9LHsgImZpbHRlcnMubGliL2Zpcjpjb3B5cmlnaHQiOiAiQ29weXJpZ2h0IChDKSAyMDAzLTIwMTkgYnkgSnVsaXVzIE8uIFNtaXRoIElJSSA8am9zX2Njcm1hLnN0YW5mb3JkLmVkdT4iIH0seyAiZmlsdGVycy5saWIvZmlyOmxpY2Vuc2UiOiAiTUlULXN0eWxlIFNUSy00LjMgbGljZW5zZSIgfSx7ICJmaWx0ZXJzLmxpYi9paXI6YXV0aG9yIjogIkp1bGl1cyBPLiBTbWl0aCBJSUkiIH0seyAiZmlsdGVycy5saWIvaWlyOmNvcHlyaWdodCI6ICJDb3B5cmlnaHQgKEMpIDIwMDMtMjAxOSBieSBKdWxpdXMgTy4gU21pdGggSUlJIDxqb3NfY2NybWEuc3RhbmZvcmQuZWR1PiIgfSx7ICJmaWx0ZXJzLmxpYi9paXI6bGljZW5zZSI6ICJNSVQtc3R5bGUgU1RLLTQuMyBsaWNlbnNlIiB9LHsgImZpbHRlcnMubGliL2xvd3Bhc3MwX2hpZ2hwYXNzMSI6ICJNSVQtc3R5bGUgU1RLLTQuMyBsaWNlbnNlIiB9LHsgImZpbHRlcnMubGliL2xvd3Bhc3MwX2hpZ2hwYXNzMTphdXRob3IiOiAiSnVsaXVzIE8uIFNtaXRoIElJSSIgfSx7ICJmaWx0ZXJzLmxpYi9sb3dwYXNzOmF1dGhvciI6ICJKdWxpdXMgTy4gU21pdGggSUlJIiB9LHsgImZpbHRlcnMubGliL2xvd3Bhc3M6Y29weXJpZ2h0IjogIkNvcHlyaWdodCAoQykgMjAwMy0yMDE5IGJ5IEp1bGl1cyBPLiBTbWl0aCBJSUkgPGpvc19jY3JtYS5zdGFuZm9yZC5lZHU+IiB9LHsgImZpbHRlcnMubGliL2xvd3Bhc3M6bGljZW5zZSI6ICJNSVQtc3R5bGUgU1RLLTQuMyBsaWNlbnNlIiB9LHsgImZpbHRlcnMubGliL25hbWUiOiAiRmF1c3QgRmlsdGVycyBMaWJyYXJ5IiB9LHsgImZpbHRlcnMubGliL3RmMjphdXRob3IiOiAiSnVsaXVzIE8uIFNtaXRoIElJSSIgfSx7ICJmaWx0ZXJzLmxpYi90ZjI6Y29weXJpZ2h0IjogIkNvcHlyaWdodCAoQykgMjAwMy0yMDE5IGJ5IEp1bGl1cyBPLiBTbWl0aCBJSUkgPGpvc19jY3JtYS5zdGFuZm9yZC5lZHU+IiB9LHsgImZpbHRlcnMubGliL3RmMjpsaWNlbnNlIjogIk1JVC1zdHlsZSBTVEstNC4zIGxpY2Vuc2UiIH0seyAiZmlsdGVycy5saWIvdGYyczphdXRob3IiOiAiSnVsaXVzIE8uIFNtaXRoIElJSSIgfSx7ICJmaWx0ZXJzLmxpYi90ZjJzOmNvcHlyaWdodCI6ICJDb3B5cmlnaHQgKEMpIDIwMDMtMjAxOSBieSBKdWxpdXMgTy4gU21pdGggSUlJIDxqb3NfY2NybWEuc3RhbmZvcmQuZWR1PiIgfSx7ICJmaWx0ZXJzLmxpYi90ZjJzOmxpY2Vuc2UiOiAiTUlULXN0eWxlIFNUSy00LjMgbGljZW5zZSIgfSx7ICJmaWx0ZXJzLmxpYi92ZXJzaW9uIjogIjAuMyIgfSx7ICJtYXRocy5saWIvYXV0aG9yIjogIkdSQU1FIiB9LHsgIm1hdGhzLmxpYi9jb3B5cmlnaHQiOiAiR1JBTUUiIH0seyAibWF0aHMubGliL2xpY2Vuc2UiOiAiTEdQTCB3aXRoIGV4Y2VwdGlvbiIgfSx7ICJtYXRocy5saWIvbmFtZSI6ICJGYXVzdCBNYXRoIExpYnJhcnkiIH0seyAibWF0aHMubGliL3ZlcnNpb24iOiAiMi41IiB9LHsgIm5hbWUiOiAiZmF1c3RnZW4tMSIgfSx7ICJub2lzZXMubGliL25hbWUiOiAiRmF1c3QgTm9pc2UgR2VuZXJhdG9yIExpYnJhcnkiIH0seyAibm9pc2VzLmxpYi92ZXJzaW9uIjogIjAuMyIgfSx7ICJvc2NpbGxhdG9ycy5saWIvbmFtZSI6ICJGYXVzdCBPc2NpbGxhdG9yIExpYnJhcnkiIH0seyAib3NjaWxsYXRvcnMubGliL3ZlcnNpb24iOiAiMC4zIiB9LHsgInBoeXNtb2RlbHMubGliL25hbWUiOiAiRmF1c3QgUGh5c2ljYWwgTW9kZWxzIExpYnJhcnkiIH0seyAicGh5c21vZGVscy5saWIvdmVyc2lvbiI6ICIwLjEiIH0seyAicGxhdGZvcm0ubGliL25hbWUiOiAiR2VuZXJpYyBQbGF0Zm9ybSBMaWJyYXJ5IiB9LHsgInBsYXRmb3JtLmxpYi92ZXJzaW9uIjogIjAuMiIgfSx7ICJyb3V0ZXMubGliL25hbWUiOiAiRmF1c3QgU2lnbmFsIFJvdXRpbmcgTGlicmFyeSIgfSx7ICJyb3V0ZXMubGliL3ZlcnNpb24iOiAiMC4yIiB9LHsgInNpZ25hbHMubGliL25hbWUiOiAiRmF1c3QgU2lnbmFsIFJvdXRpbmcgTGlicmFyeSIgfSx7ICJzaWduYWxzLmxpYi92ZXJzaW9uIjogIjAuMSIgfSx7ICJzcGF0cy5saWIvbmFtZSI6ICJGYXVzdCBTcGF0aWFsaXphdGlvbiBMaWJyYXJ5IiB9LHsgInNwYXRzLmxpYi92ZXJzaW9uIjogIjAuMCIgfV0sInVpIjogWyB7InR5cGUiOiAidmdyb3VwIiwibGFiZWwiOiAiZmF1c3RnZW4tMSIsIml0ZW1zIjogWyB7InR5cGUiOiAibmVudHJ5IiwibGFiZWwiOiAicmVmZXJlbmNlIiwiYWRkcmVzcyI6ICIvZmF1c3RnZW4tMS9yZWZlcmVuY2UiLCJpbmRleCI6IDM2LCJtZXRhIjogW3sgIjAiOiAiIiB9XSwiaW5pdCI6IDY0LCJtaW4iOiAwLCJtYXgiOiAxMjcsInN0ZXAiOiAxfSx7InR5cGUiOiAiaHNsaWRlciIsImxhYmVsIjogImZhY3RvciIsImFkZHJlc3MiOiAiL2ZhdXN0Z2VuLTEvZmFjdG9yIiwiaW5kZXgiOiA4NDQwLCJtZXRhIjogW3sgIjEiOiAiIiB9XSwiaW5pdCI6IDEsIm1pbiI6IDAuMDEsIm1heCI6IDUwLCJzdGVwIjogMC4wMX0seyJ0eXBlIjogImhzbGlkZXIiLCJsYWJlbCI6ICJwbHVja1Bvc2l0aW9uIiwiYWRkcmVzcyI6ICIvZmF1c3RnZW4tMS9wbHVja1Bvc2l0aW9uIiwiaW5kZXgiOiAyOCwibWV0YSI6IFt7ICIyIjogIiIgfV0sImluaXQiOiAwLjUsIm1pbiI6IDAsIm1heCI6IDEsInN0ZXAiOiAwLjAxfSx7InR5cGUiOiAiaHNsaWRlciIsImxhYmVsIjogImdhaW4iLCJhZGRyZXNzIjogIi9mYXVzdGdlbi0xL2dhaW4iLCJpbmRleCI6IDgzNDQsIm1ldGEiOiBbeyAiMyI6ICIiIH1dLCJpbml0IjogMC4yLCJtaW4iOiAwLCJtYXgiOiAxLCJzdGVwIjogMC4wMX0seyJ0eXBlIjogIm5lbnRyeSIsImxhYmVsIjogInN0ZXJlb1NoaWZ0IiwiYWRkcmVzcyI6ICIvZmF1c3RnZW4tMS9zdGVyZW9TaGlmdCIsImluZGV4IjogMCwibWV0YSI6IFt7ICI0IjogIiIgfV0sImluaXQiOiAwLCJtaW4iOiAwLCJtYXgiOiA3LCJzdGVwIjogMX1dfV19AF9kZXN0cm95bXlkc3AAX2NsYXNzSW5pdG15ZHNwAF9pbnN0YW5jZUNvbnN0YW50c215ZHNwAF9pbnN0YW5jZUNsZWFybXlkc3AAX2NvbXB1dGVteWRzcABfYWxsb2NhdGVteWRzcABfZ2V0SlNPTm15ZHNwAF9fX2J6ZXJvAF90YW4AX2Ztb2QAX2V4cDIA",
					"machinecode_size" : 56672,
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 4,
					"outlettype" : [ "signal", "signal", "", "" ],
					"patching_rect" : [ 1387.280934866304733, 270.698582404373155, 62.0, 22.0 ],
					"sample_format" : 1,
					"serial_number" : "YJ2C099NJM64 bits",
					"sourcecode" : "import(\"stdfaust.lib\");\nn = 8;\ncoeff = (1, 4/3, 2/3, 4/5, 4/7, 4/9, 3/4, 1/2);\n//string length multipliers are:\n//1 for the fundamental\n//4/3 for the fourth below\n//2/3 for the perfect fifth above\n//4/5 for the perfect third above\n//4/7 for the seventh above\n//4/9 for the ninth above\n//3/4 for the perfect fourth above\n//1/2 for the octave above\nlength = 1.023; //E note\nlengthChange = pow(2, (64 - nentry(\"[0]reference\", 64, 0, 127, 1)) / 48);\nfactor = hslider(\"[1]factor\", 1, 0.01, 50, 0.01);\npluckPosition = hslider(\"[2]pluckPosition\", 0.5, 0, 1, 0.01);\ngain = hslider(\"[3]gain\", 0.2, 0, 1, 0.01) : si.smoo;\r\nstereoShift = nentry(\"[4]stereoShift\", 0, 0, n-1, 1);\ntoStereo(n, p) = par(i, n, sp.panner(1 - fmod(i+p, n) / (n-1))) :> (_, _);\nprocess = par(i, n, (no.sparse_noise(factor*(1+i*0.01)) : pm.guitar(length*ba.take(i+1, coeff)*lengthChange, pluckPosition, gain))) : toStereo(n, stereoShift);",
					"sourcecode_size" : 901,
					"text" : "faustgen~",
					"varname" : "faustgen-140657777691872",
					"version" : "1.55"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-127",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1393.614268199637991, 1351.130077439067463, 79.0, 20.0 ],
					"text" : "record output"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-128",
					"maxclass" : "toggle",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1360.614268199637991, 1349.130077439067463, 24.0, 24.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-129",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1360.614268199637991, 1377.181216644283268, 67.0, 22.0 ],
					"text" : "open wave"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-42",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 1360.614268199637991, 1413.181216644283268, 88.0, 22.0 ],
					"text" : "mc.sfrecord~ 7"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-87",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1226.199999928474426, 239.698582404373155, 49.0, 22.0 ],
					"text" : "r speed"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-96",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "multichannelsignal" ],
					"patcher" : 					{
						"fileversion" : 1,
						"appversion" : 						{
							"major" : 8,
							"minor" : 5,
							"revision" : 3,
							"architecture" : "x64",
							"modernui" : 1
						}
,
						"classnamespace" : "box",
						"rect" : [ 700.0, 213.0, 640.0, 480.0 ],
						"bglocked" : 0,
						"openinpresentation" : 0,
						"default_fontsize" : 12.0,
						"default_fontface" : 0,
						"default_fontname" : "Arial",
						"gridonopen" : 1,
						"gridsize" : [ 15.0, 15.0 ],
						"gridsnaponopen" : 1,
						"objectsnaponopen" : 1,
						"statusbarvisible" : 2,
						"toolbarvisible" : 1,
						"lefttoolbarpinned" : 0,
						"toptoolbarpinned" : 0,
						"righttoolbarpinned" : 0,
						"bottomtoolbarpinned" : 0,
						"toolbars_unpinned_last_save" : 0,
						"tallnewobj" : 0,
						"boxanimatetime" : 200,
						"enablehscroll" : 1,
						"enablevscroll" : 1,
						"devicewidth" : 0.0,
						"description" : "",
						"digest" : "",
						"tags" : "",
						"style" : "",
						"subpatcher_template" : "",
						"assistshowspatchername" : 0,
						"boxes" : [ 							{
								"box" : 								{
									"id" : "obj-24",
									"maxclass" : "newobj",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 288.666666666666515, 120.0, 69.0, 22.0 ],
									"text" : "r decFactor"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-20",
									"maxclass" : "newobj",
									"numinlets" : 7,
									"numoutlets" : 1,
									"outlettype" : [ "multichannelsignal" ],
									"patching_rect" : [ 50.0, 364.0, 134.714285714285325, 22.0 ],
									"text" : "mc.pack~ 7"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-18",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 7,
									"outlettype" : [ "signal", "signal", "signal", "signal", "signal", "signal", "signal" ],
									"patching_rect" : [ 50.0, 304.0, 154.0, 22.0 ],
									"text" : "mc.unpack~ 7"
								}

							}
, 							{
								"box" : 								{
									"hidden" : 1,
									"id" : "obj-103",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 50.0, 106.0, 97.0, 22.0 ],
									"text" : "loadmess 96000"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-104",
									"maxclass" : "number",
									"maximum" : 262144,
									"minimum" : 0,
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 50.0, 171.0, 50.0, 22.0 ]
								}

							}
, 							{
								"box" : 								{
									"bgcolor" : [ 0.301961, 0.301961, 0.301961, 1.0 ],
									"bgcolor2" : [ 0.301961, 0.301961, 0.301961, 1.0 ],
									"bgfillcolor_angle" : 270.0,
									"bgfillcolor_autogradient" : 0.0,
									"bgfillcolor_color" : [ 0.498039215686275, 0.498039215686275, 0.498039215686275, 1.0 ],
									"bgfillcolor_color1" : [ 0.301961, 0.301961, 0.301961, 1.0 ],
									"bgfillcolor_color2" : [ 0.2, 0.2, 0.2, 1.0 ],
									"bgfillcolor_proportion" : 0.5,
									"bgfillcolor_type" : "color",
									"gradient" : 1,
									"id" : "obj-102",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 50.0, 211.0, 54.0, 22.0 ],
									"text" : "delay $1"
								}

							}
, 							{
								"box" : 								{
									"bgcolor" : [ 0.301961, 0.301961, 0.301961, 1.0 ],
									"bgcolor2" : [ 0.301961, 0.301961, 0.301961, 1.0 ],
									"bgfillcolor_angle" : 270.0,
									"bgfillcolor_autogradient" : 0.0,
									"bgfillcolor_color" : [ 0.498039215686275, 0.498039215686275, 0.498039215686275, 1.0 ],
									"bgfillcolor_color1" : [ 0.301961, 0.301961, 0.301961, 1.0 ],
									"bgfillcolor_color2" : [ 0.2, 0.2, 0.2, 1.0 ],
									"bgfillcolor_proportion" : 0.5,
									"bgfillcolor_type" : "color",
									"gradient" : 1,
									"id" : "obj-14",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 170.428558000000066, 211.0, 90.0, 22.0 ],
									"text" : "functiontype $1"
								}

							}
, 							{
								"box" : 								{
									"hidden" : 1,
									"id" : "obj-25",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 170.428558000000066, 100.0, 70.0, 22.0 ],
									"text" : "loadmess 0"
								}

							}
, 							{
								"box" : 								{
									"hidden" : 1,
									"id" : "obj-15",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 112.928558000000066, 130.0, 73.0, 22.0 ],
									"text" : "loadmess 0."
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-23",
									"items" : [ "x", ",", "x^2", ",", "sin", ",", "log(1+x)", ",", "sqrt(x)", ",", "1-cos(Pi/2*x)", ",", "(1-cos(Pi*x))/2", ",", "1-(1-x)^2", ",", "composite1", ",", "x^3", ",", "1-(1-x)^3", ",", "composite2", ",", "x^4", ",", "1-(1-x)^4", ",", "composite3", ",", "x^5", ",", "1-(1-x)^5", ",", "composite4", ",", "2^(10(x-1))", ",", "composite5", ",", "1-sqrt(1-x^2)", ",", "sqrt(1-(x-1)^2)" ],
									"maxclass" : "umenu",
									"numinlets" : 1,
									"numoutlets" : 3,
									"outlettype" : [ "int", "", "" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 170.428558000000066, 171.0, 109.0, 22.0 ]
								}

							}
, 							{
								"box" : 								{
									"bgcolor" : [ 0.301961, 0.301961, 0.301961, 1.0 ],
									"bgcolor2" : [ 0.301961, 0.301961, 0.301961, 1.0 ],
									"bgfillcolor_angle" : 270.0,
									"bgfillcolor_autogradient" : 0.0,
									"bgfillcolor_color" : [ 0.498039215686275, 0.498039215686275, 0.498039215686275, 1.0 ],
									"bgfillcolor_color1" : [ 0.301961, 0.301961, 0.301961, 1.0 ],
									"bgfillcolor_color2" : [ 0.2, 0.2, 0.2, 1.0 ],
									"bgfillcolor_proportion" : 0.5,
									"bgfillcolor_type" : "color",
									"gradient" : 1,
									"id" : "obj-19",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 112.928558000000066, 211.0, 48.0, 22.0 ],
									"text" : "fdbk $1"
								}

							}
, 							{
								"box" : 								{
									"format" : 6,
									"id" : "obj-16",
									"maxclass" : "flonum",
									"maximum" : 0.999,
									"minimum" : 0.0,
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 112.928558000000066, 171.0, 50.0, 22.0 ]
								}

							}
, 							{
								"box" : 								{
									"bgcolor" : [ 0.301961, 0.301961, 0.301961, 1.0 ],
									"bgcolor2" : [ 0.301961, 0.301961, 0.301961, 1.0 ],
									"bgfillcolor_angle" : 270.0,
									"bgfillcolor_autogradient" : 0.0,
									"bgfillcolor_color" : [ 0.498039215686275, 0.498039215686275, 0.498039215686275, 1.0 ],
									"bgfillcolor_color1" : [ 0.301961, 0.301961, 0.301961, 1.0 ],
									"bgfillcolor_color2" : [ 0.2, 0.2, 0.2, 1.0 ],
									"bgfillcolor_proportion" : 0.5,
									"bgfillcolor_type" : "color",
									"gradient" : 1,
									"id" : "obj-17",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 288.666666666666515, 211.0, 55.0, 22.0 ],
									"text" : "factor $1"
								}

							}
, 							{
								"box" : 								{
									"fontface" : 1,
									"fontsize" : 30.0,
									"format" : 6,
									"id" : "obj-67",
									"maxclass" : "flonum",
									"maximum" : 1.0,
									"minimum" : 0.0,
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 288.666666666666515, 160.0, 117.0, 42.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-3",
									"maxclass" : "newobj",
									"numinlets" : 7,
									"numoutlets" : 8,
									"outlettype" : [ "signal", "signal", "signal", "signal", "signal", "signal", "signal", "list" ],
									"patching_rect" : [ 50.0, 334.0, 154.0, 22.0 ],
									"text" : "abc_2d_fx_decorrelation3~"
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-47",
									"index" : 1,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "multichannelsignal" ],
									"patching_rect" : [ 50.0, 40.0, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-48",
									"index" : 1,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 50.0, 446.0, 30.0, 30.0 ]
								}

							}
 ],
						"lines" : [ 							{
								"patchline" : 								{
									"destination" : [ "obj-3", 0 ],
									"source" : [ "obj-102", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-104", 0 ],
									"hidden" : 1,
									"source" : [ "obj-103", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-102", 0 ],
									"source" : [ "obj-104", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-3", 0 ],
									"source" : [ "obj-14", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-16", 0 ],
									"hidden" : 1,
									"source" : [ "obj-15", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-19", 0 ],
									"source" : [ "obj-16", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-3", 0 ],
									"source" : [ "obj-17", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-3", 5 ],
									"source" : [ "obj-18", 5 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-3", 4 ],
									"source" : [ "obj-18", 4 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-3", 3 ],
									"source" : [ "obj-18", 3 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-3", 2 ],
									"source" : [ "obj-18", 2 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-3", 1 ],
									"source" : [ "obj-18", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-3", 0 ],
									"source" : [ "obj-18", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-3", 0 ],
									"source" : [ "obj-19", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-48", 0 ],
									"source" : [ "obj-20", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-14", 0 ],
									"source" : [ "obj-23", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-67", 0 ],
									"source" : [ "obj-24", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-23", 0 ],
									"hidden" : 1,
									"source" : [ "obj-25", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-20", 6 ],
									"source" : [ "obj-3", 6 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-20", 5 ],
									"source" : [ "obj-3", 5 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-20", 4 ],
									"source" : [ "obj-3", 4 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-20", 3 ],
									"source" : [ "obj-3", 3 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-20", 2 ],
									"source" : [ "obj-3", 2 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-20", 1 ],
									"source" : [ "obj-3", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-20", 0 ],
									"source" : [ "obj-3", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-18", 0 ],
									"source" : [ "obj-47", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-17", 0 ],
									"source" : [ "obj-67", 0 ]
								}

							}
 ]
					}
,
					"patching_rect" : [ 1226.199999928474426, 501.505399673698207, 88.0, 22.0 ],
					"saved_object_attributes" : 					{
						"description" : "",
						"digest" : "",
						"globalpatchername" : "",
						"tags" : ""
					}
,
					"text" : "p decorrelation"
				}

			}
, 			{
				"box" : 				{
					"format" : 6,
					"id" : "obj-123",
					"maxclass" : "flonum",
					"maximum" : 0.0,
					"minimum" : -127.0,
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1420.542839911694955, 1264.79858075928496, 50.0, 22.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-125",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1420.542839911694955, 1236.863409715411763, 93.0, 22.0 ],
					"text" : "r masterVolume"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-126",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1420.542839911694955, 1291.798580759285187, 48.0, 22.0 ],
					"text" : "gain $1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-131",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1299.614268483123396, 1069.000000953674316, 51.0, 22.0 ],
					"text" : "r drywet"
				}

			}
, 			{
				"box" : 				{
					"args" : [ -127 ],
					"bgmode" : 0,
					"border" : 1,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-132",
					"lockeddragscroll" : 0,
					"lockedsize" : 0,
					"maxclass" : "bpatcher",
					"name" : "abc_gaincontrol.maxpat",
					"numinlets" : 0,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "" ],
					"patching_rect" : [ 1155.199999928474426, 1133.430094842727158, 72.0, 110.0 ],
					"presentation" : 1,
					"presentation_rect" : [ -45.000005424022675, 100.498582177875505, 72.0, 110.0 ],
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-133",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1192.449999928474426, 268.698582404373155, 29.5, 22.0 ],
					"text" : "0."
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 1.0, 1.0, 1.0, 1.0 ],
					"id" : "obj-134",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1277.699999928474426, 385.698582404373155, 63.0, 20.0 ],
					"text" : "t.s-1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-141",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1226.199999928474426, 268.698582404373155, 80.0, 22.0 ],
					"text" : "loadmess 0.1"
				}

			}
, 			{
				"box" : 				{
					"format" : 6,
					"id" : "obj-142",
					"maxclass" : "flonum",
					"maximum" : 100.0,
					"minimum" : -100.0,
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1226.199999928474426, 309.698582404373155, 50.0, 22.0 ]
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.301961, 0.301961, 0.301961, 1.0 ],
					"bgcolor2" : [ 0.301961, 0.301961, 0.301961, 1.0 ],
					"bgfillcolor_angle" : 270.0,
					"bgfillcolor_autogradient" : 0.0,
					"bgfillcolor_color" : [ 0.498039215686275, 0.498039215686275, 0.498039215686275, 1.0 ],
					"bgfillcolor_color1" : [ 0.301961, 0.301961, 0.301961, 1.0 ],
					"bgfillcolor_color2" : [ 0.2, 0.2, 0.2, 1.0 ],
					"bgfillcolor_proportion" : 0.5,
					"bgfillcolor_type" : "color",
					"gradient" : 1,
					"id" : "obj-143",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1226.199999928474426, 420.698582404373155, 58.0, 22.0 ],
					"text" : "speed $1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-144",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1275.280940443942654, 1203.863410131747969, 87.0, 22.0 ],
					"text" : "loadmess 0.96"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-145",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "multichannelsignal" ],
					"patcher" : 					{
						"fileversion" : 1,
						"appversion" : 						{
							"major" : 8,
							"minor" : 5,
							"revision" : 3,
							"architecture" : "x64",
							"modernui" : 1
						}
,
						"classnamespace" : "box",
						"rect" : [ 59.0, 119.0, 640.0, 480.0 ],
						"bglocked" : 0,
						"openinpresentation" : 0,
						"default_fontsize" : 12.0,
						"default_fontface" : 0,
						"default_fontname" : "Arial",
						"gridonopen" : 1,
						"gridsize" : [ 15.0, 15.0 ],
						"gridsnaponopen" : 1,
						"objectsnaponopen" : 1,
						"statusbarvisible" : 2,
						"toolbarvisible" : 1,
						"lefttoolbarpinned" : 0,
						"toptoolbarpinned" : 0,
						"righttoolbarpinned" : 0,
						"bottomtoolbarpinned" : 0,
						"toolbars_unpinned_last_save" : 0,
						"tallnewobj" : 0,
						"boxanimatetime" : 200,
						"enablehscroll" : 1,
						"enablevscroll" : 1,
						"devicewidth" : 0.0,
						"description" : "",
						"digest" : "",
						"tags" : "",
						"style" : "",
						"subpatcher_template" : "",
						"assistshowspatchername" : 0,
						"boxes" : [ 							{
								"box" : 								{
									"id" : "obj-77",
									"maxclass" : "number",
									"maximum" : 1000,
									"minimum" : 0,
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 91.428571428571502, 84.999997322116826, 50.0, 22.0 ]
								}

							}
, 							{
								"box" : 								{
									"bgcolor" : [ 1.0, 1.0, 1.0, 1.0 ],
									"id" : "obj-80",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 143.428571428571502, 84.999997322116826, 63.0, 20.0 ],
									"text" : "(msec)"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-83",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 91.428571428571502, 43.999997322116826, 77.0, 22.0 ],
									"text" : "loadmess 20"
								}

							}
, 							{
								"box" : 								{
									"bgcolor" : [ 0.301961, 0.301961, 0.301961, 1.0 ],
									"bgcolor2" : [ 0.301961, 0.301961, 0.301961, 1.0 ],
									"bgfillcolor_angle" : 270.0,
									"bgfillcolor_autogradient" : 0.0,
									"bgfillcolor_color" : [ 0.498039215686275, 0.498039215686275, 0.498039215686275, 1.0 ],
									"bgfillcolor_color1" : [ 0.301961, 0.301961, 0.301961, 1.0 ],
									"bgfillcolor_color2" : [ 0.2, 0.2, 0.2, 1.0 ],
									"bgfillcolor_proportion" : 0.5,
									"bgfillcolor_type" : "color",
									"gradient" : 1,
									"id" : "obj-84",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 91.428571428571502, 119.999997322116826, 79.0, 22.0 ],
									"text" : "returntime $1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-7",
									"maxclass" : "newobj",
									"numinlets" : 7,
									"numoutlets" : 1,
									"outlettype" : [ "multichannelsignal" ],
									"patching_rect" : [ 50.0, 276.333328664302826, 118.428571428571502, 22.0 ],
									"text" : "mc.pack~ 7"
								}

							}
, 							{
								"box" : 								{
									"fontface" : 1,
									"fontsize" : 14.0,
									"id" : "obj-38",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 8,
									"outlettype" : [ "signal", "signal", "signal", "signal", "signal", "signal", "signal", "list" ],
									"patching_rect" : [ 50.000000000000114, 218.0, 135.0, 24.0 ],
									"text" : "abc_2d_encoder3~",
									"textcolor" : [ 0.968627450980392, 0.968627450980392, 0.968627450980392, 1.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-23",
									"index" : 1,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 49.999992716514612, 39.999997322116826, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-26",
									"index" : 1,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 49.999992716514612, 358.333317322116841, 30.0, 30.0 ]
								}

							}
 ],
						"lines" : [ 							{
								"patchline" : 								{
									"destination" : [ "obj-38", 0 ],
									"source" : [ "obj-23", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-7", 6 ],
									"source" : [ "obj-38", 6 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-7", 5 ],
									"source" : [ "obj-38", 5 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-7", 4 ],
									"source" : [ "obj-38", 4 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-7", 3 ],
									"source" : [ "obj-38", 3 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-7", 2 ],
									"source" : [ "obj-38", 2 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-7", 1 ],
									"source" : [ "obj-38", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-7", 0 ],
									"source" : [ "obj-38", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-26", 0 ],
									"source" : [ "obj-7", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-84", 0 ],
									"source" : [ "obj-77", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-77", 0 ],
									"source" : [ "obj-83", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-38", 0 ],
									"source" : [ "obj-84", 0 ]
								}

							}
 ]
					}
,
					"patching_rect" : [ 1226.199999928474426, 463.514544080981977, 67.0, 22.0 ],
					"saved_object_attributes" : 					{
						"description" : "",
						"digest" : "",
						"globalpatchername" : "",
						"tags" : ""
					}
,
					"text" : "p encoding"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-146",
					"maxclass" : "newobj",
					"numinlets" : 4,
					"numoutlets" : 1,
					"outlettype" : [ "multichannelsignal" ],
					"patcher" : 					{
						"fileversion" : 1,
						"appversion" : 						{
							"major" : 8,
							"minor" : 5,
							"revision" : 3,
							"architecture" : "x64",
							"modernui" : 1
						}
,
						"classnamespace" : "box",
						"rect" : [ 59.0, 119.0, 1267.0, 723.0 ],
						"bglocked" : 0,
						"openinpresentation" : 0,
						"default_fontsize" : 12.0,
						"default_fontface" : 0,
						"default_fontname" : "Arial",
						"gridonopen" : 1,
						"gridsize" : [ 15.0, 15.0 ],
						"gridsnaponopen" : 1,
						"objectsnaponopen" : 1,
						"statusbarvisible" : 2,
						"toolbarvisible" : 1,
						"lefttoolbarpinned" : 0,
						"toptoolbarpinned" : 0,
						"righttoolbarpinned" : 0,
						"bottomtoolbarpinned" : 0,
						"toolbars_unpinned_last_save" : 0,
						"tallnewobj" : 0,
						"boxanimatetime" : 200,
						"enablehscroll" : 1,
						"enablevscroll" : 1,
						"devicewidth" : 0.0,
						"description" : "",
						"digest" : "",
						"tags" : "",
						"style" : "",
						"subpatcher_template" : "",
						"assistshowspatchername" : 0,
						"boxes" : [ 							{
								"box" : 								{
									"id" : "obj-45",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 133.833305716514587, 434.133337581157775, 54.0, 22.0 ],
									"text" : "mc.dac~"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-44",
									"maxclass" : "newobj",
									"numinlets" : 4,
									"numoutlets" : 1,
									"outlettype" : [ "multichannelsignal" ],
									"patching_rect" : [ 133.833305716514587, 362.0, 70.0, 22.0 ],
									"text" : "mc.pack~ 4"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-2",
									"maxclass" : "newobj",
									"numinlets" : 8,
									"numoutlets" : 0,
									"patching_rect" : [ 138.83332747220993, 285.533342385292144, 134.0, 22.0 ],
									"text" : "dac~ 1 2 3 4 5 6 7 8"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-7",
									"maxclass" : "newobj",
									"numinlets" : 7,
									"numoutlets" : 1,
									"outlettype" : [ "multichannelsignal" ],
									"patching_rect" : [ 274.83332747220993, 238.0, 82.0, 22.0 ],
									"text" : "mc.pack~ 7"
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-1",
									"index" : 1,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 274.83332747220993, 313.533342385292144, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-30",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "multichannelsignal" ],
									"patching_rect" : [ 138.83332747220993, 141.795652151107788, 40.0, 22.0 ],
									"text" : "mc.*~"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-29",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "float" ],
									"patching_rect" : [ 71.0, 100.0, 29.5, 22.0 ],
									"text" : "!- 1."
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-28",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "multichannelsignal" ],
									"patching_rect" : [ 50.0, 141.795652151107788, 40.0, 22.0 ],
									"text" : "mc.*~"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-18",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 7,
									"outlettype" : [ "signal", "signal", "signal", "signal", "signal", "signal", "signal" ],
									"patching_rect" : [ 50.0, 167.295644342899323, 84.0, 22.0 ],
									"text" : "mc.unpack~ 7"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-4",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 7,
									"outlettype" : [ "signal", "signal", "signal", "signal", "signal", "signal", "signal" ],
									"patching_rect" : [ 138.83332747220993, 167.295644342899323, 129.285714285714221, 22.0 ],
									"text" : "mc.unpack~ 7"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-6",
									"maxclass" : "newobj",
									"numinlets" : 7,
									"numoutlets" : 5,
									"outlettype" : [ "signal", "signal", "signal", "signal", "list" ],
									"patching_rect" : [ 138.83332747220993, 238.0, 125.0, 22.0 ],
									"text" : "abc_2d_decoder3_4~"
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-13",
									"index" : 1,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "multichannelsignal" ],
									"patching_rect" : [ 49.999992716514612, 39.999989583663933, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-15",
									"index" : 2,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 98.916679716514636, 39.999989583663933, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-17",
									"index" : 3,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "multichannelsignal" ],
									"patching_rect" : [ 138.833305716514587, 39.999989583663933, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-20",
									"index" : 4,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 173.833305716514587, 39.999989583663933, 30.0, 30.0 ]
								}

							}
 ],
						"lines" : [ 							{
								"patchline" : 								{
									"destination" : [ "obj-28", 0 ],
									"source" : [ "obj-13", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-29", 0 ],
									"order" : 1,
									"source" : [ "obj-15", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-30", 1 ],
									"order" : 0,
									"source" : [ "obj-15", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-30", 0 ],
									"source" : [ "obj-17", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-6", 6 ],
									"order" : 1,
									"source" : [ "obj-18", 6 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-6", 5 ],
									"order" : 1,
									"source" : [ "obj-18", 5 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-6", 4 ],
									"order" : 1,
									"source" : [ "obj-18", 4 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-6", 3 ],
									"order" : 1,
									"source" : [ "obj-18", 3 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-6", 2 ],
									"order" : 1,
									"source" : [ "obj-18", 2 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-6", 1 ],
									"order" : 1,
									"source" : [ "obj-18", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-6", 0 ],
									"order" : 1,
									"source" : [ "obj-18", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-7", 6 ],
									"order" : 0,
									"source" : [ "obj-18", 6 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-7", 5 ],
									"order" : 0,
									"source" : [ "obj-18", 5 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-7", 4 ],
									"order" : 0,
									"source" : [ "obj-18", 4 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-7", 3 ],
									"order" : 0,
									"source" : [ "obj-18", 3 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-7", 2 ],
									"order" : 0,
									"source" : [ "obj-18", 2 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-7", 1 ],
									"order" : 0,
									"source" : [ "obj-18", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-7", 0 ],
									"order" : 0,
									"source" : [ "obj-18", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-6", 0 ],
									"source" : [ "obj-20", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-18", 0 ],
									"source" : [ "obj-28", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-28", 1 ],
									"source" : [ "obj-29", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-4", 0 ],
									"source" : [ "obj-30", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-6", 6 ],
									"order" : 1,
									"source" : [ "obj-4", 6 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-6", 5 ],
									"order" : 1,
									"source" : [ "obj-4", 5 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-6", 4 ],
									"order" : 1,
									"source" : [ "obj-4", 4 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-6", 3 ],
									"order" : 1,
									"source" : [ "obj-4", 3 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-6", 2 ],
									"order" : 1,
									"source" : [ "obj-4", 2 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-6", 1 ],
									"order" : 1,
									"source" : [ "obj-4", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-6", 0 ],
									"order" : 1,
									"source" : [ "obj-4", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-7", 6 ],
									"order" : 0,
									"source" : [ "obj-4", 6 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-7", 5 ],
									"order" : 0,
									"source" : [ "obj-4", 5 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-7", 4 ],
									"order" : 0,
									"source" : [ "obj-4", 4 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-7", 3 ],
									"order" : 0,
									"source" : [ "obj-4", 3 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-7", 2 ],
									"order" : 0,
									"source" : [ "obj-4", 2 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-7", 1 ],
									"order" : 0,
									"source" : [ "obj-4", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-7", 0 ],
									"order" : 0,
									"source" : [ "obj-4", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-45", 0 ],
									"source" : [ "obj-44", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-2", 4 ],
									"source" : [ "obj-6", 3 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-2", 5 ],
									"source" : [ "obj-6", 2 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-2", 1 ],
									"source" : [ "obj-6", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-2", 0 ],
									"source" : [ "obj-6", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-1", 0 ],
									"source" : [ "obj-7", 0 ]
								}

							}
 ]
					}
,
					"patching_rect" : [ 1360.614268199637991, 1236.863409715411763, 52.0, 22.0 ],
					"saved_object_attributes" : 					{
						"description" : "",
						"digest" : "",
						"globalpatchername" : "",
						"tags" : ""
					}
,
					"text" : "p output",
					"varname" : "patcher[1]"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-147",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1355.614268483123396, 1139.432955164195391, 52.0, 20.0 ],
					"text" : "Dry/Wet"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-148",
					"maxclass" : "toggle",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1426.542839911694955, 1172.188042001540907, 24.0, 24.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-149",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1426.542839911694955, 1203.863410131747969, 59.0, 22.0 ],
					"text" : "stereo $1"
				}

			}
, 			{
				"box" : 				{
					"format" : 6,
					"id" : "obj-157",
					"maxclass" : "flonum",
					"maximum" : 1.0,
					"minimum" : 0.0,
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1318.614268483123396, 1163.063409523780592, 50.0, 22.0 ],
					"varname" : "drywet[1]"
				}

			}
, 			{
				"box" : 				{
					"args" : [ "instr" ],
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-160",
					"lockeddragscroll" : 0,
					"lockedsize" : 0,
					"maxclass" : "bpatcher",
					"name" : "bbdmi_live_granulator~.maxpat",
					"numinlets" : 2,
					"numoutlets" : 3,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "multichannelsignal", "", "" ],
					"patching_rect" : [ 1405.614268199637991, 495.726187717914286, 194.0, 564.840859025238956 ],
					"presentation" : 1,
					"presentation_rect" : [ 597.084170819357382, 619.230281195762245, 194.0, 564.840859025238956 ],
					"varname" : "bbdmi_live.granulator~[1]",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-275",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 559.399928778409958, 157.015226643799451, 48.0, 22.0 ],
					"text" : "del 100"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-274",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 559.399928778409958, 130.615225499390363, 58.0, 22.0 ],
					"text" : "loadbang"
				}

			}
, 			{
				"box" : 				{
					"fontface" : 0,
					"fontname" : "Arial",
					"fontsize" : 14.0,
					"id" : "obj-171",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 559.399928778409958, 207.948585468052443, 65.0, 22.0 ],
					"text" : "PRESET"
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.137254901960784, 0.137254901960784, 0.137254901960784, 1.0 ],
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-173",
					"maxclass" : "textbutton",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "", "", "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 601.260341376066208, 241.857281165836412, 39.0, 21.0 ],
					"text" : "store",
					"texton" : "store",
					"textoncolor" : [ 1.0, 1.0, 1.0, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.137254901960784, 0.137254901960784, 0.137254901960784, 1.0 ],
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-225",
					"maxclass" : "textbutton",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "", "", "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 559.399928778409958, 241.857281165836412, 39.0, 21.0 ],
					"text" : "recall",
					"texton" : "recall",
					"textoncolor" : [ 1.0, 1.0, 1.0, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.137254901960784, 0.137254901960784, 0.137254901960784, 1.0 ],
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-175",
					"maxclass" : "textbutton",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "", "", "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 675.899928778409958, 352.274672466038282, 66.0, 21.0 ],
					"text" : "writeagain",
					"texton" : "writeagain",
					"textoncolor" : [ 1.0, 1.0, 1.0, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.137254901960784, 0.137254901960784, 0.137254901960784, 1.0 ],
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-226",
					"maxclass" : "textbutton",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "", "", "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 631.149928778409958, 352.274672466038282, 39.0, 21.0 ],
					"text" : "write",
					"texton" : "write",
					"textoncolor" : [ 1.0, 1.0, 1.0, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-177",
					"maxclass" : "newobj",
					"numinlets" : 3,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patcher" : 					{
						"fileversion" : 1,
						"appversion" : 						{
							"major" : 8,
							"minor" : 5,
							"revision" : 3,
							"architecture" : "x64",
							"modernui" : 1
						}
,
						"classnamespace" : "box",
						"rect" : [ 36.0, 79.0, 228.0, 264.0 ],
						"bglocked" : 0,
						"openinpresentation" : 0,
						"default_fontsize" : 12.0,
						"default_fontface" : 0,
						"default_fontname" : "Arial",
						"gridonopen" : 1,
						"gridsize" : [ 15.0, 15.0 ],
						"gridsnaponopen" : 1,
						"objectsnaponopen" : 1,
						"statusbarvisible" : 2,
						"toolbarvisible" : 1,
						"lefttoolbarpinned" : 0,
						"toptoolbarpinned" : 0,
						"righttoolbarpinned" : 0,
						"bottomtoolbarpinned" : 0,
						"toolbars_unpinned_last_save" : 0,
						"tallnewobj" : 0,
						"boxanimatetime" : 200,
						"enablehscroll" : 1,
						"enablevscroll" : 1,
						"devicewidth" : 0.0,
						"description" : "",
						"digest" : "",
						"tags" : "",
						"style" : "",
						"subpatcher_template" : "Default Max 7",
						"assistshowspatchername" : 0,
						"boxes" : [ 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-24",
									"index" : 3,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 114.357142857142833, 12.5, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-23",
									"maxclass" : "button",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 69.357142857142833, 66.5, 24.0, 24.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-21",
									"maxclass" : "button",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 11.357142857142833, 66.5, 24.0, 24.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-19",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 11.357142857142833, 143.0, 54.0, 22.0 ],
									"text" : "recall $1"
								}

							}
, 							{
								"box" : 								{
									"color" : [ 1.0, 0.61176472902298, 0.61176472902298, 1.0 ],
									"fontface" : 0,
									"fontname" : "Arial",
									"fontsize" : 12.0,
									"id" : "obj-59",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 147.799525669642833, 67.5, 70.0, 22.0 ],
									"text" : "loadmess 1"
								}

							}
, 							{
								"box" : 								{
									"fontface" : 0,
									"fontname" : "Arial",
									"fontsize" : 12.0,
									"id" : "obj-64",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"patching_rect" : [ 11.357142857142833, 114.5, 39.0, 22.0 ],
									"text" : "i"
								}

							}
, 							{
								"box" : 								{
									"fontface" : 0,
									"fontname" : "Arial",
									"fontsize" : 12.0,
									"id" : "obj-66",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"patching_rect" : [ 69.357142857142833, 114.5, 40.5, 22.0 ],
									"text" : "i"
								}

							}
, 							{
								"box" : 								{
									"fontface" : 0,
									"fontname" : "Arial",
									"fontsize" : 12.0,
									"id" : "obj-68",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 69.357142857142833, 143.0, 52.0, 22.0 ],
									"text" : "store $1"
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-17",
									"index" : 1,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 10.5, 218.0, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-16",
									"index" : 2,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 69.357142857142833, 12.5, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-14",
									"index" : 1,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 11.357142857142833, 12.5, 30.0, 30.0 ]
								}

							}
 ],
						"lines" : [ 							{
								"patchline" : 								{
									"destination" : [ "obj-21", 0 ],
									"midpoints" : [ 20.857142857142833, 45.0, 20.857142857142833, 45.0 ],
									"source" : [ "obj-14", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-23", 0 ],
									"midpoints" : [ 78.857142857142833, 45.0, 78.857142857142833, 45.0 ],
									"source" : [ "obj-16", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-17", 0 ],
									"midpoints" : [ 20.857142857142833, 204.0, 20.0, 204.0 ],
									"source" : [ "obj-19", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-64", 0 ],
									"midpoints" : [ 20.857142857142833, 93.0, 20.857142857142833, 93.0 ],
									"source" : [ "obj-21", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-66", 0 ],
									"midpoints" : [ 78.857142857142833, 93.0, 78.857142857142833, 93.0 ],
									"source" : [ "obj-23", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-64", 1 ],
									"midpoints" : [ 123.857142857142833, 99.0, 40.857142857142833, 99.0 ],
									"order" : 1,
									"source" : [ "obj-24", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-66", 1 ],
									"midpoints" : [ 123.857142857142833, 99.0, 100.357142857142833, 99.0 ],
									"order" : 0,
									"source" : [ "obj-24", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-64", 1 ],
									"midpoints" : [ 157.299525669642833, 99.0, 40.857142857142833, 99.0 ],
									"order" : 1,
									"source" : [ "obj-59", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-66", 1 ],
									"midpoints" : [ 157.299525669642833, 99.0, 100.357142857142833, 99.0 ],
									"order" : 0,
									"source" : [ "obj-59", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-19", 0 ],
									"midpoints" : [ 20.857142857142833, 138.0, 20.857142857142833, 138.0 ],
									"source" : [ "obj-64", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-68", 0 ],
									"midpoints" : [ 78.857142857142833, 138.0, 78.857142857142833, 138.0 ],
									"source" : [ "obj-66", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-17", 0 ],
									"midpoints" : [ 78.857142857142833, 204.0, 20.0, 204.0 ],
									"source" : [ "obj-68", 0 ]
								}

							}
 ]
					}
,
					"patching_rect" : [ 559.399928778409958, 320.448585468051988, 101.0, 22.0 ],
					"saved_object_attributes" : 					{
						"description" : "",
						"digest" : "",
						"globalpatchername" : "",
						"tags" : ""
					}
,
					"text" : "p settings"
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.137254901960784, 0.137254901960784, 0.137254901960784, 1.0 ],
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-179",
					"maxclass" : "textbutton",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "", "", "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 755.399928778409958, 352.274672466038282, 39.0, 21.0 ],
					"text" : "clear",
					"texton" : "clear",
					"textoncolor" : [ 1.0, 1.0, 1.0, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-227",
					"maxclass" : "incdec",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "float" ],
					"parameter_enable" : 1,
					"patching_rect" : [ 642.260341376066208, 241.857281165836412, 41.0, 21.0 ],
					"saved_attribute_attributes" : 					{
						"valueof" : 						{
							"parameter_initial" : [ 1 ],
							"parameter_initial_enable" : 1,
							"parameter_invisible" : 1,
							"parameter_longname" : "incdec",
							"parameter_shortname" : "incdec",
							"parameter_type" : 3
						}

					}
,
					"varname" : "incdec[1]"
				}

			}
, 			{
				"box" : 				{
					"fontface" : 0,
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-228",
					"maxclass" : "number",
					"minimum" : 1,
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 642.260341376066208, 270.835541921375352, 40.0, 22.0 ],
					"triangle" : 0,
					"varname" : "number[1]"
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.137254901960784, 0.137254901960784, 0.137254901960784, 1.0 ],
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-182",
					"maxclass" : "textbutton",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "", "", "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 586.399928778409958, 352.274672466038282, 39.0, 21.0 ],
					"text" : "read",
					"texton" : "read",
					"textoncolor" : [ 1.0, 1.0, 1.0, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-229",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patcher" : 					{
						"fileversion" : 1,
						"appversion" : 						{
							"major" : 8,
							"minor" : 5,
							"revision" : 3,
							"architecture" : "x64",
							"modernui" : 1
						}
,
						"classnamespace" : "box",
						"rect" : [ 36.0, 79.0, 160.0, 238.0 ],
						"bglocked" : 0,
						"openinpresentation" : 0,
						"default_fontsize" : 12.0,
						"default_fontface" : 0,
						"default_fontname" : "Arial",
						"gridonopen" : 1,
						"gridsize" : [ 15.0, 15.0 ],
						"gridsnaponopen" : 1,
						"objectsnaponopen" : 1,
						"statusbarvisible" : 2,
						"toolbarvisible" : 1,
						"lefttoolbarpinned" : 0,
						"toptoolbarpinned" : 0,
						"righttoolbarpinned" : 0,
						"bottomtoolbarpinned" : 0,
						"toolbars_unpinned_last_save" : 0,
						"tallnewobj" : 0,
						"boxanimatetime" : 200,
						"enablehscroll" : 1,
						"enablevscroll" : 1,
						"devicewidth" : 0.0,
						"description" : "",
						"digest" : "",
						"tags" : "",
						"style" : "",
						"subpatcher_template" : "Default Max 7",
						"assistshowspatchername" : 0,
						"boxes" : [ 							{
								"box" : 								{
									"id" : "obj-85",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 33.25, 152.5, 120.0, 22.0 ],
									"text" : "prepend pattrstorage"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-78",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "", "" ],
									"patching_rect" : [ 14.0, 76.0, 63.0, 22.0 ],
									"text" : "route read"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 12.0,
									"id" : "obj-66",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 5,
									"outlettype" : [ "", "", "", "", "" ],
									"patching_rect" : [ 14.0, 125.384386777777763, 96.0, 22.0 ],
									"text" : "regexp (.+)\\\\..+$"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 12.0,
									"id" : "obj-72",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "int" ],
									"patching_rect" : [ 14.0, 101.384386777777763, 57.0, 22.0 ],
									"text" : "strippath"
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-86",
									"index" : 1,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 14.000000023437501, 16.000012222222267, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-87",
									"index" : 1,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 33.250000023437494, 192.500012222222267, 30.0, 30.0 ]
								}

							}
 ],
						"lines" : [ 							{
								"patchline" : 								{
									"destination" : [ "obj-85", 0 ],
									"source" : [ "obj-66", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-66", 0 ],
									"source" : [ "obj-72", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-72", 0 ],
									"source" : [ "obj-78", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-87", 0 ],
									"source" : [ "obj-85", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-78", 0 ],
									"source" : [ "obj-86", 0 ]
								}

							}
 ]
					}
,
					"patching_rect" : [ 559.399928778409958, 439.6529333169442, 52.0, 22.0 ],
					"saved_object_attributes" : 					{
						"description" : "",
						"digest" : "",
						"globalpatchername" : "",
						"tags" : ""
					}
,
					"text" : "p preset"
				}

			}
, 			{
				"box" : 				{
					"bubblesize" : 20,
					"embed" : 0,
					"id" : "obj-230",
					"maxclass" : "preset",
					"numinlets" : 1,
					"numoutlets" : 5,
					"outlettype" : [ "preset", "int", "preset", "int", "" ],
					"patching_rect" : [ 559.399928801847636, 468.448585468051988, 196.999999976562549, 29.000000000000114 ]
				}

			}
, 			{
				"box" : 				{
					"autorestore" : "preset.json",
					"id" : "obj-231",
					"linecount" : 2,
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 559.399928778409958, 396.948585468051988, 235.000000000000114, 35.0 ],
					"priority" : 					{
						"bbdmi.multislider[3]::Multislider" : 1,
						"bbdmi.multislider[2]::Multislider" : 1
					}
,
					"saved_object_attributes" : 					{
						"client_rect" : [ 100, 100, 500, 600 ],
						"parameter_enable" : 0,
						"parameter_mappable" : 0,
						"storage_rect" : [ 200, 200, 800, 500 ]
					}
,
					"text" : "pattrstorage amelia @savemode 1 @autorestore 1 @changemode 1",
					"varname" : "amelia"
				}

			}
, 			{
				"box" : 				{
					"angle" : 270.0,
					"bgcolor" : [ 0.2, 0.2, 0.2, 0.0 ],
					"border" : 1,
					"bordercolor" : [ 0.0, 0.0, 0.0, 1.0 ],
					"id" : "obj-207",
					"maxclass" : "panel",
					"mode" : 0,
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 550.899928790128797, 199.448585468052443, 256.999999976562549, 305.0 ],
					"proportion" : 0.5,
					"rounded" : 0
				}

			}
, 			{
				"box" : 				{
					"angle" : 270.0,
					"bgcolor" : [ 0.2, 0.2, 0.2, 0.0 ],
					"border" : 1,
					"bordercolor" : [ 0.0, 0.0, 0.0, 1.0 ],
					"id" : "obj-119",
					"maxclass" : "panel",
					"mode" : 0,
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 550.899928790128797, 517.000000953674316, 312.0, 336.698581450698839 ],
					"presentation" : 1,
					"presentation_rect" : [ 821.166667699813843, 649.230281195762245, 152.0, 267.0 ],
					"proportion" : 0.5,
					"rounded" : 0
				}

			}
 ],
		"lines" : [ 			{
				"patchline" : 				{
					"destination" : [ "obj-2", 0 ],
					"source" : [ "obj-1", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-5", 1 ],
					"midpoints" : [ 792.699999928474426, 586.000000953674316, 692.649928778409958, 586.000000953674316 ],
					"source" : [ "obj-10", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-104", 0 ],
					"source" : [ "obj-100", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-106", 0 ],
					"source" : [ "obj-100", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-108", 0 ],
					"source" : [ "obj-100", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-109", 0 ],
					"midpoints" : [ 610.649928778409958, 802.000000953674316, 610.649928778409958, 802.000000953674316 ],
					"source" : [ "obj-102", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-85", 0 ],
					"midpoints" : [ 518.750071078538895, 1459.000000953674316, 405.696765080094451, 1459.000000953674316 ],
					"source" : [ "obj-104", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-114", 0 ],
					"midpoints" : [ 578.750071078538895, 1459.000000953674316, 691.696765080094451, 1459.000000953674316 ],
					"source" : [ "obj-108", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-89", 0 ],
					"midpoints" : [ 610.649928778409958, 733.000000953674316, 610.649928778409958, 733.000000953674316 ],
					"source" : [ "obj-11", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-112", 0 ],
					"source" : [ "obj-110", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-100", 0 ],
					"source" : [ "obj-112", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-165", 2 ],
					"midpoints" : [ 548.696765080094451, 1525.000000953674316, 405.696765080094451, 1525.000000953674316 ],
					"source" : [ "obj-113", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-165", 2 ],
					"midpoints" : [ 691.696765080094451, 1525.000000953674316, 405.696765080094451, 1525.000000953674316 ],
					"source" : [ "obj-114", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-35", 0 ],
					"order" : 0,
					"source" : [ "obj-12", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-36", 0 ],
					"order" : 1,
					"source" : [ "obj-12", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-126", 0 ],
					"source" : [ "obj-123", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-123", 0 ],
					"source" : [ "obj-125", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-146", 3 ],
					"source" : [ "obj-126", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-42", 0 ],
					"source" : [ "obj-128", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-42", 0 ],
					"source" : [ "obj-129", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-14", 0 ],
					"source" : [ "obj-13", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-157", 0 ],
					"source" : [ "obj-131", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-146", 3 ],
					"source" : [ "obj-132", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-142", 0 ],
					"source" : [ "obj-133", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-146", 2 ],
					"order" : 1,
					"source" : [ "obj-137", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-159", 0 ],
					"order" : 0,
					"source" : [ "obj-137", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-27", 0 ],
					"midpoints" : [ 1601.780934866304733, 254.698582404373155, 1396.780934866304733, 254.698582404373155 ],
					"source" : [ "obj-14", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-142", 0 ],
					"source" : [ "obj-141", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-82", 0 ],
					"source" : [ "obj-142", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-145", 0 ],
					"source" : [ "obj-143", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-157", 0 ],
					"midpoints" : [ 1284.780940443942654, 1236.705442147967915, 1355.447573916152578, 1236.705442147967915, 1355.447573916152578, 1227.705442147967915, 1367.447573916152578, 1227.705442147967915, 1367.447573916152578, 1200.705442147967915, 1328.114268483123396, 1200.705442147967915 ],
					"source" : [ "obj-144", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-96", 0 ],
					"source" : [ "obj-145", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-42", 0 ],
					"source" : [ "obj-146", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-149", 0 ],
					"midpoints" : [ 1436.042839911694955, 1197.705442147967915, 1436.042839911694955, 1197.705442147967915 ],
					"source" : [ "obj-148", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-146", 3 ],
					"midpoints" : [ 1436.042839911694955, 1227.705442147967915, 1403.114268199637991, 1227.705442147967915 ],
					"source" : [ "obj-149", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-146", 1 ],
					"midpoints" : [ 1328.114268483123396, 1227.705442147967915, 1381.114268199637991, 1227.705442147967915 ],
					"source" : [ "obj-157", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-1", 0 ],
					"source" : [ "obj-16", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-124", 1 ],
					"source" : [ "obj-160", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-137", 0 ],
					"source" : [ "obj-160", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-162", 0 ],
					"source" : [ "obj-163", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-165", 1 ],
					"source" : [ "obj-164", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-153", 0 ],
					"order" : 2,
					"source" : [ "obj-165", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-163", 0 ],
					"order" : 1,
					"source" : [ "obj-165", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-213", 0 ],
					"order" : 0,
					"source" : [ "obj-165", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-272", 2 ],
					"source" : [ "obj-166", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-160", 0 ],
					"source" : [ "obj-168", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-4", 1 ],
					"order" : 0,
					"source" : [ "obj-169", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-4", 0 ],
					"order" : 1,
					"source" : [ "obj-169", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-27", 0 ],
					"midpoints" : [ 1485.780934866304733, 254.698582404373155, 1396.780934866304733, 254.698582404373155 ],
					"source" : [ "obj-17", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-145", 0 ],
					"source" : [ "obj-172", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-177", 1 ],
					"midpoints" : [ 610.760341376066208, 264.448585468051988, 609.899928778409958, 264.448585468051988 ],
					"source" : [ "obj-173", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-231", 0 ],
					"midpoints" : [ 708.899928778409958, 375.448585468052443, 568.899928778409958, 375.448585468052443 ],
					"source" : [ "obj-175", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-231", 0 ],
					"midpoints" : [ 568.899928778409958, 342.448585468052443, 568.899928778409958, 342.448585468052443 ],
					"source" : [ "obj-177", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-231", 0 ],
					"midpoints" : [ 774.899928778409958, 375.448585468052443, 568.899928778409958, 375.448585468052443 ],
					"source" : [ "obj-179", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-22", 0 ],
					"source" : [ "obj-18", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-231", 0 ],
					"midpoints" : [ 605.899928778409958, 375.448585468052443, 568.899928778409958, 375.448585468052443 ],
					"source" : [ "obj-182", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-8", 0 ],
					"source" : [ "obj-184", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-21", 0 ],
					"source" : [ "obj-19", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-288", 9 ],
					"source" : [ "obj-197", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-212", 0 ],
					"source" : [ "obj-198", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-288", 8 ],
					"source" : [ "obj-199", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-27", 0 ],
					"midpoints" : [ 1804.78093486630496, 254.698582404373155, 1396.780934866304733, 254.698582404373155 ],
					"source" : [ "obj-2", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-288", 7 ],
					"source" : [ "obj-200", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-288", 6 ],
					"source" : [ "obj-202", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-288", 5 ],
					"source" : [ "obj-203", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-288", 4 ],
					"source" : [ "obj-205", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-288", 3 ],
					"source" : [ "obj-206", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-288", 2 ],
					"source" : [ "obj-208", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-288", 1 ],
					"source" : [ "obj-209", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-24", 0 ],
					"source" : [ "obj-21", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-216", 0 ],
					"order" : 0,
					"source" : [ "obj-212", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-29", 1 ],
					"order" : 1,
					"source" : [ "obj-212", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-288", 0 ],
					"source" : [ "obj-214", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-146", 3 ],
					"source" : [ "obj-216", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-3", 0 ],
					"source" : [ "obj-22", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-177", 0 ],
					"midpoints" : [ 568.899928778409958, 264.448585468051988, 568.899928778409958, 264.448585468051988 ],
					"source" : [ "obj-225", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-231", 0 ],
					"midpoints" : [ 650.649928778409958, 375.448585468052443, 568.899928778409958, 375.448585468052443 ],
					"source" : [ "obj-226", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-228", 0 ],
					"midpoints" : [ 651.760341376066208, 264.448585468051988, 651.760341376066208, 264.448585468051988 ],
					"source" : [ "obj-227", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-177", 2 ],
					"midpoints" : [ 651.760341376066208, 315.448585468052443, 650.899928778409958, 315.448585468052443 ],
					"order" : 0,
					"source" : [ "obj-228", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-227", 0 ],
					"midpoints" : [ 651.760341376066208, 303.448585468052443, 693.399928754972279, 303.448585468052443, 693.399928754972279, 228.448585468052443, 651.760341376066208, 228.448585468052443 ],
					"order" : 1,
					"source" : [ "obj-228", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-230", 0 ],
					"midpoints" : [ 568.899928778409958, 462.448585468052443, 568.899928801847636, 462.448585468052443 ],
					"source" : [ "obj-229", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-11", 0 ],
					"midpoints" : [ 610.649928778409958, 655.000000953674316, 610.649928778409958, 655.000000953674316 ],
					"source" : [ "obj-23", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-229", 0 ],
					"midpoints" : [ 568.899928778409958, 432.448585468052443, 568.899928778409958, 432.448585468052443 ],
					"source" : [ "obj-231", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-21", 0 ],
					"source" : [ "obj-239", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-27", 0 ],
					"source" : [ "obj-24", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-22", 0 ],
					"source" : [ "obj-240", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-26", 2 ],
					"order" : 1,
					"source" : [ "obj-248", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-81", 0 ],
					"order" : 0,
					"source" : [ "obj-248", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-31", 0 ],
					"midpoints" : [ 692.649928778409958, 655.000000953674316, 692.649928778409958, 655.000000953674316 ],
					"source" : [ "obj-25", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-59", 0 ],
					"source" : [ "obj-250", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-26", 0 ],
					"source" : [ "obj-252", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-298", 0 ],
					"order" : 1,
					"source" : [ "obj-26", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-40", 1 ],
					"midpoints" : [ 133.366668283939362, 835.000000953674316, 199.199999928474426, 835.000000953674316, 199.199999928474426, 787.000000953674316, 253.199999928474426, 787.000000953674316, 253.199999928474426, 694.000000953674316, 541.199999928474426, 694.000000953674316, 541.199999928474426, 559.000000953674316, 580.699999928474426, 559.000000953674316 ],
					"order" : 0,
					"source" : [ "obj-26", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-248", 0 ],
					"order" : 0,
					"source" : [ "obj-260", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-26", 1 ],
					"order" : 1,
					"source" : [ "obj-260", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-252", 0 ],
					"source" : [ "obj-261", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-271", 0 ],
					"source" : [ "obj-269", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-168", 0 ],
					"source" : [ "obj-27", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-172", 0 ],
					"source" : [ "obj-27", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-272", 0 ],
					"source" : [ "obj-271", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-169", 0 ],
					"source" : [ "obj-272", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-275", 0 ],
					"source" : [ "obj-274", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-225", 0 ],
					"source" : [ "obj-275", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-272", 1 ],
					"source" : [ "obj-288", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-32", 1 ],
					"source" : [ "obj-292", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-37", 0 ],
					"source" : [ "obj-298", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-27", 0 ],
					"midpoints" : [ 1698.780934866304733, 254.698582404373155, 1396.780934866304733, 254.698582404373155 ],
					"source" : [ "obj-3", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-89", 1 ],
					"midpoints" : [ 692.649928778409958, 733.000000953674316, 692.649928778409958, 733.000000953674316 ],
					"source" : [ "obj-31", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-250", 0 ],
					"order" : 0,
					"source" : [ "obj-32", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-56", 0 ],
					"order" : 1,
					"source" : [ "obj-32", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-32", 0 ],
					"source" : [ "obj-34", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-12", 0 ],
					"source" : [ "obj-37", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-44", 0 ],
					"source" : [ "obj-38", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-165", 0 ],
					"source" : [ "obj-4", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-52", 0 ],
					"source" : [ "obj-40", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-40", 0 ],
					"source" : [ "obj-43", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-32", 2 ],
					"source" : [ "obj-44", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-23", 0 ],
					"midpoints" : [ 610.649928778409958, 613.000000953674316, 610.649928778409958, 613.000000953674316 ],
					"source" : [ "obj-5", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-25", 0 ],
					"midpoints" : [ 692.649928778409958, 613.000000953674316, 692.649928778409958, 613.000000953674316 ],
					"source" : [ "obj-5", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-5", 0 ],
					"source" : [ "obj-52", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-259", 0 ],
					"order" : 1,
					"source" : [ "obj-59", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-28", 0 ],
					"order" : 0,
					"source" : [ "obj-59", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-10", 0 ],
					"source" : [ "obj-6", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-13", 0 ],
					"source" : [ "obj-7", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-5", 1 ],
					"midpoints" : [ 692.649928778409958, 580.000000953674316, 692.649928778409958, 580.000000953674316 ],
					"source" : [ "obj-79", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-17", 0 ],
					"source" : [ "obj-8", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-143", 0 ],
					"source" : [ "obj-82", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-113", 0 ],
					"midpoints" : [ 637.963439717888946, 1462.000000953674316, 548.696765080094451, 1462.000000953674316 ],
					"source" : [ "obj-83", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-165", 2 ],
					"source" : [ "obj-85", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-142", 0 ],
					"source" : [ "obj-87", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-102", 0 ],
					"midpoints" : [ 610.649928778409958, 769.000000953674316, 610.649928778409958, 769.000000953674316 ],
					"source" : [ "obj-89", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-8", 0 ],
					"source" : [ "obj-9", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-11", 1 ],
					"midpoints" : [ 706.649928778409958, 697.000000953674316, 624.649928778409958, 697.000000953674316 ],
					"order" : 1,
					"source" : [ "obj-94", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-31", 1 ],
					"midpoints" : [ 706.649928778409958, 688.000000953674316, 706.649928778409958, 688.000000953674316 ],
					"order" : 0,
					"source" : [ "obj-94", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-146", 0 ],
					"source" : [ "obj-96", 0 ]
				}

			}
 ],
		"parameters" : 		{
			"obj-12::obj-107::obj-33" : [ "tab[110]", "tab[1]", 0 ],
			"obj-12::obj-123::obj-33" : [ "tab[108]", "tab[1]", 0 ],
			"obj-12::obj-34::obj-33" : [ "tab[111]", "tab[1]", 0 ],
			"obj-12::obj-36::obj-33" : [ "tab[112]", "tab[1]", 0 ],
			"obj-12::obj-40::obj-33" : [ "tab[117]", "tab[1]", 0 ],
			"obj-12::obj-41::obj-33" : [ "tab[118]", "tab[1]", 0 ],
			"obj-12::obj-42::obj-33" : [ "tab[119]", "tab[1]", 0 ],
			"obj-12::obj-43::obj-33" : [ "tab[120]", "tab[1]", 0 ],
			"obj-12::obj-44::obj-33" : [ "tab[121]", "tab[1]", 0 ],
			"obj-12::obj-45::obj-33" : [ "tab[122]", "tab[1]", 0 ],
			"obj-12::obj-46::obj-33" : [ "tab[123]", "tab[1]", 0 ],
			"obj-12::obj-47::obj-33" : [ "tab[124]", "tab[1]", 0 ],
			"obj-12::obj-48::obj-33" : [ "tab[125]", "tab[1]", 0 ],
			"obj-12::obj-49::obj-33" : [ "tab[126]", "tab[1]", 0 ],
			"obj-12::obj-50::obj-33" : [ "tab[127]", "tab[1]", 0 ],
			"obj-12::obj-74::obj-33" : [ "tab[109]", "tab[1]", 0 ],
			"obj-160::obj-101" : [ "number[7]", "number[7]", 0 ],
			"obj-160::obj-104" : [ "number[5]", "number[5]", 0 ],
			"obj-160::obj-109" : [ "number[6]", "number[6]", 0 ],
			"obj-160::obj-113" : [ "number[9]", "number[9]", 0 ],
			"obj-160::obj-118" : [ "number[2]", "number[2]", 0 ],
			"obj-160::obj-123" : [ "number[3]", "number[3]", 0 ],
			"obj-160::obj-28" : [ "number[12]", "number[12]", 0 ],
			"obj-160::obj-43" : [ "number[14]", "number[4]", 0 ],
			"obj-160::obj-44" : [ "number[13]", "number[13]", 0 ],
			"obj-160::obj-61" : [ "number[15]", "number[9]", 0 ],
			"obj-160::obj-68" : [ "number[4]", "number[4]", 0 ],
			"obj-160::obj-72" : [ "number[11]", "number[11]", 0 ],
			"obj-160::obj-77" : [ "number[10]", "number[10]", 0 ],
			"obj-160::obj-90" : [ "number", "number", 0 ],
			"obj-160::obj-93" : [ "number[8]", "number[8]", 0 ],
			"obj-160::obj-97" : [ "number[1]", "number[1]", 0 ],
			"obj-162::obj-107::obj-27::obj-18" : [ "toggle[17]", "toggle", 0 ],
			"obj-162::obj-107::obj-48" : [ "SendTo-TXT[17]", "SendTo-TXT", 0 ],
			"obj-162::obj-107::obj-8" : [ "tab[94]", "tab[1]", 0 ],
			"obj-162::obj-123::obj-27::obj-18" : [ "toggle[64]", "toggle", 0 ],
			"obj-162::obj-123::obj-48" : [ "SendTo-TXT[43]", "SendTo-TXT", 0 ],
			"obj-162::obj-123::obj-8" : [ "tab[141]", "tab[1]", 0 ],
			"obj-162::obj-34::obj-27::obj-18" : [ "toggle[18]", "toggle", 0 ],
			"obj-162::obj-34::obj-48" : [ "SendTo-TXT[18]", "SendTo-TXT", 0 ],
			"obj-162::obj-34::obj-8" : [ "tab[95]", "tab[1]", 0 ],
			"obj-162::obj-36::obj-27::obj-18" : [ "toggle[19]", "toggle", 0 ],
			"obj-162::obj-36::obj-48" : [ "SendTo-TXT[19]", "SendTo-TXT", 0 ],
			"obj-162::obj-36::obj-8" : [ "tab[96]", "tab[1]", 0 ],
			"obj-162::obj-40::obj-27::obj-18" : [ "toggle[20]", "toggle", 0 ],
			"obj-162::obj-40::obj-48" : [ "SendTo-TXT[20]", "SendTo-TXT", 0 ],
			"obj-162::obj-40::obj-8" : [ "tab[97]", "tab[1]", 0 ],
			"obj-162::obj-41::obj-27::obj-18" : [ "toggle[21]", "toggle", 0 ],
			"obj-162::obj-41::obj-48" : [ "SendTo-TXT[21]", "SendTo-TXT", 0 ],
			"obj-162::obj-41::obj-8" : [ "tab[98]", "tab[1]", 0 ],
			"obj-162::obj-42::obj-27::obj-18" : [ "toggle[22]", "toggle", 0 ],
			"obj-162::obj-42::obj-48" : [ "SendTo-TXT[22]", "SendTo-TXT", 0 ],
			"obj-162::obj-42::obj-8" : [ "tab[99]", "tab[1]", 0 ],
			"obj-162::obj-43::obj-27::obj-18" : [ "toggle[23]", "toggle", 0 ],
			"obj-162::obj-43::obj-48" : [ "SendTo-TXT[23]", "SendTo-TXT", 0 ],
			"obj-162::obj-43::obj-8" : [ "tab[100]", "tab[1]", 0 ],
			"obj-162::obj-44::obj-27::obj-18" : [ "toggle[24]", "toggle", 0 ],
			"obj-162::obj-44::obj-48" : [ "SendTo-TXT[24]", "SendTo-TXT", 0 ],
			"obj-162::obj-44::obj-8" : [ "tab[101]", "tab[1]", 0 ],
			"obj-162::obj-45::obj-27::obj-18" : [ "toggle[25]", "toggle", 0 ],
			"obj-162::obj-45::obj-48" : [ "SendTo-TXT[25]", "SendTo-TXT", 0 ],
			"obj-162::obj-45::obj-8" : [ "tab[102]", "tab[1]", 0 ],
			"obj-162::obj-46::obj-27::obj-18" : [ "toggle[26]", "toggle", 0 ],
			"obj-162::obj-46::obj-48" : [ "SendTo-TXT[44]", "SendTo-TXT", 0 ],
			"obj-162::obj-46::obj-8" : [ "tab[103]", "tab[1]", 0 ],
			"obj-162::obj-47::obj-27::obj-18" : [ "toggle[27]", "toggle", 0 ],
			"obj-162::obj-47::obj-48" : [ "SendTo-TXT[45]", "SendTo-TXT", 0 ],
			"obj-162::obj-47::obj-8" : [ "tab[104]", "tab[1]", 0 ],
			"obj-162::obj-48::obj-27::obj-18" : [ "toggle[28]", "toggle", 0 ],
			"obj-162::obj-48::obj-48" : [ "SendTo-TXT[46]", "SendTo-TXT", 0 ],
			"obj-162::obj-48::obj-8" : [ "tab[105]", "tab[1]", 0 ],
			"obj-162::obj-49::obj-27::obj-18" : [ "toggle[29]", "toggle", 0 ],
			"obj-162::obj-49::obj-48" : [ "SendTo-TXT[47]", "SendTo-TXT", 0 ],
			"obj-162::obj-49::obj-8" : [ "tab[106]", "tab[1]", 0 ],
			"obj-162::obj-50::obj-27::obj-18" : [ "toggle[30]", "toggle", 0 ],
			"obj-162::obj-50::obj-48" : [ "SendTo-TXT[48]", "SendTo-TXT", 0 ],
			"obj-162::obj-50::obj-8" : [ "tab[107]", "tab[1]", 0 ],
			"obj-162::obj-74::obj-27::obj-18" : [ "toggle[16]", "toggle", 0 ],
			"obj-162::obj-74::obj-48" : [ "SendTo-TXT[16]", "SendTo-TXT", 0 ],
			"obj-162::obj-74::obj-8" : [ "tab[93]", "tab[1]", 0 ],
			"obj-163::obj-107::obj-33" : [ "tab[79]", "tab[1]", 0 ],
			"obj-163::obj-123::obj-33" : [ "tab[116]", "tab[1]", 0 ],
			"obj-163::obj-34::obj-33" : [ "tab[80]", "tab[1]", 0 ],
			"obj-163::obj-36::obj-33" : [ "tab[81]", "tab[1]", 0 ],
			"obj-163::obj-40::obj-33" : [ "tab[82]", "tab[1]", 0 ],
			"obj-163::obj-41::obj-33" : [ "tab[83]", "tab[1]", 0 ],
			"obj-163::obj-42::obj-33" : [ "tab[84]", "tab[1]", 0 ],
			"obj-163::obj-43::obj-33" : [ "tab[85]", "tab[1]", 0 ],
			"obj-163::obj-44::obj-33" : [ "tab[86]", "tab[1]", 0 ],
			"obj-163::obj-45::obj-33" : [ "tab[87]", "tab[1]", 0 ],
			"obj-163::obj-46::obj-33" : [ "tab[88]", "tab[1]", 0 ],
			"obj-163::obj-47::obj-33" : [ "tab[89]", "tab[1]", 0 ],
			"obj-163::obj-48::obj-33" : [ "tab[90]", "tab[1]", 0 ],
			"obj-163::obj-49::obj-33" : [ "tab[91]", "tab[1]", 0 ],
			"obj-163::obj-50::obj-33" : [ "tab[92]", "tab[1]", 0 ],
			"obj-163::obj-74::obj-33" : [ "tab[78]", "tab[1]", 0 ],
			"obj-165::obj-25" : [ "live.numbox[3]", "live.numbox", 0 ],
			"obj-165::obj-96" : [ "live.numbox[4]", "live.numbox", 0 ],
			"obj-227" : [ "incdec", "incdec", 0 ],
			"obj-36::obj-107::obj-27::obj-18" : [ "toggle[33]", "toggle", 0 ],
			"obj-36::obj-107::obj-48" : [ "SendTo-TXT[28]", "SendTo-TXT", 0 ],
			"obj-36::obj-107::obj-8" : [ "tab[130]", "tab[1]", 0 ],
			"obj-36::obj-123::obj-27::obj-18" : [ "toggle[31]", "toggle", 0 ],
			"obj-36::obj-123::obj-48" : [ "SendTo-TXT[26]", "SendTo-TXT", 0 ],
			"obj-36::obj-123::obj-8" : [ "tab[128]", "tab[1]", 0 ],
			"obj-36::obj-34::obj-27::obj-18" : [ "toggle[34]", "toggle", 0 ],
			"obj-36::obj-34::obj-48" : [ "SendTo-TXT[29]", "SendTo-TXT", 0 ],
			"obj-36::obj-34::obj-8" : [ "tab[113]", "tab[1]", 0 ],
			"obj-36::obj-36::obj-27::obj-18" : [ "toggle[35]", "toggle", 0 ],
			"obj-36::obj-36::obj-48" : [ "SendTo-TXT[49]", "SendTo-TXT", 0 ],
			"obj-36::obj-36::obj-8" : [ "tab[114]", "tab[1]", 0 ],
			"obj-36::obj-40::obj-27::obj-18" : [ "toggle[36]", "toggle", 0 ],
			"obj-36::obj-40::obj-48" : [ "SendTo-TXT[30]", "SendTo-TXT", 0 ],
			"obj-36::obj-40::obj-8" : [ "tab[131]", "tab[1]", 0 ],
			"obj-36::obj-41::obj-27::obj-18" : [ "toggle[37]", "toggle", 0 ],
			"obj-36::obj-41::obj-48" : [ "SendTo-TXT[50]", "SendTo-TXT", 0 ],
			"obj-36::obj-41::obj-8" : [ "tab[132]", "tab[1]", 0 ],
			"obj-36::obj-42::obj-27::obj-18" : [ "toggle[38]", "toggle", 0 ],
			"obj-36::obj-42::obj-48" : [ "SendTo-TXT[51]", "SendTo-TXT", 0 ],
			"obj-36::obj-42::obj-8" : [ "tab[133]", "tab[1]", 0 ],
			"obj-36::obj-43::obj-27::obj-18" : [ "toggle[39]", "toggle", 0 ],
			"obj-36::obj-43::obj-48" : [ "SendTo-TXT[31]", "SendTo-TXT", 0 ],
			"obj-36::obj-43::obj-8" : [ "tab[134]", "tab[1]", 0 ],
			"obj-36::obj-44::obj-27::obj-18" : [ "toggle[40]", "toggle", 0 ],
			"obj-36::obj-44::obj-48" : [ "SendTo-TXT[52]", "SendTo-TXT", 0 ],
			"obj-36::obj-44::obj-8" : [ "tab[135]", "tab[1]", 0 ],
			"obj-36::obj-45::obj-27::obj-18" : [ "toggle[41]", "toggle", 0 ],
			"obj-36::obj-45::obj-48" : [ "SendTo-TXT[53]", "SendTo-TXT", 0 ],
			"obj-36::obj-45::obj-8" : [ "tab[136]", "tab[1]", 0 ],
			"obj-36::obj-46::obj-27::obj-18" : [ "toggle[42]", "toggle", 0 ],
			"obj-36::obj-46::obj-48" : [ "SendTo-TXT[32]", "SendTo-TXT", 0 ],
			"obj-36::obj-46::obj-8" : [ "tab[137]", "tab[1]", 0 ],
			"obj-36::obj-47::obj-27::obj-18" : [ "toggle[43]", "toggle", 0 ],
			"obj-36::obj-47::obj-48" : [ "SendTo-TXT[54]", "SendTo-TXT", 0 ],
			"obj-36::obj-47::obj-8" : [ "tab[138]", "tab[1]", 0 ],
			"obj-36::obj-48::obj-27::obj-18" : [ "toggle[65]", "toggle", 0 ],
			"obj-36::obj-48::obj-48" : [ "SendTo-TXT[33]", "SendTo-TXT", 0 ],
			"obj-36::obj-48::obj-8" : [ "tab[139]", "tab[1]", 0 ],
			"obj-36::obj-49::obj-27::obj-18" : [ "toggle[66]", "toggle", 0 ],
			"obj-36::obj-49::obj-48" : [ "SendTo-TXT[55]", "SendTo-TXT", 0 ],
			"obj-36::obj-49::obj-8" : [ "tab[142]", "tab[1]", 0 ],
			"obj-36::obj-50::obj-27::obj-18" : [ "toggle[67]", "toggle", 0 ],
			"obj-36::obj-50::obj-48" : [ "SendTo-TXT[56]", "SendTo-TXT", 0 ],
			"obj-36::obj-50::obj-8" : [ "tab[140]", "tab[1]", 0 ],
			"obj-36::obj-74::obj-27::obj-18" : [ "toggle[32]", "toggle", 0 ],
			"obj-36::obj-74::obj-48" : [ "SendTo-TXT[27]", "SendTo-TXT", 0 ],
			"obj-36::obj-74::obj-8" : [ "tab[129]", "tab[1]", 0 ],
			"obj-59::obj-107::obj-33" : [ "tab[160]", "tab[1]", 0 ],
			"obj-59::obj-123::obj-33" : [ "tab[158]", "tab[1]", 0 ],
			"obj-59::obj-34::obj-33" : [ "tab[161]", "tab[1]", 0 ],
			"obj-59::obj-36::obj-33" : [ "tab[162]", "tab[1]", 0 ],
			"obj-59::obj-40::obj-33" : [ "tab[163]", "tab[1]", 0 ],
			"obj-59::obj-41::obj-33" : [ "tab[164]", "tab[1]", 0 ],
			"obj-59::obj-42::obj-33" : [ "tab[165]", "tab[1]", 0 ],
			"obj-59::obj-43::obj-33" : [ "tab[166]", "tab[1]", 0 ],
			"obj-59::obj-44::obj-33" : [ "tab[167]", "tab[1]", 0 ],
			"obj-59::obj-45::obj-33" : [ "tab[168]", "tab[1]", 0 ],
			"obj-59::obj-46::obj-33" : [ "tab[169]", "tab[1]", 0 ],
			"obj-59::obj-47::obj-33" : [ "tab[170]", "tab[1]", 0 ],
			"obj-59::obj-48::obj-33" : [ "tab[171]", "tab[1]", 0 ],
			"obj-59::obj-49::obj-33" : [ "tab[172]", "tab[1]", 0 ],
			"obj-59::obj-50::obj-33" : [ "tab[173]", "tab[1]", 0 ],
			"obj-59::obj-74::obj-33" : [ "tab[159]", "tab[1]", 0 ],
			"parameterbanks" : 			{
				"0" : 				{
					"index" : 0,
					"name" : "",
					"parameters" : [ "-", "-", "-", "-", "-", "-", "-", "-" ]
				}

			}
,
			"inherited_shortname" : 1
		}
,
		"dependency_cache" : [ 			{
				"name" : "AlainJoystick2~.mxo",
				"type" : "iLaX"
			}
, 			{
				"name" : "OSC-route.mxo",
				"type" : "iLaX"
			}
, 			{
				"name" : "abc_2d_decoder3_4~.mxo",
				"type" : "iLaX"
			}
, 			{
				"name" : "abc_2d_encoder3~.mxo",
				"type" : "iLaX"
			}
, 			{
				"name" : "abc_2d_fx_decorrelation3~.mxo",
				"type" : "iLaX"
			}
, 			{
				"name" : "abc_gaincontrol.maxpat",
				"bootpath" : "~/Documents/Max 8/Packages/abclib 2/misc/others",
				"patcherrelativepath" : "../../Max 8/Packages/abclib 2/misc/others",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bbdmi.2max.maxpat",
				"bootpath" : "~/Documents/GitLab/bbdmi/max/output/2max",
				"patcherrelativepath" : "../../GitLab/bbdmi/max/output/2max",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bbdmi.calibrate.maxpat",
				"bootpath" : "~/Documents/GitLab/bbdmi/max/control_processing/calibrate",
				"patcherrelativepath" : "../../GitLab/bbdmi/max/control_processing/calibrate",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bbdmi.crosspatch.maxpat",
				"bootpath" : "~/Documents/GitLab/bbdmi/max/utilities/crosspatch",
				"patcherrelativepath" : "../../GitLab/bbdmi/max/utilities/crosspatch",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bbdmi.multislider.maxpat",
				"bootpath" : "~/Documents/GitLab/bbdmi/max/utilities/multislider",
				"patcherrelativepath" : "../../GitLab/bbdmi/max/utilities/multislider",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bbdmi.regress.maxpat",
				"bootpath" : "~/Documents/GitLab/bbdmi/max/control_processing/regress",
				"patcherrelativepath" : "../../GitLab/bbdmi/max/control_processing/regress",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bbdmi.scale.maxpat",
				"bootpath" : "~/Documents/GitLab/bbdmi/max/control_processing/scale",
				"patcherrelativepath" : "../../GitLab/bbdmi/max/control_processing/scale",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bbdmi.timethresh.maxpat",
				"bootpath" : "~/Documents/BBDMI/Semaine_Cerveau_ICM",
				"patcherrelativepath" : ".",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bbdmi_live_granulator~.maxpat",
				"bootpath" : "~/Documents/GitLab/bbdmi/max/sound_synthesis/live_granulator~",
				"patcherrelativepath" : "../../GitLab/bbdmi/max/sound_synthesis/live_granulator~",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bbdmi_multi_granulator14~.mxo",
				"type" : "iLaX"
			}
, 			{
				"name" : "faustgen~.mxo",
				"type" : "iLaX"
			}
, 			{
				"name" : "list-interpolate.mxo",
				"type" : "iLaX"
			}
, 			{
				"name" : "mxj.mxo",
				"type" : "iLaX"
			}
, 			{
				"name" : "name.js",
				"bootpath" : "~/Documents/GitLab/bbdmi/max/source/js",
				"patcherrelativepath" : "../../GitLab/bbdmi/max/source/js",
				"type" : "TEXT",
				"implicit" : 1
			}
, 			{
				"name" : "p.2max.maxpat",
				"bootpath" : "~/Documents/GitLab/bbdmi/max/output/2max",
				"patcherrelativepath" : "../../GitLab/bbdmi/max/output/2max",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "p.calibrate.maxpat",
				"bootpath" : "~/Documents/GitLab/bbdmi/max/control_processing/calibrate",
				"patcherrelativepath" : "../../GitLab/bbdmi/max/control_processing/calibrate",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "p.exposer.maxpat",
				"bootpath" : "~/Documents/GitLab/bbdmi/max/source/patchers",
				"patcherrelativepath" : "../../GitLab/bbdmi/max/source/patchers",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "p.scale.maxpat",
				"bootpath" : "~/Documents/GitLab/bbdmi/max/control_processing/scale",
				"patcherrelativepath" : "../../GitLab/bbdmi/max/control_processing/scale",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "preset.json",
				"bootpath" : "~/Documents/BBDMI/Semaine_Cerveau_ICM/presets",
				"patcherrelativepath" : "./presets",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "rapid.regression.mxo",
				"type" : "iLaX"
			}
 ],
		"autosave" : 0,
		"textcolor" : [ 1.0, 1.0, 1.0, 1.0 ],
		"bgcolor" : [ 0.27843137254902, 0.27843137254902, 0.27843137254902, 1.0 ]
	}

}
