{
	"patcher" : 	{
		"fileversion" : 1,
		"appversion" : 		{
			"major" : 8,
			"minor" : 5,
			"revision" : 6,
			"architecture" : "x64",
			"modernui" : 1
		}
,
		"classnamespace" : "box",
		"rect" : [ 0.0, 53.0, 1239.0, 737.0 ],
		"openrect" : [ 0.0, 0.0, 1239.0, 737.0 ],
		"bglocked" : 0,
		"openinpresentation" : 1,
		"default_fontsize" : 12.0,
		"default_fontface" : 0,
		"default_fontname" : "Arial",
		"gridonopen" : 1,
		"gridsize" : [ 15.0, 15.0 ],
		"gridsnaponopen" : 1,
		"objectsnaponopen" : 1,
		"statusbarvisible" : 2,
		"toolbarvisible" : 1,
		"lefttoolbarpinned" : 2,
		"toptoolbarpinned" : 2,
		"righttoolbarpinned" : 0,
		"bottomtoolbarpinned" : 0,
		"toolbars_unpinned_last_save" : 3,
		"tallnewobj" : 0,
		"boxanimatetime" : 200,
		"enablehscroll" : 1,
		"enablevscroll" : 1,
		"devicewidth" : 0.0,
		"description" : "",
		"digest" : "",
		"tags" : "",
		"style" : "",
		"subpatcher_template" : "Default Max 7",
		"assistshowspatchername" : 0,
		"boxes" : [ 			{
				"box" : 				{
					"id" : "obj-142",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 2271.5, 1117.48721609115637, 71.0, 22.0 ],
					"text" : "routepass 1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-141",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2358.0, 1117.48721609115637, 87.0, 22.0 ],
					"text" : "r enable_lifting"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-138",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"patching_rect" : [ 2107.63958740234375, 1278.638599228859448, 22.0, 22.0 ],
					"text" : "t 1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-136",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 738.0, 384.975012207031796, 71.0, 22.0 ],
					"text" : "r lifting_feat"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-135",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 2108.0, 1320.546195697784924, 73.0, 22.0 ],
					"text" : "s lifting_feat"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-134",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"patching_rect" : [ 2219.5, 1210.0, 22.0, 22.0 ],
					"text" : "t 0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-132",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"patching_rect" : [ 2237.0, 1210.0, 22.0, 22.0 ],
					"text" : "t 1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-130",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 2237.0, 1252.964661788940703, 89.0, 22.0 ],
					"text" : "s lifting_master"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-128",
					"maxclass" : "toggle",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 738.0, 464.237500000000637, 24.0, 24.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-123",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 738.0, 515.671293541364321, 102.0, 22.0 ],
					"text" : "gate"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-122",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1800.63958740234375, 1011.812358045578549, 61.0, 22.0 ],
					"text" : "pipe 1500"
				}

			}
, 			{
				"box" : 				{
					"fontface" : 1,
					"fontsize" : 30.0,
					"id" : "obj-121",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1670.548413902521133, 903.0, 80.0, 40.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1069.321273821794421, 34.862348185266598, 80.0, 40.0 ],
					"text" : "reset"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-119",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1636.0, 956.0, 79.0, 22.0 ],
					"text" : "s reset_lifting"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-113",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1636.0, 911.0, 24.0, 24.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 996.0, 22.862348185266598, 64.0, 64.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontface" : 1,
					"fontsize" : 40.0,
					"id" : "obj-110",
					"maxclass" : "number",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1800.63958740234375, 952.243608045578185, 99.0, 53.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 996.0, 96.0, 64.0, 53.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontface" : 0,
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-226",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1800.63958740234375, 911.0, 55.0, 22.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1069.321273821794421, 112.0, 59.0, 22.0 ],
					"text" : "r gigstep"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-112",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "signal" ],
					"patching_rect" : [ 2190.63958740234375, 758.0, 74.0, 22.0 ],
					"text" : "mc.unpack~"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-107",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "signal" ],
					"patching_rect" : [ 2107.63958740234375, 758.0, 74.0, 22.0 ],
					"text" : "mc.unpack~"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-70",
					"maxclass" : "newobj",
					"numinlets" : 4,
					"numoutlets" : 1,
					"outlettype" : [ "multichannelsignal" ],
					"patching_rect" : [ 2107.63958740234375, 839.0, 157.0, 22.0 ],
					"text" : "mc.pack~ 4"
				}

			}
, 			{
				"box" : 				{
					"fontsize" : 18.0,
					"id" : "obj-139",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patcher" : 					{
						"fileversion" : 1,
						"appversion" : 						{
							"major" : 8,
							"minor" : 5,
							"revision" : 6,
							"architecture" : "x64",
							"modernui" : 1
						}
,
						"classnamespace" : "box",
						"rect" : [ 453.0, 380.0, 765.0, 453.0 ],
						"bglocked" : 0,
						"openinpresentation" : 0,
						"default_fontsize" : 12.0,
						"default_fontface" : 0,
						"default_fontname" : "Arial",
						"gridonopen" : 1,
						"gridsize" : [ 15.0, 15.0 ],
						"gridsnaponopen" : 1,
						"objectsnaponopen" : 1,
						"statusbarvisible" : 2,
						"toolbarvisible" : 1,
						"lefttoolbarpinned" : 0,
						"toptoolbarpinned" : 0,
						"righttoolbarpinned" : 0,
						"bottomtoolbarpinned" : 0,
						"toolbars_unpinned_last_save" : 0,
						"tallnewobj" : 0,
						"boxanimatetime" : 200,
						"enablehscroll" : 1,
						"enablevscroll" : 1,
						"devicewidth" : 0.0,
						"description" : "",
						"digest" : "",
						"tags" : "",
						"style" : "",
						"subpatcher_template" : "Default Max 7",
						"assistshowspatchername" : 0,
						"boxes" : [ 							{
								"box" : 								{
									"id" : "obj-31",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 4,
									"outlettype" : [ "signal", "signal", "signal", "signal" ],
									"patching_rect" : [ 123.125, 14.0, 84.0, 22.0 ],
									"text" : "mc.unpack~ 4"
								}

							}
, 							{
								"box" : 								{
									"bgcolor" : [ 1.0, 1.0, 1.0, 1.0 ],
									"id" : "obj-24",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 604.267857142857338, 146.5, 63.0, 20.0 ],
									"text" : "(degrees)"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-25",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 552.267857142857338, 105.5, 87.0, 22.0 ],
									"text" : "loadmess 145."
								}

							}
, 							{
								"box" : 								{
									"format" : 6,
									"id" : "obj-26",
									"maxclass" : "flonum",
									"maximum" : 360.0,
									"minimum" : -360.0,
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 552.267857142857338, 146.5, 50.0, 22.0 ]
								}

							}
, 							{
								"box" : 								{
									"bgcolor" : [ 0.301961, 0.301961, 0.301961, 1.0 ],
									"bgcolor2" : [ 0.301961, 0.301961, 0.301961, 1.0 ],
									"bgfillcolor_angle" : 270.0,
									"bgfillcolor_autogradient" : 0.0,
									"bgfillcolor_color" : [ 0.498039215686275, 0.498039215686275, 0.498039215686275, 1.0 ],
									"bgfillcolor_color1" : [ 0.301961, 0.301961, 0.301961, 1.0 ],
									"bgfillcolor_color2" : [ 0.2, 0.2, 0.2, 1.0 ],
									"bgfillcolor_proportion" : 0.5,
									"bgfillcolor_type" : "color",
									"gradient" : 1,
									"id" : "obj-27",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 552.267857142857338, 181.5, 55.0, 22.0 ],
									"text" : "angle $1"
								}

							}
, 							{
								"box" : 								{
									"fontface" : 1,
									"fontsize" : 14.0,
									"id" : "obj-28",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 8,
									"outlettype" : [ "signal", "signal", "signal", "signal", "signal", "signal", "signal", "list" ],
									"patching_rect" : [ 552.267857142857338, 215.5, 140.999999999999773, 24.0 ],
									"text" : "abc_2d_encoder3~",
									"textcolor" : [ 0.968627450980392, 0.968627450980392, 0.968627450980392, 1.0 ]
								}

							}
, 							{
								"box" : 								{
									"bgcolor" : [ 1.0, 1.0, 1.0, 1.0 ],
									"id" : "obj-19",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 444.267857142857338, 146.5, 63.0, 20.0 ],
									"text" : "(degrees)"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-20",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 392.267857142857338, 105.5, 80.0, 22.0 ],
									"text" : "loadmess 50."
								}

							}
, 							{
								"box" : 								{
									"format" : 6,
									"id" : "obj-21",
									"maxclass" : "flonum",
									"maximum" : 360.0,
									"minimum" : -360.0,
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 392.267857142857338, 146.5, 50.0, 22.0 ]
								}

							}
, 							{
								"box" : 								{
									"bgcolor" : [ 0.301961, 0.301961, 0.301961, 1.0 ],
									"bgcolor2" : [ 0.301961, 0.301961, 0.301961, 1.0 ],
									"bgfillcolor_angle" : 270.0,
									"bgfillcolor_autogradient" : 0.0,
									"bgfillcolor_color" : [ 0.498039215686275, 0.498039215686275, 0.498039215686275, 1.0 ],
									"bgfillcolor_color1" : [ 0.301961, 0.301961, 0.301961, 1.0 ],
									"bgfillcolor_color2" : [ 0.2, 0.2, 0.2, 1.0 ],
									"bgfillcolor_proportion" : 0.5,
									"bgfillcolor_type" : "color",
									"gradient" : 1,
									"id" : "obj-22",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 392.267857142857338, 181.5, 55.0, 22.0 ],
									"text" : "angle $1"
								}

							}
, 							{
								"box" : 								{
									"fontface" : 1,
									"fontsize" : 14.0,
									"id" : "obj-23",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 8,
									"outlettype" : [ "signal", "signal", "signal", "signal", "signal", "signal", "signal", "list" ],
									"patching_rect" : [ 392.267857142857338, 215.5, 140.999999999999773, 24.0 ],
									"text" : "abc_2d_encoder3~",
									"textcolor" : [ 0.968627450980392, 0.968627450980392, 0.968627450980392, 1.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-9",
									"index" : 1,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "multichannelsignal" ],
									"patching_rect" : [ 123.125, -21.0, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"bgcolor" : [ 1.0, 1.0, 1.0, 1.0 ],
									"id" : "obj-2",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 286.267857142857338, 146.5, 63.0, 20.0 ],
									"text" : "(degrees)"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-3",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 234.267857142857338, 105.5, 80.0, 22.0 ],
									"text" : "loadmess 30."
								}

							}
, 							{
								"box" : 								{
									"format" : 6,
									"id" : "obj-4",
									"maxclass" : "flonum",
									"maximum" : 360.0,
									"minimum" : -360.0,
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 234.267857142857338, 146.5, 50.0, 22.0 ]
								}

							}
, 							{
								"box" : 								{
									"bgcolor" : [ 0.301961, 0.301961, 0.301961, 1.0 ],
									"bgcolor2" : [ 0.301961, 0.301961, 0.301961, 1.0 ],
									"bgfillcolor_angle" : 270.0,
									"bgfillcolor_autogradient" : 0.0,
									"bgfillcolor_color" : [ 0.498039215686275, 0.498039215686275, 0.498039215686275, 1.0 ],
									"bgfillcolor_color1" : [ 0.301961, 0.301961, 0.301961, 1.0 ],
									"bgfillcolor_color2" : [ 0.2, 0.2, 0.2, 1.0 ],
									"bgfillcolor_proportion" : 0.5,
									"bgfillcolor_type" : "color",
									"gradient" : 1,
									"id" : "obj-5",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 234.267857142857338, 181.5, 55.0, 22.0 ],
									"text" : "angle $1"
								}

							}
, 							{
								"box" : 								{
									"fontface" : 1,
									"fontsize" : 14.0,
									"id" : "obj-6",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 8,
									"outlettype" : [ "signal", "signal", "signal", "signal", "signal", "signal", "signal", "list" ],
									"patching_rect" : [ 234.267857142857338, 215.5, 140.999999999999773, 24.0 ],
									"text" : "abc_2d_encoder3~",
									"textcolor" : [ 0.968627450980392, 0.968627450980392, 0.968627450980392, 1.0 ]
								}

							}
, 							{
								"box" : 								{
									"bgcolor" : [ 1.0, 1.0, 1.0, 1.0 ],
									"id" : "obj-10",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 123.125, 146.5, 63.0, 20.0 ],
									"text" : "(degrees)"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-16",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 71.125, 105.5, 81.0, 22.0 ],
									"text" : "loadmess -90"
								}

							}
, 							{
								"box" : 								{
									"format" : 6,
									"id" : "obj-17",
									"maxclass" : "flonum",
									"maximum" : 360.0,
									"minimum" : -360.0,
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 71.125, 146.5, 50.0, 22.0 ]
								}

							}
, 							{
								"box" : 								{
									"bgcolor" : [ 0.301961, 0.301961, 0.301961, 1.0 ],
									"bgcolor2" : [ 0.301961, 0.301961, 0.301961, 1.0 ],
									"bgfillcolor_angle" : 270.0,
									"bgfillcolor_autogradient" : 0.0,
									"bgfillcolor_color" : [ 0.498039215686275, 0.498039215686275, 0.498039215686275, 1.0 ],
									"bgfillcolor_color1" : [ 0.301961, 0.301961, 0.301961, 1.0 ],
									"bgfillcolor_color2" : [ 0.2, 0.2, 0.2, 1.0 ],
									"bgfillcolor_proportion" : 0.5,
									"bgfillcolor_type" : "color",
									"gradient" : 1,
									"id" : "obj-18",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 71.125, 181.5, 55.0, 22.0 ],
									"text" : "angle $1"
								}

							}
, 							{
								"box" : 								{
									"fontface" : 1,
									"fontsize" : 14.0,
									"id" : "obj-1",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 8,
									"outlettype" : [ "signal", "signal", "signal", "signal", "signal", "signal", "signal", "list" ],
									"patching_rect" : [ 71.125, 215.5, 140.999999999999773, 24.0 ],
									"text" : "abc_2d_encoder3~",
									"textcolor" : [ 0.968627450980392, 0.968627450980392, 0.968627450980392, 1.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-134",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 71.125, 489.666666805744171, 92.0, 22.0 ],
									"text" : "mc.send~ lifting"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-137",
									"maxclass" : "newobj",
									"numinlets" : 7,
									"numoutlets" : 1,
									"outlettype" : [ "multichannelsignal" ],
									"patching_rect" : [ 71.125, 434.0, 82.0, 22.0 ],
									"text" : "mc.pack~ 7"
								}

							}
 ],
						"lines" : [ 							{
								"patchline" : 								{
									"destination" : [ "obj-137", 6 ],
									"source" : [ "obj-1", 6 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-137", 5 ],
									"source" : [ "obj-1", 5 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-137", 4 ],
									"source" : [ "obj-1", 4 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-137", 3 ],
									"source" : [ "obj-1", 3 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-137", 2 ],
									"source" : [ "obj-1", 2 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-137", 1 ],
									"source" : [ "obj-1", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-137", 0 ],
									"source" : [ "obj-1", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-134", 0 ],
									"source" : [ "obj-137", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-17", 0 ],
									"source" : [ "obj-16", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-18", 0 ],
									"source" : [ "obj-17", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-1", 0 ],
									"source" : [ "obj-18", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-21", 0 ],
									"source" : [ "obj-20", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-22", 0 ],
									"source" : [ "obj-21", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-23", 0 ],
									"source" : [ "obj-22", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-137", 6 ],
									"source" : [ "obj-23", 6 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-137", 5 ],
									"source" : [ "obj-23", 5 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-137", 4 ],
									"source" : [ "obj-23", 4 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-137", 3 ],
									"source" : [ "obj-23", 3 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-137", 2 ],
									"source" : [ "obj-23", 2 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-137", 1 ],
									"midpoints" : [ 419.196428571428726, 241.0 ],
									"source" : [ "obj-23", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-137", 0 ],
									"source" : [ "obj-23", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-26", 0 ],
									"source" : [ "obj-25", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-27", 0 ],
									"source" : [ "obj-26", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-28", 0 ],
									"source" : [ "obj-27", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-137", 6 ],
									"source" : [ "obj-28", 6 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-137", 5 ],
									"source" : [ "obj-28", 5 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-137", 4 ],
									"source" : [ "obj-28", 4 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-137", 3 ],
									"source" : [ "obj-28", 3 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-137", 2 ],
									"source" : [ "obj-28", 2 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-137", 1 ],
									"midpoints" : [ 579.196428571428783, 242.0 ],
									"source" : [ "obj-28", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-137", 0 ],
									"source" : [ "obj-28", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-4", 0 ],
									"source" : [ "obj-3", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-1", 0 ],
									"source" : [ "obj-31", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-23", 0 ],
									"source" : [ "obj-31", 2 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-28", 0 ],
									"source" : [ "obj-31", 3 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-6", 0 ],
									"source" : [ "obj-31", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-5", 0 ],
									"source" : [ "obj-4", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-6", 0 ],
									"source" : [ "obj-5", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-137", 6 ],
									"source" : [ "obj-6", 6 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-137", 5 ],
									"source" : [ "obj-6", 5 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-137", 4 ],
									"source" : [ "obj-6", 4 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-137", 3 ],
									"source" : [ "obj-6", 3 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-137", 2 ],
									"source" : [ "obj-6", 2 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-137", 1 ],
									"source" : [ "obj-6", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-137", 0 ],
									"source" : [ "obj-6", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-31", 0 ],
									"source" : [ "obj-9", 0 ]
								}

							}
 ]
					}
,
					"patching_rect" : [ 2107.63958740234375, 886.0, 157.0, 29.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 626.519103914499283, 110.0, 157.0, 29.0 ],
					"saved_object_attributes" : 					{
						"description" : "",
						"digest" : "",
						"globalpatchername" : "",
						"tags" : ""
					}
,
					"text" : "p stereoAmbisonic",
					"textcolor" : [ 1.0, 1.0, 1.0, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-51",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "int", "int" ],
					"patching_rect" : [ 1800.63958740234375, 1042.48721609115637, 76.5, 22.0 ],
					"text" : "t i i"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-50",
					"maxclass" : "ezdac~",
					"numinlets" : 2,
					"numoutlets" : 0,
					"patching_rect" : [ 2560.0, 1043.0, 45.0, 45.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-26",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2560.0, 1006.0, 87.0, 22.0 ],
					"text" : "loadmess start"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-60",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 498.0, 359.595428462257132, 37.0, 22.0 ],
					"text" : "s raw"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-84",
					"maxclass" : "newobj",
					"numinlets" : 5,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 389.977111839843474, 279.220833333333644, 82.0, 22.0 ],
					"text" : "zmap 0 1 -1 1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-83",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 389.977111839843474, 250.0, 155.0, 22.0 ],
					"text" : "bbdmi.calibrate 4 @mode 1",
					"varname" : "bbdmi.calibrate[1]"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-108",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1078.0, 241.0, 58.0, 20.0 ],
					"text" : "threshold"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-95",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 898.0, 241.0, 58.0, 20.0 ],
					"text" : "threshold"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-131",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1026.0, 194.0, 80.0, 22.0 ],
					"text" : "loadmess 0.6"
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.2, 0.2, 0.2, 0.0 ],
					"format" : 6,
					"id" : "obj-88",
					"maxclass" : "flonum",
					"maximum" : 1.0,
					"minimum" : 0.0,
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1026.0, 240.0, 50.0, 22.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 753.277258634567261, 255.014574918746519, 50.0, 22.0 ]
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.2, 0.2, 0.2, 0.0 ],
					"format" : 6,
					"id" : "obj-81",
					"maxclass" : "flonum",
					"maximum" : 1.0,
					"minimum" : 0.0,
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 845.0, 240.0, 50.0, 22.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 753.277258634567261, 187.909657329320908, 50.0, 22.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-10",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 378.0, 1008.887500000000728, 41.0, 22.0 ],
					"text" : "s emg"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-129",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 29.0, 1104.359187364578702, 114.0, 22.0 ],
					"text" : "mc.send~ raw_emg"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-114",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1037.821273821794421, 467.796099903754111, 61.0, 22.0 ],
					"text" : "s features"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-35",
					"linecount" : 6,
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 960.821273821794421, 934.887500000000728, 49.0, 87.0 ],
					"text" : "1 - 1\n2 - 3\n3 - 5\n4 - 7\n5 - 2, 6\n6 - 4, 8 "
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-125",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "", "int", "int" ],
					"patching_rect" : [ 1002.0, 308.441666666667288, 48.0, 22.0 ],
					"text" : "change"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-126",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"patching_rect" : [ 1002.0, 280.775000000000546, 43.0, 22.0 ],
					"text" : ">= 0.6"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-115",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 2670.0, 1436.536944985389709, 122.0, 20.0 ],
					"text" : "Francesco Di Maggio"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-106",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 2405.13958740234375, 400.936932182312489, 101.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 887.0, 414.509374999999977, 101.0, 20.0 ],
					"text" : "GRANULATOR 2"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-104",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 2412.13958740234375, 26.974432182312256, 101.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 256.0, 414.509374999999977, 101.0, 20.0 ],
					"text" : "GRANULATOR 1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-103",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1075.477111839843474, 1116.640424813542495, 87.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 836.0, 362.536437296866211, 87.0, 20.0 ],
					"text" : "GRAN 2 - gain",
					"textcolor" : [ 1.0, 1.0, 1.0, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-102",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1075.477111839843474, 1091.822337409427973, 100.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 836.0, 338.090755778266271, 100.0, 20.0 ],
					"text" : "GRAN 2 - stretch",
					"textcolor" : [ 1.0, 1.0, 1.0, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-101",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1075.477111839843474, 1067.004250005312997, 132.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 836.0, 313.64507425966633, 132.0, 20.0 ],
					"text" : "GRAN 2 - transposition",
					"textcolor" : [ 1.0, 1.0, 1.0, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-100",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1075.477111839843474, 1042.186162601198475, 94.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 836.0, 289.19939274106639, 94.0, 20.0 ],
					"text" : "GRAN 2 - mode",
					"textcolor" : [ 1.0, 1.0, 1.0, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-99",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1074.477111839843474, 1017.368075197083726, 87.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 836.0, 264.75371122246645, 87.0, 20.0 ],
					"text" : "GRAN 1 - gain",
					"textcolor" : [ 1.0, 1.0, 1.0, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-98",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1074.477111839843474, 992.549987792968977, 100.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 836.0, 240.308029703866509, 100.0, 20.0 ],
					"text" : "GRAN 1 - stretch",
					"textcolor" : [ 1.0, 1.0, 1.0, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-96",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1074.477111839843474, 967.731900388854456, 132.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 836.0, 215.862348185266598, 132.0, 20.0 ],
					"text" : "GRAN 1 - transposition",
					"textcolor" : [ 1.0, 1.0, 1.0, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-76",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1074.477111839843474, 942.913812984739707, 94.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 836.0, 189.0, 94.0, 20.0 ],
					"text" : "GRAN 1 - mode",
					"textcolor" : [ 1.0, 1.0, 1.0, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-75",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1318.0, 516.671293541364093, 100.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 633.519103914499283, 356.536437296866211, 100.0, 20.0 ],
					"text" : "#3-4 - AVERAGE",
					"textcolor" : [ 1.0, 1.0, 1.0, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-49",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1318.0, 492.184554401807191, 105.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 633.519103914499283, 323.02914983749298, 105.0, 20.0 ],
					"text" : "#1-2 - STEERING",
					"textcolor" : [ 1.0, 1.0, 1.0, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-42",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1318.0, 467.697815262250288, 104.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 633.519103914499283, 289.521862378119749, 104.0, 20.0 ],
					"text" : "#3-4 - TWO2ONE",
					"textcolor" : [ 1.0, 1.0, 1.0, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-41",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1318.0, 443.211076122693385, 121.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 633.519103914499283, 256.014574918746519, 121.0, 20.0 ],
					"text" : "#3-4 - TWO2ONE >=",
					"textcolor" : [ 1.0, 1.0, 1.0, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-34",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1318.0, 418.724336983136709, 104.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 633.519103914499283, 222.507287459373259, 104.0, 20.0 ],
					"text" : "#1-2 - TWO2ONE",
					"textcolor" : [ 1.0, 1.0, 1.0, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-22",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1318.0, 394.237597843579806, 121.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 633.519103914499283, 189.0, 121.0, 20.0 ],
					"text" : "#1-2 - TWO2ONE >=",
					"textcolor" : [ 1.0, 1.0, 1.0, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-21",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 129.977111839843474, 362.438405911128029, 108.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 15.480896085500717, 337.726611828802788, 108.0, 20.0 ],
					"text" : "#4: R - MUSCLE 2",
					"textcolor" : [ 1.0, 1.0, 1.0, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-16",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 129.977111839843474, 337.438405911128029, 108.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 15.480896085500717, 288.151074552535192, 108.0, 20.0 ],
					"text" : "#3: R - MUSCLE 1",
					"textcolor" : [ 1.0, 1.0, 1.0, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-2",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 129.977111839843474, 312.438405911128029, 105.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 15.480896085500717, 238.575537276267596, 105.0, 20.0 ],
					"text" : "#2: L - MUSCLE 2",
					"textcolor" : [ 1.0, 1.0, 1.0, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-33",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 129.977111839843474, 287.438405911128029, 105.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 15.480896085500717, 189.0, 105.0, 20.0 ],
					"text" : "#1: L - MUSCLE 1",
					"textcolor" : [ 1.0, 1.0, 1.0, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-15",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 786.178726178205579, 1151.430990505219825, 137.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 862.5, 171.0, 137.0, 20.0 ],
					"text" : "SOUND PARAMETERS"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-6",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 923.821273821794421, 563.671293541364321, 72.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 690.519103914499283, 171.0, 72.0, 20.0 ],
					"text" : "FEATURES"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-5",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 457.642547643588841, 1151.430990505219825, 119.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 452.5, 171.0, 119.0, 20.0 ],
					"text" : "RMS | CALIBRATED"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-3",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 405.477111839843474, 406.595428462257132, 36.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 87.0, 171.0, 36.0, 20.0 ],
					"text" : "RAW"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-18",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1630.933633089065552, 1354.341847848892712, 176.543478750777922, 22.0 ],
					"text" : "read lifting-solo.json 1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-1",
					"maxclass" : "newobj",
					"numinlets" : 4,
					"numoutlets" : 4,
					"outlettype" : [ "bang", "bang", "bang", "" ],
					"patching_rect" : [ 2202.0, 1175.159239244461787, 71.5, 22.0 ],
					"text" : "sel 0 5 1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-94",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"patching_rect" : [ 2357.63958740234375, 1252.964661788940703, 29.5, 22.0 ],
					"text" : "!- 1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-78",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 2418.36041259765625, 1206.576606106758391, 42.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 479.0, 71.5, 42.0, 20.0 ],
					"text" : "MUTE"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-73",
					"maxclass" : "toggle",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 2358.0, 1189.576606106758391, 54.0, 54.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 429.0, 59.0, 45.0, 45.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-52",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 2202.0, 1222.469546985626494, 24.0, 24.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-30",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"patching_rect" : [ 2202.0, 1252.964661788940703, 22.0, 22.0 ],
					"text" : "t 0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-29",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2202.0, 1320.546195697784924, 101.0, 22.0 ],
					"text" : "prepend playstop"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-28",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 2306.0, 1383.565736484528315, 49.0, 22.0 ],
					"text" : "s gran2"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-27",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 2202.0, 1383.565736484528315, 49.0, 22.0 ],
					"text" : "s gran1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-19",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1980.0, 1254.137500000000273, 27.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1181.101490479505628, 267.297135424612634, 27.0, 20.0 ],
					"text" : "slot"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-4",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1989.5, 1194.15923924446156, 70.0, 22.0 ],
					"text" : "loadmess 1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-92",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 483.321273821794421, 1046.822337409427973, 87.0, 22.0 ],
					"text" : "prepend setlist"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-90",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 926.321273821794421, 467.796099903754111, 87.0, 22.0 ],
					"text" : "prepend setlist"
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.168627450980392, 0.168627450980392, 0.168627450980392, 1.0 ],
					"candycane" : 8,
					"contdata" : 1,
					"ghostbar" : 70,
					"id" : "obj-86",
					"maxclass" : "multislider",
					"numinlets" : 1,
					"numoutlets" : 2,
					"orientation" : 0,
					"outlettype" : [ "", "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 926.321273821794421, 515.671293541364321, 67.0, 22.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 630.0, 189.0, 194.0, 200.0 ],
					"setminmax" : [ 0.0, 1.0 ],
					"setstyle" : 1,
					"signed" : 1,
					"size" : 6,
					"slidercolor" : [ 0.850980392156863, 0.784313725490196, 0.207843137254902, 1.0 ],
					"spacing" : 2,
					"thickness" : 4
				}

			}
, 			{
				"box" : 				{
					"angle" : 270.0,
					"bgcolor" : [ 0.2, 0.2, 0.2, 0.0 ],
					"border" : 1,
					"bordercolor" : [ 0.203921568627451, 0.203921568627451, 0.203921568627451, 1.0 ],
					"id" : "obj-87",
					"maxclass" : "panel",
					"mode" : 0,
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 900.642547643588841, 500.559880539349933, 118.357452356411159, 52.22282600402832 ],
					"presentation" : 1,
					"presentation_rect" : [ 626.519103914499283, 185.776280635595583, 200.0, 206.0 ],
					"proportion" : 0.5,
					"rounded" : 0
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.168627450980392, 0.168627450980392, 0.168627450980392, 1.0 ],
					"candycane" : 8,
					"contdata" : 1,
					"id" : "obj-79",
					"maxclass" : "multislider",
					"numinlets" : 1,
					"numoutlets" : 2,
					"orientation" : 0,
					"outlettype" : [ "", "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 483.321273821794421, 1097.976099205017817, 67.0, 22.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 415.480896085500717, 189.0, 194.0, 200.0 ],
					"setminmax" : [ 0.0, 1.0 ],
					"setstyle" : 1,
					"signed" : 1,
					"size" : 4,
					"slidercolor" : [ 0.850980392156863, 0.784313725490196, 0.207843137254902, 1.0 ],
					"spacing" : 2
				}

			}
, 			{
				"box" : 				{
					"angle" : 270.0,
					"bgcolor" : [ 0.2, 0.2, 0.2, 0.0 ],
					"border" : 1,
					"bordercolor" : [ 0.203921568627451, 0.203921568627451, 0.203921568627451, 1.0 ],
					"id" : "obj-80",
					"maxclass" : "panel",
					"mode" : 0,
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 457.642547643588841, 1082.864686203003657, 118.357452356411159, 52.22282600402832 ],
					"presentation" : 1,
					"presentation_rect" : [ 412.0, 185.776280635595583, 200.0, 206.0 ],
					"proportion" : 0.5,
					"rounded" : 0
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-43",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 389.977111839843474, 308.441666666667288, 87.0, 22.0 ],
					"text" : "prepend setlist"
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.168627450980392, 0.168627450980392, 0.168627450980392, 1.0 ],
					"candycane" : 8,
					"contdata" : 1,
					"id" : "obj-20",
					"maxclass" : "multislider",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 389.977111839843474, 359.595428462257132, 67.0, 22.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 8.480896085500717, 189.0, 194.0, 200.0 ],
					"setstyle" : 3,
					"signed" : 1,
					"size" : 4,
					"slidercolor" : [ 0.850980392156863, 0.784313725490196, 0.207843137254902, 1.0 ],
					"spacing" : 2
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-46",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 821.0, 1046.822337409427973, 87.0, 22.0 ],
					"text" : "prepend setlist"
				}

			}
, 			{
				"box" : 				{
					"args" : [ 4 ],
					"bgmode" : 0,
					"border" : 1,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-17",
					"lockeddragscroll" : 0,
					"lockedsize" : 0,
					"maxclass" : "bpatcher",
					"name" : "bbdmi.speedlim.maxpat",
					"numinlets" : 2,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "" ],
					"patching_rect" : [ 378.0, 848.025000000001, 198.0, 109.0 ],
					"varname" : "bbdmi.speedlim",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-89",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "", "int", "int" ],
					"patching_rect" : [ 2484.119659459995091, 1250.456328384081871, 48.0, 22.0 ],
					"text" : "change"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-85",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 2484.119659459995091, 1189.576606106758391, 119.0, 22.0 ],
					"text" : "metro 500 @active 1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-55",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 2543.346038459994816, 1282.02761738408185, 56.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 479.0, 110.0, 56.0, 20.0 ],
					"text" : "% (CPU)"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-47",
					"maxclass" : "number",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 2484.119659459995091, 1281.289661717415129, 50.0, 22.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 429.0, 109.0, 45.0, 22.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-56",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "", "int" ],
					"patching_rect" : [ 2484.119659459995091, 1219.243272773425133, 84.0, 23.0 ],
					"text" : "adstatus cpu"
				}

			}
, 			{
				"box" : 				{
					"fontface" : 3,
					"fontsize" : 30.0,
					"id" : "obj-23",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 351.642547643588841, 56.465057182312279, 125.0, 40.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 429.0, 11.0, 125.0, 40.0 ],
					"text" : "LIFTING"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-117",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 1816.0, 1354.341847848892712, 65.0, 22.0 ],
					"text" : "route write"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-109",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1816.0, 1384.637500000000273, 24.0, 24.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1205.181091469324201, 338.726611828802788, 21.0, 21.0 ]
				}

			}
, 			{
				"box" : 				{
					"args" : [ "gran2" ],
					"bgmode" : 0,
					"border" : 1,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-11",
					"lockeddragscroll" : 0,
					"lockedsize" : 0,
					"maxclass" : "bpatcher",
					"name" : "bbdmi.granulator~.maxpat",
					"numinlets" : 2,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "multichannelsignal" ],
					"patching_rect" : [ 2151.63958740234375, 422.936932182312489, 608.0, 301.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 627.0, 430.962500000000091, 607.0, 301.0 ],
					"varname" : "bbdmi.granulator~[1]",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"args" : [ "gran2" ],
					"bgmode" : 0,
					"border" : 1,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-13",
					"lockeddragscroll" : 0,
					"lockedsize" : 0,
					"maxclass" : "bpatcher",
					"name" : "bbdmi.buffer~.maxpat",
					"numinlets" : 2,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "multichannelsignal" ],
					"patching_rect" : [ 1661.63958740234375, 521.936932182312376, 326.0, 301.0 ],
					"varname" : "bbdmi.buffer~[1]",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"args" : [ 8 ],
					"bgmode" : 0,
					"border" : 1,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-105",
					"lockeddragscroll" : 0,
					"lockedsize" : 0,
					"maxclass" : "bpatcher",
					"name" : "bbdmi.scale.maxpat",
					"numinlets" : 1,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "" ],
					"patching_rect" : [ 1058.821273821794421, 613.850000000000819, 198.0, 301.724999999999568 ],
					"varname" : "bbdmi.scale",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-97",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1002.0, 164.275000000000546, 70.0, 22.0 ],
					"text" : "bbdmi.latch",
					"varname" : "bbdmi.average[2]"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-54",
					"maxclass" : "toggle",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1002.0, 343.775000000000546, 24.0, 24.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-53",
					"maxclass" : "toggle",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 821.0, 342.775000000000546, 24.0, 24.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-48",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1183.0, 162.375362364451576, 70.0, 22.0 ],
					"text" : "bbdmi.latch",
					"varname" : "bbdmi.average[3]"
				}

			}
, 			{
				"box" : 				{
					"bgmode" : 0,
					"border" : 1,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-12",
					"lockeddragscroll" : 0,
					"lockedsize" : 0,
					"maxclass" : "bpatcher",
					"name" : "bbdmi.dac~.maxpat",
					"numinlets" : 2,
					"numoutlets" : 0,
					"offset" : [ 0.0, 0.0 ],
					"patching_rect" : [ 29.0, 913.637500000000728, 198.0, 158.250000000000227 ],
					"varname" : "bbdmi.dac~",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"annotation" : "",
					"args" : [ 4, 4 ],
					"bgmode" : 0,
					"border" : 1,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"hint" : "",
					"id" : "obj-201",
					"lockeddragscroll" : 0,
					"lockedsize" : 0,
					"maxclass" : "bpatcher",
					"name" : "bbdmi.crosspatch~.maxpat",
					"numinlets" : 2,
					"numoutlets" : 2,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "multichannelsignal", "" ],
					"patching_rect" : [ 29.0, 684.887500000000728, 197.999999999999972, 206.0 ],
					"varname" : "bbdmi.crosspatch~",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"args" : [ 6, 8 ],
					"bgmode" : 0,
					"border" : 1,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-9",
					"lockeddragscroll" : 0,
					"lockedsize" : 0,
					"maxclass" : "bpatcher",
					"name" : "bbdmi.crosspatch.maxpat",
					"numinlets" : 2,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "" ],
					"patching_rect" : [ 821.0, 613.850000000000819, 198.0, 301.0 ],
					"varname" : "bbdmi.crosspatch",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-74",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 821.0, 134.708695697784833, 200.400000035762787, 22.0 ],
					"text" : "zl.slice 2"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-164",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 821.0, 167.675000000000637, 70.0, 22.0 ],
					"text" : "bbdmi.latch",
					"varname" : "bbdmi.average"
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.168627450980392, 0.168627450980392, 0.168627450980392, 1.0 ],
					"candycane" : 8,
					"contdata" : 1,
					"ghostbar" : 70,
					"id" : "obj-59",
					"maxclass" : "multislider",
					"numinlets" : 1,
					"numoutlets" : 2,
					"orientation" : 0,
					"outlettype" : [ "", "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 821.178726178205579, 1097.976099205017817, 67.0, 22.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 834.0, 189.0, 194.0, 200.0 ],
					"setminmax" : [ 0.0, 1.0 ],
					"setstyle" : 1,
					"signed" : 1,
					"size" : 8,
					"slidercolor" : [ 0.850980392156863, 0.784313725490196, 0.207843137254902, 1.0 ],
					"spacing" : 2,
					"thickness" : 4
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-165",
					"maxclass" : "newobj",
					"numinlets" : 6,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 821.0, 384.975012207031796, 471.5, 22.0 ],
					"text" : "join 6 @triggers -1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-166",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 1183.0, 134.708695697784833, 109.5, 22.0 ],
					"text" : "zl.slice 2"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-82",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "float" ],
					"patching_rect" : [ 1273.5, 162.375362364451576, 88.0, 22.0 ],
					"text" : "bbdmi.average",
					"varname" : "bbdmi.average[5]"
				}

			}
, 			{
				"box" : 				{
					"annotation" : "",
					"args" : [ 4 ],
					"bgmode" : 0,
					"border" : 1,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"hint" : "",
					"id" : "obj-175",
					"lockeddragscroll" : 0,
					"lockedsize" : 0,
					"maxclass" : "bpatcher",
					"name" : "bbdmi.onepole~.maxpat",
					"numinlets" : 2,
					"numoutlets" : 2,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "multichannelsignal", "" ],
					"patching_rect" : [ 29.0, 464.237500000000637, 197.999999999999972, 206.0 ],
					"varname" : "bbdmi.onepole~",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"args" : [ 8 ],
					"bgmode" : 0,
					"border" : 1,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-57",
					"lockeddragscroll" : 0,
					"lockedsize" : 0,
					"maxclass" : "bpatcher",
					"name" : "bbdmi.2max.maxpat",
					"numinlets" : 1,
					"numoutlets" : 0,
					"offset" : [ 0.0, 0.0 ],
					"patching_rect" : [ 1308.77711182196208, 613.850000000000819, 198.0, 301.724999999999568 ],
					"varname" : "bbdmi.2max",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-31",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "multichannelsignal", "" ],
					"patching_rect" : [ 29.0, 279.220833333333644, 83.0, 22.0 ],
					"text" : "bbdmi.list2~ 4",
					"varname" : "bbdmi.list2~[1]"
				}

			}
, 			{
				"box" : 				{
					"annotation" : "",
					"args" : [ 4 ],
					"bgmode" : 0,
					"border" : 1,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"hint" : "",
					"id" : "obj-14",
					"lockeddragscroll" : 0,
					"lockedsize" : 0,
					"maxclass" : "bpatcher",
					"name" : "bbdmi.calibrate.maxpat",
					"numinlets" : 2,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "" ],
					"patching_rect" : [ 378.0, 613.850000000000819, 197.999999999999972, 206.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 208.5, 185.776280635595583, 197.999999999999972, 206.0 ],
					"varname" : "bbdmi.calibrate",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"args" : [ 4, "@winsize", 0.25, "@clock", 5 ],
					"bgmode" : 0,
					"border" : 1,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-190",
					"lockeddragscroll" : 0,
					"lockedsize" : 0,
					"maxclass" : "bpatcher",
					"name" : "bbdmi.rms~.maxpat",
					"numinlets" : 2,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "" ],
					"patching_rect" : [ 378.0, 464.237500000000637, 198.0, 133.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 208.5, 6.0, 198.0, 133.0 ],
					"varname" : "bbdmi.rms~",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"annotation" : "",
					"bgmode" : 0,
					"border" : 1,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"hint" : "",
					"id" : "obj-192",
					"lockeddragscroll" : 0,
					"lockedsize" : 0,
					"maxclass" : "bpatcher",
					"name" : "bbdmi.eavi.maxpat",
					"numinlets" : 1,
					"numoutlets" : 2,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 29.0, 48.465057182312279, 198.0, 131.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 5.0, 6.0, 200.0, 133.0 ],
					"varname" : "bbdmi.eavi",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"autorestore" : "lifting-solo.json",
					"id" : "obj-8",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1527.47711181640625, 1320.546195697784924, 454.0, 22.0 ],
					"priority" : 					{
						"bbdmi.buffer~::Folder" : -1,
						"bbdmi.granulator~::PlayPos" : 1,
						"bbdmi.granulator~::Folder" : -1,
						"bbdmi.crosspatch::Crosspatch" : 1,
						"bbdmi.buffer~[1]::Folder" : -1,
						"bbdmi.granulator~[1]::PlayPos" : 1,
						"bbdmi.granulator~[1]::Folder" : -1
					}
,
					"saved_object_attributes" : 					{
						"client_rect" : [ 4, 44, 358, 172 ],
						"parameter_enable" : 0,
						"parameter_mappable" : 0,
						"storage_rect" : [ 482, 79, 933, 810 ]
					}
,
					"text" : "pattrstorage lifting-solo @greedy 1 @savemode 1 @autorestore 1 @changemode 1",
					"varname" : "lifting-solo"
				}

			}
, 			{
				"box" : 				{
					"fontface" : 0,
					"fontname" : "Arial",
					"fontsize" : 14.0,
					"id" : "obj-58",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1890.5, 1383.1375000000005, 65.0, 22.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1044.321273821794421, 191.6617458701121, 65.0, 22.0 ],
					"text" : "PRESET"
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.137254901960784, 0.137254901960784, 0.137254901960784, 1.0 ],
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-40",
					"maxclass" : "textbutton",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "", "", "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1874.5, 1253.15923924446156, 39.0, 21.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1181.101490479505628, 242.487309956549325, 45.079600989818573, 21.0 ],
					"text" : "store",
					"texton" : "store",
					"textoncolor" : [ 1.0, 1.0, 1.0, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.137254901960784, 0.137254901960784, 0.137254901960784, 1.0 ],
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-62",
					"maxclass" : "textbutton",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "", "", "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1823.63958740234375, 1194.15923924446156, 39.0, 21.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1181.101490479505628, 218.33096840381495, 45.079600989818573, 21.0 ],
					"text" : "recall",
					"texton" : "recall",
					"textoncolor" : [ 1.0, 1.0, 1.0, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.137254901960784, 0.137254901960784, 0.137254901960784, 1.0 ],
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-63",
					"maxclass" : "textbutton",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "", "", "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1753.63958740234375, 1253.1375000000005, 78.0, 20.765432208776474 ],
					"text" : "clientwindow",
					"texton" : "clientwindow",
					"textoncolor" : [ 1.0, 1.0, 1.0, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.137254901960784, 0.137254901960784, 0.137254901960784, 1.0 ],
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-36",
					"maxclass" : "textbutton",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "", "", "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1695.47711181640625, 1194.15923924446156, 93.0, 21.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1136.605748754537672, 362.536437296866154, 89.57534271478653, 21.0 ],
					"text" : "storagewindow",
					"texton" : "storagewindow",
					"textoncolor" : [ 1.0, 1.0, 1.0, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.137254901960784, 0.137254901960784, 0.137254901960784, 1.0 ],
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-64",
					"maxclass" : "textbutton",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "", "", "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1606.933633089065552, 1194.15923924446156, 66.0, 21.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1136.605748754537672, 338.726611828802788, 64.57534271478653, 21.0 ],
					"text" : "writeagain",
					"texton" : "writeagain",
					"textoncolor" : [ 1.0, 1.0, 1.0, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.137254901960784, 0.137254901960784, 0.137254901960784, 1.0 ],
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-65",
					"maxclass" : "textbutton",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "", "", "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1569.47711181640625, 1251.954891395569348, 39.0, 21.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1136.605748754537672, 314.916786360739422, 89.57534271478653, 21.0 ],
					"text" : "write",
					"texton" : "write",
					"textoncolor" : [ 1.0, 1.0, 1.0, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-32",
					"maxclass" : "newobj",
					"numinlets" : 9,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patcher" : 					{
						"fileversion" : 1,
						"appversion" : 						{
							"major" : 8,
							"minor" : 5,
							"revision" : 6,
							"architecture" : "x64",
							"modernui" : 1
						}
,
						"classnamespace" : "box",
						"rect" : [ 36.0, 79.0, 384.0, 264.0 ],
						"bglocked" : 0,
						"openinpresentation" : 0,
						"default_fontsize" : 12.0,
						"default_fontface" : 0,
						"default_fontname" : "Arial",
						"gridonopen" : 1,
						"gridsize" : [ 15.0, 15.0 ],
						"gridsnaponopen" : 1,
						"objectsnaponopen" : 1,
						"statusbarvisible" : 2,
						"toolbarvisible" : 1,
						"lefttoolbarpinned" : 0,
						"toptoolbarpinned" : 0,
						"righttoolbarpinned" : 0,
						"bottomtoolbarpinned" : 0,
						"toolbars_unpinned_last_save" : 0,
						"tallnewobj" : 0,
						"boxanimatetime" : 200,
						"enablehscroll" : 1,
						"enablevscroll" : 1,
						"devicewidth" : 0.0,
						"description" : "",
						"digest" : "",
						"tags" : "",
						"style" : "",
						"subpatcher_template" : "Default Max 7",
						"assistshowspatchername" : 0,
						"boxes" : [ 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-24",
									"index" : 9,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 342.357142857142833, 12.5, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-23",
									"maxclass" : "button",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 297.357142857142833, 66.5, 24.0, 24.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-21",
									"maxclass" : "button",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 239.357142857142833, 66.5, 24.0, 24.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-19",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 239.357142857142833, 143.0, 54.0, 22.0 ],
									"text" : "recall $1"
								}

							}
, 							{
								"box" : 								{
									"fontface" : 0,
									"fontname" : "Arial",
									"fontsize" : 12.0,
									"id" : "obj-64",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"patching_rect" : [ 239.357142857142833, 114.5, 39.0, 22.0 ],
									"text" : "i"
								}

							}
, 							{
								"box" : 								{
									"fontface" : 0,
									"fontname" : "Arial",
									"fontsize" : 12.0,
									"id" : "obj-66",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"patching_rect" : [ 297.357142857142833, 114.5, 40.5, 22.0 ],
									"text" : "i"
								}

							}
, 							{
								"box" : 								{
									"fontface" : 0,
									"fontname" : "Arial",
									"fontsize" : 12.0,
									"id" : "obj-68",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 297.357142857142833, 143.0, 52.0, 22.0 ],
									"text" : "store $1"
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-17",
									"index" : 1,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 10.5, 218.0, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-16",
									"index" : 8,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 297.357142857142833, 12.5, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-14",
									"index" : 7,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 239.357142857142833, 12.5, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-12",
									"index" : 6,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 201.214285714285666, 12.5, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-10",
									"index" : 5,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 163.071428571428555, 12.5, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-8",
									"index" : 4,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 124.928571428571416, 12.5, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-6",
									"index" : 3,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 86.785714285714278, 12.5, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-4",
									"index" : 2,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 48.642857142857139, 12.5, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-2",
									"index" : 1,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 10.5, 12.5, 30.0, 30.0 ]
								}

							}
 ],
						"lines" : [ 							{
								"patchline" : 								{
									"destination" : [ "obj-17", 0 ],
									"midpoints" : [ 172.571428571428555, 45.0, 172.571428571428555, 45.0, 172.571428571428555, 204.0, 20.0, 204.0 ],
									"source" : [ "obj-10", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-17", 0 ],
									"midpoints" : [ 210.714285714285666, 204.0, 20.0, 204.0 ],
									"source" : [ "obj-12", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-21", 0 ],
									"midpoints" : [ 248.857142857142833, 45.0, 248.857142857142833, 45.0 ],
									"source" : [ "obj-14", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-23", 0 ],
									"midpoints" : [ 306.857142857142833, 45.0, 306.857142857142833, 45.0 ],
									"source" : [ "obj-16", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-17", 0 ],
									"midpoints" : [ 248.857142857142833, 204.0, 20.0, 204.0 ],
									"source" : [ "obj-19", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-17", 0 ],
									"midpoints" : [ 20.0, 45.0, 20.0, 45.0 ],
									"source" : [ "obj-2", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-64", 0 ],
									"midpoints" : [ 248.857142857142833, 93.0, 248.857142857142833, 93.0 ],
									"source" : [ "obj-21", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-66", 0 ],
									"midpoints" : [ 306.857142857142833, 93.0, 306.857142857142833, 93.0 ],
									"source" : [ "obj-23", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-64", 1 ],
									"midpoints" : [ 351.857142857142833, 99.0, 268.857142857142833, 99.0 ],
									"order" : 1,
									"source" : [ "obj-24", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-66", 1 ],
									"midpoints" : [ 351.857142857142833, 99.0, 328.357142857142833, 99.0 ],
									"order" : 0,
									"source" : [ "obj-24", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-17", 0 ],
									"midpoints" : [ 58.142857142857139, 204.0, 20.0, 204.0 ],
									"source" : [ "obj-4", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-17", 0 ],
									"midpoints" : [ 96.285714285714278, 204.0, 20.0, 204.0 ],
									"source" : [ "obj-6", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-19", 0 ],
									"midpoints" : [ 248.857142857142833, 138.0, 248.857142857142833, 138.0 ],
									"source" : [ "obj-64", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-68", 0 ],
									"midpoints" : [ 306.857142857142833, 138.0, 306.857142857142833, 138.0 ],
									"source" : [ "obj-66", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-17", 0 ],
									"midpoints" : [ 306.857142857142833, 204.0, 20.0, 204.0 ],
									"source" : [ "obj-68", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-17", 0 ],
									"midpoints" : [ 134.428571428571416, 204.0, 20.0, 204.0 ],
									"source" : [ "obj-8", 0 ]
								}

							}
 ]
					}
,
					"patching_rect" : [ 1527.47711181640625, 1288.750543546677136, 428.0, 22.0 ],
					"saved_object_attributes" : 					{
						"description" : "",
						"digest" : "",
						"globalpatchername" : "",
						"tags" : ""
					}
,
					"text" : "p settings"
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.137254901960784, 0.137254901960784, 0.137254901960784, 1.0 ],
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-24",
					"maxclass" : "textbutton",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "", "", "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1670.548413902521133, 1251.954891395569348, 39.0, 21.0 ],
					"text" : "clear",
					"texton" : "clear",
					"textoncolor" : [ 1.0, 1.0, 1.0, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-66",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patcher" : 					{
						"fileversion" : 1,
						"appversion" : 						{
							"major" : 8,
							"minor" : 5,
							"revision" : 6,
							"architecture" : "x64",
							"modernui" : 1
						}
,
						"classnamespace" : "box",
						"rect" : [ 36.0, 79.0, 358.0, 413.0 ],
						"bglocked" : 0,
						"openinpresentation" : 0,
						"default_fontsize" : 12.0,
						"default_fontface" : 0,
						"default_fontname" : "Arial",
						"gridonopen" : 1,
						"gridsize" : [ 15.0, 15.0 ],
						"gridsnaponopen" : 1,
						"objectsnaponopen" : 1,
						"statusbarvisible" : 2,
						"toolbarvisible" : 1,
						"lefttoolbarpinned" : 0,
						"toptoolbarpinned" : 0,
						"righttoolbarpinned" : 0,
						"bottomtoolbarpinned" : 0,
						"toolbars_unpinned_last_save" : 0,
						"tallnewobj" : 0,
						"boxanimatetime" : 200,
						"enablehscroll" : 1,
						"enablevscroll" : 1,
						"devicewidth" : 0.0,
						"description" : "",
						"digest" : "",
						"tags" : "",
						"style" : "",
						"subpatcher_template" : "Default Max 7",
						"assistshowspatchername" : 0,
						"boxes" : [ 							{
								"box" : 								{
									"id" : "obj-10",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 33.25, 258.500006111111134, 86.0, 22.0 ],
									"text" : "prepend name"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-9",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "" ],
									"patching_rect" : [ 33.25, 176.442193388888882, 198.0, 22.0 ],
									"text" : "t s s"
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-6",
									"index" : 1,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 33.25, 309.500012222222267, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-85",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 33.25, 207.5, 120.0, 22.0 ],
									"text" : "prepend pattrstorage"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-78",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "", "" ],
									"patching_rect" : [ 14.0, 61.0, 63.0, 22.0 ],
									"text" : "route read"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 12.0,
									"id" : "obj-66",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 5,
									"outlettype" : [ "", "", "", "", "" ],
									"patching_rect" : [ 14.0, 125.384386777777763, 96.0, 22.0 ],
									"text" : "regexp (.+)\\\\..+$"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 12.0,
									"id" : "obj-72",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "int" ],
									"patching_rect" : [ 14.0, 86.384386777777763, 57.0, 22.0 ],
									"text" : "strippath"
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-86",
									"index" : 1,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 14.000000023437501, 16.000012222222267, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-87",
									"index" : 2,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 227.250000023437508, 309.500012222222267, 30.0, 30.0 ]
								}

							}
 ],
						"lines" : [ 							{
								"patchline" : 								{
									"destination" : [ "obj-6", 0 ],
									"source" : [ "obj-10", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-9", 0 ],
									"source" : [ "obj-66", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-66", 0 ],
									"midpoints" : [ 23.5, 111.0, 23.5, 111.0 ],
									"source" : [ "obj-72", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-72", 0 ],
									"midpoints" : [ 23.5, 84.0, 23.5, 84.0 ],
									"source" : [ "obj-78", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-87", 0 ],
									"source" : [ "obj-85", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-78", 0 ],
									"midpoints" : [ 23.500000023437501, 48.0, 23.5, 48.0 ],
									"source" : [ "obj-86", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-10", 0 ],
									"source" : [ "obj-9", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-85", 0 ],
									"source" : [ "obj-9", 0 ]
								}

							}
 ]
					}
,
					"patching_rect" : [ 1527.47711181640625, 1354.341847848892712, 52.0, 22.0 ],
					"saved_object_attributes" : 					{
						"description" : "",
						"digest" : "",
						"globalpatchername" : "",
						"tags" : ""
					}
,
					"text" : "p preset"
				}

			}
, 			{
				"box" : 				{
					"bubblesize" : 16,
					"embed" : 0,
					"id" : "obj-67",
					"maxclass" : "preset",
					"numinlets" : 1,
					"numoutlets" : 5,
					"outlettype" : [ "preset", "int", "preset", "int", "" ],
					"patching_rect" : [ 1560.477111839843474, 1398.137500000000728, 247.0, 27.0 ],
					"pattrstorage" : "lifting-solo",
					"presentation" : 1,
					"presentation_rect" : [ 1044.321273821794421, 218.33096840381495, 84.6712446808815, 166.205468893051204 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-68",
					"maxclass" : "incdec",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "float" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1935.5, 1194.15923924446156, 41.0, 21.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1136.605748754537672, 218.33096840381495, 40.0, 45.156341552734375 ]
				}

			}
, 			{
				"box" : 				{
					"fontface" : 0,
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-69",
					"maxclass" : "number",
					"minimum" : 1,
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1935.5, 1253.1375000000005, 40.0, 22.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1136.605748754537672, 266.297135424612691, 40.0, 22.0 ],
					"triangle" : 0
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.137254901960784, 0.137254901960784, 0.137254901960784, 1.0 ],
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-7",
					"maxclass" : "textbutton",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "", "", "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1517.47711181640625, 1252.1375000000005, 39.0, 21.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1136.605748754537672, 291.106960892676057, 89.57534271478653, 21.0 ],
					"text" : "read",
					"texton" : "read",
					"textoncolor" : [ 1.0, 1.0, 1.0, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"args" : [ "gran1" ],
					"bgmode" : 0,
					"border" : 1,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-38",
					"lockeddragscroll" : 0,
					"lockedsize" : 0,
					"maxclass" : "bpatcher",
					"name" : "bbdmi.granulator~.maxpat",
					"numinlets" : 2,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "multichannelsignal" ],
					"patching_rect" : [ 2158.63958740234375, 48.974432182312285, 608.0, 301.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 5.0, 430.962500000000091, 607.0, 301.0 ],
					"varname" : "bbdmi.granulator~",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"args" : [ "gran1" ],
					"bgmode" : 0,
					"border" : 1,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-39",
					"lockeddragscroll" : 0,
					"lockedsize" : 0,
					"maxclass" : "bpatcher",
					"name" : "bbdmi.buffer~.maxpat",
					"numinlets" : 2,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "multichannelsignal" ],
					"patching_rect" : [ 1661.63958740234375, 48.974432182312285, 326.0, 301.0 ],
					"varname" : "bbdmi.buffer~",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-77",
					"maxclass" : "newobj",
					"numinlets" : 4,
					"numoutlets" : 0,
					"patcher" : 					{
						"fileversion" : 1,
						"appversion" : 						{
							"major" : 8,
							"minor" : 5,
							"revision" : 6,
							"architecture" : "x64",
							"modernui" : 1
						}
,
						"classnamespace" : "box",
						"rect" : [ 34.0, 79.0, 1372.0, 787.0 ],
						"bglocked" : 0,
						"openinpresentation" : 0,
						"default_fontsize" : 12.0,
						"default_fontface" : 0,
						"default_fontname" : "Arial",
						"gridonopen" : 1,
						"gridsize" : [ 15.0, 15.0 ],
						"gridsnaponopen" : 1,
						"objectsnaponopen" : 1,
						"statusbarvisible" : 2,
						"toolbarvisible" : 1,
						"lefttoolbarpinned" : 0,
						"toptoolbarpinned" : 0,
						"righttoolbarpinned" : 0,
						"bottomtoolbarpinned" : 0,
						"toolbars_unpinned_last_save" : 0,
						"tallnewobj" : 0,
						"boxanimatetime" : 200,
						"enablehscroll" : 1,
						"enablevscroll" : 1,
						"devicewidth" : 0.0,
						"description" : "",
						"digest" : "",
						"tags" : "",
						"style" : "",
						"subpatcher_template" : "",
						"assistshowspatchername" : 0,
						"boxes" : [ 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 9.0,
									"id" : "obj-25",
									"linecount" : 3,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 384.5, 396.0, 75.0, 39.0 ],
									"text" : ";\rsmoothmusc 30;\rsmoothaccel 60"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 9.0,
									"id" : "obj-26",
									"linecount" : 3,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 255.0, 396.0, 79.0, 39.0 ],
									"text" : ";\rsmoothmusc 110;\rsmoothaccel 85"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-24",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 270.0, 462.0, 170.0, 20.0 ],
									"text" : "smoothmusc = from 30 to 0.5"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-11",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 255.0, 440.0, 170.0, 20.0 ],
									"text" : "smoothmusc = from 110 to 0.5"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-9",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 22.0, 990.0, 150.0, 20.0 ]
								}

							}
, 							{
								"box" : 								{
									"fontsize" : 9.0,
									"id" : "obj-7",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"patching_rect" : [ 764.5, 86.70001220703125, 43.0, 19.0 ],
									"text" : "del 2000"
								}

							}
, 							{
								"box" : 								{
									"fontsize" : 9.0,
									"id" : "obj-4",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"patching_rect" : [ 546.5, 86.70001220703125, 43.0, 19.0 ],
									"text" : "del 2000"
								}

							}
, 							{
								"box" : 								{
									"fontsize" : 9.0,
									"id" : "obj-102",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"patching_rect" : [ 34.0, 86.70001220703125, 43.0, 19.0 ],
									"text" : "del 1500"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 9.0,
									"id" : "obj-105",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "bang", "bang" ],
									"patching_rect" : [ 34.0, 54.0, 30.0, 19.0 ],
									"text" : "t b b"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 9.0,
									"id" : "obj-6",
									"linecount" : 12,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 947.5, 871.0, 100.0, 129.0 ],
									"text" : ";\rctlin08 get direc12;\rctlin08 speedlim 0;\rctlin08 min-in 0.;\rctlin08 max-in 127.;\rctlin08 min-out -4800.;\rctlin08 max-out -2800.;\rctlin08 curve 1;\rctlin08 time 0;\rctlin08 grain 20;\rctlin08 send gran2;\rctlin08 param transp"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 9.0,
									"id" : "obj-5",
									"linecount" : 12,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 29.5, 871.0, 97.0, 129.0 ],
									"text" : ";\rctlin08 get direc12;\rctlin08 speedlim 0;\rctlin08 min-in 0.;\rctlin08 max-in 127.;\rctlin08 min-out 3200.;\rctlin08 max-out 5200.;\rctlin08 curve 1;\rctlin08 time 0;\rctlin08 grain 20;\rctlin08 send gran2;\rctlin08 param transp"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 9.0,
									"id" : "obj-67",
									"linecount" : 2,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 893.5, 269.5, 71.0, 29.0 ],
									"text" : ";\rreset-glaz bang"
								}

							}
, 							{
								"box" : 								{
									"fontsize" : 9.0,
									"id" : "obj-60",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"patching_rect" : [ 893.5, 239.0, 53.0, 19.0 ],
									"text" : "delay 3500"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 9.0,
									"id" : "obj-66",
									"linecount" : 3,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 977.5, 239.0, 94.0, 39.0 ],
									"text" : ";\rgran-to-dacs 0 3000;\rgran2-to-dacs 0 3000"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 9.0,
									"id" : "obj-65",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "bang", "bang" ],
									"patching_rect" : [ 978.0, 54.0, 29.5, 19.0 ],
									"text" : "t b b"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 9.0,
									"id" : "obj-64",
									"linecount" : 3,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 988.5, 182.5, 83.0, 39.0 ],
									"text" : ";\rctlin06 disconnect;\rctlin07 disconnect"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 9.0,
									"id" : "obj-63",
									"linecount" : 3,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 764.0, 192.0, 79.0, 39.0 ],
									"text" : ";\rsmoothmusc 110;\rsmoothaccel 85"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 9.0,
									"id" : "obj-62",
									"linecount" : 3,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 384.5, 352.0, 82.0, 39.0 ],
									"text" : ";\rsmoothmusc 0.13;\rsmoothaccel 60"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 9.0,
									"id" : "obj-61",
									"linecount" : 3,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 255.0, 352.0, 77.0, 39.0 ],
									"text" : ";\rsmoothmusc 0.5;\rsmoothaccel 85"
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-1",
									"index" : 4,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 978.0, 7.0, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-12",
									"index" : 3,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 764.5, 7.0, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 9.0,
									"id" : "obj-13",
									"linecount" : 2,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 764.5, 156.0, 124.0, 29.0 ],
									"text" : ";\rgran set-buffer AKStgtone.aif"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 9.0,
									"id" : "obj-14",
									"linecount" : 2,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 788.5, 120.0, 129.0, 29.0 ],
									"text" : ";\rgran2 set-buffer AKStgtone.aif"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-16",
									"maxclass" : "button",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 46.0, 511.0, 24.0, 24.0 ]
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 9.0,
									"id" : "obj-17",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"patching_rect" : [ 45.5, 604.0, 58.0, 19.0 ],
									"text" : "delay 10000"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 9.0,
									"id" : "obj-18",
									"linecount" : 4,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 45.5, 631.0, 72.0, 49.0 ],
									"text" : ";\rgran-aux 0 10;\rgran2-aux 0 10;\renable-shifts 0"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 9.0,
									"id" : "obj-19",
									"linecount" : 3,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 71.0, 551.0, 84.0, 39.0 ],
									"text" : ";\rgran-aux 0 10000;\rgran2-aux 0 10000"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 9.0,
									"id" : "obj-20",
									"linecount" : 11,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 802.5, 735.0, 115.0, 119.0 ],
									"text" : ";\rctlin07 get musc34;\rctlin07 speedlim 0;\rctlin07 min-in 0.;\rctlin07 max-in 127.;\rctlin07 min-out 70.;\rctlin07 max-out 137.;\rctlin07 curve 1;\rctlin07 time 0;\rctlin07 grain 20;\rctlin07 send gran2-to-dacs"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 9.0,
									"id" : "obj-21",
									"linecount" : 11,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 675.0, 735.0, 110.0, 119.0 ],
									"text" : ";\rctlin06 get musc34;\rctlin06 speedlim 0;\rctlin06 min-in 0.;\rctlin06 max-in 127.;\rctlin06 min-out 70.;\rctlin06 max-out 137.;\rctlin06 curve 1;\rctlin06 time 0;\rctlin06 grain 20;\rctlin06 send gran-to-dacs"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 9.0,
									"id" : "obj-22",
									"linecount" : 11,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 546.5, 735.0, 100.0, 119.0 ],
									"text" : ";\rctlin05 get musc4;\rctlin05 speedlim 0;\rctlin05 min-in 0.;\rctlin05 max-in 127.;\rctlin05 min-out 0.;\rctlin05 max-out 127.;\rctlin05 curve 1;\rctlin05 time 0;\rctlin05 grain 20;\rctlin05 send grangran4"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 9.0,
									"id" : "obj-23",
									"linecount" : 11,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 430.5, 735.0, 100.0, 119.0 ],
									"text" : ";\rctlin04 get musc3;\rctlin04 speedlim 0;\rctlin04 min-in 0.;\rctlin04 max-in 127.;\rctlin04 min-out 0.;\rctlin04 max-out 127.;\rctlin04 curve 1;\rctlin04 time 0;\rctlin04 grain 20;\rctlin04 send grangran3"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 9.0,
									"id" : "obj-32",
									"linecount" : 13,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 579.0, 356.0, 138.0, 139.0 ],
									"text" : ";\rgran2 mode 0;\rgran2 shots 0;\rgran2 play/stop 1;\rgran2 beg 0.;\rgran2 stretch 16.;\rgran2 pos 511.;\rgran2 posran 10.;\rgran2 dur 50.;\rgran2 transp -3600;\rgran2 aleaoct 0.;\rgran2 quant 0.;\rgran2 set-buffer AKSsqbang1.aif"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 9.0,
									"id" : "obj-33",
									"linecount" : 13,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 579.0, 207.0, 133.0, 139.0 ],
									"text" : ";\rgran mode 0;\rgran shots 0;\rgran play/stop 1;\rgran beg 0.;\rgran stretch 16.;\rgran pos 491.;\rgran posran 10.;\rgran dur 50.;\rgran transp -3600;\rgran aleaoct 0.;\rgran quant 0.;\rgran set-buffer AKSsqbang1.aif"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 9.0,
									"id" : "obj-34",
									"linecount" : 12,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 947.5, 735.0, 100.0, 129.0 ],
									"text" : ";\rctlin01 get direc12;\rctlin01 speedlim 0;\rctlin01 min-in 0.;\rctlin01 max-in 127.;\rctlin01 min-out -4800.;\rctlin01 max-out -2800.;\rctlin01 curve 1;\rctlin01 time 0;\rctlin01 grain 20;\rctlin01 send gran;\rctlin01 param transp"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-35",
									"maxclass" : "button",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 547.0, 690.0, 24.0, 24.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-36",
									"index" : 2,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 547.0, 7.0, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-37",
									"maxclass" : "button",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 547.0, 126.0, 24.0, 24.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-38",
									"index" : 1,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 34.0, 7.0, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 9.0,
									"id" : "obj-39",
									"linecount" : 11,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 314.5, 735.0, 100.0, 119.0 ],
									"text" : ";\rctlin03 get musc2;\rctlin03 speedlim 0;\rctlin03 min-in 0.;\rctlin03 max-in 127.;\rctlin03 min-out 0.;\rctlin03 max-out 127.;\rctlin03 curve 1;\rctlin03 time 0;\rctlin03 grain 20;\rctlin03 send grangran2"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 9.0,
									"id" : "obj-40",
									"linecount" : 11,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 198.5, 735.0, 100.0, 119.0 ],
									"text" : ";\rctlin02 get musc1;\rctlin02 speedlim 0;\rctlin02 min-in 0.;\rctlin02 max-in 127.;\rctlin02 min-out 0.;\rctlin02 max-out 127.;\rctlin02 curve 1;\rctlin02 time 0;\rctlin02 grain 20;\rctlin02 send grangran1"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 9.0,
									"id" : "obj-53",
									"linecount" : 12,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 29.5, 735.0, 97.0, 129.0 ],
									"text" : ";\rctlin01 get direc12;\rctlin01 speedlim 0;\rctlin01 min-in 0.;\rctlin01 max-in 127.;\rctlin01 min-out 3200.;\rctlin01 max-out 5200.;\rctlin01 curve 1;\rctlin01 time 0;\rctlin01 grain 20;\rctlin01 send gran;\rctlin01 param transp"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-54",
									"maxclass" : "button",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 34.0, 126.0, 24.0, 24.0 ]
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 9.0,
									"id" : "obj-56",
									"linecount" : 5,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 385.5, 275.0, 69.0, 59.0 ],
									"text" : ";\rgran pan 28;\rgran2 pan 100;\rgran swap 2;\rgran2 swap 2"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 9.0,
									"id" : "obj-57",
									"linecount" : 13,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 58.5, 324.0, 154.0, 139.0 ],
									"text" : ";\rgran2 mode 1;\rgran2 shots 0;\rgran2 play/stop 1;\rgran2 beg 0.;\rgran2 stretch 1.;\rgran2 pos 0.;\rgran2 posran 10.;\rgran2 dur 50.;\rgran2 transp 0.;\rgran2 aleaoct 0.;\rgran2 quant 0.;\rgran2 set-buffer AKSsoftbass-stlp.aif"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 9.0,
									"id" : "obj-58",
									"linecount" : 13,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 58.5, 174.0, 149.0, 139.0 ],
									"text" : ";\rgran mode 1;\rgran shots 0;\rgran play/stop 1;\rgran beg 0.;\rgran stretch 3.953921;\rgran pos 10644.542969;\rgran posran 10.;\rgran dur 50.;\rgran transp 0.;\rgran aleaoct 0.;\rgran quant 0.;\rgran set-buffer AKSsoftbass-stlp.aif"
								}

							}
 ],
						"lines" : [ 							{
								"patchline" : 								{
									"destination" : [ "obj-65", 0 ],
									"midpoints" : [ 987.5, 39.0, 987.5, 39.0 ],
									"source" : [ "obj-1", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-54", 0 ],
									"midpoints" : [ 43.5, 108.0, 43.5, 108.0 ],
									"source" : [ "obj-102", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-102", 0 ],
									"midpoints" : [ 43.5, 75.0, 43.5, 75.0 ],
									"source" : [ "obj-105", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-7", 0 ],
									"midpoints" : [ 774.0, 39.0, 774.0, 39.0 ],
									"source" : [ "obj-12", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-17", 0 ],
									"midpoints" : [ 55.5, 537.0, 55.0, 537.0 ],
									"order" : 1,
									"source" : [ "obj-16", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-19", 0 ],
									"midpoints" : [ 55.5, 546.0, 80.5, 546.0 ],
									"order" : 0,
									"source" : [ "obj-16", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-18", 0 ],
									"midpoints" : [ 55.0, 624.0, 55.0, 624.0 ],
									"source" : [ "obj-17", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-20", 0 ],
									"midpoints" : [ 556.5, 717.0, 812.0, 717.0 ],
									"order" : 0,
									"source" : [ "obj-35", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-21", 0 ],
									"midpoints" : [ 556.5, 717.0, 684.5, 717.0 ],
									"order" : 1,
									"source" : [ "obj-35", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-22", 0 ],
									"midpoints" : [ 556.5, 717.0, 556.0, 717.0 ],
									"order" : 2,
									"source" : [ "obj-35", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-23", 0 ],
									"midpoints" : [ 556.5, 717.0, 440.0, 717.0 ],
									"order" : 3,
									"source" : [ "obj-35", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-39", 0 ],
									"midpoints" : [ 556.5, 717.0, 324.0, 717.0 ],
									"order" : 4,
									"source" : [ "obj-35", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-40", 0 ],
									"midpoints" : [ 556.5, 717.0, 208.0, 717.0 ],
									"order" : 5,
									"source" : [ "obj-35", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-4", 0 ],
									"midpoints" : [ 556.5, 39.0, 556.0, 39.0 ],
									"source" : [ "obj-36", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-32", 0 ],
									"midpoints" : [ 556.5, 351.0, 588.5, 351.0 ],
									"order" : 2,
									"source" : [ "obj-37", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-33", 0 ],
									"midpoints" : [ 556.5, 192.0, 588.5, 192.0 ],
									"order" : 3,
									"source" : [ "obj-37", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-34", 0 ],
									"midpoints" : [ 556.5, 675.0, 957.0, 675.0 ],
									"order" : 1,
									"source" : [ "obj-37", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-56", 0 ],
									"midpoints" : [ 556.5, 261.0, 395.0, 261.0 ],
									"order" : 4,
									"source" : [ "obj-37", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-6", 0 ],
									"midpoints" : [ 556.5, 675.0, 933.0, 675.0, 933.0, 867.0, 957.0, 867.0 ],
									"order" : 0,
									"source" : [ "obj-37", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-62", 0 ],
									"midpoints" : [ 556.5, 348.0, 394.0, 348.0 ],
									"order" : 5,
									"source" : [ "obj-37", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-105", 0 ],
									"midpoints" : [ 43.5, 39.0, 43.5, 39.0 ],
									"source" : [ "obj-38", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-35", 0 ],
									"midpoints" : [ 556.0, 108.0, 531.0, 108.0, 531.0, 675.0, 556.5, 675.0 ],
									"order" : 0,
									"source" : [ "obj-4", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-37", 0 ],
									"midpoints" : [ 556.0, 108.0, 556.5, 108.0 ],
									"order" : 1,
									"source" : [ "obj-4", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-16", 0 ],
									"midpoints" : [ 43.5, 498.0, 55.5, 498.0 ],
									"order" : 5,
									"source" : [ "obj-54", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-35", 0 ],
									"midpoints" : [ 43.5, 498.0, 556.5, 498.0 ],
									"order" : 0,
									"source" : [ "obj-54", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-5", 0 ],
									"midpoints" : [ 43.5, 498.0, 15.0, 498.0, 15.0, 867.0, 39.0, 867.0 ],
									"order" : 6,
									"source" : [ "obj-54", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-53", 0 ],
									"midpoints" : [ 43.5, 498.0, 30.0, 498.0, 30.0, 720.0, 39.0, 720.0 ],
									"order" : 7,
									"source" : [ "obj-54", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-56", 0 ],
									"midpoints" : [ 43.5, 153.0, 372.0, 153.0, 372.0, 261.0, 395.0, 261.0 ],
									"order" : 1,
									"source" : [ "obj-54", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-57", 0 ],
									"midpoints" : [ 43.5, 318.0, 68.0, 318.0 ],
									"order" : 3,
									"source" : [ "obj-54", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-58", 0 ],
									"midpoints" : [ 43.5, 168.0, 68.0, 168.0 ],
									"order" : 4,
									"source" : [ "obj-54", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-61", 0 ],
									"midpoints" : [ 43.5, 153.0, 240.0, 153.0, 240.0, 348.0, 264.5, 348.0 ],
									"order" : 2,
									"source" : [ "obj-54", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-67", 0 ],
									"midpoints" : [ 903.0, 261.0, 903.0, 261.0 ],
									"source" : [ "obj-60", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-60", 0 ],
									"midpoints" : [ 987.5, 168.0, 903.0, 168.0 ],
									"order" : 1,
									"source" : [ "obj-65", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-64", 0 ],
									"midpoints" : [ 998.0, 75.0, 998.0, 75.0 ],
									"source" : [ "obj-65", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-66", 0 ],
									"midpoints" : [ 987.5, 168.0, 975.0, 168.0, 975.0, 231.0, 987.0, 231.0 ],
									"order" : 0,
									"source" : [ "obj-65", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-13", 0 ],
									"midpoints" : [ 774.0, 108.0, 774.0, 108.0 ],
									"order" : 2,
									"source" : [ "obj-7", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-14", 0 ],
									"midpoints" : [ 774.0, 114.0, 798.0, 114.0 ],
									"order" : 0,
									"source" : [ "obj-7", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-63", 0 ],
									"midpoints" : [ 774.0, 141.0, 750.0, 141.0, 750.0, 186.0, 773.5, 186.0 ],
									"order" : 1,
									"source" : [ "obj-7", 0 ]
								}

							}
 ]
					}
,
					"patching_rect" : [ 2484.119659459995091, 1383.565736484528315, 50.5, 22.0 ],
					"saved_object_attributes" : 					{
						"description" : "",
						"digest" : "",
						"globalpatchername" : "",
						"tags" : ""
					}
,
					"text" : "p lifting"
				}

			}
, 			{
				"box" : 				{
					"angle" : 270.0,
					"bgcolor" : [ 0.2, 0.2, 0.2, 0.0 ],
					"border" : 1,
					"bordercolor" : [ 0.203921568627451, 0.203921568627451, 0.203921568627451, 1.0 ],
					"id" : "obj-61",
					"maxclass" : "panel",
					"mode" : 0,
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1502.119659459995091, 1171.91467399597218, 566.357452356411159, 262.222826004028548 ],
					"presentation" : 1,
					"presentation_rect" : [ 1036.0, 186.0, 198.0, 206.0 ],
					"proportion" : 0.5,
					"rounded" : 0
				}

			}
, 			{
				"box" : 				{
					"angle" : 270.0,
					"bgcolor" : [ 0.2, 0.2, 0.2, 0.0 ],
					"border" : 1,
					"bordercolor" : [ 0.203921568627451, 0.203921568627451, 0.203921568627451, 1.0 ],
					"id" : "obj-25",
					"maxclass" : "panel",
					"mode" : 0,
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 795.5, 1082.864686203003657, 118.357452356411159, 52.22282600402832 ],
					"presentation" : 1,
					"presentation_rect" : [ 831.0, 186.0, 200.0, 206.0 ],
					"proportion" : 0.5,
					"rounded" : 0
				}

			}
, 			{
				"box" : 				{
					"angle" : 270.0,
					"bgcolor" : [ 0.2, 0.2, 0.2, 0.0 ],
					"border" : 1,
					"bordercolor" : [ 0.203921568627451, 0.203921568627451, 0.203921568627451, 1.0 ],
					"id" : "obj-37",
					"maxclass" : "panel",
					"mode" : 0,
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 364.298385661637894, 344.484015460242972, 118.357452356411159, 52.22282600402832 ],
					"presentation" : 1,
					"presentation_rect" : [ 5.0, 185.776280635595583, 200.0, 206.0 ],
					"proportion" : 0.5,
					"rounded" : 0
				}

			}
, 			{
				"box" : 				{
					"angle" : 270.0,
					"bgcolor" : [ 0.2, 0.2, 0.2, 0.0 ],
					"border" : 1,
					"bordercolor" : [ 0.203921568627451, 0.203921568627451, 0.203921568627451, 1.0 ],
					"id" : "obj-93",
					"maxclass" : "panel",
					"mode" : 0,
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 341.642547643588841, 48.465057182312279, 144.357452356411159, 55.22282600402832 ],
					"presentation" : 1,
					"presentation_rect" : [ 412.0, 6.0, 200.0, 133.0 ],
					"proportion" : 0.5,
					"rounded" : 0
				}

			}
, 			{
				"box" : 				{
					"angle" : 270.0,
					"bgcolor" : [ 0.2, 0.2, 0.2, 1.0 ],
					"id" : "obj-72",
					"maxclass" : "panel",
					"mode" : 0,
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1065.727111839843474, 938.913812984739707, 149.75, 203.433695697784287 ],
					"proportion" : 0.5,
					"rounded" : 0
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-116",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "", "int", "int" ],
					"patching_rect" : [ 821.0, 308.441666666667288, 48.0, 22.0 ],
					"text" : "change"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-118",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"patching_rect" : [ 821.0, 280.775000000000546, 43.0, 22.0 ],
					"text" : ">= 0.6"
				}

			}
, 			{
				"box" : 				{
					"angle" : 270.0,
					"bgcolor" : [ 0.2, 0.2, 0.2, 1.0 ],
					"id" : "obj-44",
					"maxclass" : "panel",
					"mode" : 0,
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 123.977111839843474, 279.220833333333644, 119.5, 108.433695697784287 ],
					"proportion" : 0.5,
					"rounded" : 0
				}

			}
, 			{
				"box" : 				{
					"angle" : 270.0,
					"bgcolor" : [ 0.2, 0.2, 0.2, 1.0 ],
					"id" : "obj-127",
					"maxclass" : "panel",
					"mode" : 0,
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1308.77711182196208, 384.975012207031796, 136.5, 157.433695697784287 ],
					"proportion" : 0.5,
					"rounded" : 0
				}

			}
 ],
		"lines" : [ 			{
				"patchline" : 				{
					"destination" : [ "obj-132", 0 ],
					"midpoints" : [ 2246.5, 1200.0, 2246.5, 1200.0 ],
					"source" : [ "obj-1", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-134", 0 ],
					"midpoints" : [ 2229.0, 1200.0, 2229.0, 1200.0 ],
					"source" : [ "obj-1", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-138", 0 ],
					"midpoints" : [ 2211.5, 1200.0, 2205.0, 1200.0, 2205.0, 1209.0, 2117.13958740234375, 1209.0 ],
					"order" : 1,
					"source" : [ "obj-1", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-52", 0 ],
					"midpoints" : [ 2211.5, 1200.0, 2211.5, 1200.0 ],
					"order" : 0,
					"source" : [ "obj-1", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-57", 0 ],
					"midpoints" : [ 1068.321273821794421, 924.0, 1293.0, 924.0, 1293.0, 609.0, 1318.27711182196208, 609.0 ],
					"source" : [ "obj-105", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-70", 3 ],
					"midpoints" : [ 2172.13958740234375, 825.0, 2255.13958740234375, 825.0 ],
					"source" : [ "obj-107", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-70", 2 ],
					"midpoints" : [ 2117.13958740234375, 825.0, 2209.13958740234375, 825.0 ],
					"source" : [ "obj-107", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-112", 0 ],
					"midpoints" : [ 2161.13958740234375, 744.0, 2200.13958740234375, 744.0 ],
					"source" : [ "obj-11", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-122", 0 ],
					"midpoints" : [ 1810.13958740234375, 1008.0, 1810.13958740234375, 1008.0 ],
					"source" : [ "obj-110", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-70", 1 ],
					"midpoints" : [ 2255.13958740234375, 825.0, 2163.13958740234375, 825.0 ],
					"source" : [ "obj-112", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-70", 0 ],
					"midpoints" : [ 2200.13958740234375, 825.0, 2117.13958740234375, 825.0 ],
					"source" : [ "obj-112", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-119", 0 ],
					"midpoints" : [ 1645.5, 936.0, 1645.5, 936.0 ],
					"source" : [ "obj-113", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-53", 0 ],
					"midpoints" : [ 830.5, 333.0, 830.5, 333.0 ],
					"source" : [ "obj-116", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-109", 0 ],
					"midpoints" : [ 1825.5, 1377.0, 1825.5, 1377.0 ],
					"source" : [ "obj-117", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-116", 0 ],
					"midpoints" : [ 830.5, 303.0, 830.5, 303.0 ],
					"source" : [ "obj-118", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-51", 0 ],
					"midpoints" : [ 1810.13958740234375, 1035.0, 1810.13958740234375, 1035.0 ],
					"source" : [ "obj-122", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-9", 0 ],
					"midpoints" : [ 747.5, 600.0, 830.5, 600.0 ],
					"source" : [ "obj-123", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-54", 0 ],
					"midpoints" : [ 1011.5, 333.0, 1011.5, 333.0 ],
					"source" : [ "obj-125", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-125", 0 ],
					"midpoints" : [ 1011.5, 303.0, 1011.5, 303.0 ],
					"source" : [ "obj-126", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-123", 0 ],
					"midpoints" : [ 747.5, 489.0, 747.5, 489.0 ],
					"source" : [ "obj-128", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-81", 0 ],
					"midpoints" : [ 1035.5, 219.0, 854.5, 219.0 ],
					"order" : 1,
					"source" : [ "obj-131", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-88", 0 ],
					"midpoints" : [ 1035.5, 219.0, 1035.5, 219.0 ],
					"order" : 0,
					"source" : [ "obj-131", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-130", 0 ],
					"midpoints" : [ 2246.5, 1233.0, 2246.5, 1233.0 ],
					"source" : [ "obj-132", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-130", 0 ],
					"midpoints" : [ 2229.0, 1248.0, 2246.5, 1248.0 ],
					"source" : [ "obj-134", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-128", 0 ],
					"midpoints" : [ 747.5, 408.0, 747.5, 408.0 ],
					"source" : [ "obj-136", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-135", 0 ],
					"midpoints" : [ 2117.13958740234375, 1302.0, 2117.5, 1302.0 ],
					"source" : [ "obj-138", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-17", 0 ],
					"midpoints" : [ 387.5, 822.0, 387.5, 822.0 ],
					"source" : [ "obj-14", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-73", 0 ],
					"midpoints" : [ 2367.5, 1140.0, 2367.5, 1140.0 ],
					"source" : [ "obj-141", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-73", 0 ],
					"midpoints" : [ 2281.0, 1161.0, 2367.5, 1161.0 ],
					"source" : [ "obj-142", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-118", 0 ],
					"midpoints" : [ 830.5, 192.0, 830.5, 192.0 ],
					"order" : 1,
					"source" : [ "obj-164", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-165", 1 ],
					"midpoints" : [ 830.5, 273.0, 921.0, 273.0 ],
					"order" : 0,
					"source" : [ "obj-164", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-114", 0 ],
					"midpoints" : [ 830.5, 453.0, 1047.321273821794421, 453.0 ],
					"order" : 0,
					"source" : [ "obj-165", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-123", 1 ],
					"midpoints" : [ 830.5, 408.0, 830.5, 408.0 ],
					"order" : 2,
					"source" : [ "obj-165", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-90", 0 ],
					"midpoints" : [ 830.5, 453.0, 935.821273821794421, 453.0 ],
					"order" : 1,
					"source" : [ "obj-165", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-48", 0 ],
					"midpoints" : [ 1192.5, 159.0, 1192.5, 159.0 ],
					"source" : [ "obj-166", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-82", 0 ],
					"midpoints" : [ 1283.0, 159.0, 1283.0, 159.0 ],
					"source" : [ "obj-166", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-10", 0 ],
					"midpoints" : [ 387.5, 960.0, 387.5, 960.0 ],
					"order" : 3,
					"source" : [ "obj-17", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-166", 0 ],
					"midpoints" : [ 387.5, 969.0, 723.0, 969.0, 723.0, 120.0, 1192.5, 120.0 ],
					"order" : 0,
					"source" : [ "obj-17", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-74", 0 ],
					"midpoints" : [ 387.5, 969.0, 723.0, 969.0, 723.0, 129.0, 830.5, 129.0 ],
					"order" : 1,
					"source" : [ "obj-17", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-92", 0 ],
					"midpoints" : [ 387.5, 993.0, 492.821273821794421, 993.0 ],
					"order" : 2,
					"source" : [ "obj-17", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-201", 1 ],
					"midpoints" : [ 217.499999999999972, 672.0, 217.499999999999972, 672.0 ],
					"source" : [ "obj-175", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-201", 0 ],
					"midpoints" : [ 38.5, 672.0, 38.5, 672.0 ],
					"source" : [ "obj-175", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-14", 0 ],
					"midpoints" : [ 387.5, 600.0, 387.5, 600.0 ],
					"source" : [ "obj-190", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-31", 0 ],
					"midpoints" : [ 38.5, 180.0, 38.5, 180.0 ],
					"order" : 1,
					"source" : [ "obj-192", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-83", 0 ],
					"midpoints" : [ 38.5, 237.0, 399.477111839843474, 237.0 ],
					"order" : 0,
					"source" : [ "obj-192", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-12", 0 ],
					"midpoints" : [ 38.5, 891.0, 38.5, 891.0 ],
					"source" : [ "obj-201", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-110", 0 ],
					"midpoints" : [ 1810.13958740234375, 933.0, 1810.13958740234375, 933.0 ],
					"source" : [ "obj-226", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-32", 3 ],
					"midpoints" : [ 1690.048413902521133, 1275.0, 1690.35211181640625, 1275.0 ],
					"source" : [ "obj-24", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-50", 0 ],
					"midpoints" : [ 2569.5, 1029.0, 2569.5, 1029.0 ],
					"source" : [ "obj-26", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-27", 0 ],
					"midpoints" : [ 2211.5, 1344.0, 2211.5, 1344.0 ],
					"order" : 1,
					"source" : [ "obj-29", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-28", 0 ],
					"midpoints" : [ 2211.5, 1368.0, 2315.5, 1368.0 ],
					"order" : 0,
					"source" : [ "obj-29", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-29", 0 ],
					"midpoints" : [ 2211.5, 1275.0, 2211.5, 1275.0 ],
					"source" : [ "obj-30", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-129", 0 ],
					"midpoints" : [ 38.5, 450.0, 15.0, 450.0, 15.0, 1089.0, 38.5, 1089.0 ],
					"order" : 0,
					"source" : [ "obj-31", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-175", 1 ],
					"midpoints" : [ 102.5, 450.0, 217.499999999999972, 450.0 ],
					"order" : 1,
					"source" : [ "obj-31", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-175", 0 ],
					"midpoints" : [ 38.5, 303.0, 38.5, 303.0 ],
					"order" : 2,
					"source" : [ "obj-31", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-190", 1 ],
					"midpoints" : [ 102.5, 450.0, 566.5, 450.0 ],
					"order" : 0,
					"source" : [ "obj-31", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-190", 0 ],
					"midpoints" : [ 38.5, 450.0, 387.5, 450.0 ],
					"order" : 1,
					"source" : [ "obj-31", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-8", 0 ],
					"midpoints" : [ 1536.97711181640625, 1311.0, 1536.97711181640625, 1311.0 ],
					"source" : [ "obj-32", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-32", 4 ],
					"midpoints" : [ 1741.97711181640625, 1218.0, 1741.47711181640625, 1218.0 ],
					"source" : [ "obj-36", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-107", 0 ],
					"midpoints" : [ 2168.13958740234375, 408.0, 2117.13958740234375, 408.0 ],
					"source" : [ "obj-38", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-69", 0 ],
					"midpoints" : [ 1999.0, 1239.0, 1945.0, 1239.0 ],
					"source" : [ "obj-4", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-32", 7 ],
					"midpoints" : [ 1894.0, 1275.0, 1894.85211181640625, 1275.0 ],
					"source" : [ "obj-40", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-20", 0 ],
					"midpoints" : [ 399.477111839843474, 333.0, 399.477111839843474, 333.0 ],
					"order" : 1,
					"source" : [ "obj-43", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-60", 0 ],
					"midpoints" : [ 399.477111839843474, 333.0, 507.5, 333.0 ],
					"order" : 0,
					"source" : [ "obj-43", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-59", 0 ],
					"midpoints" : [ 830.5, 1071.0, 830.678726178205579, 1071.0 ],
					"source" : [ "obj-46", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-165", 4 ],
					"midpoints" : [ 1192.5, 186.0, 1192.5, 186.0 ],
					"source" : [ "obj-48", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-1", 0 ],
					"midpoints" : [ 1867.63958740234375, 1158.0, 2211.5, 1158.0 ],
					"order" : 1,
					"source" : [ "obj-51", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-142", 0 ],
					"midpoints" : [ 1867.63958740234375, 1104.0, 2281.0, 1104.0 ],
					"order" : 0,
					"source" : [ "obj-51", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-67", 0 ],
					"midpoints" : [ 1810.13958740234375, 1236.0, 1503.0, 1236.0, 1503.0, 1392.0, 1569.977111839843474, 1392.0 ],
					"source" : [ "obj-51", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-30", 0 ],
					"midpoints" : [ 2211.5, 1248.0, 2211.5, 1248.0 ],
					"source" : [ "obj-52", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-165", 0 ],
					"midpoints" : [ 830.5, 369.0, 830.5, 369.0 ],
					"source" : [ "obj-53", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-165", 2 ],
					"midpoints" : [ 1011.5, 369.0, 1011.5, 369.0 ],
					"source" : [ "obj-54", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-89", 0 ],
					"midpoints" : [ 2493.619659459995091, 1245.0, 2493.619659459995091, 1245.0 ],
					"source" : [ "obj-56", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-105", 0 ],
					"midpoints" : [ 830.678726178205579, 1131.0, 1044.0, 1131.0, 1044.0, 609.0, 1068.321273821794421, 609.0 ],
					"source" : [ "obj-59", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-32", 6 ],
					"midpoints" : [ 1843.13958740234375, 1218.0, 1843.72711181640625, 1218.0 ],
					"source" : [ "obj-62", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-32", 5 ],
					"midpoints" : [ 1792.63958740234375, 1275.0, 1792.60211181640625, 1275.0 ],
					"source" : [ "obj-63", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-32", 2 ],
					"midpoints" : [ 1639.933633089065552, 1218.0, 1639.22711181640625, 1218.0 ],
					"source" : [ "obj-64", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-32", 1 ],
					"midpoints" : [ 1588.97711181640625, 1275.0, 1588.10211181640625, 1275.0 ],
					"source" : [ "obj-65", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-67", 0 ],
					"midpoints" : [ 1569.97711181640625, 1377.0, 1569.977111839843474, 1377.0 ],
					"source" : [ "obj-66", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-8", 0 ],
					"midpoints" : [ 1536.97711181640625, 1377.0, 1512.0, 1377.0, 1512.0, 1314.0, 1536.97711181640625, 1314.0 ],
					"source" : [ "obj-66", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-69", 0 ],
					"midpoints" : [ 1945.0, 1218.0, 1945.0, 1218.0 ],
					"source" : [ "obj-68", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-32", 8 ],
					"midpoints" : [ 1945.0, 1278.0, 1945.97711181640625, 1278.0 ],
					"order" : 0,
					"source" : [ "obj-69", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-68", 0 ],
					"midpoints" : [ 1945.0, 1278.0, 1932.0, 1278.0, 1932.0, 1191.0, 1945.0, 1191.0 ],
					"order" : 1,
					"source" : [ "obj-69", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-32", 0 ],
					"midpoints" : [ 1536.97711181640625, 1275.0, 1536.97711181640625, 1275.0 ],
					"source" : [ "obj-7", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-139", 0 ],
					"midpoints" : [ 2117.13958740234375, 864.0, 2117.13958740234375, 864.0 ],
					"source" : [ "obj-70", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-94", 0 ],
					"midpoints" : [ 2367.5, 1245.0, 2367.13958740234375, 1245.0 ],
					"source" : [ "obj-73", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-164", 0 ],
					"midpoints" : [ 830.5, 159.0, 830.5, 159.0 ],
					"source" : [ "obj-74", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-97", 0 ],
					"midpoints" : [ 1011.900000035762787, 159.0, 1011.5, 159.0 ],
					"source" : [ "obj-74", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-10", 0 ],
					"midpoints" : [ 492.821273821794421, 1122.0, 429.0, 1122.0, 429.0, 993.0, 387.5, 993.0 ],
					"order" : 2,
					"source" : [ "obj-79", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-166", 0 ],
					"midpoints" : [ 492.821273821794421, 1131.0, 723.0, 1131.0, 723.0, 120.0, 1192.5, 120.0 ],
					"order" : 0,
					"source" : [ "obj-79", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-74", 0 ],
					"midpoints" : [ 492.821273821794421, 1131.0, 723.0, 1131.0, 723.0, 129.0, 830.5, 129.0 ],
					"order" : 1,
					"source" : [ "obj-79", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-117", 0 ],
					"midpoints" : [ 1536.97711181640625, 1350.0, 1825.5, 1350.0 ],
					"order" : 0,
					"source" : [ "obj-8", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-18", 1 ],
					"midpoints" : [ 1536.97711181640625, 1350.0, 1797.977111839843474, 1350.0 ],
					"order" : 1,
					"source" : [ "obj-8", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-66", 0 ],
					"midpoints" : [ 1536.97711181640625, 1344.0, 1536.97711181640625, 1344.0 ],
					"order" : 2,
					"source" : [ "obj-8", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-118", 1 ],
					"midpoints" : [ 854.5, 264.0, 854.5, 264.0 ],
					"source" : [ "obj-81", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-165", 5 ],
					"midpoints" : [ 1283.0, 186.0, 1283.0, 186.0 ],
					"source" : [ "obj-82", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-84", 0 ],
					"midpoints" : [ 399.477111839843474, 273.0, 399.477111839843474, 273.0 ],
					"source" : [ "obj-83", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-43", 0 ],
					"midpoints" : [ 399.477111839843474, 303.0, 399.477111839843474, 303.0 ],
					"source" : [ "obj-84", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-56", 0 ],
					"midpoints" : [ 2493.619659459995091, 1212.0, 2493.619659459995091, 1212.0 ],
					"source" : [ "obj-85", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-114", 0 ],
					"midpoints" : [ 935.821273821794421, 549.0, 1023.0, 549.0, 1023.0, 462.0, 1047.321273821794421, 462.0 ],
					"order" : 0,
					"source" : [ "obj-86", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-9", 0 ],
					"midpoints" : [ 935.821273821794421, 549.0, 830.5, 549.0 ],
					"order" : 1,
					"source" : [ "obj-86", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-126", 1 ],
					"midpoints" : [ 1035.5, 264.0, 1035.5, 264.0 ],
					"source" : [ "obj-88", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-47", 0 ],
					"midpoints" : [ 2493.619659459995091, 1275.0, 2493.619659459995091, 1275.0 ],
					"source" : [ "obj-89", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-105", 0 ],
					"midpoints" : [ 830.5, 915.0, 807.0, 915.0, 807.0, 600.0, 1068.321273821794421, 600.0 ],
					"order" : 1,
					"source" : [ "obj-9", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-46", 0 ],
					"midpoints" : [ 830.5, 915.0, 830.5, 915.0 ],
					"order" : 0,
					"source" : [ "obj-9", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-86", 0 ],
					"midpoints" : [ 935.821273821794421, 492.0, 935.821273821794421, 492.0 ],
					"source" : [ "obj-90", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-79", 0 ],
					"midpoints" : [ 492.821273821794421, 1071.0, 492.821273821794421, 1071.0 ],
					"source" : [ "obj-92", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-29", 0 ],
					"midpoints" : [ 2367.13958740234375, 1305.0, 2211.5, 1305.0 ],
					"source" : [ "obj-94", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-126", 0 ],
					"midpoints" : [ 1011.5, 189.0, 1011.5, 189.0 ],
					"order" : 1,
					"source" : [ "obj-97", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-165", 3 ],
					"midpoints" : [ 1011.5, 273.0, 1102.0, 273.0 ],
					"order" : 0,
					"source" : [ "obj-97", 0 ]
				}

			}
 ],
		"parameters" : 		{
			"obj-105::obj-107::obj-33" : [ "tab[162]", "tab[1]", 0 ],
			"obj-105::obj-123::obj-33" : [ "tab[16]", "tab[1]", 0 ],
			"obj-105::obj-34::obj-33" : [ "tab[163]", "tab[1]", 0 ],
			"obj-105::obj-36::obj-33" : [ "tab[164]", "tab[1]", 0 ],
			"obj-105::obj-40::obj-33" : [ "tab[154]", "tab[1]", 0 ],
			"obj-105::obj-41::obj-33" : [ "tab[161]", "tab[1]", 0 ],
			"obj-105::obj-42::obj-33" : [ "tab[165]", "tab[1]", 0 ],
			"obj-105::obj-43::obj-33" : [ "tab[166]", "tab[1]", 0 ],
			"obj-105::obj-44::obj-33" : [ "tab[167]", "tab[1]", 0 ],
			"obj-105::obj-45::obj-33" : [ "tab[168]", "tab[1]", 0 ],
			"obj-105::obj-46::obj-33" : [ "tab[169]", "tab[1]", 0 ],
			"obj-105::obj-47::obj-33" : [ "tab[170]", "tab[1]", 0 ],
			"obj-105::obj-48::obj-33" : [ "tab[160]", "tab[1]", 0 ],
			"obj-105::obj-49::obj-33" : [ "tab[171]", "tab[1]", 0 ],
			"obj-105::obj-50::obj-33" : [ "tab[172]", "tab[1]", 0 ],
			"obj-105::obj-74::obj-33" : [ "tab[153]", "tab[1]", 0 ],
			"obj-12::obj-45" : [ "live.gain~[1]", "live.gain~", 0 ],
			"obj-14::obj-107::obj-33" : [ "tab[2]", "tab[1]", 0 ],
			"obj-14::obj-123::obj-33" : [ "tab[155]", "tab[1]", 0 ],
			"obj-14::obj-34::obj-33" : [ "tab[3]", "tab[1]", 0 ],
			"obj-14::obj-36::obj-33" : [ "tab[4]", "tab[1]", 0 ],
			"obj-14::obj-40::obj-33" : [ "tab[5]", "tab[1]", 0 ],
			"obj-14::obj-41::obj-33" : [ "tab[6]", "tab[1]", 0 ],
			"obj-14::obj-42::obj-33" : [ "tab[7]", "tab[1]", 0 ],
			"obj-14::obj-43::obj-33" : [ "tab[8]", "tab[1]", 0 ],
			"obj-14::obj-44::obj-33" : [ "tab[9]", "tab[1]", 0 ],
			"obj-14::obj-45::obj-33" : [ "tab[10]", "tab[1]", 0 ],
			"obj-14::obj-46::obj-33" : [ "tab[11]", "tab[1]", 0 ],
			"obj-14::obj-47::obj-33" : [ "tab[12]", "tab[1]", 0 ],
			"obj-14::obj-48::obj-33" : [ "tab[13]", "tab[1]", 0 ],
			"obj-14::obj-49::obj-33" : [ "tab[14]", "tab[1]", 0 ],
			"obj-14::obj-50::obj-33" : [ "tab[17]", "tab[1]", 0 ],
			"obj-14::obj-74::obj-33" : [ "tab[1]", "tab[1]", 0 ],
			"obj-175::obj-107::obj-33" : [ "tab[143]", "tab[1]", 0 ],
			"obj-175::obj-123::obj-33" : [ "tab[31]", "tab[1]", 0 ],
			"obj-175::obj-36::obj-33" : [ "tab[145]", "tab[1]", 0 ],
			"obj-175::obj-40::obj-33" : [ "tab[146]", "tab[1]", 0 ],
			"obj-175::obj-41::obj-33" : [ "tab[147]", "tab[1]", 0 ],
			"obj-175::obj-44::obj-33" : [ "tab[150]", "tab[1]", 0 ],
			"obj-175::obj-45::obj-33" : [ "tab[151]", "tab[1]", 0 ],
			"obj-175::obj-46::obj-33" : [ "tab[156]", "tab[1]", 0 ],
			"obj-175::obj-47::obj-33" : [ "tab[157]", "tab[1]", 0 ],
			"obj-175::obj-48::obj-33" : [ "tab[158]", "tab[1]", 0 ],
			"obj-175::obj-49::obj-33" : [ "tab[152]", "tab[1]", 0 ],
			"obj-175::obj-50::obj-33" : [ "tab[159]", "tab[1]", 0 ],
			"obj-175::obj-6::obj-33" : [ "tab[149]", "tab[1]", 0 ],
			"obj-175::obj-74::obj-33" : [ "tab[142]", "tab[1]", 0 ],
			"obj-175::obj-8::obj-33" : [ "tab[148]", "tab[1]", 0 ],
			"obj-175::obj-9::obj-33" : [ "tab[144]", "tab[1]", 0 ],
			"obj-57::obj-107::obj-27::obj-18" : [ "toggle[68]", "toggle", 0 ],
			"obj-57::obj-107::obj-48" : [ "SendTo-TXT[31]", "SendTo-TXT", 0 ],
			"obj-57::obj-107::obj-8" : [ "tab[138]", "tab[1]", 0 ],
			"obj-57::obj-123::obj-27::obj-18" : [ "toggle[6]", "toggle", 0 ],
			"obj-57::obj-123::obj-48" : [ "SendTo-TXT", "SendTo-TXT", 0 ],
			"obj-57::obj-123::obj-8" : [ "tab[94]", "tab[1]", 0 ],
			"obj-57::obj-34::obj-27::obj-18" : [ "toggle[69]", "toggle", 0 ],
			"obj-57::obj-34::obj-48" : [ "SendTo-TXT[32]", "SendTo-TXT", 0 ],
			"obj-57::obj-34::obj-8" : [ "tab[116]", "tab[1]", 0 ],
			"obj-57::obj-36::obj-27::obj-18" : [ "toggle[70]", "toggle", 0 ],
			"obj-57::obj-36::obj-48" : [ "SendTo-TXT[30]", "SendTo-TXT", 0 ],
			"obj-57::obj-36::obj-8" : [ "tab[117]", "tab[1]", 0 ],
			"obj-57::obj-40::obj-27::obj-18" : [ "toggle[67]", "toggle", 0 ],
			"obj-57::obj-40::obj-48" : [ "SendTo-TXT[33]", "SendTo-TXT", 0 ],
			"obj-57::obj-40::obj-8" : [ "tab[139]", "tab[1]", 0 ],
			"obj-57::obj-41::obj-27::obj-18" : [ "toggle[71]", "toggle", 0 ],
			"obj-57::obj-41::obj-48" : [ "SendTo-TXT[34]", "SendTo-TXT", 0 ],
			"obj-57::obj-41::obj-8" : [ "tab[118]", "tab[1]", 0 ],
			"obj-57::obj-42::obj-27::obj-18" : [ "toggle[72]", "toggle", 0 ],
			"obj-57::obj-42::obj-48" : [ "SendTo-TXT[35]", "SendTo-TXT", 0 ],
			"obj-57::obj-42::obj-8" : [ "tab[119]", "tab[1]", 0 ],
			"obj-57::obj-43::obj-27::obj-18" : [ "toggle[73]", "toggle", 0 ],
			"obj-57::obj-43::obj-48" : [ "SendTo-TXT[36]", "SendTo-TXT", 0 ],
			"obj-57::obj-43::obj-8" : [ "tab[120]", "tab[1]", 0 ],
			"obj-57::obj-44::obj-27::obj-18" : [ "toggle[74]", "toggle", 0 ],
			"obj-57::obj-44::obj-48" : [ "SendTo-TXT[37]", "SendTo-TXT", 0 ],
			"obj-57::obj-44::obj-8" : [ "tab[121]", "tab[1]", 0 ],
			"obj-57::obj-45::obj-27::obj-18" : [ "toggle[75]", "toggle", 0 ],
			"obj-57::obj-45::obj-48" : [ "SendTo-TXT[38]", "SendTo-TXT", 0 ],
			"obj-57::obj-45::obj-8" : [ "tab[122]", "tab[1]", 0 ],
			"obj-57::obj-46::obj-27::obj-18" : [ "toggle[76]", "toggle", 0 ],
			"obj-57::obj-46::obj-48" : [ "SendTo-TXT[39]", "SendTo-TXT", 0 ],
			"obj-57::obj-46::obj-8" : [ "tab[123]", "tab[1]", 0 ],
			"obj-57::obj-47::obj-27::obj-18" : [ "toggle[77]", "toggle", 0 ],
			"obj-57::obj-47::obj-48" : [ "SendTo-TXT[40]", "SendTo-TXT", 0 ],
			"obj-57::obj-47::obj-8" : [ "tab[124]", "tab[1]", 0 ],
			"obj-57::obj-48::obj-27::obj-18" : [ "toggle[78]", "toggle", 0 ],
			"obj-57::obj-48::obj-48" : [ "SendTo-TXT[41]", "SendTo-TXT", 0 ],
			"obj-57::obj-48::obj-8" : [ "tab[125]", "tab[1]", 0 ],
			"obj-57::obj-49::obj-27::obj-18" : [ "toggle[79]", "toggle", 0 ],
			"obj-57::obj-49::obj-48" : [ "SendTo-TXT[42]", "SendTo-TXT", 0 ],
			"obj-57::obj-49::obj-8" : [ "tab[140]", "tab[1]", 0 ],
			"obj-57::obj-50::obj-27::obj-18" : [ "toggle[64]", "toggle", 0 ],
			"obj-57::obj-50::obj-48" : [ "SendTo-TXT[43]", "SendTo-TXT", 0 ],
			"obj-57::obj-50::obj-8" : [ "tab[141]", "tab[1]", 0 ],
			"obj-57::obj-74::obj-27::obj-18" : [ "toggle[66]", "toggle", 0 ],
			"obj-57::obj-74::obj-48" : [ "SendTo-TXT[29]", "SendTo-TXT", 0 ],
			"obj-57::obj-74::obj-8" : [ "tab[137]", "tab[1]", 0 ],
			"obj-83::obj-107::obj-33" : [ "tab[20]", "tab[1]", 0 ],
			"obj-83::obj-123::obj-33" : [ "tab[18]", "tab[1]", 0 ],
			"obj-83::obj-34::obj-33" : [ "tab[21]", "tab[1]", 0 ],
			"obj-83::obj-36::obj-33" : [ "tab[22]", "tab[1]", 0 ],
			"obj-83::obj-40::obj-33" : [ "tab[23]", "tab[1]", 0 ],
			"obj-83::obj-41::obj-33" : [ "tab[24]", "tab[1]", 0 ],
			"obj-83::obj-42::obj-33" : [ "tab[15]", "tab[1]", 0 ],
			"obj-83::obj-43::obj-33" : [ "tab[25]", "tab[1]", 0 ],
			"obj-83::obj-44::obj-33" : [ "tab[26]", "tab[1]", 0 ],
			"obj-83::obj-45::obj-33" : [ "tab[27]", "tab[1]", 0 ],
			"obj-83::obj-46::obj-33" : [ "tab[28]", "tab[1]", 0 ],
			"obj-83::obj-47::obj-33" : [ "tab[29]", "tab[1]", 0 ],
			"obj-83::obj-48::obj-33" : [ "tab[30]", "tab[1]", 0 ],
			"obj-83::obj-49::obj-33" : [ "tab[32]", "tab[1]", 0 ],
			"obj-83::obj-50::obj-33" : [ "tab[33]", "tab[1]", 0 ],
			"obj-83::obj-74::obj-33" : [ "tab[19]", "tab[1]", 0 ],
			"parameterbanks" : 			{
				"0" : 				{
					"index" : 0,
					"name" : "",
					"parameters" : [ "-", "-", "-", "-", "-", "-", "-", "-" ]
				}

			}
,
			"parameter_overrides" : 			{
				"obj-12::obj-45" : 				{
					"parameter_longname" : "live.gain~[1]"
				}

			}
,
			"inherited_shortname" : 1
		}
,
		"dependency_cache" : [ 			{
				"name" : "abc_2d_encoder3~.mxo",
				"type" : "iLaX"
			}
, 			{
				"name" : "bbdmi.2max.maxpat",
				"bootpath" : "~/Documents/Max 8/Packages/bbdmi/max/output/2max",
				"patcherrelativepath" : "../../../max/output/2max",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bbdmi.average.maxpat",
				"bootpath" : "~/Documents/Max 8/Packages/bbdmi/max/control_processing/average",
				"patcherrelativepath" : "../../../max/control_processing/average",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bbdmi.buffer~.maxpat",
				"bootpath" : "~/Documents/Max 8/Packages/bbdmi/max/utilities/buffer~",
				"patcherrelativepath" : "../../../max/utilities/buffer~",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bbdmi.calibrate.maxpat",
				"bootpath" : "~/Documents/Max 8/Packages/bbdmi/max/control_processing/calibrate",
				"patcherrelativepath" : "../../../max/control_processing/calibrate",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bbdmi.crosspatch.maxpat",
				"bootpath" : "~/Documents/Max 8/Packages/bbdmi/max/utilities/crosspatch",
				"patcherrelativepath" : "../../../max/utilities/crosspatch",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bbdmi.crosspatch~.maxpat",
				"bootpath" : "~/Documents/Max 8/Packages/bbdmi/max/utilities/crosspatch~",
				"patcherrelativepath" : "../../../max/utilities/crosspatch~",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bbdmi.dac~.maxpat",
				"bootpath" : "~/Documents/Max 8/Packages/bbdmi/max/utilities/dac~",
				"patcherrelativepath" : "../../../max/utilities/dac~",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bbdmi.eavi.maxpat",
				"bootpath" : "~/Documents/Max 8/Packages/bbdmi/max/input/eavi",
				"patcherrelativepath" : "../../../max/input/eavi",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bbdmi.granulator~.maxpat",
				"bootpath" : "~/Documents/Max 8/Packages/bbdmi/max/sound_synthesis/granulator~",
				"patcherrelativepath" : "../../../max/sound_synthesis/granulator~",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bbdmi.latch.maxpat",
				"bootpath" : "~/Documents/Max 8/Packages/bbdmi/max/control_processing/latch",
				"patcherrelativepath" : "../../../max/control_processing/latch",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bbdmi.list2~.maxpat",
				"bootpath" : "~/Documents/Max 8/Packages/bbdmi/max/utilities/list2~",
				"patcherrelativepath" : "../../../max/utilities/list2~",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bbdmi.onepole~.maxpat",
				"bootpath" : "~/Documents/Max 8/Packages/bbdmi/max/utilities/onepole~",
				"patcherrelativepath" : "../../../max/utilities/onepole~",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bbdmi.rms~.maxpat",
				"bootpath" : "~/Documents/Max 8/Packages/bbdmi/max/feature_extraction/rms~",
				"patcherrelativepath" : "../../../max/feature_extraction/rms~",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bbdmi.scale.maxpat",
				"bootpath" : "~/Documents/Max 8/Packages/bbdmi/max/control_processing/scale",
				"patcherrelativepath" : "../../../max/control_processing/scale",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bbdmi.speedlim.maxpat",
				"bootpath" : "~/Documents/Max 8/Packages/bbdmi/max/control_processing/speedlim",
				"patcherrelativepath" : "../../../max/control_processing/speedlim",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "lifting-solo.json",
				"bootpath" : "~/Documents/Max 8/Packages/bbdmi/events/2022-12-08_MSH/patches",
				"patcherrelativepath" : ".",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "name.js",
				"bootpath" : "~/Documents/Max 8/Packages/bbdmi/max/source/js",
				"patcherrelativepath" : "../../../max/source/js",
				"type" : "TEXT",
				"implicit" : 1
			}
, 			{
				"name" : "p.2max.maxpat",
				"bootpath" : "~/Documents/Max 8/Packages/bbdmi/max/output/2max",
				"patcherrelativepath" : "../../../max/output/2max",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "p.calibrate.maxpat",
				"bootpath" : "~/Documents/Max 8/Packages/bbdmi/max/control_processing/calibrate",
				"patcherrelativepath" : "../../../max/control_processing/calibrate",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "p.exposer.maxpat",
				"bootpath" : "~/Documents/Max 8/Packages/bbdmi/max/source/patchers",
				"patcherrelativepath" : "../../../max/source/patchers",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "p.gran~.maxpat",
				"bootpath" : "~/Documents/Max 8/Packages/bbdmi/max/sound_synthesis/granulator~",
				"patcherrelativepath" : "../../../max/sound_synthesis/granulator~",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "p.onepole~.maxpat",
				"bootpath" : "~/Documents/Max 8/Packages/bbdmi/max/utilities/onepole~",
				"patcherrelativepath" : "../../../max/utilities/onepole~",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "p.scale.maxpat",
				"bootpath" : "~/Documents/Max 8/Packages/bbdmi/max/control_processing/scale",
				"patcherrelativepath" : "../../../max/control_processing/scale",
				"type" : "JSON",
				"implicit" : 1
			}
 ],
		"autosave" : 0,
		"boxgroups" : [ 			{
				"boxes" : [ "obj-44", "obj-21", "obj-16", "obj-2", "obj-33" ]
			}
, 			{
				"boxes" : [ "obj-61", "obj-58", "obj-8", "obj-109", "obj-117", "obj-67", "obj-32", "obj-18", "obj-69", "obj-63", "obj-40", "obj-19", "obj-24", "obj-66", "obj-65", "obj-7", "obj-68", "obj-64", "obj-36", "obj-62", "obj-4" ]
			}
 ],
		"accentcolor" : [ 0.298039215686275, 0.372549019607843, 0.996078431372549, 1.0 ]
	}

}
