{
	"patcher" : 	{
		"fileversion" : 1,
		"appversion" : 		{
			"major" : 8,
			"minor" : 5,
			"revision" : 3,
			"architecture" : "x64",
			"modernui" : 1
		}
,
		"classnamespace" : "box",
		"rect" : [ 28.0, 66.0, 170.0, 237.0 ],
		"openrect" : [ 0.0, 0.0, 170.0, 236.090328454971313 ],
		"bglocked" : 0,
		"openinpresentation" : 1,
		"default_fontsize" : 12.0,
		"default_fontface" : 0,
		"default_fontname" : "Ableton Sans Medium",
		"gridonopen" : 1,
		"gridsize" : [ 15.0, 15.0 ],
		"gridsnaponopen" : 1,
		"objectsnaponopen" : 1,
		"statusbarvisible" : 2,
		"toolbarvisible" : 1,
		"lefttoolbarpinned" : 0,
		"toptoolbarpinned" : 0,
		"righttoolbarpinned" : 0,
		"bottomtoolbarpinned" : 0,
		"toolbars_unpinned_last_save" : 3,
		"tallnewobj" : 0,
		"boxanimatetime" : 200,
		"enablehscroll" : 1,
		"enablevscroll" : 1,
		"devicewidth" : 0.0,
		"description" : "",
		"digest" : "",
		"tags" : "",
		"style" : "",
		"subpatcher_template" : "",
		"assistshowspatchername" : 0,
		"boxes" : [ 			{
				"box" : 				{
					"id" : "obj-24",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "bang", "bang" ],
					"patching_rect" : [ 776.455412386952958, 91.0, 33.0, 23.0 ],
					"text" : "t b b"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 11.595186999999999,
					"id" : "obj-25",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 970.0, 224.0, 188.0, 21.0 ],
					"text" : "window flags nofloat, window exec"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-34",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1285.0, 200.0, 154.0, 21.0 ],
					"text" : "Hides Max runtime window."
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-38",
					"linecount" : 2,
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 813.0, 184.0, 150.0, 35.0 ],
					"text" : "Hides Max from task bar and task manager."
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-39",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1265.0, 223.0, 86.0, 23.0 ],
					"text" : "statusvisible 0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-97",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 776.455412386952958, 30.0, 58.0, 23.0 ],
					"text" : "loadbang"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 11.595186999999999,
					"id" : "obj-96",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 790.0, 224.0, 173.0, 21.0 ],
					"text" : "window flags float, window exec"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-94",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 790.0, 267.0, 70.0, 23.0 ],
					"save" : [ "#N", "thispatcher", ";", "#Q", "end", ";" ],
					"text" : "thispatcher"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-37",
					"maxclass" : "newobj",
					"numinlets" : 5,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 323.0, 490.605667587303401, 110.0, 23.0 ],
					"text" : "zmap -1 1 0 16384"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Ableton Sans Medium",
					"fontsize" : 12.0,
					"id" : "obj-10",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patcher" : 					{
						"fileversion" : 1,
						"appversion" : 						{
							"major" : 8,
							"minor" : 5,
							"revision" : 3,
							"architecture" : "x64",
							"modernui" : 1
						}
,
						"classnamespace" : "box",
						"rect" : [ 59.0, 104.0, 221.0, 350.0 ],
						"bglocked" : 0,
						"openinpresentation" : 0,
						"default_fontsize" : 12.0,
						"default_fontface" : 0,
						"default_fontname" : "Arial",
						"gridonopen" : 1,
						"gridsize" : [ 15.0, 15.0 ],
						"gridsnaponopen" : 1,
						"objectsnaponopen" : 1,
						"statusbarvisible" : 2,
						"toolbarvisible" : 1,
						"lefttoolbarpinned" : 0,
						"toptoolbarpinned" : 0,
						"righttoolbarpinned" : 0,
						"bottomtoolbarpinned" : 0,
						"toolbars_unpinned_last_save" : 0,
						"tallnewobj" : 0,
						"boxanimatetime" : 200,
						"enablehscroll" : 1,
						"enablevscroll" : 1,
						"devicewidth" : 0.0,
						"description" : "",
						"digest" : "",
						"tags" : "",
						"style" : "",
						"subpatcher_template" : "",
						"assistshowspatchername" : 0,
						"boxes" : [ 							{
								"box" : 								{
									"id" : "obj-4",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 78.0, 265.0, 51.0, 22.0 ],
									"text" : "pak 0 0."
								}

							}
, 							{
								"box" : 								{
									"fontsize" : 12.0,
									"id" : "obj-1",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"patching_rect" : [ 78.0, 204.0, 29.5, 22.0 ],
									"text" : "+ 2"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 12.0,
									"id" : "obj-18",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "", "" ],
									"patching_rect" : [ 154.0, 204.0, 60.0, 22.0 ],
									"text" : "zl lookup"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 12.0,
									"id" : "obj-119",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "int", "int" ],
									"patching_rect" : [ 67.0, 168.0, 30.0, 22.0 ],
									"text" : "t i i"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 12.0,
									"id" : "obj-19",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 67.0, 143.0, 28.0, 22.0 ],
									"text" : "iter"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 12.0,
									"id" : "obj-6",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 3,
									"outlettype" : [ "", "", "" ],
									"patching_rect" : [ 67.0, 59.0, 40.0, 22.0 ],
									"text" : "t l l l"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 12.0,
									"id" : "obj-25",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "", "" ],
									"patching_rect" : [ 14.0, 112.0, 72.0, 22.0 ],
									"text" : "zl compare"
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-7",
									"index" : 1,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 66.999998737861631, 11.0, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-8",
									"index" : 1,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 78.0, 308.0, 30.0, 30.0 ]
								}

							}
 ],
						"lines" : [ 							{
								"patchline" : 								{
									"destination" : [ "obj-4", 0 ],
									"source" : [ "obj-1", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-1", 0 ],
									"source" : [ "obj-119", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-18", 0 ],
									"midpoints" : [ 76.5, 198.0, 163.5, 198.0 ],
									"source" : [ "obj-119", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-4", 1 ],
									"midpoints" : [ 163.5, 261.0, 119.5, 261.0 ],
									"source" : [ "obj-18", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-119", 0 ],
									"source" : [ "obj-19", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-19", 0 ],
									"source" : [ "obj-25", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-8", 0 ],
									"source" : [ "obj-4", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"color" : [ 0.0, 0.345098, 0.592157, 1.0 ],
									"destination" : [ "obj-18", 1 ],
									"midpoints" : [ 97.5, 151.0, 204.5, 151.0 ],
									"source" : [ "obj-6", 2 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-25", 0 ],
									"midpoints" : [ 87.0, 95.0, 23.5, 95.0 ],
									"source" : [ "obj-6", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-25", 1 ],
									"source" : [ "obj-6", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-6", 0 ],
									"source" : [ "obj-7", 0 ]
								}

							}
 ],
						"styles" : [ 							{
								"name" : "AudioStatus_Menu",
								"default" : 								{
									"bgfillcolor" : 									{
										"angle" : 270.0,
										"autogradient" : 0,
										"color" : [ 0.294118, 0.313726, 0.337255, 1 ],
										"color1" : [ 0.454902, 0.462745, 0.482353, 0.0 ],
										"color2" : [ 0.290196, 0.309804, 0.301961, 1.0 ],
										"proportion" : 0.39,
										"type" : "color"
									}

								}
,
								"parentstyle" : "",
								"multi" : 0
							}
, 							{
								"name" : "Jamoma_highlighted_orange",
								"default" : 								{
									"accentcolor" : [ 1.0, 0.5, 0.0, 1.0 ]
								}
,
								"parentstyle" : "",
								"multi" : 0
							}
, 							{
								"name" : "black in white",
								"default" : 								{
									"bgcolor" : [ 0.953755, 0.965255, 1.0, 1.0 ],
									"textcolor_inverse" : [ 0.0, 0.0, 0.0, 1.0 ]
								}
,
								"parentstyle" : "number001",
								"multi" : 0
							}
, 							{
								"name" : "default_style",
								"newobj" : 								{
									"accentcolor" : [ 0.65098, 0.666667, 0.662745, 1.0 ],
									"bgcolor" : [ 1.0, 1.0, 1.0, 1.0 ],
									"textcolor_inverse" : [ 0.0, 0.0, 0.0, 1.0 ]
								}
,
								"button" : 								{
									"bgcolor" : [ 0.682032, 0.698052, 0.748716, 1.0 ],
									"color" : [ 0.960784, 0.827451, 0.156863, 1.0 ]
								}
,
								"toggle" : 								{
									"bgcolor" : [ 0.636487, 0.648652, 0.683149, 1.0 ],
									"color" : [ 0.0, 0.0, 0.0, 1.0 ],
									"elementcolor" : [ 0.786675, 0.801885, 0.845022, 1.0 ]
								}
,
								"parentstyle" : "",
								"multi" : 1
							}
, 							{
								"name" : "default_style-1",
								"newobj" : 								{
									"accentcolor" : [ 0.65098, 0.666667, 0.662745, 1.0 ],
									"bgcolor" : [ 1.0, 1.0, 1.0, 1.0 ],
									"textcolor_inverse" : [ 0.0, 0.0, 0.0, 1.0 ]
								}
,
								"button" : 								{
									"bgcolor" : [ 0.682032, 0.698052, 0.748716, 1.0 ],
									"color" : [ 0.960784, 0.827451, 0.156863, 1.0 ]
								}
,
								"toggle" : 								{
									"bgcolor" : [ 0.636487, 0.648652, 0.683149, 1.0 ],
									"color" : [ 0.0, 0.0, 0.0, 1.0 ],
									"elementcolor" : [ 0.786675, 0.801885, 0.845022, 1.0 ]
								}
,
								"parentstyle" : "",
								"multi" : 0
							}
, 							{
								"name" : "jpatcher001",
								"parentstyle" : "velvet",
								"multi" : 0
							}
, 							{
								"name" : "jx.style",
								"default" : 								{
									"accentcolor" : [ 1.0, 1.0, 1.0, 0.25 ],
									"bgcolor" : [ 0.095481, 0.100396, 0.100293, 0.36 ],
									"bgfillcolor" : 									{
										"angle" : 270.0,
										"color" : [ 0.0, 0.0, 0.0, 0.45 ],
										"color1" : [ 0.65098, 0.666667, 0.662745, 0.64 ],
										"color2" : [ 0.0, 0.0, 0.0, 0.65 ],
										"proportion" : 0.39,
										"type" : "color"
									}
,
									"color" : [ 0.8, 0.8, 0.8, 1.0 ],
									"fontname" : [ "Verdana" ],
									"fontsize" : [ 8.0 ],
									"patchlinecolor" : [ 0.239216, 0.254902, 0.278431, 0.45 ],
									"textcolor" : [ 0.0, 0.0, 0.0, 0.690196 ]
								}
,
								"parentstyle" : "jpatcher001",
								"multi" : 0
							}
, 							{
								"name" : "master_style",
								"newobj" : 								{
									"accentcolor" : [ 0.65098, 0.666667, 0.662745, 1.0 ],
									"bgcolor" : [ 1.0, 1.0, 1.0, 1.0 ],
									"textcolor_inverse" : [ 0.0, 0.0, 0.0, 1.0 ]
								}
,
								"attrui" : 								{
									"bgcolor" : [ 0.786675, 0.801885, 0.845022, 1.0 ],
									"textcolor_inverse" : [ 0.0, 0.0, 0.0, 1.0 ]
								}
,
								"button" : 								{
									"bgcolor" : [ 0.682032, 0.698052, 0.748716, 1.0 ],
									"color" : [ 1.0, 0.95051, 0.0, 1.0 ],
									"elementcolor" : [ 0.786675, 0.801885, 0.845022, 1.0 ]
								}
,
								"ezadc~" : 								{
									"bgcolor" : [ 0.65098, 0.666667, 0.662745, 1.0 ],
									"color" : [ 0.0, 0.0, 0.0, 1.0 ],
									"elementcolor" : [ 0.862745, 0.870588, 0.878431, 1.0 ]
								}
,
								"ezdac~" : 								{
									"bgcolor" : [ 0.65098, 0.666667, 0.662745, 1.0 ],
									"color" : [ 0.0, 0.0, 0.0, 1.0 ],
									"elementcolor" : [ 0.862745, 0.870588, 0.878431, 1.0 ]
								}
,
								"function" : 								{
									"bgcolor" : [ 0.65098, 0.666667, 0.662745, 1.0 ],
									"color" : [ 0.0, 0.0, 0.0, 1.0 ]
								}
,
								"multislider" : 								{
									"bgcolor" : [ 1.0, 1.0, 1.0, 1.0 ],
									"color" : [ 0.0, 0.0, 0.0, 1.0 ]
								}
,
								"slider" : 								{
									"bgcolor" : [ 1.0, 1.0, 1.0, 1.0 ],
									"color" : [ 0.461105, 0.492646, 0.591878, 1.0 ],
									"elementcolor" : [ 0.65098, 0.666667, 0.662745, 1.0 ]
								}
,
								"toggle" : 								{
									"bgcolor" : [ 0.682032, 0.698052, 0.748716, 1.0 ],
									"color" : [ 0.0, 0.0, 0.0, 1.0 ],
									"elementcolor" : [ 0.65098, 0.666667, 0.662745, 1.0 ]
								}
,
								"message" : 								{
									"bgfillcolor" : 									{
										"angle" : 270.0,
										"autogradient" : 0,
										"color" : [ 0.290196, 0.309804, 0.301961, 1.0 ],
										"color1" : [ 0.786675, 0.801885, 0.845022, 1.0 ],
										"color2" : [ 0.65098, 0.666667, 0.662745, 1.0 ],
										"proportion" : 0.39,
										"type" : "gradient"
									}
,
									"textcolor_inverse" : [ 0.0, 0.0, 0.0, 1.0 ]
								}
,
								"umenu" : 								{
									"bgfillcolor" : 									{
										"angle" : 270.0,
										"autogradient" : 0,
										"color" : [ 0.290196, 0.309804, 0.301961, 1.0 ],
										"color1" : [ 0.786675, 0.801885, 0.845022, 1.0 ],
										"color2" : [ 0.65098, 0.666667, 0.662745, 1.0 ],
										"proportion" : 0.39,
										"type" : "gradient"
									}

								}
,
								"gain~" : 								{
									"color" : [ 1.0, 0.861448, 0.16921, 1.0 ],
									"elementcolor" : [ 0.65098, 0.666667, 0.662745, 1.0 ]
								}
,
								"kslider" : 								{
									"color" : [ 1.0, 1.0, 1.0, 1.0 ],
									"elementcolor" : [ 0.65098, 0.666667, 0.662745, 1.0 ]
								}
,
								"parentstyle" : "",
								"multi" : 1
							}
, 							{
								"name" : "newobjBlue-1",
								"default" : 								{
									"accentcolor" : [ 0.317647, 0.654902, 0.976471, 1.0 ]
								}
,
								"parentstyle" : "",
								"multi" : 0
							}
, 							{
								"name" : "newobjGreen-1",
								"default" : 								{
									"accentcolor" : [ 0.0, 0.533333, 0.168627, 1.0 ]
								}
,
								"parentstyle" : "",
								"multi" : 0
							}
, 							{
								"name" : "newobjYellow-1",
								"default" : 								{
									"accentcolor" : [ 0.82517, 0.78181, 0.059545, 1.0 ],
									"fontsize" : [ 12.059008 ]
								}
,
								"parentstyle" : "",
								"multi" : 0
							}
, 							{
								"name" : "number001",
								"parentstyle" : "velvet",
								"multi" : 0
							}
, 							{
								"name" : "numberGold-1",
								"default" : 								{
									"accentcolor" : [ 0.764706, 0.592157, 0.101961, 1.0 ]
								}
,
								"parentstyle" : "",
								"multi" : 0
							}
, 							{
								"name" : "tap",
								"default" : 								{
									"fontname" : [ "Lato Light" ]
								}
,
								"parentstyle" : "",
								"multi" : 0
							}
 ]
					}
,
					"patching_rect" : [ 323.0, 527.411592012211713, 63.0, 23.0 ],
					"saved_object_attributes" : 					{
						"description" : "",
						"digest" : "",
						"globalpatchername" : "",
						"tags" : ""
					}
,
					"text" : "p indexing"
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.2, 0.2, 0.2, 0.0 ],
					"candycane" : 4,
					"contdata" : 1,
					"id" : "obj-2",
					"maxclass" : "multislider",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 323.0, 356.605667587303401, 158.0, 81.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 5.970209402728315, 148.527472637511892, 158.0, 81.0 ],
					"setstyle" : 1,
					"signed" : 1,
					"size" : 4,
					"spacing" : 2
				}

			}
, 			{
				"box" : 				{
					"activebgcolor" : [ 0.984313725490196, 0.007843137254902, 0.027450980392157, 0.0 ],
					"appearance" : 1,
					"bgcolor" : [ 0.18784, 0.661873, 0.140981, 1.0 ],
					"bordercolor" : [ 0.0, 0.0, 0.0, 1.0 ],
					"id" : "obj-20",
					"labeltextcolor" : [ 0.156862745098039, 0.156862745098039, 0.156862745098039, 1.0 ],
					"maxclass" : "live.text",
					"mode" : 0,
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"parameter_enable" : 1,
					"patching_rect" : [ 327.999635993986885, 169.5, 83.911552785932145, 18.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 10.642565579372331, 31.06737782921196, 79.911552785932145, 20.000000000000004 ],
					"saved_attribute_attributes" : 					{
						"activebgcolor" : 						{
							"expression" : ""
						}
,
						"bgcolor" : 						{
							"expression" : ""
						}
,
						"bordercolor" : 						{
							"expression" : ""
						}
,
						"labeltextcolor" : 						{
							"expression" : "themecolor.live_dial_fg"
						}
,
						"valueof" : 						{
							"parameter_enum" : [ "val1", "val2" ],
							"parameter_longname" : "live.text[4]",
							"parameter_mmax" : 1,
							"parameter_shortname" : "live.text",
							"parameter_type" : 2
						}

					}
,
					"text" : "Refresh Port",
					"texton" : "CONNECTED",
					"varname" : "live.text[4]"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-12",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 623.455412386952958, 520.0, 23.0, 23.0 ],
					"text" : "t b"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-8",
					"linecount" : 2,
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1061.0, 795.0, 236.0, 37.0 ],
					"text" : ";\r#SM deleteoutport \"from EAVI 2\" coremidi"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-6",
					"linecount" : 2,
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1061.0, 738.0, 236.0, 37.0 ],
					"text" : ";\r#SM deleteoutport \"from EAVI 1\" coremidi"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-72",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 714.455412386952958, 490.605667587303401, 62.0, 23.0 ],
					"text" : "qlim 1000"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-67",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "", "int", "int" ],
					"patching_rect" : [ 525.705412386952958, 713.523442326622899, 47.0, 23.0 ],
					"text" : "change"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-66",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "", "int", "int" ],
					"patching_rect" : [ 386.205412386953014, 713.523442326622899, 47.0, 23.0 ],
					"text" : "change"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-65",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "", "int", "int" ],
					"patching_rect" : [ 246.205412386953014, 713.523442326622899, 47.0, 23.0 ],
					"text" : "change"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-64",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "", "int", "int" ],
					"patching_rect" : [ 105.455412386953014, 713.523442326622899, 47.0, 23.0 ],
					"text" : "change"
				}

			}
, 			{
				"box" : 				{
					"activebgcolor" : [ 0.984313725490196, 0.007843137254902, 0.027450980392157, 0.0 ],
					"activebgoncolor" : [ 0.309803921568627, 0.63921568627451, 0.988235294117647, 1.0 ],
					"appearance" : 1,
					"bgcolor" : [ 0.18784, 0.661873, 0.140981, 1.0 ],
					"bordercolor" : [ 0.0, 0.0, 0.0, 1.0 ],
					"id" : "obj-52",
					"labeltextcolor" : [ 0.156862745098039, 0.156862745098039, 0.156862745098039, 1.0 ],
					"maxclass" : "live.text",
					"mode" : 0,
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"parameter_enable" : 1,
					"patching_rect" : [ 626.642565579372331, 170.0, 124.911552785932145, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 10.642565579372331, 7.6259859691163, 132.911552785932145, 20.0 ],
					"saved_attribute_attributes" : 					{
						"activebgcolor" : 						{
							"expression" : ""
						}
,
						"activebgoncolor" : 						{
							"expression" : "themecolor.live_freeze_color"
						}
,
						"bgcolor" : 						{
							"expression" : ""
						}
,
						"bordercolor" : 						{
							"expression" : ""
						}
,
						"labeltextcolor" : 						{
							"expression" : "themecolor.live_meter_bg"
						}
,
						"valueof" : 						{
							"parameter_enum" : [ "val1", "val2" ],
							"parameter_longname" : "live.text[1]",
							"parameter_mmax" : 1,
							"parameter_shortname" : "live.text",
							"parameter_type" : 2
						}

					}
,
					"text" : "Bluetooth Configuration",
					"texton" : "CONNECTED",
					"varname" : "live.text[1]"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-54",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patcher" : 					{
						"fileversion" : 1,
						"appversion" : 						{
							"major" : 8,
							"minor" : 5,
							"revision" : 3,
							"architecture" : "x64",
							"modernui" : 1
						}
,
						"classnamespace" : "box",
						"rect" : [ 660.0, 108.0, 626.0, 440.0 ],
						"bglocked" : 0,
						"openinpresentation" : 0,
						"default_fontsize" : 10.0,
						"default_fontface" : 0,
						"default_fontname" : "Ableton Sans Medium",
						"gridonopen" : 1,
						"gridsize" : [ 15.0, 15.0 ],
						"gridsnaponopen" : 1,
						"objectsnaponopen" : 1,
						"statusbarvisible" : 2,
						"toolbarvisible" : 1,
						"lefttoolbarpinned" : 2,
						"toptoolbarpinned" : 2,
						"righttoolbarpinned" : 0,
						"bottomtoolbarpinned" : 0,
						"toolbars_unpinned_last_save" : 3,
						"tallnewobj" : 0,
						"boxanimatetime" : 200,
						"enablehscroll" : 1,
						"enablevscroll" : 1,
						"devicewidth" : 0.0,
						"description" : "",
						"digest" : "",
						"tags" : "",
						"style" : "",
						"subpatcher_template" : "ableton",
						"assistshowspatchername" : 0,
						"boxes" : [ 							{
								"box" : 								{
									"id" : "obj-29",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 3,
									"outlettype" : [ "", "int", "int" ],
									"patching_rect" : [ 16.65080816316231, 143.5, 41.0, 20.0 ],
									"text" : "change"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-16",
									"maxclass" : "toggle",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 16.65080816316231, 175.5, 24.0, 24.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-3",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"patching_rect" : [ 57.0, 288.5, 50.0, 20.0 ],
									"text" : "loadbang"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-211",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 16.65080816316231, 338.5, 186.0, 20.0 ],
									"text" : "text CONNECT, activebgcolor 1. 0. 0. 1."
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-210",
									"maxclass" : "newobj",
									"numinlets" : 3,
									"numoutlets" : 3,
									"outlettype" : [ "bang", "bang", "" ],
									"patching_rect" : [ 16.65080816316231, 216.5, 40.0, 20.0 ],
									"text" : "sel 1 0"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-209",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 16.65080816316231, 111.5, 73.0, 20.0 ],
									"text" : "expr $i1 > 200"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-199",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "float" ],
									"patching_rect" : [ 17.10840568189758, 69.448056669465132, 61.0, 20.0 ],
									"text" : "clocker 200"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-49",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 347.21963004325778, 338.5, 252.0, 20.0 ],
									"text" : "text CONNECTED, activebgcolor 0.129 0.694 0.098 1."
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-43",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "bang", "bang" ],
									"patching_rect" : [ 347.21963004325778, 303.5, 55.0, 20.0 ],
									"text" : "onebang 1"
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-39",
									"index" : 1,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"patching_rect" : [ 17.10840568189758, 16.0, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-52",
									"index" : 1,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 16.65080816316231, 400.0, 30.0, 30.0 ]
								}

							}
 ],
						"lines" : [ 							{
								"patchline" : 								{
									"destination" : [ "obj-210", 0 ],
									"midpoints" : [ 26.15080816316231, 201.0, 26.15080816316231, 201.0 ],
									"source" : [ "obj-16", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-209", 0 ],
									"midpoints" : [ 26.60840568189758, 90.0, 26.15080816316231, 90.0 ],
									"source" : [ "obj-199", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-29", 0 ],
									"midpoints" : [ 26.15080816316231, 132.0, 26.15080816316231, 132.0 ],
									"source" : [ "obj-209", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-211", 0 ],
									"midpoints" : [ 26.15080816316231, 237.0, 26.15080816316231, 237.0 ],
									"order" : 1,
									"source" : [ "obj-210", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-43", 0 ],
									"midpoints" : [ 36.65080816316231, 273.0, 356.71963004325778, 273.0 ],
									"source" : [ "obj-210", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-43", 1 ],
									"midpoints" : [ 26.15080816316231, 273.0, 392.71963004325778, 273.0 ],
									"order" : 0,
									"source" : [ "obj-210", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-52", 0 ],
									"midpoints" : [ 26.15080816316231, 360.0, 26.15080816316231, 360.0 ],
									"source" : [ "obj-211", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-16", 0 ],
									"midpoints" : [ 26.15080816316231, 165.0, 26.15080816316231, 165.0 ],
									"source" : [ "obj-29", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-211", 0 ],
									"midpoints" : [ 66.5, 324.0, 26.15080816316231, 324.0 ],
									"source" : [ "obj-3", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-199", 0 ],
									"midpoints" : [ 26.60840568189758, 48.0, 26.60840568189758, 48.0 ],
									"order" : 1,
									"source" : [ "obj-39", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-43", 0 ],
									"midpoints" : [ 26.60840568189758, 48.0, 356.71963004325778, 48.0 ],
									"order" : 0,
									"source" : [ "obj-39", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-49", 0 ],
									"midpoints" : [ 356.71963004325778, 324.0, 356.71963004325778, 324.0 ],
									"source" : [ "obj-43", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-52", 0 ],
									"midpoints" : [ 356.71963004325778, 387.0, 26.15080816316231, 387.0 ],
									"source" : [ "obj-49", 0 ]
								}

							}
 ],
						"styles" : [ 							{
								"name" : "AudioStatus_Menu",
								"default" : 								{
									"bgfillcolor" : 									{
										"angle" : 270.0,
										"autogradient" : 0,
										"color" : [ 0.294118, 0.313726, 0.337255, 1 ],
										"color1" : [ 0.454902, 0.462745, 0.482353, 0.0 ],
										"color2" : [ 0.290196, 0.309804, 0.301961, 1.0 ],
										"proportion" : 0.39,
										"type" : "color"
									}

								}
,
								"parentstyle" : "",
								"multi" : 0
							}
, 							{
								"name" : "Jamoma_highlighted_orange",
								"default" : 								{
									"accentcolor" : [ 1.0, 0.5, 0.0, 1.0 ]
								}
,
								"parentstyle" : "",
								"multi" : 0
							}
, 							{
								"name" : "black in white",
								"default" : 								{
									"bgcolor" : [ 0.953755, 0.965255, 1.0, 1.0 ],
									"textcolor_inverse" : [ 0.0, 0.0, 0.0, 1.0 ]
								}
,
								"parentstyle" : "number001",
								"multi" : 0
							}
, 							{
								"name" : "default_style",
								"newobj" : 								{
									"accentcolor" : [ 0.65098, 0.666667, 0.662745, 1.0 ],
									"bgcolor" : [ 1.0, 1.0, 1.0, 1.0 ],
									"textcolor_inverse" : [ 0.0, 0.0, 0.0, 1.0 ]
								}
,
								"button" : 								{
									"bgcolor" : [ 0.682032, 0.698052, 0.748716, 1.0 ],
									"color" : [ 0.960784, 0.827451, 0.156863, 1.0 ]
								}
,
								"toggle" : 								{
									"bgcolor" : [ 0.636487, 0.648652, 0.683149, 1.0 ],
									"color" : [ 0.0, 0.0, 0.0, 1.0 ],
									"elementcolor" : [ 0.786675, 0.801885, 0.845022, 1.0 ]
								}
,
								"parentstyle" : "",
								"multi" : 1
							}
, 							{
								"name" : "default_style-1",
								"newobj" : 								{
									"accentcolor" : [ 0.65098, 0.666667, 0.662745, 1.0 ],
									"bgcolor" : [ 1.0, 1.0, 1.0, 1.0 ],
									"textcolor_inverse" : [ 0.0, 0.0, 0.0, 1.0 ]
								}
,
								"button" : 								{
									"bgcolor" : [ 0.682032, 0.698052, 0.748716, 1.0 ],
									"color" : [ 0.960784, 0.827451, 0.156863, 1.0 ]
								}
,
								"toggle" : 								{
									"bgcolor" : [ 0.636487, 0.648652, 0.683149, 1.0 ],
									"color" : [ 0.0, 0.0, 0.0, 1.0 ],
									"elementcolor" : [ 0.786675, 0.801885, 0.845022, 1.0 ]
								}
,
								"parentstyle" : "",
								"multi" : 0
							}
, 							{
								"name" : "jpatcher001",
								"parentstyle" : "velvet",
								"multi" : 0
							}
, 							{
								"name" : "jx.style",
								"default" : 								{
									"accentcolor" : [ 1.0, 1.0, 1.0, 0.25 ],
									"bgcolor" : [ 0.095481, 0.100396, 0.100293, 0.36 ],
									"bgfillcolor" : 									{
										"angle" : 270.0,
										"color" : [ 0.0, 0.0, 0.0, 0.45 ],
										"color1" : [ 0.65098, 0.666667, 0.662745, 0.64 ],
										"color2" : [ 0.0, 0.0, 0.0, 0.65 ],
										"proportion" : 0.39,
										"type" : "color"
									}
,
									"color" : [ 0.8, 0.8, 0.8, 1.0 ],
									"fontname" : [ "Verdana" ],
									"fontsize" : [ 8.0 ],
									"patchlinecolor" : [ 0.239216, 0.254902, 0.278431, 0.45 ],
									"textcolor" : [ 0.0, 0.0, 0.0, 0.690196 ]
								}
,
								"parentstyle" : "jpatcher001",
								"multi" : 0
							}
, 							{
								"name" : "master_style",
								"newobj" : 								{
									"accentcolor" : [ 0.65098, 0.666667, 0.662745, 1.0 ],
									"bgcolor" : [ 1.0, 1.0, 1.0, 1.0 ],
									"textcolor_inverse" : [ 0.0, 0.0, 0.0, 1.0 ]
								}
,
								"attrui" : 								{
									"bgcolor" : [ 0.786675, 0.801885, 0.845022, 1.0 ],
									"textcolor_inverse" : [ 0.0, 0.0, 0.0, 1.0 ]
								}
,
								"button" : 								{
									"bgcolor" : [ 0.682032, 0.698052, 0.748716, 1.0 ],
									"color" : [ 1.0, 0.95051, 0.0, 1.0 ],
									"elementcolor" : [ 0.786675, 0.801885, 0.845022, 1.0 ]
								}
,
								"ezadc~" : 								{
									"bgcolor" : [ 0.65098, 0.666667, 0.662745, 1.0 ],
									"color" : [ 0.0, 0.0, 0.0, 1.0 ],
									"elementcolor" : [ 0.862745, 0.870588, 0.878431, 1.0 ]
								}
,
								"ezdac~" : 								{
									"bgcolor" : [ 0.65098, 0.666667, 0.662745, 1.0 ],
									"color" : [ 0.0, 0.0, 0.0, 1.0 ],
									"elementcolor" : [ 0.862745, 0.870588, 0.878431, 1.0 ]
								}
,
								"function" : 								{
									"bgcolor" : [ 0.65098, 0.666667, 0.662745, 1.0 ],
									"color" : [ 0.0, 0.0, 0.0, 1.0 ]
								}
,
								"multislider" : 								{
									"bgcolor" : [ 1.0, 1.0, 1.0, 1.0 ],
									"color" : [ 0.0, 0.0, 0.0, 1.0 ]
								}
,
								"slider" : 								{
									"bgcolor" : [ 1.0, 1.0, 1.0, 1.0 ],
									"color" : [ 0.461105, 0.492646, 0.591878, 1.0 ],
									"elementcolor" : [ 0.65098, 0.666667, 0.662745, 1.0 ]
								}
,
								"toggle" : 								{
									"bgcolor" : [ 0.682032, 0.698052, 0.748716, 1.0 ],
									"color" : [ 0.0, 0.0, 0.0, 1.0 ],
									"elementcolor" : [ 0.65098, 0.666667, 0.662745, 1.0 ]
								}
,
								"message" : 								{
									"bgfillcolor" : 									{
										"angle" : 270.0,
										"autogradient" : 0,
										"color" : [ 0.290196, 0.309804, 0.301961, 1.0 ],
										"color1" : [ 0.786675, 0.801885, 0.845022, 1.0 ],
										"color2" : [ 0.65098, 0.666667, 0.662745, 1.0 ],
										"proportion" : 0.39,
										"type" : "gradient"
									}
,
									"textcolor_inverse" : [ 0.0, 0.0, 0.0, 1.0 ]
								}
,
								"umenu" : 								{
									"bgfillcolor" : 									{
										"angle" : 270.0,
										"autogradient" : 0,
										"color" : [ 0.290196, 0.309804, 0.301961, 1.0 ],
										"color1" : [ 0.786675, 0.801885, 0.845022, 1.0 ],
										"color2" : [ 0.65098, 0.666667, 0.662745, 1.0 ],
										"proportion" : 0.39,
										"type" : "gradient"
									}

								}
,
								"gain~" : 								{
									"color" : [ 1.0, 0.861448, 0.16921, 1.0 ],
									"elementcolor" : [ 0.65098, 0.666667, 0.662745, 1.0 ]
								}
,
								"kslider" : 								{
									"color" : [ 1.0, 1.0, 1.0, 1.0 ],
									"elementcolor" : [ 0.65098, 0.666667, 0.662745, 1.0 ]
								}
,
								"parentstyle" : "",
								"multi" : 1
							}
, 							{
								"name" : "newobjBlue-1",
								"default" : 								{
									"accentcolor" : [ 0.317647, 0.654902, 0.976471, 1.0 ]
								}
,
								"parentstyle" : "",
								"multi" : 0
							}
, 							{
								"name" : "newobjGreen-1",
								"default" : 								{
									"accentcolor" : [ 0.0, 0.533333, 0.168627, 1.0 ]
								}
,
								"parentstyle" : "",
								"multi" : 0
							}
, 							{
								"name" : "newobjYellow-1",
								"default" : 								{
									"accentcolor" : [ 0.82517, 0.78181, 0.059545, 1.0 ],
									"fontsize" : [ 12.059008 ]
								}
,
								"parentstyle" : "",
								"multi" : 0
							}
, 							{
								"name" : "number001",
								"parentstyle" : "velvet",
								"multi" : 0
							}
, 							{
								"name" : "numberGold-1",
								"default" : 								{
									"accentcolor" : [ 0.764706, 0.592157, 0.101961, 1.0 ]
								}
,
								"parentstyle" : "",
								"multi" : 0
							}
, 							{
								"name" : "tap",
								"default" : 								{
									"fontname" : [ "Lato Light" ]
								}
,
								"parentstyle" : "",
								"multi" : 0
							}
 ],
						"textcolor" : [ 0.0, 0.0, 0.0, 1.0 ],
						"bgcolor" : [ 0.356862745098039, 0.356862745098039, 0.356862745098039, 1.0 ],
						"editing_bgcolor" : [ 0.356862745098039, 0.356862745098039, 0.356862745098039, 1.0 ]
					}
,
					"patching_rect" : [ 623.455412386952958, 553.849528952100513, 42.0, 23.0 ],
					"saved_object_attributes" : 					{
						"description" : "",
						"digest" : "",
						"editing_bgcolor" : [ 0.356862745098039, 0.356862745098039, 0.356862745098039, 1.0 ],
						"fontname" : "Ableton Sans Medium",
						"fontsize" : 10.0,
						"globalpatchername" : "",
						"locked_bgcolor" : [ 0.356862745098039, 0.356862745098039, 0.356862745098039, 1.0 ],
						"tags" : "",
						"textcolor" : [ 0.0, 0.0, 0.0, 1.0 ]
					}
,
					"text" : "p data"
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.2, 0.2, 0.2, 0.0 ],
					"id" : "obj-46",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"outlinecolor" : [ 0.349019607843137, 0.349019607843137, 0.349019607843137, 0.0 ],
					"parameter_enable" : 0,
					"patching_rect" : [ 714.455412386952958, 529.849528952100513, 24.0, 24.0 ]
				}

			}
, 			{
				"box" : 				{
					"activebgcolor" : [ 1.0, 0.0, 0.0, 1.0 ],
					"activebgoncolor" : [ 0.9, 1.0, 1.0, 0.100000001490116 ],
					"appearance" : 1,
					"bgcolor" : [ 0.18784, 0.661873, 0.140981, 1.0 ],
					"bordercolor" : [ 0.0, 0.0, 0.0, 1.0 ],
					"id" : "obj-7",
					"ignoreclick" : 1,
					"labeltextcolor" : [ 0.156862745098039, 0.156862745098039, 0.156862745098039, 1.0 ],
					"maxclass" : "live.text",
					"mode" : 0,
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"parameter_enable" : 1,
					"patching_rect" : [ 623.455412386952958, 592.523442326622899, 87.911552785932145, 15.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 58.642565579372331, 85.368532135443914, 82.91155278593213, 15.140284219885871 ],
					"saved_attribute_attributes" : 					{
						"activebgcolor" : 						{
							"expression" : ""
						}
,
						"activebgoncolor" : 						{
							"expression" : ""
						}
,
						"activetextoncolor" : 						{
							"expression" : ""
						}
,
						"bgcolor" : 						{
							"expression" : ""
						}
,
						"bordercolor" : 						{
							"expression" : ""
						}
,
						"labeltextcolor" : 						{
							"expression" : "themecolor.live_meter_bg"
						}
,
						"valueof" : 						{
							"parameter_enum" : [ "val1", "val2" ],
							"parameter_longname" : "live.text",
							"parameter_mmax" : 1,
							"parameter_shortname" : "live.text",
							"parameter_type" : 2
						}

					}
,
					"text" : "CONNECT",
					"texton" : "CONNECTED",
					"varname" : "live.text[17]"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-21",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 104.911552785932201, 40.306018298250024, 68.0, 23.0 ],
					"text" : "loadmess 1"
				}

			}
, 			{
				"box" : 				{
					"activebgcolor" : [ 1.0, 0.345098039215686, 0.298039215686275, 1.0 ],
					"activebgoncolor" : [ 0.49803921568, 0.67450980392, 0.38823529411, 1.0 ],
					"bgcolor" : [ 0.666666666666667, 0.666666666666667, 0.666666666666667, 1.0 ],
					"bordercolor" : [ 0.0, 0.0, 0.0, 1.0 ],
					"id" : "obj-580",
					"maxclass" : "live.text",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"parameter_enable" : 1,
					"patching_rect" : [ 104.911552785932201, 68.51582592955333, 179.911552785932145, 32.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 5.970209402728315, 107.756331744105637, 156.506489395200333, 32.0 ],
					"rounded" : 20.0,
					"saved_attribute_attributes" : 					{
						"activebgcolor" : 						{
							"expression" : "themecolor.live_active_automation"
						}
,
						"activebgoncolor" : 						{
							"expression" : "themecolor.maxwindow_successtext"
						}
,
						"bgcolor" : 						{
							"expression" : ""
						}
,
						"bordercolor" : 						{
							"expression" : ""
						}
,
						"valueof" : 						{
							"parameter_enum" : [ "val1", "val2" ],
							"parameter_longname" : "live.text[2]",
							"parameter_mmax" : 1,
							"parameter_shortname" : "live.text",
							"parameter_type" : 2
						}

					}
,
					"text" : "OFF",
					"texton" : "ON",
					"varname" : "live.text[3]"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Ableton Sans Medium",
					"id" : "obj-47",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 744.455412386952958, 532.411592012211827, 46.0, 21.0 ],
					"text" : "DATA ?"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Ableton Sans Medium",
					"fontsize" : 10.0,
					"id" : "obj-23",
					"linecount" : 2,
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 626.642565579372331, 208.763383132501872, 80.0, 32.0 ],
					"text" : ";\rdsp driver setup"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-4",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "bang", "bang" ],
					"patching_rect" : [ 776.455412386952958, 690.5, 33.0, 23.0 ],
					"text" : "t b b"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-33",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 525.705412386952958, 746.0, 32.0, 23.0 ],
					"text" : "$1 4"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-35",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 386.205412386953014, 746.0, 32.0, 23.0 ],
					"text" : "$1 3"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-32",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 246.205412386953014, 746.0, 32.0, 23.0 ],
					"text" : "$1 2"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-31",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 776.455412386952958, 643.0, 58.0, 23.0 ],
					"text" : "loadbang"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-30",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 672.455412386952958, 738.0, 104.0, 23.0 ],
					"text" : "port EAVI-BOARD"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-28",
					"maxclass" : "newobj",
					"numinlets" : 3,
					"numoutlets" : 0,
					"patching_rect" : [ 105.455412386953014, 799.0, 60.0, 23.0 ],
					"text" : "ctlout 1 9"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-3",
					"linecount" : 2,
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 790.916551827489457, 738.0, 232.0, 37.0 ],
					"text" : ";\r#SM createoutport EAVI-BOARD coremidi"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-27",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 105.455412386953014, 746.0, 32.0, 23.0 ],
					"text" : "$1 1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-1",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1265.0, 267.0, 67.0, 23.0 ],
					"saved_object_attributes" : 					{
						"allwindowsactive" : 0,
						"appicon_mac" : "Macintosh HD:/Users/Home/Documents/Max 8/Packages+/Sound Control/resources/icon/sc_icon.icns",
						"appicon_win" : "",
						"audiosupport" : 1,
						"bundleidentifier" : "nl.francescodimaggio.eavi",
						"cantclosetoplevelpatchers" : 0,
						"cefsupport" : 0,
						"copysupport" : 0,
						"database" : 0,
						"extensions" : 1,
						"gensupport" : 0,
						"midisupport" : 1,
						"noloadbangdefeating" : 0,
						"overdrive" : 0,
						"preffilename" : "Max 8 Preferences",
						"searchformissingfiles" : 0,
						"statusvisible" : 0,
						"usesearchpath" : 0
					}
,
					"text" : "standalone"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-13",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 161.955412386953014, 528.411592012211827, 76.0, 21.0 ],
					"text" : "14-bit range"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-11",
					"maxclass" : "newobj",
					"numinlets" : 6,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 525.705412386952958, 681.046884653245684, 118.0, 23.0 ],
					"text" : "scale 0 16384 0 127"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-14",
					"maxclass" : "newobj",
					"numinlets" : 6,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 386.205412386953014, 681.046884653245684, 118.0, 23.0 ],
					"text" : "scale 0 16384 0 127"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-15",
					"maxclass" : "newobj",
					"numinlets" : 6,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 246.205412386953014, 681.046884653245684, 118.0, 23.0 ],
					"text" : "scale 0 16384 0 127"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-60",
					"maxclass" : "newobj",
					"numinlets" : 6,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 105.455412386953014, 681.046884653245684, 118.0, 23.0 ],
					"text" : "scale 0 16384 0 127"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-16",
					"maxclass" : "newobj",
					"numinlets" : 5,
					"numoutlets" : 5,
					"outlettype" : [ "", "", "", "", "" ],
					"patching_rect" : [ 105.455412386953014, 642.829367483828378, 580.0, 23.0 ],
					"text" : "route 2 3 4 5"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-18",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "int", "int" ],
					"patching_rect" : [ 105.455412386953014, 558.217517169417306, 36.0, 23.0 ],
					"text" : "swap"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-19",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 105.455412386953014, 589.023442326622785, 34.0, 23.0 ],
					"text" : "pack"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-22",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "int", "int" ],
					"patching_rect" : [ 105.455412386953014, 527.411592012211713, 51.0, 23.0 ],
					"text" : "xbendin"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-26",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 105.455412386953014, 490.605667587303401, 116.0, 23.0 ],
					"text" : "gate"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-29",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 244.455412386953014, 167.0, 58.0, 23.0 ],
					"text" : "loadbang"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-316",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "bang", "controllers" ],
					"patching_rect" : [ 244.455412386953014, 246.45314718380348, 84.0, 23.0 ],
					"text" : "t b controllers"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-126",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 243.773771220957599, 302.588123556160213, 196.0, 23.0 ],
					"text" : "setsymbol OWL-BIOSIGNALS, bang"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-36",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 623.455412386952958, 490.605667587303401, 55.0, 23.0 ],
					"text" : "qlim 100"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Ableton Sans Medium",
					"fontsize" : 12.0,
					"id" : "obj-40",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 161.955412386953014, 303.588123556160213, 51.0, 23.0 ],
					"text" : "midiinfo"
				}

			}
, 			{
				"box" : 				{
					"align" : 1,
					"autopopulate" : 1,
					"bgcolor" : [ 0.701957941055298, 0.701978802680969, 0.701966941356659, 0.0 ],
					"bgfillcolor_angle" : 270.0,
					"bgfillcolor_autogradient" : 0.0,
					"bgfillcolor_color" : [ 0.701957941055298, 0.701978802680969, 0.701966941356659, 0.0 ],
					"bgfillcolor_color1" : [ 0.301961, 0.301961, 0.301961, 1.0 ],
					"bgfillcolor_color2" : [ 0.2, 0.2, 0.2, 1.0 ],
					"bgfillcolor_proportion" : 0.5,
					"bgfillcolor_type" : "color",
					"fontname" : "Ableton Sans Medium",
					"fontsize" : 9.5,
					"id" : "obj-41",
					"items" : [ "IAC Driver Bus 1", ",", "to Max 1", ",", "to Max 2" ],
					"maxclass" : "umenu",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "int", "", "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 161.955412386953014, 362.217683503533408, 100.0, 20.0 ],
					"pattrmode" : 1,
					"presentation" : 1,
					"presentation_rect" : [ 7.857001824069258, 56.856670001650855, 152.509037136136612, 20.0 ],
					"textcolor" : [ 0.0, 0.0, 0.0, 1.0 ],
					"textjustification" : 1
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Ableton Sans Medium",
					"id" : "obj-209",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"patching_rect" : [ 201.911552785932201, 435.711858977122006, 40.0, 23.0 ],
					"text" : "midiin"
				}

			}
, 			{
				"box" : 				{
					"angle" : 270.0,
					"bgcolor" : [ 0.2, 0.2, 0.2, 0.0 ],
					"border" : 1,
					"bordercolor" : [ 0.0, 0.0, 0.0, 1.0 ],
					"id" : "obj-91",
					"maxclass" : "panel",
					"mode" : 0,
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 161.955412386953014, 361.217683503533408, 101.0, 22.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 8.844742331470844, 54.50876968930762, 151.501096558382187, 23.418370586040631 ],
					"proportion" : 0.5,
					"rounded" : 10
				}

			}
, 			{
				"box" : 				{
					"autofit" : 1,
					"data" : [ 152036, "png", "IBkSG0fBZn....PCIgDQRA..C.L..H.GHX....PNcc7d....DLmPIQEBHf.B7g.YHB..f.PRDEDU3wI68lEijckdme+9NmycI1xLx0ZeekEWZ1CaotU2RpwXMZj8XXCC3mL7a1.Fv.CrevO427q9Q+fWvLFCF6w.CfArszXXIYqkQpklVc2jr4RUjEqpXwZk0FqkrxsHh68dNe9g68FYVEIU2MytYwR77iHXDYjYE2kHy.m+2+ee++je3e3ekRjHeAQZefZZdP68ATAx6lviVcE7Rfk1yBr68uK5NSOv3Ai.UlO8KZjHQhDIRjHQ9bYryfQAWHfQAiZH.3s0e+xxQLS+YYxnBzz97925A7e6+i+y35ObC9O5e7+E7e0KsLimrFar58Xwklm08dVY0MXg4VjhwqStuDqBdRvKYTXSAB3zwXohKa6xE+fKw+j+G9mR2jT9u9+x+w7JG+3Lo3QTTNl7t8PDCpuDcRERPvZyoz0khJCc4t.FBFPUAsYAkFZkk3quSM.BAwhhCECJF5HqRU6OpKCONVaTE2ak039qNh+W+W9+A29SdDexJqRmYmmS8MdM909d+VblWd2LKv2hw7vG9PFOojAyLCc50Ee.pHfy4XzjQ38dPqHwYHM0gnAlLYSFOdLcym+IeCQ+rWOqZB02KUM2WuSa8I6veCHxNA6+o+G+ex+MOq2Ih77Kxm5Qs2qf.AIPUUE1DGyLb.yLbVRxRPIPPUjOmOvHRjHQhDIRjHe1TYDD.ipH.BBJs5ECng.IIVlLp.wkBo4b4acGt08WASZFe6CLOYYIngRDKHFGh.IIYLY7ljZLHnnhEUbDvBhhgJDUIj0kgyLfY5MfW6keYdkScJTcLiGsNJAFW3opxiEgTWJIFAUgfJ3rFfwnBTu2a.o9nPockjBSUE29yfAUp+pDpH383CfHFbtbbtLRy5P2d84U+leSN7QNFoc6xidzJb8O5xbsKeYt6cdLOb8.KkYnyvEYl4V.uKg6uxJ730WAAHKMgDmRp0fyBAeEESJv6Cjk0gAcGRg22rO0d1+yCAUz5CEo4nSDLg35eeVRT.bjcDeZAvJfN8yrJ8EnFn6fNLbgYYvrCvk3HnUT48XE2W56yQhDIRjHQh77Ldo1AXSiDRUTTS8M.TMf0ZY73wXbIzMuKqUAW55Wmqc4KyKt+kY26cejlXX73MIIwgykh2qTLZLoNKfzH70RvTuvNCUHBT4KX97dbj8seNywOF8SLLds0oeuNj0oOAwhXRPTAI3wFJa1WULZEAQoV.os9YaD.SyVAsQS71eNZjZJJohh5EBUJpJXEGoIIjljSmzbVreWNv91EG8HGi8r7x3D3gexc3Jm+83bu9Oh2952m6OQwN2RjzKgNc5yf98IwUxFq9.L9wXpFiCEqXvZbHRJAbToF7Zn1qZQldb.FD0073ZWsUC0GaBDZdexC3hBfelRT.bjcDeZAva8kpDnnpf7N4L6byvv4mkN85hXDJCk38dblXIfDIRjHQhDIxuHDDCBxTAv.ayzz.PfDigphRTQHIoKRu9bwO5p79u0awhyOGm5zmhYxxX730wAj4RXxjJLXvZDPLnhPPpEnJnHhhQgxpQj4bjYSHQrrwJOFTkN8GvMty8ofLR6MCItblLYLUSFSpygwJTVU.RB0N59jN.OsU5DnQgIhVK5s9mHfPfbSBgfhF.MHnAE0qDppcF9wqtFoFGKOHmSdv8vq7JeCNvt2ENwS4lqw6b9qvEu1M4hezGwCVcM5LSelq+.RLYXRqchtpzSQYsKyFSBFSJdumIimfw0tOqS22ELsFB2pduAc6uKAHX0mZcyQ9Rkn.3H6HDss7OpQa9CeUp64g.d5Oaelaw4o2LcIIMg.A7ZEgPHJ.NRjHQhDIRjeAoV.71c.NT2eoshxPI0YQ.pBdRS6iM2wst6C3C9fyy3JCm5EeQ18LCH3GgVVfQDJmTQdZdyVodMdpHMs3p1HFE5lmhfgIaVQwDOiGWfKuO25Aqv+x+u9+g26p2AWugL+7yRZZW7gRLFAwJ3UOzVAfRqp8sGIQ0hImVXwhR8gVnVDNJDb.FDwLsZB0fmPkmP0DFzqKgxBJJFgyZYXlkCu2k4EO0w3W6UeI7KuGJlrIW58OGm8cdGN+EuD25gOBeVG5LbQrIyhjM.wkiGgxJOZnhTKzK2QIADQaJqYnwe2Fwu5TA60kMthzH50nlF+gi7rjn.3H6H9Tc8v1D+pR.ahk4leHysvPRySQUEu5QUkfFhk.cjHQhDIRjH+BSqDwsDZskyiJhTKeMwXopxyjfGmqCiJJ4128Nb0auBG7PGjSen8fUqfhBRLFJKCj2oOUgZco08up1rEaZ.2ZY0fJT5Uxx6PVmbb44bkO9N7G7+6eF+q+guIU1LVdOGfgyjSdZNdeAApWanFLfV2mwshpEs9HZqRedaqxrUPYiP4xh59o0XsXcNbNGFiAq0fwHjk3Pvi5qvWNlfu.mMvLcRYWCmkid5SvQNxAX3vADBJ23F2fyd1yyEuxM3Z25SP5zmJaJC50irjNXyRoxWhhmDqPUXKw30m50sVCrrk3XoY+sNnxpeaxnxzyoQd1PT.bjcDeZAvgoe.fhRduNL+hyyLyMSSYuThO3QLfQL0kKRjHQhDIRjHQ9E.6VtgR6ZuXpijNigxIEjZsDz.imTRddWjDKq83Gwaeoawf4FxoN9AYPhEqufNNGUUJ1jT7n0N+Jsh6ZErUGFUE9wfwfGC44cvXMTDBnIoDRx4p25db+GtJO3QOhgCmiEme.AeAU9RRyyfRcp38mTDYqf9sB.q5GE11SEnvaAiAiw.MABlHBFqAmyvFarNFmgNcRHKOAwDnrXDiFsNiFsFCyS4.yNfW9ENFm9Tmfk10dQrc3tOXMt3ktI299OlO9dOhUKBn4YHNKh0gGOiJJoqZvFZD1hu48.e8Mim5KBQXZfdMsxnoVnea5PG4YCQAvQ1QrcAv0keyVheUQo6fNL6byPuY5ghxnhQnphw17gVwdfHRjHQhDIRjewPMaygQcqovSyxprFCi2by59tUDFWLgNc5iKwwJO3A7WetqSmtc3zG+.r3LcIEk7jDJK05h40XQae8EEQaEyYQEgzbAqyRUU.iUXiMVk02XMVbtk3zm7nnc5y6c1yx68VuEyOSON0g2GYNkppBxRSwTssd5U0oh4Ma2XEYaxFEsocaUPETIEwT2GtdumhpRpp7Dvi.jmmRvWwli2fwSFCZ.Whg7rTxxRXiGcGbFO4tTVZv7b5icXN8K9Jzqee1bzHt34eetxk+Ht3GdQ9jGbebo4LyBySZVeBo8nagGE6VoWs.hoNXrDoV7Kr04uVmyqW161ufBQdVPT.bjcDo1DJqpKIDqyARfIESHPfjLGuzKeFRybT5qHDppKUEqL8Cn+aK33iDIRjHQhDIxmloYlrzN8MBO4Zp7dRRbHphhRRZF9PsH1idnCyO7FOhqb9yS+to7Zm4z3GuAi2XCla9EYkG+XboYfHHD1lPagoIbrtAgfGq.RnfTwSdlgpvXp.dwicTRSS4g24lr+kFx27ENJyzMEqwyjwiHy0AMTg0JjklfUBTLdSBAOYoYDBMBDEoMKrp0A2D5TZyXaBo14WqErFAQDBnTEpPEv3b3bIH1ZA8As9VtKGi3p6aXeIRvS+LKuvQO.+a8a+sX4Emi4ma.excuMu4q+57C+Q+Dd+KeSFkzmj4VhckjAtLTIkJ0huphfpjZsjklfQTbVCNmCwJnFEuDvSfRTbRb8uOKIJ.NxNBQqSdOrBXTJ80k3bV2LFLaelew4puZWls5K35+gs2E+.fHQhDIRjHQ9Ei1ER4a56TflvVRz1QjDTGNST6TYiKjBAduGUw0u7kH0BuxKcFlev.7UE0NxZrfwV2KqS6y0VWKqE.akI0i3HDPACkXTes6mDvZ6x918xblieL9dequI6ew4oX75DTEiwQQEToJdeEZvSh0PdVFh.aNZDNWZiaolFGT2ZbaV2ky0AzkngoO2Sthxs4bb6qQsZ4lyAVZWEpg.VB3zRbgRRBAN9g2G6c2KyxKuKFN+BHIc39ObMdq2983O8O8GP+NCXkIdR5MfN8Swl0AEgpxJJmLAiwPnr1Y5.JhwfXMM4ysGWrE.elRT.bjcDpWvXDLNC9fmhxBDivryMKKtzhj2Ke5GLGjs0uCQAvQhDIRjHQh7EjlHgxDPjlYR6zfip4+ZMssozaq6m25UdUL6t4b+z2f6bqaxQN7Q3X6cWfnr4nMneuN301WQca2.UrTK+rVXYfsRiZSa4LKBO3AOjcM+hrzvYoqKghMWkM1bL4ClgjrdLQsjk2Ew.aNZSBkkjmkhfvjIiw5RoMAp2pDhqEWqH0AJE0kPsQaDxN84Zu0JxbayP31+UhaaOuhQ8XzJrZEVohAI4r3fYY+GXub3CdD18t2MFiiG9fGxmb66xO5ruKe3suKq4UbCFRuAojYSwjzkPU81V80FEYMVRbVTpKA7wiGStK6Wc+pQjelDE.GYGg5oozRfJeEU9JR6jvh6ZAVZWKV2j+Os3WHJ.NRjHQhDIRjuvHMAOpudRBAMNxZQBloxRAHz7Pcaq5Ja1Y3BW7i3Bm+BLXlY4k9lmgbaFEiVkN4I0AV5TA00BMqmausimHEEGp3PvfAsQDtVGxShi948oX7lLYiMwZMj2uGi8VduK8grlZn+fYIylQP8r4Fa.Zfjj5DcNzLifq6s4l9cV2RHbsn2sRFZSalxz1Zshocug1fnptjtq+u54Z7SJv2z1ytJrw30ozWRlMk460gCtmk3XG4nbzCeHdgSbTduadCVY0GwEtzE47W3Rb66uBpMid8mgAcynzK.NT.uWoxGvfRddFCx6SUk+WQ+dQjedHJ.NxNiff0ZQk.U9JLNKCFNfEVddlY3LT4qfogiEaqhTLQwuQhDIRjHQh7EAoN8mESffnS8xzFpCXzZIpLcL6NsOZqk+g00kUVecd+KdYVaiQb3ibL1y7yfgRDsX5T3UTAQksl2vMRqUwhRyssEDTpXQMFRx5iyjx3wiw4bzalALpJve0a917e++y+y3ie7Drc5wtWdg5wLThgJeEViPZZNU9Pyq6V6ysgGUsPX6TAse11oT+yKMiwosByq1xd1WWt2sx1EKAwRPb02igJefxhJLHj4bLL2xdF1iCu7PN12+6w7KNj6euayG7tuMm6rmkq9weLOdyIrQkvbKtH1rDxx5f3xnrnjhwEnUdbFvGCA5moDE.GYGQ6b70GJIPft86wBKMOyN2LXSSvGJHzJ9caDE+FIRjHQhDIxWblNidE1lX0FoqM2Gpe3VoOb65ur8vkkwkuw83pW+lLbg44Dm5HLHApJ1fDqL80io+qZ6o11RPVQaVGXcYJWO+aCHLdREp5YzlaPZhEwIr5nwb46badqycNdy2+5rxlEze37r2klibWNIoVJKJqecooyd2Vy8V6TquNPnab3s8GQ2pEmeRl98aBIrl4yqMDvnsmOLaUp3M8VbmtcwHBDpH3KP8iwDJvpEjYCrzrC4jG4f7Bm5jr28sOLNG28deBu2GbA9QuwaxJiq39qNhzdyvr8xnSVGbY4TNofMVeLoYI+R92Fh7KBQAvQ1QjHIDz.EUSvXMLb9gr7tWf7t4TTUN08WZhC9s9Hzn.3HQhDIRjHQ9hvTAsl5XUpVzai.X0VKbUjlQTYqP4sJH3MmLh4meYtwC2fy8dW.rBm9jGic0qCRXL1lNnUwBv1bJs9wIZIL0UVPndF3JTu8bIVbVAzJ51KCLJ1rT18gNLG73Gkyciw7Iexmvsu6co+rCXOKu.NwPU4DjfhH1otVuUhWW+5a.pLIa0evM6W5TgxzTd35z8w1PXcqRdtIDsZ8NuMzoa99pV2OyVihyJXLMxia95vCuGK2IkCMbd9Fm3Xb5W4avf4mm0qBrxFaxae1ywktwGyMt+8YkIkHIIjk2gtcFP298Pql7qneyHxOODE.GYGQpMiJeEEUSvk3XgEWfEWddLNCaNdDVm7DCl8n.3HQhDIRjHQ1gzH.VaRpYUZJUY0z3ZasxwfoQHnoULX8JvVas0YldC4lqUvq+luKAp3kewSygWXFbRARHTOoOv1DByBnsBIURBU0NLisQgpm5RN1ifhWEBdOpujzDGaNYSF6qHyMKCVbdXoSxGdgKwEdm2Dq0vA12xzOKE7kXM1Z2W0mrBB2ZjLEnRRp2rx1i+q1+2VB1aEB21exSKMZbr0ZR2xhFo47oOTgwH3bl5yuAO9lQ5YH3YgrTz0eLqt1pTIVFLnG6+H6mS8sdMN8q8c39qsNaNZBe3ENGuy67Nb0qcEplLhdc6P+d8w3iBfeVRT.7Wy4oiM9s2mtHfwZHnA7UJpBVwh0XwIVrhk0bOf0qVgjdF16g1E6Z+KgIwvnxw.J3piY95vHnsCRDZZQk5OONRjHQhDIRjH+bSamuZTKh5PTK0gTUsnW03QMdZCJJSi3XSSO8NWmDJqlvhyNCOdi04m7luKt9Ky27UNAiVSYPOEwMFeREhIAFmfehvDW.MygGKdio1Y4sMifUMAUSPvhHVrtT7AAqjPpXw4GQOeA6e24b7c0kKc1eJad+ayu8q9M3j6YWjokXpJfLCETQkyiM0fDBTMJPR0.5kLGAcErZcBXW2SvAPBXDec0GZppOG.MmSrDjDBRBdIECUScFu0c7sLqgsDfGzFStMXMVrFGFwwDIAeRNoYIjJdR8av.+D1iYLGqqvu8KdLNyA2MYoIb667.N24uNuwEuK+z6Ll259dVdO8wLy7j35RkMmhIALpEmIg5Vb1PQvyDeAkphIwhMwUKVODvZ1.QJvPEF70IvMJJABhgfg56k1.+RwpfQCXCJAyWuMhJJ.9q47o90+m5IzlXvGsd3haj171SQCAlHah0Zoe+9L+7ySmtcQEpG15h.F4y70s8qiBfiDIRjHQhD4KWLDnnRwzY.2c0047e30vXS3LG+EXeK1CsbUzph5hqVr3BYfXva8Dj.1mNbW9Ej7jY3.6ZObvCc.9t+5+57JuvoXRw5LZy0gDCiCPoOPHDvDfNNK8y5BhiwilPHsrIbuZ6g21Lcda95NcerMQrMS6UXityRgp1Tjts2moYq2ZzyvdywbKs.6ZO6iCdnixR6deHVK28l2jK91uC24NWkO9N2CuIkdC5yrc6PZVJiplvJO9AzqWWxRrjmmR2zbRrI38dFOdLar4ljkmfJMIkcSjkEdhyEls6m0TGwqqV.1V.i80ShBfi7D7zkPRnQHqQ.iotWKTMPP8DTOkxX50qGKrzhL27ygKMgppJTUaRG5s8B+zaHhBfiDIRjHQhD4KarFgQEUzq6LX6Mfy9gWgab0qxxKNOu7w1OkS1.zRDp6o3DIAi0QkohppRRL1cz1uZxpLHKgEFLK6e2KQvOlUVcExmsGo4yxFgJx5zmDLD1XSL9RxxRAQY8IqgIscN9Zq6ZWY60zno101mHUr11r.Vzl.v5KNASqywgZomMkNsQaRdZ7jXLrTuAb78sKN0IONKM2LXBkDFuAu06cMt70uGezMuC2esMXSABYF51sCyOXFVe8GfTVfonDsrjpxRTERxRo6fYnJT0H.1zjF2NnYDUYUClP8ZrMzjAOB0tBaUpLriu.FOuST.bjOadJAvViAQDTUIDBDZ+fir.CGNjk20tne+9n.UUUfH3RbzNZ1iBfiDIRjHQhD4qFjlXYs0GQu9yR+d4b468.dm29mRZZN+lemWkDLjXotLf8JFLHVCdihW83ZSg4ufj3KQqpvWTPpqVXa2ACPLc4hO3VbuGuI8mYd565B9.SVcUPCDbPvBBIPyrBd6N.2JzcZi8RcIeW26vz3SZXGmEM5zMSanZU6pbcYoqrwFqR43QDBkjljwvTC6e2KvYN1g4a8xmgpYO.pJb8qeMd+yeVduK9A7w26dXybL2RKQRRJVaBpZorrhhIkTE73RRH0kRo2Sqq1a0pgMI2cqn2FguaeTjFZJy6n.3n.3HeFz9wHZnBm0.llNKP8Dz.Facv.jNSJyuvBLb94HIMkxpJ7dOFqAq0tkP4n.3HQhDIRjHQ9JAtjD1XzlXsNbtTVI.u8YOKaNdBG4Xml8u3bzw4vDT7kkng.FmCbFBAE6NT+TGqABBDrjl1EwkPPx3b28Z7e2+z+479ezsgzYXe6dI5mkiHPQQAkt59gUBMNP2NniY6KnrNcmmJxUzlf7JLUvJrybvdqDmdKAv0Sk35G2qS25QIUUE9Iiw6mPlQYgdcYOyMfy7h6michyPdmTJpJ392+Ab0qcSt3UuIm6hWkAKrWl3sj2aV50eFx5kSPCr4n0Xs0eDcR5McFM2J5scVGqRffUIXzs4Tcs6zF0fM70awuPT.bjelDvXLHFyT2ewH0WAp7Llc4ALyryPmNcHDBTVUUWxz15KM1zONJJ.NRjHQhDIRjuRfzLmeqFMlzdCP60kqbqaycu68PDGu5oNA4FGIhRnZB9PERRsKvAsNVS2ITVLFiwQvKnpv3xBFSfqd+Gve0O9M4MeyyxJaVR+AKvd287zOqCFCL1WfHfAWyrNVab3koy.4sJ84s4yaiCvasxzcl.3suEpMUstmaacc1683rVxyyIMw.gRpFOlphMPqFgKoGKNvxK7BGhW5EeY18d2Od0vst0c4RW3C4u40eat3UuNqOofjd8na+Ajm1AaVsy2Fma59fUASPwP8rMVk.poVzaPTjl9U1nfKnXC0gQ6WmIJ.9q47zo97zV1sYXlCfwX.C3CdpBdDig77b5zoCKrm4oSmN.vjhBBZfjzDvZnJ3qCAqOiKzz12rQhDIRjHQhD4KOJppX19yv3MVmI9J51e.OpXDW3RWgO4StOeqW4aQWqktIf5KnBOhSvXSHD11XF5KHlbGIY4f3HMMEBARRyXgEVlcejiyEu9s392eEt8ctK85Mf8t7BjXgppRrhAC1s47ZiCunaIzcZHO0JBNrsmJ.paGs+WOukaJu51.1BaSe4ZYyQi.wfwUeQCpUsqDZ9uGbmqQGmxRY8YoNobrCtaNzANBKt3dXvbKxG7Ae.Ob0M3Z24tb4acat0CWgwhPRm9zq6rTJknlsD.aCJNU2lSvfZjlfBqtujcAY5OqemZg+y4DE.+0cjO6uT1dTvWO8uox6wGBXbVxxynS2drvdlCq0VOKfKJPDgrrLvHTVVhX9a+RL806+7KRjHQhDIRju7YbQI8y6Q03MYiwina+4XsPIu469dbmadK91u12lA44La2Tr3oTJv3rH3vWEXmNEcl3mPP8TU3IOMgphwr9pOl7d8Yt4FRu4WlKcwKxG7tuMNixg26RLSmDrBzwkfFZMRodLH0VJxlsuxRY6N7zLNij1gK7NyA31Brd6Ees137rJByLybXSbLopfwEionpDAEWhCWhiE5mPlnrwZOhMWeMREG6ctAbhirGN0KbJN4K8xza1AbsqeEd2e5ax6c9Of68vGijzCSugj1yRvzFDX08crMTW52SczRYZ6QKpNUltfh+mw5y+657qbAvNmqtDZk5wnSKsiWG4q4wv8yZ11rRG.LsN+17WyooITVVPYUY8LAFEwJr3RyygN7gnzVVOXzaJKZiyhODvqgmP76m20IL9tejHQhDIRjHe4hMIkf2Sl0PRZBSRE5L6PVcRf27u4mPUE7pu3Kwxy3X0MdLtTgjTKiGUPdVGBA+NZ6abEHhhw.d+DRnhNoB3mfQENz91C851kMW6grvfL91u5Kvv94Tr9pL6fYXTQEgPEFCjklfQqX7lqSYYINqqITn.DospjaVuaS7Pq6LAvRSHSUGpTxzQKTauAW3qnx6QwfwZwXrfsNdpBAEMnnJjZrzw4HU8XJKvFJnmANwdVfW7EOFuvYNCCWZIVcsw7gW5Z75u4Y4u3eyaPnWfRik4laYxsYTDrrw5iPBV5m2EBPdRNYINzPfhISnnp.WdBYc5PY0NaLP87N+JW.roo2QeZZE9FE.+rksZh+16ex2qTMf3rfHTU4wkYYtEli4ledbYNTafOyfj6my2Viu6GIRjHQhDIxWtnhAQUTsr1zhDGdwx0u684JW8iwWZ3EN0oXeK0CAOhs1fjPEjXSIn6LAvhTVKNsQznHkXHfUUbZfto8XOKuLuzIOA+5u1qvgWdWXMA1bxXDigfIAup0lzTVfy.c6zgzzjlIXR6n3zPSBX0dj2zqt6zPvpBsY9+plvT2eq+dO4paamNwaubsCRSxMqRcu6R.qVgQ83nBUKw3CzuaNG9.GhScpSyt129QMVVas04cdqWmye9Ohqeq6f2kyBKuHyLnGUtLt+idLfiwSFyjIkX.xxSIoSFUgR1XxF3jrczw+y67qbAvsBb2tH3n32u5PPZ5ggsMfrqeX8W68UXc15DkSCLyv9r28sWFL2LT4qfDdxF58oZr2eVcHR72.hDIRjHQhD4KWDwfFBnZfJMPRhCqjwpiKYk01jyetOfCc3CywN3dnSVFpLgPUEnFRrN75NyAQW.BXnRLDrBAI.lZAvFEFu9D5k0gcuvbzMIkhxMpGtOIor5jRLo8HIqChAFOYLRHPudcw5bTVLY5XARklxetIgpj5ZYDQ2gk.rop40cqTVdqQsz1KI61QjDXCR8r4UE7VGpXIHBdiPn4V8bEFLUdrkkzyXXOyzmCN+Lbrid.18xCYuKOj6d6M3w2eMtzktLu2ktL23AOlwYcvN6.xF1irN4H1TTEvGpK+YifXsXMl5YI0Wi4KMAveVOeT.7ydzljgqNdzexd+s4m.enV7aVmNrvxKxbKT69akujcZN3G+MfHQhDIRjHQ9RFwTGtoTKfKULj4xw6rTQB+k+E+.lawk4HGXervLcPDO9xI3DGNr32ogfkJDDGdiq1MTSsynFTD0h0jhV5QCAFOZSJ8Ezq6brtp727SeKVcRfAyLGcS6hK0QwjIHZ.CJUUUfwU6trAZcAVzsIDdGJ.NXJ2pjmamrIMkEcsfWEg5PmxnaoAWZ9OeSKHFPHHFBhfZLDDKF0fSLXUASUEgxwXvSGGr2EGxKc7CyIN7qxQO7gQMVt9MtNm8ruCm6RiGPlK...H.jDQAQke.28wOlMpT5LyPrYIzOKm77tDLBimTh26QLVjulGCzeoGBVsBeau8YUdzQ9xisK.V1l.3lVZ.qyx3hQXbFVbWKxBKu.tz5q7mwZv+oDL+KFQAvQhDIRjHQh7kKBF7zjFvhPZHPVZFlzNHoc3u5m7F3EgkVXNN7A2MIDvWLhDwgAXmU.zfDrnhEuXaJG6lonqXPvPRZW7J3LNLhAahC0kxO7ceW9m7O+eAezsdDl7Nr28uaRMoXSLLoXB9pRLlDPLaqTjaqzQcpP35vh5KNASn90sQArglWdpSk45sQ8SVerwVioIDLRQ8+.CnXIXr3Ig.VBhELIj3xHIwh383mrFgIqiSGSW6D12ByyINvRbxS9Brm8uGprBex8+Dt3ENGu9q+S3VqrB24AOBMOmNC5Spjf35PUoxjwdRce8dE3eoI.9oK64n.3uZvmq.3laFWcYLOXlAr7t2E8FzmxfmRuGqyQfcVIv706+7KRjHQhDIRju7whkJMfl5PLfaRINrXSxQ5zgO7NOfa9w2jDKb5SbLFj4PKGSpMAMDpcscGs8aioXWc4AqJF7HnDDgwEdBg.VAT7fIPgpb06bGtzUtJu9aeYVabEc5OC6ZOKRGSFJUn9.oIts7hcaNzVWVx9oys2cDhNsqdaGGRsxaaMGuc9D2rh5lfxpM+c7XZJQaQ.zouZ.fKwgGOAeIAIfKwPhQPBd7Sl.gdXDgg8S3nGZO7Ru7qvAOzAwk2kIkk7Nuwqyku104x23V7IqrFE1Dx5MfNcxoSdWjpQ6ri+my4W4BfUU+Thd29WGBe8NExdVyeaBfq+9AlY1YYocsDyLb.Rhgfpn.Az5Tj+ynGf+48iEiBfiDIRjHQhD4KWrMN.Wk5vYr31bLRoGRc3sILtaO9I+3WmMWaC9lu7KxtlsKhufDiE0WgZ1giQHsDip3BJIdHw6wodDIPvpnNgjNI3LJhujrDK4o4L+t1Em5zml28imvCt+C3N28tzsaG18tWhLqEM3I0ZQ0sR+4oZfEOB08CqtCGCRnNLAac4ZqsienlM3V1HARcINqRct6DLRS.XU2gv1fVOCeCUjD7X0JDoBjJl3KXTnjB.0lgjzESZWrI8PSVgwSVghMdLcDg8zqKmX2KyoO9Y3keoWkGu4XLY4bu6+.tzUtJW5F2f6u9l3ccP61m4X7N63+4b9R0A3m9FPT.7yX9YI.1qUrzxKyx6ZQbINJCArINPDJJKw39ruBZQAvQhDIRjHQh7USrpot+eSxvhfczlDppPRRozkgcw43O5O7OgMd7J7a7sdM12RCw5mPhwRnZmK.1pkXPv5MjD.q139qwSvnrQYEFiPXxXJFOBSHfWfRigAyNjvtNMW4RWlO78eW51Imidn8xrc6.UkSmQuzNxeaPHz3ba.E2NZ+WzZGfDsMgm2595MY85iCMiiofoVTbnozrklRi1nArZ.mpX0Py3HMvZatARpij7NXSxoL.iG6onzSk2fKaERrPlQfIULYswTMFxRSYtEx40909lr2S7hTXfO9i+Xt5E9.9vO7Jb66cOt6m7H9dmbe6ni+m24KkPv5om+uppDBABgPbNAuCwaXZC3+jkVAOgSrSuVT5Sdsox795+3KT2GBFSci4WJUTX8bzW7XjLHmpj5mO3TpTOJUXsBV0zjncO4sOqfg9ukvhNRjHQhDIRjHeIQkaLFTRJErkPH0glaQkIXBaxLAKcS6wO9s9.d2O5Vbpuy2k451iMt6GxxCSYTnScPRI0k5qzj.x0U8acO8ZCRcPNoJHABVOASEdmGSHifXvagRmRoSnxZQIAIjPtjhMXvHNrIYnVGnJo9J5TUvqtmdLq4tbnkE9MdkCxw1eO5jLArUDHfWsPoPdkmN9Rrkqi2uIUYI3yxvUVKSttTtq68XUZGYRfPELsM+ZCpJKAIEujhkIfD1VRP2tF7sFsS0qOuMcn0F2m8SK065+cBAiAuw1DHXNTbj4xIQsXqTrUdRz.YFkTihy3w38HAGAIC0kfjYwjo3raPleD8pVkSMvwuwQ2Ou1wONKtztYyRgKeiGv6d9Oj+OO6U30ePA9YODytzrz00EmKmM17grwFO.StmRwyDLTEbTFRAoKVaGr1tngmucP9K8Pv5oINmf2Y7zyw2VjeF2usWA.viRPCTQ.wJjjlPVmbFN+r3RbXSbXs15ndea+61ogHPjHQhDIRjHQ9xE03a5cUG0J9B02IdTQvkzm6u1DdmKdYVeiQbpW33bnklktLg7pILw1gslvPgsZ005W8o8xZaPPoxVB9TTb6vwviKIid86xoN1I3zG+jzKsCkSJnrD7ZJYclEiKC0GPDHIyBVCaVUgWUbX2pGgAZZDWl5lqrU+3tcqapCoKACk6n8+cJpzTF2pEvMU3sLMkqULhPm7Nr7B6lCe7iw9N3Ao+byRV2NbiKeIdvGeG9nKbd9nO5Fr1FaRRmTVb98Pu9CYyJO9JGVbjmzgdYcwYMTUsIiFsNIOmGhVOyE.GmSv6Pdp9sc5nNaqu8Sb+S2itllGDz.9fGu5w5Rn+r8Yl4FRu9cwkjfKIo98CQabwOT6ZeT.bjHQhDIRjHOWgHk.lsD.SctuzJ.1Z6hj2kKbsawU9vOjAyzmW4UNEKlmx5q9XLY8PzlDOtwcSPQMZS5Ku8BAtV.rJJJ0IvrcGNFdBUULSdW18rKQljPwlSvHoXS6yjPBqLNfIKiROTTVPmNI3bNJ8JViio6hshxehxSrQ.uHeJQvSW+LU6n8+cJJI.NPqmmvpzHBl58z7DGRUIZwXxSfAtT1+byxg1+7bxCrHcmc2j6rbqadYd+26c4bezE4JO3g7XDF65xxytHcR6QpliLtD+nUopXEBLBqq.ij9L83emxWYD.+Y87QAv+rQepSQ+rbB9S8yIT2f9TOqewZna2tLb9g0t+l4v5bXr15T6lPyEqPiBfiDIRjHQhD44QjJDsorewz3P6VN.GDC861m6s9Hdm28cYznM4kegWf8NyPlTNgTSBhDZlstAfvTQtvVxEMPiPxs0xdnX0clCvUiFCAnXxXVe0MPCJtzdb4adG9i+K+qYjjwLKu.4oNpBAL1.oVGBFbVKAsV.6zJarc9e1r+1NOeqKw6lR8FcKGukmsYXjn0AvkLMMZglc1lGqHAEJqX75qynMeLniYtdc3fCmmW5jGgy7Bmg8rqEQRf6+nU3pW8Z7Nm6R7iei2kNyrOFOVnSVFC5mRRlAOSvalfZB3H+YzQ9ub3Yt.3ml3bB9WLlVBz5mVjK72RoOu8q7U6bQyYHsSJClc.yN+rLXlADHTK90zdM7BMI6cy6UwN4MRjHQhDIRjmuPp.0.z5dH.APpKm4M1rf7r9Dx5x49fKxct0Gy9Ov94EN5AQRyI0OYak+bS3RIPXpKp0Ysb638glASjZpcW0n6LCTRSrXMVrhEmKit8GvlUJ+w+f+M7+z+K+uw5FG4CWfkVZHooYH5DRjPcwKOpDwoHhNMonmN6hlV51aMiea0U1TDzS6e2mkXCfg.ll4xBvT2rAXznwjllQ+t8IOKE06wWUhPEYIJgBKK1MgSbf8xK9hmlCr+CfKsKqrxHt2seD+vezavku4sYS0S1byR298vljilHLwWQFQGf+kBw4D7WL975A3V976821ugu9OrcVx6zgAyNfYFNKcGzkjrTpBUMk9b86Ed0OU.rwX9zVPGIRjHQhDIRjuRScFPapE+hcZO5J3AfISpHqyLjMSGty8dDW7RWfzrbN9KbF5kmQV4HPBXIzHHLLUkn1XcpzpbTq6qVUjs4j5NS.b8.IQPLNDqCqMgJivsdzi4VqrB+z25mxcdvCXlgCXO6ZHNeAZwDbXYxnIXx1VIMqzbr2dHr0hq2dES9Dq38Ys.XUaNC2HHm.ZaJzhAmygHFpG1NFbNKNmEz.ESFw3UdLZwHLVOy0YFN3d1CG8nGk8u28w91+g3SdvC3gqtBm8hWf25Be.23wOfxLKclYFxRlirphmoG+6TdlK.NNmf247Y576miivOMAojfnXSbzel9L+hCYlgyfKy1H1MLMC7l16u.Fwh03hWfhHQhDIRjHQdNCCgZoSs8NpXdhd404RXyIioeZWBIo7Nu643wqsNG9HmfE10PFTMFpkPCPiH31zO9IUN1NZfzs0es6zJHzSIEUE3CJUUkLobBt7Nr3d2Cm7keQtvG8Qb8KeQxSfW6kNMCLA7i1jrjbLhsVrX6dnJMg0bSe+ps6+s8A7zCk5Re9Yr3258k5xNu9BODZJs7ldYFCYY8nxqLZ7DFWTTGSYNGAmfZsrT2NXBkLZz5TVLhbKLe2Nb3EmmSbj8wQOwQwMnKW+d2kqdgKx6eoqx0ev5LpxRosCGt2y2s.4ybAvsDmSveA4y4yO948iUpjBPL3RsLyrCX14lit86QPTJJKwusKPAvzvupczUEE.GIRjHQhDIxyWXvWK2UrM8Abyn5QqEq1sSGdvCeDC5ODWmN7id82f6duGvgN4YXuGd2LuebiH1vzRbVQQMRi.3ZqfEodd2pR8XxbZu0tCE.KlJTERS5PhKEeUIpDHMsCyLX.m9kdYt34OGKzMi+Ae2uMy5xXiUWk7N8pGqRgxoBw2p7laFYSs4aS65eactVZKQ7s0uvOivaZEm253ktMQ6PYUIhXHykQZVJhUHHPk5AQoq0BIoHc5gZrTUUhVMhbSE8yLzePO5M+BHCWjMjb17QavmbqOgabi6wUu5s3e+u8oeld7uS4Yt.33bBdmg74bqkPntmcCz1quscgADTkJFybKLO68f6kE20RjjmvjpBp7UMo.nsNpnMMyIXw7Ylb2QhDIRjHQhD44CjVGfM1FObaJA3lRZ168zq+.VazH51qO6Z+Gj+5e3Ogyd9Kv2+262gkkBxxxYyMVmM2bS52qKFmiwkEjlm2XfkzTVwBnlo99JScp7KNF0iUbfWpC.KSchTGzBjPfg85y2+a+c32823Wi9FCSV6QLXvrXxFv8WeSzRONiCqwRUQAZUEotTxxxvZr38gldjVlFPX0mi7MkK8yVGPqRRwllfXrTVVRUQQ8EuPpEzu1JOhNIoHDXx3MATRycTVMl6+nGx3pDb8VfBoCa5rTZCzOKmdtwL5wOfY6OC+A+e+Gwu+eveBSFanyrKyFexiQLF9d+l+17cO1bOSO92o3dVuC7yCedyI3n.re1L8BGXjm3BKz98lcw4n2L8HKOGiy1LWw.ey41XK9FIRjHQhDIxeWi5xdFILcwdhV2at0FIpfVhU83zJF1oCKszhbmGsIu64uCm53ojkYvl0sdskXqWWda9PssknqaObh+kF1FApls01eU3BFDQwoBUEiADRFzCa29roWoPUXl4nqVfAEewDL1LxxrfprwZqS.o1k3mZMvZSxOaPehiumE7vG9.xSSoiyhSL0B2sVDwRHDXWKsLEimfQgDqkwimfIQna1P5rqgLQy4lilvY+vqxM93qwRC6vu1oOLGcXJYYNV4Q2iu2290XxLGhe76dEt8MtE8meA9s9NuL+teuuIvJOaOArC4q7BfaE49zyIX8oJM2utxm2YfomsLxzQXj26mJ.1l3HIIgEVdQ51sKocyPbFBdesawpV+AVM+w9SucZachP7sfHQhDIRjHQdthvS3foGSaxM2r9tVwrV7jJAVZt9bxSbXt2O877W8W9Wvuyd+9zs2bjzsK9xJTwfpd.odzH8q58esIEhkZA6llDqwnM2a7zI0g5KPEH3x4J2457A28hTYx4evIOIcySoxWfDT53RnpXBUkkjjlN0Mb8INOY.pyFmm0c.6gVdwlRVOfuxOcM9sHpxjIioSmtjkjxnxBTbrouf2+hWf+xK9Ht4MuMevEuLkaNhu6qcFNwtO.ggCfDKUdO66XGf8tQBkuw6fNYUdkW4E3ez28k4km2.ieFdv+KAdtQ.7SSrLb+4is2K0UUU38dRRRnW+9zsaWFLbVRRRv5b3UkpPf.JXDDSTcajHQhDIRjH+cOZkvEpE+13bqz5ppQQCdbRfDIvBC5yKd5Sx6d4qy4d8eB2724LL2hKxrlbLYSPUCpFvnBS0goz3R6u7IHsutlFwp0hSoc97pA52sCSFqLtr.uS4ityc3e0+e+.t1stGK9e1+4bpidDlsSOzBghP8DNII0RVVFE9m9bU3IFUvOqQBELd7XJJpPCBASBXsXSLXcoXcITZLz0kfw4vkjSZROt9MuF+9+A+g7G9SuFIo4j45wtWbA1276kYyGhW6vZSJnSuE4ct504u3G7mwCuy03jm5H7O5u+qwqbfEHsbSpdVeBXGxycBfeZWe+5t.31RJ4yqTkac9068TUUgwYoa+dL+7yyryNKj3IHAJCka0mvlldI9qH+QdjHQhDIRjHQ9kG042baRBW6XXc08skfUUUrFAi5IG3jG8fbfCrOtz68d79W9Jrm8eH5MaJhKixpRbR8bEV9rFStR.ze4U4fdyVpQklJRtMooUfJeECxxovKLZxDrNGc52mJEt2s9X98+S+Wy2+2bBemW8kXXdeJ0MpOGj3vqUMmGL0oh8SIh+Ys6u.r1pqPkGb48oWu4nBGapvpUk3q7rxcuK28l2j8s7Rbn8e.LIYDvPZVW12dOHuZ5hb7CcDN4tOH6pSO127CY+yMKpHLxNKqFT9W8m+Wxa+i+a3vm7j7ev+teO9Meoiy75FDV4gPm9OqOEri34FAvOsv2X4O+yOgP.u2iy4na+dL2bywvgCoe+9rFqfpJk9lqky15Y35IZ9Wuu.CQhDIRjHQh72MowwCMzH2q18z..hEU8jZrnUS.WB6c4E4zm537FuwqyYO+k3Lm4UXeyNDUsHTgpFLhfDDDLLcZB8qD7zzMt0F17TKWU0.kgJzPEpujTwxKd7Sx+N+CqX142E+Y+Q+oby6cOJCE78+0eU538jpd5j3nX7Dr1D1JMnaG0Se5syyJFLbeDPXhlvFJbmIJm8BWhK9QeDO5Q2mqbgOfpMWke2u2uA+Gt7RLS2dLZzpLauN7u2+1+d7RACGdW6hcgfsRgIEjlHrtBaJv+h+2+i4c+ouOCmaA9899+V7ceoSSeFS4nUIKC7+r2E+JMOWH.9o642m99He9NA21yuFmkNc6x7yOOyM2bj2oCAUw3jljhtoWeMlowndHDlllbel7UnRAIRjHQhDIRjH+7g1VxvTm2Kl.M1mF.wfw3nRJvYcTVUw3h0XP2E3Tm5DbzSeB9fK9db6O49L43GEGFBpT2Othc513oW+3uLyMFiop1YV0gnfQqp6+Wo9nRrNJ1XSbDH0lfTLlkyliem+deKN7tNHW4iuGW+ZWi278OOeyuwKfKyfehmNYIHF1ZN6hYqdZVpKQbSSe.+rjU0Tt7MtMm+JeLW89Olqb66yGciavidv8H3GSdZf8L+rjN6.pnBqTfSKHwnL6h8HGG8YBIUqR4lqSZZWlvrbw6rIu4ktE+4+I+.lat442625ay+vW4uGCqpnXzJTYCT4Rv8r9DvNjuxK.F3yLvqhhe+4i1Fh24b0876fAzqWOTfxxRr8q+fpoUR915Y31wPE7jZcMeE4peEIRjHQhDIRjuHTO6eg16BHsS3VswPE0fw.AumIEUzqKrqcuDG3PGj26G7myZquAkMyOXaSTO2J.VzvuZmjHZ.XqTq1naUJzF.WpkU2X8++Yu2rnkqqy6762d3LTS2YbwEy.jDj.bRbThRTThhjRTRVRs8pkcG2cb6ztiWdsRxCIOk7XdNO0YkU+Pbb2oyZ0osUTbrTKaIKKJwQSQRMwYRPQLwA.BRhg6TU04b168Wd3bp6.3nDfzEfX+SqCKT0stmZWSWc9e9999+m14ojmkyBKt.UkP2tSwtlcV9u4+w+m3+q+s+uAFK44cvhiJY950ro1LvzB3Wyygy+NY8u97ye9ivi+ydRdvexOmW8MNMNkl7wmhsbk6isuqsyMe86kqdGywt6jQR0RLX3xLQ6bbgRV3zmjwlZNjpyvvEdKxLJ5zpCmVJ3IOvyw2567fTLzwG6ZuI9xe1OM6dRM8OyoHWqv1oGKNb4KNDP99f5Q+tO7EPuc9qNqHPqI+fW6FbguPYQOZLxW+5TKiL4qUGx+0d4nuDZbCqqPttt0kCl5ckSpvIAd6ScBlZ1YXq6XaL0LyPRqjlVbVPq0DbWT+1ejHQhDIRjHQ9M.duGQDRRRvZsTUUwRKsDKszR7O8+k+ZlbxN7m8O6qwW95uBzEKRXXe5Mwjb5EmmjjDTBXBqQPsB7ZGhJfNrwJgpLYRVZwSSuVNRrA5uXexy6Pm7o3LmZd50JAu3gDAURBCKEJWZHsjD50tKKL+aQq1cHMKkgNOEhPPCdwQQUAcFqMkECX3v9zNOm1s5xv90tL8X8lfhg8QbBY1DRLYLrviXRP0Nk9R.m1P.g4cyS4xKwDY4zyjPpyQpxv24vuL+M+0+c7JG5M4x209Y+W9d35uhcy918DjwxL8DY0uGRJdURyIiHfVMDEAZqDFToY9pLLiMGKivC7zuBeyu0+e7xO6SxW7Kc270+B2I2v11Jrz7japiJ0E8UXyxw5p1Pe+6bkK1EvCbocNAGTfVqvXLDTPH3HHdpDGhBlYSyv3SNIsa2FaZBZsdkSPvnPJORjHQhDIRjHQFgpwSXVagkTp5i2LMMkY19VnZgSwwd8Wmhq+JnaZKpFNfReAYo1UrPFAdWNTyMdajRIUnBk3FVR6dcoU2TJJUTVDvN13LvOnt0mqpP68jYaSmw5gTFn+f9j1qCtfPQw.BhBUhgrjDTZCsxyXgEOCc5zh7TKAmGuyg0nPqRQoELIVFTLDQYfTKltsvogE7Bu47KyAO1Q30N1qygd4WhbihO8sbS7IttqitsGCCvsu+agN+A4LXIEaay6jo5zgY5onmUghEobv7.P.CqDuUih5JDbACCcB175tB84N7ax26692xacrWka3VtY9rexOI6YaamDUfAkE3RrfwVWgc8F+6emqbQu.3K1yIX86n+PTu6WakmdiZ5907gOiFQCAIfO3nJ3QYUXsVlaGyRq10eoMKMEOd7RffT2pKFVcVMhDIRjHQhDIRjQBfgUGKNkRQZZc96d8W293m8f2OO6y+7bja753Z1xTPRBkCKHOOkhpZmTVFEwR06IVaVCuQhgEI23v3CXIgPRWNvq+57RG+vfV3ybqWOioRHwO.2hKiXfrtiSUlhkJFPV2tLr+.7UdxRRHwpQG7P.rJESj0gVIsovOjEFr.hUHMMEsVvWUhRmPV2tXS6x.AN3abZNzqeLN3qcbd0icBdpm4mwxKr.k8Wfq5x1Cer8e8TVo3zgkY3hKRmMsItlsuEZkOAszFppDzg.Etgr7hmh1sy.TM4k7ptkMhEEvYVTQ13ShJoMG3LKve28884m+DOL6du6ku9W8KxmXeWASqTXqFfQBnsJbp.9p.dIbAvov3biOxH.9r4hpbBdEua+c4xO.755cPPpy42RuCShk714ztaNSM8zXSSHIOEQotP3u4DIRjHQhDIRjKf4rMbVQpGcNq0hVq4ScSWKG3wdDdwW3k3oeoCxdmaJRZ0kxEdaZkZvDpk6NpNOp0YaTqZ.WaX3Fv3sSHL.BNgJkhi9Fmhu0+vCvq95Gkw27+8b06bV1psGlLGEKWfVuLlrDroFF5FhS4HMCRS0XbdFNX.AWc0cqbAFV4XfygxjRqtig.LXPeHH3V5LXa0hkbvSevCy299dP9oO0ywBK0Ge.15N2E66pub101lka3JubtsqeeLU21TN7Ln0Z7COMcUVZopPbUTr3BXLJzspqTqW0XPXn.wfRB0YkbSTUsbnM4Is43kB+m+6+A73O1+Hys8svW8K844Su+8Rl2ghRJFrDIoZRxRw4b0cCf+hbGvhOBJ.9htbBdkg1WsZNlIeXz9Nx0lAevgS73BdzVEs5zholYB5zqGo4YnL0s8bPjZeqSoPoz0uVcwtOlGIRjHQhDIRjyqLpSJWq.3Qhf0ZMersMF6btY3vu3A3Ie9eI29G+VXa409LSnrDHYk1XTZhZI.TA6EDSemTVR9XsnrzyvgkDRf7wmDuNgS7FuI+e9e5+W972wsxW9S9wY11yPdx.bECvUtLZUcUwSLJRMZTx.pJqiaISRFIsZifkhfFUlgrTCCAdq4W.WYfMuoYXbwg2po+fk3zm4MY9kNIsGKkK6p2Gaea6ja8ltE1yV1By10PGshN5511NQAclrCU8GfVKXJWlgCqHU4pmI4zVjjkPQYI0UaWgVB0BfaL1LTP6YGmWcfv29Gd+7CtueHooI7G70+83q9YtC5fhhkNEEhGTdr4o3TdJCkX0JxSrDJt3V.wGYD.ewpKQOxLqdut7CxA87ZAuyiODvZSnc2VLwTSx3SNNs61gfNTK70W6Fehp9OdwZ9iZQhDIRjHQhDIxHFc70ms2wLRT7Tn3522kwO+IeddoCcTNvQeMl4p1AoI15V70lfFv0DRuAZZaVgFQXavUQTLTUFvGDpBEXTv9upsyW5KeuLwrSy8827+CEKuD4lLtqO4mfIrcPYMTtzIwDbnsIjkZffmh9CI37jj2Fa6tDTofjReuvQO1ayqch2jC9xuLu5QOB6XGamO2m8N4yrkbpPwzSjyMeCWKiM2L3UorksuSlHqM3gIMJRDnbokXPnDsTA3PkmfTEHnDDpPIARr0UUuHTPkyWmIyBXkv5pplWYQTPeD9AOxSxe226e.IHb224mku3m91YZTL+IecltcFHBdshBU.eYAdumV11zVmxRTrQ8N24E9Hg.3KlyI3ydFfWMizVuaOKm0enXzmkCDHnBnS0ztaalb5IX7Imf71YDzATZEAQPBRc0k0pFQ0ZBAINAvQhDIRjHQhDYcLRr6HAvilCXn93qSofad+6kG5J1KuvANDO8ANDWykuc1ZZKLA+JM3rrx..KqTUGUSUH2HIIYb5Orr1PpxLDBKxj5db22zdYOacKb5i8Z7lu4I3a98d.VRR3NusalozojnrjpEJcVToYHREUoNi73O...B.IQTPTgBro4X5zlSMnfW4MOA+xi+17puwY34eoCyQNxQ3Tm3MQ7NtghJ1wd1KWWqInRBjNVOlqaOF6JZgfAKVBxxHCKncZKjhBztJZ0JGkwRgu.sQiJOqQmiGiVfDCUhPkCTJKpP.aHfQbf.AkFmxRk1hWY46+zuAeuG3An+xKyW3tta9m74talUaPWUPVPHI0fCvKAJCdHDHKImVZKphKtE+BeDP.LboWNAu11i1q7HFE1LCs50hdSNNc50k.dJqppaA5P.zp5.7VAnTq7GxLWzOF6QhDIRjHQhD47IgP.iwrtimdswLpewSxd1xlYm6XG7KdxmiW4XGmEF3XtdonSxH3Zj.2D+NM6geq9b38ijrwYoAmfNssj2xxYV9zPhioRmjz45x+C+Y+Y7+5e9+G7Sehm.cdalX1435101XS1tzMUQ+EJHIzAspDwBlVo3jTd02737XO4yxe0ey2AG4T4f1c6wG6luI16d1MW+92KW2UuO5ZVh9ECwGBDJWFW4.RsVxRRwWTQJZZopvQEC8CorBHnoH3QCXDEAuCQBfAjfP+hRTJE8Z2AUi3WsHMyhslJskg1V3TZ9l+0+k7Zu7yy0b0WEe464yw9mcRJVbIbCWhYlZRVrbYpTdBVMJiEqVS6jVXcAVdoEg1YaruAdNxE8Bfeu9h4HxyxvGB389UxyrQmQKUiPvMVd+Efth4AnW84UXMVRuCGSN0zroMMMiM1XXRrT5KwZMj0Judf0aRHbEfH.95yFmVq2v8ffHQhDIRjHQhbgEFScOB580y54nHPZDoUk30B2wm7SvKd3Wkm+YdNd1Cb8bk2xUyhEUjaSVydazAaN5fZ23O3ykq7j0YLDVBWQIcsVBRIgh4oE4b0aYb9y9W9ufY1514At+6m+Mu1w4O8O8OguxMtWNQoiA1Vb3e4qwVmaJla7YnfJFVVBlLFLnfYlZZ1zV1AW4UsO12UtOtrsOGS1NmL7n7CXQmGLInT0AUT2z1nEvTEv1TdphhBPowztMhRiSCXywMR5i1hnpGww.JxZYqC4nJOkKuLcyRHscN8KJozlfNoKOyQOF+a+e+eGG9EdFtiO+Wf+fu38xtmdBBCJoiQgoWGF3JIXrf1BZc8aWAX3vg37ALYIWzKe3hdAveP3Cg2wbL.WDXNVrp32QlBcfFQ9pQ4plhImYZFqWOx6zFSZs4CHg.tP8rV7dMCwpK7e5GIRjHQhDIRjK.wHBVslMO8Xr4Ylj23UOZcUfk8SdRabJG5ffRBMIaxZ8A5Md4SNkTKBRznBJzHXj.ApPqD.C6emyxc8otUlewE4vG4X78+gOJG9kOLlfim8IdBZklvc8Y9z7Y+T2JsxLzIuE6bKaia4ZuN9r29mg7NcX7wGmdZKY.oJ.oON+PJMZTBXn97AnalYWkTe6AU.g5VE2qqGQx5J413oOM0OK.DFM5mBnDAMv3iMFhqhyL+BTjjgpcGN3IOC+8O3iwge02jO9cbqbyW6dYWaZbFKQSRYEZ7fQClDBdGnTX7TKLWDTAMhJf+i.MY6G8E.ulyb0YGn2ilugMVp+D76n4PTgZgupZSrx6qE9VW8ZCVSsK7s4sLGsyyIMOGiwf26IDDVIAxMMlD1uMeJEIRjHQhDIRjO5hp1zk19DobMWwt4YepeNO+y8B7R2v0v0uy4vWtDJcI5ULgoFEap.Z4B.Iv5BDIfN.VuYE2QNnCfpDo5zLc5j7YtlKGkOv249eB9E+hmlm6IeJlZKaghW6fr8ssMzFGJ7XEHEnmIiqem6gti0iPSUnjPIR0PFVNfPYeDWElomfQk3RgFsXQjQdDVsPyfpVbqnD.GJU.kH0BcowSeDK0u1pnogOAIfwjyhkUrHVR6MMucE7s99OH2+C7nj2aZ98u2OGW1N2AaqWWxTdbLrVfsdzIpPg1qvF.Sn1AoC.UlZw3IWbaBzWZH.dT6Nu1A4ejH3MZAvua+AfUp76n40UB3CdBhfMIg7rL5zsMYY4LwDSfwXVmK8EIRjHQhDIRjH+lBQmwvk6S21YbC6+x49mdZN3KePdpW7k4p24bnUZrB0BfABMtOrIDZZA5MXIHJGZB0FxE0adEHJGnbDbCnDOcR1Lepq+J3jKE3EdwCS4BmlMum8x89E+XroIlhq8Jtblc7wnp+7Ts3YHMuEiYTTN+7DFkTMZACBoFCIc5gUCKPI.nESyIInNmdCMc4oWqWohunBnI.hfQ4AothwATfJzTjKMJVMukWb3PFfgzI2JCP3gdheA+n6+gvXz7E+peYtkqb2zKoC4JGNWAU9RZx2IDznDEFIfM.5PilDSsWdGt.nE1OW4i7BfWaEeWalls1rMaCc8oFMCvglqyJWJpFWaN.9lrWKIKgN85vjSNIc5zAcRs4x6DeswVoBfQPiB0Gt.ENRjHQhDIRjHQ9PSPmvxKuLS1YRtpsuItl8sWNzgOLOyKcX9Be9amI0ZDIfQpqxYXkJ.6PswW+WLR4JheqE0YInB30ZPCIFCu4IeS5LQFsxljoFqG4oJ5rqcvW3duG98tbgDLjqRPw.BRA373FVGKoYVU87yppcfVsVio45J.c0agVznDOhXPgCg5LRdE++A.bnj.ZDT3QGnIWeAPWO+uBnUg5Wkqi7EFJZjNSvxJgG3m8h7W9M+annX.esu5Wge269VYL0aShL.WPnpxCFAk1hVoQntJylPyHSpXk41Vq.Qt32.cu3+YvG.qM.uOaKbesCy+ERHq4C+AQPapMzpti0iwmbBFahwo23iQmd8v4bTUUgy4dGs68GD5n33HQhDIRjHQh7qHkXQoL3FbFlPA2zG6ZY5MsINxqdbd5e4wwaT00fQpqfnRzMCtpvE.M.cSkMCMUaEbl5ptJMyqrMIEiMASVFE.G3kdNl+jGmsu4I3ltrtzRpHSUguZIJGtD4oIzpcFhVAFEdb3CdpBdp7NFVVvBKuDmdgE4sO87nCIfXPjZsH0FVqiZ6rxgBGZbXZN0AFAzAC5fABo.Zzq7ZJ0UPV.ZRbYS6dzWD9oG3M3a9s+tbje4Kws8ItE9C9xeN1UpBU0PbCFfqp.sQHIwPZhlDihbstNqiwgncDztUhiUc.R13e66blOxWA301ZvqsBvW33Bz0LpRvqMueEpsg9rrLZ2sMcGaL5zqK444XRL3BUDvcVO2ZNaSR8yMstd+9tJ1czY0IRjHQhDIRjHQ9PxPmPuwl.+fSgjlxUc46gq3JuJ9EOyKwi7XONepq7q.PSaFaX80bai+XuSbZB55Y90qAOdTzHzTT3cZRZOAdZygW3L7TO2ShJLjqdqSyVUfe4BRa0AWUffnwj2lpfmA993sJ7nPoAiJTOcthBSHASPffhPnoHbJ2ZtL.JOJjFK7QgHplSdPsjsPyL+poOAkFACiZ2S0n1MWqnRY3m9ruL+0+fGlW+0dCt4a+N3qcu2MWV2LX3agffCOB15QoToAmqdlnQgHdDE3zMcjpnpE+5zXBJJRt3tJZWRTA3Qle06nBv5Kbe5O5iUhHXsV5zqGSLwDL93iSqVsHfvvx0GD0qs0tG878C7AHRjHQhDIRjHQ9UfhJO4oYXDO8WZdloSB6XG6jpxRdgW3kZRqDPg7NKByE.yPpQznCMhfABZowro.PSQQIXRwAbhScJN4oda1zrSw9181oUiAQkfEUPiy4oLDnnxQeWEUhPEApBdF5KovUgyWVeb4ZMJsFgTDRZtTuRjkBxJy7qtoEx0qTk2Q+NV.asKQC.p0ktKBBK1e.O9O6myO4QdTlX143e1+7+PttqXmLboSPRwhXLl5bdV2DqptJJGVPU+g3FVadYJDDcSNBqG85VcqQewNejuBvqUv6n1BdjP3J.qs9kfydVgGYlTePsRrVFYCZMsZ86xcWoTHTKH04p2ui9fmek+pPn99zr+Dc8u2dul8h0ZIIyRRhFuY.d.uQPTALJ65qh6nu6nV84FT6XaqeQ899zJRjHQhDIRjHQdWY7tsY9ENIF+xL0jSPgpjacOSyKt2cxy8bOGemG8mxW4Scajl54LG+0YrNozpUONUkGa93jNr+F55eXBnBZrNMVpONYQWG3shBLosPIJ5Sed0C+Rzewiw12xtYpwlBiSvjlwfxBzFnkwBECHEXpzLnxwp0XLcMOpBDpq3qR4WIGXThCBBZIPPqPDCdkBQYQzil4VEPEpFyyxGZgQpncwxn0dvZYXdGNIsYAA927W7Wwi8.OH66J1J+o+W7E4l2bBSF5Sd2tr3YpHIzBEf0C3AvfIIAR.2ZVwFWcTMMhBKf8h+pncgaIP+sHuaNm74KCxpNVhBqz10FiAq0th.3ppBTZAahkfHTTTfW7L1Xiy1241W49p0ZTZYcyzbjHQhDIRjHQh7aaDITeLpoYLrXHUgR1xV1L6Zm6.j.G4vGkStzYPvRVm1jjjfMQiVoXP+MVwuvn3FpIihWi3WkTukosXEnEVtp8rKtm65d3dtyOC6Yqyddq.1qDApi78my5x0WTMA0ZZeSKAZmkhnMblk5yxA.5vab5SyO7gebdpe5iyV21V4yeWeNtxKe2jnLTVLDm2Qdd94mm.WDyk7BfGIjbsBJGIx7CmH3lu4L5ZBqqMDBAPDEhnQoViX1QOFFv4qnnbHdwQVqLFexIYSyNMyt4YHMMEapAss1E4pC75.hNz7k0HQhDIRjHQhD42dHR.s0PVVKFV3obvP10Lyv0e0WEc60iW7EN.G90NNkBXa0Bm2i3cjkXPeAPQbD0HCvJfWGPTglpx5wHdRBALkUjId16bakem6314d932LacrVXqV57vie8kgQBvU09AjWopk5pVuDsldXkQhzyDGVIPYPwPcFUsFmSivS7Lu.eiuw2fhAC3Sda2Beg67SyV60AKdBtJppppMpqKwIJ.98H6bOeGSRmsaT68dbNG1TKU9JJpJHMOksrsswt1yNYxom.zB1TClDcsnYiFTg201rNRjHQhDIRjHQ9sBp.gfCcZNVSBdWIo.6c2am8dE6hW+0dcNzQONKf.11rzv9rb+EIyZocV1F8peUQmzHFVEHzTEKs.tgkXCZrNgwroryMsIlpUBTsHlvfyCO9MEyR0jstnndpaqE+thxDYT19N5dTuknBr3RKQkRQuMsCVjTdjm444G8H+Xd8idDtmO+cwcc62FaN2R0v9nCNxSRopphxpxy80+E4DE.eNlSvZQutsQgo8nMswfpwrsBg.tF6PezlSbXyrL1jiwryMKyN2lXrI5gMs1kmUFpE9pUDT9lyPT8WXh4XTjHQhDIRjHQ9sMJpnxUAXoUmwHAHUAS2Mia4FuA5ubAuzgdUN1oWp1rlLZFNXYBN2EFFPjnQIqJBFpax35LAVSoCxxZShNmLSJikkiT1mkm+sHy3de20end3aNVdQQiaTqIn0MheGoinlUD9JAzgPiaOGXXwPT4iQIvS9KeU9O9M+1bjCeDtkO8mle2emuHW4lmhPUACNyaSpRSl0PHDPNOVfuKVIJ.92vU.1XLq3JydYUWnVaqmEXQKLwzSvN2yNYKaeVRaYnnZHU9RrII0ewTjZwuhP.+5DpGIRjHQhDIRjH+1DcSEfcAAiNmLaBCGdFZINtgqYeL0l1LG8UOFu3AeEJAZ0qChJfqpfx9mGpf5455OnQI0arF2TNnz3zFBoYLPLbhScFN0oWhDcFoosvGbXxLue65OjDXswAUsG1pWiyN2LVkDZFuxlb9UUeuJEAUqVL.C+x2dY9d22CyAd1Wf419V4O9O5OjsO0XjAnJGPdhkTidk3QMNCvQAvmGlA3ytpuqGUyY3wKNB3QY.apg7VYzcrNroMOCyL6zL9jiQVqT7hihpg3BdTFMB95vzVp2BglbBSKwJ.GIRjHQhDIRjeqihZSvZXkmgtJRLZTUEXbCXGaZJtkOwswYN8x7SdxmgS5EDSBZst9vWu.HGfUAayVcbHoBVPrDHkJUJtVc4EO9I3a8idX9AO1SvoJqvlzgPR54kjTQirNOC5r0STK9Ezgyxegj55AunVC8FmCehSxe829uim9odJth8c07Uu2u.W+11BYLDws.IFgdc5ffhRe86YwBnEE.Cb9yEnOa2bSXz42QZB4HAkVgMKg714zpaa19N2F8FuKdwwvhBDDLIVPonppBuDdGQyz4xZLRjHQhDIRjHQNWPDGIIFppbLXvPLJEIJASnhNFE25McS3cdNvu7Pbz23Trjyi2XHMMkV4s1nW9qXgsZYj.yZgmdklJSJCUFd9W8X7suueDe26+g4HG+DDDCBFFVc9o.TJkT2NyipB8nnLUTMUmds06p99HJMhxxBIoTfgCbzWkG79e.FL+B7U+72M26scynb8ok3P4FhVIHJECpJIffxnY3vgmWV+WLyk7BfWaqDe1FTky4vZsqOJhVy8UDgpPEUgJbhifxCZEJsFiUgIQinD7AO9fGsUSmdcXSadSr8ctc10d10Jy2qLZX70BJKqtojls280YjHQhDIRjHQh7aSLJPBARRRHwlhy4PoDxrBA+P16t1A6a+6mW6HGkG3gdXz1Vj1oGu9wOA4Ya7sfqnBTm3sAz.llVLV.bZEADdje9OiSu7RruO1MPuI1Tcqbm2Cwcdn.ThrxvGqXT6N27+DAeQI3CjmmSm1cwjjxxCqneY.SVaVP0huw8+P7W7e3+.Vil+z+q9i3Kc62Jc8kzwUPRUeRT0khyQ.k0.FKhHX0WxK+6Bi4P+BcdupPb8L41HNsQ3KJEnE7hfPf9CFPRVBsGqE850iti0gNc5PV6DvFVcx6G8X8A9cJMzLO.QhDIRjHQhDIxFMAVoFkXBNltaF6bqalmMKmW6Xmfiu3BbY85QZ6EYvvM9Y.VztUEfJvp8woFQAmXvx7lm7Dj1tE69x1CSOVJhSHTFvnMbtZCV0QAkB+Ycb+pfBEBooofDXvfB7LDuxPZmtH5DF307HO2ywO7gdTbdGetO6mgac+WEiIdRK6ixM.iUAJMUZZbW5QU8VCDKfV7T.7AvGzLBmjZpC1aCqLqutfiplsz1IzY7NLybyvbaeNlcqyRuI6gJQQYnbk7788NWeGMK.u6aQhDIRjHQhDIxuUQTqa1TEM3UZBJAs3YRshq+p1KSM8TbvW9f7LuzgnDEsGaLV9B.SvJnqV2Fp0FyPddwm+Y4DG6UY7dsXu6ZmzEEgACgJO1v4CSvZD5lVftoUPU0ZARRRPazzuXHmYo93TFLI8X9Jgm4fGku8256vQd4Wlq7JuR9m7Etat5cNCcjgzVpHwWgVp2OMcU8JTWT33HTFUP8AvGjKQayRPasDTBtPs325430PRlkssisxV11rL8llhtSzkjbKhNPguf9k8+0bU8da5VQhDIRjHQhDIxuQoYNU0RsnQuRSv.nTXTdxAt1KeGbE6d2b5SdJdpm8.bFQHMoK9KD7vFkGQ6QTRidv54wUKALAOm5sdc5jo4x19br4wZW2+kCFPhUi+raeyeMHnTqnC8cq6OCAGhVgNIEadNl7wXID9Yu3Ky2769C3HO2yxd22Uwu2W4Kx929LjDbP4Pzhf0lBJMRiac0jtvMUcFh5Ghs.8GHePFOUoqDuKPP7HBXSSnUmbZ0ImjLKyss4.MnMJpjJJp738dB3QaUuyVY9W0p5p13cRuHQhDIRjHQhboD0GupQpadXuVCJPIdTR.wOfsNVKt18cE7TO8SxK9xGkW5UNAysq4HuSGvWtwt7aZZ6fZkgKDPil.IdOW1byw8b62F6YG6koRM3Vd.oVEosRornOmyRnTJDQUG8QJ.QPsF6ktvUgIwRZm1XT4L.3.G6D7fOwOm66g9GY+W294q+67k3ScMWEoJvObI7tBpbd51qKk9J7JUSkfcM5MFYhVbI+nTFE.+AvHAvmsv2QWue+An0ZrVK4sSoSutza7tzcrNj0JmjrD7AWsQY47Hp.JiFadswZ4GDEvFIRjHQhDIRjKlnNRiPBftVNofFiJT6rw8Wj7dYbsW0d4x26UxgO5qyS+hGjaYWywXoo3GrwJ.VsRl6pIfFsp10kMA.Sfa9p2OW111AikON8RL3KJHI2BYFJ7BImiO9g5EQcsjEPTJVYNjUA7NGfAi1x.A9kG604gehmjC9ZGi7Ill+3e+eWtoqd+zAvWrHsRUrbQf9CKIaxTpZxPXaHfRZl4WIfWaaddeos9in.3O.DQVwvqN67AVoTXSqsz8tc6RuwGid8ZSV6LrIInsZpbk3wiWpytWkVg1nQoUT+I928J9pW4w+CX8cAPWjDIRjHQhDIRjKcPK0w1iBGZABJa8gzFzjhGs3oZ4EXmaaSbsW20yAN3qvK9xGj29ieMzdhVmOhR2ys0evfWoPvBho1jnjZog3fwZmwjy1Cknnr+.rFEXEVvMjgIPx4nKXIJUsF.EMMnLfp1AnE.ShkfB7hmiepSwC9nOFO3i8yo8zywW8O5eEepqdejqAU0hnck3LAT4YXyZyht.JkEqDPIEjDpE.KXPjZWt9R8J.GaB7OD79kSvc61kIlXBldSyvLyLCiM4XjkkgK3Y4ACnxUhHBlDMooojjlhRonzUR+hMdS.HRjHQhDIRjHQ9UgQGZrtoRhhpwsgaT11J0fqX.imnXm6bmnLVd625zblkVfhxM51eFHTOCyHFpmRVCixhWMv7m7TTUNjkW3zr7Byih.UdOKVzGm47m78frdSpZzqe1zZQ4CpFxwNwI3Ie5mjCcfCvjyNGek67FHorBe+knsQiVGX94mGSZFc6LI8q7DZlAXsDPGp2Txk1U8csrgWAXeiD7y9iRiNyDqb4Yc6q96e1Z3Cq6xU+ouigss9+VkUanUFEZsl.dDQvKkDBAx6zBu2SkufppJB55LOqc61jmmyt1+tWyibfAz7kZKXrFfFmhys5JSglDRatx69GFW4V2nOEYQhDIxknb1i9xup7tcxSiDIRjOJfKo93ccRsKFm5qmoVkXAzTlXXoAuAS5R4Kruw4Et08ve6O3Q4e+O7J4+5+3uHexkdEBlLJSaSgWiDTzJMAqTR+kNCpVVBJMNx.IoIkQCnEGJbbtVCOehlvvkYltor7RyywWpho2794zX30di2hcM8rXRSnv4n0LsXnBpFTv1Z0C0vBVHordEHJzAElfFD6JyZ6pFb05ON+Qmv.qaZzoFd6kNIUtEYKyLFJpXv7KQZ6I4MWJAU2btuW3T7W7m+CXgSdF9S9S9Wy+7uvmjYU8QQAngAkd.CcGaJnxSn5LLkFvU+9SkNipy5kJsDiAoMbAvvGdwu+lf.Azp5d+GsfFMdwgpYp3me94Qa0XsZZ0oMo4IzoSG51sKsZ052fqrHQhDIRjHQhD4hSZ2tM.n0Z1w11BcGqKm5sdKN9xBdsEGBtpvJUS16qmaUSRBH0o0ihQhHWUEmVzmyI4SH3IDp2u1jVza7wn.CO0u7k4G+HON6aqyxm8ieyLQur5IE1XQmkgHBCFNDcRB0B9WuyJKMS33n+8ZGUQkrZBDoSMT4cjYMzJqMJ.WkfCCkkB851hu+SdH9V+m+GnX444Vt0aka75tZ50pEgpgb9LHltTjM7VfVAqjiXqMOwdWuuq4mok5s09yp2duxIWEqWJc80MIZTqKCeqZ173CABDvjnoSutL4zSvlmaSL6bahMs4YXpYl77xqAQhDIRjHQhDIxGIPzHhPud8v68XLF12UsO15Vlii+JGgm7m7xTkjgWkfKDH3pPIAB9J7gZ2OFpaQ4UO585JmpD9UOwTdWHwVuOJKb3jbRy2LKC7yd5eI+f6+Q3Ie1Wjxp.4IY3KKQ7UXM0Ou7PcLPEDTMwAU8hqV6aPGHz7uEkpYq410ADUfgREUggzMKgdlDJNSepFJX6rIJyFiCsji+1ev+.O0i+Pr8sNE+d26mia7J1IoBnbwtK5bkMdAvuKuGthXVdua84yWXRpq7akTPQ0PFVMDevixpwlZY141DadKyxl2xrroMOCiOwDj2NGzfKbNNA7QhDIRjHQhDIxEg7dcr4hBbNGYI43cBJAth8ra18t1EKc52lm5Idbl2anJIGk1fVInUBAeEk9RDciYyJ0UXUQ.VY67ilfDcBVaJCJBrboh.FN0hAdkicRVXogrosrclbpoQC3FN.eYAkCGfSBzpSWLBqQ76H2jd0p9NRvqffnDDUn91atOKWrHU9RRMZv6XX+RLI8HXx3MVth+7+i+UbnCeHthqbO7U+7eF9jW4NXbT3FzeMUcNxutbAg.32wFmUsZeeq76YWo20iHZDQu5WcTqeqvUTuUVfy6PYTzpSKlXpwXSaYZ1512BadKyxTyNE8FuGYsxPTBEUEzeX+yyuZDIRjHQhDIRjHWXypB+V+wrKMdmPH.BRcaFG7zFE68x1Mc5zhW6keQN3auHKIIjj2hDiAipBkpBu3pSNEQiNTG0R0l2jzjesu+G2+GV7AGFsEHm.4T.bzW+jb727TzYho3ZuwajDqlhA8Q68jHBdW8ZSkksxb+NRWxnI8sV7qpQvaiofoBMhaBfptBvFs.hmpxg3cBsFeJz44bzkC728POA+v+geHSuoY4+xe++o7k9D2Liq.e+Si3JAczffNWYCW.7Yy607.etv6WTAUFJwq7XxzzpWNSLy3L8lmlY25rL2V2Ls50h714XSsnrJBp5ylCp2+8ajHQhDIRjHQh7QYd2NN8QU.cX0PzZMH0dr7durcy9uxqfp9mge5KbPd6AEnzonTBJ7XMPPI3BdPZxkWYj1.oYq1roNWobvPzJCIIYjm0l.BG8HGk25MdC17lmksr0svPmmkWbIxxxHMMEiBTZKEUk0sgcyIAXUCupQ7ayP.qno50ixK407ZU27TzJgAECIjlftSWdqfvO5IdR9de+6iwldZtsa8l4tt0ajMknvszIQULfzzjy44eNxE.lf0606guWBeemuoud2U6cJJM79JTs6XcvZsj2NiVsZ03tyoXSSvXz3BADsfO3v4cDBg5LAVqvn0ef4zajHQhDIRjHQhbo.BBJ.q0xvgEjlZPTJJvwd11l4SbKWNlDTA..f.PRDEDU2.u3y8r7nO4yxUr2qhKarwPE.sJPRhAixRnQnqRpiaI0JGquYE+94bEkpQ7pGLZMJGb5SbLBEKv9upajo5lgun1roxxxv4pmk4rzVb5EOE8zYHipinrVCuZEE6qWKyHsHM2loxgUowmlPQZWdqJG+fG6WvO5QdDFNbI9c+ZeMtia7ZXSoJj9yiu+Rj1JiPKCCpBXh5ONmXCW.7uYX0LICV6IbYM+qle1jaZbxxxnc2NztcarVCgfPUUEkkCHMMEOTOX9decaY.jjXpaAAe7zvDIRjHQhDIRjKUY8tzr.XroTszRzpSNATrvxKxDclj8cY6jwZkxKenWg2dgknDPEBjYTXRrXviqZ087nwirtPxm+T8kkkg3UTVVhgJTpDX37LdthaX+6gwMJToFL91nzVVdwEHoUFJTT5bHosV4Y9Zm620tlg0TnuUV50uNMbvPLsaiJqG8AdtW6M3u+Ad.NxAODW4UsO9x24mjsjaPJFP0fAn0ZRRSnToPDOW.1DuWTg4e8+h+j+m2PWA9vnSuScd7NZSqQoUDjvJM8vnMTJT556yfhAHHnMJLVMZqFkVpm6.IPRZBAB0Uus45iOw3L6byxbaYNlZySRq1swlXQntRuAwiRAJqZkgWGU8ig1pwX0PSKNb93rPEIRjHQhDIRjHWrffBTxJo4Bp06fOECGBJHKMo4XxAiMmRuCD3we5CPRVF250tOlnSKFNbAxxxoJD.QgQpq1K5.JBDTBJznaZM5f4byHZKJKPqRHylhRa.smq5p1M28cc6rqsLCoZvDbXUJbdOljDDslReEoIY0UfswOgpE+VOmxffFAuqhPYI9RGZAxSxHKICixfDDpbdR6MNCvxidviv+9+S+kbfm4o4V+32J+28u5OhcLVFs7EnCk0ZaRSoBAeviVA53bXdNwFdEfUp0zY+q4L6L5eaLlQ2QDodX5cdOduGIDHuadys6wUUQHDHzTk1.fGgzLKSN4Tzsaa51sK444XSSPq0ToKdOVYu+sNcjHQhDIRjHQhboJ0EAxQc0HWsJvhRiMIoNhgbNPWGYPF7LddJW111DcmbJN9a7FbnicblY2aGuNg9CK.kFiZMYp6nGKnoJXAB5y8hOYsoTTTvxE8IIICamblnkgLG.kjDrnEUiqMqQzMEDiUk4uZ0eaL4pUZWaMY1DTljUpDr3EbAGdDpbAjVcoujvy+FuMOvi9D71m3jb46a+7otoqisOVJsbKgEOAM3UZDTnPiIDPKg3b.eNxFt.3yVjoOr9Y5048n0ZTFMZsFsto0i0JBg.9lfrdzr4p0ZRSFMCuFFexIHOOkN85RmNsvll..UUU38tlyXyGl01Y+ks26euHQhDIRjHQhD4RMF4BzooYTVTTmCvXPINTgBFukk8eY6jcsmKiidfmke5S+bbM6d6foEKMXY50NqoyPCMc8Y8waqC5UhPH47vwfqTFBhmfwfJ0RRpkbzzFgAC5iUp.wVK9cMs27nJuFT90r2Bq6eOZ1kyr4nQQYYUcaSqUHZSsf17t7JmYAtuG4Gyi8O9ioamN7Uum6g68iecLgxgMLDTBEZKdkAPi0KjFBX8PQx47KAWRyFt.XUSkcgUaO9UttBLVScVZIA7907ALiFiQyfxkwXLjjlPZdFsamSmNcHucKroFFe7wWM2svyvpgqTk3fOfI4c+Tn7AW82XqOGIRjHQhDIRjKc4caT.EntpuMhgsZEVEDJGPZdG15LSxG65tVd9G6g4m7jOE2yc9oYqc6.EUXUI3bCAilftQ7qnWIBT8pZyocsQh5uN3bNxa2FUZaFV53z8Wj1srzRAIZEZeS7opGY.uiZ2aookua5TzQwaDrRUfg.tROdUBnLT58TDBXyZgJwhRTbhfvC7D+B9wO5OlTig64NtCt2O4mf4RTL3zuBlbKdUsQSGTzHLodzKMwZvcNyFt.3vZa6YE0y26J1mFjjkRkygqrptMJ.zZMlDKVigIlZbRSSIOOuwHqxIMOijjDzVEdIrRaSOZdh0ZM1LKZkkR+fekVuq2cpihfiDIRjHQhDIxkZLpsmOaFkCvATFMFUfDilfVQUYIAkhTaJ230te9dSNIG7PGlm+fGgMcCWMsaMAVsfyOjfMPPGPIZB.FeSkX00Qkz4p.XeUIc5LAKKFdtW6nbzidP1wVlgqaOambUJq1N2zD+oAzTK3WGzDVmJTodFnkZegVipNEYDgJwiSInyywj0gJTrruO+sOzOi6+9eXV7LKvW5dtK95eg6hsjnvM3LnjZWvNnp0Vq0BZe8iu.30qnzNxulrgK.dDxYYBVit9xK2GQpC+ZShkrr53JJucKRsIL9Lspq.bRBlDKZsl.d7AGkkgZWaVopEMaM06ap+XiKT8AsrdeHJ9MRjHQhDIRjHWZS8QdqW2sT47XsVLJORPvnT3vSnn.s2yd1xLbsW+0wi7HO.+zm9Y4Ftx8w3crHkU02WkGQUmntlfdEmUVrABp065z+5tlCn4XKtL22i9S34d9mgO6m9Svkuycg0nwJ.J8ZLC25b7U0TM5PiAf8tKCUvZqGeSAEJcJ5r13QwIFbZNzQeE9a+t2GCNyB7wttqku7c9YXGcRwMXA5O+YXxI6QUvCBXEvJidrU30PPEE.etxFt.3vnvhVqab245Y8U0Lf6YsTXsVRyWaN8lSZVFIVKIs8q1xzhfWbqT02.droIqHlVT0s9rODv6qmc3zj2ilnuokNhyXdjHQhDIRjHQh7AQXkrwMDBjZMPvSUUEFIPl0PUPP7NZqfa6SbK7S94+DdtW7.7lmZA1R9DXKbzNMikTKWWe4Za+YMhMCM92y4l.XqwPo33UeqSwS9hGhW8XuI2dxXj1ZJFr77zQmPsAeUaxUixh351fV2XDUp2QNGEZ5lUcyHbVEBDzV.CmIzme1y9b7PO5iv7mZItwa5V4q84tMtrsNCZofPQeRakhWaIHFzdvFbXjZQ+dklRqg.JRCw9f9bgMbAvhHqLi.pQUp0X.pqD7N14NIIod9dyxxPaLHhfy4HDBTVVtNSvRLplYB1fwjRQn994B0ND8nVf1XUjnSP7uuKuHQhDIRjHQhDIxGRFUaRi1BgRbUUnLPhMEcHPnwzjux8tWZ2qGu4wOAmd94obSSfsnDUqbBAZJ46p6WEzzSvm6qQq0hCMKObHKNn.SVWlc66D.JbAZkpQiFQ4VcFek0mwu.MUCt1XtpKLaSqSif26oR.m3IjTwae5yvS+7OKO7C8PL6U+6wc7YuKt08OEpxJd6Sdb17zigIqEu07ySdVaTBXBfIDvn.wT2N3Nilzn92yIrVy6eMNEQdWimnUttUutaes2+OLWlukr5J7llRqVspau47bRSSwZs380JTEJXHqIxhLm8JUynYQWPnJ3qaeflaKQkr9eGe8VrBuQhDIxGMYzIW88hy9++rHQhDIxGNzxHEXZBzzhx.ZY0740WL.PgJoEN.W..CngMs7aQm1iwe3W6Of+c+e+M3a7P2Oa8p+Wx9lIkAKcZZKZb5DVNUSgELThJngPJZw13ByBhtjZygBThAUvth6MGTfWGVoBtJQgVpMRpggtnIge5QNBm3Mecty67NXeyMAgkNESpBj4JQGzj5AuQyfTGNS.UZ.wqXgpknW9jzVlD2PECTdDaEd07DJVfwzFDIGa6MiyX4oNim+xu8SvS8SeEl5xuC9O9e6ci0tLIyWRRRBsmZbJEHzuOsrVzhCQCE55oJdDoAOogX06NWwFDOq1XAuyKCgU+4JkdTZWuxOu+f9.qV810NGuJ8pUzMIIgzzTxxpqjaRRcN7JiGVWkeGsMZeMR.bjHQhDIRjHQhD4iFjkjv1layL9DSva+FmfW60NF6cmyhn0HgZS1RgtIJWzeHRnkQtw7psHshPSrJApQico.YVEu4R840OzgvWUvlmZB17DSPGFRxfgH90WoYszL0vMYhT21iCNEKWN.InPkXIIQgwji1por+PTI4TZrbrk87DO9iyK9z+bFqaKtq68tqmO5F8N5yC4Zbje0v1upf2OAvZcccUG0l6p0XVUfhV8xeOEvp0Z51s65DAmjjf0ZwZqMrpprp2Q0iWaKM+AcF7iDIRjHQhDIRjHW7fnqMFqKe2ywku6syO6o+I7rOyKvsN2zzIuK9A8q8vGIzzslqUjXXs6nFAumcG8zzm0H0BeapPccVyDvHNdyW4nL+a7ZLd2T1wryPaTDbkTVMjTSK7rZUt0BfXPGTnDMsk1rb0PFRI1rDxRbXBUj3bfwh21ExmliMrh66w+I7f+veHkKdJtiO2cxW+ttURV3MpSkFqcUC.9r5T1H+lCa2I5xpVY967xzTKfFkRPqsT6UUFpOYEZ5Ld1JBfWqH3QUvMMME3raQ5.NJQ7Btx02D6QAuQhDIRjHQhDIxGcQaM++yd2YAIWW2442+dN26M2pUTXuv9N.AW.I3NkDojH0hU2srj6d7zSG1imH7DN7S9kIbXGgse09AGd77f6IBGNhw8KcOyzsa0cKoVhRsjXSRIvMQBR.RrSPruCTUkUtdumywObuYVE.gHkTIhs72mHRdqpxkJSjUwJ+k+Om++oaVaV1P03gtuswu7cdM12d2Gm5g2IKeUqhrn1XCYEwdm2x.1.4YTLy8oE4E6OWd62hcyq5qw3A7X719ys3xIFpVxv8uw0wV23Z49V2pwRWZ2XFJGYHXyHDhyW3qEc+4H7PH+1v0AvGQT4.IkMX8coamVj0wgIoJgQmfqPfW88de9A+neLSckKxy9LOMeqm+KvJLFhKVIryUTQ4Vo30ro0N26dxM4XbhECQXrALDgMBrl39edT44twl+Sh8N1tc67O2Zl2kYtNrrMjccWm4eBfzzExnJRDQDQDQj6jXhrzZ15TdzZ7PaeSr5UsNN8INC6+HmhsO4pHwl2nmx2qwVBAHXr.YXMdLtREaKydKMZ+793hstI4ecSnH.LzuZx9PWV8JVB+9eou.wkKyVV4Jfr1DgiQGYbZ1tKlfGquHBtO+5Y7wXBAbNC13DhK4IPJttMwjlhMpJTaDpigW58ND+se+WjybhSvSs6cyexu2WmstrInYqqQ4n7FSz7C+1qXgJP7m8hGcQixmTEf89L5UAXiI55NBVRKZLUgPffO7wZBVyO36M6XrI95t786ly5GBDQDQDQj64DJ1Stt1sXCqbQ7nO7t3u66cFd2O3v73O7CvZqEkW2WedmVNPb+wfz0sDnonYQ+whKD.7EEZqWIh8DL4MrqKWeJpNzXrs0uNhIlxFGMZLKDBXwfunVf1hcEpMXm2srAuMhjDCPK5ztEQcRoRopDUYbZFUl2+BSwK9ydE9vCcP1xV1FeyW34YaKaBJaxX55WFFaI42V2vxdVUD9Vi3LeQ6PFtoGmeSnxXx3FEhK5RygOd3W.pUq10cddu65BKmXR9XWeewrsJDB8WB0hHhHhHhb2uTmiQGoF0qWmwqTkG6Aue9o+rWlCezSv6cjiyJdjMSbnCw3uwogDy+yLda+Q5RQ+oh7tBcQmelPdSvp+EJuJwoIkI1BVWJoYswm1EKAJEEQiNs.qkf0iue327lwUuuGc8cHJJgHGX63I1TEa0Q4BcCbzKcY9qeweLm3i9P1zl2D+weyuAO08sUh8YzNcJFazQ5m0Y9leQB09.9yVwA6MeoO2eIPWNhf2z+yC3tty2DAeRUPtcmNXLQ.9hifwFm+CklHBot9uaG8VK7vb+Pf5BzhHhHhHx8NxxxXnjQvlMCkLv5W4xYrgGkSc9ywwN0Yn6t2FkMw3Ii3fsX4O66OtSuoB8BCWL5iB94MtlL3L4yQ2fwR4JKGKP2NSia1YYj3XFezQIy3YplyRTRwXdctYrJXlq1ycbMoBCSBVb1ZjTYH5XKywtxk3Ueu8yq7O7SXQKdw7Ue1mguxi9.LtAl5hWfJUhXrQGkoajOEctwJ+diMDK4yF17tjVwxI3lbz6yHfiPvkG98FN+fiOwSQlXrDgkHLEabb7FvaH393q88dc.ZmyovuhHh7as4uxhtYmDQD41izLOcbMXQiNDYsqyxG1xW6EddLFC6YOuNm8pWChqQlKehzXsVZznQ93CxZIXm6+GtsnIUM+ENr0ZIMsCQQwjDmPi1sv4gjJ0nYpmu2auG9S+K+y4BWYJV9RVINObsoqSqlcINtL8Z5VA.uw2u.xdqGWjGabW5l1jtsSwXpQ2jxb1VAd08c.9Au3KxHiLD+W8G+Gw294+7DZOMMaeMpUqLgPDc53uoMOXfqakvJe1oXAs26en+s4Xu0D+usGEQDQDQDYPQRRBAuAuKCW2Nj.r1UrDV9JWI0qWmSd9KQarXJUh1oo3boTtbBQFK3BDHTTc17LI1hYEbNKAuGSvh0XxCTZrDUoBsCv4u1L7W7W82vq+N6iydgKQWeFAqgjRUvDUNekuVz8mg41ewdiOeYQa7rngGgzzNzwAwiTgy2Nv+va7trm27WxLWcZ9ZewuDOz5WEiYxHg1Dk3fxEyz3LkA51MqIju4wuccRDQDQDQjAGwQ48.Hm2SV21D4gsugUy8uysQmVMXu6+.LcpijRiPmLGY9TJWIIuBoYgquoWUD9MOaQQue166WU0rLO1nxDYqxLcy3vm7TbhO7TPRMJO7nXrkvVJgPbLAigrLe+JJGL9aXAWmuBXyxx.SBIitHlN.u59NBemevOfydxSyN15l4a+beA14JVFwgl37cvYgPbDVSLQNE.91M8LfHhHhHhH2x3cFLlDrII.d7cpyJpYXW2+VoRkJ71uy6you3T3BPTopDvQj0iOyQHXxy71ODbw3UsHopIjuMKs1XHXvGBPTBtPDWqQKNvwOI0FaobeO3tXcqdsXAxBd53RKlfvl7QlzMTA346hWYJpLx3jYf27vmk+le3OfSehiy8s8sx+0+y9mxFV7hnbVW51nItLWdmild6wWUAva2r85lY+1dRU.VDQDQDQjec4xbPvfMoDQkRHzsEIFXyqcRV+5WOm4TmgC9gml1.kpMDlHv46RZZJQDQvzqmDkyzae.66ERwlO9j7PHDiIpDNfKNcCN5INMlPDStzkS4jHZ2sNcSaSlqKAfRIk+X4TBEcCZSwsa4gWAtPYd2ycE92+292vgd+2ist8My29q8k3I13pv2XZ51nAVeDkKMDQTBeV.OcoCMuU+O2xMPU.VDQDQDQjacBVBFCodCwUJQTnKwzkkOdM10tdPhhqv68AGgO5ByPvFgMxiykANvXhuoUkMW9RgN1FkWo3fAiMFLQzB3RWqNm8RWgZkFlULwRoDFvkQkpwXhLzaAOaCE6q3q69bQH6.Tp1vbzq1fuyO3Gya+NuMSN4J3a+M9Z736bSzs8LD7sHDYnRkQnVxnD6iw5C3rYzVAfusyFvxB5j0ufNIhHhHhHxfiRwkw6f5MZRTbIRRrzY1onpAdfctcV452HG4vGm28CNHcRyvDYHx3J5XxwDLd7b84Hl+xVNJJp+jkwFGQvFQKmmqNyrL8LyxV13lXaqecLRRLQ3nR4xXifPvSj0vbS8ldxq9ajGhcV9nFd9t+jWgW+sdGVwpVMeyuw+I7zOz1XnPaZU+RPYfRkvXqQnaBllFpRLkFJFW4t2h9WY4WEUAXQDQDQDQtkoR4xD7dZzpU9mmjP2NMwBrxUtblbMqiqbwKxw+nSPqNswXn+XCxXh5e6za6XNeFxC.G74Uz0ZiHXf1cSoYmVjllwjKa4rngMD7Yzt4rDBY3bNLFCkKUp3Fu3Fq+dL11uyP+O9Z6kW5muGlc157jOyyvW9YeZFKoLYspyRVzXXhs3LA5l5n0rcw0JixQkIwDiM4WY4qkaQLS27JZm3JhHxcbl+bh+lQyxWQD4tSdqGBw3IlXumx9VXBNRswLaxX78emSv+1+c+4zsaG9e7+g+6341xJv13brrgqwzW57jMx5HJjQhuI1P.uIhfIFGwXvynUsL8UtHVSf3JUoQlmfMgVcS47m+7rsMsV.xaHUdCDJUr7lsDLdt3UOIStlkA33bMtJDOFCWZ0b0NAd228r7+1e5+6Tojku3m+I3a9U9Br5IFlZFO0RLj0sKY978gr2D2eYSaCdrAOlfmrXsJX+rzm1qeH9Vz8CQDQDQDQj9L2jOyDBL4xWFKe4KiO5COFm+bmitaY4TxlPy1sIJoDYFOA7DLV.OACEKHZOfmlMZwniun7Jw5CjlFHtbMFEKCO7HDnCl.DBQfIOfZdC9M+5O4jSRv2klcaS0jgHIYwLCd1yu784m7y1ClfiG7AdH9bO8SxpV9xw1sNc61jHeT9rJ9iwO2QU.3a6zRfVDQDQDQjawxqFJ3wioHLKXvyFV6XrysuELl.G3CNHWodSnTEZztK13RzKnq2.NiEuw1eB0.v0ldZ7AOYt.Wdp5LSyNTuQWltwrzMMqnWFECgXvGi2XwYAWjGeTFkiRnc8N3a.STZkDSUN3QOA+ne9Kyq9l+iriMuAdgm8o4Q1zZoFFh7NhLf2kgIz6wE8uelGROKetBaT0eucSAfEQDQDQD4VNCdr.Ar8CAawy3FCO7N2LSrnw3fG3.bzO5rPzvPRYHJh71AcdfWm01ODbOSLwDDkTgtdKwUGmZiuLbwkoiCpTcH.HXxCA6MwEAnCfICvQ8Vyh2EwniLIAJygN8k4e3k94bhy9QLwZVNeyu5WlG891BI.yV+pjXCLR0pDgAu2Wb6SwQOgdsTZSdPX41KE.VDQDQDQjaYBLW0dITrDlKBvZCYDCbeaZUr8srQt7Et.688OJyFBjTaTRArlTfL7EWOe+p.WLkYrFp2nIsCQDUYDtb8Nr+CcLN84tDwQU.xanU4WG6bUeN3vRfNsSo1HKFJUlicgV7c96+I7x+hWiwV7h3ex+7+XdpcceLdrg1ybMxZLCksPrwPv6H3CyaLM0qBvABVOdi+i20tja4T.XQDQDQDQtkyFxG2P8Fup.XwiuyTr7Zk3gevcR4JCw9NvQ4zWnIApPZHBSvkGd1TT4XrL+Xky1XVt7LyR2PDWpYF+8+zWk+e+y+K4k2yd3bW8R8ubASQydN3wFBD6Aq2vPitXBIU4Dy34Gtm2jW8sda7d3w20t32a2OJCE4Hj0jDqiEMxPDaLztUCbtLLV6b218aFSEAgMAla+.K2tn.vhHhHhHhbKkoHTnITDj0XIed65wjVmxzgGZmak0tt0vIO4Y3CN1IoIPmfEaHjmkrH0aXtaT.OiN93Tc3goToJzlH16AOLG53eDcAhqVI+5Z.HCioKQgLhcdhbQfuLlng3CmpIe287K3G8F+BLCUhm+q7k44e7mfkZL351fzNMX3ZkYzgqP61soapiRkqRvDkGl+FGOSAvFx6NzxsW5Y.QDQDQDQtMHzuCNmuLnyqJbjyg02lUOwPr5UNIyNccN4Yt.S01QpsZd.XxCAanXF8B8apVtfmlsaSGfqNaSt5LyRogFhUug0wHUFo+k0RF1PFQgLL.dhI0TgYBvGbxyvq7KeCN0o9P13VVGeqe+uNOvxVLst7TTJNf03HJJPpyQyVsfnHpTYTbyqgdMGKyE6RsA5a2T.XQDQtiTHD9DOIhHxcmtwsAa9RQ11u5nQjQZy5DEfu9W8KwpW+Z4E+A+.12AOJQUphgHvEJB+F.bjXiHJJhzzT7DfnHL.W3RWlyb1yx3Kcwr9MuIZQF0pTkl0mgNybUFsrkgqVlFMayrtHLUqv9O003O6+v+Q16u3UXWO9Cwexeze.KqVDcloNqrRU73HJwRqNsocVWpL5vXRRXlNMKZtV4gfMACFLEU+EnXVCKe15S60OnmADQDQDQD4VOCP+8+6bwRhLVRLFhBYr7EOFqesqh1MavANxwYV.myi2mGrLwFQrA7tT7NGwww.v3iONyjlwde22gYmYJV25VOSN4JA7bgKdAlX7QYEKdwzdl5L8TSQkQGktUGliU2w+t+C+ULyrs39ezcyy93OJabIixHgLRboDacE2KmqAZ0+DQTDK+lzqq5EvWwutcSOCHhHhHhHxsLlhFAkGy7lgu9htmrESTYhiKQrOkUNZIdjGXaXBY7Fu8d4TWwQvTFrQXCPTvSrAvkQv4INtDyNaShMIb1SeJ1+a+FfqKaYiqkQIl1cmAeliJIU.LTu9rjEUhPog4jMZv+wezKxO+UeIFcrg3a+UdA95OxtXU1DRZ2DSjCWo.dr3C8NEWbJpXa+Zl2iy.lh8qrG.ybM6K41G8LfHhHhHhH2xEJlAuWOKdSDIkJAttTC3A2wFY4KewbhO5Tr2O3nDOTMhKWkTuizzNDiiHKDBNLFCoooXLdHsKKY7gY2659Y6ab0TBvlkxxV1xnUmNbsqNCQUGixitLlh.uza9l72+8+dr5MtN9bOwiyWbWODqLoLTeZLslEh7z03lWkbsTDyc9OpxajWEgeyebleNdx2myxsWJ.rHhHhHhH2xj2rpHuRp8C.O2WKMDQvj.NGAWKV2xFkc8faGrI7luy6SaeD9nx3wQmtMAiixIFLFCNWfZUGlHLr5ksX9Ve8uB+W7G9Gv1V0JIhVLV4RTItL0azAW4goxhmjKFB7W8O7y3m7S+oj1tN+QeqeedgGeWrpZUfYmlzYmkxkJQR4RzraGLXKVny2rSyauIa78O4sf2ZvaUB3a2huceGPDQDQDQjAMyM5iv3xGQPEcE5fIgNYdrgLRaOCiLzD7XOxCw68gWhO53mgid5yx5W8JnR4RztSc7ttDmTIuSR68XsV51sEKZnx7428tvPLcRmktMmhxIQL0TWChhIY3Ind.9Quw6w2468CoSmV7bewmku3i9.rppCgMsCcZ0BabLUGYT5DYv2pMVaB4QcymESFrjOmeon5u97ZBG7E6OXJtj4UL1p933sUpBvhHhHhHhbKUu.g99wQJVtvXwlTiFM6PonXJYBThT19lWGaXaam1M6vO+sdatV85TJoFwkiIKqKNWVdW9EKAumNMaf0kQUimXSahR6PTZJgVsIqaFkGdTtLAdw8eT9d+zWlFsR4Qej2MWcg+...H.jDQAQEk+je+uAK0ZoT2YoaZSbkrvPUvYiw5BTk37wuTHTzgmsXHfMjOBmrgdy1XGXBXLEMMqhPv9Od2wRtESAfEQDQDQD4Vtar6OmyhMIglsRIJ1PkRwXBcYwiMJKaEShMNg8+AGfomsAdfRkKgOjgKqKtfGiwP4xkobRIhHP6l0owTSS0nHFezQI1XXzQFgjnRbjScA9d+3eBe3gONqdyaiu7y9bbeqXkDZUGeyYwGxHtTBYVKy1tEc6zkJIkJB.ClhNQcu85ad1VedesN3oWn9dKu6fwih+d6mVBzhHhbGIi4SdeRoYArHhb2IaHNuhol1.yqIQYAnK9rTlXwkIMoDc51AelikTpCe80zhSm7dbfS8H7VG1wVmrBASU7C6I1DQ54axPKZoLiIhCd4KwIuxEY26XGrsQMz5RWftKtKcJeMNVxp4TW0yey28Gwg2yqwN23p3e4W6ywiu00gq6rTsZUBDviKelBabDLPJNRAr9Df45l0Wu349p4k4teD+n7Gkel7uoxb9zd8CpBvhHhHhHhbaWuUGr0Zw6838N7dONmi.4y12cricPi104jm4TLUZfnnpzsYabYoTs5PjYMbpKbE96dweH+8+fe.m3jm.mGHIAhGBaow4pN3k+o+LN3Gre1zl2.uvW9KwpW0pHKqKAmGvmWU2vMFURMvp6En.vhHhHhHhbGidAfyRyvDffyiOjwRV7h4IehmfPri28PuGG5Dm.qoDgtVbM7TYnwnswv4Z0jWau+RNzA2O0aLCcBAZasLSHgq4pxO6EeU1yK8SoaiY4y+LOEekm+KxRlXQzocaJWpTwd6kO1obJD7c6T.XQDQDQDQtifI.Aen+1bINNeGa1MsKUqTkst0sxRVyR3DG+P75u66R6PDUiGgPVD13JzLD3DW5Rzw4XEqZUL4xWFQIF7kqw4l0ydd+SyO86+2hw0lm4odBdlmX2LpwfwkhkLJEG2eO8BLuY9qbuB8roHhHhHhH2x3M4m9Uw4x6bxFqgn3HRhhIqaJdmiQpMLO3ib+j1sEu0689b1YpSsJiShsBcBvzcayddy2Drk3gu+cw5mb4D.xJkvaejyxew28kXpyeFdxcuK9C+C95r5kLAcx5PHqK0JWhNsafIbyBIYy63yerkEsb2F8LnHhHhHhH2wv68XM14ZdTwQXLFboY38Nd7GbGrxIWIm5zmk8d3OhTSBIUFh1oM4pScUN5ANLUJOD6Xy6fgvvrd3cOxQ4e3k2C68UdCdhc+f7bO8iw1Wwxv36RyYlhpkrTsTBMmotBHcON87qHhHhHhH2wvZsDEEg24wklRr0R4RIXHPV2t7fqXBdrG79I044022A37S2Da0pzt8rbsKdVxZ0jEO5Dr9kuQr.u89N.emu+Ojid3iv510t4+7+S+CXqqY0X.rosYnRVrNGtrtjDGAg7FgUOFuM+jp.78DzyfhHhHhHhbGCq01uQXk1MEBPRRRdnXumUj.2+FWOwkJyQO0o47y1jL.WVWZb0Ki04YjxCyHCWlN.u6gOFu9a9FTJIl+a9u8eIO4N2I0hML6zWhJQFFoVEZ2pNca0jwWzn2te3KeFSyAXQDQto9zlideZVnyoWMmeEQjAK89qN9r78.brMBrQ3yb82WvIwITyD3A1xFXC6XG7gG5H71G7fry0tTFejg4Idn6m+4+w03K+UeNpXf+0++7i3u968myp1vj7O8ex+Y7HqcT51cVJa8TsRB3Rwk4obb9T5sUqVL+ZDFl2eJre0eMZV9tPrPe8EeZ9zd8CpBvhHhHhHhbGs4ONhh7sXYSLLqcsqAvxGbjixIu70vXSvFr7UegmiqNcf+5W7f7tG6fL4l1HOym+o4A2zpYIFCQgLhBAr3wFxO066Q9G.drWW3W4dGJ.rHhHhHhH2wv7obpU25LZkZ7P2+1YjwGgCcjiwwN04INZHFaQKmgiL7N6684u568WxEm5R7Xe9mhu9K7kY6KaILbVarAvfCSvik4B+1Kvq2j+wAigvMFWRU+8tdJ.rHhHhHhH20nQmlXvw8s90xV23Fod8FbfieJlI.DUhW6CNNuzK+S4ZW6LroctNd1m4wYCiLFkLcHc5KiAe9nNp+JkMTD9Mel+FvVD90zOHLfB+dOBsGfEQDQDQD4VlaboEa9MskODGgi1LY4Z7vacy7Auy930euCwJV0FYISLA+o+e+mwrsmlG3Q1Ieiu5mmcrzkPDozbpqRUqgqKFav1OXqu39l2.y+tzuw2+j6no.vhHhHhHhbWigFZXZ2tCKpxPri0rFlXoKmibrSx+9u+KwRWzD7QG+L7P6d67Md9u.O0V1JiSflW6RjDxn1hVDy1I8icalumeKp9K4U+ExWx0AB86PWJL7c+T.XQDQDQDQtik8FBclPLca2jgJEXyKcIrosucN04qyoOwk3hmeF19C837EelGjm491NKmLLMtFC4bXGtJsrzupu99UhNeWgFnXu+x0WAXLFL86rvdztH8ta5YOQDQDQDQtqgqqCWmTrocYwilvF2xlYh0sdJM7hvaJyS9LeAd3G3gXwwUHjME1FsX7Z0HobIt7LyjeiDt9XPASuPvye8YaQwkt2ip.rHhH2TeZyQuOs432m14q47qHhLX52zkQr+F9yIQcKwDSrRZFGv4ayitnFbTyA4HcOF2+12A+K9JqhXWFgtyhOTkzwpxz.gtvHkFK+C.le3VSwpb1FbeJ2aTf3EpE5e+egNGgMS27J5UfHhHxuwVn+AHE.VDQjeaDZ3IpTYxRBDUcHl02h25cdGtx0tBaYSagMt10RjyPTviMXYtksrm.Vh6G.VtazB8MXWAfEQD42JJ.rHhH2NTpqAuMhYcsItVUhSpxTcqSqNMYYirBRcyhsW.XeQWdNXwayCBGExtc+PPV.VnAf0RfVDQDQDQj6ZTtbY7VCsa2kNMaheDHwZHDkPVnIVGEU+EL3K5nUdLXHflkuC5zhXWDQDQDQj6ZzNsCVqgZUpPRbDjlQILLboxjDfHBXCPTHuCROWfmquEWIClT.XQDQDQDQtqQ2rtzIsKVKjDEiI3.mOuput.QdOQAvD74MbqPdUeMp5uBZIPKhHhHhHxcQRJGSpKESHeAM68oPvAl.oYApVJonSS6yasylhiJ.rfB.KhHhHhHxcQJUoLy1nAYoNhRhoTbBQQQXwf21qAW4+XGC8BAGzhfcPl5BzhHx8nTWZVDQj6EEL27t3ro298c9+4KSdD3fA7E4dibpFfKDKzWewmlOqe8G5YeQDQDQDQtqQ3FxeYlWdIOf0L2G26xeiWGYvkB.KhHhHhHxcMBE8w2aLSauPt9v0+48C+pE1jfB.KhHhHhHxcY5ko0bSB09qphupHvBn.vhHhHhHhbWjaVPVS35q3a3Ftr2rfxxfIE.VDQDQDQj6pzKP6MsBv2jKqI.1hyvqRAOPSAfEQDQDQD4tFlfsev2O0Psy+xEJpHrB.OPSCAKQDQDQDQtqwMF98F+3ekWOzRgVTEfEQj6X8Y8b1ag98WyIXQDQtc5Fq5auvsQ2j+7j2fp76ulVn+8+a2u9kOMpBvhHhHhHhHx.AE.VDQDQDQDQFHn.vhHhHhHhHx.AE.VDQDQDQDQFHn.vhHhHhHhHx.AE.VDQDQDQDQFHn.vhHhHhHhHx.AMGfEQj6Qo4zqHhHhbiVnyo2E5bB91MUAXQDQDQDQDYffB.KhHhHhHhHCDT.XQDQDQDQDYffB.KhHhHhHhHCDT.XQDQDQDQDYffB.KhHhHhHhHCDT.XQDQDQDQDYfflCvhHx8ntaeN8IhHhH+t2m1e+egNmfuSmp.rHhHhHhHhLPPAfEQDQDQDQjABJ.rHhHhHhHhLPPAfEQDQDQDQjABJ.rHhHhHhHhLPPAfEQDQDQDQjABJ.rHhHhHhHhLPPyAXQD42RKz4rqlSuhHhHxM518b38d8W+gp.rHhHhHhHhLPPAfEQDQDQDQjABJ.rHhHhHhHhLPPAfEQDQDQDQjABJ.rHhHhHhHhLPPAfEQDQDQDQjABJ.rHhHhHhHhLPPyAXQjAVKz4r2m0y4WMmfEQDQt2ym0u9.4Slp.rHhHhHhHhLPPAfEQDQDQDQjABJ.rHhHhHhHhLPPAfEQDQDQDQjABJ.rHhHhHhHhLPPAfEQDQDQDQjABJ.rHhHhHhHhLPPyAXQD42RKz4vmlyuhHhHxM5y5476f9quPU.VDQDQDQDQFHn.vhHhHhHhHx.AE.VDQDQDQDQFHn.vhHhHhHhHx.AE.VDQDQDQDQFHn.vhHhHhHhHx.AE.VDQDQDQDQFHn4.rHx.qA84fmHhHhbq2B80e7oMmf0qu4Slp.rHhHhHhHhLPPAfEQDQDQDQjABJ.rHhHhHhHhLPPAfEQDQDQDQjABJ.rHhHhHhHhLPPAfEQDQDQDQjABJ.rHhHhHhHhLPPyAXQDQDQDQj6Rn476Bip.rHhHhHhHhLPPAfEQDQDQDQjABJ.rHhHhHhHhLPPAfEQDQDQDQjABJ.rHhHhHhHhLPPAfEQDQDQDQjABJ.rHhHhHhHhLPPAfEQDQDQDQjABJ.rHhHhHhHhLPPAfEQDQDQDQjABJ.rHhHhHhHhLPPAfEQDQDQDQjABJ.rHhHhHhHhLPPAfEQDQDQDQjABJ.rHhHhHhHhLPPAfEQDQDQDQjABJ.rHhHhHhHhLPPAfEQDQDQDQjABJ.rHhHhHhHhLPPAfEQDQDQDQjABJ.rHhHhHhHhLPPAfEQDQDQDQjABJ.rHhHhHhHhLPPAfEQDQDQDQjABJ.rHhHhHhHhLPPAfEQDQDQDQjABJ.rHhHhHhHhLPPAfEQDQDQDQjABJ.rHhHhHhHhLPPAfEQDQDQDQjABJ.rHhHhHhHhLPPAfEQDQDQDQjABJ.rHhHhHhHhLPPAfEQDQDQDQjABJ.rHhHhHhHhLPPAfEQDQDQDQjABJ.rHhHhHhHhLPPAfEQDQDQDQjABJ.rHhHhHhHhLPPAfEQDQDQDQjABJ.rHhHhHhHhLPPAfEQDQDQDQjABJ.rHhHhHhHhLPPAfEQDQDQDQjABJ.rHhHhHhHhLPPAfEQDQDQDQjABJ.rHhHhHhHhLPH918c.Qj6bYLlEz0ODB+N5dhHhHhbuB85KjamTEfEQDQDQDQjABJ.rHhHhHhHhLPPAfEQDQDQDQjABJ.rHhHhHhHhLPPAfEQDQDQDQjABJ.rHhHhHhHhLPPAfEQDQDQDQjABK34.b2HOF.aH+jIXwF.uABl7iX.OYXAhBArdHxCDf1IkKtkxv.XBdhB.AKfEOV7VCY1.AiGHCKdhvCAO9vHEW+Pw+0m+ol7iVrE2Vf0kezDrXJ9Zgn1Kz+IPjaaVnyQuOq8oc+SywOQDQj67nWegburEb.XuIFaHO9Yf7vv.XCd7.FS9W+5K0rAOf0.lhe9LXrP9WEmwWbIxCQCVhviOzKTau.2VRKB916ab+ecn+O26K9dX6GJN.8+XQDQDQDQDYvvBN.ryTgPvmW4Uim7PrYXCdrg.VuGeu2kFigPvjGG1ZID.qetPt.3Mdnev3d2d4edb.LDHxavFL4UZ9FWD22z2PHed0iKpJMAadkoonRzhHhHhHhHx87VvAfs44JmWtydIJCXLA.C1P.vhOX.iEuIl.fy.wNKl.3s9hp.WbsM8B9FxiFG.aHOAqwaAhff85qrb+kNcdkj6WkWiGenWPaHXmep2E7+DHhHhHhHhH2EXAm9qjOeOzlmsr2RWtnZqysfnwik.w4mqwRf7SIgr9UkES.B4eciIeO9BdBg7yJegTGi21aO7ZIP29Ki5de25s+dyuM82vRdN.FC9h8FPzB8e.DQDQDQDQj6JrfC.m3y5+wdS9RRNXr8CslqnQTMuk5bfhp7Z84YR68EMl7qWuFWUwRg1Fr3M4MDqPHODLDQbnKghkLc.9X622hcLLysbpy+9YmWHXQDQDQDQD4de+NH.rCOfyZxC9VTsWio2Ri1fIDJ5vy40BFCXMY3Cf2jgEKAiknfEeHfIuHsEIZyqnav3wF.m0RlwSVTd8lq4xueXJVeyFe9xrFt9sCb.ffMuB0g7v2ZFPIhHhHhHhL3Xg2EnIpngKaI.EK0YSQwbMD4m293EOd7EiIIOVCjFUTS3P9dDNO.a+ru8aFVlhPqFxvX7XHBrNLYyuRuFB1PwRmddMB5hye9KU59KSZTWvRDQDQDQDYPvBN.bpoFtfmttT.K1XCQQQf2iIDvk0kRIwDEYH3BzNsCYAO1jXRJWhVlZzEOkHlHhIsSJtrLrVKwQdRhM36zlPZJVafxQkHw5niuEc53.WIrVKl3HLlHBVCdefLmGOg76KXlK7aw9Bt+xy9N6wblHeh9zlyceVOm7VnyIPMG+DQDQtyyB80Wb691Wu9B4SxBuKPWtBdeFVG37Y3Rc3yxHIxhk.0FpLcZ1h50aQJPToxDUtJNqkNovYsFN9gONsloNStjky5Wy5Y7gp.jWa1FttXLArVOQgLHySDPkjHJWxR0rp3bN5jlRVnCdrXKESoxkHJJlNcaSHX62An6uGiQU.VDQDQDQDYPhY5lWYA8VfjEJiMBrVffi.oXBdhr4UBtS6tj5MXSJS4xiQ2.jBb1Yay4N+k3uauGf885uE0uzEX0StZ1911FaeaahMto0y3iWikj.kwPYfHuC5TGeVaBjA3oFky2vvQFH1BFCNSf.A7XHy6neX2hk8b9xeNOQrM3VHO7E4NZ2oWA3OM5cnUDQD4NO2oWA3E52e4daK3.vMa0gRkJQ4RwXsfOqCodGgPffMlNoArkqQToQnKvYmoKuy9OH64MdGN7QNJcGd4b0SbBLl.iM9nzo9T3csYEqYkrxUsRdtm8ywJGewrgktXV1PVpZ.myQ61yPlKkg7FvZwXBXrA7FOA7DHPnXoVSvTTAXSQH3hBeGfnfp.rbuKE.VDQDQ9cME.Vta1BN.rw0BiwfwXvGL37dxvPHpDNaBkKOJyRfic1qv6r+Cwa+dGfi8gmfolpNooNpsxsg0.6Z2OD6Z6aly8QGh8s22jqM8knc21LyUlhUL4p49258wCticvVW+ZXUqbIrngyql6vFHyGHMsEYcaCgThrPoDCIQFbt7J75K5I0ghYIb9CZCwdUAX4dWJ.rHhHh76ZJ.rb2rEb.3JIo3xBj4.uIBSRUBwCgCCc.duieVN5IOCu4u7cX+u+AnwryxpV2F4Idxml6+Ad.9e8eyeFUpVgu4W+E3O7KsIFB3hSmxGd5SvQN9w4ke0eAy1HkFS0frtdFe7gYCaY8rsGXarp0LIOvpWCUSRX3pVFBCw.QFOPq7kJsKCB48T5.QDJF4R4cpZy0MGiE4dMJ.rHhHh76ZJ.rb2rEbSvpaHeu15Sp.QUosOgKOUJG+BWgyc0Y3u767cndiFzocSFcrQY2O5Cwi8v6hG591NqXzxTNtKSTKgUUywxvPMCrzwiYqiuIdfUsH9i+8dAN7IuLuwae.16689bxydJ9k6+84sOz9HpTDO0i8zr3EMNqaxUvFlbErpkLBilXIw4v5LjXhxWlyAKdik7w0T9w74ErB.KhHhHhHhLHXAG.NKySvDgwFSpOhKb0l75u2g3G8p6g8s+CRoQFhEs3kvi7zOIO+W7KvCN4vTF3rW7h7Sd82kFW4brtkrEV+xGipF3Zm9iHqccV55VMqdQCyzAOaasKgsr1OOeyu4mmSUOvdO3g4Me2Wmyd7ixK9c+6oTkxrnEMJqcxUv8u40vCroMv1WyjrhIFEbo4CD3fEKVbFfdAf6OKfEQDQDQDQj60YlZ5FAiw.w97Y0aDfwQpOEmKEeVJwwwLT4ZjPDMa0fotx0nToJrzIVLGOZH5.bjS64U9E6g875+Ltz49HpTMlIFaT9u7O5ayVW1RX2aZRhS6vTsaP0QWC66hM4e8+1+L16a9i3+4+m9egm+IdX5N8rLRjkZkLztSShSrjYb4MvpaHrZuwZze9KuON8YOOu+QNJezIOKs6jQswWBKYMqmIVxR3YetmiUt3QYCK1xRLPELX65HpaGhCNx3xXsVhhRvDYIXhvGBz06HDff2jOShsI4yaXffySVVWv4II4FCQattC9hv18dL3M8t+e8igIS.L85L09.fs3qke98VnFyMNmTy6RV39rdIF8oQKAIQDQj687Y8quv16kAW7sw0afubytuz6X.rEWfTq5APCxhqLZYZznNMp2.BNpVqL0pTlZQQDhhHo7XLS65b1KbY7ACiLx3Lwp2JALbsrN7puyQ3PG5X7Vu894bm6bTajR7jOySyCuqGfMulIYwkRXxgpPYSBs8MIXiIM.WZ5o4JSOMKYoKkZUKSDE+xhwmO2dCtes9kmu0y+rzND3BWaV9vScVNvG9Q79G5n7Qe3w3Ce62h28k9IrhIWI6b6aiGXqafst5UxpV7hXoiVijHnailD7A7c8XHCq0SRjkxQIDEGgKMkPHPVZWx7.VCVSL13j7NLsuAyMSg6EWs2bGddieoPuKgAB8lEwf2V7KlFJtLABFvD7DLW+uHGLPnWv2hiVuphsHhHhHxfI+uFYsMgO4OWFrDOieJnTfpwkHIDnTvRbZJgVAbt.Sm1fxiLNKe4Kk1AnMvwSC7dG3f7tu+6yq7y9wzpYGrQ0Xqadi73Owixi9vO.qd4KhgwP4r.CaMPVSZ2ICWRBMIiieoKvEm5xr6MuIV7niRBfOBnXDFgwiuH.XdKrJW+eFuWfxVWlQJUhwmXH13Daicu00xYdjGjSe9KwUlpAe2u22i10mh87J+i7F+hWkkrzEwV17lXqacqL4jSxir4UQhMuxvkMfMjhucSRmcV5zoMCOTUv.krQ3hiwYhIyDHECgfgx14tG5ABDS99L1Pd4zyGSwlPw67jO+RZB.FOoV+79kPa9iey7pzKEgdM4+aiu+C77iVeoem9CDhHhHhHxcz5sRK+TB+9q5rU.3AawsaVmgpTkZkqhk.o0aRiVcwXRvFUARpQb0ZzN.Wtafe9d+.d828838O3A4hm+br3gZyN2413werOGO5C+Hr5IFlxXH04nyzSyHUFl.YznScRMALUqx4mcVN1oOCsZ1fGX6OEKdrgH1.Y1.1P.e+QSju3zGuJm89A5ghbDxZiOMkRlHVZTYV5ZVAOz5VIdfu0W4I4fe344W7V+R1668dblycFN8YOCu7qsGhKUlm6Y9brjEsX13ZWMaZ0qjIGuJCUYLpTYLnaaZL6TXCAv5vDEvF6oTRIhJkfAKs5juMpCEMUqPnH7a+Ezbw82fGSwBht+RaddKEi4d7Vjs23mq5ulOdvWsDnEQDQDQje8L+WysUAfGnEu7jxj1NkFcaQZlCiIl3pCS4gFCiMFS.12Emk2b+6m23cdO1+gNBMZ1hIVwj7vOyWh+vuvZYxULIqZIqkZXIMzkVyNKUifIFtBct10HNIlLWFkFcX5xPbpKdXN9oNE1jD14l1HiWqLFfHbDaBPvkG.zzeUAOmfse0SAnTRYLdCgP.uGxbcvk1AGQ3wxRGdHFdSqjMutuAesW3Kvotv44fG63bfCcHN6YOK+U+k+sTZngYhkrLV6ZWMaeKagct4MxlW6jr7QKSsIVQdPUWKBoMgzlPVcBMcXLfuxxm28s7C1PdsfyGzRNLAOViGBALFO1481NE4pk+AEOd7F+bqF5hp9luifu9uGpAdIhHhHhH+FJ7q2xlVt2UbBIzMMeV9Vp13Tc3ESm.b4PfKMUKdkW+M4Wt22k8uu8iyYXkqd07TO0Syir6cyV21ZXikZSLwXwSWWS5VeFLNGUpTlpI.QPRoHbgpDUYDZDfi7gmfKd1yxRV6ZYCqZRpFYI3ZQDAhrAbNOgPup+BXf.dr8B+NOWaplDYrTJNl3nXhsVRhMfAbl.W7bm.akgXzwWDKZhQYCSLJO1N1BW3K9zbsolle9dOFW37WjieriyA129Y+6cu7iGeBlbUSxJV7h3YelmjkLZEVyRFikUawTp1XThtztwTztYcnbbwcQe+p7ZJpbskdgdC4OVJVZyNl6W7r8lBSEqya679kRSHuYYYBdLy08qJdC.BW+5jVDQDQDQjqy7a9U8JjjdIzC1ha0JPHZXpLbUxhqx4BdNxouHuyAOLG8jmj2ZOuFQQIL45VKO38sSdpGYWri0uNVbECw.M6LMYXHlXpFGwPiUifKC5lxzSeEpVoLlRwj4RwGrLkqKG8veHcloAO9i84YoiODQjgqcJQFGVaDNe.uIfKDx2.s2f7cJatIV7j3vSV2T5l1AeZWBNGDk+S3qYkKmTbzn00nSlCm0P0jxrwgJQ7PqjGcUql1D38+viyauuCwANxw3zm8R7Qm7HbjiFXeG8vr3Ilfsr90y12zZYyqYRV4hFiQqMIkpAlNyjWo2PfnPFfiHb.Y8qjqm7frdSLNiEGw4yjXigZt4Rz6KZbVghJ+B8Vwy1he4MuYZYB17JFGr3hzRgVDQDQDYvwbSUkhi+Fd8C2jsWoL3H9JlxTs1HjEY3zWsNu191O+727s3CNvgXpKdIV95WKO1i8X7k9bOC6XsSxRiMTy.Yy1fFSMEUWZY7NGAWWxxLXRLDasDUJFSjkrTOtfiYSSwV0wUmpNW37WjPVF6bSajZIknam13xZSbbDwVKYEIbC4ak1Owepdp50wZgHiEabDwIkyuBAGgfi5MtV9CTig3HHXLfqCjkhwzj5SUmgGaL9BaZi7jabiTm.G97Wl8ezOjSe9Kxq7RuBG+nWjCs+8xOLJgUthUw8ee6jG5AdPV2pVCaaokJBmlgMXHx.YghIMbv2uPsdik7E2bwLHFC3M3tt1vt+l9NRY5ubmm2urF5MBmU.XQDQDQDY99Xuj5OVdhdkTSGGzNZNj2GN1EtJ+iu7qxqsm8v4NyYn1Pivl1xVXCqcs7u3e12lDCTwCkxxnruEkbYD6yHJDX1RkA.CY4A8xayw466UOjXhwTpJWtsCSkQ469KdM927+4+WrkssS9W8u5+d1U4ovF.SHCKA7FOXB3rVbFSdEf84yeWSvxpde+A...B.IQTPT0eYL3M8ZXT8BBNO2PCjpeeipnZp8XCP4tw3Mfy5wYgzXCoVKoQQjZh3BSWmie5Kv699Gg889GlyctKSm1NhhqQRoZ7EepUyplbUbeaeKrk0rZpQdkwynIMm9ZLT4RDEx22uVLXwPz7ppsKJJeLK4bDHOLb9bINBq0RVVVw9dNO3roWEf6EJ118i+3WDQDQDQtG0upJ+1uPR9.VigzzTRhhYjZCQVZFc61kZUqRizTlqJa53f1w3+O9K9q47m4rbtSbRx5zgsuicxit6Ggm7g2EaZ0KkDODmkQoPJw9t4Aey2EqDLAxi0cCoqC8JeKjk4vSFQkqwrD3Lm6bDGEyZV4JX7JlhAY80WEy.l41b58CuZu90vegHJB.Fr8WBwL+ftWWyh55+9jOlkxueGE.i2iMKiR1.YdvYML9Xix5GaQ7X6bGbtoZvQO443CN7GwQO1o3RW7J7C+IuLQQwLzvCwJWwJXKaZC7P6bmrysuEV5XqpnVuAxRafuSKLtTJGAUKWlxIwbw5sHNwRRRBQwk.B3RynaZWxxxnb4x8CzaBVBFy7ZPV2r9isHhHhHhL3xZsXMFhLVL9.ocSoUylznQCZ1nA0pMFyqyxpiCXGi+Q++8cYzEMNaX8qmc+P2O69AtO195VIiigRFnS1zDEJ5jwjWkTGFBw4y3VmIu5r2Xvzdy7VWVFcxRIoTBW5ZSyAN3QoT0RbeaaSLVYv2N+5FLEwSM2vZdtn5m82Cr8FUP81mrgdIj882ir8hEleN1h4pqm.17iF520kqWIonaMmgMjQTvBAGI9.IdG0qeVJO7PLRowYoiWk6a7sxSeeqhycgqvUmdZ9IuyV4xW7hb5SbR9fO3XbvCdbds27CXcqeCrhksDdxc+Pr7EMLqZwCynICy++r2c1uwUVdB98u+Nmy8FajA2WDEWDI09ZtqrxLqkt6oqdoF2trQa3FvFyfAdLv.3W7C1O5W54O.+jALr87vfYdXrgwfY7.z6S2UkKUtpLSkoVoDo1Hknnjnn3Rrbu2y43Gt2HHUV8LMlVYUUpNOe.hLZFQvHt2HX0P+heawBXsOgM17wj0pEU5cj7g6UF3sN7dKhyhXsncdzNeQpq84A36U3UEeIC68yxffffffffffuE3q9O+8utVHToTnTJv4IIIgs1ZKVe80Q7fs0p+R43L3alLuw24s3vGbdN6KcFN9TCPEDhARZ+DdxNaP8pkQviGONQgUIEy23hRO1mGt4S+Gd6l8Us.cVquKuxprzRKw.CLHG6PySEjh.eU4AASmUATw9ORx6YVo34p6esK4SY4NAMq7Rw8qdpWao33pSIP6KlBy46U27WKqoE38EAvmO8oU9hYnkWnjRCMaQyV2GuQixDwnQwL99GB1+n7BG+jrYFbiacOt7BWmEVXIt8sVlO6i+DZtyN7S9K+IL4D6iSbz443GdVNv9GhQ6uN80+AnLfOMAmKiLaJ3yPIfQYnV4HTJgjj1fO+8DYu8H7WIC4AAAAAAAAAAAf26w68HhfRDbNGsa2ls2bKxxxP0J9W0GhA+Jj4O7ex+cTNFJEkudcrsaRKWSJE4omAqmG.F4AfkuUaymlwNQCHXr4671NS8XqOuSWwmeqFSDZQXGumqt3MYqmrMm4zu.SN1HnonDjEW2ffcrm92sHyuhS8yUpu4Ay5w6ivhJuuiKtu7ojbmGoCoHCv4YI1gGOph6WjG.hB7l7rqJFrdAq2f3UTtu9w4bjkkWRxtjDrsainxetGHpeFNJh4lee7lyMN2+0eAt4cWikV497fGuE+je5aysuyxbsquH+6LBiL5nb7Sdbd4W4kXtYmlIIBiNhRwUIV.s.1zcXmVMIKsA0pTJO.87yZbd2tYwl8DvePPPPPPPPPv2B72zZLJKKCsRgHBZ8t+aksVK1zLJqp9K3ivfuIyr+dDZ0NkVa1DgLhhzDEEgymPiFsPUTpycJm3tqjmhwzrBOB97Idr3wJfuS+3p.TZTJC28AOlqb0qR4pU43G+3LPoHncKbBEiFpc+iYemD.2c3Oke.jmATGNIu+iQDD2Oe.fN0WYxHK68mUnbtcyLrxC9cyHruaC1pvihlsx+B.DDzhAi.ZIOnZDG6r98gnRDUoF0KWiZCVioF3f7xGedZ4g250NE2Zk03hKr.WcgE49qcet6e983m71uGwQU3286+FL7fCvzSNAyN49XjdinhoFw8VkHaSxRagH46X3tu2zIS1c685ffffffffff.wCNmCbcx.bdbFFkFiRiSYIqU95DsyZEMb82tt1r1ZWGkRQ4xUoZsdwHw3vwFOoIat4NLxHi..JWQHvhCJl3yhGzt7YdrHNrE2uUb3PiWDRAbFM2dk6xhKcCFXnw33G43TQfzVI3pH69Wqc5e2NCpq87GxxW41f7rEWx0oroyueqxsm.D6DhXdPhhyfxUzexcdI80KNu5D8sp6ymWbDWJlrrLxxxHMKCu2iV.iwfVGwfiODVqkjjDZryVXso3EPqzTUz7VyMNm4.ivadlY49O9IbyUViKuvRb4qdUVY4U4e8e9Okp0pwniN.yL493HGXZNxrSxAFeHFLtJYZGZmEQrEu2u64E.5+19+2gffffffffff+tfu5vyUd5fFDQdpKkKUkuIrNdBW+qlqMCMTUDQi2Kzt0lrQKGhXnZkZLPeiPilMK5g1LDb6YXWk2XucxPqpn+TsECcpN+AXh0gWq3AquNq+fGvQdgYX+SLX9ZMxlwtqwHOHptY5jhm0+Cpnwd0dGNoypQB1auASQ+E686FzamiYonGgkh9Sd2dq0087AOXSSx+ViLFLFS2dJvYAapmFI6fV.sRnjIBu1i2kgy2BmE7INpHF1WuUYhdmgiO8L7C9NuBqrdCV6Aqy+p24SYi0ViaemavkN+myeYjvLStONwAmlYmXT9O+29GBJOJuu36Ev04cqfffffffffffuhNA5B48CLE+626bIII6WwGgA+pj7jF2uHVJETzGtTDzH.cpvXmJqnWcyilzoy+CmxMFFgDP0DDnspBBPIaS7BrotLOx2C+O8O8+Mt00uI+O++v+P9Qu4YnhaKTIaiWJ8K4S4uYwWtDqswi4J25tb9EtEe4RqvMtyC4IarEosxn2wFm8O6A3kdwSyKd7CwbiUg9TPIDz.k2bs7obmNeZ24zc9eraw68DoMjllh0ZAkftTDZslTmkrrLppKiyAtLEJz48JAdbzBmKEmxiSkO4uAM3iP4Ez1hxVOrGhCBBBBBBBB9kHqpStvT6Vonr2DZkk+uKNyiFMhCdv8dD2812ijVITREFBVeal4m+ld5rtJ9NSRYEd.kJOHXU2dusneZ673676TjqxnnZb6kVgs2bSFbjAYf96Ew6w6b7yUSyeKTV5NLTe8xqelSyIN4Kvc2nIKsxC3l28A7nmzf2+cdWV85Wk+cW5K3OqjvDiODm5XGgW3jmfCLyDLe8AIy6v4xvac3rY487fWx6jYwfHjO..TRw25UBoVKVmkLugrLGYodbVOZslHihXS9.LKyllmN8N6e3hd9FwwdRzePPPPPPPPPPPv230M.3u5d7siNeiJ4qhn7gDUdIEnKFDUNJhPp34wi38.JrJCBFN24+R17IqyK7RuLSN5vnrIXsVDju0GBrwkRjSgwXntQy9FtON1.0YyCNC6jBe+iLK2d06ykVXAt1MuA2512kat783O889PJUtL+9+1+.Fn+AX+iOFSL9nzmHX.7NOYMaAssDKFzFMh3wgCQKXhiPPQjyPj1QZjmLWdVicdGoE6fXkSWrlqTEsncBNwCZa9fCyElhdAAAAAAAAAAAOena.vtu5dk8qNDp5tecAW2gwjP2wwTwiuyvxBTXwvN34yuvkHKsMG+nGj8MbOnx1FmygWY9Ve.vZslrjVzb6sIKyAdMFSYFHtLCVqJy9hyQK+br12+0Y4GtNWa4U3BW8ZboqbEt+J2k+O9m8ufdpWmIlXBlet43nG7PbnCLCSMZuzWsJzpkGQmewYaSyFaSZZSx7VTJECE2KZilnXCQJAqOCapCu0RVlGipbded6y+H16AMd7zoj3CBBBBBBBBBBBd9f4uo8nUG44zsXfWsmrE2IXXeQ+Cq74SWKmnvJQr3ZawsW99zS8d4XGbZpqT3co3bdbhFk7s6hnMgHTQQnibXJxdtMyh2tEYM1AJWgHSI1ekpr+oFhWXpg3W6Tmj6c+GxCdzi4O8i9Hd3Cd.Keqk4Rewk3OqRIldlY4XG6nLyD6iyb7iPeUqxP0LTUUkxQUwjzhjjV3cYzd6lniznyDTZONsM+y5XEpxkIqM.BhqnjpIFuOAwa.IL.ABBBBBBBBBBBd9wd5AX2d1CuEqMH+S2kmttSRY0t2b2ctamfic3QgSoHUh4be44oYyLNwoNHSN1PXj1jk0FQhwiF3a2AQ4xx6MWsRin7nDGhVi3cDADEIjl1jj1sv6DL5Rr+xkXx4FEY9Q4kesCyZOJgqt3hboEtBWaokX06eO9i+itNosayQO9wY7gGlCM2A3nyMKGXh8yX0KSs3xH.Q8jfBONRHKqAYoof3vDUkHIl1RwPtRWrxj8f1a.7nbB1+iLntCBBBBBBBBBBB9lDiHd7dYOA+55VNyNTHhCuOeuI0cU8xdm1Z6I.3NkIKfShISAm6yuD5nRbzidPFrVDYsd.JWBZSEx16zy5aoJa0jkZIwlPFN7JASjFSrAi1PKuEzBZwhOMCW6sg1BZsgHslgiFl8MbIN9PGmemW63bmMaxEV3ZbgqdUt2p2my+oeNKs3h7Ye94YjgGhCL0zL+AliomZ+zee8yjiVipwwTSGQTbDZZhylhsMPrsn2e8.9cWuUNEZeIxq18PQPGDDDDDDDDDD77ASdPq4i54tA+luvYQ5FbaQYNKEKPXuZOS947Rd1Sd.y5hGaFFrJ312dYhJUhoFeLphPVisohBrBEAV+saFkGi1PkJwXk7sqblyRRRFs8swZSoboHJGavn03DvlzDuKAKBoa0FaTDRoJDWpBSWuBi+Jmhu6qbJR.9xKeCt90uAm6SOGKtvBb8qt.ePe0YjQGk580Geues2hQGbPla7QYe06kpQkwSBI1LnsKOw9hEA6t6fXQgEER3iufffffffffffmiXTEo90KR9.tx6w6s3vii70hC6Y.YsaFe67Dnoc6TDUL0pTgMVaUJUqejRF929G+Nr5ctCu9eue.u3IOJwjgVTTobLssdLQp7H99VLqzDuWfz7NrUvfFnBF7hALk.Kj01SFVPzPoZXEKo.U6r2ekV3RyPkZHRLTgHrX36dnY46b3Y4G+q+CX4GtFWZwE3Kt7E3ZKt.29JKvUu8RHNOyef43MdoWiCO0rL4vix3CUgdiKlR2dKd2VjUz6vhIhn3pXwf4a2UvdPPPPPPPPPPvyQLth.PEATZPKJvHHR9kjz7d.02ICwcK6UUws6QYzfWH0lgNJFc4p7jldVY0GQopkYjA5kZQf3aiHBdL37YnUtP.vp7LpCECPLmqXXi041yy1t24viAuRU7kPjmqciJAJF7XJeFJOH9Ne8EVvqQWNhA6Qyn01GyNyv7JuxwYsm7P1tYS9ou6E35W5xbgy+kbgO8hTsxfbfYliSdrSxjiOHGbxgX+CDyDCUhdqzCIFMVDr5H7Ngu0+AXPPPPPPPPPPvyMLVuFoHqu4yiJGh3yC.1CJSQctVz.vN5jwwhRc1KXzwjk4nc6DhJUFLQbmUdDWaoEou96g4lZbpaz3Z0lHULVL3bsnj1gK8WMm3eSgUL61y097dtV6.nSeW65tFh7n5V13d.MPZTQuWWrppzE2ohLDeJa7vMPGGQbspToVYhQwf06k4pWEGvadnyvMt8Jb8auJW8lqvEW3Nb6GuI24i9Tz3Xr9JyO3kOJ+luxQ3.i1KQRDZufWGQjNBR+V9GfAAAAAAAAAAAO2vDUsm7fecdv6P4b4WKdT3v4b.dx+uEaCXo3gKfypPEY.RHIKk3Z8RKObsaeGtycVlCdj43fSNN8Bj0NAzkICCNZx21m.z.HNyO2sYU4YwUQQeW6KJ6bO4ACipnzjUjVbehj2I1V7nU4Y.V6cz+.0vZyvltCs2Xa7FAmRPoUHBTyJ7pyLMmXloYyuqm+juXYd2O5K3N23Nr08Viku7myHizKuzIlkwRczIZcqyiNRsmYDdPPPPPPPPPPPv2rY1x5Q74YNTgffCw5P7dvYI1nQDGJedgOmOIfU6Y5MK.4YK1483MFdzlMYgktCMZrCGZ9oYlQ6mRRJNmCebURvgVKfOkmZSL8sPkRxO+cp7rp6Dvob3TfEGcF.YhWUL4scnbEqoJ.kKlNqvJmjUbc9iKy6o2R0HKqM91sQRsnQgBMdmtnGuKQilI7fFM3CWXQ9nu3hr182.q0hJxQkAqyvSOICruIHtpAU6l3SbPJHVAu9W9umEDDDDDDDDDDD72FlcPgQqnjRiQbnxL37sv0tMXsDEEsmceTmesN8gpBkjuFe7dOFigLugad2kYwacap1ScN9Amk9qXfzFDqzjfgDqkRkhv5ZhPO+p4L+aH5TJ497YPV904emBO8iqSfv.dkCUwWBgfqXnc6J9XxiiNYOFVe6M.GHhPbkdHtTEjhOCxRx31a1ly84eF+Uu26v4u7kIwDy9NzIX7o2Ga0aM1oBL0jiQ+8DC9L.GFsGAgHwSqeI89TPPPPPPPPPPPvyJy68oeFUqVkg6qWFt+AXvZUomJ0IppCi2RRycdpxb0IE8hJ.nPoLjkkfRf33XxvysW4tbuUWkAGcTle1onj2RZilTNpFsEEoYVpaJQq1O4a6qAXZVpM6FsqpXzh0oudyyNr3AjNYBN+ZmjW93c1eyH4CQKg7RXWvixKHRLp3RXJWAOwzvCqsUSt8cuK2esGx+6+QuOa9f6SZiGyn6aZdsy9c3EdoWkGtw17Qu+6wlspvD0hoJNxZuNZmkXSo7xlWBkvdPPPPPPPPPPvyOL+g+g+SYngFhCcv44jG8vbh4OHGXhwYz50oVYCd.Um9PcOk+rW5DplFapCI1gwTg1dGO7wOls2ZalXpoXnApijsMtj1nhqi0CVGHnw4x3a6qRVqwBTTdyEs5qpXPWIEeyCxS0nscePO0yiPVQwnaQ4182SiFPSpSXmrTtyZOlO9KtH+rO9S3pKbMZUd+TehY3Ue4eD+vu2avQFPPC7+8ezM3s+27uge7+U+tL8X8SI73zdDeJJQQZVFsSDTUq7Kj2WBBBBBBBBBBBB95l4G967Ol0V6gb0ktIu6O6mfJ5cY+ydPN8K+BbnCNKm3XSwv0EFx.UPP7sw1XCLosojFJUSHI4IHMRvTaHVXS3O4b2EquWd0y7FzmUQozdwV0vFY6fwrFCURP2pL8yvzvmOsisJGdwhW7fjgWJ5CY.AAkGTNCZqfxoAuBwqXmnp4wBJt70HDEWKthIibQIFWbB2Iv8NqdHq9o2qw+mpDUmdXtywpEk.PFJGn8zcJM2YkF4DUdVzEHJMt32WQmUKEEOVm.hVHylgykg2mVL7p7.Y4kc91Pr1PkZ8B5HHKkG9jMIsTDkFneZCrg2yBObKd2O3b7Q+rywp24dDY5gdG4j7+xO5rbpiNCyOUM1b6UnMSvMZ33BKs.UFdX9M+AeOp6bDkzDaaCMbJrUJS7.VvmPZnFnCBBBBBBBBBBdNg4+9+A+dr41Ib2UuOW+l2jquzMY46tJu+O4OlexeRSlbpIXp8OAm73yxINzAXxQFfApMHFDTBb2MVAIyQ8RUY6zVbkqbWRatEG7PSw280NNpL.aSDRAkf2WBqVShuLJIFGMnaVMoHIydCJ1cxG2ojfcHfRvJphogLHRxt+dRQuu142Ub6IvW1yv6ZuCwqmMptcda9+UI4+f7TAy9z+F6dbHcCT2u26Gny6I1TONuEmygRoQIfRUrJjbd5e3AXmsavsu6cv5Tz6.CSs8MBIdXS77Ne9U4Ku103be94Y0UtGUpVmy95uBuvK7hbfYliYauN80ecxZ2BqSvgl6rxsY0UtK8TuWFbfAnVkXLFC9zTTdAOfy4wlkBD80yajAAAAAAAAAAAA+BlYt5JndYN0Dyva9RSyxqcTt7B2fKrv0X4UVkKcwKwJ2457oe9GP+CL.SM8TbhidbN5QNHiuu8wj8sepfgXIkM8vBW477jUuFGcxyvDUSobxNTSr3JUAWbURvSy1BOIQS6DOC225cyFqxqw6Tn7phx.V0cxS6DAOdRUBdw2MfwXWqmJVVG7TA25KB30WDXZmdlsS9ZMOaI.FsyumWGW2HYkNk0bmWr8r+d6b7INATthM9amZfVwdyFb4Rw3bNb17Gkx6ym31N.qka8jaRkp8Rs8MBVSUrpHVw64BW6g7YW5J7m+m8umFM1F7Ybfolg230dQ9NuzKvr6eXpgfo0XTIV3Ia7PrkJgC3JW45r1cuKG+3GkQGpepE4PHEu.hVgHBVqkzzTh91dMrGDDDDDDDDDD7bCy1arHZignRkYfn5z2nivQFcL90eqWisSR3JKdaV7lKyW7kWfEW7l7Ie3mw4O+Unm5CPsd5g+fez2mJkKwAmeJd71sY4GtIQU6kYldJFrRIZs9p38o3rY3SKClXhLkwDoolP9p.Vbf2f2K.Fvqv6Uf2rmfYcEYz0WLsjy+YsKiNOHmTL9j6DjqW5NckAUdxW2afneMP6yXuAp+T76FcX2LP28F5bkhetXvKxJL.QZEodKdOXsV7hjmUcDbdMshJSu0FkLzrCd9zEVhO7bWfO67Whku0cn9.CxQO5w30e4Syqd5iv7iVmJ.Yo6PqFawfUGEuyRiTKk6sBa68bkqt.1j17cdsWk5wZhDK1TKVmCQYvKBdmCWd6KGDDDDDDDDDDD7bAy95e.ZkzllsaR6VsPEGiNpL8phnRrhwO1g4MN1g39e22jaux8YoasBW9pKxUu504lW9Z7+5MtCYNKyd7CQ+iset4Zso+IOF6+Pu.MApOzTDwNzHIizVsQZuIFQiItDFig1DQwFsEOJ.MdQ.YuQV4.whBKBNTdKN7nbt7ohbQIEq6TdtPdDmhpau2BTTZx6Mbym8n25LUlUzYOIuaOF6Evsmxgty8s2xx15KWL7p74WJljyhOCDOs1oMVqCmyAJCFSLpnXDQPiPYynbqLGe5W7YbtO67bwKdQdzCdD8zScN7gmje+e7+kbf8MFyMdezCBNmkF6rAFwS8pUw1dKbZE5RUQhpyMW9gbiadG5sdOblieHz3vl0Fa6lXcBdcm2yDTx2t2gyAAAAAAAAAAAOewXsJTDSr.NuCehEax1X8dbdENy1nKUgo6oWl4HSyqcjo4Au9Y4N2cUdvCdDu8GdNt6CdDarYCV49KPymjPsAGjO5yuDa+n6wa8RyQ8RPuU6ipQCPYIEe11r01Olmr4VTu+8Cr2RS1WjAWawk7aMO7RGJW9zn13Jx1qWkeMRdFjkN21tA2t2Dyt2Iprx6v80Ru.2ozk6bzlG7qTDVq7TOthGSmxf1GWjwWGB17yjhg3kx6osKCQavTtL5nJHRIR8BIIozNIi+nEVfO78ee9zO5CncqcXr8MFu0adVdkW7zbhCcPldvQnFPj.1rVzd6sfzLTkKQjJFueGrdMk6cPZB7kW5prwidLm7TmfQGrWjrF3RZBNGdw.hfy4P6UXLg.fCBBBBBBBBBBd9g49q2lXSDww0HtjfQApNWDEasylXatIssahGMQQk4.UqwAO1DnN1D7a7pGm6t4N7md9Kxe5ew6QqmzFQo3JW5Z7Iu6x7Ne3HL5HCvbyeHNxrywbizOCTqF59zXp2KpVcBJzCXKBH0+TQs55liTE3iAeQnwdEZoE3KJiXQ8zkZ7dB1U7EEpbwDiVJlLyN8yVVf8EwgaE5Vt04WjcmyUclBz6I5aew+2Jqj2uyRwt6k7.7c.NkfoRYPWFnLMvvi2tI29tOfktyJr1CdL+q+K9Y3yRYfA1Gm53ywa9pmlybz4X7J8PIIgc1YEbdvIFhLwDUqJVmhVVOarcJiTxyFsSPp1Oaj54yu3Uw6gW3Tmjdi.ocJhyhnzn0QjoTXcoHdGwJM1utpk7ffffffffffffeAyTu+gPqT3cNRSaRijFHdKwwJJEGQrHfQSEiBqG79T7s2DRz3.phlCOZct9jiSO8TFY+Qb7ScJJm0fqcos4Bm+KPGa3i9zKxHCOFGZ5o3TG+fbjCueFajAQqSAnH6mdTRF4g75JV0sth.DAeQlQwWTtzhBqjecm97cu8gambuJdeQ4HqPI+7Yg8YgqSYMKEY9cOyEZeQNf6DPuTD7cwuI.D4ZW7n6L4pUXKVURVQgUL3oBsQyi1tEWYw6vG8IeAevGdNt8h2folaNdkW+r7CdqWiiM2jLbEgZBXSeLarwiX396AaRJIIMHoUazQ4YRthtBkqoPk0hVIsQ.1X6LVYkUItbYN1gN.UQP6snUBZSDXhv48jY838dBM.bPPPPPPPPPPvySLNWq7IJLNDsmnJkHelD6nkyhHJP7E6ZW.eQPcE89ZyM2h9p1KKd0KwBe76vuyO92m+I+27cot.oI+l7y9rOgkV4d74W7FbqkVlqcwavO4cNGCMd+Tefd4u+u1awDiOFyM8DzuRHFH00jrFaixlPukiQ7N7VOh3QjT7pN8Hb9.hRIBJkBu2gyYw0YRLKFDQP5VtwfCU2jKKeMj8xc64W9J8sLnDKtTKhjuxnTdHykkOUmwh26oWsCkQiTpLflMZtCq83svTsWpWePb.29IM3C9jyyO4c9YbkKeMbdC6e144G96+F7+3evuFkLQTMRSEsCchkLeBJuPu0GhVVKnUHUJQjWgxChKEUVFJuPaZSsd6k1.e3m9kr7stEm8MNKGdtYo4NaRciffhTmEWZBNQvnDz9H7EkidPPPPPPPPPPPvyCLBYO0M3KxP4SEXGfqSo7Vj0uNg5Y5cPVOIk6d26S4xkYhApx.BTIKEOo76c1yxSvy89MZysV4gbsquBW8JWikt0U4t25lFVrxhB..f.PRDEDUb0u3xLvvCwAlYZN3Alj4mYRN3TSvrim26psRZfQkgV6.uEqMAWZduAKhP0Z8hGOYtLxRSIKK+9zpHLZOtNQCumrC6KN5Ex6C3mIcJ2ZY2q5LnsDui9q0CoIsoUqFX8dhhhvTNBmjOUm0wFd7i2fGu58PUoWFXj8yviODOAOWZsM3O6u7s4VKuJKtzMXmsavTyb.dwW9U40N6YY1YpQOabOhwPUoDwdMJO3st7LR6Drpnh4lsCcw4qx6Hxk+4YCwiNNlGk5Y46sJl3XFezgndrfwluxk7hp3hKe8N087tyn+JHHHHHHHHHHH3a9LFe679t0K6NkhEUQVT2cHGIcFoSO0dzEjZ03St3MYgabW5ej8wbyLUdoyZ2gJBrcicnunJLXe8vQ6aRdqCueV6rmj6s5J73MdD+K+yOGM2YGtxkuAW7BWkd6sWNvLSxAm6.r+wFhW5TGi9qTgAhhnr.Qzlz1ahsUCrYYri2gVqQqzDKJLZv4bH1DDmkHkFG61avdIevTkeNn.RdldCTJB.V7tcKoZI+mU3XmM2.iRQknXTp7xa1l4I0YIyYoYVJRs9nZ8QHkHVGM2+Is4s+fOg268+XtxEtLkJWg8Mwn7lm8r75u7KvINxLLPjfFHINkXiPYcBFz3yx+rKumjkhISs.nKJG67LQiOuenyDEQ5pbyasFKbsqSu80KGZ1CPM.m3K1GycduxgPVw6dlPnuAAAAAAAAAAAOWwH3Q4EbRmIsrB7fpXE9.61Ksc+Ie9cHdHAOW3pWmGr5C33m7HL+z6iHRHsUCzkKSOpXroVbseDnMTOpG5arZbrwOLfmy7luN2Y4s3RW9RboqbEV4dqvBW6Zbgu773rY7hu7Kx9GaXNxbSyb6eb1W+8y.0FfZ8MBJf0V+lXzBQFEFEnEMkLFLZEZkhFMau6vuBEV7HdAmR8z6k2+VpS.v9N6tWoXRUS9O2pQCpToGhhhH04YmVsIwYIpbYJWsGdrHTQpfGXg6tJuy6eN9ryeIVYk6yVOYKN9oOEGd94467JmlybjInODhEXmcVms2da5seMFMnDAwABY3DPUzKxcWASEGuNxCBOeMJARbYbdXwabaV4N2lYlYRNzbSQj.IYVbZcQ1ey+8UX2yH61.xy1WfPPPPPPPPPPPPvurX798lkWfhRb06Ac2R4UULEkAv2MZJwCY.25lWEeZCN5byv9GtNdeBIVGINMkiqiNKEmuAVaJsYKbIPVRB1rTN9HSxQmsNmcxWkG8FGmkW+wb0acGt3BKwJqde9o+6+KnbsdYfgGlIFaBla5CvQO3A4HyeHFerHFcfCjWJyEqwIaVCRa2FuqMJuiXSzdNccnQgu6xBlu15gUwmuAl166MJf9FX.bNEMRrzNySpDgtZchJmGz65NO27V2iy+4mmO9iNGW+pKfflSb5yvK+huHeuuyqxP8UlQhEJIPZ6mPqFOgpZXn9zrgtZ9.9RQwRG1WLQoSIBUQeOWLAswju6gECdEj4UHwvVVGKcqaQqlay7yNI6ezA.RAWFdcDNQgCKJ.k2hPdVl8ggfUPPPPPPPPPPvyQLtuxfaRJ5I18FWXdexp5lE08ZwG9Xt2ZqQeCLHG4fGhZDSVxSPzJrn3gOZCLdHV2DcIEwkhPzQnKqQxhXyUWDcTL8zScp2eMlr+5bxYmgu2q+xr9ls3iN6qx8t+i35KdKt8sWlqc0E4seuOhgFdT5uu94256+RL7fCvjSLFi2aDkM8PTTOjzdSRZ0DsRULMogc2Wu4KVouN1fOdn6jcV59Zj2fsNufRoIIygUYnb+8RYDZCb6G2h0d3C4O5Su.W3yOO2bgqPkJU3Lu3Kxq7BmlW9LmjiN0nnAhvgx1hrzV3SaPjVPqySDqxqyKNcmOevZUbToUp7gukKeJT62y4uGvpJ95LTk412YUt0suEU6oGN1QNH8pDZ2bGLhBmXJlr04Sfaw4954MtffffffffffffeIaOA.6JFdStt6r17.GE5DsnS5js3hxGVfO9xWkG8jVLw9lkomdV.GoosnbkJn0ZFaj53RxvklQhjvNYsv4snyDTdPWuWxRyn8NaCa2.SbD0JWl9pTApTgW725GPSfuXwayWrv0YgadaVd003AObEV9dKvEV3ZL7HCwAmaFN9gmkCN0nL4n8wP8TlZ0KSRRSzXQ5bdQFcBBr645yf812uthIksJ+sKbBznYSDcLDUhFYvi1tEW9F2jO6KtDW+F2jEtyCnT4XdgW9U4ruzo3sdkyvbCWmpBfuIOY8GhVCkJUh3xkgJCRJV1b6lzXyVLREKJkfnTjgBqOesP4DEZsFqOCwC587kW3jrtG6sck3523lb+6uFCOxHbv4mkHf1M2lZ0pSCqBmHc+cQxWOUN184KHHHHHHHHHHH34AlNAvkOseoavuzY+0hBPJFTVr6vjRAfhad66Qq1V5evQnu9L3whyaIN1fQz7jGuMZeFFIAJCRbDPLfkrTGUq1S9pKJMCWRFjlfscJNYa7JMINAeoJbl4mlSL+zzDO2MEtv0uN2712g26sWhs1dSd+O3b7yd22kgFnLm3PSwqbhCw7SMNGa1Y.uEMc5S27L.2Mt2ulB.N+sEWQIGmeCdfn3Xjnxrc6LV3V2h2+SuHezmeAtwsWls2tAyclWmu628M4236eZlrhPLBhMgVIaAs1lI5qJNuGqyR6lMnkGb5RDUqeFnVDlm7H7JAmN+0zgBTBhRChNeaDWzy1zoGkArEwt1NMi6s5Zr0SdBSM6bL5P0Q.roYTJtDMZk+2.699ku6TtNHHHHHHHHHHH34IlxYMw6ivQYrJEo5TzRJkyRP4Dr5RPoRzLqMUiKQ5FqS6TGlgGmEu+83yO2MnZkg3270NFSaDbsWmdiKS61snosMwULHth.y7P01sw4838wfuLYMaz8fQYzfQ28mEfx.PanUa.nGfQ.NyrCAyNDO7rGgkWcM9rEtAe9UWhqu7C48N+03sO+sPzQbrW4kYh8MNm7vyxomaTlLRnB.1lztwV3pTBsyQIaaJ4bXJxDddeupHtRY7YPVVFYYJ7XvRIblR3DgpUtGdqmrldRZBoYQ3i5ApWEWD7PO7YKsB+k+U+U7Ee3GSiMdDSL1D7q+q+83XyOO+Au4gKNa2.ZsmOYDApzKak18iJPfRcZP6VaA.NST9pqp3RDfxo.G38VRZ2h5CVmMZ7XJUNhXcDM2oA8PYzn4Cii4e4ew4nT4x7O5+h+dLP5VH1FL4XiviVeUJUtFJQg2metmn5s33wAz.cHV3fffffffffffmSX9p2fTryX2s1dy2Ws4I.NeU+TtmdoEJt8JqR6c1h8uuAYf9pmuxcbdDsfyZQqiY2cE6dSg7WeQM0eu0nd8CxAl+v7Ve2VbiUeLW4Fqv0t0J7v02fK9N+TthV38KaXzgFfiN+jb5icXNwgliIFZz7fgAbIay1s1FxRIRKXJaHtTLO9IaQbbLUJUlRUhv40zNyRizsHoH3zrDK3io1fCQUcDs8vM2nE2ZsGw+h+e++gGu4Fr81aw.80Cm8ENCu5YNMu3QNFSLROPq0elN++qKA1Ngt88rwXv68n7dDQviirrLbZAuGV9A4e1L3vCR850ye7Yd73wX949yiffffffffffffmaUzCvJvmW9tO8PQNuNYsooXh.mMAQzTtRcdXlkO6hWi1a+XN3ru.6e7Qxuee9Th168n0Z717.xzekwN0tCLpmMIa8XJUoGFsbcFYvg3nCNDu9AmkGs4NrYiD9I+r2m6s1CYwaeKtysuI23FKx68YeI6ap8yfCOL+nS+5LxfCvLiO.CNXO4kGr2SiFayS1tAlnxHZOJeaxxZPl2g26ItrhxJEa+39vDWBUMCMAtWKOe5ktLuyG8gbkKeE13QOjgGYDd4ScJd0SeJdwibTldvdnRQuBm92v42eS7hq6tYV4yeuF1sLrMwQ3rVvAQJCYYYX8dxLZxRr7Ye34QqfCLyzL7PCPjJiTqkLaFFiA6WSSI6ffffffffffffeUy36D8TQ+glOrnT46EXuf3k7.fMJxRcfVSFJt25av4u7RXjLN9gmk80WIrM2hHuCQxKiYkRg0VL4g8Bhn51KwcF5VOqCT3506mrrL1Y6GiKyiRLTOtLCMTOvvJNyT+mwldOKd2GxWdsE4RW+5b8adKV7pKxWbtufO9O48Yxo1Om9DGgSbzCxAlZ+LxPCPeU6kJzK4mIsoYqsncqF3SaiV6oZ0xDWtL0FrFMx7rz8awGdwOme1m9wb8kVhVsZRTrleue7OliLyL7hG4vLcukIV.alkcZtItLKUK+rMHo7xe8axoNIvWqTXSSQ6EhQSlKCkIFutDajrAe4G8AToRINz7yQcsFEVbYVRRRPD4m64KHHHHHHHHHHH34UlLU9thUm2ktcmjw30464UuGw4Q4bj4xPGUkTOb86betypOfwFseN7ziSYfV11nJBZRj7oQrWxKc5NEWcGJ.7O6A.a0U.kCkjhWkf2lQZ51jksCnz3EC0qTiWdxQ3z6eDdzadVt0JqwMtyc4gquN+ae22lUasN25c9K3+ue5eL80W+bjCeDdwSeFN3LSy7SMJUkX5s7vTsxvDQdci2p4lz7wM4y1pAWeok389jOfO6y+T1dmMX+yd.9s9M+g7Jm4zL6nixfkLzWjfjYYmsdLot1DWVQO8WEWK6yz4uuX5Sq9J2V96q42oKMAiWJ1AxBwk6gLh4VO7Qr5MtFG5HGj4ldJLj22vNWdYRq0l8DccmcB8yzgaPPPPPPPPPPPvuxX7hpaFYAxmvudxC9sXvUoTfVxKqYUTIVscat30tIsSDN0QO.6ez5XDPYsXhktk+r0ZIebHmOPoPrcaEX7t+Zyb4+oZ6sZfVAZshXSIzwQHdGNWdoJ+vGtFwkqPkZ0oTkdXewFFa1w3klYLRrddwe32iUVdYt3E9Rt5ktDqt7x7IezGvm8wmiXSDm8UeCFe3Q4HyOOGc9oX3dUzpomauz5rxx2g+Y+Y+UrwFOl1savXiMF+nW62luyq9Jb7Y2GCJ4q5IeyDx1oM3aSkXEUiqfS6ncVShH9Y572Ic9xDx+YkGrRmL15wZyWCRkDMh0gMyiNtLahmyesqiOqMyN0DL0DigBHKMAkRgunL1Q3Y9KoHHHHHHHHHHHH3aBLNuBk3vINT9N6E2hr2B3sNhLFvaQDMNJyxOXMt1R2Acod3kN17zejFqsAFrXzFxrVzQwjl4Q2sLZy6y38lBwuNB.d3x8Rlyh0lRVlkr7QwEhViHdlXxIIykQRRBM1bMbYVzHnTJJoT7c5cbROzr7qMyj7f25M3NqtJKb8avkux0Yk69.d2288PEWidFXXFepoYvQGAuOgGr5c4A26t730uFiM1D7hm5U46+ZuAu7gN.8J.Id15gOfg5uOr1Db5LLkqfIVSFV1r8Vr4NayHUF7Y57ua1d691Zdtf2aPqZQgQmWB6samQopQrdqc3i+xKR8dpv7yLAi1W47welMiRkJgon2v61i3c5WawQwNv5ou8ffffffffffffugaOi427.GUEo7qypi0gGiRChGOJb.O5IawCdviHpbMlchQnDYj1rAJb4YOLyhVqoU6Dzlh.xDn6v1B9ZaWxlzdGDIueiiMZDwj+ZoxCutwNsQvgRfxFMpn7iAs3AuklOdMDkl9JWhQFcXN7HCyadpSx8VeCV6Qaw68IeAW8Vqx0t4c3dezmPbkxTpVYvmQRyl7e6+f+q4nyeXN0zGhAPPklw1quNk8BizSI7tlf1i0aoQqmfskGsQHNNlwGbDrMe1JA5ck+9oS1yFbVbPwmIJQQVlm1IoXP3wMawh2ZYFn2AXhgGjdP.eBdWFkhiPDAaVH2uAAAAAAAAAAA+cG4A.KTLBg2M6dcGhRZMZig0e7FTc3w3I34se+2mMV+g7R+feKNxLSgMYGh0fVhIM0hRoIM0RbTDh+om9y6cXJkGD7y1PfxF0.7JTdAbBh2f36TV2JJgAuTzqwhCKYfVHUxW2SQkUXUBYRFMR2.DPgv96UwD8NBmb9eWdTCOsTve5e0mw+7+u9+jdLiv+3+Q+C43GcNlnucPCTJaSDKDk4obUCHNRjDRMNbJEYB.wD4AiSgtMHsrf9+vmaecnZ0pz3wOlJU6g1sSXnQ2GMAd2O3SYyMaxIO4gY9IGCAvl1DinnUqVTsROzNqIpn3etAf0S+IZPPPPPPPPPPPvyGL4sJZd1ecE8r5Syg0ZQWpJhpBqucaV6AqiVoYh8MFZeJZXO+dRw3zp3m76I3WxKq5uNGnvY5r7oVsWgxoP4x.Ln7BJuCGp76W4xe8Ul79aEWd47RdUYqTth0UrCENzVvKYXEEi1SLOnkmjsdDUh8bno2GGchAY7xB8jle9qsNzNP6cf.YJvpfLkGq1iCI+ckr7gNlxqPa8jo+5HTRGNQgx694BVMIoEJigzzTRAhUQrd6LV4dO.Tk4XyMM8WsDZr3rVT3w6gLuCQavK68yR1yGdOaewEAAAAAAAAAAAA+xlQUDvlS3oC9UJBlx4Hw4oT0dw4garxpr7JqPkJFN4glAMY6If1NYOVA7zAN42SfS9hma+WCw9knhQke3hBAkxixkgVxCJtSKG24Zssn2lk7jeGk4vVLHvThCsOOHVsyhUfltcnhIl1osYk6sDkqo4TmddlepAQ7P4l4mmcN8R04uWZUctNeBX28XT1sucs5m8bot2RI20ouskhxe1CsZ2hdqVic1ZaRIBsGV7Vqv0V3FTpR+7pm9jzWoRfqAhKCiFrJENmKePlUbDJP2uvf89pGDDDDDDDDDDD77Bkx6P45LfiJtxC9h9G0INRrNzQUnIvUW7l73G9PFa393Dy1GfaOy0phglTQYTK9hrqVDjlWJdM7e8k8PsMFUVLhyPd8DK3TPl1UbICmxgS6xeYK1athGTt7.0E+dt35jIYEZOjzNEGvct+83F24VTs2pL+AmkR.91a08brSfmVkJO.xhfbyeNAkc2ufAOPlI+36YkxW7Vpr6kNuHB4qynHSDIdKNilLfEV7Fb+69.FY7o3PyLIwhEWZaTdGQlHLFCYYYEYrODjaPPPPPPPPPPve2fQ6b3U4ap27xzsS.ON7hGQ.ujmmw6uQJW852.TBGYtIYTIO.uhmptCeIuralMUekgck2q5Fn8Wsbc+aiZMKmezpb3EGVcw0EYYVJBjGOnPUDvpBw6P7JxJNRsJCfBQxvR99u0JFbZEM.V3Vqvi1XSNxwNJSr+oyWYPsZRZkNqwH0Sc9nJBLdumhd.uxgUsa.ykxd19x.57kO35lYYUwqewvFq30xYDTkKwV3YgEuAtLg4OzIodYCos2AeVahUBQFMhHzJscdOgqBk5bPPPPPPPPPPve2fQ4839JUgqW.AOdwi24QzQz1CW+1qvMuwxL7XiwKb7CQ4tg2oJJmYU2ffAeQ441oOaU+BohYMVo33UgUAJWd4GCJ7JGNAxCsO+ZUw0c9uJkI+nrS+.2ICwhFGJhpTik2IkqtzRfVyQO4IXjdKA3oh2gWkwS0OrcB7sn7qU6Y5LSmx9VnaYf+r1KsJewyM69728G8PTTDo3PLZDSDKs7xrzMtI8OznbpS7BfMkjV6PEiJu7ryrnUZbj+kezkr64VPPPPPPPPPPPvyiTZe99+E3qDPlOeHRINzlHZa8r3RKyZqsFSO8A3jG4f3oQQYxpvK6d8dI6InotkJb9Xm9qkRgNMJkLSJVUV2o8r3U4CYJmFi0PjyfwaJJ4Yed+MqbfJCmJAmNAmJCm3JFVVEY8VTnMZt68uG2XwEoTkRb5ieDphP5lOf9MdbRFHY.YH9LTEueJECYKawvvxoxeuryP1x38D8U+lG9aCO+0L3x1MH3nnH1o8N3MZfXt1hKwp2cUlXhCvQORO3yRQxJV8QNKsZ0.ONDQPq+O1HpV95IE9AAAAAAAAAAAA+RhpyLXJeMAs2cTTdvZduGsVS6TXsG7PZrSCFcrwX75CR6s2p6Cuavu9cG.V4A955d8uHlbvdUdvrcJA5NuthWgxo++m8dy9QxRtRyueGyr685KwZlQtEQtuU6jMYylr26lXFgQiDfDZAM.5AALOLOp20eK5Q8hdPPP.BZqGLZ5Ylt6gpWHY2EYwhUkUtuFYjYF6Q3teuWyridvtdjQVrZxtUUMynJZ+.b3Q5gmta2q6d.+y9NmuCFUPh1zgCG1AyjfW5DNOcNHqhbPHUM85m87myKV6oTTVxYNyIvBLYuMwXmdb0MucQQzHFkC1Tgz4lC0+y7xME3Kh.f9SKA8SWF1FSZrFoFAEgUW6or216xwN9w43UBVTLFnrnjXLxjISHDB+bE.mcANSlLYxjISlLYx7kQb6W1GCdJi6iRASLGGSrf906gUaXDiPpFvGt4l7m+i+aYgEmg+fuw6QEf0UPJGmhfz.zMVaeYcP20asILchCgtx18KfYfa7SkLwSSE4CzAdXwZGRbNj5c2hVGgPK8VbN1cusXFSKyLnOuXqcg4NIahx+q+w+on0N9u8e9+k7VkkzKTy3EWhcbPg+m0Ez3g2GgokjbX5y6WraBvjxITDsT0TfDKo0Vf2p3MiHV3YVmAeqRO6o38e717+0exeCm4hqvezu6oYYYeZiJE8FxNiafhATTLf5.TXcDqqw8JOaubsazvWnGGutPjOetXqeQDk4YxjISlLYxjISleofQdYGillYtckH7zwfjJVFQMO9wqRScMqrxY4XGaQhnewLGidMyTWNU0m5AVUo0GRcMr33IaLh82aOlYlgbrEWHE9UdOliLgC0OSLiADwz0ytiaZoW+dDAdziWk1lIbxStDGewicn.OKSlLYxjISlLYxj4q9XDUQTCJttDfJhfGSWZJIk8YicGwG9QeBMSlv671uAm8jyiucL+Lom0WBw35D.68T3DTMRSaCpsj.V9Q+3Ojc2dWNyxmgyuxogHzTOBmwhdTnGX6psZULGTJ2BQPSye355V5ObA1tIvG9QeH9lFtxkuLm4zmL8ZblLYxjISlLYxjIyuhfwzYeXTrDQ55gUOHABBX6MKOcic3S93afw.u80tJyJB95IeknWPS84affukBqAq0RPr3pFRMJu+6+SnsskKcgywJmZVDZv2zz4b7QAWfsf1MBpjHHdD7GzuywnfgJV84avGeiOlhpBt5UtByHUns0utW7YxjISlLYxjISlL+RCi.XTCQs.EWm3oj.3nA7ZA28IOm0V8orzRKwEV4TT.Pz+UBAvs9ThGSnAmQwZcXK6SrXHOa6ZtycuGyM67b8qbIFh.9ZLjJAZM9K9w+erwPpulUCDMvzPGyDiHpf35iWMb2G8PV6YOiSd5SwEN2xXAhMMudW7YxjISlLYxjISlL+RDyKCkJKQrcCnHOQ.uwwNn7g27tzT2x671uMmdg4PIRky8y6w8KMz3aQLFDMfDhD.TSAShJexce.as41btyeVt5kNKNfneBkNKFDhwi.Jf0CMYikTpXav24MsAiqh8zHexMuMMMS3MdyqxYN0RDYRZiLxjISlLYxjISlLY9UDLhFAETrnX6RQ4zXExKBOb8c3mdiaSQYIequ4Wii22QydaxfxJjuBjDvgXjhhBDUI38z5CzpBaLpkezO4lDhJW4xWlye5SjDU5qoeUYZlFGd8aAtbPO.CASD5buOk90FhlRdzZufac26hqrf24seKVnzx381kgUUudW7YxjISlLYxjISlL+RjCkkwSmguo4gavXvaJ3oquEO4IqRQogqewKPeAzISnWQwWIFALQTJJbXII.tI.ShBaOpk68vGgQLblSdblqvPLtGwPfhht42zQgP.qKHtRI1cL03uGLams3wwy2ZGd9KVmpxBN+xmh9HzLYLtxrGvYxjISlLYxjISle0Ai26oppBvRcaCVwf0YIXJ.2.9W+u8eOwH7c+t+gbh4b3CM3LBO64qxfA8dcu9+bS+g8vGiTVVRSafn3vYJ49OdUt0MuEm5Lmheye8uNwXC6r45TUUvnQinvXwYd8KfL5Co9QVhnZfhBGEEEDivjl.NWA+E+0+Mr5ieL+1+N+tbkkOIswZFTUxdau6q6k+qcTU+bcISlLYxjIyQODQ94dISlL+8CUUhwHwXjPHPH7k+J.1fwPHDvXTLhh22forDush01aOV64avfgyvoWbApLf5awX.mycznGX+bRHDPUklFOhwPQ+YYq1HO7IqQaaCW57mm4G.UlH8J5RIZDhpdjXJ5Zs1CdSI.wVOMsMzBfqhMpUd55qS+98Y4SsDU.1fmd85gbjYVFmISlLYxjISlLYNJvW02jHiw4nosECIAvsAOPO7l9bi6tJO5QOjSdxiykO2oYHBZaKNWIUUEDhsutW+etoMzRDkw0MHE8vYp3AO847w23FnAOeyu1axrH3zHUkEXcNh.sssXNBHA1YEzNQ7PRPu26ASAldC4tO4IbuG7.V7XKvac0KQO.osl9k8IZrudW7YxjISlLYxjISlibLUD7gqZhupTAEFsyAXgHVRBnhXXqFgO7l2kcVeStv4OKWYkSQkDIFBXMNJJKPOJLGf97hnHhfOpXK5iG3NO3IowezByw67FWgR.0WiSDTqAUrnwHV4ngCpduGUSGGpBhTfqpOQA9I23lr1ZqwJqbJt1EWgBZP8MnXn4HwbLNSlLYxjISlLYxbTgCK3c50eUP36TLJoffBMfP.w5Hfimtwdbia8.btBdiKeAN87CwDqA.0XvXLD+Jvf.1X.iyhXqPJ5QCvsu88Yuc1lqb4KwJKMOn03qGA.9nfZJPDA6Q.8igXKQ0iQTLXAUnrWevzi8ZU9oexMQAt7EuDKhEosFaaSZLWcDvA6LYxjISlLYxjIyQK9z8L+Wk5gdimH1BGRrEzVJJJXTLx8dzy3AO7oblycVt1kNGCEgvnQXEGQD7w3WMNAXLHhfqrjn53oa2vct+CwXs7M+leMFZEZFuKRHUt2sgHFqCA6QhPPJFavXR8BbJXlDJJ5gG3AqtJ29d2iEXccJA...H.jDQAQkNyY3MeiqgEv1zPg.iaqQskutW9YxjISlLYxjISliP72UYO+UFAvg3z.sxCZjpx9r6nIbuGtFas01bkqdMN+xmFC0zNdOrlBBQC9X.q6K+m.fHAMhqnG9nx8ezp73m7D5OrOe8u16QIPrYLVqCiwPqWAap2Y8MGA5AZQw4LTTTf2GosIffkcFMhabqayFarAW95WiqekKiAPBMzyUx3ISvTjE.mISlLYxjISlLYdIe5Rf9S+yeYGSPiXbVHFPhJNaASpaX8M2hIipYkUVg4JDh9FBsMoTPVSwf8WExPooQ6swXv2pr1KdA6r8Nzev.VYoAH.hFozYwZcDQwXbnRp2aeciHBEVGViEMDS8CLJiFOgGu5pLYzHN8xKyIlYHQZwnJkENZaCXLtW2K+LYxjISlLYxjIyQL9rD+9UEQvtnsGZzx.sf8ojWn8382bM9gu+2miuvP9i9M+1zt91rW+FFbpEvp6yPQoM.M6AGAFEtetXtpJ9jG+TNwxWiWDU9e6O9eCB07u3e9uCKP.yjMXgxRhQCSZBTXKvGZnNpTLSJQnecRqedLEAJzQXs6get9rs53Fa2v+we3GwbyNK+y91eKlAg8d9yXogCIHJKT1RXzpfo+q00+unOH8KpLy+79AwiBkwdlLYxjISle4x+X+E4ye+hL+iIUdCJPqySv.ASjfATYpxDClnASDLpCvfDMHpAvvrs6hJfBDEvaRWqcWGLBwXDEknFSFBR5yMVQvqwWomfSsgoRLncAyq8S0yvuZvI0n6evOKhzst.S2mKC9.1oYUjBlO0+emW.iPDPsBQqfJoqwHDH84OiBSirJS20BfyYDhQOQUvUUR.kG+nmP8j847qbNpJcTJsuxAmJfHVLlW2x+97yDeMC5OjHvidzFLZ+8YwkVhSb7kvgfQMDk.wC82IOJs2GhI8GY89HViCWYOpAVe80Yuc2gkO6xzupBPwXEDigPrEUUL1uBXgelLYxjISlLYx7qRnFPhHQGhDwDATEUroIbSTPPQTAg3Ah.S+PjlNM.Q.DHX.jXRPIfffqq8OO3+F.wHZTIPKDUBZDBZmPWCVaR3qyURLFIDTzPRunwj98FwRYOGZjz8QS2uPLhOlBm4dyNOhX.Y5lUYQ5J8XQ.QpPQIDCz1coQCoGKT7gvA22CKcVjzwhqvoDaZoECtdyvNAkO5i9HpGuOu267VLnxQo5vYqQifWULpI8.ZLu1c.8yKi1aLyO+wYaT9Qe3Gvd6sEu269NbwUV4k67.FTQPECR2aKNpfHPH5wW6onpOpcHOcx9b2GbO1YqM3+z+S9CYtAUPrgRmCiyvjwdTAJMN7eY+EvLYxjISlLYxj4WgPmJJTAavfQ.HhJwNUJGRza2sCPraDttaQwABCe48SYZgQr4F6hyXorvhy4vJoPCFMhpJ19QPiD8JXTTLHFCViAiXYuc1EvjLLUMHRx.0nDQPndRaRPqXPMVhVKQmkn0PzVv10szhxjPfIssLtogwMMzFCDBdtyCdFQUwGC3CdZ8dZid7w.ppr2d6dnSVcGmG57myhRcHYwsfgGs5S4V23Snv43W68dKFTBUACpX.BDTPTAPSBf+RdEdndgB6.1XzX9vO5mf22x0t5k4LGaQTeZrOEEHHGb9CQUPhu7Fdch3QiQ7sApFLDkd7fmbKt28tKpF3a7tuCyU5PqGSgy.NvGCXrVrlB7wvq6ifLYxjISlLYxjIyeOIHZmHXPIIRQ5J6YQhfZRk7amqtQSDsSjL.QS5mLZxgTo6ZR5YYggyhA4SUFy.RJ+flzzfhoyk2jStQMMZVUwPYuAXJJwU1mBWEHBgPf51.gP.eugz11xnQSXms2ks2YO1Y+8Y+ISn1G3AO9w3UEeHhWiof9Mn30j.2YV7X.FhDHYTYjjz+HfgACGR5HJ8uQiHG5e6rg1jc1ECXWEtwstOa7h04Bm6xb0KsLEFAmFHfRLBfgfJ3L1CNI9kYLtd.vCexi4AO5AL27yvUt3EXFf15IoyYR5MJo2bkJK.fWorne8QHEfWQgRojInbq68PVcsmxoNyo4ZW77LDv6anr.BRfFIReaUp7IHK.NSlLYxjISlLY9xBsEMbPw8pSKSYCBjDAye25TTAJi0cA8q4f9jcZmsZ.5U1mP.7AOMgVhcs.KNSRnsN.iwhozgzuDwZvaLzJBsFXmwSXhFYuQ6vNiFy16uG6t2tr6nwT21vMt+cRkrcTgXWYRGTjfhDUN1bKRgXYg9CXPudLS+9Lr2.pppvYcbpSOSWeFqHFWWvLaQroifYl6kBfS8Wb5ZQhnpAWvWiXKPK5yy1tgO3m9wfZ3q+duMGuOHgZLQOwt99UUCIe1OJX+4meJqFRsBexctC6MZat90dGN2YNMVABsdnpS7KjpC8HXhJZmvX407ogn1RYQIw5RBQCazLlO4N2k82aO90+FeSN9.GEBnAOlJGsZfHJNaY2FZjISlLYxjISlLY9xBI8Gct59xrhBHoaQNjnXUhnhIExUcUzZAMfZPzXxoXU5jSmBrp81eeBHDEA0X.mEwYw5JAqA6fEwGBLp0ynwiYmQayN6uG6NdDi8sbuG8.F0Vyt0iX+wSnM5AifsvgwZY3LKPUUIyNXFVb94XoEOFGat4XwgCoeQIwwioPMz2ZouyQ+hB54Jov4nvXYmsezqnAS+Th8iw5O6Sbc+ebglZLUU3sBOcys3l27tLXl43a90dOJPPaGSL1fwJoFq1XAUHDCj1egubKD135wS2XS9j6bKTh7FW6xb5iOOwl.khEEOAoaWThfqqDABvQhtAVidJJFh2FoI53EasA269OlPLv29a8MoTASaMDaQPnwCQQv4b3G2B4bvJSlLYxjISlLY9RCUdyqDfUIwswCtMoqJc6xB4CR6YsqGfKBwz8QfHFPLD0TKupXvMXHhXPMkDsVZiQpidlz5Iz1vcpeNiGOlM2bc134ufMV+EryFawj82CeSKyO6LT3LLnpGm5XmjEWbQV5XKxhyu.CFLfStvI6DvpHnXPSg0EAjXDwYA0SLzfNtkQ61xtwVzPfXLxbC6gnbP.MKcWCoay494OpWcdeDSeCAfM1aBq+rmyRm7DboyuLNTT+Df.pTf3jTyJmp17zSzqaEfeNwaDdwV6wZO64XcVVY4SwLVAlzRo0PZ+CDhjrMWfCJ0fiBnZ.AKTTQi2xNiZXqs1Fi.W8RW.Z8n9IX5dSgW8DMfwZIFxBfyjISlLYxjISluLgDcXjz39QhQjo0urLMGm69mHnc885zRAN0emEctlJDES5BF7FAUbXKKwKFZhJ6NZOd9VawpO6Yr1yeF6t69b+c2FMpXDkphJVX1Y4RW4hbpEVj4FLfSuzRz24XlpBFzqGUFAIpD8M3a8X2Y2NwqoP0Jp9zTIBOpForzgXTrRJXtjRKVqEjTinFZiIGqOHknSYzjHFLphuyY6CWF3S6YZ.b8FLjwXXSU4O4O6OmlIs7O469c43yVwn81fEcFLQknBQODLS6YT8PMS7QWFOdLCFL.QD7dOEEEXLFZaaossEaO39qtF29iuAW4seS9NeieM5A3qmfTJDkzQ3zxc1zUxAzcautM.upvwDeCE8mkQAk+c+oeOV8QOj+y+u5+BVYgYvVWiDCTV4nM3wTXnWOGiFOBmw8ZuCf+7Nm7xyYuLYxjISlLeZ9798C9EMmfye+iLuNYrVjbN0IXLJNSDnEMlRpYmjliuwHnJnhhwYPrEXsV1ZSoqjlcnkVBVC0nLNFoVUt2cuGOaqs3Aq9DV8YOk8FOFWYIClcV5OX.e2q8VLyfgb7isHGag4Yt4FRYgM0lnQO9IICTssMDa1mZ7HJGLRlTe53XZEbaEASgAwXQDWRrKov0RUkPaDesGkTeC2alEe4mAiZpehUkXml01fOUoxlCEhWGJLubdJfhd7jmsKq87myLyMKmbw4oDgRmjNqALcDDG0e15r9nLEEE.zMTlSIPl26IFiXsV1LpbuGuJhwwYWdY5YSt65rJgCEw0xmxr6iJ+YufOfP.0BOcyZd5ydAyL+7bgUVlR.i1fgtDf6Hff8LYxjISlLYxjIy++mxAkDBABwZZaCf1fQhXMBNAZaaQUAEAOI2cipBdOQIR0xmCUflnms2cWd7SWi69nGv8e7C4EauASZZwUUxLyMKmckywwOwRrzwWhEVbQ52eHWc+QTX5FQRVAw6QaaPCoxT1k5f3WNRllVL1c5J6OeezXpRVCgohW6LXTiLd7jtPtRRgbkwgwUdfX1Wr95HhbPRUaNzOKhbf9OnSDs.Z3kkIsKXbnhiO5N2lG83GvxKeJtzYOMUDnrvBssoZCWMIwSoNkNs.ki9U.cUUEdeZaFNr.XmyQYYIO3Ed9Qe3MnWug7du06v.rDZFSuBKg1ZTI4yspGj0ZujiBG7gHZgPCvse3S3d2+9bpSeFduqeEJPwnSm6Wvzi.oKs2PNZ6delLYxjISlLYxj4UY+IafwBVwf0oXPPUgXH4favChqDJpPJ5CtBhFGsHDT3e+c9orw5qyid3C3YO8oTOdL85UxwVXdV9LKy0tzkXtd8YoYmiiO2rLWUOJEKlfh5CzalRzPjPngXajXL9JhPCQsar4BF8mseK2cusSZK6FYSpI4E7A1tVZSNWihpAHFPCuzEuybxY.3U562CPU7glW446SWwFNJlgIn7w27Vr6VavU+N+FbtSebLgFLZjnP5foKRsSy.WoqjfO5amn0ZottFiwfy4HDBGry.EEEbiG8Xt08tOm9LKwW6MdS5ADZlfspj5FOPZGDrbnDe9nfv2NrFGpTv1Qke5ctKq+h04cdu2lqe1ygDF08ZXLMqtnamWxheyjISlLYxjISluTx7CJRkFrFSk8aLRaLUwmAbza94v1aHld8YbzvparI25Q2madu6wydw5r03snnnf986w4N+JbxicLN8RKwIVXQlueEmX14nHFnLpTzFPFOBcRMwISH58LZV2AAPEXNPmk04vZbD8SaxRymYyxJtRLct0ZLltzk5feKN2Ke7iwWVJzSudz16B7p83ajWVttiGO9UlgwGbo6N3v.O8E6ycev8nrWEW6xWfE6U.M6SHzfZETbLMKsrZj.FTIkYWxQ7d.F3.QuSuTVVRYYIMMM7StycXbcMW57WlKr3h3.BgVTo.01YwsNU7O.wWIE0dcSoqhVWAqsYM27t2Cqyw0u5UXVDB0iO3MBAiEUhcI2MXz3WpJk8LYxjISlLYxjICH6uKfAwXRionhBbt9nEUXKpXywsr1Zqysdzi4tO4wr1laxXuGS2XH5e5UeCVXgE3zm5DbxStDyNyPrZjl5IDZFS85qQSnglfGmpX.JMVpFXomsjQconaRTosaD4ZvqPaHP7PovbTAlpaZp3CoA55K2nltSgPZd.GiQlDGmpX0tGemwfQDrpfHFrtE5dbDDaZD8JVS5wzHL2hctNe39DFNHDmcs.+zadGd1SeFqbwyyUtvJTPDQaS1MaK4vkNax.3HJ1iDg.0uHltSAhHGX+cUUEwXjm+7myMt8sYlEOFu8a9VzG.eMEhPPBHklCF0Ql3z.v5nUOPavRTMb6GbOt+ieDm9bmiqe0qfEP8dDmiPW5tkR8MOVU6b.NB7yOlvyjISlLYxjISlLGcX6ZOk8GP0byQYQO1MDYss2kUW+4r49S3iu0sX2Q6yt6tOAUY9Emi27ZWkKc0KyoN0o3cZZv68zTOglM2jcV+4TXMT3LT5LzyIfqjoUBqnwNiDUBnzLtEiwf0Vf0lDcpwjoiQRignCJmYgtjnVNnbZGMtAqXv3Ro6rHNhVIU0pJT1aPpDnUk.Bsc54TMomq2fEIDiDzHsAOMAOs9.sA+Ke9UMsd7ABwz0wP.hJtQAkO7m9wr2Nayuwu82lUN8IH5qomwPzLMVrS0kcpYlm53qkOith8HGppXLIq4Cg.gP.iwv96uO26d2iUev84bW3h7lW85XAZmTSkyPSrASoCeaJFsMc8.czbzxw6PafZZ412+Ar9KdA+de2eetxEWl5I6w.mkji0lt93Nl1ult4t0QoR4NSlLYxjISlLYx7KFckKwdQkUmzvK1XKd3K1fGt5S4wO6Er616gIBqbxSx25c957VW37rxwWjJIRXzdT+hmyN1FDQvZMLXfAiTAQEQCngHMMo1GUrIMTpXIFhLpygVuOUbyVqh0ZS2WIUHyV.KwWp23yfAyzGiwAhPvXHnvXej5nPsWQhPiHTGgIdOSZao1Gn0mD8d2e7cIFicBeaotsglPam.3HgP3fjgVi5KSJ5X51bSpgG7vGSS8DVdkkYtA8QmrKtJGAMRiZ5lgTSIhQMuRMWeTmoBfaaaw68npx3wiYs0ViwasEK8q+s3TmZQBQHTWialRFqSvYJAw+x..qiiRN.211RKAd5yViI6OhUN6YYVAlLdeN1vBpCo..S6BAKI9pixoLYxjISlLYxjIyWd3o6OgG8jU4Ct4s41O3QrWSKCWbINyxmiq7Vmjeqeiec50FnWyX52NA1dar9Z5EaYdMvpEwj0lwHwtRQ1Qp7hMVgxpA.Pnyqy.BXTLFEIpLbXYRbYmfXUCopDlNmh0W1xk.+LsLacrfl1ZlzTytSZX8c1g02cOVeuwrWim673UIHVZvPKB9XjfJD0HQEJ5c7zyi0jFuStNw5NW2nOBDiAQL3rodS1X5baFA2+C+G9o7B2w3LW7qy3cUdQDNcu4XW.J2C+nsvRfZw2EE0VLhC03ADFGl1jySS3KSWrTaS0ps0kRp3P.qBFS2PYNlNg0zq6DhZNvOYQ4S02sujOs3Su3PPwFSA8MlFhhAOkfVhIThS5Q+3Xh06iszxHFvOZRM+O+Q6ia2ayez24eEmnRXb89zeXOlTWSkq.ssgBqRzDnsnavQCf5R0kdD97JjrpMRv.9h.gtYSkIThnFTRkCPSnAg.UENpLf1lFQSwnxnicJ9927d789A+obtqdJ9O6a7drTTXjWQEgwko0WUC3BFTFPz3oovSzDop8y0x+y8bx6Wz++eQjmCeYxjISlLe0i+wdN7lmyuY97fKX.AZsofXJJcSbFzjdmPDaPQjTf7pkFBVglXfVMfKNC0imPz2Pkqf450CqIEbu6OolAyOO615YDJw98HTMfspmvCV6o7j0dF+o+42m98JXo46y0uz6xkO0bbwiOKmdg9rPkgvF+XLSc9jRLlTJPuqVPH3neq+.MVVRk1bgXNHnnpaFmJqYCnFAwHfURp8rBwcRNDi0hAGdofno.srfXYEwxB1osksiszXLLw.ar+t7fG+Dd9yWiUez3NGYaH11hF7fuEw2fD7bhElmYsVlqpjiMyLrvbCYgAyxrCqnzUvIJ7GDfVScf9kNQK+c942oGyt+h+s+eyFq9HH54uvFY+sWmqb9k4BKuDm8DKx4l6rccIZMgvXZZFgDUr.NmkAtWNmcgTO2xzcBHJTVXvGi3idZUEqXw5JwUUQgwhudmtkj4fEk7oVjGfZ9YuM6KugzXYx7oLaWQLP.ELNbkED.VeqsX802jkWYElY3LomyXprfmFVV+x7u8oSmoTGJRvm172ViAHcNMzDI3UR6aiPCJ2+gOlXHxoNyonzYIDUbkV7w.FoK8mm9DIGJ7qx+s8LYxjISlLYxj4eXHcSJmtQKpQLnRWetpPwf9Da8zz1fuslXiuSvHXEAMtOyOX.N2PZaaYq8GgOFnp2.FbhkXm5FpV7XXrB26Iqw6+Q+.936dG1c7XDqi+vuyuOy1qOGet9bhY6wIFVxbtHU3IFaPjBh3IZjCZhUroeVDHxnCz6DIIXrsM0pnwHr3wV.eTotcBMgHAeL4Nryh0ZHr3r3bkHVCgfxd0SXm8Gwd6rIS7AdvSdBOe6sX0W7B1dzHTqAaQIQCDivoVZEJJsLyfkX9YmkElYFVb1ALSudz2YwQDWPovDo.nTLTHljQpZD+NOKY1pQQDKVKo0mjRkZk.pZP5zTMUaEDQTCtAk0L6kWFEC6s2H9i+W+e.eaKKbrk3Tm9j767a8c3LmXQt14NMm83yyr8WfRAfIfVSyNoXnFUwHJEVCN6znv1RfFbVntDZiBspPcv2ElVJC7epPX5Uhy5+NdOmBGDLWccnrJoqiR2iQWY9pD65+2HRgkhpYXasl6+3U44OaM9m8cdGNwBGCKPLpXrJFyz2PjDD+JqC8kyQWYZBY84Aw7YdjJZWCiqJNa25w2RaaHMqsLknFgcU38+IeHFDd6q+FLnxguslppJZi0fsq+k6NipxgBfM8neObmISlLYxjISlLGkHXBHJXiRmH3Tka1ZSkM73fRnKHoTufwKTEUFHV54JviRa6HlLQI5bTN6rXbNFCrtWYyZOq9vOhadm6x8u+CY7jwbrieb9lu6WmkWdYd6YWjRmk9kET4b3jVzPf1lIz3anWud36z+nhAqwfwHI2bUnpbZJIaRiWHLnQAI5vnvCV+4TT1idCFR0v9XrttPypkIsdtiqkwasGat0l7rm+Ld1yeNau81zTWiFfRmEIZnmqjqbxk4zGeIV9jmhiuvhLrZHQ19.GbcVKNiPgwhy.FBXzXpmhUEzvA8vKwTkGWTUxzzXVDyz.kty7vt99UicBpNz0nXzHt+69W9ufEO9RHE83tO3o7Cd+Ofa7Q2lMVeat+ceL+z2++QV5zmf23ZWh27JmmKc9Sx4O0w3jGadlqZdlYtThJSHfFaIFaoI3IDaglZhwHhyh0VgovPSTooUosYLgnxLl9oy+RRDqpcGOxmZL87ob+UnykTI9J44j1EXWpDQzTYQartzNWTTfRIOcis3N26ATOolu4690Yo4mGg.onhJ0yy7yszXdEOU+bQjzHU5.QoG52IDI5i3prHBnZxEXq0gXcHVKOdyH23F2gA8mk26MeSFT4HLZL1gELd7HD0ggW1CyJoyuFLSSD7LYxjISlLYxjIyeeQzNMGJGVvxTsJ00sX5YonvgyVfwzhq0iMnDaU1bxH7phVVQQ+AvLyxdia3NO8Y73m8bd+O3CX+QSPhvYOyx7tu4ax0u3EYtgCQCQlemUSBWGqDhQhckspJBVSWPE2cao0TnSiSmnoltYHr1RaDB.QQHZsDDGKcsqR.K9PjMFMgs1bSdw5avKdwlr+96y26929fiYiAp50iYmcHKe1UX1A8Y4SbZlupOGevbr3fYXn0QQTvE0ToYWjzHlBTqFhsA7dOD83iQb1jAoo1psqTmQNXZJEsuz.UsSPipZZ58DinHIkZSes4fWi5lCv+gW6LzuXF7pgu9hWg+f28Rr556v8e7y4wOaC9y9y+dr2nI7i+feLe++5+R5MXHm+xWl29cdaN+ENO+tW4zTU1ig8DpPvAn5DFMdeB0iwIBknTP.IFoPUbnLrvhwXITWlD6RxwS03OPLbZcFe4KdSKK3oB5jHnweFmLiZ23TVBcw2kAu1h0MfF0vse3C4QqtFybriyae4qvLhi11ZrpfQjCdSzgNO8pumua87Y6c6+vPES539vO9woOxFhAODAiIk5YFigxxRzhJ7gHe3cdHat4N7NuyU3Zm6bTIo2LqlRhHX0C8FD4kWX54wbXXkISlLYxjISlL+8lnj9VzAYZPQEOn0IEf9FCRKH9HhFHzFHDhLQiXIh+3mAaYIMFg6u4lbyO484VO3Q7rMVmwSpYtYmi24Bmm27xWgKelUXwdUXlzR6FqiexXXP.syYSwjJE6zHIxBckkrMBlXDiFwhEWHfDRNot6HOVWI1pBJ5WQzZo0YXrABpvG7303E6tOO4Yqwpq8L1bmcntoEDCFqkyLbFle944jm3jbpSrDGa94XXudzqnjRiPo0RAfMFHNYDs6MlwssXUEqXPK65Y5txv1XDJ5UfP0ANCKciAooWmzmkz740vKewXpVs3KE7KGxbwCtaSGIvB3501v3QOk5VOFWEmZgE3jmbAt9IWfWr2d769MuFOdsM3ityc4iu083wOYMd38e.28N2EDC+oWdYVZwiyEu3Y4pW9xbgUNMGelJFNnGkCRMCtSZQi03mLhPSMFMRYgixpJF0IvUkX2kNwuRr6.pKFsEPI0L4wXxo1oGyRW8bmNPmJqKhA51oCKMQkBwwdD3F259r0FaxEeq2lSs3wnTf5lZrhhU.eLh1IpVUA8fA.7W7SNnTiyeHA7uhqrIq5QCnwHRLf0VRQQEiA1aRM+3O7SvUNj23JWikJrXiS.ajVsknXS6VhRpDB3viwIyAy03LYxjISlLYxjIyeOQEBSMWr6lDIE3uhBUlB7swtRPFzhdH8J.miVAlL+w4QO4I7ge7GwstysY681gdC5wRm3Db7Elmeyu02hANGCvhLZD6+hmgLoggtBlspG6EqwJFp5Wg0XQzThNGBIw1RpzQ6bMM8c90nRrMPv6Y3YuLAfVhrayD1Xu83YatEOemsY6wi3G99uOAIogpnWelcgE37m+Db7kNAyLbF9MO6onv4nxkJY5XSM95ZB6uGhOxjIiHZLXcBNikJqvvAEzqWAEEEr63VTd4XIRiQzvTQtJoMT3yRnRxfPi4vAc0q9BgRpmr+zYcj9xwPLtpAKgNYDA+1XBQz81jhBGEhxvdvUlcHu6INK+1u6J7h891buUeAe3MuKevG9w7vG9H9fO7lTT8.9Q23NbpS8IbtycNtxktHm6rmgklaFVXnvPWACMk3JKIn6RrYeDe.QGAlxCV6hDA7cK8.up6jFPLD0TyaO8McRmCvFEhcAokJche0XWs4GIDSmL1otlacu6y3Qi3q8M90XlxRLnnMMTzqDQjTCfqFTif9YznuSc+ke1e0+fYZII+Y5zr.VSpUtUumXHf0.QikQiq44auKexGcCl+XGm27JWk9.Daw5TZZqSw+MBIMu5Aax.Rxk4el.EKSlLYxjISlLYx7yEkRPiDkHPmSrphnQbQSxzJuhhE50iX+grqn7hQiYy82iezeyOkUexS34q9XJcBW8RWlu969l7FW77b7YFxVq8TL6sORaK8EG8GzCW+9DpansdBMEguN1KJ...B.IQTPTEXwfFAGJVMPLDw6aIFiXcVDiAuXwXJHnFBsAp0Hsd3YkJ6t6tr1ZqwpqtJuXsmw1asESFMlXim29MeCVXvLbhENFm53KwRKdLlevLTUThSLr0leBDhLtwmlzOAkRmkEp5Su98XbLfw.XMnhhmHSpGwd60PSvywFrLSimKP.ikjVkz421Pa2XLJ0VpFCXrFnyw3vnQuxqGo62geA5Uqv0oRdl936VeqswHJ8GLCUNv2LlI6uCENKCF1i82+E3UAm3XkgyvYu1U3acsKyF+AeG1bm84O968Ar1pOiaem6vG727C4u8u56yrKt.mdkyxIVZI9C989s3zGadtvYVfi2qhdCpfAyPa6DBZ.6z0W2afRCQY8UUsqFnyOW3PNlpFLZL4jolr+WOvM0HlXm3QUwqfPA6OYBq+hsHFBboyeVbZfXSCpuECEofzR8DE6mYHI+RQiewzGvQSpd8m537mVGr0lp2c0GHFTzBk1Xj8Z8r9diYiUWkKc4yxJKcBJDvGZw5LLt0i00CIX5ZOg3T6xO333KjP7JSlLYxjISlLY9UNLXXZqXFvDELpgHofz0WTPvUB8Gxdhk6+hmyGbqayce3C4gOZUN24OK+g+9+9b8KcNN8byRIsX1aW1c8mRkOhCvFAq5IJAZwgonfxx9LnT6b6sg1XMlH3bB1xRbNKipaPMVvUB1RhXoVZY+fRsW3+k+O++fc2YG1cqMwW2vbCGxEO+E4MuzkYkSdRVr2PpLB8PnHpHsdhasN0SZXTqmpEBXsFrCpPTkPiGBQpqGSa8jTX711hutlnpXJcTUTRud8Rk8baJDtlJCbZ6fJchfsFPEkHdzX5XM1jD7opxPsB3kiyrzn5M8ypQv2EVVeZguSwU1O8qBDXTDDaIxfigmH6zBhs.GfSMPc.XKJ.lwAm+X84h+W+OkQMv5asC2+QOkO9StK+jO9lb6aeW9vev2m2+G78Y9EVfKdoywa8VWk28MuJW3bmlEKpvBbBokZeMSFm1QCinzupjYFLfBSA6r2NoZA2ll+vQAD0RjTiN2zLgRqCmIchzGBDzHh56bP0QHDn2fYPof+x+p2m6c66xW+272m23hCgQ6RH1vLC6CpRquk986SraPNqcwYN7YoU7ye+yphzcjz8b7oTcaLoR91ZszePAq8hcXwyLOkyr.+u+u4+IZ1aK9898+ug23BKQntIMKjEIIbdZZUygbZ9fmnX2GR+GWxyYuLYxjISlLeQyunuewuHxe+iL+7HDBTTTPYYp5Pig.9PZRr.vby1mI6Oh58pgnRuhJJqJwaDFihLyLn8pXm1.ex8e.+nO7FbuG8DBQk98Fv+8+q9WhSDprPeAp1cKpTONMETV0iGQUQOJqpHHVlzFnIDIF8fA5OIPuhBL85g26Xb8DZ7fXLXqpPmcHdaA611xidwFbq69.dvidBat8Nz15YgJKmcok3Re62hyuxJrzBKP+BGNTDeKZy9XidDUSsUpFwZhX6qPeHZJHFUZ0PRSlUPc1C5oWegCouCSbHE.ZHltTmZqy8YcrVKUENJJJvzM6d8gVHDQ5ltOBgWZfWWBOCPouKEqOTFWEm1xrphwzMVbOzzu4veh24zIci5mjKqp3PIEk2oYyarKduiG31pQijjNEw0SXwRXoSNOKOeOt5Jmje8u104oquMaueC+I+69SXmc1k+1evOfe32+ujiszI3xW6Jb827M3Lm4L7aetYYlACYtYNI8mgtdvslm7hmy96rCm9zmBM3QqqQ0IIwvEkoSVNKUkyfF.ZCngHVikphBJJ6gULLZmcIXLX5MfMZ77vUeFkU83BKeZFxzzj9vhCSy0qCKV7vykXoa7J8EUJPqnGHBV5d08vhfUgtc9H0j68FNCX5wZatMO54axbGeAVneEURpTuivAg3Up2nicutBJlTngwOqP6LYxjISlLYxjICLb3P7dOiGOlPnyMQUO3xiez84Tm3Tb5ScZrXYyc1kMGWiLnOtEVf697mwcV6o7g27SX00dFNSAW47Wh275WmUNyJLyNOCqXnWDJiBERDaLfUCHQkElYNTUnMDYRnk5HnlBjBK1hRvWy31Z70AbEUzatiAtRFQj87AtwctOO54ufad+Gvy1XKDqkEOwo3xu0axBKbL90le.860i4FLfdUU3hJwwSHzLA7szqvfvK06cfoecYIjigfSnPjTdFYhDHkrzppreSCppX8ZJftDAGFbhf0YoeYpGk8g.MiaOHomcNGtBKiFMJk7ylhTfXIxAZWTUYsc1KcaVSJ3ubFDqMMqkkTgUOUO2zQj6gM8y0KtWmfWKAJPkHdoLMOnvPDapogAhh1IBNIrR.ZpWCqofAEkLnWAm3zyx0N07LAgVfu86cAd3pOmexO8S3iu4sY0mtF+v+xe.e++h+FDQ3+329c4jmXItxktDW+xWhUN87LmTwvSbVl8DfnAjPM3qgPCNiRYgghhT4MuiGBssDmzlBJJiiPPnoMfnJFDZCJXK3QO9Y7we7sX14liu1adEl6URa5zUQ4U8D8uKghwt9L9yqPX8fcv7v6j4qlNyJJdeKtRGCm+XLA3SdvpbmGtJW8ZuEKuXepjzFSDOHIzhXrZWs0+xze1zkj1F8kgKVlLYxjISlLYxjoCUOXL8Hhfy4RtJpoxw8Mt7EYu8GycexCQckXGLK94misZBr0pOm+e9y9yY7jI.Jm6TmkKuxxbsycANywONyzqh1sCHDe4bDVR1fE5ZIxQiaHFUThfIMNkrttPC1OhsrBhygsXHQWI6zDX0UWi69vGypO+473mtFQwPYUEW+BWlkW9Lb1UNCKcriyf9UbxlIoVGUins0DZZQZaoPSABrIjD8F65WR0jVWcwyKi2Xmj.0BGFqEbBBJVMMVlloWY5bnM.w.FQvJBVqEqXHDEh9tToNzEnXFGgnEw3vnoxIWjBDwjlWwwoul.K81WK4XbL4JeaLfOFnUSAOVrKCrhccPaZrydnd.F01kaxVLIilSp8kP2nIpSf2gF0Uo1UNc6M0iQnFaqAiXQrFLhgdRAkhkuwJmlu1Jml+fu06xZaOg6d+mxGcyayGei6vSWaM9q99+3T481+ulkNwRbsqbAdu25M3ct9UYki2GmXv452Mjm6AwZF0NF+N6h22P4LmALNJqDJrNrEkHhhOLAeqOMwqBE3At0CdBO39Ohq8FWk29xmkAozfJcDeHgueVwl8gSNroD+BV7Xp44eof5zaxROosQECVJLUrSsxGcq6wlasKe823BrxhyPo.sQOTXQI1I9OlZV7CsVMQCGT3yxWbNYmISlLYxjISlLeUf86bfrnH4.I7RGfEQnAgXuJb8mgXUeFEUt8pqwe6G9wbqacWJKq3hm6b7Mdq2gqc9UXwdkDGOh5sVmslLhE6U7J1coHDvRnqGX6O2Lzz1RaacW0uZPLFBdOdumvwOCMMsr816vie7c3d28g7jm9L1e+IDiJW+MtNm6rKy0uzkY4StDkFk1w6S6nQnasGM00cOwJZHBQMkVykkT4rLtYLSG5tQ4ksRYraEO6r8O3bknJZWMHGIhpBEdCQRaVfOBMAeRrZWO75Ft.FWIE8KnrnLIhVRdN6UEiwgOFntwyjlZlzzPSSKM9VhQ3Qeu+eIDBz58zzzPcaK0so.1JDCr8NambAtS.7qzKvB3lHK1s3SoNLck2rQi.0u7cBGTlzfNstpUCCFbL.MUS5w.ZL.g.FR1Yuy9ufxpgL2f49+i8dyhwRROOSum+kX4rex8JyJWp8pqtqt6hMY2r4hHEWGs.Y6ABXz3QXF6aLfGOvWLXrg8c1FF9BOv9FubmMf8XAaIvQDRyHMjRZjFIwQTjrYuW6YUYVYkYk6qm8X4++2WDwIyrptojAnnatDO.YEm07DmHNYgy6+2226KiznDW5kNOu9yeN16vOEcZ2gem29Nr+N6xpqrBarw5r5iWlu626MXpIGmQZTge4eguBMpFvjiTkIpWkxhJH7CvoZiKwmAt7.U15.RIU3PJy6+akDkL.oxmVQNV7gqxf9wrvByyz0qhx18Xgute.hAE4lq0yNqrCEI+CaqDOTfq3YJEqSHwIr4tdctCXiBiCdx16y8VZU7JOBuzElhyzvGY9JRIBTXcVboFTRMBRwJz3Dp782LQ1NYZd0eKD.WPAETPAETPAETvPF5+NCSGlnnHLFCJkBsVyNwJJ2bLLRA2c0U3Md+2mUVac78JyBKLKexqeCNS8FLc85T13vr2tXGziPEzrdYF3xaSW2IyitEGVGHjBRkJrprwjTHkHz57LxIhDK7m892hiN7P1bsMY2s2ALBlc5Y30e8qwEmedBUZp3qorBT6sMwcaAQCntRRo.O53jHDZD4Idyv8AmTPhPfQpOoEhQbbAPGJWQUxK2XpRwllhK0brlIoPfIJEgVgme.9kzfmBqHKqeP.65WAiChSRn+fAzoeO5ze.8hiIxjP6d8nebDc52mN85SuA8yb3ZmEbRTaevwyaL44FLRQ96GNd9eAN45bpJ.mJ8yE+l2VyBNteuE44vDBANm.ovfM+2lCYVEDcZv5P3T4Nw7vZFaQHjTy2CiMlztGhQ4izKflZepOZIbiVloN+jzJBdzpOgG7fGxCd3R73UVk0dxp7fE6xhK8PFsYCVXtY3JW37boKLOyN8YXzJSPfRj2j1F7rQXFLfn98vZSQJyBD5JkKiS3wZqeDO3QqP0QGiqdkKS.fMtEVoJ6ibGm4vximQ1gmHgS1N7CC+Mk.3OLbGuRKBrNKZkFs1GjRRAVb4GyZquMm4BWgKclQnYnDa7fr+vTDluu2GoMaUXPXIOIvxN+3b7zdiVAETPAETPAETPAE.fVqAxLCqgsAcPP.kByLlpsEk3lObYdma9drxpOFzRtvBmia77WmKc14nlThpeLt16Sun9nbV78U346gmmhdIj6+OYkBy4xa0Zo.mPvQsaiHvC+vxjHUrW+9r9lawiex5r696wc26.THodkx7xW65bgElmyOyrLdyFTVKo896hoSKZ2qCxzXpnTTwyi.kBc9qkRYQo7xT8Ys4sPrgTi8o06jqIRAGKc3n1Gl0grtrNTU4oQozH09HjZT9Aj3bzEG8RSoa2AzIJhdCFPbZJKt2tLXv.5zsCsa2lAQCHIMMO3ejDTJjTmMSvqPfTqnT4.7CCvyyiW85uHBoDsRg1yCsuGZsFUdqpWtZkm574GvEnEhAfPl0i4tgQO7vXG5THN80Ow4hsG063rWRHD3D57RIawIE3WtTVvLaGVC0Xbow3x6g6yUtBtPEW8RmgO8Emj0O7E3QquCqr0dr6gs4a968M3wc2iUWeedi2cYlX7wYt4lmElaVFazQ40tRUFobElHLfvJ0IvkPRZDBgCs1m3XCIBGqr15r9FawLyeNt7kuLN5ivL.mp7wumFtwkOarYhayahbwyVC3ghl+gcFf+PDQ6jG+54bhrJYq8IEI8RbrzidLGcvQ7Zu9mioaThRXXPRVuuqFliVVGBqAjp77+MqOtyxLYCFbY80egF3BJnfBJnfBJnfBNFkRQZZZV65p0TJLDi0R6Ncn+1ayW6wsX2M2f3iNhyM2U3SeiWjm+byiNoOc2cSjdBrlDv4vuhOAAkAgfACFvAGzgfpifDv4RQZMnr1rpXpjfTinjO8cBdxQsY8i5vRauKqt8dr8AGQu9C3W94eYZ1nFyM8TL2zSwnUBIpSK1Zo6wJauNW3BKfP4P0Lj.cMjREwQIramtzqWOJUsLJTnEoGaFu17p8MbdjE.JGHrxbSP9DMKo5.DJIBsGVshXgjNVnuyw.mii5bHshhX2VsY6CNhcN3.Z0tM86GQZpgRlixJfpvQfmO0JUgliOBiTuAUBCXtYlEekjPOMkCCoTP.k7CHvSiRon0Q6e79pLuRvGWQXfACF7C7bqzAZDoYWyIeJAcGO6u4BCEOqz47auVX47mddF85xrwZiHqFioFGt7UHHamzj+rSAmg1q9X7BKitdcZFzjxiLNSOxX7Bu.z2Ae9u5eKd3Rava9luM25seOt28Vg6e2GS8QFgZ0ZvslqOWZg44Sb4qv4mcFBJElsZDBC3zDmZvpfc16.ZcTadta7RL43U.WGzXwJFtemeP4+O9GF+MMYZtkOysM7ChJTdRLoJ5MHls2YOhihYl4WfxdJzXwjFmaM6YQ2zvJViyl0N.bJ+tZ38IrEBfKnfBJnfBJnfBJ3T3o0GK.d3L.e3AGvcu6cYokVhuSKOtx0tFe4O6OOO2YlhJI8n2l6RnqOM8jD5KI0ZI1kfkX5FkfCENGHC7x9N9NadWaZQAnx65VqvRm18X4c1m25AKwcexNzFIiO2E4U9R+BL+ktLextcQgCgKAygGwlarFJWJSTwmEd9qvt6uC5.MBgOc5GwfnTbnnTkxL4DSR+NGjYpW45fLF6ozpkIHNS7KHcFz1bAvjIB1HkXsPRbDsZGyVc6v56uOqdXKNneOd7laShPhQnPn7wuRMpMxTb1K1jRkqxqLoGZoBeOMU7Cyh.WuPBUBBjJ5t+9DHT3K.+rbukjCayfA8xLrqwJmMS1.oN2wUiFx1NLdbe1zRan.dwQ816iTIPFUl.bgKyYsk4l.kvkMmw9kJSpzQujD15v83dO5g7N2583cu8sYiMVm1xyijPBJcFlX1Y3723rb8qeQdw4mkoEYkqumC9G8e1+Sr0pOf+o+W+OlO8UV.ZsFSLZCNre+iy6WqELNKRQlYZo0ZRiSy9v.hLmRyja+2xLa2NMIeADF5hz4uuFtdA1mUQ8yTIYUp9TsfdLBUOrBHRVFbRpfDrN1ONEYso328tOl+W9e7+YVX5l7O9+z+i4Uq8CmSb8i5bzqHGfKnfBJnfB9YOJ99EE7iR54KQZsnHEkyhmwhzJ.mGNxleUCNRsV78B.mOat117j01DSJT02fM0fIMAgyghrBQIjBRQxQchHXjwvq4Xrc2A7cu683Mu0s4vN8wubY9O+KdIBzATR5QnvGOiFoUgTnvIULvkhACNgEoxhVKPhAgIEWZBdQRRkJhJUhjJ0oSXY1IJg0OnE6cXK9K9y+VnLVZFn4xmYFtw4mgKLwDzvSfHMk9lj+JO9jjjjYhW4sDLjahWlLmstua.ggAD3GfxAwQIjDkfwYAgGoNKBu.vK.7Cwnkj5TDYSw3f+jsZQqCOjs1XM1eqMYP2VnEPk.EU78YgolfQCBY5QpyrMGgwKUkZd93KrfwQqgBj9.cX6PEpli+abQdmEmUg2bQtwmjxMm5Ych9qe.29Pz+0+QrOZX3NbmNcPG5SI+.lc7YYrwmjW3kdQ9Ea2ld86w+7+M2mc1beVcosYo286witaedywKyzSLFiVcBd9q9ZTt1T3TRN6EuHMGeZjZECTkXy86PPIeDhrdH24bHRSIMMgzjHF3bHDYC6tVqQJzHxC4YTRPnvXRep86iWYgbovmTI17Jrebk14op9ZVaWKya+4rVSVhDiwPbbJZu.hvwFOYcvYYlyLEMpTAn2OZNATPAETPAETPAETvOFhxjU6IIC+t1Y18aVB17AavQQdB2HbNTNKw86gyYQIj36GPnmOoVAs50m18GvbW9xrx16xsdiuK2akUYi81mffx7Ie4qykuxyQotqfmQimTivMz8jx7OIINZVuJQICnc+NLn+.bNCZoB0Ps.MGAoeHNkh8Z0l6d+Gx8e7S3I6rCc5ziYmYFlcpI3JyNKyMRSZfEYTe50Y.JiAQXvG3XxwM3ovRPfNaDTMIjllUYWkHq3cdddDFDPbbBsOpCCFDiwBAgkoZ8QIrRc5kjPpTPeigCZ2i01YSVZkU3Qq8X1auCHpRC788oRkxL23MYhqbANy3iwTMpS0.eTIwDJfxJAkkR7QPpKyAqwXQI7y2ocOi3T6IaymU4r2S4FFbdjQk9LBb+fGLFZxvOyu17eWejK.dn2K6PdrCiIO06ln98IJIFYPDReOzddLpeMZLVMb.+W92+EYuNFdvhOgas3hbuUuCqu9RrxctG2sys412ZcpN5rbvgs3LyOMOZuVL4jgTuwjHA7r8A.mMEqMAgCBUR7K4iVIoa2tHycTLiMgXCDmZycsMnR4FfHyZvjNKBg83rRFNYgMxdKIyM2JYdKlKQ3FdFIeViwCg8j1s.qiAChIrVC1YPedvcuGJgfm6BmioJG.CJD.WPAETPAETPAE7yN3YOocby5RSvHT.ZrBYtmFk8uBrHHEoKAEw3bNJ6YwZcjDkRuAIbjSA9kQTsIkFqD+au+C3VKdet+CVDABtz7yyyeoqxEl4rLRspvQUQ4xLXVmRfUjfSYPj6Zxat4NTtbEZVtJHpwf3D5GkPhMS.2dMGmCN3Hd7JKwRKsD6ryNHQvriNBSN+r7bW9BLdiZblZ0wKMk31GfYPuLQkgAzwPt5N6Iwx5vA2E2wF2kTIyDQ5L3bVhSRPXsD21h1OjxkZR8QJgSERWCrS+HZuSWd3pqxlGd.OdymvV6tMQoITpVIZNVSl+byyMNyjTxOf5UpQyJ0ndoPJI03I.k0hKQCVCFSJVSJoNCo.hrd8lJQCmQW6SI.dXKY63TBnDBDxrhC5jh7sgenet3XMjV6S4wRm9xR9w.Av+.I20wlXhIx5gdaV9QE2a.F2IAbb4RknZUMK7wVfO0MlmsZ+pr7pOlkW9wrw1s48t+lrwFqSb+tr8Sb7a9a903sO2Y4kdwqxzSOEO2HUPqkDn7Q4khkADmNf985izZnZsx4CCthTxhbIOeEH7QHkjXRFVvVrB2wY5kSHy+iRxpp6v2VN4w4JL4qWTlv3gmJ7xmIfrmiT6QJCvIBXsMVkku2hTsbIt1ENG0dlnSpfBJnfBJnfBJnfeZGsA.YtI8ZONtVyRRk7tszl46PR.gyf1khxklILNwfI0fwoQ3GhzuLcsB1duCY+Aavu+e32.kVyjiOAO+kt.W+7mmYpWG0fHh2YMJIByLQXgkDxMWVxJDlFAiLVc52Mlc1aWPFfekFnazDKRhSR3O5MdKNXmcYuM1.aTLmczl77W7h77W57L8XifHIhj9cYv1qSu3AHcPYOO78zfTh0kmmKmJUZD4k91IfRgkv4bjZRyxKWqEqykM6wRAUlaNLoV5j3n2fD1s0ArxZayCWcc1Xm8IJMAkuGkqVlKe4miImZTldloX7wGkZ0pvbQswYIKwYRM3h5iKIkzjrNo02yGq0lEsSBCNo.gRgTpQnxRuF3jVU9XFVYWm.q3jQcP3.jmjmQYGqyJFo3TGCN1bgcliermzMtm7R7Qt.X4eU1Nk.rFCBf.gBOkhPc9I6bWo9ncdBRkGZ+RTMHjR0KyzuvyyMt504nTn92584a7M9WSTrGMGaDVe0k4Au62m+z+rZ3GFvW7m6yvDiONma9Y47yeVlnRcJGz.MfVjxtGrAZoCsPfBAJo.ekBeOAZkOGY5lU7VW1zCXcCybWwwszrPjUcXoMKikyZYiTDNHMuRul7SGZmNedfy6WeOEpRknGv8V5Qb3d6vK9RuHyO433Kbj9ANnUPAETPAETPAETvO8hzd5XIUhYnv27exp3mCgygvYycvXW92+1wfVwfW.dMZhpQSNDAOZ0mv24cdGt6ctGm4LSyKboKxq7bWkyTuJttGQ6mrJAJGU78w56iSZwoxD9ZclrQojrpXJ7BHUYHQIvqRSRq0jc6zi2ewEYwkVlk2aMZTsNKL0Y3pKLOWZlyvXkBPFMftqsJ9ZXPTeRRhw22mRkqfVpnyfAzqUW7pW9TU.GjVApi6dXEcNnCFbYlUkTfLvGco.7CKgzSy6zsEas0Nr7xOhMWea50IBIZTBe7Jq3i+7uLiVoJyL9HL0nMoVfFQRLI86P7AGPOWuiSsFoPgj7Vr1ShxuDRo7opzpykk0vtDAj.c87Fdl7C87qMuBtBF5ryhiMjYgP.xnrG3oEPmK9UJx8..2vW67WogWmeLP.7oUiCbRPKS1NZq1sQJjYGHy+wgCatsMOyjiiwjxf3Hhi6izpojtJZc.JsKysvR6yy8RWkuxW3yS+mrFO7t2hEe7Jr1SdBesu9+BpTuJSN4TbtElmKe944pW7BbwElgIBzHaNCd.djfxZvD2mj3AD2qONmC+ZJr4YsqAubmUVgUjGkTCWIhrbZNqR9NKPZ1JVHFV82rGT15FkIN1JgDGnqUm051k2612EoDt90tJiWuBljN++amlJnfBJnfBJnfBJ3GG3oD+Jyys1iuyrJBKr1i0W7TxrDPXiowuZEZIfa9vk46b66vJasIJu.txUtLekO6miIJUlFZIp9CvFGSfmBoxQhKA7Rxi+UalI45x59SDJLJMOYuVTezIXjoZvts6y6eyaw6u3hr9N6RuAQ7YdkWfomXRN2zSyTMqSfMkztcHoWKLwQfmLqve0pgTqvgfdoojBHKUJW7qXXGOCHyiwVvIkjHsnBBwuTHFsfVwQr69sXmCVgVsay+16eGrVKRgfxgUYgyMOydlYXlIlhlkqwLiNJtACv1oEI6sNshGfm0hmzQIgDUnORkDoWVEcQ.oVxxRXaJNi.oThRnQIyhQImwh0XxLT3OjNX9jJ3NTrqDmMSQqPHv4j4a4XWd9Ye9C6f3gQz6oNk+TyK7G4BfGVoySKf+XG7BnR8JYJ1cRbtrdZ2Yr3rYqzxdGb.fEoxfV6gRXIwlPOiO8Ev69F+ED2cWdkq8U3Kc4wo9kmfNehqyJ6uGOdqM426a+1z5niXus2ku8296w29a+8XjQFmYm8rL0XixuzequHMqDvD0JwndB7KUAuRwXh5gKMAgqOJmL+C84GfEl7Ud5jItVjmhzNQVnNKIyc5xhgJa1y2oOY.1yetcSLnT0Xs81j6r3CnZ857hO+Unhxizt6gzy+GgmcJnfBJnfBJnfBJ3GuvHyEAKs3xMbGAVbNABrHGNBoYluCFAXjBRE9XDVrUOCa1oE2dkGx2+luKK8jUnQy57IekONexW5FzTpv0tGwauCVqkFMpRkFiQqAcX282kpkaiPnPfDqQ.NMH0XnBQ5WA..f.PRDEDUzAX8Bw1nNqOvvpqbOd+6bGdvRKiWfOO+K7Bbsq877p0KghrYycvA6Q2nAHvRoRgTdzlzaPeD4QLabZBwIYU7LHHfxAAHOp+wBdQHwHjXjBLnvHkTZlonqIk861kM2ced71axZatIar8NzoWOtX0IXrQGg4m6rL6zSyXiTixAdnvhxkRqMWDO.eETthDYMez4p0bVG1CSyxMXCfRhQBwVGIVCotShTIsTgVHQKkHs4lYkRRk72OmPdrzlaRXFiAHe9kMxbGfN+7rPfy4wOHbhLcWmd1he1Vs9ibAvm1vqdVKp1If9QCP3.EpiylJkmFeeurgHuWaTZAJs.esFgUQThgdQQz1po096Rsp9bgIZPcDza2soj1iWZ9yvKL+Y30e0WlchfadqGv69NuO269Ojc2YWt0MuC2VH4a+c99L6LmgW7EtLu7ycEt3bmgIZTgR9MQ36f3V4dNWFmjmWliEwd5Aud36KS9GZUzCqvmr4WHkbcxHIaNm6EmRoPXu18X2s2gEN244byNCJfHiA4O3y+ETPAETPAETPAE7ScXxL84bxc7WWZ92CWlWwOQtG8HvIjDq7IRqwffkNrCu4a8l7tu+6feflu7m8Kvm3kdAFsZIrc5PqCOjlkKQyJgzuWeN5ninSbej9dTtTCbh1fvhynvhhTgBmJDqpDVuPdvZOhadu6xcW7dHkRdoqeMtwKdclYhIwSnPs8lDaRwXsH8kTsTHnjLHpOs2cOBCyJvkL28lK4G.3vZLLnaWpXCQJfToHWXujDU9ORE2e0UXkM1j6r3h730WGqEF+Lmgm6pWmomYF930Gm.OOzJGXRv1tMIGFgSlhPqnBIYJURyJRmAHc3HdBTuRErVal4VkZvHbHkBB7TDfFsuWVQKs1Lmd1XvhDoLqLsByImKyNGM7ZYETzJxliaQ9VDBNczmE68LkP9TcPbVwiGZlVm51N01eLP.7IMkf0c5cv7XDRkUh6r45Nq+wM13iENKCJiwDQrKBkCZs+QTcz4nVIM+e+a86yVq8H9096+qwKewYIPLf.kg.OMF2.5LHlZ9ATQIYgO1k3W3UtDGEAKtxV78emay8e3Rr+AGxZasK24t2i+4R3ryNC23UdYdsW8iw4O+YYTpiuFBTB7ATNKt31Xh5fINhxgZT4sDQpyQrUhwo.sBgTQfNqktgTrNOD3vZRvYSHwJnQsIYKaJei+f+XTZe9U+U+ayTgdzt6QTKrBIm1Wu+Q.+vlidE4vWAETPAETvO4wG04r6G0u9E7i2H88vjjfwDCNCZjHQhyYvZjDmZY7wlhTrrWqivJ7Q1Xbh1uOKuzi4+g+2+ei4mad9Du1qxKdtywrMpQUqCui5fzkhpYY51uK6F0GDRJUtLAdAfUfIwQ4QFg81+H5zqGUaNNMmdFNJ1v6b66x6du6w6eu6xLyMCu9m5SwEN6YXpJkoBBJc3dnQfM0fuuOpv.bJHwkY3uVkB+RgHbVDFKXrnUJzJMRk.iChMVNJwhJvCuxUQUIfAXXsc2g6s3xr1Vqy2+MeKZzXDl6ryyW3y94YtImgwp2jJ9kPoTTN8PjlHvjEKTRoIKgWcRjFG3T.RrVGNq53zqIqprJ5n6jIEV.RI36DHLNbo4tSzfjrYwUJx9Q.VoiTLXsVhD4RPEYyHpPo.kDg1CDhLS6RJvl2VymXvYY7DS1umzzTRRRHJIN6xooXcV1byMepG+P9wFAvmpmeyaK3ScWhm9gIbePE7Vm.oRiPjPp0gxqDBOMGzwxS1bGBKEx3MpREoBQbDRgCEVRiiPiklxHhMPpQgvqLUCDLxUNCWXto3v1eJt4hKw16c.KuxiYsmrJ6u+d7M+l+Q7u5a7M.f+d+s+0YzlMXtomjEldbNSSIUBaPPXMzjPzf1XONKjDnsBDRIRkORojjnTbXybGM.MJjZuLC2R6w.Tr0t6Pud8ownixD0qhOj49zx7JGWPAETPAETPAETvOiPzfHTRKdJEdRO7kBbVvZDXTRp0nJKuwiYfwxbm8BjB7Nu2c327292g6duGxm7m6SxziLFWZ7oX7xknZZJAIQHsIfHkNl9nJEvHiMJJOeroFLCrjzKBWRJqk.MG+LLx7MY+V83a+tuO279OjMOXehLF9xeou.i0rAyL1HLdkPpiE+zTBDNzNG1Z0HI2CiRbojhISvlMEgwgmul.u.7C0jDanWudj5r3EFP4R0PL4TXbvNcNhGbqGvcVdQdxNaRrMEsuOeou5Wl5kpv3UGkopMBiFVkpRO7Qhz5XfKyElE4wD0IlDVlpwffRHLJbt7HlxkYruNxJL4dC1FgPfuTgVnwSpPKknxuegRRbZJQFCQIIXkJTAJ7Bqf12i1VMJkJqPmBAolT5MHld86STbJQowzOJgNc6vAGcDGb3gb3QGQ6tcHINld3xmy2rYMVLba97Bmll9A5r3SyG8Bf+AxvJCaONmqNsJ9iur0fefGjlfM0geo53.dzl6y8e3RL53iyrSeVpnBIpWGBzdj3ooeRJROOFz4HRSMDkZvgBQoxDFTh4JUkYKUhmaxWft3X6VuFqtwlb+GsJO3gKwxKuLGr297a7a8+CUqUiyL0DYln0EWfKe9YYtomjQJ4SXv3j0v.Q3RivX6CIQHS6iVHIMMqIMLDCtDLnP4DnwQrm.zvRKsBcNrMme9E37SMNABHNNEkeIvU3CzETPAETPAETPA+rCZ.ERBjBTBPXbXMNRrNhsN1Z6MozHiQifZb.V9y91+E70+5+qXqsNjq7RuDe0W44ohWHM0AT14PGEiIMFGofzgPpPJy9F7wCFPzfAXhRPYj3oUTY9WftIIr3i2h26V2jacu6RTRLKrvbb4EVfm6RWjxZAUkB7LI3h6hMMgDmCqDjdkxboYaBBokPsDsJWjYhkJkpvftCX+18Pn8HnwT344Q23H1qy.1StOO5QOhacyaxpO9w3bVl4rSyKdommyN0TblQFiRRIkQRIglPaL5nAPZBtTClvAbrVK2v.DVkMhlBI6cXej4yfqJWfo.Ex7VQd1YOGIIILHJl3AQzIMEvgH+XGRETN.UP.AZOREPTRBcRMj1OkUSGPu983fCNf82aON7nCoS6tDEEg03HNNFfbCPN6bgPJQ3WEYffO6rSiTJwyyCesG999366iVm04s0pU6CLBpO6me9HkSU.Xr7LY3D4mSN8C5YPhEegDiSPZpkvJkniCVbkUYyM1fOwm7kY9yNMdBnSTL5ZUP34QbTaJoBPn0364QfTlmWVFLIswY5hT4iuN.sHj5084b0mmW4pywdc9jr4N6Qq1s42928eIsa2gM17wr7x2m+huSHmY5YXt4liolXD949zeJZV0mwpERcc.d5JXoKw8ZSu39Tu5jHvglnrYc1pyrRbb37BXObbuEWlz3Dt1kuDy1rAZRxbPMgG3d1gHufBJnfBJnfBJnfe5kv.Ov5vZhvll2NrVAIBMIBAkFcB7Tgr7Qawu0W6qy2669FTNrIeku7WgW4FuBMOXSTBG9tXjVKVWLNoAgTBJHzKfAIQzs6QjZLHUJBCKQXPYzZMO3vtbm6bGdua9VbXqC4LSMNexqcEt5BKvTMpgsSaBGXQ6RxD9lFgCCQJQV6LOnMRojP+LAdBD4l8q.qTxds5vfDKVolxUafq9nzNJhk1dKV6Iave7s91HERpUoL23F2fqd9yybScFpE3i1XvMnOpjD7wBVCwFCIoVvXxbFrRpicO5i6t1gs4rSPPoRHyifHo.jHv4xbUamyxCd3pn093GDfWXI7pDfPqvpTXjB5zOhDqi98hnU2cY6c2iM1Za1ZmsocqVLvSgwXxh6Vg.eOepTsJSN4jTJHjYldZzJMkBBnTXIpTpLkKWlvf.zZMUNbKTBARkBsTgRovSpNtZvwCh9.4+6PNc7L8QFmbP+DNsh8i0+JNIGmxtirV+UCHrIHrNjNINofsNHgEWYYRsob0qbYlrQC7HOTk0ZhEBRsNbNKGkJIHHfRAZzHPQJ13XrtTb1Azc+CvozH8BwyuLi4UhlU0LW4IIMcLd4q+OhM2bKt88VhacmE4gO5Ir41awiWcMbH3O+69VL0TiyycoE3Et744ByMISUqFkKWE+xV5DIPKRQIbnk1rrKy3H0IXPhfGs+9rzxOlfvvrUSR.t3tn0ZRKF+kBJnfBJnfBJnfeFCINRLIXhS..sJDuvPvKLadQA9Su06x+rei+Yr5iVge9O2Wjewu3uH9VMqszpbVa2rDXQqvIEjp.TBbHQhBbRroZzNAgUCInQcRENVu0Ar2daxu4ex6SZRDkC8309DebdoW3pL+Digqaa5u9ZTySimyhhr4fU3GPhRf0WPpRiWq1nDA3gBSrkjnThMFbdAH7CQTKfpkqQhNfM1+.V7sdSdvRqv9GbHwwFlrREN2bKv0u5UY9ollxREt9Q3Z0CkwfvZXnA65jNR7AW.3TCcV4bOcdXTJkmcxBGHDNTR0wBekXw4Rw4RIMe1amn4BYQfjmlDAbTbe1YuCY6VGxgC5xCd7JzOIhd8iXPz.rNKJkG9d930nIuPcepVtBiNxHL5HiR8pUILH.kSfDHT6mIfMeNnclD3vCNNEfz5ji0Kl3bj.DAGmMxoIImD0tC8Mpge3w8iQBfgOnaIO75CEAK3CJXVqASRDVSJBgOIFGKu1ZrxSdLMmbLt9ycEJKDHRs3AfPfCGZoCgIlDqChSwZsnkYhowYyDVCL03iSbRBQIojD0EWZDZkO9JEBsjxxRL8LywKLyb7k+4dcd756whOZUV5wqwN6dHu6a81r6FOga+tuCeyJgL8zmgm6ZWgW3ZWgYm8rrP453o7IPIPgAjfwYwjlR2DC27dKxN6rKm8bmmyuvr3QBIC5SoRUIwxGZUwKnfBJnfBJnfBJ3mVIJY.IIFbVCAkphePUL3QOii8iR42329qwMuysHNwxeue8+A7q7k9JDLvxJ2bQZjjfVXwIyLWWqHqsYc4l9j03H0ZP4Uhf5kPVpDGYS4Aq8XdyaeSdviVBonFW6hWfW95OOyM0jnMQDu6NnhinQfOpjXDjYtSBoDmViRBVq.iyQf1gSjPZpCSJXwCcPUjkqiqZY5.r7A6ycW9QbuEWj81cGBU9L6YlgolXR9L23BHEBjoNbGc.CRr3gi.jDnTPdAdSkZbRGoBGVsHK9cjBbFcVxyje7TjeYAVDNneTTV6G6oyZ8XkFiVgw5vfC+Zix9GcHO4IqvpqsFOY2s3f1GQujHLXAkDOeOJUpDSO9TLwjiwjSLISN13TobElWGgTpQKAbfMIknA8IteVqlWqbIv4xmM4rE7.IHxsO5Ts7C7Yhi0MZcTown+.+ri7GKD.ebFVk4yyeXpzO8Vqv9ThlU9JFzsGRiComhdV3Qq8X1d6M4rmaVtxElCeqAau1DJkXHKGdCEVTw84LMmDm0gwkGLyJur9WOuG26asj5jDaAqyhvj.Xgb2q9fC1fJUpRi5MXDOON2ByvqM+Tre2qxg8h39e1WgU2Zed+68PV7gqvsuyZbuGtK+w+kKQklix+gekOC0JqYxQgIZTllUpQI+x36Kv.bq6uHcFLfomYFFqpOwI6iMY.kqOIGzIlfOXNPWPAETPAETPAETvO0RpvfUZyLU1fPhvi0OrEu88Vlau7i3O5eyeNuvK+h7K+K8U4yboKRcDbua9tH1cWtwTmk06tINqCrNToCED4vZ.i0hSqwoC3Hqim7jM3tqsFKs45bXudP8o3uym3k4LSLISzrILnGGt6tjF2m5MpyDiOFas81XDxb8BRToJD.dFPY.SUEwotLCuRUAUXUhbZ1umg8Zc.e+EuMqs6lr41afmRvUt3b7wu5U45yeNlnQC1cskHdPDVqkxkJQ8wpgTHnS61r8g6SiFM.qHyeiLfTXQl6zvNmCmPmWsTGRmCIVvYy6vVIUqVlDDLPpnm0QqDCshSnaTJQoo7m9G9+IQ8GPTudXSRobfOi0nIyOx7LZ0p7RW6ZDp0T1ym.sBs.rlDRhiH8niXPbGRMVLlrw5TnjD56QkRkvuQI5zoEPd0nEBjRQ1BJj6N7oImXBvG6X7mZ6dc6b78+gYFVezK.d395ottbXkMepVd9TW7T2tTHHMMAkSl0VvFC6s+dzueOlbpIYDsFWbOLQQ36ooOlrJ7JAYbLo8GjO2.1r9bWqP3jXbVrVKRsDmShR6imThTjU5cqIkzzTtzzyRRZBC51hAwwj5RQp8XzfPFchFL+DSQKb75etu.2es84lObUt+hqxVqtE67n84+l+q9uiwGuFW8xM4kt544JW5xL+YlkQGYDbkZv5arIooFZL5H3ADE0GkIAGBRrlBAvETPAETPAETPA+LEJsGHrHkZrNAsi5wcV7g7M+i+S3u7MdK90+G9eDuxG+iyBiTkXfAQobwoOKGzIksdvCIYlZ3v.FKViAOgNKIVDJjBIoJOFXsr9AGvabq6x67vGfnTUd4W+03i8puFWZ2kIoeDsVeUBUdLU0xnU0IJtOqu1SHnVMLVHwZQ5zHEJTNOj3PHfd5NXRMfEjdZP3wNGzk27AKwsVaEVZm0Y7Ymha7o9TbsKcdN2n0oQpAWqiXicVmZAZpTN.KPbbLau8F3bNBBBYjIFk3njrDFJ23pjN0wyDqv5HVpyq1aVCiqxpCIHcXDVbBK8hLrUudr5dGwxauCOZycXi82mtc6ynm+rzbhQ3hm85rvLyxLiLBM8CIvBdww351GcTJh9QXcFL1rWq.giPg.gmOn.gnDBg.mvlILOMKRi7yqvqMWzWVjFMbmDzprbRdXbncb5.M7987epa+YyCXwA826GpIIU9C0y9GdhhCYjFkXq8tK0F8R7t8T7O4ex+8jzqK+29ew+P9zWpBA1Vjn7wgFrZDHPm2a7wxp.fvksRBBRy2luxB4yZ7vJUaECCV4gcLeJRGnro3YFtRJYUp1Hk3UIjANH0owJTjFKn0gsYi02hC1ce98t02iUWeGd3JaS+1NBGYVl7ROOye0KwYFqAe8+W+mxuzuzWjesekOGWZpwvyLfPk.OkFqIup0+Uvec4j2ec4r2ecTjCeETPAETPA+zG+v98C9gkhuewOciUnvgiTo73Vb0K0guwhxZQWuLIw8oisGINKRkDsSfWp.sSf1UlVx.5UsDq6b7a7u36vevW62jKNSS9O4e++c4m+FWDW2CYPRLAA0PnJy5qrKas71fAR06RMcYJk3A8E3b93JWmVA9rKNZGB+Euw2k2567Whmwwm5kdQ9Lu7KyLMZhMJhAcOJy3kzYlWq03vkWLMgPfwkkyuVaVN0pjjc8jX51sK8XbpO8DjzrB2dm03O+c+d7vG9.rwFZFTk+te4eYFyHXBmhJRIHFfUXHxKEmDpIalW.urBxYxEPhLyTo77kjFESR5.zBEgk7wWpyqBaLGZsH7CwUJDaPM5n7okQwQIF5af23MeaZcXKNZmsH5n1TQpY9Ilfqb1yxTiONu5jgYhREfc3V33aC33YJVle4gsyrvAoRGVatnWm63XLZ3OG6+w4++PO61zX6wuFeXnTp+Ju+Oxq.7OrHwQTbe7BpPDFd3C2fjzHl4rmgIFebf9YOPm.DYhXEm5+T8DAu.4Yg0I24oEWl0R.YSP7ISk7IOdIoxS6d0JbB3v8ZgP6gWXH9AJJG5Q4IaxH0Jyf4mFwkmmHihc6kvxqeH2e4MXyGuI27a8mw6F0hRUCoVEepFDPnVCNAFiIaOwZKFA3BJnfBJnfBJnfehBmvACEEwIETylkFOr+VqSX0PJUsD93HJNEi0AnHAGVsFc4RbqGsI+e80+84gOZU9E9peI926m+04ZmYDRGzBoUhxyGcnOR7Pq0n8xLAppAMvDaoyfD.e7pERpuGa2uCOZ+c4q86+uj5MZvKesmmKO67bgolfF9gzuUKRh5S0xgHkxr1t0cRkIytblyKqDfxKfnTCCRSIwSfesFD1bDjxw3tqtLeu+32m6s1RLPjxLSOEO2EtBWbxooRrgPDn0BDBCNg3XAlVGbPui..gLK6dkBYdqB6i.KQ8hoVs5Tt7jjjlvQGcD60sERkfff.pb1YH0A8hMryQsXkM2iEe7SXo0Vi82+Hp1rIMZzjqegyyBSOEms4nLVoJTQIyR529aergSIxeaqxcT5ePKckaniSKfJUBwZsXLlL2f1NTOUln3nnn7ES3DwwCWbAgPPRbl9M4PkPxmVfr6jgo8opR7vK+S7BfUXYvfDBpVmsih3sd62lz3HdgqdIN6j9Hh6R9ZO.N4wmrr4yAtx9L4n6oE8lWUXm3TYRLf3TGT0VKVfTU9vyiJWfb1qU0F0wklYA5l3NXDNzZGUCzznrhqM17XANxAFwN7vGtBc26IzcusQPLe5W+FbsKNGiWuLdCc7tTKZgDoPiqHGfKnfBJnfBJnfB9IHrXQJjnsVjVIh7LPMQCNgEanDcYe7DBR6mfKwhLnBDVhTmj8kZ9V+keW9Fey+P1a6c3y8JuJ+pegOMu37iS.vQGXybTYOvo7H0Xw5Ff0L.gQ.6ZHEnuuG1pkIthOaz9.dy69dbyada780bgyNCu9K7hb9ImjfzDR51BrITIzGgzKqaOsVLtzr1oVXxhXGmf5kBoa+AzIJBqW.9iMItpUX09cX682m27tuC6sy1bv96R0Rk4UN+435W5BL6niRcoh1asEkTB7TFbRIoXIAKFgDq.RqjjoiwkUAcOAnREncRvoIPGxfVIr+t6SBVDAgDN9n3UpLp.O99asGau817nkVl0WaM50sGUKUl4GaTd4YmmKb9EnVXIFuYMFoRYJq0HhhIoeGRihPJM4Kfg3opr6PbjseZjRLxLcWFg33JEGeT6rGmKazREXQJknDYsHdsJg3bVbjc+NaZl3UW1qaipMxe9t7OOkKxMeaTZBbJAyBg.jBDhLgz+Du.XIYCismXb17fM3127134K4ZO2BTFQ9ICM3xdqJsYYGrQXwH9PD.mKNF9vGZ5Lr4K3S9vsKxDIaPlMv64utRaV.MmjjfIJAqKEouDoVQhVPLVNzBqt1g7Fu06wa78daVe00nR0J7xe9Wim6hyx0uvrboYlfF9AXS6QRbBZQVRAKDhefqxRAETPAETPAETPA+3HNgEbfxJy79GxEIkKVp9HMARocmtLneB9UFA+flzyA6FEwu825Om+r+nuI19s3+feseU924m+KhNIgC1XKpDFfR6ivWhSlPTZB1HCwQ8IMIFQrit88o73iAiUgU5b.u8ctIKtxxb3AGfmVx+f+N+cotTQcDXO5P5E2GoyRoPMkB7naTJoVSl3WmI2rlxdejJf1IwX7z3UtIhJ0XfWHOd6c4Mu6c3dKtH6d39blolhO9qbCd4KdIN+3iPUmin82gdsNfZ9dnjRjZvIcXrNbNP3jnbPXnNSKX9bDmjjf0pIkLQfIFKFoBp1jxUqfwSy1GdHKsz8X8MVmu88VFOklpkBXrQmjW9piyBSMEmcxQYzpkoV4PrwCHtWOLGrCcSSQK.OohRZEXFV097NiUP9b7dRQCc40eLKZkxDqaE4FcrRj0Mq4QszIs.cVEeSSyh2pSFEBEXMYl2kC1c+8OQbqbn.W4wUBNJNN6EUlW8Xk73svOETAXoyfPoIAIO5I6wt6rKmct447yLdV1aIr3bY8SgvkkkUf83RzqNcKOe5xkKjOk3xOfXXW1bBmJ8vgCa9JWIcFDjk6WBf82dej.dAATswXXwmANGq2ZG1Yuc4+iemeG1amcX6s1hzjTN2bmkW8i+R7oe0OFWctIwiTJi.kHqO98URB7BQffjnXT5hlftfBJnfBJnfBJ3mb3ziP3vwLDxmUTAjZMYNFbpA+JMHn7nz0AeuGtBu469976968GvMdwmmekuxmlO60uFUDwjZZSPn.gO3zJbdBRSSwYLHSbDJ0T1uDVqCwYOKwAZdTmV7Wd6aya79uEoow7pW+U3yeiWkyFVAZ2kjdswlFivGvyw.Ro+ftnjkv5xLQKe.sViTJwHkD6fCShox3MPVqNacXad625M3V28AbPqNHPwuzW3ywTiLJyNxnLpuF29aS69cvyZoVnBqx.JGoRUV0MsYEVSaUHQRvACv5fTiCCYUZM0ShU6g02GU4J3TZZ0OhU1XMt+CWlkWaUZ2oKFmiqL2BL9HMYgYllElZRlrVcJILXFzES+NzY+cxLIKgCkvgVKPI8NdFcIMqCXsxmdgKrxSzUkM2uFTtLmuVaAYZ1sOPk0jxC+WmUf0Xw3b3PRbbJBgBoLyXxTJU1qqLS3b4lbb0bkRQ1iSJxh6VojJValnbmiTmEa9rFO7mehW.rR.dA9rmywMu+iv5r7bW8hL8nUHMoMZGG2VxBjHbomHl8ozNNzjqF1+5xOjJ.+zyDb1j.qI+Qm2xz1iyPK.JUqDBsOnKSWmhs51l269Ofuya91b+GrHq7vsn4nix0txk3S7JuLerW54Y9IaP.BbldnLw3D1r7HyIHLnDZsOChyGpds+eyePsfBJnfBJnfBJnfeDgTjY.sVYJY0iKSvhzAVmjjAIH0ATs4X3HjcSi369t2guwe52h29cuI+BewuB+he0uHWe5QQPWVciUndoJLxHS.3QqntYsCrMWjpeIJWsIGUxQWaDCJExady2iuys++k8dSCxxxOKyue+WNK28bOq8r1W5t58MoVpkZoVBDRiDxZkE6gwDFb.gIlOLgGO7gIBS3w9CLgCO9CLDADgUDFXDBPi.ARiPKPiPcK0c0aU2UW66YkUkYVYl27l2syx+E+gyMypZQ.xgEfkPmeUbq6dkmkadp6y488844UYstqw1mYVtmCdHdv4N.6q03zcgaRj2UTnoHMVoEi2gwa.rDH8HkdjNAZUHQ5HrJEodK88NhlcRt55s47m5M4bW3BrzsVgJgU4QO18xwO5wYOyDhvZvOLAyZcHOoGABOUpFQX0H5mkfSrYH4JJ7uHqmPOE9QTtpPfnViOPgUqvDnHUKHWnX9acCVXok37W5JL+7KPt0wLyrMdnG5gXm6gipWH...H.jDQAQ0bm7nysSbVKhrbj44XVaYFfgfQYjqTJQpJ5ZUorPijwZwXs3xyQK2T2TgHXO2IlZ2Tsj56pOUch6bhORRxPJ0H0fTEfPoQnTi74YIMmbFLnv5bja8jZ8jaMX8N7NAhPENui7bC4o4jm0mbSdQW2NZdh8NOlQo1Sd1lOWw7F+C8BfcBG5vpL+RavoN6kHLLh66XGfopEievxfRhWnAjuk9S2Swufskv2MGbagXT+ie2U.1W7AOgbK2hdyR5CFD.J6ltQVw0VIXUZTAMYcLL+J2lyckavYtvU4BW75r3hqPudC387zOE6aO6lG39uWt241NwT7AubaWFrQaDJINkDsL.sPh2KvXJDXGETJ9sjRJojRJojRJ4GtP4Jx7TqxgUV7cokNIJ6nKBMpvpj5CY9tqx27EeIdgW4jjj33Idauc9e5m6SfDvRNCGNff5MvnjrgMi.oEovfzIPZcfWhLHlbEb670Xo05yKeommKegKRm0Zyg2693oejGkCu8cfnSW137WhlUiPDJwE4Iy6H0XwggHsDsJDUtEmuvPb8nIUpH0oYUqiUyszdgE4Mu343RW9RHLNNzb6g6eeGhiO29YWSuMV6Zu9VterJPSPsIwJ7jaLLLIGuPixoPh.kygH2WLuzBGAx.5FOFn03BUjHcrdVB2t8Zrb21rQRed4W4Uwa8TIHjiezCww12AYu6XWzrRMDVOgqdMTiLNKwnBEVXJUEquUpVCGdRsVvXQJDnURBhBPKUzKs6V9pjxAgdOXDntaqTRH1p5vF0ndvUT3NyUhagWVLNmNgBiPPNdxDPlG5zYCRbdRLVFlmQ+rLFjlRlwf05XgktdwxatgbSNlrbL44XGkPNJsFg2i24wYG41zNWgChw+DnEn8REHkrz58YgarD0pDvd19LDKbj6xAUDdjHFMj3EBg2zbqJD9t4s25FaZ01ao.1+Vtufh4Vnnm1yQ5KL8J7EhsMBI4RIYp.tUmM352dEd0SeVd0W6M4V23VToRKN9wueNvANDez20CRiJRpf.APuMVByvdTuRHaarljllhVGf.EFiiAIYH7PbbL0qWm9I2InmKojRJojRJojRJ4GzQ4.6nJS4DENYrSHKFgSulbzjkGvx4C34e02fu7W6qS61qym7i+SwG+G68RfwRu9aPlOkZspSjbLr9bRS6QddJ0iiQIjX8JrhPRDArtYH2nSOt3hKxW+49Fru8rO9fOyyvCu+CxDHI+VKiNOkopWk9Y8vJ.iPfczboFP.JuDUt.uoXtbM9hpV5PxFdG2NMi0FNj+vuvmmJ0pxA16d3s+fOH26dliJ4VFr7hb8SMOSWQW79jRv4vZUX.LdANuhhQjdTK858EsAsBPAdki1xJH0JLROaLrOyu7xbgqdQtvUt.291KyC8fO.6d66ficfCxdlZaTSpv1OgrMVCSVFUCSwWXV038B7RMJcDggQHzALHKq3mq0gy6P483xK5oViGbwibvawH+C1KJZwYWQwEshBAu4JAYRI45h1zNezbAOjpjabjlkQuAcYi98Y8taP2ACYnIiKb4KiEAV.i2Vzt2ipdqCApHOJohfvPhBinZsZDGFQfN.sVQiZMPJkDHUDpCHHHfPcvVsR8VBfkR4VOn0ZIOunTwgg+fcUF6jlRsZvK+ZmhNqsNuuO4GlcMyXLb3JTMPRwHT+V6k46zZyaNyAEhi8e2uRgmjjDZTqFZslzjAjmmRTvnsSNK17A3DAXbR7AUHpxXXAluWNm+hKvm6K7GyJsWiA85xjSMAu+22yva+QdTtm8sCZJE3LoHLdb9LTdGshDnBpi.GYICQHzXMa1v.RzgEKqVqkgCG7cup82fuW43WYN6URIkTRIkTx+XS42+3GsoVbcZ2YUFhkZMpiPFvf9YHEATqdczd3rKzle++r+XdtW34Y+GZe7+vuzuDOzb6mb7n0I.oHkBTBMFeN86zgHeNS0nNBTrZ6MHSESywmfdd3ae1yyexewWgacyE4S7g9PL6XiydaNAMRyw0uOxzA3TPpzwvPKUa1.sVPutcgTKUCqREU.lLK1vXFLb.oAQHZ0f0cVN0EuHu7IeCtwUuFG6PGkG5XGk6e+GfFBOtEWhLrDpxInglbQTwbCqKlw27bKFmCgWPDAjmlQTXHAQZLXXfeHYBGxXEgwRLiOEW9hWhW4UdEt3YOCoICXmyNKOw88vL2N1A6d1YHvaKpz9Z2lTWQ9JGgiJAfOJ.PhvULhndWQ9DkaM3sNrlbjHHToPITnwizBBmCvST0J3LVrYFLdagnXY.pvHDAZ5mXPTsJpZUQEFvf7bt8FcXwUVgM51mSesqQ5vDF1uOC52uvYo8PfRgVJnQkJzLJlwqWmwaNFi0nA0qUi3nHTBIStsI15yRaFES2crLIc20i6uys270q2L6k1L+ktagv+ft3W.BZLFKNHmkt8pTsQC19jsnhRfL0fbyzOBOR+nLycSWgdTqKC2oskELp.vadLYO3xSIOSA9fhedAAnCCKBo47bVeXJSN4LnIjLOb4MR4a8xmjm6DuJW3xWBDR10dmim489LbeG8fr2YlfwC0DaxQ48nECKjf6Mn7VDd2n4ItnpxthOxcmnXZjPcDkY.bIkTRIkTRIkTxO7QuM1f.Y.ggwfPiEAA0pggP55gW4rWgu3+k+bN2Eu.uym5o4C+AeeruYmAiY.tzArgsOgQUPKBHcv.DNGMCBIRovkaXgkWjI14dQqpxk6jyW3K8U3q7U9ZznVLerO9GgcUoAiEEQCumHWJPNFsGQDPUIUCZvsWYErI4LYywoUqIvNHmtcRvacrgOhpSrcTgJN00tD+Um36v7KLO6ZW6lO5O4+L1UywYWMGmo7fo+F3RSQF3vq7jYSoRylLHMkzd8w68DEFR0fJnkphhxoio6v9zIY.AMpQkssCzZAK1YUVZwk4O4y90PKkDGn4HG5fL8XiwtlZJ11DSv3UpfLKCs2hDKRuajI85AgCm.xrRjHQJDn7Es+r25JFYZuCqwfLH.kVQTPXQA0rNr4o3LVVXg0HLNh3pUHnYC7JM8sNFlaIwjQJVV4V2fquvBL+MuEqzdcRxyQHUnzJndLZkl5MZvN291YxwFiIa0hIpWmZgADqTDIDTQoHVoKh4ImCrEswbdxfQwa6lEyrnk5k3Q3KlYZguvslDdQw5NhQZ9jnqToBNmCiwTrxOxcrtaAw+fLxfpbx27zb8qMOyL6Lbv8rSph.qwhM7NFYkXjfWuPhW3P3kHEa1.zvcx3W2a49SOw3.PRtgrrLrHv3M3QPddNUlXOz1CWbw03keiSwIN4avkt7kIIIi.cHum2ySw8r+8yideGgcUUSj.71gP5Pj3wHyQ58ENk1Vgn8lheUixQq6R.7cYvV2skbURIkTRIkTRIkTxOLPZVFiOdw2wd01afILhf50oM4bg4uJel+neeFzKkG5QdX9zejOJGbxZfyPudcXhlwjkEBBAJODffZUpfVKXX+0Yw06vjycH54C3ac9qwW7K8MX9KeE12b6kG+XGjief4HnWFANAAtTrlLLJC9.G9PIBoCoWRyvpnwSUeL4cxXigoPXH0FuEAyrMN8UtDm7MeCt57WgLyPtmCbPdfidDt28tOp6gJVvOXCX3.TZAUpVEuFbJIC7CAsmPgBEBBPfOe.IFOFqCYTEBGaLBpUiddGmY403B2XdtxMtAqztMQc6yzSNE6eW6lCrmcy1FeBppCPjmiKaHXKZfXuvUXltRF40QEWr1PTdAAil7yHgFozhW3w6EzX5oYXdJcGLj0WuM4VPHUDDUAcXDSsiCPt2xf7LtYReVdsUXw1qxxcVidC5y7W6Zfyiz4IVFx3MawriMIyN1jznVUZ0rJAAADGGS03JTILfv.UQNJKrLneeBDfT3Q4x.uEu2LxUucTYyhEJ963Z9tt1Oxk07BzooonTJzZMBg.q0xlUE168+.u.3g34UeyyxpKuLOwa6w4f6dmDK7j3YzL4B3KD5J7xM8qYXzb7tkLRgCg+Nwn7nGDm2PlSPlsHPs0pXDAgENAmvxk633a+BuLes+huNW4xmCu.1wd2CO8y7z7HG+d4QOx9HFAwVGxjAXMInvRPf.kTg2ZJFN+MWg7Ey.gGEfdjv2uq8Ahhky6NvoKojRJojRJojRJ4GFPGWgjLGICSIyInV8IX.Zd9W4D74+heIFNLmOzG7CyO469cvjHHePB9zMnYEMJukff.bFegHNkBmwRhwhKHh56X2zyGvW767c3+zezeFoCs7S799w488vOHQ8Vi1W6ZTqRCrVC4lLbNCdE3kRbNCjZPj6Y7ZiQTX.8VuGCcPzXSfoREV243a95mjy7luAyekKwLSMFu2m78v8ejCRUuiAqtBVqiT7HLF7JOQQwHBBJbwXTjmLf3vHhiCQX8jljRddNnBPDFiIJDiVxpquNm4JWiW+rWjEWsMQUqyXiuc9zuqCRbXH0iqR0f.BvCI8wjjhIOi3vBm0dz34tUY91rfZwxP.GXbXs1QZfDiDLq3525V30Z7AQnZNNpvP75.7h.LBIm5RWiM52iEW81rT6kY0MVmglgX8FjRO6Xm6jV0pyNmXJ10DSwzUaQCc.AVPXrnIqvkokRDBCjkQd+LRyRnuwfRKHSHPKU3jRjRFYZXdPVzN12osX8iLjrh0wMEH4jiT0IuqV6cz05ACFfVq2R.7lNR1lOVdd9+P+6.eewRd3LW7Z3wxQOz9Ya0CQ6SQJTEFbkeyxh6.gF+HQvBLE1u9lAx7HQk2spRGvRquApfXhqOFgxHL.ql64F2ZEVqcG9+3+qu.c2XUDBGG432G22wOBG+H6i64.ywthqSrvgvL.6v9f0QPP.Bc.NAj57HQBBIV+ntWejfWGaJ78N4h1cDqWbkW78bDfKojRJojRJojRJ4GnHrVMVu8FHjgL9jaiNd3YeoSvW8a7rbiqbM9k+U9U3s+.O.Sh.WZJt9sY7wZBZG2d0kIJHll0piTqoWuAjjmgpVE7Q0IgP9M+C9c4kd02.oNhet+4eJ9HOw8yvE6w0O0UHNIkbY.FWNVuCgpvIlkBAXs3yrDJBHqSJCMI3zwDOw3jFp4zyecd8yeN9pe6uIGX+Gf2+S+t3X6YNloRHwc6iHKk5VKduEkVgLLDiySh2yvd8wY7XyMTqB3yRHIMEuWfSoQznNhJUg3pb0atHm6pmk23zmmUVoCMZLNO789vb7iderisuC1QxEKh2mzDRS6QFfRJIHTQk3HxyyuiFAuD8lxarEWUQ6vZrXLEUUM06KLV3f.bZE1FMQWuAA0piQnocutbsEtEW8Z2fUWqMW41q.dKJjTMJjYaMNyN0gXGSNAspTg8r8YHDOAXQY8Hr43SFhIKEm0hQkiPHJxwWgF2nEVsFz5hwvUbWi.p26AuezrJ6wHJzmdmnss3FNwcKy8NO+VO1l+bZ0p0VBeMFyVBd0ZMZ0O3K.9ryuN2Z4kY7Imf64f6kXAXFLjvfpj6yGoPzgP3P5MXGkwuBWgSyYjfeS2gF2VEa0g.uPyXStcxIfbDz134ByuDuvKeRdoW404l2XQxb4rm8rGdrG6g3s+XO.GXWSRCDnXHx7NjLXChkZhiznjwXARcNxbdrNHRpFojczNY+lh1GI7k2ZaY+2LahKojRJojRJojRJ4Gdv3gnFMHLrAc7vu2e7eF+m+heQ19b6le0+0+p7tt26gzjtzYiUXxJUY7oZRZdFCGlQkZ0Iv6Qp.SdFNkfFiMKodMemKeN9peyuEO627uhG6IdG7gd++37X6eNpCzd0agzZHLLXTd9RgHUsBkRgvKPaAqWgRExPiAYsFD1pEK1e.m3jmlW4LmlkWcEdxG5943G9HbeG3PzRoIa0UHqeOpFDP8pwjZxwqkEBx7NxxL3sPfVS0J0HfAj6cfThHtF9pUXi7btwxqwhqeE91m3kHK2STPLOz8eebeG5nr+ctWpEFC4IXFrQQm5Jfv.FIjTfPUDQrdgaTJ3LRKgS9V5mTyf93QfPKIHtBtf.b5PrgZrZMVghEWuCW87WhqeiEXoktM862GioPA8AO7AnQ0ZL83iy1lXBloQKFqVMpojD48z91KhzZJZubqAszgNPQkHMZcDRsYTzDIFkauEKcRoFPPRRBEJf1boVV75250UDmR2stH+cI9EFI.9tt8ceC85quNUpTgJUpPTTDRobqyFvOL3Pem9BWgACR3fG7vru8LI3sj0e.0GebRSbEFfk..CPvnYpUfB.OXonr5L59ad5RbdIVz.gby1qyouxM3LWdAN6ktNWa9kYvvLjwSxu7O0aicuisy914tXxZ0I1XPZyf7hYJHNtJJsfbohgdGFawEjJj5QU9Uvn10VBnQLRLdgqk4GIB1BBAN+c5e+hg49eT2bWRIkTRIkTRIkTx2WXbdjAU3RarNekm8awW8u54nwDyxG8C7Q48eu2CUEP29cw6RHNJBmHmMF1EuJfwZLM31fdI8wIkDTeLV2Cm3Lmg+KO6yya75mh2+68CyG8G6o49lYZHsKCVaMFqh.6Nlkku8pTUJQKDfRV3Zv4NvJPXUHjRRDfsQMFDn3l2dAdkSeVtvktBgAQb+2+Cxu368IvkaX35qRmgIDq0L93iiDOCRSw6k3RFM1oJMQZENkm.slvvPR6ZPEGAwwzCAKb6tbwasHWdgEXk0VizTIGXO6lG9X2CGat8vDZE4azlt2ZYF1YClb+iiTHIPTDnOd7XMNxyyv47nkJ1LsaJ5v0MK9agnXmICYPH9f.xTAzEIqOLgUZmROqkW4MNEc6tAquVaxGNjwpVkCr6cxw1+AYGytMpSeBBBnZXDgRMXyHuSW5LX.lzDlb7Ivqj3Biv60XbVR8FFjlgaXBgH2pBvBg.ornTjHcEO1nfJRiZzyK25hPHXXZBveywA8uQcB+aQmj94e9mmYmcVlat4XpolZKmeNMMkrrLhii+98y3+CJ2bwEwXbL0TSSKk.SZBlrDTJEdQQYxKnnJptsr54hG0KjXgsbELvskqK6jRd0y9l7xm577W+BuFW3JKfgH18Qted+ehOHO1StSt2vDpQUBwQd+NXRynQTDQggXjR7ARFjmRpMCmPPPbEBC038E8bu2WXU0EKq5QKCElfk361Xt7NjhBQ6veq6SKojRJojRJojRJ4GXQpTrR+N7m7k+y4O5K9kY+G8A4ey+i+qXtXAqsbebxDlpVMDAJ50ecFBTowXfrJcR5QnqOcGLjwlbZfP9K+1OO+1+e+GfKnF+3e3OF+y+fuSDNn+ZKxLwZZNYSt00uEqzoMIBn9HgTN.atg7bGRif.gDuNDDJ5YMbp4uJO+IeUVZ0Nr64N.O0i+13nG5Xz4U9Kod85TOJlnPEVmmjjA3bEZnpUoFNmoHUcjAHCjjlkQRZJCFNjHQLAAUXnUxEtwM36blyvUWbIHJllMGiepelOBwNGULYjzdMVMYCpYyY1pZp1ZRtgIYjeMMZ7U8RTZEJklf.I1b6aY68VlB7nBoUsZLNU.oJAcSS3Jq1gydyE4725lrb6MvIfImdFt+G+I3P6d2rqoFiFRExjDroILqTf0lhoaeRMVDHITqoR0.zMqvvzzQUjUhWIPJ0HDZjhhpUGlKdKBaw4wZKZIaqyRXX3VFyr24HO2r05hPHJ5U5uKtawveuJPn3H+27uyG0rFSuyswgNvd33GbNN1d1NyMofwPPrn3bGjzuMIC6h2lSXX.QUiILLjACFVz1.REdu.quvBsQVnX2laKhVoM6gaawND8n3VpmxfxaHvaJr2ZzXIDKw3QgyXnQr.ex5DEpnS5.r5pnpLKm5xWheo+8eNZTKfe0eweVdOGYG35tFwBKnkXbRTZMFaFV6.PZKNSOh.blPb4dFuthjdIrVpESsIwWMlkMvIuTat3Ulmuzm+yiKOkpUfit+cw69QONOw8dX18T0IRA4Cs+su08+Wv2u4z62q2+2K9ggp7WRIkTRIkTxe+x2ue+guWT98K9m1LHH.kyi1IQYKJViS5vob3DFRSGx30afcvProFpznEcFjPOikIGe6bCD7e327yxK7beMdlm4cxO6m3ivNaTCemtTWVD8nVuiJsZgEKqzdIZUIhVgUnyRKyJhYY7YaRa77a74+K3a7m+mwt2y13+1+qd+7L26wAyhD4Ej30.U.aLqsPat8UtI1A4rdkUoltBwYZr8bXcAHZzhrlUoWTHKLXcNwq7RblW8kYrvXdOO7CwSduGm5RA8VaMb5hzkIHHfPYPQ14lYQJ0nBB31qtB0GqIUZTmbmkt82fLqg3ZwTudctZyiyq+JuBuvy8WxJKbclc757fG8f7.Gb+riwaUDiQNGZmaznQBvlyJq.SkQQWjqvUjYTx1nTBjBE862m33ZDE2.PwvLKFiGTZBzQ7RaeJt47yykeySwBW3Bj1YUFKLl8L83La8F7LO4ai.qAsyixW3kRalot.D6xwgGmGL3wJKD5JjRPHPGDVDytFOdeQ56n7hszD5bqO5XPaFyqiZw4Q07KNHdTpD4uSDMcWB9It384fQsF8nTLZTqRKUAid9hSzgezw6173d58ee6mNqtBKcwSwMdiWfSzHl8umcwwNvdXmSMAG6f6ioZVkYFebZTaBDX.rrVmUn2sWksMwT37BDNv4rEmEhQqNJoBcnZzNsh97VFnKT6qKBVYUloHGpF4ZyEY3DXGcbYop3W.DRA43vKjDToJCwyUVXAb4CXlY1OsZVEAfyYvp.munkCZu9FDWIl33p3UNxyywX7nEZBqFxM6rNiMwzT2qoKvKdlqwy9seYdoS9FrzsVjolcV12wNBO1CbLdvid.16z0ooxgvkMZ9nC96mijTRIkTRIkTRIkTxODfz42xfVKFMu63SMROLY8IXkUVjXUDUpTiNCRHrVSFWVgE62l+29M+7jmzmOvO1Gf2269IXmMll.FBAZzJEdumZwUY40WkLSNyN01.7r15qQbqIX7JM4Ut3B768m9k4pKbCd7G8g488TuMdriLGwBAVS3HCAVfWBBgCuvfW3vIsTMOj.TXsdx0Rp1ZBBlXRtd204LW7J7k95eUp2nJ22QOFGYm6lCMyLHsNRGlhv5ndy5jllRZRFI9Tj.dm.knXTHO3AOHqr1pr3MWBuVSiIGinJwrwfdrvBKyezW32BLVFqdDG9QeD16rSwdmYRltdUpJjj6yJzFgDDEB4JpnZwF4zA8KRwGoBUPvn7usPrnw4odswYXpgtc1.UTEjwUXnzvsWaEVY0174+CecBTJpDExtmdB1wgOH6Y1oX6SLNSVKFRRFo10sU5AMx.lAOLzY2JSeURIZsZKGKxKfNsaiTJQq0EEJEv3b3LN7NGspUEmyg05w3JxvWgRgJL.oTxf7bPnJV8Ug3CcLZ.VQ.LHnnT1do3tZkZ0VyBsREf26JRWmQIajywVUUV+u4m+mjas3hblydNN24OO2XgE4bm+LbpScR7dOG9dNFyNyjbjCtON1g2O6cmyx3xHhZtSpzbm3FtNJoBoDTJG3rHbFTJPJrLLYHd.CBDREnBwZDjmXwXrLdbkhDu0oPhCmPfUHKbkYAnjdxc4n0fQ3HUpHlJz1lyKe1yi2zmit+cwNlrUgat4xwpzj6DDFno1XSgyYnW5PxMIX8FzpXjgRjpHVerswU2vyEt343EewWhW60eM1X80YrIFiG336im9odRNzb6jis+4XLjDIxwj1AedBARAYhRAvkTRIkTRIkTRI+nCAtQiMn.Lp6LtgRuCkqHpaln1jHjZ5kmSttJJYUN6xKvuy+oeeN6abN9P+3e.9je3OD6pQDABne29f2PJV.GtgFj4FlnZCzDvxc5vPaHSUYRNwxavm6K+mxIesSvS7XOJ+7erOHGYpIog.RWuCAhHDJIFgCqPTH9UlgUmfUkwNL0oaRFckfuYCVuolEW+F7Bu9I40O4aPipM4n6Y+7nG9nr6wagNIEa2tHDvX0ZPtWMRTsGmvhJPQnVWHTz4Y4ktENuhwaMAx5sXfRvkWbYd0ycVtz0uBc1nG6Z6am6+fGhis64X6UqSr0heXebICHVKviDiDbxhQ3byHMBfVQw3rfyXwmlgyTH.2hGmPiKVhHpI5HMsSS3Z25Fb94uJW4F2f1cViCGuMFqdS1w12F6bmyxzSMF0qTAszCtbRRri1+VDarBOHcfxKAOzKH.kPtkHbI.aVsVmglUpgRUzV1RoDmvi06vXL3rB5r9nNnUHPnBvKUEt9rSTLop5JXkiLQLj3vhWH2Z7OWLThy4G0954EE3LOeTNAC850aTqgCN2lsIteqNSQe+STk6ch8y63d1Gq16o3pKtBm8xWkydoKyhKcat5UuBW4JWhW7EOAsZ0j41yd39u2ixwO1wX26bFhsgDpzTUGhBKdaRwBwf9fIm5MJpLaEY.53HDDfEXXtgzjrQqFEBdcnvJjE6fG8LJkjbSNJs.qQfWFfEM2b4U4Tm4ZTKRxwOz9Xx.A4C5BdCdnvQwjU.gjjTCo4VBipQyps.TzYPF23ls4qubeN0IeMd4m+axsm+ZLwziy67IebdOO0amicv8xNpVGENThLrYCYP5vh7GSIKbprxN7ojRJojRJojRJ4GgP4JhOTixiW3wKLEhesfxIo2Z8o0DSROiEQbUBUZdkqcU9c+b+g7pu1qwO2OyOCuyG6wYOiD+1doEQIrDVO.gzQRRBYVX7VSiGE2tcOpL133.dtyeQ90+s+LLViF7o+jeT9m8zuK1eiVnxxn+58HRGgWHvo.Go.djdORgAIFPjSRWChJwTcplzqhlW8ZWjm6kdIVY4aSUcLexOvGjoqTmIBiHHOmzA8vkmgJL.kRQ698Qo0DGGiCaQEHkTz1u.aLHgolYaD2ZRtY6NbhSeNd8KdAVY3.PEwG5ceeLYyVryIllICqPbVFLX.1gI3cNTphJX6uqKal4s.DkWL2wY4dxsEsfrLJFYXLhnP1HMmk2Xctv0uJm9RWjEt8hHizr8cuKNzgOLe78dODDDPTb.ZsDmOGsy2sJ...B.IQTPTkj9cn+fdjkLjlMpUH5khKEo0pDKNP.pvnQhK8jmaQ38fwAtBgl0pEiw6HK0hwlQt2VzNyTbIp41QoTDDEhJPiEAYVC4lbxrFDAJRyyYPZB8RFPu98n2fALLYHFigSdiEv6AuyNpUqsXMV7VO37ibHaABDiJTqDkPgTJPhDc55KhNLf5QwzndM14Aqy8efcyZu8GlMFZ3EdkWkaszJboKcYVX9awKbiWgS9ZmmIl76PqVs3i8QdFZTuNSOwXL4XUnlpJg0pQPsLTtLLlTvkWrvMHg.cNJojZdGUCEzCAiVTJ1PJj3GY60hQmwALfTnwa8nU0wAb4KsJKeyDN7QmkCuqcPn.5mkPjzVzF.nv6SoW2MPniowXSgPnXHvM6lxIdkyv4N6k3q7B+0njRlZpw4odpOIuiG8A49O3bLQPDJFRjXHlz9jljfRHHLH.spJV7jXKU+VRIkTRIkTRIk7iVH7tQilZw7gJ8NTdP4.sShQFwRq1Ce85DDo4T2ZI9M+L+Nrzhqvm3m9miOwG7ootLDgyxFqsJRaJ0aTk3pQXnXdOCCqhwCqztGhn5XAdsKcS9e4W++SpUqFumm7swm588zzR3Hs8xzesNzr9DnaVi9CSvqjXICovfx6P4J7aHsSRacHQsZQ2.Im7xWjm6UeQVckU49NvQ4odfGgo0UnRRFpgo3wfVJwEqIECIC6PlyRs.EAJEoCyHKMETRPpIWHYlidDVtyFb1S9p7Zm9brvsVlJUGmG8ddLNzgNL2y3IfwhK0fqaGxxK19oiKFUzbuAmj2RakK7fXTeFaWMAYTDQ0phpQLcCjz14nsIiAY8367xuDqrxJz91KSfWxw16A43G5vbv8LGS1bLT8tE3MXF1Gq2hz6Hz6IVJPTsJdSgoS4QhUBHDjosihVHAwBONuqX7WsfvJPIDnzE426Zq0CgThJJBcPUhCzEykqRhPJX4pUIO2vfA8nyZav5c5v5cVmM50kzzTtx0tJ.3D9QmTA6VsuL.iEzBsNjv.MUpTgZUpPbTLwggnkJBCBPh.kTfRHQqjHEZTJ.TnGuQUxyyIq+FjwFHzAzLHhVMpfqQ.G5C7dY8gVt4sWiqb8E4BW95bwKect4B2haM+h7e3FWhlsZxN24NY+6cNN3b6h8tqcxNmtNMkgHCqiDONSexSFR+gEUPMRqHHH.BtSUT2xgx.j3PRgkjqnvcy7.hfXVnsiydgEH2VgG9vGhs2pJZ.gyRXkPr3wKTLzXo4XyhCnStgqdqE30tvk4Ud8ywYO20XkkVkct+I4XG6H7NeaONO38bHlTTX711r0wjLDOVBjRpFFN5LenHyIvIjHTRD1MqhcIkTRIkTRIkTRI+Se1RXlXjAI4AsCjNMNjnpTiJggjqE7xW8F7a7a8awRKtLepO0OEexOv6lVXvY5gIKmpUBHnQ.JgmjgIj4LTsVSRsvZ8FPv3iiUq4O9YeA9S9JeUBhpwu3O8mhG93GkVBHqeGxR5yLyLC53lrwfg3Bjf1gyZQLpMdkdI3CAuknCsGleoE4adhSvqclWm5MavG3odO7v66vLFZRWdMzRPnkHTfWZwMxAnjHQjmU7uqKGqIC7RBipguREbJIuw0uFu1YOGuwYNKlb3HG3v7PG6AYOytShTgTc3FjkmSdZFBOnFMqrVumLWNnDa0joakkud2n4BFb0GCWXD4wArg2x7qtJm6V2jKdy441sWkzzTlc5o4c9NdmbOysW12DyPcOjuQWxu4Rjp6.dORO2opnZIAph1Zd3vb1rCcKbN5h3hxi.AdDYVj9Qt9jPhHRiToQnzfLj3p0woj3DRF5kLLOk9CFP+ACIIMiW8lWkjjD50aC52qGooof2iPVDOsSN8TDDnnR0pTudcZznA0aTipUqRnNfYcw2oEr0ZB0Ehd0REJgDO1QER0heTEgws4FOOZqThWV7l0NKZbHMofMEqWQMU.SUsF6atY3w1yLb6G493p2Zct1B2hUVac9bew+PVckUXkkVgS85mlViON6bm6l8u24XaSMIG6P6kIZTgoqWi50qS.ND4CPKbDnjDm7VywoM+nEBC3cnrFD3w6s3TA3TBt1Js4zW85D1pEOwg2KiofgarJRDHhqQpwRlLjgFACLNl+lKwq8lmgW5juNWd9aPlCZN4rr6Cd.9k+nuclb7IXhwZQfKmg8ayvrLZDWgVMZgwXPf.m2Qp0Ql0hW3PoCQqCKE.WRIkTRIkTRIk7iTXUuUgupQhewKIWFRVTH4JAu4sVge+uvmmkVbQ9ve3eR9jef2MUQfsamBSwMThJPheTFwZSyw4DjWQvfbKoQQzwlw29ENAe1+f+.pVqA+q9W9qv6cOSPfRhoWaxSSoVqIPUsAoHn8F8oZbLRoC7F.ANgFCwX70HGGuZ6k3MO2qyouvYoQTMdWG+Q3oN7CPCigUmeAlZ7lf.REFR8YjYxPHg3fPh0AXMcQXSwhFgTSXsJ3iqwxICX905ve9y8rjCL1rSwQ12A4gNvQY2MFG258o6MtF40KNIBZgFB.iRPF4jaLXc4DqitioSAH7a55wEWu7rSvFc6xsVbAt4sWhkV81rd2tjjkPrA9HuuOHyLVS19XiQUuGS6UYid8Hx4nlRinZMfQ4Grygw6XfyhylgK2QPfdqVfV3jnbfx6QM5Le3sJDJENkBeflTgfg3XnImDeFNshM5mwJc2fkWcctc6Uo8FcXvfhQkslICsVSbXDaehwYrVsX5ImjwZMF0pDQ8J0PgGoTRfTgRoPIAkThDAU6mTrraSwlN.aeCYVKoiLj4vvfBQu9QsH8VFgkonZ5qMbYuvSQqAfDEfv5HOKm7bCBUHnBvGDAgUvoz3Ta5EzvpdOWY9U3EOwqxKehWg4u9MHO2RsliQilMXhwFiCcf8yi9f2CGYe6hwpJPZbHc4HENZZSJFt5M+MJubzscHvgvlCROVolgpHLQs3q7xWl+ielOODTkO6+5OL6ZaaiUWdIzwQTcrIniywPQ.cyf+7ux2fKeoqvoO8oXs0tMSt8o4wexmj26648xQ19TrC+vh730VzSAJgrvLtbtsbKLiwPlyhJHfvnHz5.LNK4YVBE2IWp9+KTFCRkTRIkTRIk7O1TFCRk78C4JCRf.KHsRTthoEMWFRhJDerlW8Z2fOyu2uKW7RWjO5G4ixOyO4GhlHX8U6xz5TDZAYRC4tT7dOwAQTInJJULyu7ZzZ5sQe77e9a70426282kYlYa7K+e2+87vGZ+LY+UIO2hQBpfHLdHwXPp0TqVUFl0Gozg0ZQHBP3hY4asN23ZqQ5PK+69S9cvklxwl6.7S71eJNxTyR+asH9davTi0hD2PRbYjJJZ0akRgBAhbKXr30dBBhvffTmBiNl0Rx3UO243kN8aR+jDt266d4we3GhcOwDH61Ge6NzDAiWsNcRFfrPQGlQ+wiqXVUUJ7tM8TYI3kibA5hbKFgjyDVm4u9037m9zbyqeCBDRN792GOx8c+b34lCoIG6fdj2uOAdCUCBIRKP3rXyMXj4.fWVX5T9QwWjPV31zV6nJnh.0HmeV3kHcEKSaHqfJPiJLBiBFXMzdPOZ2uGajNjuyIdQ5mlR2d8IKOifnJLwjSxTyLCsZ1hGc1YINLhpUqRspUINbznvZLf0gKuvErcNCRaQaPKbEBZ8dPlmUT3S0n8MZAJUwr9JDBRSGh3tysI17XdEaWEcRVvuY7CgWTzd.nwuo0aycZyAuvMp4yG4JX.tvowifgVOq1cHW752hW+bWf23zWjadqkXvvDbNKAAZlXpI3fGXeb+2+8x8dOGlYZFvtkFb1hcFdiEgyRfTPnRQnDLYCHnQEVZ4UHZxYYnrN+Z+FeF95O6I38+o9ul+suu8PkZ0INpJ49P5fmKrRO9ZeqSv294eYtwEuB0qWk8rmswi7vGkm3QNFGXtsQQiYmw38r20vkKGMj2at44Nq6a1z82c6G.Eyr7eW7OzBbK+OXJojRJojR9QO9GZAreun76e7i13BxQZgHmh.QD3zj5UzWIIQqXgt83e6u1+yzt8J7K7K7KvO163cPMDnLYLrSaFONhnpQrRuUH2YX5wlFCV5r9.TA0ItZCVw3429y96wW9K+k4gerGfeg+E+K3PSLKIIcnxFKS0p0wGUmbmGmWfVIPasjmODSVJ0GebFXEzMSRX0pr3sy3K8k+Z70+p+EHZEwiceO.OwguGlRGR3fADhCk1StOma2aUFelIILJhNquNoc6SUYDshqhFEqjkC5.pN8jLLPy29Mdc9F+0OGc6Ojcr8cwO8G6iSnwRj0PnMmHWFJeFJWgWEYpzh7zLxyRvaxQJgPsFsViPoPGnY8dCXf0gtVcp1ZL5ma352ZQV3l2jO6y9WwDSLN6cm6gir64XuSMMSWsF08PrGzVCXyvZM3DNDABD5hL6048zLJhrrTRSxwasHEJTp.DhBeXpRs5jaMjjmSlwgSJHHHDckHBBh31SOCq2tMW+5WmqbkqvMu47zciMvYMnjPyp0nUilriomlcN61YaiOAspUmv.UQd.2o2leRZTUPKzWIXT7ZI7iZg4B8Wao1Zz8SUphWmby78s33QhQhzbd6lugh+V7VOdkn2fE7dDiLeJEdz3QheKe+ZTkXwgzaPRNBrHwhv6HwDgJrBpnpfHhgdniwys6lPmdo7W97u.Kc6U45yeCVYkkwZMTuUKlXpInQyl7wdmGmIZMF6XlswzMaPHfVTjuUo82.rYDFoYfwPqoliEyM7q8+9uAuw4mmO9O2OO+Ke22CVftIIb9qs.e6ScFdoW+bL+MuMC6mvC8vOJGZu6lG99NHGc+aiwCDDRezttHr4nSaLZCyctrYVaU73Eq6vHwuPQOpSQaIjK+6NFjJE.WRIkTRIkTxeeSo.3R9+OIyrAiUeLbIdZuVWjAMnwLiSaObs05v+q+6+0Ab7Ne7Giehm9oYlZUPYSnQbLABGduh1sWEivR8l0.ohrbPEVEHjq1tCe1+vu.u7abRtuG337Q9fueN1N1MAzidsWk8Lw33PRhWQlwULOqVKA1DvZnZqIXoU6POpPqIFiUx8749i9p7r+EecLY47w9w9.rqollsEWE0fg3Fzmn.El.KCsITexVztyZXyrLdslTKHlzdCIMIGgPQ3N1GcGlvEWbddsKbNtxstAAwQbjCcTN5b6isUsIw4YDasnc4H84fzfUZ.AzMSPnNfZAQDoUHbNrY4LLKiDWNcyrzblYnxzSw5CS4bW45b5yeAVXwkXP+D1yg2OspUiYGab1wXiyjUhoFBByyPjkixkWXyvJf.Enji5fWONDjr1JDGWg5UpgVEfvIHOyRlwg0Cq0sKQ0aQ8wGmvF0YHFVY8NbqauLs61g+zW7kFUsZAgp.pVMhwaTmIazjFUpxg18tHVJohRSMohPg.k2gvZKhKoz7QeR5tqPqazcc24CZh651id8ROXhqAilJYuuvYoA2Vy46V0mbq2+a83UZgMFDij5JjaUJb2VU40i.OJJ1f.Ek+1AHQQ9vdXRFfPuApfXBpTkIUZZNd.4iEx89o+w41CR4hWcAN2ktLW3JWmqc844pm+rLnaeN+YNISN4Tru8teNxAOHGdt4XWyNESUqJUqTiPAjZMD3g0cd9VuvqwUtx7r68rGdWuiiw08dN+EtNuzK9x7lm5Lb8adCbH3.G5nbeG+37jOz8y1mrE6nQ.Q.d1.aRGBbVh0R5ptqpbu0FIyHAvN1Jwo7fDwnsehQsiPIkTRIkTRIkTRI+nEMBhQKzLvZHEMA0pRWObxqs.esm8av0u5k3m8S+o4m8C+QXRshdquLNSBohTrZEIVMFojIp2D7RVY01T6+G168LHK457LMeNmS5t9x2dG5tA5tAPCaC.AGMf.DjhBFZEoLTtQiFiLSDJ1Py9iMhc2X2HzF6tZFIMSDqlYjkhTTjDfdCHA.oH.AgQMZ3Zuu7955Sywr+HuU2MnbwDC2.wrHeh3FYUcUUeuYlmJq6a98889NxX3Hfy2bU9K9reddsW8M351294W9C+QY6CUCsoEBaOZT0iDTjjoQas346gzYQXR.xP56wpMaSoQ1HVDb7Ez709lOAO824ayP074C7f2O29H6fPkBR5SRbeTdBjUTnTBjoJLoYDZ8vW3SIqGl9Fh0NbUqR4gFh4kk4Hm4T7CewmikWYQ1512B2xMb8r+ctSFMJByJqQfQiuMuPZFYddDmI8wJf.YF9NCjlfMKWak1XH04HVnn9N1HcDJN4TSwQO8Y3Xm3rzucBiM5Druse.tu8rEB88HJJff.ERgEWVJohdnkYHkN787yMFJgBiAbo4swrGR1xF2DoooD2OgNo8QJ8PpBP3EfTEvlFayzQqYldcXlomlKr37L4ByyBKuHs60gsVoAUJWgQGpAiOxnLwHCwnMpyvUqRsvHLw8.cFVcFlrdjlkdIioB.+nR+8rpxdI8Wv5cg6e+2ns.j4cqr0MvfydyZmWezVu7TS+lQzoU67TkRj+jZk17PGV.tA+uHWWX3kbgLtzfYGEpPqGLz1FctcYiCsygwIoZiwvQDZfDfURcbwomiKL0LrzxKyW5I9djFGSVRBdRISLwXbM6Y2bs6aOr8MNAaeKalvn.pE.cA9S9q9F7U+JeMt5a3V3W3e9u.esuxWjyd7SyLm47D36wUeM6kCcnaja6lOH6drFTl7xomzaUxR5hfLhTBJI.ejzNrQ9Ah0qx65149Ud.2keVHu5uqWY7ACi9em6LwOxA3hJ.WPAETPAETvOlonBvE7VIUDFZ2MlTQHkGYBRbvQmeU9xOw2fevy784C8v+T79t26gcUqFIsVFgIE+.IcMInwheoF3K8QjZnSmd3WpNQUqvqL0T7o9beddsW+M39uu6me1G9QXiUBnyZKhzDS0giPqiINsLFiAu.ehBC.SLIw8QhAuvJzJSQT4g4kN2p7G+m8o4Bm8Lbvq6p4dOz0yt17FPcxkvSpPZMXbZjAR7J6SpyP+jXbYZpGUEOsf1chIAGdC0fzJAzIKiu0KdNN6oOEo86xAOvd4PW+0wXUKgWbe7yxHzYwyZPLPTmVBFY9Vq.FQJneuXR5kfyA9gkvqRYzAAD66wb86xQO+43UN1wYwkWk5UGg8sm8y0tm8m20rKdNPAhAOLVMZqlLSdzy56mmWwJgBaFXxLXyxcvZoTREkgdwIjnM3UtLUFZXTgkoSlll8S37yMKyu5ZbwYlgYWbI5qSnT0JLxXiRs504cr6cPne.UhJQjmBOq.kwfynwyAXL3LFXP7EIkjeyE7yi4ot5r+AVYY+GPxaNq+0jlfqb7dw4txVmNedfEWQtI+id0RQy1YNgavr8JzHPm6.yXtT+T6vgEENgDG93PcIgfhf7CzBWFJAD3ovCAHxcaqkWZUbJOj9QnBJiJnDV7IyAFikCOaJyLyLbridTNwwONyOyEIKMgf.IAggrscsS19t1K2963c.dA7k+xeUd4m64IpZEFaSajSc3mmgFaibMW0Uwsb8WK2wMte16FaP.PbmEX7pkHsWG5mlfzSgekpnDdj1Ig9c5h2n42AhKUobG4heW+f55V785yC8aZqDI+i6BzEBfKnfBJnfBJ3G2TH.tf2Jw0uGq0oOU2vVPFTlSuZa9q+xeUlbpIYKaXB9M9U9jLpPPuUlCc21rgMLNnjrVRW7KUFkHj9o8n0pcnd8IHHJhW4rSwe0W9w4vuxKy649dm7S+nOJ6rZM5rxRXS5v3i1.izxZcZQkRSjKrZP33lk0mXiAWP.hv5nIfu6gec9Te5u.KM6h7vuuGjOzCdeH6rLG6HGlMJFlff.TJAFqgT2.SgxYwYbTKrBlTHNMkTkB0viPOeEG47mlW9nuNmepUYKSrAtsq653Vtl8QcbzZtYfjtznRYbtLPXwnthQqzcY8DCglzLCo3PTpLDUgtNGy2tEKzsKOw266Rp0PXPI19V2FW6tuF193ajPDj1sGU0K.jOBrBg.0kj34.D344iw4v4DjosXr.BIRkedx+rbGTQkvqdUrUKwZtTN6hKxwm77L0hyyEm5h3qBnbXIFo1Prsw1H6biahsNwFX3p0oUqyfuRM3lH.jYxeXymkWeY.Jg.gPfSHfAcX75yraht+aZ8zkuZljejw08M84q+8kZE4Ejcvyw5OPXQfhzzz2rnW2a95khU5F6xkyYQfNO6cQif7gGV5xmBXmH+jVdsQ8vlm7tzJc077qRBg9JB8xCcXo0hQmQPP.YYZhSyHyLHGoj9WROtr933.ZacL6rc3Tm6TbrSdBNy4NKKtxJr3hqfJrLCuwsiwJXwolBgTQsgpSR+9b62193Vtgaha+5ud1PME9FHRZQj1FcmUoRnBoPhy2CimOFoBiQhK0.ZG1RYCV.IF3vYfzJGruutr30sd7KeLzM3O7HuzPV+2OEBfKnfBJnfBJ3G2TH.tf2RQaQDVBqeEloWLe5u3Wg+lu+Ofa9ltQ9m+y+yw3QR70cIqyZD4Iob0ZrV2tzMyvXCuQZGOOwYNBJML9pRbhYWle2+u92wzyMCe3OxivC89ue1neD1z0HdkkYjgx6Xy1wwTqwP4wvCVroIjo0H7UHJUgNDPafu724o4Iexml9s5wm7m9mlG8dtM5txZbhW7EPjFSU+gPoTfTPlNizjTv5nbXIJGVldcSIVaPToN9MZvbw83EO1w4vm7nrvRKwceC2H6+p1KW0l1Dd8iQ2pEQNMUB7QHMjYyvLnpuqW7LgSlazSNI9Y8QEUFYsZjDDvbc6xwu3z7Zm7jL4zyQ4nRrisrENvt2CW0l2L0C8gjXR52AcRBUJMnchGzct4FHkD0.8ZNjXLFxbFPHP5EfLv6RBPqTZSDmkwbqsBG+Bmi237mgoWddhQiJvmctycwv0qwVGaL1xvix3kqRjUfHNEmNkDyJCpvr7RiHpTHxirHoGcZ0NuspkRDJIVWtSNaLFbVvqrBFjovqKq8xBcurHXg6xMv7UJDVqt7GKFHvVf5MU8X2UbMxezFgVrRxxW960s9Sj.gac22ZvKhAAgb9O0ka6WQoR3LVLNykThacZzZMFigf.uK8hac6oFxsWasVSbhkRkqPPPUL.ZGrXRKldokYw05wgeiSwYN+rL8zKyhysDo8iYu6a+bna9lnQsp7QeWaggKODkDgXRMzbs0.ilFMpP4xgzqSavCzRI8MFhMZjVnpe.k7Couq05C.b99p06RGruz9NLnp3qu0N3rfEKg7OFEBfKnfBJnfBJ3G2TH.tf2RIHDkpBS2qKe1u72fm6ENBaaqakO36+8yseMam31snhuivf7XDJNSS29FbVeJWpNMacFpM5DnkivqO8j7W8E+l7pG403dtq6j+k+BeLFBAVaKbosnruOBgh1siQ3BYzgGm05tHdBItzTrVnT8QIUEvqN6x7RG8D7E9hOFaa6alep2y8x68ltUpJr7puvOj4O+Tr6csaVdk1WZMrP.hLGJqfJAkIpTMt3hKfnRcRqUgYa0gCeriwoN+4ob4FrycdU7g26lwySQ+t8nS6lD5GvniLLRIzpUKT9qqkfAOG1KUnM.LnvEDRSAb1kWliN8LL47KQR+L7sd7P268wFJUlIhBHxkQRxZjY5feIEQk7nelL+0uMW3KVARTHbq6JxRR0oXbFT9RBB8whit86Pud83HdCypKsJSM4jL+TyfoWelXjQXe6Z2riMsI13HCQjBJ4oHPZPZMn08IMoG5rTp4JcIgmFmCiMC6fJ7JUxb2rVHxy5YgL+XrK+5FRfd1TV2DqVW.r7JpRq6GoHj+nUF14Iuj4WYMVrNW9qCbXERhJUZPp9H.gXPwbuzSEhkRm0c4xBqP3VW.nLuRnNY9fkOvEnYfqHKGTg3LYHVqEs0lui5oPJUXG3JWFmYPN6lKfz2i79RecaqtWezNGoNCoRINOeHpLNQDZBnKvLqYX5EZxryrDMWbM19l1D29srMlnDLFySmU6hMAFpxn3WpNYoYjj0aP41s3jBx7T37T3jBDNCdYFDZMN+7.xV3Dfyaf.3Ah1uRw+rtv2TPXxefECU+G85CEBfKnfBJnfBJ3G2TH.tf2JoYlgx0Fiu4O3Y4+vezeJ0FYB9c9s+c3fatN8ZmPIWOJEXAOKch6QlEZTeCHEQzZ09zndaDp57xKLG+e9e7OliepI4i+I+U3m49uy7dLMoM1dqvHC4SfLf1wwTNZDTTh4ldQJOjiPOeToYXs9DzXDlqsl+xu0Swe0i+kXW66p3S9y+w4129VvmdXVdQBRcrvzKv4N84o9DSLHifE3K7yma09FrYNP5inVM5EDvquvb78doWhyN4LrksrCt667cvAOv0ylOyKPu39XbBBJGAJORxRujobItjuIYGrcPm0N33WleEZllxQmeFd9ieBNwLyP0Fiwsea2C20AuUJ2IkvlcIneSJIRIHTC9ozw0g3rdP3D.q6KSdfShB0f+MIgggjj1mTql.eIpPEww8X9klikWdY9iO6hXR0DnBX6aXqbv8rWtlMsMFKHD+zLJgEQZeLY8Pm0GqHFoxgz2gxSRvpg4Z97T3DBxD5bwmJGRkjTSV90HLVb1bk+dNABgCoPfQo9QVQIdSa+6S.7UhUjeMH8fBplZLjYLj4bXbVN64uHVYtUNaGTEdm.V+xVhl8Vt3JXuERg.2BJnfBJnf29Qwe+uf2JISYQhEk0hzJ.TXH.srDNDjkEyH0JQRmEItaSp0nAFmjlIVJWYXV1Kfm4Hmi+n+y+gLbsH9e9252fqaiajtKs.kB7YMcWFcjIPfklqsL9FCd9dzQjgSJnS4swqe9E3O8O4SyBSMGevG5CvG48e2LZf.SqEHJxj2xrBObRebZI1zLBcZB77Pjlfr9vLqC54UlYyf+K+m9y3Mdgmm64ltE9s+U9YY3.AoIqfxkOKsyO8BL4YmBgUgpdC5tZK7rFFNpF3brZ2DRiBPMwDzsTI9Re6uCuzK8BL9Piw8bCGjqaialQAhxrztRS7DJB7pRnWHtLGY8yv5zDnrmdOnA..f.PRDEDUjjRFRUda3psBzFINW.BuPTpP9ptHNwg+a4hu9KwXdv8cv8yOw0rap6IIt4ZfRgLHDuRkwo7PaAs0fypQHDXrwnPfxIPjYwocHPAQgPXHKzuGAiLLlZ0X1lMycR5ScZVboEHMIkehQBXGaaar6ctGlXjwwyoHqeJj4vWLn3mNKdJI99Bj9fCCIYwn0onBFBm0h0pQg.Om.oCLlLrZC9dd4UnUjWUVG17sCDtlj5gme.9Qg4QekuOoHHFIwNPUsNcLFVoWeVtSWVrcSVsYaZ2uCYYZlY14FXRw466XyGsUgMCgwhSqQXsHb5ARpGLxpCJir2aI+VWAETPAETPAETPAE7VBWwjUl2CvWZ7+rf.FpVE5zdEBUBFeCaj9IIzIVSTogw2Oju+gOCe1OyeNadhw3W9m6iylGeBVX4kohRhLzmwqOAytzzHcv3CMBlLMIVMQUpgEIuxRc4K93eIVY9Y48d+uKdeu66hg7EXRiwXxvnkfH2.dwk2tqBoDDRDdRZ2UBIZ7K2fyL0L7W74dLldxo4m5m7mjG5c9Nx8zHQtQ1ZymQSPj6iQBmjElbZ1xl2LJKL4zy.Be1v11Edg9bt4Whu1eyeCK2oKaahsv0e06k8sycwXkJQTu9nDoTOZHxR0j1OgTaBBmDqw.VCYBAggAzscOhMZJUcHJO7Xj3jL4rKxzydZd5W8jLT0pbaW20w927FYuiODQgd3RiQFDQPoHLFK852G8U3MuBF33w1XjddfxG7UnEPr1QZbBIoZ1w92GuwYOGO228Y3Dm6rzOMigFcTN3d2OCO7HbWi5QkRkoRopnPRb+XbYY4yQrmOdRObFCZcFo80X5mxU1C2wwqj24uBINOOP5izAoNGZKXEJrBwkE8JDHTxb2YVpnwVGmzDMs61gUWcMVoYKVoUKZ0sG8xzbjW+0wpTfmBTdH78QHUHTRDBEiM5vnjR7U9DE3SI+PJ44QnmGABAaeqa4Jp9t8uSjJUH.tfBJnfBJnfBJnf2VQt293PMvffFH.FCRmEoygzlRPo.zFG8zVpMzFnuSwgOwE3O9O9OfR997AefGhehcsKR62G+PE0pVgEVYFFObDrIILb8gHzOhYVaEjUphG0X5NKxe9exWj4ldRdm28swm3QeOrwPI5r134z3GEg0BH7.GXr4y4INMZQ9bi1e7MgF3DSNGepO8mgW9EeAd3G9Q3W3i7SxPHHt6Z.4YtqvkWcaqK.myCKJ1b0J3mYnUZJ5fJDM7HztTEdkSbRd5u+yxxqrF6ZmamaX+6iqdGamQh7PkzmLcLIlTp1uAnSwpyiPVkuD+HEBiCq1fSaoV0QndXYRj9bg05vQu3E3Hm7zL4rSydqON6ciix0eMWCabjgQllP+d8PifvxMHCCYtDLCDwE54imRA1byjZGisIVbkUYoVsP6GQowFmfxUnSuLVtSGd7+7+RVY0lztUaJGDx0rycy926UyN2xloQs5znyExqvdRBYYYPlAI4iopTIHyj.BI3ayiBViO.344gumGkUNRzYDGmRlVSpxhzK.QPETU7Xkt8QFFhWPIj9AXDBhSRoebBoIZdpm+6RVZJ8i6Rud8HMMFSlFANjBXKaeyD4EPsnPpWtLCWqNMJWgpQkIv2GqLYf.XO7U9D34O3XTdLOEGGCLPxqHOjachKaFzEBfKnfBJnfBJnfBJ3sQHsv5w7o6JD.KcZDXoay0nVspXcVVXkUwu1vj4TbxoVj+n+r+RzYs3S7y9qvCb62BYc6ClDFpQURQCJX0UWhIFcTJGUils6RpJfZkpwYa1hO6W3qxoN5w3g9.uW949fO.iHDzo0EnpmOUqTilM6QPTUPnvXLXyRvfEkuOF0fpcJfCe7o4y7Y9KY1ot.erO7Gle1OxGjFHX0UliJgd3FD8NN7xmATxmUVbRpVtFysxpjFTlMsm8xpZAO0KcX9guzgYwYWfG3c8N4p21VYmaXBhrZRWYUDlL7T4QeS+1o34KIzuDNgAmHCA4ylpV.8RMLb8JPo5bgomlu+QdEN8LShrZIt5q654W75O.AAgDnBv1sCIII4wJqedbx1qWLRUdB6HE4t6rvNX1XsvIN2EA+PBFdbBqWkVFAm97WjCeziyYN6Eocy1rystCdW2wcw91wUw30qhu0hINFc6ooWVSLqWQTkj.OIRkDqvPlNijzL788IHHDIRrJE4iwrBiURq9cv5rnAvO.mmOoJOzBIoNG9iOF8RMrV61rvxqwbKtDyuvRrxJqQud8HKJDkRRPfGkKWlMM1HL5PCwv0qRkf.lX3gHzAkjPIkjxREQJEgdJ7PR+9q.LvTsbo3zY3xtTJAiMI4xy8KBrRwkp+qkBAvETPAETPAETPAE71JDj2WsVQdbzfPhvYQgFgSCBCNmgtIoDTcHBBGhytTS9ReymfSegI4W4ewGi69lOHdYojzsECMRM5zuIq0aU1znaBS+tDH8nYqtjnBn7v0XViiuw29ugm5odNd3exGgG4AtOFQHn0ZmiPaB0aTEKFxxL3GnPHTHrZjNvKLBunxz2IoqIlWb5t7Y9BOFyN0j7I9neD9YduuKBEvJKOMk7jHHEPgCIh7fBJOgaDf.KSubSHHhvQFiYZ0km8UdCd9W9kobop79d+OH2zt2MiWpDkMozeskwlkRkJkvqbHFqkd8MTwWgmBRh6QRZu7Lg02iLY.M19VY1l83Dm7U30N4oX1EVfgFYXtga9F4.GX+rotyQZVJoYoHbRhJEf.EVcJ862GbN7QfmCrZM8SLXs443quWHwisEBJUllVGm6hSyqb7Sv4u3zjjZHxKfG5Q9PrwgFlsL7vTVHIsyZD2cUPav2SgLpJXs3DVTdBTd.3HQmf1jfmm.AFzYIHrf0jmovRW94kd1PBBBHnbI7JUhDrrZ2NL+pKyJc5wYm5BzNtOq0rEs61CswQPTIp1nJitwMxcc0WKJkhnv.JGDR0xQTsbDkB7IPInypqhzZ.SF1TM80YzynwZyMWKomacuiFIBTWpCFj4GOCKykaz+qnzuCnP.bAETPAETPAETPAuMBYdB5r9mAjKLT3zHwP4Z0XglsHSVhp0Gl133y9XeId4W803dd2uK9.206DxRP2aMB7gdIsQK0TobYRy5QU+PVasdjEFge4ZLswvm5u5Kvqejixcba2C+LOx6iQJIHt+J3arL7P0oe21zKUx3isYZ1NFkvBNCddd3EUlDmhEh6wEmcI98+C+OQfTvuzuzu.O7ccqHAVbtKxnMpRkRgzr6ZuIMOqGgNtAyFrswXDUsAmewk3o9A+PN9Eljsr0swcba2J6em6DQmUQk0AcZLJWFAgdDFDPl1Qmd8Q4IAoGNmAsIOQYBKWEQ4ZfxiytbKN7abLNxqeT.3.6aebiGX+LQipHatFMaOO4pNC.UXtIV4xauauAA6qz4vncXsNrJejgQ3WpFgQUvarZbxybFdgW7E4jm5zn0Z14N1I2xAOH6a66jHmCecFxNMIIKFqIgR9PPsP788IKQgkLLVK5TCdVPJE4thcP.BghLsFcVFfDeuPBBKmmsuBExQ1Dc61kYWYEVXxYYgUWfEWYYVp4pzseOZ0sMQQQTqdc12d2MaZCajMtwMxniLBUpTg5q0IekmvkuVTmgsSKzYojZzTqR4bcqJINkBBTXcFbtnAmQyW8JDx747UjmnOBQt6RG2KYv480iOoAhfENvIJD.WPAETPAETPAETvaqvkK60gKOIVc1K0PzHbzOSiHnD9kGiUsN95em+FdsidL16d1CerO7iPUSJYYZ7K4QpNgtocY3gGlxDvhyuHAUFAiLBY0gYdr7495eMdlm6Y3Vt5ahe0e9eF1bYAIoswGM0FcTrFCM61Ge+P.PXyPpT4tGrTPeii461gm4UONu3geM7Ls3C8veXd365VIBXo4llpA9TqTYh08yy+UxMMJoyg.adUsEVrRn2vafiewI4v+suLSMy7b06XGbmG5VXmaYCPuUvkzD6.wnpv7HFJNIizLClXGAUS.b4FTkmD+RCAkpxRoFldsk4wdhuMwFK0ZTkCrm8vcbfCvtGcXbsVklKsHYkCPI8ADXMInSMHcjajSggX0NxLVzBPFVAQXEREdrbrg1sVhu7S9sYkUVj31sYrIlfqaO6k8uycvDkKQXbWneO7rF7END9NnjGZokNYIjzsCkL4BIcNCBgEsVjK.1SgP5i14HCAZOPFDgLpDcQRmdwzs6ZbpUZwpqrByLyzrz7ySb+t3IDTJzinvP94deuepF3yvUpPiRkIRlmYyoc6S1xqRXPd73pXPd9ZrX0FLFGBqizlo3DJPJ.oJ2Dyjx7OGHTl+y4r4wtq1YwYAKZbNGpff7y+WgY5u9MX.JD.WPAETPAETPAETvaq3RRAb.XyEH5r.VLnX9UViw23USOfm8vuNegG+qPspU38c+uC1d8.DqsFflToffJQ3Wwij3dXRiYzg2.nJiJDVBGO0y877zemmjIFcDdzG79X6CIIq+bXhSnZ0p3bBRRrrgI1NffydtyyDiOJn.iSQhUP69Ib1oliW3EOLO429o4O726eK23UePzY8XklsXmaZSHwvLydQJWq7khUVgyx.Y8XjRR8TXcvqO0r7reuuOKMyrbm27Mx8bnag5gRLqsLgRMYY8woDXjJDBIoYoPlDe+PpOTCxrygPlaPWZoOodAzraBG4jmlW9XGmEVZEtoa9l3dtiCwNGebjsWg4O2IojNggBCYAWdqY6qTD56iHvBVKBiCiQiv2mLzjfDYP.1f.la0V7Zm7Lb1yedN8Byw12913Ntq6j8sisyDUJSfNE51ARhQoyvWJPojnsNxRyHAKNOE9QknThComGddQ4Q0jQSr1PRlEs1hJLjDOIVkDYXI5KDL+xKyoNy4XxolhiOybHUJpVtLSL1Xriqa+rystU1xDiyHkqP+0VAo1fLNlzVsQaznrVTHnjH250bN2.CXCXvqUkJWvquuONq.mSf0ZwZxEIazNDNvJFzh6BataTKEHTBrh7pXacN.6kiWX.j44.rPTjCv+Sx+ecPyWjyeETPAETPAu8ih2eQAuURkHe5zoGRY.Bgfj39TtRDAAJVbs1DqBnbsw46ezyw+9e++iL5HM3e4+reQtwcsEZ0bQ1ovGUiHVIcMVs6ZL5PMnhnLldFhSkrPeKM13H70doWi+8+9+dbs6+Z323W8Wj8M5DXh6RUu0HIQSbhCqKfffHB7iPgCiMCUfOSuvRLxl2Ffjm7u804+6+c+9L5F2D+5+l+a3F1LHsh7V20ARa95cmzBnYokVhcr0sivIX94WAsHBiviybgo4Dm9b768o9pbf8uOtqa7FXeaYiTwl.cZgmKg.OEBOHHJBkW.YYFrIFjZAj4vjlgWTFcRyHXjIHbrI3UO2T70dpuKmexYnQ857q8K8KiuUSnICeaB91TTjhZvMYvIKSVZL9RAQg9XRSnWudnhhnVigYglsQVoFhxUYsdo7pm9T7pG6nr3JqhP4wu1G4ChGN7kRBvQnyhuyhzkhxk6TzqahUVojrLHSayiiHjTp+hDDVBUPHZmfNYZRrBr9gPTIjkJS6LMyr7Jb5KdAN84OOKrzRjY0n7B3926dX7QGiMukMxnMFFeoDalFaVJXMHrtKU4cgvcE2nkb7FjGvVAXW2rpjBDJODBAZsFoMupsJSdaR6YAgK+i6oagPHwIAoTBCb+YT4BfyrlA81vk4x0+sP.7+jT7GnJnfBJnfBJ3G2T79KJ3sRjnQI8QqsjkkguuBOOO5kkvpcSYjI1NOywNCO1W66P6Nc4ceu+D7.28gXSgBbl1TpsCsGz1KF7cTVE.ZHqKnUkwVoLe9u2yyW+a+cnQ8p7n2+6j65FuNpKyHsWK7bsA7wPDN7QQdL+HcZxrFVtSaFaCaitN3u9a9c4a8sdRFZjF7Adv2K2ygtdJmtzf46T9l2wD4BLqEUhUZtJc6lv3aXa3TAbxoVkuvW8I3vu5qQkg1L6ZKahqaGaksTuBQo8f31D3bDD3Ql0ReslTKnj9DHTnrPfPPjmOyk0ipiLFqYb7Bu1w4EO5wvHjbM68ZXe6ZWrwpUHTmQnMAOmFPiSXwHykkEj5gTJQHM.NDdBrRIIFGcxrL5V1FytRKN8jSywNyYXpomBgmf8cf8w0dsWKateK7DR7DPfTgGVDHxmwYfnJkoeRJIoYXsfRFhmTA57XZpTPBYZKYNGhfxnJUlLu.5ZLzI0wab5SxLKtLSN6rztaGpTuFaeW6jccUWEiOwDrikminfPB8C.mirzTrYZ7j4qitzLWKrWJ9gbCN+3PhoWBRoDomBTRLNAZqgTcFZqgRgQHbCt4FNARmCkShxkKlt1nkv4HOJmrVrVMZqMedowgxS8O3ZemSVzBzETPAETPAETPAE71IxLtbQINykD+1MNgDDLzDagTfu429o3jm337nevGk2669NnNB51dFpnLj4UmDaBJkGJgh3N8vYjDTdXDdk3kmdA9rewGCSbF+hezOJumCtW7LIXscP5mQbeHJJj.UILZvkkg0nwgkLGnpzfdN3GdpY4q+T+.Rxz7weuO.OvgtdvpQY7xM4Jm7xlckH2YfWW7kw3HrZUbp.Z6fm3YeQ9Au7wHNSwidiWG0qThQpFQnKAoKM2HkjJD3iQa.sh.khvfPvYHqeGP3vKvgdq6fSu5p729xuN+sG40v2yma+VND2z92OiVJBaqkIzlgmMCIVzRIFgGFmBqTRTVWpTqJFghU60lzLHrwPjUJjjDKO2wNGm57Wjye9yiyXYWadKrucuS1yN2BSL9HDe1EQ3DDfORKnDh7FYW3gU3Xwkal2dwRGAdgD5II.I9d.HnSiZXcBPHH1IXst8YpKdQN04OGyL+BztSWJUNhMO13r8a35YaaYiLRiFD5oPhkFMBxadYaB5zTPqwGHzKjRARR5OHGdE4QCE.VgDmzhyAQAA3IkW5FA5vQlCzNEVDDJ8whavOuLe1scRzX.jrxzym6R3BQtPZgB0fsBgBcZ9ZiKeCRdy8Bcg.3BJnfBJnfBJnfBdaDdgQzqaWJEFPoRUoS+tj3fvZCg.Eelux2holbJt1qc+bu24gnNBzIMw1uCoJGA0FBkKBkxRm1cHseJCMx3X8JwzcZwe3+O+GHv2iG9C7v7tN3dIPCqs37TYTOTAfvKDoWtIPo0onyxxmEVeOrNAUBqwW84NLO925ooV857Q+POL26sc.7EFVa4onQ0gP3jWRb0aFIWXtYXiab6XHj45ziG6q8z7MehmgfxCy89tdWruMTJuUc0w3zIXrYHsNrBAoN.7oTPHBOez1LRMoX88PDJIIvmWc944vu3g4LG+TrwQFi69P2NW2tuJhzZhWXZpGHQ4RQckMhqSgxo.mGROE8SSHU5fvRDTtDwRelqYWlZgU3o+dOKNKLR8g35u5cysbsWCad3JX51j1m6rDQtnWk.Thb6KSfBw.SkJJzCUfOBkDqNij3DRsFJ64Q4RQzMrNwIIrxZMYxomlyL4jrvBKQbRLfk64tuSFengYSiNLMJGgmNCWRLdIN7TB5zdUB7BIvShmvCu.ejVGJQ9LL6FbWHr4wVb9MoPXvLXqR4k+8kY.WtCX6IkD36iT5Q6NcvHjX7DXDBzdBrJAFIHjBZrqq5Rhayc+4AQc0.WfNINK+P95V6lXPFJOX8Rg.3BJnfBJnfBJnfBd6DREVgDuvHz.8yLzXnMRWG7COxqym+y8435tgafG8C7dXaMJQVZShHiFiOAIcaQhKkvfPzoFxRbTsw3nBpwwWXN9be8uNSegyxm3m4SxG99tSBEPmlqPfuGRgOoII3K8P3DXLojl0ComOppkw37I053jWbVdhuySwByLEerO1GiG71uVh.51ZAFoVYzWYqOOPrkv4xEEKETow3j5B4BMWgG6K+M3EdgWigpWma8FtYNz92GUV5DjoSIyjh0kajRBOEZmGBiCE4UWLNNltowHK6SP8prlIlkVYIdrm74PXzrm8tGtyCdibfsrU75GiqWSFtjOn6CBGYRGNDfSlWo1AyBqqRIZ2qKZoGQMFlDkhyL4LbjicBlbxYod05r2crSt98rG17PMHHKldWbF7MwzPBcCJgUHvgBbpbwuVANatQIWRIwXbnMFz3vFnPH7nqmfXkievwOEqrxJL0jSwhyOGVikstkMwccfai8tqcP8PeJqjDZ0X60gz1MQljPIOeJGEfiR34BvaPNKawh1poelFsKgfRgXEtAheycd60aCZm.R50OWxtxi7ZtKw4TXFbdbzMsYzBH0IIFG8cZRbVrNKVDbrzDrZGooojDGSbbL8iSIMMCswvryMGVQ9Zj7JHOnRzBAEt.cAETPAETPAETPAuMi9IITtREjReVqcKLROzN3jWbF9S+TeZlXCaf2y65t3P64pPa6vZKNMCswIvS3SOmGYY8QJfrXKkJWivR0XUiku82+Y3IepmjG4m5g39tiakR.Ma1jpk8nV4wIwzmNs6S4HGNxvXM3TN7qTAsKh0rZN+rKx+a+t+evvCOJejG9Cv6+duCh.xRah.KkKUmV8yqvWdE8rHc.t7p8Ywivnxb5kWhOyW5qvQNxqwUeUWM26sdmrgngHd9Kh0zCmM28qUBIBOEJoGBSdaAmEGi1Z.OOpTcHrUBYwd83EO1I3Hu9qSf0ia3ZOH210c.1Ts5P6VjzoE9nAoDib8JflO.rhAuFk3P5DD6Gf+vifSJYxkVgCerSvwN0YPp7YaadKbnCdCrogFhwJGgraeR5zDQVB9AJhB7oGtAy7adERkVQdT25rXsPbRLFo.BCHrVMrAdrbmVbgomlYleNdwiOERgjpkKw0dMWCW0N1J6XyahwaTiJ9JVYlYHwZ.rTxyipkqhHLhz98yOeN1VGL+sFR0lby0xyCouOARGFm8Rhcg7r38MEIQkafmmGpfPTReb.wIYzKIi9YZLsViDsgN8yncudzpaWZ2sKciSHSmwwWb5Am5c4ppW2DzF38AVqMe4f3JZC6A2yDWQKPWPAETPAETPAETvauParT0Kj9YIXDRhpLDmY9E3a8jOMKu5p7u3W6WkCcSWOgBM1jlzHxCxRomVhFOTxTbVMfjRUGhUsN9beiuAuvKeXtoa7l3C8fOHSToL86sBYl93pODoHHKUPjnFdlVHDJj.999XDgzAC+v23T7LO2KRy183AefGfO566cRCkCqoMFcWDA9rVZBJxM6JwfFLV5DfK.KdXIfyuTS9JO4yvablKv0cKGhG59tO1ZPDsN1oQs5hXGtDB4.ERHtTqyJDNjRGNUFZr3E4C0B3hq0lW3Xmf23hyPKWD+yt86jstkMwPkJQulKgNoKgkjfCVocKpTo5ftsUl+LHr3jFr3vhk1t.RRRYlEWlW6DmhSc1yimW.G751O250ePFoT.dIwjr3JHxxnbTHUGoNIYFVtca7j4UnVLHhbshLbNCVoCqSP0QpRlPQrPxLc5yEWYUN8ryv4mcVVsYStlgqyXCOJ6d6am8dU6fsLxv3RyX0Ymk4WZA19V1Lw85S+9ozzESW+PBKEgZ3wvWHINMCiydI2VVJUDD5gmxCkTRutcQNPzqxYuzGud9SuRzPzOyR2tYzV2hUy5SyzXZmkRryxqbjif0BNqEapEahAShEm1.VG6Z2iSfxivfPJGDPknRTMnDkB8vWpXGaeq4Q6E.3vdkpuonEnKnfBJnfBJnfBJ3sUnT4SmZmtcoZigAj7C9gOOO2O7E4Zu1CxgNzMRHN5zaAJIszXrwnS6trV+lL13aFisIFiDguGBfic5yvi8E+JHCk7+3uy+VFobYDo8HJviRkpQq9sYwtKSCUMFtbCLoMy8rXg.imhDf4Z1jm+HGgu027axu9u9uI24AO.0UdniWhtsVhxkp.Qkna+LZnFToOgAoMOZevkWDXKJ9retGmCepSw36Xq7Q9D+zrKeAyb7yxxSdFNvl2JyhIOSXcNrVAFsAKR7xaBWpUuBq0sKchayJcZwy9ZGmm+3mgQ19UwC99+fbyFKVLjr1JXS6hWfivHeP5AxpXMfzt9r4NPHlvAhT.GIZGuwwOAuvK+ZrZmtbMG3Z4Nt86fsL93351g9qtL0DfOYXLwjzWC.FoBsTRnSjWAXWdUrsx71C1gEq.xrYzyjwjK2jW53mhW4bmmdHXS6d2bS27cvCsiwwSpvGKt98YpSsHJslxRAae3QnyhKR4RUn5HCiFEsSSnUhArI3GDRf0j+7IDHj4UPWaAsNCmyhzkW0WkETVIJGnr4N3r.XxIWhU5zhoVcAtvJyy78aSWWFNe.OEUFYX7CinR45LR8QYCMFkIpOFCUpJkihPzeVTBI9HHPnHPHHTJvGvyAwcZhzIAgAgiK4N1NQ91hXP5+F4+ViwfhXJnfBJnfBJnf+qk+od+GEu+h++2HLU.bXUwfvhPjBCpJpzIAmG37vr9bPJy+Z4wDjCa2IYzg2EmdlDFcSalu6YVg+M+V+q3P26Mv+S+1+qYKBMMbfMKidwYDq7wJE3KxcM5JYFlocBgiucd44ax+K+u+6RXnG+O7u9Wjaa66Bau4XzvHbNE86mQlvgSHvPebNKSTVQu1oLqtJUFcLlI0wevev+El7TuJehG48wC8tuG7bFbHvw5N8rAEIHwR8fQneutny5Sm3LrQUnbiw3TqEyewe8miu+26I3S9y+g4gd22NaHH.QbFKM4xbwy0jz9Np3TXCrjFkhSkhxkfWljxwQXLUXor.FZuWKGd443u7w9rL6ENAuya+V4Qt0alMUtLqjLGJijRpPJKKCNIYYFRIEg.xz8ITJIPF.Be56rzU3Q+vHbgg7+5e9WGQRWt5wZvCbSGfCt0gw0cUh62Doe.oRebDgTDfTFfGFT3.mAgyfwjPfeDAxHRS0zJwPWo.yv0wznFG4hmjid5SxYN4QQljwMr0cycum8yAFcyLjzmdlkFLSt4FKkQl2lvVgLul5JIVqEqSmKjUleyBL5LrFCaszvjnynqVSeSFoHv4EfxOBoZO12d5...H.jDQAQE.+vpjI8nKJVLKgy0bUN2hywzKMGcasFsO0YHvOjFUqv3MFhM0XX1P8FLdkxTy2msO9XnvLHKg0CVa6vIM.fxDkut9G42KV2Qv+6r8G4qWTA3BJnfBJnfBJnfB9uivIxmOSw.SUJutZx7VAFYdD3bE2ijAij5kZY35CsAVsaLaXyagW8BKwm5u3Oma3VtI93enGEYVFFWFwVCRmD+vHj9AnsVvj.oFVVmxnSrUN5xqvi+4dLh7U7def6mcu8sig9TILBiwPVZFFKnB7Gj4q4sr6JyOCppixXMFiK1yxm9y9XL0EOK28c8Svsca2Z99HNt7Nw5s6rCvx7KLCaXCahdwB78cDTdXlpUe97e9GmW3YeV9He3Gka95uVJ4qHMsOxTCFsEIJ78UTsbHq1dE51pMAkk34Gfw5Hw2GUkxr4w1NO9S8c36b3Wjn5Q7w+PeHN3t1NkSyXkEWjI17njDmhNIk9l9Hkd3jNDVCNqiJQgjkYnaZJpHEQiNFciS3G9huDO8y9broq9lYaisUt1MONiOTY50uGRsAUTE7B7ATXs93bfzjgyZP6rHb4hAE9gr3ZsHIcEFZzIXnssYb5LdwSbLNxoONmbpKvPiOL27Atd16V2AWUiQYi9kHLygIyP1f0K1A4z65GlECp.tyZPXyW2njR7jp7ZYK8vosb9YmknJUHZnFLbsZnC7oWpk051m980L8ENIK2rESsvhLypqPqzd3B8wuVIpTpD+TevGl.kGkT9TKLjFgQTyOfp9dDgf9sVKW7qydoWaqOOw.4tI8eu+dwad6+PTH.tfBJnfBJnfBJnf+6HLpTDHP5V2.nFnhQ.Vj3HOmbW27gtr.X.rjlVF+xUXMmi+hG+ulyb5ixu0u0uI28UsWHqGpzDxxxPFDPfuGBoBmNCWhALZbCuUVvo4K+sdBdsW4v7tuu6mG9ce2LABroYDpTjEGiwJwOH.guOFw+ur26YLV1Yd5886MbR2TkqNmHa1M4P1jralSCmYHGNLLCImzt6rAuZgWsZWIu.qjfsrjAjLf+jMfgbRZWKsAsd1U65IOjbVNAxgMyglglotaR14T0cWoacSmzave3TcObkj+fgAL.gO+ZT3sBMv8V08T.0y44++mGakfFumE8Ir91qi9d3I+Y+Dd4WXubE63x3y8YuC1vjyforWU5Iu5yWA9puWwW4.73MXo9KSgLl3FSvQVNi+zu4eEu+6+9bK2zd3q8P2OSEJA2J3xKQSDBQDd+HDBEKObQBhBXF0DTTVR9vRLp.xZ1DUiD1+6tOd0C9ZXR6xm9F+z74t5cQCSIkCmmn3FnWwiozSpyfWYPGaIToHv5vWXwkCVOHZ0Fail7tmcNd026c3HmZNRlZRtsMMA67x1FabxwvzeExGjSbX.DpXTYIggZjde0NuVXwupyuNoCDdFLDZOy5Xxw6vx4iX+G8f79G8vbpyMGiRGw0ricvVW+F3J251XciMFgkFJ61ikRGguzftSxpWObwco1tZjZ4P.DoUUoHcgGmwrZRJKoZFkErlcrCLdXnywYFLhytReN47Kx4VbYVo+PN+bmCIBhzR5zJlKaKakMt9YYCqaVFe7wIY3PjNPi.oGz9bzEEnRAm2Qj6W3sqCvJkfnJAn8er.s5+mxEWE3ZAv0TSM0TSM0TSM07IHbRypBeq9XgWCdIV4GK0a4h9BupH3KNvndIKlpn8jw7XO6dYe6+039ev6m65FuFroiXMMhIsbDkRKnz3PPwvTbkFBjJDRECA9NO4SwK7RuD645ud9ZO38yZjBrEinIRbYkXLNzwQjzrIo1ptnEeNNmk1q6xYnGd7m4Y4m8S9wbUW4kyW+q9vrsMtdbTtpv8egCvWTbljU240.M8GkR73iyw6ky+1u4+ddy23s4dum6heieoGkoCkDvHxyyHTnoQTa5oJvasfsjQECXpjoHTEhoPgNrE51s4LC6wGdr2iG6u4I4x25kw8bG2JW8l1F5tKyvUVgXsfwFebV4BcwqAcjFqxBBGd73sVLFChfXR5LA8URdiO5v7ruxqvEVdEt1qa2bm2wcv5V47LQSIxr9jmN.kDBSZPgwvJ8VgNsUHcdzlJA+Bo.uThOPiSBgStVrMhXtA8Yeu294Udq2fdC6ykeYama4FuI1yNuJTYEnyxwb1yiEGZIDknQzLjQR+ktwHRu6R6lqzWI.1VHvZ8UAQkL.mNBkNtZGmQvYsVVbok3nm4LbryNGma4kYToEzJz5Ptwa4FYrFMYsi2l0LValLIhlROBSIhhRxskfq5VbHc9p6VfqZ0MrNPqqtANtOlRW2kFE9pmm+Gx+ob88+fru5RTK.tlZpolZpolZpolOAQk.3ptkU5zU8eqPBdItU2mSANv6fKUAMUtng.DiMFO+ANNeuG6Gw1upcxW8Qe.ZhGQZe7XQJknB0HiBv67XRyIHHh3VMnz5XuevQ4G+ydNlZ5Y4W8q+0XqcRvkmQjs.EfwVEpTpvPb.YYYXsVhiRv6szEOO8K+l7Cd7eDSLQa9pO78yMdYWNNJne2tLVRHBDUNFCWR7KB.ujy1eDSN9FYthR9K+t+.dm26f7.OzCvu9W8KvXJAEilGgzfqviJLFsJFbfzZHvCMmXBFMZD8ySIr0DjLwLL2ng7ZG5v7Z6+0YroFiq8SsC1yUrMZjVfYPWZFpQEnY4dcwDKHJIhn.KEEo3yyAgBoPgOLFWiNbAmmW4.Gf89RuDFig691uCt4q5ZnkWwlZpIa3xLJqDPT0GyFOFiiPcD17BTBAARMJUHdc.FshBkGqTv7wZdy2704U12qR2tKx51zF31uy6jqbKag0D2hNFPjYvNLEk2QPjFBjTJsTXypBuJptICUNvtpfXmBoWVMHzJEhfHrAATHzLzXYk9qPZVNO9q+ZXJJwjmgRIYh1iwNWyrrw0sVlZrNLQm1D5cDADXsn6MDWdA9zLnzhaMwUiCumpw7VCd+pud6kTdwZMRbw.Dq5F4r5mlP2+wBfc7eB0t+eSVITK.tlZpolZpolZpolOIgzfypoRVfb0NvUiWdQSgc+G08pUiwpFGvPE7W7c+AjUjwu523WloZDwnkOGW1TyvEl6zjLQKrRAARE1RORoljliQgRwwm+r7W+W7sHJtAO5W5QXGyNIC6sBglTZM4jzeoEHHNhpVu0QZYNixRoQylDG0hBaAuyo6y28G7D3bN9U95eEt9ctcb1LzdCchBP4pDm8Kzu3Vc7tUfPQiNqgK3J3u56784ke4Wj67N+z7K+k+BLoRPZ+tDJ8DnBvqUXbRxKbXsVzXIRKw3DTTXQF0AUyw4Hm6B7ru0avQN2oYrVc3K74tK1b6NDVLD+vADobzLIhbIXLVJTRTBChxRLo4nQPXyFnRZRoPyAN6b7xu86xgN4IX5YlgaeO2HW8l2JssFJWdILAoXxKQIUDDESIBRGMBkRwzSNICVdYBTJzggHBiI06YEqgkFkyvxRdrW7mxvA8HPExMei2L2vttN195WOgFC4KrL4oozREP6jHjJH2UvvhQjZKvfinlAH7xp5IpZFAVsSg0XjRZN9DjagAEEL+f9blkVlSL2Y4jycVVp6xnFasLdqwXca4xXSyNCabhwXpVIzTKHP3o67mGbVJb9UGfcEMzwD0rCgJMyk0a0WV8UA12puUUURdhiCu3E5UGdGfb0oXPt5zA7KvC+BwthKcES0W6iqAtNDrpolZpolZpolZp4Sf3WMcmoRhfWTMdnU+w9tU2kS2kF+YmPhUHoTUc9s+YuEm97Wf6+9tet9MsMbCWjVQg3vhKTPgrJXs7NGdiCgN.SfhCsvB7D684on2H9xewGj6ZOWS0iewPhhCAWINsDmRfy6wHrTTZwIDniZPpWygO1I468CeVhTAbOegGj6752CAtBF0aYRZzDoPA1KNt1UmUO+0XEUB0Vv64I9QOEuxq9pbi64Z3a7v2KSG3YP24Y5NsHO2SXXCJckXJcXrUho0hRnLiAowjzZBrM6vgW7B776+s4vG63rwMrVt4qeWryolknxLz4ivSIFIzylARMAshXPVWjt.T4NTkdhhZB5lL2.Cmn6R7Lu4ay4VXdV2Z2D22m9Syt13FnX9yiuWWVaqlzKGDpHTgAn0gXxKQZLDHkDgfBo.gJ.iNjLmmymlyIVnKGaw4Ygd8n6YOGW8NuJt0ce8bEqeSDZMjettXF1m.WINWIiBKnPqnZBicfFRhaPPP.trE.uBqPgmPJEJrhHrh.Lx.VXXUeCe5KLGmcgKvR86xnhLJnj3wZvW5t9BzIIjoZkv3QZRbFHqGEKtLkY84xmcF7.Vg.KRxcPVoiUJxwXRYswUo3r+i+OY0HP6Dfv4uz02.Wx4+KddozM+h+5f3i8FeryO1Wq55npyZAv0TSM0TSM0TSM07II7qtruHqDL3+XN+hCg2coc6zup32BUHYAJrBI+Uey+Xt5q9p3Qu+6GkojNggzILjib5ivl13lIyUf0XwacT5rTHCI2Cu+wNCOwS+774u0OC22seKzBAC5tDaXlowTlxYtvYYlYmgQYEX8NDBARc.5PIVB4LK0iW9sODuzS+y32626uKeo66yfz5vULhoa2Amojz9CoYRUHM4DtpXuZUAv4hHbBAe+m74Xu+7mmstg0wu4uxWgsM93jMbdR7EHsVLEk3hgK5BsR6IHRiU5XzfQH5rFB5LNmZ4k4EdyWmie14XGaeaba6553xWyzTrvbnwf2WV0grgJDgJLFG4Cyocy.zDBVGNgFWRaVNCd8ibb12AO.m3rmk68ycObW2vdnoywvybZ5ngvlAzck4oLoIZkBPgsnppghCiv68zueeLxJmXcHYEikyzc.e3oOMG7XmjyO+77O+u2+4jn0zTnvtvBXxKHvTPjRfJLffw5PpImQkYXbVTBMJOXyKwNLm3vpel58UONExHJkgTJBwH07Bu5qwEVXQN64NKoYiXpoFiO0UtSt1q6pYSaZCvIGhqr.2vdTrRA3KogRv3IwDzNgkVZADRIDDfWWMB2RslPgBcHXSyptNVHvKEHDJDW784hkdzG28V0eKGdGpu3zOTc8QU8dIuz0KdgqJztDtUSD8U++s5sLpV.7+uj5d1qlZpolZpol++Zp+6O9+eizoqbdEckqVJWkPMgGAV5D2fSbhiwLSMMMZ0lSu7JzZ7NLWut7O8ew+BVyrSvuxW9KxFBUHMNjVOElblXloYXYJiFLf0NwZYotKA5DBa1lm3Uec9e5O5Ok0t0Kme+esuZUHbkWRm3PFNZDRLzd7IHqvPRRDEEETVVRgSfLtECvyy8VuK+Q+E+072625Wi67FtV5nD3xyIREfBA1RGgZMRIL2ENOaZya.TwbrKLOSLyFPB7C9oOG+Y+q+2vC7EuO90dz6kMNdGD1EokxhpYC.HLLDgPfVZQG3QprTRNYREYQQL952Hu6g+.d188ZblyOGaaaaga+Z2EW1Dcvuv7zxXQfAWfDuVfUnvXrnPSivDD85QoojFSLKQSLCe3EVje1K+JbfScB7BA+S9G+OljRCM5OjXaNZuAqof9ZGtwhHROFiF1mRJociDzRECGNjRaU.ZQbLgcFiyrXWd5W7k4Me22mliMN20cdWb86d2L4hmGo2Sfq.s0fV5QDBVogbomzhgjYKovThzIITAwBMgdERbz0q.Q.hjFXCinWdAG8ryw6c3ixoNybrxJ8YsqYV18MrathMuI1vDSRKMnrF3Xmgj9CPHDHBTHZnQDjf04XEigx7RB5LwpAq0p2DDmCoOiKNXytXGRkDjpp.9BOFmfRiAiUPPXDNO3br5zNHPoBPoTHkRF1LDm0i0YvTZwXKqNcNbNKc60EmCJsEXJsTZJvT5v5JwYqc.tlZpolZpolZpolOQgzW8mv6EqVKLWJ.frHvQ2UVfstkMS+9oblElm1SsdNeYN+MOyywnRGeiG3t4Ssk0Qh.FrxJH0NPoQHDDnCXsSLC862ivjVHCGm8etSyO4Y2KaZKWFOzi70I1VhDOBJAgYUm1t3yMXgyeAV+51DKzqKpjVnTQ7Sdk8w29weBtpa5V3N1ymhMO8Xn7NxyFhRIHHLDoVgP.YlR13V1L8xFwB8Vh1yrdJAd5W+83a8CeB9BOv8ysdCWOaXpIIBChxBbFCnB.omVMZw4W7BH84L6zyfwZH0TfqYShZMIu6bmgm80eCN1INFW8UsC9b2xsvllXRj86hKeDwwAX8RxTfwWM10BuDg8h4HVDMFqMox.d6O7i3kNvA43m+7rwsrI9T6XGjTVPixbZXyIxX.gAiviUTUoOtrQznQCBTRLlBJJJQDGQTPaHHlt1R9oO+Kxq7VuENufa7luA10U8oXlNigX4EI1kCTIvzKcXvUEXTBOVgmrzTRZ1fIZONX8jNHkQCMXvQnNf7jow4gk5OjicliwgNwwYtEV.CPPXHe4G4gncRBy1pMSFGQCOnFkhOMEWYIIQdLNKEkNJsRbFEBc.HCfvPFNJi.slj.MgJYk4sNCVSIVaIqX73K7UtPiDuRiTqQFFB5pwv1J.qEJMVFNJiAqrLC5Oj77bd+ENGNiiRSAE4kTTjQYVIklBbVOR0Em6YOBgb08IWt5umHpE.WSM0TSM0TSM0TymjPZ+auyu9US8YAFDNX3fgLwXSStygQnwB7Ru4ayde1misbEWIO7seKrl3XDEFJKxHLHBjBjlpp2IPqY3xCYxMOKK687W+X+P9fCeX9kdzecdfqeSDLZEfKJm.bdYUbEsZc5zoQGLFK5fXrxHNvIOAeuG+Inz64g+Fect5sMNRfrz93bFHHDuTfyI.sDgWgUFPuBKznEHC44d2Cw29G9XXA9FO78wFlcRlNPgv2ibiEEJjnv6fhA8HR4nSiHDtgzqWJFsBSyI37KlxO34eJF0sKW9kuMtqcuG19zSiY4kvLrKsGuEVWI4NAkNGNmDoPhxqPZAoCBlcyrvvd71G4X7FevAYttKw5V253lulqhceEamryOGw1BTdCNUkvWmPfvEfxIoQ.nkNxJxXP5HTQMHtQC5U53bKrHuzaueNwYlChB45tpqha85uV1XmNHGLDS5PDJOdgCi.bR2Gqdf7nbPGcBLxQdu9XcRrRAxlMglsf3HVHWyIOwI38e+2myb5iiTBaYSafcdEWNaZcyxXIwDIDDgGQYFxhRb44nbFP3YnVh0JnzK.oDkVgVqQIDH8JRZjf2YvVTxvxBLWLQ1TRPHIZ70iWT8ZsS.4NGokU2jhrbCevQNHCxyn6fdrR+ALHMkhhB7qdAVr0iRFPXfhvvDlX7FDElPnVgRnYlIm.AJzRPJznkBzxfpGdgpV.bM0TSM0TSM0TSMeRBwpgfU0tNJvupyuRW0lQ1oy3zKMCeXHSzYbNd+U3m+zOCRz7vOzWhoBknL4jkNfvPMwMi.b3JLXEB5ksBSut0yHuje7K8bbvCcH100bM7YtkaflHPPAU0pj7i0UqxppWBn8XSvoN+4X50rYNwxqve3+l+L50e.+c9s+sYWaYFDtdjkURYok3jHBCivZsT5KAbnSRX9g8P0YLZIR3cO0o4O9O8OgzLC+N+t+84p13ZHR.vPxSyv6AUPDdkFu0wvQ8YCyNI3GxEl+7HZLIclc87pG4P7jO8KxYl63biW6t4dtt8vla0lzKbdL85Rbb.HbLxUPo0iw5QITnjAnDA38PINFDDv9N7Q44dy8Qt2wt10UyMtqcwlZ2D5tHI1LBbFbBGVIXQAHQaUH7B7hTFlOhRDnazhfVsYo7Rdq2+CX+G7frzfQb467J3Fugqiss90idXe5c1SPaigYa0g9lRrRvItXcAIVMAkEH7BvHnXTA4FOgi0h3IljQJ3XKOOm43yySt2CfBOsSh4J240vN1x5YyqYFZGJQUlia4kQJ73EV71pELWHDnB0n0Z54b3kJTHPJAkPBkkTVThsb0YAPJ.kDQiFHBCQDF.AJPoYfsCooorz7KyEVXdN24OOmeg4YoUVhzrT5L43XcNrdCJshwa0hNiONSL4DznQS1PyXjRIgZMA5.hzADFFRfPgVpPKj+h9u1AxK9ymU+b0BfqolZpolZpolZp4SPTY3miK19oRwpAeE.dIsaOFevINIqayWN4.e6u+2mic3ivW7Q9Jb6aeS35dNxDJrHHrYBA5.JxSQXb30dxTRTAI7ZezGw2+w+wLyTqku9i7k3JlJghAiHR6viuJMduTcKs5X3hjEGNfnwlhEMddxm8EYtyOOO3i7vbeW6Ux.OTlOjxBC5fDhhBw6bTZxAbHBTj67XChHPjvwVtG+4+E+0LnWe95e0uFetq6JH1NDuyRos.OfNtARUHNGXbF5zoEdfE6tBtfFzt0Z3CN5oYuu7Kyg9vOj69yrGt1suCVW6DJWZI7CFQqjlHhErT+9HBqpKJsTSjNlvfFXPwnhBRKc7Lu9qxQNxQvpjbs6XGbW645YKSLE1Emm9yeNZmDhS3nTUkf0Bm.sSRjQfzIoqaDxvHBFqC9nVblkVl8evOhO7COBKtTW972+mmMtg0xZmdLD86RwJKSrvRqHMBaNZGn7fxIvVshr3W8lhH7RFkWRT6NLVmwwzHjSOrGu6wOJuwGb.N0YNASq1DaayalqYGamsslYoiBDiFhpeeBLknkNTBOHUXjdJEdrBAFk.gxSnsCdqEeQFRqEoqDbE3sVrNKiM8zXiBwDERpRxxEkL+vgbgd8XPZFG5iNGYYU6ZdQZFJumVwwLwDSylBiXqqe8zJNhIZ2hIa2lwZzf3P8pcCMTDN.u2i24qRLZuGuM+REErIu.oipOuyi8hUsD.Nes.3ZpolZpolZpolZ9jDB+pi85pi.8E682JwnRJQRXywn.3MN5g4YetmmMuosvCd2eVRPPTjFqW.ZEBkhxhLrE4DnjPnlv1iyoSy3I+4uHKu7Hdf66A3F1xVHPjg0rL1PA3Uq1+pUOmVMGdwHgg4NZ1XR99+zmg+8e6uOelOycyib+2GMQfJc.MZFiRYPphQHDLJa.dqgFwA3UB5mkSRxzb1RO+4+keGdm8+d7a8a9avu9CdOzOcHsTEUAAmThNHFgJnpBcrk3ckXDdVreFplSPR3Dbj4Wf+3u4eIu+AOA29m9t4tu5sQ6vXjCGhurDDBLNORuhFMZRgyh1CAx.BBhwoBneYAmNsOKzuGO+K9hrt0rFtqa8l45uhcx3JXoieTjYCoUTDfuJEhW8EoJG6kqtmtNZM8zTHUz2.ezgOJ66seONyYu.aZCah691uStpse4n74ju3EHqeOZEpYlIl.swRukWAYXDf.ouZb3cHWMsrEXkBhmYL7QwLW5.du258XeG5847cWjFi0gO0Ucc70tw6As2iBK5xA3FYHV3HITQTrh7QiPJkfVgV.Fb3bNr3vYczP3.e4kDbFDDiNrEDFfUIoLNjEyx3Tm8BbhKbAN87yyE51iACGRQoAoLjFwIr1omj0N8zr9Ylg0M4zLU6VzNL.yfgDBD48nvfvjicTebEkXLFxiyqb6VBJQ0diKEU6ZrvKplNfUSTZoRsZ.RKPI.706.bM0TSM0TSM0TSMexBg6icV89hUE+hWxhc6yTSMMu+7WfuyO3wXrwmjG4g9Rr0wiXXOK91A3bdbBMdmCQQYknf.IYROCvy2+Y94bnibBtsa6t4ybi2NMEP9vKPbbIVZhSHtTB8VI3ygQ3oTBztA66DGgmbuOOMmdFt2O+8wrZA4yOGqa7NTfrJEgEBbNCNSIgJIZofQ4EDD1hyzsKOwy7pb3ibJ9r2y8wW799bDKfUV57HmYb7RY07dKqBRJusDkyRPnhAoYXkQzLXVNcVJ+e789a38OzQ4Zt5qgO6ccyrtEli.xITDfIRSJNFZLD6BncTCX3.zHPIUXkN5ZGwQWYYd2ydJNw7misL6ZXW63J3F15kwzZE4Ks.t98QGoQmjPdQA3kn7RT.Wzc7zPC.DzZcL2Elm29nmfCczSwx8Gw52x141t9qma4p1IKepiRnufXeAi2HFUXHYFCEEdx0wDE.RmDsShzIQ4AirpmmS0ZFZK3Dm9zbfibXN1wNIEiJXqqYSbS6553Ju7qfFcOI4YYXrVhZDQqVsv6ErTudrxJqP61sQ6TnLBjROduGAPfWVMn6t4QfBYjBDwjJCXjWvJYF5Yc7tu06Q2zLVbkkY3fAf0Smjlrw0LKsazjcu80RRTLsZzjFIIH8dJRGQ54OEKmNhYmYZrkEzKOESYNJgmnnPhaFSifHZXi.jUW6P00Pd+u3L2jW4HtziTpQHpNqht75ZPplZpolZpolZpolOgSUbTI7UNA1qeeRFabdq26.ruWae7nO5ixcey6Fo.j4kjkTfyAVAnoxwrHU.FkkTSAyUr.O+q9xX8g74t66k02VgIu.mIEQjAGsnxQStztUBUFd5kPl2w+6+Y+oL+xi328eve.W4VlgzbOwkk3F0mAEkXsNBSznkJhzAjDGfP3XzfgzYl0va8VOO+nm3I4Jtxqie2emuAsPvwNxGv0b4agQYivfuZmbKsXJSwaKocnllQInBCYPojttRd7e7d4Y16Kv0u6ahu7i7EYo4Wfj7bTVOkdCkVAQMRnSTLYiR4bm87LQylH7V7RAEdOcKy3TW37bnSdTN4YOM+y9M9ufwiiIrrfkW3LzP5XiyLCFsm9CFApPDdIJ6pgDlD7BCFUUnUc5yNGG3vGg23COJ4dMW2MbSba69lXZsl4NwIYJMvvQHUNRZFSNP29inTnIpYKL1QnjRjqNJzWz4eKRLJA68MdE9nydFlu6Rr90tQ9B26sx0s0qjjbICleIFWWfrQ.dYLYVCCWYILBIgsRXSyrMVtWebTEvUNqGrFThJGT0RIY1QUt9pCovKYgAC3vmaQNvomiS2cENeudD1rIiMwDrtssC1zzyxVlZFVe6wocbLAKeTLFCt9cIe4EQHfvf.ltgFcmIw4Jvq8DoCv60Xwh26I2mQdQFQYUEpzESdbDU6nLRIBfnnp5vxs55o6b9pdEd0wUPrxnEqKR94gKiG...B.IQTPTsZpolZpolZpolZ9DBMhBoWQI8cJj3YLeJQNKRKzWlPZ6I4MN+H9e4e0+Rjki3+9+q983plbF5N+YX7Fs4BNMMUvjZK97LRSynLYBxZME8wy+s+O7mxfUVhG8ycG70u2aGaYORGrHslnCixxvZmh.WAiqKPIgBmkQ5FrjtE88ve3e92kO7cdad3O6sye2G9AHlQj0eQZ1tAK2aYjs2NCVYdhsCYhjHLFCCLZJauVrZA+78eL9i9i9ei0NaB+S+C98X6SNEC5cd5znIMBRvhi4O+4oUPDiO1z3KyXTQI9j.x0wrPgkFgM4a9XOMeu+5uKO3m8d3gtq6frEmih9KyTtYwpLjQNkjBtLhkPCqFoLjxfXlaTNosZyPojWdeuAu9KuOtxsrc9kdjuBSjNORu.ssxIVINrRGVkAmzPXP03ISokBCjQDYAQLTGRtPv+c+QeO13Zmg8r00y0r9wYSMfVBCJekywEVAMaNERcLYoFJsYnBjXkVxJRYiikvhKOfA4dBmbsHWy54jCGxy8t6m8+NuECV9bryMuEtyqX6bMqcsLdf.Jq9t0K8jl0hFMhIJIDmufz7gXKxHTKogNDkCDVINulRmhTg.qLDWXBBsB6Z1Jm97mk24vGfO3Xe.KrzE.eIshCnSPDOvMeqLgWxD5.FSGRrB.GFeIkVC9fXfKM87WhKETUBQ0Hs6pF85pvlVhRoPHDXz5pQf2W4Ns259a8wJYkxWIUSYfX0P7Rt5iXsCv0TSM0TSM0TSM07IHVtWe5L9TXLkLn2JDzJhnvlTLHi7RHCO+z+leD9xB9hO3CvrSLIix6gTnINtAsEBFr3hL4XMP1rMkYFPGP+bGuvac.N5GdPt+66d4l1ytw3AaQNQQQ3bPPPDshio2x8vGnAuiE51inYZiEXeu0A4c12qwcba2B29sbij6JHevJzLJDOJLdIZmgnPMiEMN3LrxvTTMagWK3HmqKO9i8cY8qeVdfO2syVlbC.onPg2KXXVN5XAs5zlFpP7lbFLX.NUHJYDFuhNgI7G+c+g73O9SxMeS2.e8uxCyDJEG4bmDQoCangQYCwHJItQ.wIQXyyHKqDKET3DL05WGmZv.d0W4k4vezQ3p19Uvcdc2.czZj3PPUGL6jq1AxBeUS+3fxLCJkh7BCoVn0LSipQadm2883o26yx0ryql0M4Dr40LISMVChCrnr4HV0xRoUvn7L7o4H8RTZAB7HkPRRDezYOGSNy5X1MNCyMHmWeeuLu2wONKLnGsC07feouLqqQLatQC5nTHJyv5knTQ30RlZh1TVVR1vUvZKqbfUFfzBoFCgpPRyKIyTfLHAcq13zQzcTJ8VdHOw2+wwfCqvRPrlcroMyrSOEqepwY1jFjXbzwAIJY0XjKrUgllupop0WzZV3RU3TUsdU4oaPfFmygvIP3DUBaArNGBgfx7gUiZfvgTn.bHkJD9U2DcWY0zPHppwJwpan9E+50BfqolZpolZpolZp4SPT3D.RhjBLJOdmCq0SlJFcRKd98c.12q7hbMW8N4KdW2AsvP2U5wTiME4NnYfhT7j4bDf.SRaBi6vIO1Y467s+VLyDs3SeK6lMNQBkY8HPBfh9CFRq1SPHEnkNbBXfwSgJlXUCNy4Vfev24aSyn.t269NX6yNCi5OOAVCRcKFjWhJHAS5xDEnQnBnadJ4gIzNYBNyJc4a8c+Vb9ybR9ZekGlG5NuKZHfzQiHQGSnHlAoinP4oYRBRmmdc6QgwRmwZiQ0f9ok7zu5KxO8m9TLyTiyu5W+QXaSGxYO7oQWXXhwlfURywHJq1sTznKEXKTXTR7QIDLQGN1ByyK7VuAG3fGhss9MyW7yb2bYiuV5d1yiNziW3wp.e0vBiz6pbDFIBUDcGLjxjFzXsSwExy3MdsWh24PGjLeNO5UtQFqYSltYLiEpQiACVbWLPw7dj3IVGfVow4JYX9PLFCJsj3MtCFn0b3k6x9+nOf8evCQVVFW9l1L6ZqaicuosPRQIQlB7oEXrFrBMHTHHfd8NOXcn8BZnBINJBsJfROT3EblElmwV25Xl0rVxkRNwbmm26.uKG9nGiEVbYzARZ1LgML4zrgYlg0O0LL6XsY7nXZHkTzuORuc0Uz1i0Kv.XERbZAw1JAv1K98aUXheoyrhpPNyiEb9K4dqT3QffXMUIBsTVEBVRERoF4p876nQC.DHDRDBERQUHjID06.bM0TSM0TSM0TSMehiFsGmg4iHPYYrFQjMLkg4d7QSxHfu22+6wZldJt265lYbDXKFxXIsnQTSleokYhNgL03iQ+rLLlRhaNEmKujWYe6iydhiw+0+S9ujqXiyB9Rr4inc6XFkkRdZIsaqXzfEoYRHiLFJEZFalMw4Flxy+rOOm9nGg+g+i9CXmaZMUcwJVZ1tEYFCCGUvXSLICW57HTMnaZACcRh5LKqfmW3Uecdt89y4W8a7Ky8dG2BsDP9nkwLHkNSLIBUDJETZFgU4XT5HFUlSqwm.ePCVpzyacfCye3e3eBW60sK9M9U9Jb0abVFtRO5M+bDqEHsdL9QznYDAdE1TCYkVDAAHa2BeqDNcut7zuxKwg9vOjq5J1AOvcb2r8oWCtE6RXQJxPvRk3Wmzg.ekSkHAutZuhaOCAS1j4yR4od4WhW6MdCV25WKO7W9KwNSZPjRQnzivURos.i0gyKp5rVgm33HjdAEYCIsr.Qf.UbLnDT1db1+6+t7J6+MYgkWjomYFts8rat5MrIVeyVH5tDpRKkVCBoGmVCREVKXMUiprzIQgDbBLkdxMNbZMk5PV6Ud0rhwvad7iyAN7Q3iNxgYvvQzrUKV2l1.250bEzHLlIRZwXQwzzIghTLKrHiJJnyXcvK.qvRINrBOdjfrpedcNWU0MwuvAXt39jKflwI.NbTMRy37T4ALW5lM38dLENxsYXsVrVa0HS6cnTJ7RwkBZMgPfTVE7ZP8HPWSM0TSM0TSM0TymnHLHlkVXIlnofvPEodGQiMNWnPwS+BuIG9C9P9896+aymd26FaVWjooLVmwnzYwKjT1qGMmbZleXFzrMo.+jm8E44egWj69tuSt2a5ZwaJHu+xDI73sVzx.Zlzg7rBz4CINIldCxHdhon.3Idp8xdelmkuv8bOb+27dv4yY3vtzJIf.c.CyMHjAHHfVARJLFxbJh5rF5gmG+odJd1m+44putOEO7Cbur9j13J6S9JKSi3lfySt0gNJBos.SlghhBBa2.YbGF5g89Z6mm3m9rr9MrMd3G5Kwd11VPQFxx9rtY5v7mdQN+4NGpVZRRZfnDJJrHjgD0YZFDH4LK1kevS8SHsXDW+0sKty8bSrwwGmkOywQOpj0M4DLeYeP3pRW3K1urBvJkXPRgJF8XSvwtvE3odoWfidxiv129Uvsei6gKeyalNKbFvIwajT5EX8B7HQpTnDRTJOBomQCFRud8P0rAclXZxjNlu6x7iepeBm6rywvACXaqaybqW+0wUuksRi7BRW7BzNThy6nT6wojH0J.IhRvaLDpTXLVxsV7xP7ggXTQjJULRHXtScFNvwNBu2AeeVYkUX1omgqeW6hKeKagoFeBl0ThRHIVJIvVhrzBk43o.mpfB+HLRGF737UpZkRMRg.kPRdPNqVPTHWsGo0N.+perwh0WE5VV7X8Nb9pSuWPnpIP0dAq0BBBWUbqTfWJHIIA7UsjcU8MUc5WcGgqE.WSM0TSM0TSM0TymfHOOGsRB9RxyMDznCdB4jKrLe2eviyNtxsysb8WCiKjLXPeZElfozR2zdzpcGbcGPVVNDkfTGxgWdD+MO0yPddJ+c90+UQhGW4P5DGBXIMMkFsFmjjXVb4tzJoAokkTJBPfjCO2h7S+Y+bjH3evu8+YnvhoeWhCcn0BRKFgPHoYRCJSyocTCJKLD2ZbL.uw6eH9deuGiNcZxu++neeFOIhrzkPkOfw6zh3nVLHKmQlBRZzlXuhLiEcTDwMlfTujm6c2OO0y8hbzidB9m+ey+LttcNMkEojO3Br9IGmUJJ3XYCPEGRjziMKCaoBBBId7owkzjSblyv99f2iiexSvcdG2F290ecLlRyJm6rjXszpUHokCvKs.fZUwuNQUBCWJUTHCowLyxK7luCO6q9RzOcD6YWWO2wMciLcyXFt3xHc1pN00qP3EnDAUiwqRgPJvVjSoCDQAzZ10fnYSVwYY+G5Hr+2683HKuDacCala85tMtxMrElIJlvUFfIa.ZmAgNpZDscd7dak.SQH5.EQ5XVdk4wJkniaRT6wQjzfkGLjO3Dmhictyxa9NuCQMSX1Ylla8FuQ1wl2LyL13ncFJyyIovBtBbdG4NGRgk.k.cCMZU.iJyw6q1MZgWhTHQ6cHQhzCERC3kWxw2JmyunUvRFLJEoRC5.DggHTJjJIBkppylUctjXVq0VIP14V8zSwx8w5bXrVLVCkFSUpSuZnZUK.tlZpolZpolZpolOAgsLklIIPYIdoBcz3bpgk77u79XokmmeqeieGlpQ.ZeFiE0.IZJyLHQfVIP2nIKzaHAyrQNeokevO5IHMOiG7A+Br4IZSVuEIQ5HLIBiwiwAFiAsxQnPfOHlzbGIiOMGY9E4a+8eLzgQ70dzGgwDBVYoySmDM5f.JJxwZ8DpRP5cXKsj68fNAgLhO3Tmim5msWZD0jG3yeebYSLC1htTl0m3v.BShH2TPINzgJLtLZZDTXDPiVj407lm5H7jOydoeZFeoG9gX26bZ5ffr7ADKp5o3AoCovURiIlfj9ojkWfQFitcaFnjb7ybFdyO7fbrSbRt8a5V4Zu7cvzAQjt3BnKyo8DiiRJX9EV.cyDjHHvB3EHDJJjZFFFPlVw6dzOfW7cdCVoWOt8a7l3dtkalwzJJVbIhrPdRLdaUw7nPgVnP5AeoAGdrlBrJMAMaiJIgSszJ7le3Q3.G6XrbuRtoqcGryMsM1052FSJiIewEIav.TIgzZpwYo9KheU2oUdnRuYNNoBoTAStNBCBoLTwYRy4DG6nbjSdZN9oOMcWdY1wN2IaXlo4x23FXMcZSj2hn2xDJ7LlRRaYLVggBugRogBokRoGzdDBOVY00ZABAJu.kSTkrzdO3KIHP9wpOKIdDXDJ7RINgDcyYwKkXjZJwSAPg0Pgwg0a4z4ySYYIiRGwf98oW+9zaPeRGMh7hBJxyujd5KN1yB4uHyoqE.WSM0TSM0TSM0TymfPKbjDmvfbGAMZSgWvK8FuEu591GaZSafO6tuNF1cNxsEztQGRWIEuJfwGKgAo8X7lSvvUli1.G83mj89yeZt1q8Z4W9QdHrdCcZDAEiXznQHUJBhhY3fTxGlSi3lLz4nPIPC7QG6T7rOyOmG8QdDdz68No2JqPm3XFqQ.CKFQdZNMZ0Bg2S4nTRBiY9bGMZ2jT77huzqx6+luCOzi7P70umOGiR6wzIg3bgDpzjUjSuQiHoQGRhavn7QTZTT5DTR.8bk7NG5HbfO3CY265l429qcujsRApNgzTqwX8jOJEoVSRilzueeZJSp5Q33XbMS3LKsB66fuKG+jmBo2w8da2E5hQHGNhIShvqEzqeOHPR7XswXLUuP3q14WqTQgJfLc.oAZ9le6+J1352DegO+8vt21kSaqCyRcI1YYrlMXNYk3ToqJTmTdVMHyLT3rHShoeYA17b5tx.d429830em2mwmcc7o+r2G25VCIvAxAcYTtCcoijPMkZG8SGfPW0sxMTZzHnHufQk4LpLmR7j2ZbDBEKtbed6CdPdmC7tLJKmstksxtulcwMcMWMgFCglRBsFzdCfAaQFEVCibcvKWMDvT9p84UB38fyQfH.sPf1InJ9ppFwaq2gyJvEDV8iNj3PhQJvKjXjRrBENofzRKqLpOK1sKWXwkXgEWhk61kzzb5i4R+tfPHv+wde.hajfTpHHLf33XBCiHNNlff.TJUs.3ZpolZpolZpolZ9jDABCBmkgFOiqFigdO+jm94X9ycZ9e8+4+Goz1iIZHQl6nbTJQMZSgyP+dKCxbNWuTVy52Lu+7Kv29a8+IqYlI3QdvOOsPvvdKiVCgBv3E3LdBihINpI9rbblRNYuB1vZVC68cOD+g+q9WyscG2A+xesuLJfXsmVQQLbPObJAsa2ghRK4iFPCgjPYHYMm.sTyO7GuWdrG6Gw87Y+L7q7EeHh.TBnbzPj3IyWhWHHtQBHLjksDBmGS3DnBSvp07yd1mg+r+j+cb62wcy+ve2eSv5Y7vRLC5iylRRTBBcDF2.DNIIpPjMljAYySXiFrrImmYeuDuy6cPt1O0mhO6sbqzzTPPYIpxBvUBZApv.JoplfjYEzY7ooeuTVYXJSs4sRXbB66Ued9YO2Omssssxst6afqayaivQ4XWoKiqBQpjzc9K.qab7XwTVf0Z.Tn0Zb.oNOQwI3SZvANxw34e0WmdCyXO64F4Zu1qmolXRZM5nPoAoETZM5FAXEPoojrhQDHBHTHI1FfyXov3IWpvMdBAIIblTAu6671r+27M4+K16NKF65N+.+92+Kms6RU0s1IKtHRwMItJJJJQQIQs0sj5Vc6ts61isaamYCSPblLCRdJ.A4k7PxKAHCFXjELHAHvN1v1X5tk6Vp0RqcIJIRJQJRw88kpXwZ6V0c+r7+++7v4R1xCRdUFMv4CPg5VWVEt25TmC.+c9s0ocK1v8sAd3ctKdf0udFqbY5tvB3aL3YSQgAqHCmzhzSfvq+zx14vZyvZyxmTyFKJCHcPjxCrN.U9DfVJym.zJvojTOE7BCvqbHnUzJKk4WYEla4EnQmNbpSeFhSxnSmtj0KAgSPfWHkBhnTPUdrMrZzZMgggDEEQkxkIJLhfv.zZ8uIqut++95mh.fKTnPgBEJTnPgeKhumlFsaQ0gWEMcB9Eu6mxxMZxSbvGmUMXDCPKjIVxRsfzCsG3jR7Cz34oossBscvQN5w4NSeCdjG6Q3A235PC3Icn.bBIBY9pjwXcXRigrLzHYrIFmSOyR79e3gYsqaMbf8uWFNThBP6oncqFTsZY5jFS2twToRUpDVhdKuLqzXYXzA48N6k3vG6yYaOvV4PG3.LbffrNcPaMHkNzZePKIyZ5uSXM35WRuyaSX.+Pdyi8E7S+Y+B1+9eB9c+dee7SyHxkPfLFkmkTsGn8wIz3rZjYBjwVNay6v8sksvs6zhe4a+1boqeEdvGbK7POvVYHeEAoInyLHs4YZzh.DRTH.mkQGXTZ1MEWPDiM4Z31s5xa+9e.m9Rmmolb0bv89vrtQFlfrt351DgMEqW9NwUWJj4u8sYMqd0L7jCSu1snSiVjJEHCKSf1iNRAu4G+I74m5TLPsg4odxCx8u10yfdZ760lg8KQJIjQBIlTRRxvpDXcB7jJFcvQnyxs4NMqixOD0fUw4IX5lKxsuws3m85eLkBiXxImf6ec6mss96iIGXPBLYjL27Txlg1jgDKHx.gq+TuN+7hU5TGsTgu1iPOMdBY99J13vY.qErBAINvnjXU9PnOxf.jJEZgl4WZQt5oOOW4FWiaO+cnUm1XvBBAiL9DTanprw0ceL5PCyHCND0pL.k7CIP4yHlU9MA4d2o7rCDBP3xHoWR9EJ8q5Y4+IABWD.bgBEJTnPgBEJ7aQLlT5DmP4JAbq5M4W7puAgAA7CdwuEChfPgiXaBNzH87wJfjr7.YjVP5I43W7l7Au+6yfki3ke5Cw35PZ2ZdpD5ixXHyYQHUnTZRSyvjjgVHy6qWfW40dc9xS747xe2Wfm8w2GUPPVZS7b48wpPIQjIv4b8KAVEFsFgRyrN3m85uAyb6aw+h+n+HdjsuADFGwcZRkJQDGmfRqPH04C4nzLvIvSIQJDDOPUN8xKxe6q7Jr7RM4+5+reG1+8sFxZrLkiDXMwH0BLHwHxigyjIPlJwKUQ5H0XQkjO8qNEG+jmfoVyp4.6YWb+iNAQYonRRQ6xvBfRhTpPJTHcBrNXk5swEEgqZUtcRLezYOEG6LeE0pTgma+Gjcut0ieRWLcZgw1CYnhj.KoBAoRISJKQnMiNMpSqtcvo7PUtDcQx7s6xa8IeL2312gpiLBO19eDdnMuYJklhb4lDgfNx7IZrEIVgCoyhxJHDPpBXwYtCR+HziNFYAAbmjXtvMtIe0kt.2XlawTCMDqe0qgsuoMyFV8pXPOOj85QRmlXiSP5IwprjIxmMUNg.W+xXFGLbs.boFboVLc6QVZ9ZfxhFqPfw2GYTIjkKiy2il8hYt5KwbW6ZrRyV7Im8T3r17xk1yiAGnJ22ZWOiN7.TMpL0pTkRddLT4RTMLj.sFgwQVZJtrNHscxWOR7axxq05ta7t3Kx2yvRa+mvAxu10OEA.WnPgBEJTnPgB+Vj1c6Q4AGlDfO4K9RVbwE467Bea14Zmjz3kwSkhIygeoPBixGXUwYIHjVxvROOG+p24cXt6ba9S+m7iX2qeiXccnaikY7olh3jXxRRQIUntal1zdnChPH83qltAW7RWjIFcDdh88PTCI35PVROLYYLP4xzoSODBAQAgzrcaPnQGVAgTye+a7NL6smgcu6cvS736CEP6VKRfVgmmGc61EmSfKyPVhAiwgx2CzA3bRp6R3+q+p+Ble943O8O4mv92x5HP.l3d3UpDcRiwHzj3DXsVjVQ+aFPDFsjZ2+F3seqeMez6+grlUOEuzgdF13jShnYSTBARaZ9zSVjm4WDJDNEBCHrNluSJSN4TLaVLuwG+gbxKbNlZMSxg1yivd1vFP2ZELcZh0zC+POD9djHr465WAr9QGg6r3BrXyV3UY.FX7AoowwIO+k3jW3Bbgqdc19d1E66g2Mqt1vniaCMaQ4rThPxbFPqUnCz3q7P5rHMVDYNbH.+.7GdT5EExElYZN5o9Jt1z2BkVyTStN9IO2yQYeehjJrc5P2EV.SbWB8UTpbHIlLrx7o3rqevjBWdVdENP6rXPRVVFoYVDNMR+.TQkPFDgUKnQlgEWdQtyR0Yl4lkomYVVX94ocyNTa0ixPCNLqdhIXcSrZlXngXnvHBEBTYF7cVTVKBxPzoIVaFFSJ3xvYsjfAgPhVHxmd1RARsBkTfBIooIjm9WKfDovgvIv0+yEA.WnPgBEJTnPgB+VDiN.ecUNw0tIu66+AbeaXc77G5wIP.0azD+RJbjGLIXIKqEZMnKUEiyxgu3s4Xm3DrsMsI9NO+yhOVV3NywHUGjjzLPJv3rHsIfAjBA9khvJBXgVw7+ye4+2ToTDu3y7XrsoVK3ZSbqFD4GPRVFBsGcazfJUp..cZGyfiMBYn3yNyY3C+UuAaayaleuu+2kgDBZzdITXnZsZztU9PbRJ03rof0gmN.unJjX8nYqt7y+fWky7Umlm5.OA+S9VOMkDvRyLGCEoIoWBVxCX0lkhwBZo.cT.YQgzMEN9oOGG6yONk7B3acfCwtV+FHq9xjzsKYBAJok6NzfkHQgBqUfyZvZLTdCafo61kO8LmfSetyv.kKwSr2Gh8rg0iakkIqUKHsCJeEBsBCP2zj72KddTu9hj4bL3jSfWsQYtls3KN844Tm4brvh044NzgXiaXcLd0Jjs7BjzoCCWJjAz9j0tMgCLBJAnHeULosFrVHCGcERFbi2GWdgE3He9WxWd1yvx0qypFeRdr8rW10VePJuvrn60CANHIAYVBZOIpPMVOIo.VgDq.jNIRqDoSfNKuTiWX54vOHhvRkobsxXCBHQpnoMid.m3bmgYWZQt0L2hEleIDXYrZivNefsxniLB6YaaBMR7EB7MBzoIn5zBWuDjYF7DVvYvRFVoCzfWfGJOIRu.RRj3bfyYADXrFrNGoFCBKHk4aY3779Jv.4oxVj+4h.fKTnPgBEJTnPgeKRXkQHE38+3OgomdZ9C+8+8XqqZXTXHRoPqBv3RIkLhSVg3jNTcnwvHhnYZO9O9J+bBhh3oOzSw.dArzctId.CMvPLW8EHrRHNmAo0hv3PJBPKC3NsS4ne043Lm737i+c+A7LG3wnjvPi5KhuTfueYjRIs5zFsViR.851CsuOfhazJg+5W8cnhTwS+nOBaexIARIoSSlXrQ.jznSaFdnZHTJbYwn5u1lxbQbykqyEt5M4M+4+R1+CuO98+9uLg.ca1fR9RBKUlkWdQhpDhRKIv3vIT3E3Q6.CKYy3VqrDuxa8NLRkx7bG3oX6qeCzYl4PjzkpkCoau1HD59A+kG.rylmI4DSFolLZNXId8O783zm9KXcqdUbvGdurs0NEpNMociEY3Rkw5WBizg05nWRLYYon7zDH0TuWCFZrIwa7I3pKrD+5O4HbxSeNFs1H7rG5o3Q14NQl1AQmlTwYIrRHJmftoY4q8JOvjjlWV5H.c.DnomPQakjO6KOIm3RWjKdsqR4Rk4PO9A4g21CxXQQPmljsxJXDBzZMAAZBpVBTNxbVhSR.QdFe019Y9EPY5GRoCJOv33EDgnbDcDBls4Jb4YtMWd5avbKtD0WZIB78YfJkYWacqrlIFi0N4jL0Xiy.UJSxByiIyfKNELVDVvWJwOvmfRRh60AgL+l3XkfQZI0YIMIAabLVWH.8GDW16sSfu6GQQQ8uRQzecH0O329etH.3BEJTnPgBEJT32h3viib5Sy4t3EXCaZibfG4gPCzbw4YnRUHUIAmfLqgrjNHEonENpaR3S97Sw0t7E4keoWj8uuGlzjdHSLTangf7kxCoYV.GJoEbNDBEFbboaNMu5a8trksd+r+8sGpo8oS64njVQfVQ2d8vOpB85zlgJWFaZBXcTs5fbmNI7FevmxYu5s4e128ayA2wNIDXkUVfQpUEsPQidsHnbYbRAYtTLoIfPfEIKah4nm6x71e3mxFmZy7i9N+P1bsQnW65X61lwGYTLBMtfHLnwyoHvlhSBRkiloc4ryOCewENOzyxde3cyd2zVInaOZsRKJEnPHcHC7vXcHHOymfDrVxbF53homMkW63Ggu5lWggpMDO89dD168ceH5t9P5xC..f.PRDEDUzhjtMoTfhLOKHzfSfMwf1ZwWFhBPDmfd3Ingziqeoqwm8Umkye8awvSLEO5d1M6eaaC6R2A+zdT1yQTT.IFKMZ2hTqjvxUwEuB1DCjZAuPxTAzvlxM5rBS2pCu8G+w3o8YyaXK7HacaruMuIFMPxh29Fb6adMt+MtURMYjjkRh.DRKBDXst7o9sPgzJuW.u.XENb3HSApgWE060kou87b8kliqu3bbmkVjlMZPRmdr+c+Pr5AFjMMwpX8iMBC3qwF2kFKu.qbyqvPAUAgLuDqkBLJGscFZjkhy.BkCkPhRoQIjHQjuxnr4A3902suBk.mTbuggkS.III7+OC.Zfh.fKTnPgBEJTnPgeqROfi9EGmUVoIuv24IYcCWCKPu1sQO3fjXrXcfzYQHc34IAbb8adKdm28CwOJjG+IOHiDFRy4uCqp1vn78XoEVfRUqPmrtnDBTJAXbXwQpMkaMyr7YG4X7u9+1+L155WGozklKsDabMSBVG2Y9EYPuPzZM9d9zocCB8CQpC4yO7Q48+vCSPkZ7RO8SyvQBZ0bQLI8nzPCROSLys3Br9o1D85sLRaFljDDdJbXYwFs4zW7JbjO8y3+k+G+ehcrtUgu.5E2kRgZDBGoNCQUFjdsagG46YWEBxLFZztEW512jSbgyvKdfuGaciaBS6djzoMSN7vjl0kka1jvpUvHx.S+de0BNbXLFRrojPJexG+gL93Svyr28x12v8icoEIq9hnCTnB7Yo1MwyODkvGoSRjWDAZEoI8nSm13spw4B27l71e9Wx4uwzrtsrEdtm4439qMLsuyrLBNhvhsSOZzXY5ID37iPFUhLu.BLMwZAsxCouOswwMVXQN7UtHm6FyPogFlcu88vAevcxTAADOyM4ZKNKUibriMtdZXRxu4HjmgZjfzIPhBsThx.JKn5GEoCKBIjleeU3xSOC231yxWd8Kx0laVLABlXcqi8u28x5FeUrwQmfn18vqUaxleQVNKAsKg.kkn.OjYFPXwHkXjfQAHE3jd3DfJvGmwRRpCQpAkC7bJ7ER7zZhSZ0+8EXcNL8GHV2aJX4oxq1YxGRVt6978UD.bgBEJTnPgBEJ7MHiqDBQFH6AXvIcfShAMBmBeglR99bmYtFCVsBkpN.0WtIDDg0o3DFG+0u96yiuuMwe525IHzzEhsL5Z2.2tw7L3f9TeoqxlW0F3Nykfo75oiSvO63yv6dtY3+s+M+Nr9zEobZ.QkGfFwcwSjhZHnmYAhS5xppsd5z0RytJJObIdyO+h7m+W+WwNegmhe3A2LAoKR0dZFtxpoaKKFkiJCWEmqC5zDHsJskCinTElI0wqb3Ohjlyx+C+28eO0DwP2DFPafABnQ5R.ZlbzQw1tECjFftxPb05cHXvgIgH9qds+ib328S3e6e1+M7L2uERlkLSIJGLBBmfdssHEsQIhQFXfvxr.9TpzHzFGu1mdAN9mcA1+tOH+vMOJF4RjpxHqJrRliPmGCKp.sjH8UjPLhxJZk4no0GyfqmO5JSyO8m9K4YVmlGay2G6Z3gQN27j0sMgCL.wpLVpUSzZOFLpLtV8PjJQWIh4a1j5RCSd+agW6NywQ+xuflyMCOyCuCNzCtSp0cY7pOOkhJgUoooJfVNIDVhHMncFTIMQzoNBF.qWH8pUi4xr7dm7q3HewwHLLf8+f6hm4g1AUR6QoEuDZiAsSPzPkHS5wchkLPhEsxCeOHUXI0FCNG9BOBEdXxRvZgDcDxAGh3xgLS21b5YtA2X1Y3C+nyxfUqvFGeLdo8tE17HCwpq5Q0PAdoqPxstMVgEiPB9BjZMBiBgwGoQR2R46QXtaFcMVzVvWjO1lUl37xaNK6dY7UoTHzZrRKnD2q7msVKt6NsmIOKvYoo.fTjuSfkRMRo7dYItH.3BEJTnPgBEJT3aPBgk7IT6c8alztBx6swdc6fueHZ+.Z2oKoNnRz.zsWBu8a9wL9plf8r8cgGRxRRQKkXwgTo.rLxviP23D7BivurfO+Fs37m9Tr9Me+L43SvfUG.sVRRJHTfPqPq8PHLTZnRrR6kQHFfxCUhoWLlibzOigFXPdwu025qUVr4e35+XoP.NGCMXMt8cVfRiuN5.7luwaxhycGdhG+wY8qZXna69GH.KRjHw5DHc4GWjZel6N2gImZczwA+8e7ulC+Ae.6bG6kW741CjMc9OrvBBS9wu6EEjjRgU3V0WBQ4gIE3CO7WxQ9jCy3iNLG5wdTr8VDHOKmBmCoSfIe5IANKdJEF7nQq7.v8Tg7Ae5g4Hm+FrksrINvCL.qZrIvYrzqWGr1LTtLjHvW6wHCOBSesaPUcIFd3wY4NcHy2ivxCvUlYFdue0aPYee1212E6bSaggJWEcutfmhLoiEquD5nPJ46kWtu2ceDq7wghfAGmTSBWdlo4KN+E4BSOMiMz.r8GbKrqMsQ7rFTVCJmk6UKv8mfyHbDKyvjYHKwhxWRfmGBghz3DVJtMd9ADMz.nBh31MawYt3o4b25lL6J0INKim5Q2CCDFwjkCY0Upv3gdD44.Ry2YzBEFgp+KsDD4AepxO8fjjN4SaaoDkJ+beq0Plwhw.UqVBqUfTIwXHO66YFLwIXsVVNtG95.hhBHJpJdA9fTkGTrSPnVmWt117fjSrNb2MXYmqH.3BEJTnPgBEJT3aRh9Aq0eSl1+A4A+JbB.Cc61kvv7.fWbokPGVAGBt9ryy68puB6eu6gmb+OFd.sSSQE3QhIAOOM851lpQCP8kZRkgGidN3Md62f4t00328m7Gy3iMFkCKAFGolDLBKZopeOeJITDxLKNCiM0jjIg24neLm3K+B19d1IO6VuOzYyiBAVAjorX6ujUE8GZRFqjDmfxR3RSeGd8W+UYrwFie324kXPDHHOfNiPCXAmEkCzVa9wlPn0xF7cv0VYI9E+8uFCVY.9C9AeOFQH.qGNT4GCkIHMRDXQ3.qPwJwFhFXLLxR7Ym9r7SekeNBSJO09dH1vH0vLy7HcPPlDkMe0DYTVLpLDVIhT.sGhfgnM9b1abK97SdbjYRd5m5wYmqtJNigz3tj5Rwyi7LSh.ekDWuXpMPM7jgrbbBK6.Q4pboEtMu468tnZkx12wV3I2wNYMUF.UuNXLVhIkVwcvE.AQd3gj31wjYMnBCIyuDoNCyE5yoO6k4KN9WP8kWl0rl0vt2wCv8u5IoluFai13aRPYbnbfCINjHbBTNIc7SIP5QYmGzyP1x8v3DHCKQ3P0PUqJS2oIm+5WjSdgywsldFDBESs5oX0qZMbf0LIdBEkkBBTR7s.FCYVAcQfUFho+MGQ55WJ0x7ysb.Qgh98wq.Q+aBhPHQ1urqmq9xnz936EhWP.5.OTx7I5ry4vavZ3bBLlTZjjQ2zXZ2qGch6QRVJqzrAFgfTqgTqAi0jWx2FCNaQ.vEJTnPgBEJTnv2vr8atT4Wq+DyyPWdEdJvjYvefRXPPpSPkR0XEii29i+D51XQdjGZmr9gpgMsC4y7JG8haSoPOhajRrSAdQj4Tbpommid7iw5mZBdtGcG3KZgMKijjXRMIX0NLjgKyRVOKNWWB7KCREWY9E4vG6ynx.Q7rO1AnLBzt98VoDx9Z8XorejvKrxJL1pWKyljxq8qeCRhi4YOzSy5GpJwchwSlgSjGTFnPZbnb8C9UXYgFKyDqY0LSVJ+E+M+sTegU3e4+7+47v2+ZXoEWgwK60ei1zOquxLbNI3.KZlY1UXUqa8bq1s3u7u4uiEWXN9C9A+H16l1FSe4KQsHKBiDOCHrRPkOogyDl7jIG6PHBPWoFm7hWj27veHUpThmceOJab7AQ1KgdwcI0EimuDu.E3xvljuygWdwko1nqhV8xnsCT0pwWcsqx6czCyrKsD+ju02m0M9DLoeYbMZRVROTdBTAJhSRHpbDJoirlwjzoKB+.jAUI0Cp2sG+5ibDt7ENOIsZwd19Cxi9P6hwqTB51FW8UHDCJa9sWwg3d0Zfv4.gEsuOtLGIYFDNIJ+Hjp.RCBIwOfSc9Kx4m8lbtqcI5E2iIGaBdvsrE1xZ1.iNXMFZoafjLjNAhDABmiLKjQ9Mfv0uhFxuwN19u18q5AAHRbXc17AclCL.VD3jJbROFYsqGCJRwQ6LCwIozsaO5kjRVVF23BWitc6xJMVg5KuBMa2h33DLXv5.kVgUPdeEC8Ge02cRPWzCvEJTnPgBEJTnv2nDjWZp4ANd2fEjHxmEuHEJ78y6awVoonKWgLfSekava8QeLO7CsC18V1H9BnSmN3EoIylRRZWJEpnjeDcZmP4wVKsbvq91uEsiayO9fuLS4IHqSVdvJVKnE46WUoDWpAxjzoWJiO45XdG7Fez6yMl457cewuKO8t2FY8L34x2QrYxeyuS46J196OVkjLG7VezGvGc3Ohm3IOHe6CcP7EPVbOnjAHOXIgSgxlm8VIVLBnkKEgCd+O8X7YG4D7X66w4POR9dNthJEg0OO3WQV9VdUXwgBG9jIBo1DCPKb7W929y3RW9R7G9i+w77O8SP8KdE761BYPP9.BypQ4j3bYXH+lHXjV7pL.Ih.N+0mg26S9BVdk1767hOGGZOaiUlYZx5kWxtZOEdAZjBCoww3LPfpDFsh6rzJDq8QMXMt5r2g238deZkzku6286yAV+VIscaRWZQblTB7k3oCQF.5PMcZ2AiwgLSfWTDdUpQaohKL6bbwouEewoNEiOxHbv8rGd3stYF1SR2EtCpjNLXoxjljWV3Vg.iTlemJvRdUF3P2yQ6t8vjAQUGjnpCSyzLt3zyvUlcF9nO+X3WJjwmXT11CsQdvMrAFu5fP2tz812lTQ66UR0Rm.rJDREBkFMx7Avl4d02.JWFJ2cuIGPm5Yn8BHHvGUTHD3gQpHQJHVJ4FM6PijTlqwxLa8kXt5KwBKuLsa0ldIwLPVdOD6rNjRIQggLxPCQ4RQD4GvXiLBJDHQfVpPpDnjBjRERJB.tPgBEJTnPgBE9F0cCG4tekqeFUkVGRWd.xAAAj3bzMIEc4ZrLVNxIOIK1nM+qdtmhUUtBoIcvYyvWGPZutnEVDNCkhFfNwsI0Am3p2hi+UmjMu06mG8g1N9.BiEmHuue8zRj57b1IbJPpHySPJvwO2E3ve1mwHiMBOwAdD7EPi4uCkFwGIRLRKNj2qmfcHwJzDNXYtzByy67AeDgkh3kegWfgPxBydaVyHCSGS2+AGODt7xeEghLsfgFdJN50uD+c+7eJ0pMJ+I+A+gLlufYm4Zr9IGmtc.AI488qrevz.VglT7IHPxeyu3c3Me82fCbvCve5O36Q6EpyMtvYXmabyrvxKhJuhk6m8w7rG6HurrcgdLyRs48O5myr2YNdhC9HrmMuIRVYQTYMwuz3nbFrxDvZIMMlzDCAxPj9g346SpShKpLm7xWhW6sdSRMFdgm643g111QWuAY8ZiVXnTkPjx7r2mllgzWiv5Pp8HpTUPExhwIb1qbcN5ENOW5l2hs+.aisukMyVWypHxlQm4WlHmgHuPbIYb2fesBINg7dY9T5xK875K0gpCUinIFl1NGmdtY4zW4Jb4abKVrdcVyZVCaXsqicroMxpGdP7xRHc94PjFSEbfmGBW+hXvIPHAgPgreu9lYM8+Ka+yxERLBKVY9cLoz51HHjjgitlTZ1qGK0nNK1pIM61kC+4GCivQpT.JMxPezA9L7HiiRqYuqYJB88IJLhJkJSonHh7BvWowS.dBEJmEkSfBABmEoPjesUwPvpPgBEJTnPgBE9FlyhTvWqzTy2.uBmKePEYcn8zzNKiDG3ihSd9KxI+pywl29t4g2xFIjT5ztIUBCyWoLoITIJDgwQhyhW0AYEb7Fuy6QVpku8y87r5gFjzNqPYsFCNxzBPkmIXalAkQgT4AJMqXc7Ne7GQu3d7idoe.abrIwCnjuf6k0Zwcylc+AYjPSlvikii4m9Z+JZzpAemW76xVFeRz.ABCJL7a1WM8+cueOPaDPpPRcR3u8m8yoYyF7e1e3+T1XsR3KfPkhrrtfH..jt7nVsBvHjjhGIRAexmeQduO7iXi2+l3O4O72GefYleVJ44gxYHL0gCAoJa+LYlmkTgExzJt1ctCm7R2fkZUm8u28vy+v6iJYIrzB2lQFJjDW9ZARXEXRL4Y9UFB5.5jAIAdzQp4p2917IG6KHtWLuvy9r7z69gXk4Vfr3NnkBzk0fOjZxW4SYcRg1oTZvAw4GRRPIlqSaNwktHe0EtHcSMbeaX87x6aWLXoxnh6PxJqfuvQ4xkAqiFMaftTo7Lw2+3rDS+BgNOCrhfZX7Ff4RLbgouIG87mkYmedJWtLaaaaku0ANHCq8nBVbKuLcWoNBSOpTMfpCTlYMd4Ao6x6wWgCDFGRqEoED86qZq.rRIFgjLg.qJ+397C3SylMYtYuC2Ylay7yMGMVZYh6zESRF0FZ.JEDxPUGjwGYTFe7wYrQFggqUiRQQzZ4agRpPqT48htMCSRaRZESZZBFW94F2aOFaM8OGihL.WnPgBEJTnPgBei6t8tJ5ey.vxB86XQLNKBkf33r7ckJNtzkuJ29NyxK98+QLVP.YocvZM3WJjLSODFGUCBnSOCK0pCgCUlasPSN5Q+blXxw4f65AI.Kw1dn8Ji0YxWUMNKDmhK1hmyGYPD8zdb0YmmSe1yxXiLFemC8zTE3l27xL4v0v0O3l68du+fVxH0jpzb4adc9vO4vrtMbe7cegmmPfrtMX7QGlFKs.hJkxmLv3xmhyBAVjjp7IQo30e22kSc5uhm4odN9AG5.XhyXt4lk0ttwncbCnencBHuOpQhQ3ShxiXE7+9+m+GHyjx+5+K+WxVGaLVdw4Qay3AdfsvYOwoX0UGAiDbRKFrHsJbn5GnlGe0UOEe9o9R1752Le+W3oYUZEycsKPnuOAQk31ysBAZO7kBDVHPEgzOhNFXw3DTQZN9EtHu068dHbF9we+uGOx12NtUVgpY8PJrfBRrIzsqAsRRTkHJ4GQ2XGoFE8Rf5Ic3BybSN1YNMKr3B7n6ae7RuvKR4YtFttqPfVwP0pRRbL0WtANADUtZ+rZm+2FvhEEBgDW+dvcnMtQN+UtDe5WdBt30uJVsf6aiajGdm6jstl0gnYGr0WhFcZPYOIiNTI77JSytKyLydKDCNU+dq0gvI5OEvk3vgEIVYdFnyjBLBMIJIoZAYh7uuewa+qowxKS8ElmNMZQjuOqYxUyt10NX0iLJabp0hu.7rN7cNbFCDmgY94niwfmeV9.Oy4v5bHDNjBAkjNj9JDt7axhf7L9d2LSKDBjESA5BEJTnPgBEJT3aZ48i4cc2rScWYFG9x.VoacFcz0v4Vbd9Eu5ujUO0F3k+1OFwKcCBKUlRCTlF85PfzQ0RkIsSO7zgX78HA3u7u4uC.9i9Q+dTEABQBgAB50pK9UKwbMWfpCTlgJMHo1X7zk4Nc5gtTH+O+m+umzzT9m8S9iYPD3haypmZBRi6P6dFFu1nL6hyQfmOd9QzraLUGd.VJIk+c+u9myjSMEu7K7RTEIMVdIFP4PURizSA3gIMixUiHyjvxKWmgFaRTReldw57u+e2eNu7u6uG+vW5EQYcn60ggJ6wxcaPpKiwKGRil0w2SR6tcoQhiQFebZfi+7+O9KXgklm+s+a9uhGcWOHxrDFuRD5Rgb8KdCpTqF9h.VoaaP6iPAwYVJO3vzHNiidpSyG8weJOz9eXdhcsCbsWfU5zkpdJL9Qr7JID5oQ3.kSQfeYLNGMiMzyOD+gqw6drix4uxkYjZ03fOx9XOa99QtxRXZrBCHfDoCsVguuGYFHKNglwInEAHCKiW0QYlEqyGbpuju7bmlgpUge326kYG225Q0bYDcWDoPgSDRpzRlvg0Wg0BcRSwWqwZL4A7o8vA4qBHoBse.+pSdD9rO9SXgYmk8tycxi9P6kQqVk.mivUVgdqrLJRQWVizWPhuiTQF1RAD5onp1irrLRxrjZRISHQH8Qn8Pn7nSVFR+PvOjVYYbyElmycsqx4u5UYtEliZgATanZruMtYV+ZVCSN1nTMJBeg.EFL8Zfxlgn+t9UhEkyhV5PH.LA28JG.xCr2ANqCvgx2K+lz3rXcNRI+4ct7RytH.3BEJTnPgBEJT3ejI9ZOVo0rXykYfQFmd.m3DmjnnHdfssUBDvfgQjkmdKjdZTXQZbXxxK+4RkFhO9RWiYl41rkMuUV+HiAVGoYcvS3PnTXwgeP.Jolt85RVuLhFYHp36wq9kmDmRxV2xVXMCOLgFHsWOrZEIjwf0Fl4arDdBEUKWgtcRIrx.TO0v6+oeFqznIO5Adb19l1HkAv2CQRGZ2tEh9A.KDNL1T5zoEVsDmxmY5DyO+0dC1xN1CqeMqiQhJQILHco3jfQHvJUrvR2NeOG2MlDihwmXJZ5fW8s9.N9o9RdgW5EXaaZcTEARSLANvqelhs.M5zlpCNHcbIzncWzkFj5Iob7KdENwYu.6b66lsr50v3k7IxEiRjfTIAUHHDnHg.c.lLGc5FiHLBYkpzJNgadqo4Tm67L5nivCrgMvlV8j30qGttsw2kfumFqmNuTcyrHUh78XquBDQX7JyzKUm29S9TN0kuHq89VKG5w2Oad0iincS5sxxLRflDikjjd3LNDJe7iBQIznjRxRSoZ0AoU6trzxqPvfCRkQGiKdyawm7NeHmq0BLzPCvCsymgce+al0VqF5dwX5zBQyNLbk.LVHwjP2rTZmZQHc8yhpjkWbA7iBQFFfVWhNoF5jYx6wWgCYXIt0rywYuzk3p23Vrb21DTtD0FeTV2F2HO1TSRfmOkhBnTXD9JEhjTrow3RSHRKQhEoKO3WQ+0j0cKgaqK3dWqzeCKQ+V5Fm.50INeHoIEjGwb92jn+iKB.tPgBEJTnPgBE9GSBG35u1ZDfSqYw5KyDUFkKWeId+O3CYpUsJN3i9PDAn8Bx6aTqEomFxLjllhy.IBAFfe8689zrUC9we+uOaXU0Hqwxn7x.eERsBq0RovHTnncqUPgOoBniwwq7K+ETpRIdhG6QYMCN.xtI3RRw3bjZsHPyxqzf0Mwp.qftYVpV0iKc8avq7puJSLwjbvG4QXMkqRVbapnE3PSiNMnR0J.JjRII8hIy3HnRU55fidxSwa9qeG98+I+D14V1LiWtDgldXHEmJe81.R5kFiAGs5khW4Aw3fibhyva9V+Z77T767891rwgGFkqK1dcP4EkebVo.oj1ciITqv00QfWHlvHN60tIG6Bmitoo7h6X2rlZkXLsAautjRWbpR3Y0nMdTxjg.HKyRJRvOe8Ac0omli8kmDiwvlV654QdfGfAENRVddbocP3AwDSy1wToTI7EBRRRvXAYTHskZVpWK9kev6wBK0f0ut0xSsu8yNV85PzpNosVgQTBbJOrNCtr792Vo.sRf.GNSJYwcnoMCoe.UmbLluaBe1INAm3BWhqeqax12xZYOOv1Y6abSTVHvr7x3RSvWXv5RvXs3TfTpQYkHxbHQhGJzBOBGWR2jT5XLXjZrCL.BohlchoQmt75uxqhIyf0ZIJvissp0w5lZJVyZVMiTaXlvzevkYMfwfMMkrrDHKCbV7r4qMIY+QktDY9JbpeUSXj+mb8S+xvm98ibfJ.I16sGhy+g5uNlJFBVEJTnPgBEJTnv+34tC+p6xJx6vUcXIRAN1W7kb4KcI9S9S9i4AmZUjQdPnIYFbJIXrzqWODFKZ+RHCJyYmaANyYOGSs5UwiuucPYArRROhpTFCIX6WRngRORRSvIDTZngYkLGG6qNGW8RWlm44ed12N1N9BHItcdl8jZDBAcx5R4xkIzOjEVbYTgUHwAe5mebtwMuI+W7u5+b1xZWGZf50mmR0FfvHe7yBwhDkHOahc6kRT0A.UE9zyeV9fC+IDEEwis28vZGd.hDfIsGYNCh9u1JmjAGYDludcL3SsnJbs5s4Ue82DSVF+9+t+PV+v0H.G8Z0DuTCVkkrr7bDJ87wqREp2rEA5.FXjQ3bKtHG6LmgtI8X+O7dXCiLJCISwOIlNIcIwl2uzJmCQFfwP2dIzS6gp5PzRp3R235blKbdl81yvS+3Gjss10PYrj0ZEL85RnmDmF5ljPPPIDBAIIFhy.uJUHMHhKeqawWb1yxEu9U3Q2294o129YhxUHc96PRikX.OICTsD0i6hxKeZWa52KrlzTLYNRSSwy2GqViw2i4Z1lO6TeEew4NOxxUY2O5ixuyN2DiNzvncNVYtYIoYCFrbHBeObVGsa2.UP.9AgnTgXcNboNvJwhja1qMROOTQUIUoXgFc3xSOMm+xWgYlYV7CBXxQGlMceqmMu10xjCWkPAj1qCIMVLON29mye2x+2SHxC3VnvZymj08aI9eyDSueuyeu8+7Wi6tCjM.eOe.U9PRyXwZs3bh709EE8.bgBEJTnPgBEJ7Mq6MAn+Z8B789O2KI1XYngGikwwm84eNCN3fr2cuCh.Vo4RzV6HSHHP4gy4HN0hWfOxxUHy4wu3W853rvit2GlZJAwcZgBKAp.VtULRIn.TNGocSvuTEbREWZ56va9AuGqd7I4g15VYhfPj.sL8HHPiDAdBORiSnRkJzMNlDqfJkB4HW9p7AG9SX6aeW7TO5iQIm.RaQ4.MoY8P3A9kKSVpEOYFYNCVm.stBMbva+QGlKckKy26k+Nr9QGHuuiy5jmgTLnDJvk+dNvKhVcpynqdszyA+zW6Ww4t3444+VOKeum7..8vjDiylgNzGqRQblASZF9BMddgzoaa7GrBKllxmcpuhad6oYGOvN3w28NobyVnM8Hit4qnJc.HUPFHsFr3HAA5ApPZ0RbwqbM9nibL51sC6Zqak8+fai.SBoqTGR5hRK.sBTBDdBln1nbiomgFcSYvwWElxU3hSOCe1IOMW4ZWgm7wdD14V1DqphOzXIzIcYvJkQJglMZSl1i.uHTJAwwwjkjfT4gxyCq1mfAGj1FGmNLGWCS...H.jDQAQ0JWhO9nGiYWZItuMuI10C+vrpUuZlZw5XVbIRxhwylRXsJ346QlIkDxvojHLNb8L4SHaiDqQhwlWQwloVMIFCKTuNW3RWlydwqP8UVgxQkXpIlfm+YNDQBAU7gJBHrUCjocQ1qK9YY3ENDP9tm11+zeiMOPUiygPkGhpSBtuVyA35+PE8xu5Q7O7e3tecGSu6Efsi786Lh6MrxKB.tPgBEJTnPgBE9GO19MuX+uR.oNvG3He9I4V251ru8uetuUMEdhLBr8HUUBoTPfxGaZBVohnxUoiSy0qu.e5mdT11V2NO1d1CZfkWdAFLJfzzTxhMnKoPYc35lgKNC+pknCvwu343bW3B7hO6yyt1vlHP.sZUGmu.hTXLNBDgnCjXRMr7JMnTsQnMvq+duO0azh+o+o+KnVnGtlsnbYICL7frPm5zsSKpFNb+p8NFmShNnB8bvIt3U47W95LwjSxK+ReaJi.sIkrjDRcFLZInk4SvWiklYco5.CAH3nW7p7oG6nL0ZWKO2Sd.h.xRhQ4L3EFRfeIrYBxbfyXwlDiSpvuTYZHfic9yyWdoKxviMJ6YKalpXPkzFrwfVfxqLAZMBqFINTZH0IPWIhzRAbyEWfSd9yvz2dF1wl2Bu3S9jDYRP1qCRaLdAJDBMVbHs9TJnBMWdELFGUFaB7FabN9EtLu969djjziGYO6im+w1Op3VDO+Mwy5nTnO9RMYVGJ+RHEBDRObYVx5kgy5PF3gJrDYJOloYLG4TmjicxShCG6c26g8t6cw30F.QZOxZ1HeW8JcDUJ.zRxRRwZSQqT366iIygwP966.MJcP9970IX5Ns3ZW8Fb9ybZle14obTH6c6amc+fOHaXponWqFnxhQlzCxRwYSQKfvf.zkhnQlCgvBRIRQd1fcNKlr7oRtm1+dWK70tJ4dT33e.A+CdFi0hHe4DiTpQJkHT4AbWTBzEJTnPgBEJTnv2vr7eZSL1OevBvITXERZiiO9S9L79+k8duBVNNSSSumeSZJ2wavAd.BKI7D.z.Z5oYS1SO9dGM1chdCoUwDyFRgL6MJzE5RcmtUgBsgjVocCIcwL8NtVS2yN8zroEjDdGgkvevwaJeZ9M5hr.HYuJTnKzx4l7AwgIqCpJqpxJSD06+2626WXLeq27aQDB5zZEFsRDqFHQmCjag9VTx..Mym0j26S9TxxLbrCeT19jMncqVDnDTsdEZ2Mg3vZ3zV74NDYNBPhEEewpKvkt0mitRHm5nmjcLwvXyyneRWpNdcbHwzLgFgUPozzoWJpfPTgg7QW7Bb0a74bzSbBN5A2E9tNzYVjwVP4wq.q0gw4P5k3cY37RpTeTtwiVge7e26SXbCdm24sXLk.YVJdSe.OhvPD5hJEpLVT4F5jAiL9Lb4EVh+p+1eBQMpyu5u1uL6YSaljdqyP5hvOJWowHkXvCAADGFfL2QfE5qkbg6ce93abchGdXN4KdBdtIGC6xKPrv.RGNkFoJDrJ7FGVmADN5obnqp49qsHm9RWkmrzB776eu7xO+AYxJwzYgkHT3QDBH7X8BvoQXU3MJ51rMCM93zY3g4b2+A7yuzEY0zLNw9N.emW9UXTWBtj9388IJTSNPuzTjppTq9PP+tE150X.ffnJPPDKzMgGs9JbsG9Ht9cuGx3g40e4iyKu28PT+l3ezCop2ACMBZmEm0RdZF9t1hETQpPoTHDJL3vnE3C03BkzwzikWuIq2oE+Yu+m.VOiTqFG+XGlis28v1mbZDY8IY1GPj2PnRQXnlvZU.pf0ZocdN1LK4jUDnVdwWNdhjfLjhYgsHCnvkB9Aks8qcEySy0J+WpP18zDvBHTE7rGg2I.mGmwNv5+kU.tjRJojRJojRJoj+AiAl2bvsJ9R6JshEWuM29N2hwmXZ12N1JV7jzsIyLzzX8RbtbxRyvmlBUT3Px7KsHm4bmiwGeB18t1KZfNI8YxF0HHHjrj0YxoFmNht3xRQ3bDnBoGVt2Ce.OY94YhImhss4IHDna+93bFTDhEKlrLTA0HKKGsTQiwFhdd38+3Olt86wa9seKp.nbdpHUjzqEYoIHhUEBgxYvb6MGuWi.39OZN9zO6rbzW9j7Zm3n3.74IHbNPKQEFgWMHUfMFDNKQQ0wAb1KbQNyYOCuw29s4UN9QG7ZtG5F0vXbj4xI2IwZDnzZpDGg2kRnURmNc3R23FL2JKxoN02hibnCP8VsHIoG5ZA3DB7JAdm.WtCSRFHbDpUzWZPXR41O7db9qdIFexY30dsWi8M4zL+MtISUSSr1SFN5jzGDATMpAjqnSqtL8XiACOBW3gOj+pe1OmlFOuw25ayqu+CPPZO5u1pTymgT6v6xwI8PPDFqmUa2EovhghYZaTTEBqDSWqfGM2rb1acWt38d.G3EOIG+3GksL5HXauL9NcXBOLjRvCcVBT5hdO2BgJM0iJBJrd85AnvA3zfQ3X89M4tyMKW5y+b9h6eWFdqGfct8cvQ229YaiOAULoXVuIhjtTEK0iivZyIOsO85CVEfLDUPHhPEQhDbCr7ry4JBuKg.sRgTHvaJBIK7f.6+NiIruht2mg7qbebFONWdQHkkmSVlov8C4EAyUo.3RJojRJojRJojR9FDouHLgbVGBATMrHUeaasjpBAYH+o+e8mS20Wg+i9C9GwTBKcauLiM7TL+x8gfDlZ3QIJBV5Icn5z6j1d3c+nGvsu1r7e6+0+Wvth6QcumJinnStCahkFiNLc6zhZ5ZzMyxZx9Tc3IX1d83u9G8d36p4+7+S9SHz2lr98HRmw30CIOMCkSxvUphyzmpllLWy9zq1A3O+bWj2+rmieye0eMdycLCiXywPGhGpFopgIqmDS+TzRCVxQHgd8sTYrsx693U3+se5Oissyswu+280X6hNzqySHWEfo1HjmDPceMXwUv6So1Fpwb8VgdCsItxWLG+v+5yvj01M+we2ea1tPvRKdchqJnYHzQ3QohXLUHUk4zoaS5zoKBB4gG7f7u7ew+C7n6cW9UdiWi2YO6hgmednaSzQg3qDSurTH2Rs.EAJEcUVZCjqTzYmuLe3G7t7ge5EXyaXF9G+cdK1ajfzadQ1rBREZr0Zv5s6SdpmoGpApTncm0nVsZbsMuOt8suI+rO7ivZ5y2+0NEGe2yP0tyQTpAoWgwFikJHB03Ed7NCReNQAJlzkQy7D5VoAYCOEyaDb06beN+ktEO9N2h+Y+g+dLUrjoSWl5KsDVqgbgk0qFPKcHCUaLVe4kIKoMiVoJMhBIOyPRlkrfJ7jjLpt4sPZ8Ht5stCm9i+HV5Qyx1ldF9tm52f2deSgzCp70Q8j0Q4JplqS.Nsj0xRPnUHhp.3wkYPXrHLdBjNrUsnDEhdcHwaGDRUYfGI4VOxfXjAQHBhvpKrvdl0gwYYog1LY4Yrd+NrZy0XwlqxJquBsZ2jrzD52s8fl+khNLvKd1VgnrBvkTRIkTRIkTRIk7MJRoD+.6eJALFC4dCdgFkJj4Sx4wO5Qr4stU1412Fdb3LVBhCvFXQD3HO2fLIGcTL.73USXgEVfMtoMxTiOF0pULqTyMFbNvgEIJPBNugdI8IbjpXAt6cuO850istksvziWEYVK.vIjTTU5udI2Z2Igo25N4VlLtxEOOyLyFY+6YOTQnHIoMZjzOMgboGoTR0fJEyfUqtnF2w0nm2w4N6YYom7X9de6Wmct8sgk13cRhpUAuuH0fU0iIdzgX8UWf060CcbCZ4ge5O8uinPIe+u2uBiMR.q2pEfj50GgUyyINHFgWQVder8SQDDRPsFjmK3zu26gMMm8rycx11vLnDdRR6i1YQojjlkhRoPJjjkkQtU.AATItF1.IW6ZWg6dmawDMFliefCwvw0nU60QKgv3PbBGcZ2l55JHGtFca2CmwQznCgrRMd7ieHW4RmCo0wIN3gY6abCHxxvllh2AYtBKIK0JjJEdAH0fxpQKkLeut3hhPUeHRyy4xW7x7oW3xL7vM3e7u+uMC0HlZZHHPgva9Je7IAmf0WXApWuNUFYXRy5yxs5fyC5npnq0fmapI472417g+au.Ob1YINLfW+DuDGXmOGaZxoQ16IOaWZE7rfrBJpN6XiNJ8SSnW2DbVKABEgpPTRIBmmtc6iRoQq0nzZDgJrNv3DXbBFYiyfwInuwP6dor7RqwBKsLKs5ZzsaW9zq+EHjRTQAHC0HC0EBtUE8T7FGaTTREg5.hChnZXLwgQDoCPKUkBfKojRJojRJojRJ4aTjZ7FCJg.gziIOmLmEpTAAwblybZlat44c9kdcdto2DIYqiTTHlLHLjv.I1LCM6UTA2LObtKddla16ywN5gXCSNF0BsXcEyzUGJDBIBbH7NLtdX7FpE2f4Sy4zm4BnjRdoidPFBABukAweLVz3Qf.XPcqYUQUj9p79e3Gv0uzk40d8SwQeg8Rf.5jmRbbcRRRHSZHJVgVx.6tlgwoPVaJN6suIe7G7ALZiJ7c+kdSFRHY4UViQqUCmK.ouHHr7ltfRRtzhwooRsw4C+vywEN644kN1Kwu0aeHh7vbyNGaXlgPPD9rbZDDiykSmVMIxqInwPj1Li4ZsFm9m+drgIGmS9BGfcNyFQ1Ogzz93DVB0BxxxoRs5HQSur9fPQXbExhTrduNb5O88HqYKdk8sed4mauDmkhIOEciX55sXsfOMm50FBuWPqz1DMxnjORCt6BywYuzMXwmLKuvN2Nm5.GlgUdbc6Pk.EVSJdsGjAH0phOGLNDNP4EHbBVLdHFaxIYs9o7Aexo47W9xL7PM3UOzd4X6eaj0bcpWjaX3rBrthJshqn2niSSPp0zNKk0SSwqhoxHiRhJfVIo7o+3+Nt2rOjEVdIlbhw3DG4vb38rWFQFPVm9DjGgW.NoCmzgU7kilHoywRqrHJOEBeCzDpBQhBq0h0aINpNVuiLKXbd7JO9nPHHBePH+cW4BrduDVZ8lrzxqSqtsIK2fGIBgjWYOajff.pVqF0qWmZMpS050HJJh.kFu0gBAQJMgRMQRMQ5.BDJzRYo.3RJojRJojRJojR9ljhTu0iRKPAjhGgN.zgjgmO3iNMQgU33G6nnARy5Q0v.xxRIPGgT3KlMspHjwMX0bOm+hWjjdc3UN4wnVX.JrXLFLHPnTEIfq0ivYHwYnxv0IG3hW+K3xW9Zr+8uWd8W5nDH.7FjdvJjesolZg9IO5o1Fqim26C9HBjRd8SbLFWFfiTTBA53P74oHEJDBAoI8wXLHUQfPPW77i+4eLMWcU9m7G8GxtmXH.Cc61moFcTZ2sGwJIMpDgIoCsyRPWuFTcDlKoK+j+leByLyL71u4KSHPZ+1zXnZTq5vzr65EhePRZZNhrbhGYXxzviacO9rqcc7c6wNdgmm8uksxHxPRxVCgzhHPiQ.doXvmSB7REx3pjEDviVZQt1WbSVawmvw1694k229YTmjd85RbTDxpgztYSh7RpGVCYlmtIID1XXBmZLt4xOge5Y9XdvCWhCu+mm27EeQlpRL8WbdpnDDpTz0aPEDfPKwaASlAetCkTRtrnue0aYabm4WjKbtyvMt1UX5wFi27TuL6Zqah7VqPMsD8fywrth2GBTnPhBICMTCVqaeVwjiu5PTalYHQn4F29tb4qdEtxkuDaYyajW+DmfWXO6hMOw3DZbjs15j0pKgUB+RGF6k3jFbhh9zFgGAdpDVgJgQfE5kjQ+r9XEdjJMFcUzAgfVg04X0lqyim897n4mmUa0hmrzRXsN7nILtBiOwDrgMrQ13LafgGZT1spMRY.AJAApPTJAZQQJcKQQZutH8RDNKXxQj2G5Kv4JDQWJ.tjRJojRJojRJojuAI2Zv6MDIB.gGjJBqTm19.lco03V2317puwqwt15VQhAk2R03Jr5Z8IXnHH2iPnQWOlDObs6dWdzbyxF23zbvcuU7oIj6Rv4MHj5BwFRQwH.xlQFVpWaCLu2yGe9KS6NIb7icLFWqY8UmkQi0TTuWMdg.AEhZJlopBxUB94m4y4928Q7s+VmhirimCaVK5kkPs33hr1UoIJ.TRGYVKBojfJUv4UblGsBm6hWk8um8v2+a+ZDIfUVddFczQIKWfzpwYcTenX5l0gTRYjZahUbd9g+32kG+fGy+o+I+Gyw2+Nvl2EuoCiOwnj4gzLnVXDJmEQRFUihIrxPzry5b0G7PN2stIG+42GGX66fwTAXVeUb8SHtRDDVX44vvvhfTxZQFGinZUVIKkqc+6wouvkX+6cm7xG5fro50wr5ZD383bN51qKAwQH6ZITGSVRNBcLwSNB2Y4E48uvY4tK7D1yF2Ju9wNLaahwn0rOlPSNwQQjaRwK83TEmV3cNbFGBoBUTE7RE8M4bmkViO5C9PdxWbKN7d2MuwwODaX3F36tNwVKJqDOEYNFNABum.gBIRB7PlPhpQCFtRUxqTmmzoOW9V2hO+ZWiUlad9Nu8awN13zryYllZJIcWbQ5zsGMBqvLiONql0FXf0m8fx8z4JcQxLWQEiv.8LYj6AiRhejFHhBgv.ZqqSylM4IOdVdzrOjEVXAZ0ZMRSSAiiCe3CQ8J0XxQFioFcbFqQCpEEiVVDRVsVcYvlQVNCRXaGX8fyhvAgAAfCj3AunnEBPTbPkxwfTIkTRIkTRIkTRIeih0ZQJ.DNLlb7BHPVg0Z1kycwqhJHfSbriQUDzo2pTWKP3cXc4EolatGgJ.UTMVHKkO8BWDYfhS8JGm5Hvl2Eq0fOPVXiVoDu2g2YJRW43.Ld3x2bVt0suO6Z26gicfWf.AX5zFhFqHbhFjJ0BuGDNbRINujk7v6c5yxHiNJ+Ru1qRMfUasNgRA5J0Iyji2aIPGfDAJklvp0QHavJ8aye1ewOBcTUdm27MoJPy0mGkOmQaLEqrxxLV8QIsaa7VCh.M0GZbR8JN+UuN+M+s+bN3AN.u3gNHUEvpqOGiL7PHEvpMaSbTUBviKICm0ST8FjAb069HtyCdHDFyIOzAYySLNx9coyZqSXnmfvHxEVrdOQ5.RSsX8RDAQrdZet0ilkGO+hfHfu0KdL153ifsYGDVCUpEwZcaQGWe17Lag7t8IMMGCRBGYXVNKkO3reF25t2gssqcxau+WjMM5nzckkHueWFY3Z.V51qM53pXLVPoP3UfTgNpF9nJzJuOK0sC+4+8+Lh0RNxgODm742Kad7QQkzGStoXzAYsfnPbnPnPSQk3UBO3srhWRPiZXqVm6O2B7y+ryvs9hufML4T789tuEG+E1OwtLz48IsUJxrbBTEiHobWNN4SSs7hA5kvWjByBuDoWh0IHyZwnEnqWCc85z2YYglqyJy8Dt5iWkVquJqr7JzqSapUoBaeyOG6ZGakMN4TLbkHhURpH0D38PdNYqr.850krjTpOScjHJN2BARg.oyCVOBmGkRU7pSTzSxOcqWTTY+RAvkTRIkTRIkTRIk7MHEgZjBAPddN4x.TdEKtRK9vO9SX26curucuqBwE1LhCTj1uKUihwiCgSfUHw.7EOYAN+kuDaX5MvuzabJbdCQRIJu.iTBRMFuCqwfxYIVKgZMnIdN8m8ozqWW9c+9+ZLUrjzzlL7vMFX6YIBeg32hmIHWFhSn4zWcVdzSlkS8hGi8sisA1DpqzHCzjaMXAbdKjCdgGoH.orJq5cbgaeetym+47Neq2jW5vGhHArVutL5v0PLXbPoEfSpHIMibsBubXtaqk489nOgfnZ769a8avvwJr1lzHN.sPPZVeDRUQvJY5iwZwoCwG0f6ubBezYtHqudKdoW9jLYiZD68350Ck0RbPD37jYKlOs.XrdbQgjKEb6YmiKciqi.Iu9q95rqQFE50izr9DF.o9LzARF1UCeGCAh.56cjWKhUb84St9U4NO39rgI1.u9QdI1SiwoeyVj2sMUpDfU4I0jAAZDJIj6PJkfPhWqISERWaNWetY4F26tjsxh7Zu4qyKcnCPnIitqrJiUoFUhqSmlsHHHBmuPvmTHQHAOBrNK4tbVq1jzrYWdvMtCew8tOKu5xrqsrQdoidHN4AddV4wODSZBdLTKJl3QFBPRmt8Y00VgZiHKRAZu.gCzNILHYysRI5FMPqUXzRVIIgm73Gy8e7i4AO4Ir15MYk4ViImXB1011JO212NaaCSy3CMDU0JB8F5r1JfICxyI2lgBO0zZFsQH5QqvB8s3EE9RvID38TX0aagsu0gBb3wI.KdLBOF73DdbTJ.tjRJojRJojRJojuwQNHbkSM4XUZB.VoUat4MuI+1+t+dLVMM41dDGpQojzoeKpO7Pj4b3PSl0SNvbKsFy9nGyAOvyyHBIcVYA1zP0wKAzRLRA1bCdiovDyZMNpwhsWmqeiqiVAu7Q2Og.O5gOjCt6cSqdI.C5oSLn7EhzyEgjKC4hW3rXxS3vGX+zPHn8ZKyDCMDdsjt8RPGGBdCFiEu2iAM3C3wKuJm8Z2lPgmSchiwT0TzZ00XzgZPfVxJMWjJUqPZVejZEsMYjICPPDm4xWmabiuficrSxKu+sRyVsHMqMiVuJc5l.5XFowPzoSGzCpPoKJlLAb+EVgq842AmIi23jGG+8mEiyfxXHNTSTTDI1bxRyIHHBq0W3n1n.5487vEli68vGwg18yy67xuIpEuNoooDUIjLqg9c6wziNI0z0392+QL7TSSlDxpWga8jGxYu5kQffiu+CwdlZKj+jYIueOFZz5nBfkVcQTJMCO7vjjjhRpQJCvJzXDR5ik4Z2hq8f6yYtzE4G7NuEab5oYH7njRrBMo85CJIUhqAHw48HDdPo.k.i2W7iPQZ05byaeaN2m9YXyR4UdwivqexSvPgJV9t2gIpWAoJBuQi24IIoGND3kBhqEgzmf.I3AoWUDTZBIFklLghlc6AwUnUuDt08tGW9pWiEVXApUqAiOwD7G7G88HNJhF0pRsvPB8fsUK50uKsy5Si3HjdGBkGoTiPVzwwYtTxxf.YsAUzVhbP.sUrZMth9d1CBgDoT.RAZkDTR7phjWuT.bIkTRIkTRIkTRIeChRVXC5fvpzOKiQmdKrZdF+3exeGCMxH7678eaDdP4LDHDjkkQsZ0vXyvKzj60HhhIGO+n+l+FldlMvu2uy+HRM8odbHYI8QJ8jZE3TZhiiPFpf9dx8PFd9y+q+wr3SdB+I+w+wLYff0WaE15l2Bq2pCYDvnCMDKO+CnZDHCjH0wn0w7gW7Fble5eC+feveDG+f6EuyPsnJXLFxLfwYohRR+7DpOz3r15swFTL9it98lke3ewOl+C+C+cXSiTGuCjd2fwgCDEnPIczsUalb5svClqECM7Lrr2yO7u5uEsRyO3O36iM2vHwZhbgjljiVGgUnHIsOAZAJkh0x5S05af6tZe9u+ew+qzrYG9m8C9cIewEoRdJ5n.hpERVubZ1oMNMnkZvWD7W5ZUvUuJW77miO5yNM6641G+xu1aP+YWh.SeFcjgHUKn0ZcQ38j2MkDumpQ0Yt0ZxLG444iu804G9i+KohNhu2a9c3.abanVqC99ILVsFX8d5zoCRoFkRSV+LDdIQg0Y40aQzHiQsollqeiqyO5m8yHW4469q+qyI23jEsyZutHDPnRgTnv68EU90miPpQJkjCXjRbJM80AjYs7+3+K+OiKMisOyz75u3QXuadi351ltKtN0EB78SFTYUOVuCmMAGEtVHLPgL0RPbHxf.RyEzNImTAP0Pz0GhUVaUtx0tLW3hWflqsNaZ5o4sN0ax91wNYxQGkF8VBvfLsEhzhjiNDJl3VwZ.KHAqDxEhAVXVfSHwKDDlOHjxvgyOP6qGDB9ZBb8VOdWQJZ6GXCZnrBvkTRIkTRIkTRIk7MJE1r0SNVhpNDVzb2G+.Z2oKO2ysKT.ZuCk2gz4FXuTvI7.NZmYXjQj7I29QzrYS1292M0zJhLVhTBzRAFmuXNCKD3xMX8lhplEFxxd3gyNOadSyvt1xzHwSnOmPcLIVPpBoSZeZLTMjtTRR6Qs5afEsdN+0tMadSSxjCUgHLHbYffh9EV5Ao.qKmZ0qVDpQnIp5H7EK2kO6bWgglXCb78rC17v0Iv4IW3QoBAkCu2h25YzIFiU6zhZSMCswye0+12EuUyabpWkwBgvrDjdCEYvjpHrtPhv6.gi051ggFYSLaZJ+7O6rX.dwicD19jSxZy+XzJHOKoneQCzDGDBRI4NONglVc5yHadLN6c+BN+kuLaYKalSbjivH5.TYYDnjzsWGxBkTsdcpilvbM1d4jAL711HexMtJe7kOO0GZXNxN2G6XxoIJKGSmdDozXyyw38nTATSqKFIVNIFmfUVsIUGeBxBi3St5k3Stzk.kfmeu6imeW6gJsV.PVjR2B.j3kEglk.PoDnBzzIOg1o4niGEWbEtz0uIe3o+TlZrwYSSNA6aSyvlFpAhVqSPdBU0ZBBBneVNBknnOyATji26PHbH8Vz0ZvZMaSRdOpMwjLwV1FqmX37235b4a99bkqcUpUuNaXCafW6juD6YKagYFYLh7fOKiHSeJdUOPQ5.kotA2zIDOq2ccBFX04Ami4AmzPgZYGB+SGuWEaYvV4S+8HAewumA+9RAvkTRIkTRIkTRIk7MJEUjJw3nRiQoOv4u70X8lM4se6uCAdP6ReVu2hPNHClAg2QpPSlGd2268vjmxqb7iRLRpnEnr.BMduoXzGI.SdFFmEcXEbAQ7om657nGOOu0qdB12lmAeRSpnUH0ZDNHHHjlqtDachgHK0hMK.CAbsu3g7om+hbxWb27baXDpHr3MoH7V7RMHAkRgIKk5wwzqaJp353A93ybNt109bNxIdIN7ysUpqEj1sEVuAupFdeVw6OQQHFM2JyyXacbd3hM4m7S9or4Y1Duy25MnFBzCtuNJ5SVmP9rirdf9NOQ9.tvMuFu6G7dL8zSw240ecldnZz6dIDOTcxSRw4cTIJFkNrnGRwSlS.wUXotc4Su34XgEdB+F+J+5bncsajKzj5BMIwg0n0H...B.IQTPTAPZtAmUQsnZDY0jkaHWoQMVcVW43it343AO3AbxCeLdkm+fLoLDW+t384npDPRdFFigPcDgBEViGWpEu.Bqz.ebL2YtY48OyoYoUWkCefCwqbfCxz0FB05K8rpYZgAgRkCuxg0CAwQrd60vpCY3omhE5mymc1KvEu4sYkjbN0V1.6641IacrQw1tEllcnRX.pvXRMVb5H7BIRoBINTdOBWFBSJ3s739ZFZrMvXCOLq2OkKdkqxM9h6wryOOc5ziCrummMO0FXuaeKr0omhZAJL85RulqSZ+tTqptn+belf2u7ytuJe0v0ZvXFFgG5F9UBgKOfnXNIWHDdv1AWq7ra6KbaPYO.WRIkTRIkTRIkTx2vHDd7HH03HTEypIYb0qcSTJEm3XGg.mg.WNpAU+0NHXlJpAlkfQqxxYdN64NCaZyahi776CR5PkJQjXr3UZLdPIkHkfy6PoT3CBoiWwG9QeBA5.NwfjTNIKiZUhIOOuXj6H8XroEy4UolvgFm0M47we14YkUaxKevuKachQIPXH2lgSHPo738E81rK2PVVNNYHww03wc8boqbUBCT7q9c+NTWB1zNjjzEGLvlsf.EAwQz2lRsQGFKdduO9ioWm97puzIYqiUGgIGk2TjR0hhjp1C7zobiCIUGcJlKsCu+G9wzpUK90dmeMdgcsU5bmaQiPEQgZxS5iPVHUJseJFK3TAXzZndE9zycVd3CeL6eO6gCrqmCcVBocWkQpMBVofv3XPHIOIm06mgyBTqF1gpxGdtOkEVXQ15F2Du3dedFWGR95MQKbzndE7Jag0b8Bv4w3bXRMXb.wUXjYlhKbu6xouvYncuNbf8ueN0K9hr4FCQxxqV7vP9UNipXAUrTrKalzGaXDgMFhV44bwqccNy4uHwiLNu86783PipX7Zwn52k71sI.nRTLYdOM60ipMFcPnY4wgGwS+iPA3IZxYvFFyiVsEm8JWlO8bmi1cZyt14t3ju5Q3X6+4ohPPj2gnWe50uENaNgJO0FoBtLagZWAENaf+eP76f+ixWHjU9LwvE098oO9uJ9u7xjuxNoXievyChRAvkTRIkTRIkTRIk7MJBJDKZQhE3N2+wL+RKy121VYi0CPl2BsOEou3ax6EZ7.RxPfGkRvG8YmiVsZxu4w90Ylnpr1hOFckIwlavITXQPfDTdGNbDDWklBIOdok41e904kN1gXO6b6XconvSTXDsZ1jnpUP3sDGIoSVOrhPDpFbkacOtzUtBabqal8skMy3UqghhYJrPowIbEUZSHPFDPmdYD0XDx7vYtvUX1GOKG3P6kCrgJj2sIVuCBDDFDfCO44VjNMRBYs9cX3FajyN6r79+reJuvd2MeqW93XS6QDYf2WHdRHGHOqPdiWBVzHoF+7O7c4pW6ZbvCbD9keyCiesTtw8uKachwKVP.g.sRi.E4I43TZj0phMPw8VbQ9rqbYFc3Q3a+xuNiJzjuxxTKVQhoI1HIQp.b4VR5khTDfpwPrpOmm73GvYN+4X7wFi27HuDO23SS1BKRdmVDMZMDZK8so3UBTdUQOG6K5UaY0X7UqxsWXd9jKeQlawE3.uvKvoN1wXxnp3WaEh5mPd3WQ7qvgDWw4RBIFojLITejw4IKuJe3mbVt+ieL6byagCd3ivl1zVXlzEHuWK52OAg0iLNlLf9dGxvPbXw4.u0QfRgHPgTFgwGg0anWXEt3EuHe1m8Ir1Zqwl2zF4cdy2f8sscxD0qhucWTlb7lzhYor1gNPfHXPPTkDUbdM9h91cv6ihy1KjDKFbcx.iOWjH53PHf.aXgklE7rsB+SOl7zQ2Ug8vE9AKPBe48uT.bIkTRIkTRIkTRIeChBAoNGhHMcvyEtx0wIjbxSdbj.AXP6LH8BrB0.q9xfJa4nMd9vO8SX5YlgierCQ.PfIGWRB38j6bfTfvCViAANBkQzrSONykuB38bhW7XLbTDcVeAFJLBPTLigwhKOkFUCoaRJ5ZEyQ2O8heNsa2hek25cXrp0HVHvZxKDfok3whyYIPoINpBc50EqLfEaY3i9ryfRI4Tm3HDSQR7FDDfNTCBIV6SCrHEY4RzgUwA7ge36w5Ks.+Q+d+ALUjfdqrJiMZUrYC5QzAGRThBqh67ZrxHlsUBe7mdIpDWiuya9ZLLBVp0xjzsExo2.4ocwYcHCzHQgDIgUGBes5rb+t7QW5BjIfW4.GgcswMQxryQj2vXSMBKs3BHHFStEW+7hPqZjgvTsF2+92gO4BWDsHfCrscy92v1P2OijzDpTMBcHzIoEoNGwAQDnT3s.gJzAgzWEPaWF+3O3cY4lqy12113UN5QXKiNFqcuGfpWO1xjSwx4cQPgkdEd2yp.tSnIWFvvSuAt30uMe7mdFVdwkXGacybxCeH1xziiHqCtrTr4YnjRBiCw.zLoKnULxnMnS6dEgvlGTRENglddOMMR5l53O6G8+Ic6zFsPwINxQ43G5f7badinxyIYsUKp7qIGINDQJTgAXG3zfzd8YH4PesqGjdFH0kApgK5m6hJ8JvJJ5+coWBdOEg7s6YWOH9E1VvWJn9qsU3JE.WRIkTRIkTRIkTx2jHDTH1TEyZs6wUu10owPivq9JGibrD6yQ6M3FLOd8nwiCE.33dK2hqeyaxq9JuDacpIwZ5wHUqRuNsILNl9dPJUHDNxxRF73f4WYM9fSeV14N1A6cW6FnXVtpUgzsWOhCCQKbjzqEMFdDZ1yfVFw8VNgKb0axDSMEu5qdbjt1Xs4XLFjRPE.4FKNmAgViPnHtRc5Y7b8aeet6CdL6d26fW8vGDqqEdY.doGDFbji0BZsBoSSZlmfZM3zW+7b4KdANwINJm3P6GMP0HIXRvQXg0mEEUQDQgXHmPfUDvO8ceWla9U4MdsWhSr+cQr.7IMYxomhNIon89BqzZE37Nzx.hhpvRY4bk6bGt6rOhCdfWf8uq8.cSI1XohFRy6fttBqA7FGRuBc0JjFnX1lKysm+w734miW+HuBGZ66kZ4dRZ0jfHMUqGPlsOFWZwArAilnbWJpPMDo3AqtL23QOfuX9Gxd1y94jG7HLdkZjrzxDjmRsPIReF9AMDqvYKhBJu.7ZrDRlrBW9tyy6ctqxbK0hi7BGgScf8wnJGAqLGCGFRKLHTJDwg3TJRSyIwkQHgHEFhD4CD+pw37rZRNOrUGt0JKwSVcUV7gyxyuu8xKezixN27LD5xIY94HqWavmQkQZfSZIefk38HPICndbD0ijjj15YF3VLvtxxmpacveSg81KvIonR4Oqv2lm494hd7s3T.4ufOpe1tTvyrCs6YOCkTRIkTRIkTRIkTx2X38BjnnauDdxSli50GlZBnUy0P5bCr+I.eYR+9Td3ryx5qsJ6Xm6fXDj0uGCUuFY86SPPPgco0CjKma.WgfgUatN25V2jcrysynCEfwlQTTDRoltc5SbbLJA3LoHwiPUTqrGM2B7jmLOyrwMwFqIwllgIKGuMGgPPQKJWLCVk.Fign3pjjAO5IySZZN6dW6lJHvzacxxsjjlQujdzqWOxySITGQfNBqUf.Eu+6+ArzhKv671uEarQMZ2YcFtdE51sUg3nmJPRLnKRGzznND7wm9bXLRN4INIw.O5g2FsvyV1xloU2dDnznFTwaadNdu.PxhqtBW9y+brRO6Y+u.C2XH5tVKFoZM744L67yR0Qpg.IJglZUpPbbLq1oM25A2i4VdYBhqvge9CvlFeJrooj0KgnnPDZAFSFggJzAAn0ZLVCIooj5Lzwjy8VXV9jyedpN5vrume+r8stY5tdSZu7JLwHiv3iNDqsxheEqByfYw6Seuqv407u5+i+TVqcJG3nmfCc3ixXiNJUEBpZxIrWaPHPpzX8dRLVrRPGGhCOsZtNwAPEsj.UQEWa1oK24wOgO6JWiO9Lmi+j+o+w7Nu42lML9XX61izlsIvaYnJgL1P0.rHzBDZv3czseeZ2pKca2iztI3ENbC9AgCgunStkCBppm1quE1XtXAfrBIFTXDJ7BOO8OEJnG7yW8+eP2gKe5s89hTTW3+G9J.mpAoSQfQgzKwIyvoLXeZTeY0TUFPnVgRXIgbRrFxcBbBMUr1A6outVd+fa2oWWpUqFQQQXsVLtmd+cXsVPaJhIamFgSiSJGb.ePzy6KtnxIkXjfQVzeCAdewLqxWEuTReikj7LhiETuRUrXoS2bDpJH7JjNOJRPQJNYN4ZGNAHkUv4nnw4cJTBMgxPBCTnEBZ2uEdgA2yVRCQQ+QXkEdcWTXqEIN7dIFYQLv6jRjdCUCUjkzGjJb5HRLJLNOQAhh+ANeeDNIRGHcxAtqQVD63RGYlLpVqBfijd8KragThcvxnXHgff.f.b1hUPTKCHKyPuNcoQ8XJ7w+fUpZv1m81QGfR5vjzgr7TT0aPbPc74NLY4XzEo0lUJQ4jTIShx6vnRvKs3sCiW3vpMTjDbtm0C.djDVIFm0hKOCepsnKQDJbww3UZTYs9+mNStjRJojRJojR9+aX6zFcswYMf+U+suKxvb9m7evuDSKxXcWa5JFAa0wo0ZyyjM7TWaX8UaQu5SRdXL+O8u7+NNzI9NbfstAFRzAcUAYtTxpOJckwTMRhMqCxpArtKmQmXebOum+M+zKBU1J+W8adJDosPYUfOfd1DBGJl770P3.knAq1Nfn5ivh34O8u5eCMpa3e5u0oXB5RfbH7VvDnvJsj5xH.Ii3T36p3dcSX3YFlq1aQ9W+i9WyqtmCvO3695nx6vj5ZzpSKFaxo4Qs5SUsjoqoHa8koc7TjNTMNSyL9K+fKw293Ggu6AOB5NVpaiPKr3BCwGMB28VWgi8b6EPvMmcMFcqOGy487m8W7Wy72+x7ey+7+K4TaeRLcaxFlYKr57qvCt+ModUEq45S05A3Z2DqOFwTaf6l54Cuyi4AOYY9m8G9awnosIZolDqzrlO.WbEpyln47VZj5IuVEtWffrZ03I8y4CO60IrcO9O62+2kgs8vrVSPXn9ndjlt3aB07gHHjltEHdhMSy7JjM7znlbCb0qcMN8GcQxa1j+4+Q+gDYsDN2rT0IQUqNI8MzW3f5iy3XoehCBioiWR6vPzSNAWY1Gxe4O7+c7Ux3kNzV4M12NoVVJ14lm3PEznNKm1CoVhyZQ4fHQ.MTZ7nHMOktYRdTllFacmrhNj26rmgydlSSfMiWau6ji+a7VLibsB82oCNg9YUXUh6ooZE7rd2MPo4Y1P.CR2WJA0COyN6+hgZ0Ss0rx69xGN.+B25+22GeIOUd4+fK.V3GLel.dZdl48LPAu.q0RuLKodGfoXFgEpIPGCxP7t9eklb9e28ei5UIIoOI86RTTDAgg38dxsEqR0SSXrhWLeo3omcD5WXe90OlJKFH4dEZslJAwXM8Y8lqgRKILnJF6fchvg2+kw7MHw6YvJN8kOIVSNINCo8rEM4cPgjwmsJfCh2rmlxYhA6qml9ae0+N.51sMl7LjpPzBMgggnbfMuKYYYDUM3qc++EQJkEIBn2QPP.gAAfPgwYw4bfOC4fYYmGvYyw4g3f.pN9njl1+oeT903oGGLlbBUfRqIRB4dO8R5gLavyuR.RGNu7qrRntAyFLAhmsxQe4mYRu6YwgeyVMKl4cNGZu.kPiP5Au8q9gQIkTRIkTRIk7MFVoDUX.qj5Y0kVjYlZFFajQQ.DEEQtu3aVEGWCuGx51qvZwJEOb14HqWG17lOHyL8FH2jPVutTItFZsr3625KBiJq0Sbkp3AdzbKR6lqyN10y80ew7U+dTC9B0Rc.Y4Vz.O5gqSm1MYG6XGLw3iiwlSfPSg3jhmqmNiU8HvIELzvCgAOW9hWh3fXN3AdABDPmd8XnpgDDDfCKQQQDp.SRexrNBihnCddue5eOSuwMxQN3AQIf77bBTEoYsPnHmDlbhI.mitMaS0FCg.3Qy9Dt5kt.G+3mfYlYCnjJx8d7dKFaFNqGsRR0.MJOXUZhaLLcURt4suAO4wOlcr0sREklHgCsziTVTAxAUNBIBTAAzJKmfpMXo0axUt3EQ5bbjCd.FY3FH519qb7EbC9tvOM8hGcjIY9EVEW0InwXCy0+h6xG8weDUhh4s+M90Q6Ak6oAAkCOEU8jAUZ23AYbHq0oG1p0n9XivEt0M4iN6mRnLfW9TmjcOyVHTnvkUzuydewnzRnUDEDRuztXx7HBzjJbjkawGFR7H0Xpo1LezEuF+8m+hrvJqwFlXLNvt2FGXiSyDiz.Ve8+890H+6S9GbKPKFTcPAFPXfAMy8SWwfmV8VoJ.oTiRoQKjfyfMK8qTb6u9OOc+VOpBQAZzROAJPIAuy.dKgApAmH5+x4F0fm+B9JKgAtuTfExASkYABgGqMG.hCCINLpvxHNGwZQw6qA6G+fA4rGUwERnwZs3bBjRMAZIQAADFnHLH.sp3eLQMnxlRu6oA89f8kbvbg6q3w8mQwuQqjDoUHDEuGiTBpFHoRfj3eQix+09fo3wGFFhwXvXLn++t8Ny+Qttxut+4t7du5UK8BaxtIatSJRQQQJQRwQhij7r6k.m3XXCXGj.Dj+lBf+oIAI1dbLrmwNSlLKIypzncwUwklh6acylcS16UUus6R9g2qptojP.hSzrDTGfFjDr5Wce22Btm6462yQoPH038dbdONODGFRfVRfBhBDH8NxS5PdRaDtBjdy5alfv8otNEDowUkYbAQUaNQQ07YPHJWY.vqoWo.45GJ1N45Wu130u9JAiiZ0BIJJf33HhpUinfxfFGuAmM4+Cuac.FfAX.FfAX.Ff+uGtvPDhXl5JWg4mYVdtCdPlXjMQRQ2p08Jv4Lzndc7NAKtRazwMHGIe34+Hj9LN7A2GiEUm7bCII4HPhTIv6JUQvKjjarzn0nX.tvEtHsWcQdkW4k9TimxLSs7u6QfHLDmTPW7b1yeVxSS4jG+kXHYSr4V7BGdQOBZT1+sUUhnQpoY8Xt8rOl26MeK1111Ju9W7T.PdZJRIDTSSZdJMBiQqhXoUaCAgHDvzOZU9Y+vuOG44dN9huxoP.TXyAYoSQGHCHsSa1xXalhrbdxpqvPiNV443YOGSem6wu2W+qwt2w1qbaaKdbXrkh2njZBMBb4VLgg3ZTmG0dUt3GeERVYMN0KbLZoCnoJjvf.TJcYdJi.kDjBOKmlCpHTp.t6MuEW6RWjcs0I30d4ShztdkOVpJo.uTfSHJy9VIX5JP3CIp4P7jUWi28Le.yN2rbfCred0SbRBMNBsNztxiiSYwprXUFrJCYVK4Hw0LFaiHt5CtM+x26sXw4mii8rOKu5gNJ6bngILyf15oVPHdo.CNL.IqsBQp.FMtIBuj1oFVSHocyX5LTCdiKcAd2ycZl6N2icNzn70ewWhemCeDlb3VPV2OGex3WM3W6Df2nkV2KnhkUzDAvY83DZDZEAQ0HLLpzMxLVxS6x58EAeJkLkdGoYsoVfhgZ1.k.xR5RVVBNmAS+xmtWlRUcCakphUbbqTa7yZjKw4sX8FRyyHIKE.pEUCsTPR2t8I2W9hhdD2jU6TlDgHDonhHtyiGa4NwI8HUPO0gU8Zr69ubRV1P3P+RNvug4hdPq0TudcBBzTTjQtIEmKGkuf3ZehxGnu8iu9jRPfFIBjxxurjjDVqcGxKLX8fwVPdVBE4YnEPs.IABONSN4oc6e8sOIXVeN0K.gRQZVFIoIjabXLFrtRkmkHJK4AmuJ+u1vFIT8yFOt7T26T94Cp5wBjpprFq7+W5sn9L11fAX.FfAX.FfAX.9bGwMo.IW8JWgrNs4nG94HFnS60PJkkpdlkgBEduBKZBBawhq0k24zmlcr0QX+6bBj.VqEcPUE847f2UVoa.4VIRZvxEYb4KcIBDdN0Idd.PTEwR8DPnTWjx1nyoBHZnVL6iWkyctyxnCODuzwdQT.RWo4UgvTt9498fZuX3QiA3bm+iXl6dWNwK7hr0gpSZZJggg.kFiTQdJABEBDj6jDOzXz1Au4a8ljr7R7RG8HLYyVjWjBZPnUTTXQoBQXrnPQGaA0FYTDH4zW9xbwKbA12d1KuvgOL0kPddabdCB4FVzrWhqSFlbG95MX97TN209XVX4kXuStMN5d2C0EBhDBDUYMboLLd7UpIuXdApgZwid7S3pW9RLRiXd4i8hLwnCS6kW.gnm7Tx0cq5JxuNgj4VLgw11tvHB4W9AuG2+A2iie7iwIO5QIYokIv3Q2qslorWYKWmtEOdxzZZ6sDN1l3QqtB+nex+Sl6QOhW6juBeiW9KR8BGxUSfjtTSqIpdLdohBis71CmmPc.NjjYrPbSpM93LeQNu4ENCequyeOq0sCeiW604O628avKtqcPPmtjs3B35z9y8GO97F+Zm.75wRbOhm1JBJ.dIIY4jT3Hw3natgrrbr4FjNHR1uslqHtUdahn+ChvpKuLVSN3cjkkf0anV8XBhpQZtgRa1VT84cqSBtupst9NsFrdoKHqZL6BuifZwniqQt0Q6tIjkkgMufhzNUJaWdLbxRxu8ZPcuWiRpQ3k3JrTjmRZZ2RxzlbxLEfPTVlzdP3DHckme9prOq2HyuA2MyugMSHIoCJsrrQ6cVxyyIIcMR61FWwm8N3rwyYWEoyff.DBAoooXbVBBiHHLFiiRiLHIirzDTBOMqGSqF0HJPBX52H68Faa7mNIojjlPgyiTo.UD5vHDJMs61Aprfco2Vptqn5NF45A.dOiWn7N.ek5ukkcdddNo4FRxKnadAYE4XskJSGJrexS8AX.FfAX.FfAX.9bGFwv7njU4AO39L1HCyA1ytPAXJJvXJPJbXME3rVrdA0ZNLEH4lSOK2312muvKd.FennRBoRIwMZUkaqVjUBG3cB7xPLH4l24tL2idD6Ymakc0Trg1Fz02EcwWpVoEE4H.Q.23N2hYmcZd9m6frygZP6NqPHp9qSTUQbtLdZJU+MWpX9jBtzktLiLzP7EO4IQAzY0UXngah0Yv4L3cFj3wjaIn4njRH2b5Y3Mey2ficrWfir+8fBHo6pDVODe.XcfVFQbflbSJEBAsFaKrp2yO9G+yY0EWl+z+3+kzLpTjmzNsw6sH0RjZERoFuED4BzpZXq2fqN6C4LScIZzpNu7QNLCADasn7kk3sw4KMKKiGmy.XQM13rTgk26LmlEdxi40e4u.6axIXwG9PpqkUh1TBgux2dpDOyiD8lFmtxHN6UtBWbpKy11137Zm5kXqizhUeziHvaP41.WDJ+Q.n8dRqUir3Zbq4lk29zeHKrzRbhW7E4KehWhwipga4kw2YUjVakKLaovZ.mfZg0XzQGirhBVoSBtnZ3a0hYVaM9fKcY9Eu86vXaYybhi777ZG+EXGC0B6xKhc4EH1aY3Zg+J3IjOewu16A3dvIpHU1OQuJKu2Z0GsjJp0huHkrhBjtBzZM0BpQ1S2BsehdA1QbbLP4tigRSXTLApFXQPXyLbljmxFt8BS4tosQ0T+LqT3RUbiFpIRhQPHn8j0cIxyRntRwP0qSlq7F1d8j5564PoYaYRK.gGEkp0JTRDRMnJIbVjkgz6P4JIB5P7TDc6Y668O+6ac3kp4ZcNPnQDJPT3IpdcjXovjhaik28F98VeL5IKKAu2SfVWEzzRpEGSfpNcJRHRODJsCWdBYY4fMi5Q0PI7TXJnbv166Qt9wWT1KBVmivn5LTqFHH.jEnE0PKBHoaFUcBd+wYulbuTu99aM1m9piu7ZXXTcJ7B7NOBmAQgAmKGu0fy4AU7m8AX.FfAX.FfAX.FfOmPJdl5l2gkWbIN1y+BrsgZBXITqvZs3EBBTBxyyvV.MGdLVv34xW+FPP.m5nOKM0d5luDgJMAQ0HuSBApp1uy6v5AQPDcvyEt3Giy44Tm73DsgEO0q2e6ItBTJvPgTwZEYb4qNE0qGwIOwwPCr7pqvHiNBEUp+R0ZTchRhcFY.ERMm97mk4e77bxS7R7raabbdvVjSnZX5jY.oGkprhNMFHpwnj3gycoKwhKs.+a+27ulIGYHxxVAuyfNRfovgTnwYgXkh1c6BQMH2Cm+xWkq9wWmCdnmku7weAbYEj6KvYKHLTB3Qn.kL.uAzAQDzZDlMKmot68X00ZygO1yw911VIe4EIxaQ5skksLBrFQkFvkUfY33agybtyvTW+5r8ImficjCCYIr7bOjidvCvxqtzSMGWtACkpIi.Ztqcy6clyva8ge.sZ0hS8RGmwpEQ1RKxlhiqH+5gpxl1KjUFzq.gSPacoHgu8oOMW6ZWmW8keE9ce8uB0MddxctKiFEgRFhP3IyjQtyfwYK4OohncZF4NEQiMD1nFbwaeSdiy99rXmkYaiOI+y+xeclHpA5hTVcwGiBCadjlDHsjzcseqe8y+FfBvk1NdeELEk+aPf0qX5EVfkyrXTJT0ZfJpdo4W403chp9+7Sd7VGwwwfPgWpQEEyiWtMW9N2g6t3hfnAVQP+LVi98YZopzdgi06F4JUgqT9s2TmiXt4byxkt+CX4BKA02DD0.mTSPTb+.ZF1P3NK58GhxvI25QKEkFTkNjLmfkSsL+ZYjIiIWDhUDgUTtaV8JE3dk46S0AvavIv7.gQ0n.X1EVlomeIVKyimFHiafLrdeGCeikN8FU7trq8Agnzp3kRIQQQjXy3Z23lbq4dBIdE5vwPFzfLimBiibiijjjmdrsAk0opGcC0gTuQSDDwBsayGemoY5UVgTOHazBiTiU7zj9KGQUkfNqWVI8+VpJ0bqTx7KsDK0MkNNvphQFEiPEA.lhbFfAX.FfAX.FfA3W0XUObwO9Fj2MkW5ENJ0AZuxSXnls..mwRbTMrEFRMdL.24gOlqdiawj6YebncON0EFroInTJLFC4VC0pEVFsHdONu.gJlk5TvEu7TLTyV7kN0W.Gqu9mMZppBb3ERrREFYDe7ctMW+lWmCbvCvQN39Q.DHUnD8DznxGV7kIIhmREfKTv68gmAoVwq+EOEg.qtzBttsjqC..PLbRDEDUDWSiGOEkQeBAJINiCoH.OZt3cuMm+JWl8r2cyIegCScgfhtcHNL.OdxMYnCiHK0hvVJQhOpAyUTva7NuG0hqwW6K8kH.vWj0OMTBBTX8EkVIkPCNA4h.rAQbiGLK289SyN1wt342+AH14HrHGo2TJfE.dYoR6H.kDuTyMm+wb5otLir4Mwq8EOEAXoXsUXrQZQQZReunoW4gqbkp.6qZ1yoV5w79W+xTHb7ENwKxA15VQt1ZnRRoUXo9jNAXpp3S7RjdMBqFsMjYSay4t1TL6rOhCsumguzIdEFuVCxexhHxRIPKQF.VskLeNV7n0Zzx.JRMrP2Bbw0IIHfq9fGv6egKvbyNO6chcw+hW6qxyN5VXHiCe2UPoLDOTHpHIEE4jmlwusie8S.12qLlkXkkthruWNVIT7Sdi2hO3hSwzKlPW.QPSDA0JI0hfdY+Ueib1uQ4ZkzoaBIooXQQZAboqeG9d+zeIu6EtJqfGqH.qPuAhtkkQqeCDI60+u8CZ4M7MrXQNu8YtDe+e9ayT26QjAXzMXsDCYYE8+bhdkAcexekOPDnTDpCHPV9xqkVsM2Yl43rW8F71WXJRzgjopSgHDG5xcNpbPVoL5mtOVW2k3.mTyhqkvUu0C37W8VbiG9DVzXoqUSxmn..9rbCZkRgTHvaKiMJe0KTexhKwklZJ9ou2Gw0ldQRPhr1HHqUGcbcBBqgPETMV2PYfTNp5+hVqwfsvxZc6vEm557levY4Le7MYtBKo5.JT59jfc8J4cgDuWhvqWubpoWNgIp96BbBIuyoOOm8JWka9f4XtU6PWiDBBHHLffP0m9Dd.FfAX.FfAX.FfOmQGf6Myiv4L7L6bRBDPV6NDGFW56JNK0zA38BLVKFO7vGu.yM+iYq6bmr43PpIsHqhCyjzbbNGgZMd50FakeWq0MiomdVZzrIaRGRxxy+IFMqSjsLJLUXPxst+LL27yw916NYXDXx6P8nvJsVpDhgMZdVRJjRJ.twsuM0qWmmae6GMPVxZzrdcRySwY8kIzgPBNOJcHE.e7suOOXlY34N7gXyMpixUVEjMhqgyWPgorBPyyM3rVzAg3AtwClgKe0oXhstMN4weARyKPIbDGpIJLDkTVVx0UNis2IXImg1d3VOXZVYo03f6cer2Il.R6PyZAUsuXUqUBn7k9giWEfKLjyekqv8m9gb3m+443uvQYoYeHZSN6eW6f4lY5xeuMzBfNj3DRLBEYpP9km68YtkdL6+.6gW3YN.0JxoVQAiVuNsauZopuxJQA8RDdENzXEgjKC4IqtJm47mmFw042+K+0YK0Zvb27VHs4L41lftYqQpKCSEQ9fnPpG2.uPxZs6P8I1NoA047W6l7ieieNKtvB767Jmh+vuzWgCr4wo8LyfckEIV6YnghHsHgom+grZmtLzna5ykmI9UI90OAXj.5xKzPkQEInLviC3CN+k4u3+veI+f27cXMmGDRhhGlBW4i2kJ.apbAX+m33Jw4j3jAXkJTQi.ws3bScS9q+N+.9a+QuGYDPXPSBpWGqujjmn5Fdmy8YRJDJipIORD5FrlQvO7m817M+a9G4CuwbHCCXrw2IK2IgdpG63oUpU5cHcdjBOgZIooojkUP8FCynacG7W8s+uy+9u42h29B2lGm5QGUGUbKLlxGdiBknjkz8DaPQXv2edvKfBmmls1BS+jU3u9a+83u5a+84R2dVR0MPpGCgn7AZoTVZW9dWY7FIb8MnfvvR2pFJMsJGJ7RI24d2m+l+g+G7c9AuAWdlEwBDUaHVoaNqzMg5Maw52hs9XTz+EsNpGEisvRy5iRyQGmyb4qx+ou0eO+j2+rzUIfvlnq0jLCrZ6NX8NBCCQK0nDJrtJEeQgyqv3D3bBPFhLHltFO+Eey+R9a+d+HVJW.gAH00wJTXs+uwErGfAX.FfAX.FfA3ehv4JWOkeCqMciq45wFOezGcI95esuF0zRbVCSN93jjmfy4ITJIOMgff.pEWGKv67Ae.dj7k+JeELYqhMqKMhhv4bn0ZhBioSm1nTJrNKQ0hwC7M+O9el5MGl+n+n+HT.iVO5S4MKqKVgDKZRA9G9teO1+AOHu9qdJLTPrtjzpTpw3LDEGhwTPQQAEYlREqEvey296wCt2c4O+O+eEapdHc5zgw27XnThRBndAtBC0ipyJqrFQgJVnvyO5m7yItUS9C9C98vlmhOKglw0HKKghhLBiBHKKi50aRZRNMhGlUwye2+32EjZ9S+S9SHVIPZKHTIIKKih7bTHvWsFeoPPfVyv6dO79W9Jb6acOdtC977h66.nSywmzEuyfwaQDUl7I4oY3MFhiqiKLjqMyC4s9EuAuxW3k4v66YXwYlgMWuNCGWimL2brksrYJJJHJtFQQAjaJHyVPsgGFaiFb9acKd+29myd16t30NwwYj.Ag4YDTjSZ2UAkDKNPqnQbSbEvpqzgf3Qv0bXN28ll+tu0+Edl8c.9i+8+CYy0pSwRqxPgQDEDvJcVgnl0nvaHLLfZQ0nypqwps6htVLgC0hfI1F+v29c467e66gVEx+ru5WmScjiRKOjM27LRfjlABDtRuCxKrznUKBpGS6LyuReV5yC7aHDfqhGH45psVtCTAXBpwRcx3R23d7Vm9Zb0G9DVM2PXiVDTKth.H7IUBsWfEoiBAY.FuFKPgLl0Jj7v4VlO5Z2kO3BSwMezbjV3ILtN5fRUKkRIgg0X8HbV9T6jSOjC3z0H2GvCdzh71m6J7tSceVHofg2z3an2e6c1tNAv0aP9R2KV5U3DkD+sA0wnZvO9cOKu4Ymh6tTJEnHpQSDBO4oIXJRJMuqO4fZClBkCIFu.U8QoiUyGe243sN6Tbga7.Vnua18zubd8xLt7eK7z+yzyrwTgADDWiN4A7w2addyO7hbg6s.Ka8TqdKhpWmzbSE4zmVc4RApWOPn5cQ2JjjXTL+BKy6cwqv+0e5ayzqsBIdHHtNgQ0QHKIiaMFvZqdwsbCispM9PnvJzXzArZRA23dOj287Wgyd8Gv7cRQFFQPb8O4MiCv.L.Cv.L.Cv.74Nt1slEktF6X6aiQpGi2jgoHurRH8dDHv6sDDnvKE7jTOKrzxL5nCyV2xlQ4KM8S5u1Tw5jYATAgXcvhId5zIkI15jroQ2DJgCuIs+3ne64sg10yKTbsaeeTpZr8crCFtUSjEIPdJBb3pV+ny4.kDoN.kL.iGlag039y7PN3ycDFajQPCHr43cFJbE38BTp.rFGBpZqNGL0MtKYVXe6a+L1HCiBCJ+FWa+FioTPGFQhyxUuy8ocVA6dO6iI1zlI.PKp9r9dFjJfX8TBAf6s5pb2GOOgAQrusNIaJLhfrDjdCJk.qTf0WlAvZk.kRQRQN26wKv0d3brqsscFe3QnkVSrSPn2ix5AqibqEUfFi0Pto.gThLJh1FK2btGy0leN1+91K6b7MyHZMg4YnKJPgEgVgNRiJJDuE5txZHsdFZ3wX07Bdmqcc9YScYd9m8Pr2I1FiDUCcgGLE3sF7JvI8T3LX8kUtYnTQTTD0pWGSfhkME7c+EuAys5prycsWdt8c.10lFiQUZBL4U2eX.gEIVDdSeyE1JTXk+FiER8OY7q8y.guxVwkk4JqpW4rhDqPP7viQt69bsabarVKO5Y1F+Au5I4YlbbrXQ5ceJRlaDA5HxJxKcFOEDDODDzfjUmgae6o46m9DN0wOJCchixtGuERWF4Y43DAnTkVkN9ptMUP4Nxz2vnjXAp0XHzwMYkk6x6e1KxSleVhxOFe4ienJSoR1uKh6AYux1VJvT8.pTJw5AqTgOnNDDyY9vywRKsLQtTpe7CwNFNnj.nqffv.bavHichJSDyCHL3QRVQNg.gsFEpMLyN6h7Vu+GQ2ztXkg7U2YM7xxFx2K.uzUJucUsd2y03KUEtxQpQRPTMZL7vDzXLd3bqvO6MeeVYwEgemu.m7f6j5pXZ65PYUF2a9hmZyJjdH.IhpShfZCQPyVTja4pW8l7vGLMiFJ3jGZOryw2DAQg3J7XLF.OJUkQXguOgcunLG5DUAPUXiQHyI4ASOO+h24zLy8d.xW+3L7ytMBk+Fv9+L.Cv.L.Cv.L.++cnW0D9IQOAEtv4uBQMFlCt+8wvwQj2tSYeiJzXcBBjkNNbXb.3Db669.l6wyyAe9Cy1apf7Md7e50y3oh.bNbq6bGVdk03UO5KxVGeTb9L7lBDUcolSvFV+cUalghSelOBcTLO6ybPFUFBoKgv4QHzT3JMmIiyABIRoBsLf14Nt2zSysu8c328a7MXhMOZ4Z7r4PnDiyiFIZglbmfzrLp0nIKkZ3CO64wBbxS7RLhL.ouSoSVWIzgTrdkT5ABqUmGsRad+y9QzMMiW7UOFStkV3KxHP5fdispcEPtwTmAGWa9Gwsm8QroQFgCs68xHZE1hTzpdIUpFmGhDBjBAZsj0xK3ZyLKW7tOfisuCydGaKkjFSSIx4Q4gbmCKfLJhTSARqGUP.h3Xlo6Zbg6cGt17yw23UNB6dKSvnREpzDbtbPoPDpAY4lfDpzjlzFgJD8v03VObVd2adclIKg+cG4TL4lGmg0QHRxv3yKuHpnxunAo0it.TZAAxP7REKjzgq8vGvac9qxt1xD7RG4.bzImjIZTGgKiDgCSHTn83bdzdGZGkbfjhp7xQh92xiRzeCf.ru7FMpLeoJBR8tMs4Halv5sHMMiqeiahsyi4H6emrisLBBSFwpO8KX7hRGSF.gzi2ZIWTPPPMZN7nL53SPXyGgwp3rm+xDFFwg19VYhQafz5vXKiiobmAIpm50JkFFkCuP2WlzlsZwviOA4lEXwEWgOZ4mvtGIj8M9HL4XM6meuxdMPr2SO2Q1ICw5K6EBsjpWp.BcDdY.NSF2+dOfy1Ry91RC1Tz1PYMf.pGUiztUMhd+ogd8WqGGVJJxvADTqAMGcBVZUGyO2xbgKcMBhh3KMYYXnK6s0N8lCqTFVIjH7Vv5vqVelPEDR8VCQqw2IsmeVl8AyxYsoryM2f8r0QX6C2pRM4RiJyi7ocS6d6hm2h2ZplGGlQlXRhGcdLVAy+nE4Me2OfMEoX7QZQMgGqyVFMRJEAZIEE1xI2mx8uKgEIwMGgnVCSxBIb+6OKqsvBr+suYN31GhFJK0pLDqAX.FfAX.FfAX.9+U3yh.7Fq3t6ey6Qs3gXxstET3vVjQPsPPEf2mUEIRYXwgLLh4WZAVqca1411JMEzWfE7hpk.16XWlzFVGfPwzyLCc61kcuqcSSkfr7DBjh9Ffpux3PwVY.rT5mJW6F2hnnFrisucz.EIcQGW5uKFuCkVh0VVAiNuCoN.mwwie7BrzSdBG9POKMUBrIk8wqPIwZr3cg38BB00HIIinQFlNK0lot90PoB4nG9HkFNkuz+b5QUQ3546MUhnDEyZqr.W85WmBimm6fGj5.IosoYyHxxKCNHgWWNG48UJAWtF+6+3EX41c4vG3fL4XaB2JKhxaHLTSZQN5vPbNGJguLIZp4HwZ49KrLyt3p7mcpIYqsZPMmEegAENjBINg.iPfFIVqsrjwQfU.yu7Zb6G8HZab77StSZEFRjwhnvffxxi2fDq2SQRBC0ZL7ZEYdOIEYbqGNMy0dM1wQOBGZxcPjNffBCE1B7BOh.EdoCgurT08RM38XLkNBdg0vLK7Dt30tJFOL4t2CO+9O.aAIpr0vYyPE.5nXLEofThxJKMwKJW5tSRYuQ+a4cQ3+K.d.Z7mZ2c6yH.....IUjSD4pPfIH" ],
					"embed" : 1,
					"id" : "obj-9",
					"maxclass" : "fpic",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "jit_matrix" ],
					"patching_rect" : [ 681.0, 322.0, 172.0, 107.5 ],
					"pic" : "Macintosh HD:/Users/Home/Downloads/8.jpg",
					"presentation" : 1,
					"presentation_rect" : [ 0.425985795694388, 1.0, 168.945205628871918, 234.527472637511892 ]
				}

			}
 ],
		"lines" : [ 			{
				"patchline" : 				{
					"destination" : [ "obj-16", 0 ],
					"midpoints" : [ 332.5, 627.0, 114.955412386953014, 627.0 ],
					"source" : [ "obj-10", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-67", 0 ],
					"midpoints" : [ 535.205412386952958, 705.0, 535.205412386952958, 705.0 ],
					"source" : [ "obj-11", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-54", 0 ],
					"midpoints" : [ 632.955412386952958, 546.0, 632.955412386952958, 546.0 ],
					"source" : [ "obj-12", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-41", 0 ],
					"midpoints" : [ 253.273771220957599, 348.0, 171.455412386953014, 348.0 ],
					"source" : [ "obj-126", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-66", 0 ],
					"midpoints" : [ 395.705412386953014, 705.0, 395.705412386953014, 705.0 ],
					"source" : [ "obj-14", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-65", 0 ],
					"midpoints" : [ 255.705412386953014, 705.0, 255.705412386953014, 705.0 ],
					"source" : [ "obj-15", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-11", 0 ],
					"midpoints" : [ 535.705412386952958, 666.0, 535.205412386952958, 666.0 ],
					"source" : [ "obj-16", 3 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-14", 0 ],
					"midpoints" : [ 395.455412386953014, 666.0, 395.705412386953014, 666.0 ],
					"source" : [ "obj-16", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-15", 0 ],
					"midpoints" : [ 255.205412386953014, 666.0, 255.705412386953014, 666.0 ],
					"source" : [ "obj-16", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-60", 0 ],
					"midpoints" : [ 114.955412386953014, 666.0, 114.955412386953014, 666.0 ],
					"source" : [ "obj-16", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-19", 1 ],
					"midpoints" : [ 131.955412386953014, 582.0, 129.955412386953014, 582.0 ],
					"source" : [ "obj-18", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-19", 0 ],
					"midpoints" : [ 114.955412386953014, 582.0, 114.955412386953014, 582.0 ],
					"source" : [ "obj-18", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-16", 0 ],
					"midpoints" : [ 114.955412386953014, 615.0, 114.955412386953014, 615.0 ],
					"source" : [ "obj-19", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-37", 0 ],
					"midpoints" : [ 332.5, 438.0, 332.5, 438.0 ],
					"source" : [ "obj-2", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-316", 0 ],
					"midpoints" : [ 337.499635993986885, 231.0, 253.955412386953014, 231.0 ],
					"source" : [ "obj-20", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-26", 1 ],
					"midpoints" : [ 211.411552785932201, 459.0, 211.955412386953014, 459.0 ],
					"order" : 2,
					"source" : [ "obj-209", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-36", 0 ],
					"midpoints" : [ 211.411552785932201, 477.0, 632.955412386952958, 477.0 ],
					"order" : 1,
					"source" : [ "obj-209", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-72", 0 ],
					"midpoints" : [ 211.411552785932201, 477.0, 723.955412386952958, 477.0 ],
					"order" : 0,
					"source" : [ "obj-209", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-580", 0 ],
					"midpoints" : [ 114.411552785932201, 66.0, 114.411552785932201, 66.0 ],
					"source" : [ "obj-21", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-18", 1 ],
					"midpoints" : [ 146.955412386953014, 552.0, 131.955412386953014, 552.0 ],
					"source" : [ "obj-22", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-18", 0 ],
					"midpoints" : [ 114.955412386953014, 552.0, 114.955412386953014, 552.0 ],
					"source" : [ "obj-22", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-25", 0 ],
					"midpoints" : [ 799.955412386952958, 171.0, 979.5, 171.0 ],
					"order" : 0,
					"source" : [ "obj-24", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-39", 0 ],
					"midpoints" : [ 785.955412386952958, 171.0, 1274.5, 171.0 ],
					"source" : [ "obj-24", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-96", 0 ],
					"midpoints" : [ 799.955412386952958, 117.0, 799.5, 117.0 ],
					"order" : 1,
					"source" : [ "obj-24", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-94", 0 ],
					"midpoints" : [ 979.5, 261.0, 799.5, 261.0 ],
					"source" : [ "obj-25", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-22", 0 ],
					"midpoints" : [ 114.955412386953014, 516.0, 114.955412386953014, 516.0 ],
					"source" : [ "obj-26", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-28", 0 ],
					"midpoints" : [ 114.955412386953014, 771.0, 114.955412386953014, 771.0 ],
					"source" : [ "obj-27", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-316", 0 ],
					"midpoints" : [ 253.955412386953014, 192.0, 253.955412386953014, 192.0 ],
					"source" : [ "obj-29", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-28", 0 ],
					"midpoints" : [ 681.955412386952958, 786.0, 114.955412386953014, 786.0 ],
					"source" : [ "obj-30", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-4", 0 ],
					"midpoints" : [ 785.955412386952958, 669.0, 785.955412386952958, 669.0 ],
					"source" : [ "obj-31", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-126", 0 ],
					"midpoints" : [ 253.955412386953014, 270.0, 253.273771220957599, 270.0 ],
					"source" : [ "obj-316", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-40", 1 ],
					"midpoints" : [ 318.955412386953014, 288.0, 203.455412386953014, 288.0 ],
					"source" : [ "obj-316", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-28", 0 ],
					"midpoints" : [ 255.705412386953014, 786.0, 114.955412386953014, 786.0 ],
					"source" : [ "obj-32", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-28", 0 ],
					"midpoints" : [ 535.205412386952958, 786.0, 114.955412386953014, 786.0 ],
					"source" : [ "obj-33", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-28", 0 ],
					"midpoints" : [ 395.705412386953014, 786.0, 114.955412386953014, 786.0 ],
					"source" : [ "obj-35", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-12", 0 ],
					"midpoints" : [ 632.955412386952958, 516.0, 632.955412386952958, 516.0 ],
					"source" : [ "obj-36", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-10", 0 ],
					"midpoints" : [ 332.5, 516.0, 332.5, 516.0 ],
					"source" : [ "obj-37", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-1", 0 ],
					"midpoints" : [ 1274.5, 249.0, 1274.5, 249.0 ],
					"source" : [ "obj-39", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-3", 0 ],
					"midpoints" : [ 799.955412386952958, 714.0, 800.416551827489457, 714.0 ],
					"order" : 2,
					"source" : [ "obj-4", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-30", 0 ],
					"midpoints" : [ 785.955412386952958, 723.0, 681.955412386952958, 723.0 ],
					"source" : [ "obj-4", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-6", 0 ],
					"midpoints" : [ 799.955412386952958, 723.0, 1070.5, 723.0 ],
					"order" : 1,
					"source" : [ "obj-4", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-8", 0 ],
					"midpoints" : [ 799.955412386952958, 723.0, 1047.0, 723.0, 1047.0, 789.0, 1070.5, 789.0 ],
					"order" : 0,
					"source" : [ "obj-4", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-41", 0 ],
					"midpoints" : [ 171.455412386953014, 327.0, 171.455412386953014, 327.0 ],
					"source" : [ "obj-40", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-209", 0 ],
					"midpoints" : [ 211.955412386953014, 384.0, 211.411552785932201, 384.0 ],
					"source" : [ "obj-41", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-23", 0 ],
					"midpoints" : [ 636.142565579372331, 192.0, 636.142565579372331, 192.0 ],
					"source" : [ "obj-52", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-7", 0 ],
					"midpoints" : [ 632.955412386952958, 579.0, 632.955412386952958, 579.0 ],
					"source" : [ "obj-54", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-26", 0 ],
					"midpoints" : [ 114.411552785932201, 102.0, 114.955412386953014, 102.0 ],
					"source" : [ "obj-580", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-64", 0 ],
					"midpoints" : [ 114.955412386953014, 705.0, 114.955412386953014, 705.0 ],
					"source" : [ "obj-60", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-27", 0 ],
					"midpoints" : [ 114.955412386953014, 738.0, 114.955412386953014, 738.0 ],
					"source" : [ "obj-64", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-32", 0 ],
					"midpoints" : [ 255.705412386953014, 738.0, 255.705412386953014, 738.0 ],
					"source" : [ "obj-65", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-35", 0 ],
					"midpoints" : [ 395.705412386953014, 738.0, 395.705412386953014, 738.0 ],
					"source" : [ "obj-66", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-33", 0 ],
					"midpoints" : [ 535.205412386952958, 738.0, 535.205412386952958, 738.0 ],
					"source" : [ "obj-67", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-46", 0 ],
					"midpoints" : [ 723.955412386952958, 516.0, 723.955412386952958, 516.0 ],
					"source" : [ "obj-72", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-94", 0 ],
					"midpoints" : [ 799.5, 246.0, 799.5, 246.0 ],
					"source" : [ "obj-96", 0 ]
				}

			}
 ],
		"parameters" : 		{
			"obj-20" : [ "live.text[4]", "live.text", 0 ],
			"obj-52" : [ "live.text[1]", "live.text", 0 ],
			"obj-580" : [ "live.text[2]", "live.text", 0 ],
			"obj-7" : [ "live.text", "live.text", 0 ],
			"parameterbanks" : 			{
				"0" : 				{
					"index" : 0,
					"name" : "",
					"parameters" : [ "-", "-", "-", "-", "-", "-", "-", "-" ]
				}

			}
,
			"inherited_shortname" : 1
		}
,
		"dependency_cache" : [  ],
		"autosave" : 0,
		"textcolor" : [ 0.0, 0.0, 0.0, 1.0 ],
		"bgcolor" : [ 0.529411764705882, 0.529411764705882, 0.529411764705882, 1.0 ],
		"editing_bgcolor" : [ 0.529411764705882, 0.529411764705882, 0.529411764705882, 1.0 ],
		"saved_attribute_attributes" : 		{
			"editing_bgcolor" : 			{
				"expression" : "themecolor.live_surface_bg"
			}
,
			"locked_bgcolor" : 			{
				"expression" : "themecolor.live_surface_bg"
			}

		}

	}

}
