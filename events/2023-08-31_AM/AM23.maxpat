{
	"patcher" : 	{
		"fileversion" : 1,
		"appversion" : 		{
			"major" : 8,
			"minor" : 5,
			"revision" : 6,
			"architecture" : "x64",
			"modernui" : 1
		}
,
		"classnamespace" : "box",
		"rect" : [ 34.0, 66.0, 1444.0, 882.0 ],
		"bglocked" : 0,
		"openinpresentation" : 0,
		"default_fontsize" : 12.0,
		"default_fontface" : 0,
		"default_fontname" : "Arial",
		"gridonopen" : 1,
		"gridsize" : [ 15.0, 15.0 ],
		"gridsnaponopen" : 1,
		"objectsnaponopen" : 1,
		"statusbarvisible" : 2,
		"toolbarvisible" : 1,
		"lefttoolbarpinned" : 0,
		"toptoolbarpinned" : 0,
		"righttoolbarpinned" : 0,
		"bottomtoolbarpinned" : 0,
		"toolbars_unpinned_last_save" : 2,
		"tallnewobj" : 0,
		"boxanimatetime" : 200,
		"enablehscroll" : 1,
		"enablevscroll" : 1,
		"devicewidth" : 0.0,
		"description" : "",
		"digest" : "",
		"tags" : "",
		"style" : "",
		"subpatcher_template" : "",
		"showrootpatcherontab" : 0,
		"showontab" : 0,
		"assistshowspatchername" : 0,
		"boxes" : [ 			{
				"box" : 				{
					"id" : "obj-2",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 0,
					"patcher" : 					{
						"fileversion" : 1,
						"appversion" : 						{
							"major" : 8,
							"minor" : 5,
							"revision" : 6,
							"architecture" : "x64",
							"modernui" : 1
						}
,
						"classnamespace" : "box",
						"rect" : [ 0.0, 26.0, 1444.0, 856.0 ],
						"bglocked" : 0,
						"openinpresentation" : 0,
						"default_fontsize" : 12.0,
						"default_fontface" : 0,
						"default_fontname" : "Arial",
						"gridonopen" : 1,
						"gridsize" : [ 15.0, 15.0 ],
						"gridsnaponopen" : 1,
						"objectsnaponopen" : 1,
						"statusbarvisible" : 2,
						"toolbarvisible" : 1,
						"lefttoolbarpinned" : 0,
						"toptoolbarpinned" : 0,
						"righttoolbarpinned" : 0,
						"bottomtoolbarpinned" : 0,
						"toolbars_unpinned_last_save" : 0,
						"tallnewobj" : 0,
						"boxanimatetime" : 200,
						"enablehscroll" : 1,
						"enablevscroll" : 1,
						"devicewidth" : 0.0,
						"description" : "",
						"digest" : "",
						"tags" : "",
						"style" : "",
						"subpatcher_template" : "",
						"showontab" : 1,
						"assistshowspatchername" : 0,
						"boxes" : [ 							{
								"box" : 								{
									"fontface" : 0,
									"fontname" : "Ableton Sans Medium",
									"fontsize" : 40.0,
									"id" : "obj-11",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 1174.0, 30.108035714286189, 145.0, 54.0 ],
									"text" : "website"
								}

							}
, 							{
								"box" : 								{
									"autofit" : 1,
									"forceaspect" : 1,
									"id" : "obj-10",
									"maxclass" : "fpic",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "jit_matrix" ],
									"patching_rect" : [ 1174.0, 109.6875, 145.0, 145.0 ],
									"pic" : "qrcode-website.png"
								}

							}
, 							{
								"box" : 								{
									"fontface" : 0,
									"fontname" : "Ableton Sans Medium",
									"fontsize" : 54.0,
									"id" : "obj-7",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 908.0, 21.608035714286189, 145.0, 71.0 ],
									"text" : "REPO"
								}

							}
, 							{
								"box" : 								{
									"autofit" : 1,
									"data" : [ 7475, "png", "IBkSG0fBZn....PCIgDQRA..C7O..Lv+HX....fkBvmU....DLmPIQEBHf.B7g.YHB..bndRDEDU3wI6YGjpjjiD.ELTSb+uxpWnD9YQ0UMqFjyCyNANtjxjGwZu26G....fr9maO......++k3e....HNw+....Pbh+A...f3D+C....wI9G....hS7O....Dm3e....HNw+....Pbh+A...f3D+C....wI9G....hS7O....Dm3e....HNw+....Pbh+A...f3D+C....wI9G....hS7O....Dm3e....HNw+....Pbh+A...f3D+C....wI9G....hS7O....Dm3e....HNw+....Pbh+A...f3D+C....wI9G....hS7O....Dm3e....Ht2aO.OOOOq051i.v+g8de6QfOlvuSNg6CSXOLAS3rXBbefoYBuM8t.loI76C9x+....Pbh+A...f3D+C....wI9G....hS7O....Dm3e....HNw+....Pbh+A...f3D+C....wI9G....hS7O....Dm3e....HNw+....Pbh+A...f3D+C....wI9G....hS7O....Dm3e....HNw+....Pbh+A...f3D+C....wI9G....hS7O....Dm3e....HNw+....Pbh+A...f3D+C....wI9G....hS7O....Dm3e....HNw+....Pbh+A...f3du8.LE6891i.7KVq0sGgQXB6A+9vb3r3XBuKl.2G3adWLGdaxz32GN7k+A...f3D+C....wI9G....hS7O....Dm3e....HNw+....Pbh+A...f3D+C....wI9G....hS7O....Dm3e....HNw+....Pbh+A...f3D+C....wI9G....hS7O....Dm3e....HNw+....Pbh+A...f3D+C....wI9G....hS7O....Dm3e....HNw+....Pbh+A...f3D+C....wI9G....hS7O....Dm3e....HNw+....Pbh+A...f3D+C....w8d6AferVqaOB7wduu8HvGNKNrGNlvuS5rXNbe.3OYB+9.G9cx4vW9G....hS7O....Dm3e....HNw+....Pbh+A...f3D+C....wI9G....hS7O....Dm3e....HNw+....Pbh+A...f3D+C....wI9G....hS7O....Dm3e....HNw+....Pbh+A...f3D+C....wI9G....hS7O....Dm3e....HNw+....Pbh+A...f3D+C....wI9G....hS7O....Dm3e....HNw+....Pbh+A...f3D+C....wI9G....hS7O....D26sG..9aVq0sGA9Xu22dDFA2ImiIbmz8giIbV..+c9x+....Pbh+A...f3D+C....wI9G....hS7O....Dm3e....HNw+....Pbh+A...f3D+C....wI9G....hS7O....Dm3e....HNw+....Pbh+A...f3D+C....wI9G....hS7O....Dm3e....HNw+....Pbh+A...f3D+C....wI9G....hS7O....Dm3e....HNw+....Pbh+A...f3D+C....wI9G....hS7O....Dm3e....HNw+....Pbh+A...f3du8..veyduu8H7rVqaOBiXOLASXO39vbLgyBNbV.v74K+C....wI9G....hS7O....Dm3e....HNw+....Pbh+A...f3D+C....wI9G....hS7O....Dm3e....HNw+....Pbh+A...f3D+C....wI9G....hS7O....Dm3e....HNw+....Pbh+A...f3D+C....wI9G....hS7O....Dm3e....HNw+....Pbh+A...f3D+C....wI9G....hS7O....Dm3e....HNw+....Pbh+A...f3D+C....wI9G....h681C.+Xu22dDfwYsV2dD3CmEG9s5iIbeXBmE1CvL4cA767k+A...f3D+C....wI9G....hS7O....Dm3e....HNw+....Pbh+A...f3D+C....wI9G....hS7O....Dm3e....HNw+....Pbh+A...f3D+C....wI9G....hS7O....Dm3e....HNw+....Pbh+A...f3D+C....wI9G....hS7O....Dm3e....HNw+....Pbh+A...f3D+C....wI9G....hS7O....Dm3e....HNw+....Pbh+A...f3D+C....w8d6AXJVq0sGAfgZu22dDFwuQYOLGSXO39vg8vg8vwD1CSvDNK.9c9x+....Pbh+A...f3D+C....wI9G....hS7O....Dm3e....HNw+....Pbh+A...f3D+C....wI9G....hS7O....Dm3e....HNw+....Pbh+A...f3D+C....wI9G....hS7O....Dm3e....HNw+....Pbh+A...f3D+C....wI9G....hS7O....Dm3e....HNw+....Pbh+A...f3D+C....wI9G....hS7O....Dm3e....HNw+....Pbh+A...f3V68de6g.fIasV2dD3C+k0wDtS5rXNlv8AN7t.Xx7k+A...f3D+C....wI9G....hS7O....Dm3e....HNw+....Pbh+A...f3D+C....wI9G....hS7O....Dm3e....HNw+....Pbh+A...f3D+C....wI9G....hS7O....Dm3e....HNw+....Pbh+A...f3D+C....wI9G....hS7O....Dm3e....HNw+....Pbh+A...f3D+C....wI9G....hS7O....Dm3e....HNw+....Pbh+A...f3D+C....w8d6A3444YsV2dDfewduu8H3cA+B2I4atObXObXObXObXObXObXObLg8vD3K+C....wI9G....hS7O....Dm3e....HNw+....Pbh+A...f3D+C....wI9G....hS7O....Dm3e....HNw+....Pbh+A...f3D+C....wI9G....hS7O....Dm3e....HNw+....Pbh+A...f3D+C....wI9G....hS7O....Dm3e....HNw+....Pbh+A...f3D+C....wI9G....hS7O....Dm3e....HNw+....Pbh+A...f3D+C....wI9G....has268sGhIXsV2dDXP7r3XBuKbVb3r3XB6gIXBmESvDtOLgyhIrG.9cS32G3XB+N4DtO3K+C....wI9G....hS7O....Dm3e....HNw+....Pbh+A...f3D+C....wI9G....hS7O....Dm3e....HNw+....Pbh+A...f3D+C....wI9G....hS7O....Dm3e....HNw+....Pbh+A...f3D+C....wI9G....hS7O....Dm3e....HNw+....Pbh+A...f3D+C....wI9G....hS7O....Dm3e....HNw+....Pbh+A...f3D+C....wI9G....h681CvyyyyZst8H7r26aOB1C7Klv8gIXB6AuKliIbVLg6jSXFl.2GNlvdXBlvYwD39vg6C7MuKN7k+A...f3D+C....wI9G....hS7O....Dm3e....HNw+....Pbh+A...f3D+C....wI9G....hS7O....Dm3e....HNw+....Pbh+A...f3D+C....wI9G....hS7O....Dm3e....HNw+....Pbh+A...f3D+C....wI9G....hS7O....Dm3e....HNw+....Pbh+A...f3D+C....wI9G....hS7O....Dm3e....HNw+....Pbh+A...f3D+C....w8d6AXJVq0sGA9XBmE6891ivHlgIXB2Gl.2GNbe3v8g4XBmES3cwD1CSf8vg6jG1CG1Cygu7O....Dm3e....HNw+....Pbh+A...f3D+C....wI9G....hS7O....Dm3e....HNw+....Pbh+A...f3D+C....wI9G....hS7O....Dm3e....HNw+....Pbh+A...f3D+C....wI9G....hS7O....Dm3e....HNw+....Pbh+A...f3D+C....wI9G....hS7O....Dm3e....HNw+....Pbh+A...f3D+C....wI9G....hS7O....Dm3e....Ht0du22dH3XsV2dDFAWIOlv8AmEGS3rXBlv8gIbVXOLGNKNrGNlvd.91DdWLAdaNG9x+....Pbh+A...f3D+C....wI9G....hS7O....Dm3e....HNw+....Pbh+A...f3D+C....wI9G....hS7O....Dm3e....HNw+....Pbh+A...f3D+C....wI9G....hS7O....Dm3e....HNw+....Pbh+A...f3D+C....wI9G....hS7O....Dm3e....HNw+....Pbh+A...f3D+C....wI9G....hS7O....Dm3e....HNw+....Pbh+A...f3du8.7777rVqaOBO6891ivHXOb3N4wD1CSvDNK3vYwg8vwD9MJmE7sIbmj4vuOLGS3s4DtO3K+C....wI9G....hS7O....Dm3e....HNw+....Pbh+A...f3D+C....wI9G....hS7O....Dm3e....HNw+....Pbh+A...f3D+C....wI9G....hS7O....Dm3e....HNw+....Pbh+A...f3D+C....wI9G....hS7O....Dm3e....HNw+....Pbh+A...f3D+C....wI9G....hS7O....Dm3e....HNw+....Pbh+A...f3D+C....wI9G....has268sGBli0Zc6Q3YBWImvdfC2GNrGliIbVLAtOv2lv6B2IOlvYwD39vb3N4b3K+C....wI9G....hS7O....Dm3e....HNw+....Pbh+A...f3D+C....wI9G....hS7O....Dm3e....HNw+....Pbh+A...f3D+C....wI9G....hS7O....Dm3e....HNw+....Pbh+A...f3D+C....wI9G....hS7O....Dm3e....HNw+....Pbh+A...f3D+C....wI9G....hS7O....Dm3e....HNw+....Pbh+A...f3D+C....wI9G....has2680Gh051i.eLfqCtO7wDNKl.2G3adWbLg2ES3rvd3vdXNlvYAyg2EGS3cgyhCe4e....HNw+....Pbh+A...f3D+C....wI9G....hS7O....Dm3e....HNw+....Pbh+A...f3D+C....wI9G....hS7O....Dm3e....HNw+....Pbh+A...f3D+C....wI9G....hS7O....Dm3e....HNw+....Pbh+A...f3D+C....wI9G....hS7O....Dm3e....HNw+....Pbh+A...f3D+C....wI9G....hS7O....Dm3e....HNw+....Pbu2d.fIZu22dDdVq0sGgQXBmEb3N4b3cwwD1CdWvz3cwg8vbLgyBN7k+A...f3D+C....wI9G....hS7O....Dm3e....HNw+....Pbh+A...f3D+C....wI9G....hS7O....Dm3e....HNw+....Pbh+A...f3D+C....wI9G....hS7O....Dm3e....HNw+....Pbh+A...f3D+C....wI9G....hS7O....Dm3e....HNw+....Pbh+A...f3D+C....wI9G....hS7O....Dm3e....HNw+....Pbh+A...f3D+C....w8d6AXJ168sGA9vYwwD1Cq051ivHXOb3NIeaB2GXN71bNbVbLg8vD9cxIrGlfIrGlv8Ae4e....HNw+....Pbh+A...f3D+C....wI9G....hS7O....Dm3e....HNw+....Pbh+A...f3D+C....wI9G....hS7O....Dm3e....HNw+....Pbh+A...f3D+C....wI9G....hS7O....Dm3e....HNw+....Pbh+A...f3D+C....wI9G....hS7O....Dm3e....HNw+....Pbh+A...f3D+C....wI9G....hS7O....Dm3e....HNw+....Pbu2d.3Gq051ivHr26aOBNK9XBmESvD1CtSd3rXNrGliI7tXBlvcxIbVLg8.LMS3s4D3K+C....wI9G....hS7O....Dm3e....HNw+....Pbh+A...f3D+C....wI9G....hS7O....Dm3e....HNw+....Pbh+A...f3D+C....wI9G....hS7O....Dm3e....HNw+....Pbh+A...f3D+C....wI9G....hS7O....Dm3e....HNw+....Pbh+A...f3D+C....wI9G....hS7O....Dm3e....HNw+....Pbh+A...f3D+C....wI9G....h681C.yxduu8H7rVqaOBiXOLANKNrGliIbVLAS39vDNKlvd.91DdWLAdaNGS3rXBuKlvdXB7k+A...f3D+C....wI9G....hS7O....Dm3e....HNw+....Pbh+A...f3D+C....wI9G....hS7O....Dm3e....HNw+....Pbh+A...f3D+C....wI9G....hS7O....Dm3e....HNw+....Pbh+A...f3D+C....wI9G....hS7O....Dm3e....HNw+....Pbh+A...f3D+C....wI9G....hS7O....Dm3e....HNw+....Pbh+A...f3D+C....w8d6AfYYsV2dDd168sGgQrG3vYASiei5XB6gIvYwwD1CLGtOb3s4bLg8vDtO3K+C....wI9G....hS7O....Dm3e....HNw+....Pbh+A...f3D+C....wI9G....hS7O....Dm3e....HNw+....Pbh+A...f3D+C....wI9G....hS7O....Dm3e....HNw+....Pbh+A...f3D+C....wI9G....hS7O....Dm3e....HNw+....Pbh+A...f3D+C....wI9G....hS7O....Dm3e....HNw+....Pbh+A...f3D+C....wI9G....has268sGB.9SVq0sGgmI7yj1Cvuy6hC6gC6A91DtO.eaB+9fu7O....Dm3e....HNw+....Pbh+A...f3D+C....wI9G....hS7O....Dm3e....HNw+....Pbh+A...f3D+C....wI9G....hS7O....Dm3e....HNw+....Pbh+A...f3D+C....wI9G....hS7O....Dm3e....HNw+....Pbh+A...f3D+C....wI9G....hS7O....Dm3e....HNw+....Pbh+A...f3D+C....wI9G....hS7O....Dm3e....Ht2aO.OOOOq051i.v+g8de6QfOlvYgeq9XBmESf6CG1CG1CywDNK76j7M2GlCe4e....HNw+....Pbh+A...f3D+C....wI9G....hS7O....Dm3e....HNw+....Pbh+A...f3D+C....wI9G....hS7O....Dm3e....HNw+....Pbh+A...f3D+C....wI9G....hS7O....Dm3e....HNw+....Pbh+A...f3D+C....wI9G....hS7O....Dm3e....HNw+....Pbh+A...f3D+C....wI9G....hS7O....Dm3e....HNw+....Pbu2d.lh8de6Q.9Eq051ivHLg2lS3rXB6gIvd3XB2I4XB2IcefuMg6jb3r3vuQcLg6C9x+....Pbh+A...f3D+C....wI9G....hS7O....Dm3e....HNw+....Pbh+A...f3D+C....wI9G....hS7O....Dm3e....HNw+....Pbh+A...f3D+C....wI9G....hS7O....Dm3e....HNw+....Pbh+A...f3D+C....wI9G....hS7O....Dm3e....HNw+....Pbh+A...f3D+C....wI9G....hS7O....Dm3e....HNw+....Pbh+A...f3du8.vOVq0sGA9Xu22dD3CuKNrG3a9Mp4XBuMce3XBmESXFfuMg6j9Mp4vW9G....hS7O....Dm3e....HNw+....Pbh+A...f3D+C....wI9G....hS7O....Dm3e....HNw+....Pbh+A...f3D+C....wI9G....hS7O....Dm3e....HNw+....Pbh+A...f3D+C....wI9G....hS7O....Dm3e....HNw+....Pbh+A...f3D+C....wI9G....hS7O....Dm3e....HNw+....Pbh+A...f3D+C....wI9G....hS7O....D26sG..3+s8de6QXDVq0sGA9XB2Imv8A6g4XBmEv2717vd3XB+Fku7O....Dm3e....HNw+....Pbh+A...f3D+C....wI9G....hS7O....Dm3e....HNw+....Pbh+A...f3D+C....wI9G....hS7O....Dm3e....HNw+....Pbh+A...f3D+C....wI9G....hS7O....Dm3e....HNw+....Pbh+A...f3D+C....wI9G....hS7O....Dm3e....HNw+....Pbh+A...f3D+C....wI9G....hS7O....Dm3e....Ht2aO..Lc6891i.eLgyh0Zc6QXDlvdv8AlF2GfYZB+ewD3K+C....wI9G....hS7O....Dm3e....HNw+....Pbh+A...f3D+C....wI9G....hS7O....Dm3e....HNw+....Pbh+A...f3D+C....wI9G....hS7O....Dm3e....HNw+....Pbh+A...f3D+C....wI9G....hS7O....Dm3e....HNw+....Pbh+A...f3D+C....wI9G....hS7O....Dm3e....HNw+....Pbh+A...f3D+C....wI9G....h681C.+Xu22dD.Fp0Zc6QXD+F0D1CSfyh4LCNKlC6A9l2lG1C7Me4e....HNw+....Pbh+A...f3D+C....wI9G....hS7O....Dm3e....HNw+....Pbh+A...f3D+C....wI9G....hS7O....Dm3e....HNw+....Pbh+A...f3D+C....wI9G....hS7O....Dm3e....HNw+....Pbh+A...f3D+C....wI9G....hS7O....Dm3e....HNw+....Pbh+A...f3D+C....wI9G....hS7O....Dm3e....HNw+....Pbu2d.lh0Zc6Q.f+n8de6QfOlvYg+y5vYwg8vg8vg8vblAmEGSXOvgu7O....Dm3e....HNw+....Pbh+A...f3D+C....wI9G....hS7O....Dm3e....HNw+....Pbh+A...f3D+C....wI9G....hS7O....Dm3e....HNw+....Pbh+A...f3D+C....wI9G....hS7O....Dm3e....HNw+....Pbh+A...f3D+C....wI9G....hS7O....Dm3e....HNw+....Pbh+A...f3D+C....wI9G....hS7O....Dm3e....Ht0du22dH..921yNlF....FDl+c8TwdHstf...vON+C....wI9G....hS7O....Dm3e....HNw+....Pbh+A...f3D+C....wI9G....hS7O....Dm3e....HNw+....Pbh+A...f3D+C....wI9G....hS7O....Dm3e....HNw+....Pbh+A...f3D+C....wI9G....hS7O....Dm3e....HNw+....Pbh+A...f3D+C....wI9G....hS7O....Dm3e....HNw+....Pbh+A...f3D+C....wI9G....hazahUcrEGcwL.....IUjSD4pPfIH" ],
									"embed" : 1,
									"forceaspect" : 1,
									"id" : "obj-1",
									"maxclass" : "fpic",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "jit_matrix" ],
									"patching_rect" : [ 908.0, 109.6875, 145.0, 145.0 ],
									"pic" : "qrcode-repo.png"
								}

							}
, 							{
								"box" : 								{
									"fontsize" : 14.0,
									"id" : "obj-76",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 26.0, 95.608035714286189, 184.0, 22.0 ],
									"text" : "2 Channel EMG Sonification"
								}

							}
, 							{
								"box" : 								{
									"fontface" : 0,
									"fontname" : "Ableton Sans Medium",
									"fontsize" : 54.0,
									"id" : "obj-46",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 23.0, 21.608035714286189, 702.0, 71.0 ],
									"text" : "AM'23 - EMG SONIFICATION"
								}

							}
, 							{
								"box" : 								{
									"bgcolor" : [ 0.058823529411765, 0.058823529411765, 0.058823529411765, 1.0 ],
									"displaychan" : 2,
									"fgcolor" : [ 0.403, 1.0, 0.2, 1.0 ],
									"gridcolor" : [ 0.137254901960784, 0.137254901960784, 0.137254901960784, 1.0 ],
									"id" : "obj-9",
									"maxclass" : "scope~",
									"numinlets" : 2,
									"numoutlets" : 0,
									"patching_rect" : [ 488.0, 511.565178571428532, 393.0, 161.0 ]
								}

							}
, 							{
								"box" : 								{
									"args" : [ 4 ],
									"bgmode" : 0,
									"border" : 1,
									"clickthrough" : 0,
									"enablehscroll" : 0,
									"enablevscroll" : 0,
									"id" : "obj-59",
									"lockeddragscroll" : 0,
									"lockedsize" : 0,
									"maxclass" : "bpatcher",
									"name" : "bbdmi.list2~.maxpat",
									"numinlets" : 2,
									"numoutlets" : 2,
									"offset" : [ 0.0, 0.0 ],
									"outlettype" : [ "multichannelsignal", "" ],
									"patching_rect" : [ 216.0, 258.313392857142844, 199.0, 35.0 ],
									"varname" : "bbdmi.list2~",
									"viewvisibility" : 1
								}

							}
, 							{
								"box" : 								{
									"annotation" : "",
									"args" : [ 4 ],
									"bgmode" : 0,
									"border" : 1,
									"clickthrough" : 0,
									"enablehscroll" : 0,
									"enablevscroll" : 0,
									"hint" : "",
									"id" : "obj-57",
									"lockeddragscroll" : 0,
									"lockedsize" : 0,
									"maxclass" : "bpatcher",
									"name" : "bbdmi.eavi.maxpat",
									"numinlets" : 1,
									"numoutlets" : 2,
									"offset" : [ 0.0, 0.0 ],
									"outlettype" : [ "", "" ],
									"patching_rect" : [ 216.0, 141.6875, 199.0, 88.0 ],
									"varname" : "bbdmi.eavi",
									"viewvisibility" : 1
								}

							}
, 							{
								"box" : 								{
									"fontsize" : 12.0,
									"id" : "obj-55",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 31.0, 511.565178571428532, 142.0, 20.0 ],
									"text" : "• 4 to 2 channels (stereo)"
								}

							}
, 							{
								"box" : 								{
									"bubble" : 1,
									"id" : "obj-53",
									"linecount" : 2,
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 460.5, 367.6875, 143.0, 37.0 ],
									"text" : "1. set to Direct\n2. set to LFP (800 Hz)"
								}

							}
, 							{
								"box" : 								{
									"bgcolor" : [ 1.0, 0.788235, 0.470588, 1.0 ],
									"bgoncolor" : [ 0.55, 0.55, 0.55, 1.0 ],
									"fontname" : "Arial Bold",
									"hint" : "",
									"id" : "obj-54",
									"ignoreclick" : 1,
									"legacytextcolor" : 1,
									"maxclass" : "textbutton",
									"numinlets" : 1,
									"numoutlets" : 3,
									"outlettype" : [ "", "", "int" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 433.0, 376.1875, 20.0, 20.0 ],
									"rounded" : 60.0,
									"text" : "4",
									"textcolor" : [ 0.34902, 0.34902, 0.34902, 1.0 ],
									"textoncolor" : [ 1.0, 1.0, 1.0, 1.0 ],
									"textovercolor" : [ 0.1, 0.1, 0.1, 1.0 ],
									"usebgoncolor" : 1,
									"usetextovercolor" : 1
								}

							}
, 							{
								"box" : 								{
									"bubble" : 1,
									"id" : "obj-47",
									"linecount" : 2,
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 460.5, 163.6875, 138.0, 37.0 ],
									"text" : "1. adjust parameters \n2. turn on simulator"
								}

							}
, 							{
								"box" : 								{
									"bgcolor" : [ 1.0, 0.788235, 0.470588, 1.0 ],
									"bgoncolor" : [ 0.55, 0.55, 0.55, 1.0 ],
									"fontname" : "Arial Bold",
									"hint" : "",
									"id" : "obj-52",
									"ignoreclick" : 1,
									"legacytextcolor" : 1,
									"maxclass" : "textbutton",
									"numinlets" : 1,
									"numoutlets" : 3,
									"outlettype" : [ "", "", "int" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 433.0, 172.1875, 20.0, 20.0 ],
									"rounded" : 60.0,
									"text" : "3",
									"textcolor" : [ 0.34902, 0.34902, 0.34902, 1.0 ],
									"textoncolor" : [ 1.0, 1.0, 1.0, 1.0 ],
									"textovercolor" : [ 0.1, 0.1, 0.1, 1.0 ],
									"usebgoncolor" : 1,
									"usetextovercolor" : 1
								}

							}
, 							{
								"box" : 								{
									"bubble" : 1,
									"id" : "obj-18",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 639.5, 744.003571428571604, 85.0, 24.0 ],
									"text" : "load preset"
								}

							}
, 							{
								"box" : 								{
									"bgcolor" : [ 1.0, 0.788235, 0.470588, 1.0 ],
									"bgoncolor" : [ 0.55, 0.55, 0.55, 1.0 ],
									"fontname" : "Arial Bold",
									"hint" : "",
									"id" : "obj-25",
									"ignoreclick" : 1,
									"legacytextcolor" : 1,
									"maxclass" : "textbutton",
									"numinlets" : 1,
									"numoutlets" : 3,
									"outlettype" : [ "", "", "int" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 612.0, 746.003571428571604, 20.0, 20.0 ],
									"rounded" : 60.0,
									"text" : "2",
									"textcolor" : [ 0.34902, 0.34902, 0.34902, 1.0 ],
									"textoncolor" : [ 1.0, 1.0, 1.0, 1.0 ],
									"textovercolor" : [ 0.1, 0.1, 0.1, 1.0 ],
									"usebgoncolor" : 1,
									"usetextovercolor" : 1
								}

							}
, 							{
								"box" : 								{
									"bubblesize" : 20,
									"id" : "obj-17",
									"maxclass" : "preset",
									"numinlets" : 1,
									"numoutlets" : 5,
									"outlettype" : [ "preset", "int", "preset", "int", "" ],
									"patching_rect" : [ 488.0, 741.503571428571604, 100.0, 29.0 ],
									"pattrstorage" : "am-sonif"
								}

							}
, 							{
								"box" : 								{
									"autorestore" : "am-sonif.json",
									"id" : "obj-3",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 488.0, 706.191071428571377, 384.0, 22.0 ],
									"saved_object_attributes" : 									{
										"client_rect" : [ 4, 44, 358, 172 ],
										"parameter_enable" : 0,
										"parameter_mappable" : 0,
										"storage_rect" : [ 583, 69, 1034, 197 ]
									}
,
									"text" : "pattrstorage am-sonif @autorestore 1 @changemode 1 @savemode 3",
									"varname" : "am-sonif"
								}

							}
, 							{
								"box" : 								{
									"args" : [ 2, "@dB", -6 ],
									"bgmode" : 0,
									"border" : 1,
									"clickthrough" : 0,
									"enablehscroll" : 0,
									"enablevscroll" : 0,
									"id" : "obj-6",
									"lockeddragscroll" : 0,
									"lockedsize" : 0,
									"maxclass" : "bpatcher",
									"name" : "bbdmi.dac~.maxpat",
									"numinlets" : 2,
									"numoutlets" : 0,
									"offset" : [ 0.0, 0.0 ],
									"patching_rect" : [ 216.0, 701.191071428571377, 198.0, 84.125000000000227 ],
									"varname" : "bbdmi.dac~",
									"viewvisibility" : 1
								}

							}
, 							{
								"box" : 								{
									"annotation" : "",
									"args" : [ 4, 2 ],
									"bgmode" : 0,
									"border" : 1,
									"clickthrough" : 0,
									"enablehscroll" : 0,
									"enablevscroll" : 0,
									"hint" : "",
									"id" : "obj-201",
									"lockeddragscroll" : 0,
									"lockedsize" : 0,
									"maxclass" : "bpatcher",
									"name" : "bbdmi.crosspatch~.maxpat",
									"numinlets" : 2,
									"numoutlets" : 2,
									"offset" : [ 0.0, 0.0 ],
									"outlettype" : [ "multichannelsignal", "" ],
									"patching_rect" : [ 216.0, 511.565178571428532, 198.0, 160.999999999999943 ],
									"varname" : "bbdmi.crosspatch~",
									"viewvisibility" : 1
								}

							}
, 							{
								"box" : 								{
									"annotation" : "",
									"args" : [ 4, "@cutoff", 100.0 ],
									"bgmode" : 0,
									"border" : 1,
									"clickthrough" : 0,
									"enablehscroll" : 0,
									"enablevscroll" : 0,
									"hint" : "",
									"id" : "obj-175",
									"lockeddragscroll" : 0,
									"lockedsize" : 0,
									"maxclass" : "bpatcher",
									"name" : "bbdmi.onepole~.maxpat",
									"numinlets" : 2,
									"numoutlets" : 2,
									"offset" : [ 0.0, 0.0 ],
									"outlettype" : [ "multichannelsignal", "" ],
									"patching_rect" : [ 216.0, 321.939285714285745, 198.0, 160.999999999999972 ],
									"varname" : "bbdmi.onepole~",
									"viewvisibility" : 1
								}

							}
, 							{
								"box" : 								{
									"bubble" : 1,
									"id" : "obj-27",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 113.5, 731.253571428571604, 72.0, 24.0 ],
									"text" : "start dsp"
								}

							}
, 							{
								"box" : 								{
									"bgcolor" : [ 1.0, 0.788235, 0.470588, 1.0 ],
									"bgoncolor" : [ 0.55, 0.55, 0.55, 1.0 ],
									"fontname" : "Arial Bold",
									"hint" : "",
									"id" : "obj-35",
									"ignoreclick" : 1,
									"legacytextcolor" : 1,
									"maxclass" : "textbutton",
									"numinlets" : 1,
									"numoutlets" : 3,
									"outlettype" : [ "", "", "int" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 86.0, 733.253571428571604, 20.0, 20.0 ],
									"rounded" : 60.0,
									"text" : "1",
									"textcolor" : [ 0.34902, 0.34902, 0.34902, 1.0 ],
									"textoncolor" : [ 1.0, 1.0, 1.0, 1.0 ],
									"textovercolor" : [ 0.1, 0.1, 0.1, 1.0 ],
									"usebgoncolor" : 1,
									"usetextovercolor" : 1
								}

							}
, 							{
								"box" : 								{
									"fontsize" : 12.0,
									"id" : "obj-22",
									"linecount" : 2,
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 26.0, 321.939285714285745, 132.0, 33.0 ],
									"text" : "• Listen to Direct or Lowpass filtered signal"
								}

							}
, 							{
								"box" : 								{
									"fontsize" : 14.0,
									"id" : "obj-23",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 26.0, 297.939285714285802, 107.0, 22.0 ],
									"text" : "SONIFICATION",
									"underline" : 1
								}

							}
, 							{
								"box" : 								{
									"fontsize" : 12.0,
									"id" : "obj-8",
									"linecount" : 2,
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 26.0, 165.6875, 128.0, 33.0 ],
									"text" : "• Simulate EMG signal\n  (e.g. 4 CHANNELS)"
								}

							}
, 							{
								"box" : 								{
									"fontsize" : 14.0,
									"id" : "obj-2",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 26.0, 141.6875, 50.0, 22.0 ],
									"text" : "INPUT",
									"underline" : 1
								}

							}
, 							{
								"box" : 								{
									"arrows" : 2,
									"border" : 2.0,
									"id" : "obj-81",
									"maxclass" : "live.line",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 19.0, 297.939285714285802, 10.0, 374.62589285714273 ]
								}

							}
, 							{
								"box" : 								{
									"arrows" : 2,
									"border" : 2.0,
									"id" : "obj-80",
									"maxclass" : "live.line",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 19.0, 141.6875, 10.0, 82.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-213",
									"maxclass" : "ezdac~",
									"numinlets" : 2,
									"numoutlets" : 0,
									"patching_rect" : [ 19.0, 716.003571428571604, 54.5, 54.5 ]
								}

							}
 ],
						"lines" : [ 							{
								"patchline" : 								{
									"destination" : [ "obj-201", 0 ],
									"midpoints" : [ 225.5, 480.6875, 225.5, 480.6875 ],
									"order" : 1,
									"source" : [ "obj-175", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-9", 0 ],
									"midpoints" : [ 225.5, 495.6875, 497.5, 495.6875 ],
									"order" : 0,
									"source" : [ "obj-175", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-6", 0 ],
									"midpoints" : [ 225.5, 672.6875, 225.5, 672.6875 ],
									"source" : [ "obj-201", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-59", 0 ],
									"midpoints" : [ 225.5, 231.6875, 225.5, 231.6875 ],
									"source" : [ "obj-57", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-175", 0 ],
									"midpoints" : [ 225.5, 303.6875, 225.5, 303.6875 ],
									"source" : [ "obj-59", 0 ]
								}

							}
 ]
					}
,
					"patching_rect" : [ 23.0, 36.0, 103.0, 22.0 ],
					"saved_object_attributes" : 					{
						"description" : "",
						"digest" : "",
						"globalpatchername" : "",
						"tags" : ""
					}
,
					"text" : "p SONIFICATION",
					"varname" : "sonification"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-1",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 0,
					"patcher" : 					{
						"fileversion" : 1,
						"appversion" : 						{
							"major" : 8,
							"minor" : 5,
							"revision" : 6,
							"architecture" : "x64",
							"modernui" : 1
						}
,
						"classnamespace" : "box",
						"rect" : [ 34.0, 92.0, 1444.0, 856.0 ],
						"bglocked" : 0,
						"openinpresentation" : 0,
						"default_fontsize" : 12.0,
						"default_fontface" : 0,
						"default_fontname" : "Arial",
						"gridonopen" : 1,
						"gridsize" : [ 15.0, 15.0 ],
						"gridsnaponopen" : 1,
						"objectsnaponopen" : 1,
						"statusbarvisible" : 2,
						"toolbarvisible" : 1,
						"lefttoolbarpinned" : 0,
						"toptoolbarpinned" : 0,
						"righttoolbarpinned" : 0,
						"bottomtoolbarpinned" : 0,
						"toolbars_unpinned_last_save" : 0,
						"tallnewobj" : 0,
						"boxanimatetime" : 200,
						"enablehscroll" : 1,
						"enablevscroll" : 1,
						"devicewidth" : 0.0,
						"description" : "",
						"digest" : "",
						"tags" : "",
						"style" : "",
						"subpatcher_template" : "",
						"showontab" : 1,
						"assistshowspatchername" : 0,
						"boxes" : [ 							{
								"box" : 								{
									"id" : "obj-4",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1024.0, 1461.892857142856883, 72.0, 22.0 ],
									"text" : "prepend set"
								}

							}
, 							{
								"box" : 								{
									"args" : [ 2, "@amount", 0.75 ],
									"bgmode" : 0,
									"border" : 1,
									"clickthrough" : 0,
									"enablehscroll" : 0,
									"enablevscroll" : 0,
									"id" : "obj-48",
									"lockeddragscroll" : 0,
									"lockedsize" : 0,
									"maxclass" : "bpatcher",
									"name" : "bbdmi.smooth.maxpat",
									"numinlets" : 2,
									"numoutlets" : 1,
									"offset" : [ 0.0, 0.0 ],
									"outlettype" : [ "float" ],
									"patching_rect" : [ 216.0, 764.0, 198.0, 66.0 ],
									"presentation" : 1,
									"presentation_rect" : [ 230.0, 762.876071428570867, 198.0, 66.0 ],
									"varname" : "bbdmi.smooth",
									"viewvisibility" : 1
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-99",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 1024.0, 1724.55178571428587, 39.0, 20.0 ],
									"presentation" : 1,
									"presentation_rect" : [ 379.0, 724.063571428570867, 39.0, 20.0 ],
									"text" : "mode"
								}

							}
, 							{
								"box" : 								{
									"format" : 6,
									"id" : "obj-97",
									"maxclass" : "flonum",
									"maximum" : 1.0,
									"minimum" : 0.0,
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 1024.0, 1533.991071428571331, 50.0, 22.0 ],
									"presentation" : 1,
									"presentation_rect" : [ 379.0, 680.869642857142708, 49.0, 22.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-94",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1158.116064263196904, 135.026785714285666, 80.0, 22.0 ],
									"text" : "crosspatch[2]"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-92",
									"maxclass" : "newobj",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 543.5, 980.0, 49.0, 22.0 ],
									"text" : "r output"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-91",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 222.5, 1693.0, 51.0, 22.0 ],
									"text" : "s output"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-89",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 75.0, 772.991071428571331, 70.0, 22.0 ],
									"text" : "loadmess 0"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-88",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"patching_rect" : [ 75.0, 980.0, 29.5, 22.0 ],
									"text" : "+ 1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-86",
									"maxclass" : "newobj",
									"numinlets" : 3,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 74.5, 1339.0, 302.0, 22.0 ],
									"text" : "switch 2 1"
								}

							}
, 							{
								"box" : 								{
									"annotation" : "",
									"args" : [ 2, 9 ],
									"bgcolor" : [ 0.180392156862745, 0.423529411764706, 0.776470588235294, 1.0 ],
									"bgmode" : 0,
									"border" : 1,
									"clickthrough" : 0,
									"enablehscroll" : 0,
									"enablevscroll" : 0,
									"hint" : "",
									"id" : "obj-80",
									"lockeddragscroll" : 0,
									"lockedsize" : 0,
									"maxclass" : "bpatcher",
									"name" : "bbdmi.crosspatch.maxpat",
									"numinlets" : 2,
									"numoutlets" : 1,
									"offset" : [ 0.0, 0.0 ],
									"outlettype" : [ "" ],
									"patching_rect" : [ 216.0, 1027.991071428571331, 199.0, 281.439285714285234 ],
									"presentation" : 1,
									"presentation_rect" : [ 488.0, 141.6875, 200.0, 281.439285714285234 ],
									"varname" : "bbdmi.crosspatch[1]",
									"viewvisibility" : 1
								}

							}
, 							{
								"box" : 								{
									"htabcolor" : [ 0.298039215686275, 0.749019607843137, 0.980392156862745, 1.0 ],
									"id" : "obj-75",
									"maxclass" : "tab",
									"numinlets" : 1,
									"numoutlets" : 3,
									"outlettype" : [ "int", "", "" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 75.0, 838.991071428571331, 124.0, 115.0 ],
									"presentation" : 1,
									"presentation_rect" : [ 1301.07372727272741, 141.6875, 113.92627272727259, 281.439285714285234 ],
									"rounded" : 0.0,
									"tabcolor" : [ 0.188235294117647, 0.188235294117647, 0.188235294117647, 1.0 ],
									"tabs" : [ "direct", "regression" ]
								}

							}
, 							{
								"box" : 								{
									"fontsize" : 12.0,
									"id" : "obj-65",
									"linecount" : 2,
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 26.0, 165.6875, 128.0, 33.0 ],
									"text" : "• Simulate EMG signal\n  (e.g. 4 CHANNELS)"
								}

							}
, 							{
								"box" : 								{
									"fontsize" : 14.0,
									"id" : "obj-66",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 26.0, 141.6875, 50.0, 22.0 ],
									"text" : "INPUT",
									"underline" : 1
								}

							}
, 							{
								"box" : 								{
									"arrows" : 2,
									"border" : 2.0,
									"id" : "obj-67",
									"maxclass" : "live.line",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 19.0, 141.6875, 10.0, 82.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-64",
									"maxclass" : "newobj",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1027.0, 1261.991071428571331, 68.0, 22.0 ],
									"text" : "r calibrated"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-63",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 345.0, 980.0, 70.0, 22.0 ],
									"text" : "s calibrated"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-60",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"patching_rect" : [ 751.0, 1370.301785714285643, 58.0, 22.0 ],
									"text" : "loadbang"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-57",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"patching_rect" : [ 843.0, 81.601785714286052, 58.0, 22.0 ],
									"text" : "loadbang"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-55",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1246.539277115836285, 135.026785714285666, 57.0, 22.0 ],
									"text" : "speedlim"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-42",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1069.692851410557523, 135.026785714285666, 80.0, 22.0 ],
									"text" : "crosspatch[1]"
								}

							}
, 							{
								"box" : 								{
									"annotation" : "",
									"args" : [ 4, 2 ],
									"bgcolor" : [ 0.180392156862745, 0.423529411764706, 0.776470588235294, 1.0 ],
									"bgmode" : 0,
									"border" : 1,
									"clickthrough" : 0,
									"enablehscroll" : 0,
									"enablevscroll" : 0,
									"hint" : "",
									"id" : "obj-8",
									"lockeddragscroll" : 0,
									"lockedsize" : 0,
									"maxclass" : "bpatcher",
									"name" : "bbdmi.crosspatch.maxpat",
									"numinlets" : 2,
									"numoutlets" : 1,
									"offset" : [ 0.0, 0.0 ],
									"outlettype" : [ "" ],
									"patching_rect" : [ 216.0, 261.601785714285995, 199.0, 161.439285714284779 ],
									"presentation" : 1,
									"presentation_rect" : [ 26.0, 233.736071428571591, 199.0, 161.439285714284779 ],
									"varname" : "bbdmi.crosspatch",
									"viewvisibility" : 1
								}

							}
, 							{
								"box" : 								{
									"args" : [ 2 ],
									"bgcolor" : [ 0.694117647058824, 0.035294117647059, 0.0, 1.0 ],
									"bgmode" : 0,
									"border" : 1,
									"clickthrough" : 0,
									"enablehscroll" : 0,
									"enablevscroll" : 0,
									"id" : "obj-31",
									"lockeddragscroll" : 0,
									"lockedsize" : 0,
									"maxclass" : "bpatcher",
									"name" : "bbdmi.eavi.maxpat",
									"numinlets" : 1,
									"numoutlets" : 2,
									"offset" : [ 0.0, 0.0 ],
									"outlettype" : [ "", "" ],
									"patching_rect" : [ 216.0, 141.6875, 198.0, 88.0 ],
									"presentation" : 1,
									"presentation_rect" : [ 26.0, 141.6875, 199.0, 88.0 ],
									"viewvisibility" : 1
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-61",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 994.0, 1768.794642857142662, 55.0, 22.0 ],
									"text" : "mode $1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-56",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 994.0, 1810.991071428571331, 44.0, 22.0 ],
									"text" : "s nime"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-84",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1311.962489968475666, 135.026785714285666, 37.0, 22.0 ],
									"text" : "scale"
								}

							}
, 							{
								"box" : 								{
									"annotation" : "",
									"args" : [ 9 ],
									"bgcolor" : [ 0.180392156862745, 0.423529411764706, 0.776470588235294, 1.0 ],
									"bgmode" : 0,
									"border" : 1,
									"clickthrough" : 0,
									"enablehscroll" : 0,
									"enablevscroll" : 0,
									"hint" : "",
									"id" : "obj-83",
									"lockeddragscroll" : 0,
									"lockedsize" : 0,
									"maxclass" : "bpatcher",
									"name" : "bbdmi.scale.maxpat",
									"numinlets" : 1,
									"numoutlets" : 1,
									"offset" : [ 0.0, 0.0 ],
									"outlettype" : [ "" ],
									"patching_rect" : [ 74.5, 2127.270833333333485, 199.0, 281.439285714285234 ],
									"presentation" : 1,
									"presentation_rect" : [ 896.666666666666742, 141.6875, 199.0, 281.439285714285234 ],
									"varname" : "bbdmi.scale",
									"viewvisibility" : 1
								}

							}
, 							{
								"box" : 								{
									"bubble" : 1,
									"id" : "obj-82",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 759.0, 2713.751785714284779, 90.0, 24.0 ],
									"presentation" : 1,
									"presentation_rect" : [ 1235.5, 502.272499999999582, 90.0, 24.0 ],
									"text" : "load sample"
								}

							}
, 							{
								"box" : 								{
									"bgcolor" : [ 1.0, 0.788235, 0.470588, 1.0 ],
									"fontname" : "Arial Bold",
									"hint" : "",
									"id" : "obj-81",
									"ignoreclick" : 1,
									"legacytextcolor" : 1,
									"maxclass" : "textbutton",
									"numinlets" : 1,
									"numoutlets" : 3,
									"outlettype" : [ "", "", "int" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 729.5, 2715.751785714284779, 20.0, 20.0 ],
									"presentation" : 1,
									"presentation_rect" : [ 1206.0, 504.272499999999582, 20.0, 20.0 ],
									"rounded" : 60.0,
									"text" : "3",
									"textcolor" : [ 0.34902, 0.34902, 0.34902, 1.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-79",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 640.5, 2714.751785714284779, 77.0, 22.0 ],
									"presentation" : 1,
									"presentation_rect" : [ 1120.0, 503.272499999999582, 77.0, 22.0 ],
									"text" : "file drumloop"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-78",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 662.5, 2757.751785714284779, 126.0, 22.0 ],
									"presentation" : 1,
									"presentation_rect" : [ 1120.0, 470.772499999999582, 126.0, 22.0 ],
									"text" : "folder C74/media/msp"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-74",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1357.385702821115046, 135.026785714285666, 38.0, 22.0 ],
									"text" : "2max"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-73",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 843.0, 135.026785714285666, 67.0, 22.0 ],
									"text" : "crosspatch"
								}

							}
, 							{
								"box" : 								{
									"annotation" : "",
									"args" : [ 9, 9 ],
									"bgcolor" : [ 0.180392156862745, 0.423529411764706, 0.776470588235294, 1.0 ],
									"bgmode" : 0,
									"border" : 1,
									"clickthrough" : 0,
									"enablehscroll" : 0,
									"enablevscroll" : 0,
									"hint" : "",
									"id" : "obj-72",
									"lockeddragscroll" : 0,
									"lockedsize" : 0,
									"maxclass" : "bpatcher",
									"name" : "bbdmi.crosspatch.maxpat",
									"numinlets" : 2,
									"numoutlets" : 1,
									"offset" : [ 0.0, 0.0 ],
									"outlettype" : [ "" ],
									"patching_rect" : [ 74.5, 1738.876785714285461, 199.0, 281.439285714285234 ],
									"varname" : "bbdmi.crosspatch[2]",
									"viewvisibility" : 1
								}

							}
, 							{
								"box" : 								{
									"args" : [ "@dB", -20 ],
									"bgmode" : 0,
									"border" : 1,
									"clickthrough" : 0,
									"enablehscroll" : 0,
									"enablevscroll" : 0,
									"id" : "obj-69",
									"lockeddragscroll" : 0,
									"lockedsize" : 0,
									"maxclass" : "bpatcher",
									"name" : "bbdmi.dac~.maxpat",
									"numinlets" : 2,
									"numoutlets" : 0,
									"offset" : [ 0.0, 0.0 ],
									"patching_rect" : [ 74.5, 3152.749999999999545, 196.92627272727259, 85.625000000000114 ],
									"presentation" : 1,
									"presentation_rect" : [ 898.703530303030448, 734.063571428570867, 196.92627272727259, 85.625000000000114 ],
									"varname" : "bbdmi.dac~",
									"viewvisibility" : 1
								}

							}
, 							{
								"box" : 								{
									"args" : [ "am" ],
									"bgmode" : 0,
									"border" : 1,
									"clickthrough" : 0,
									"enablehscroll" : 0,
									"enablevscroll" : 0,
									"id" : "obj-70",
									"lockeddragscroll" : 0,
									"lockedsize" : 0,
									"maxclass" : "bpatcher",
									"name" : "bbdmi.granulator~.maxpat",
									"numinlets" : 3,
									"numoutlets" : 1,
									"offset" : [ 0.0, 0.0 ],
									"outlettype" : [ "multichannelsignal" ],
									"patching_rect" : [ 74.5, 2804.937499999999545, 607.0, 301.0 ],
									"presentation" : 1,
									"presentation_rect" : [ 488.0, 428.063571428570867, 607.666666666666742, 301.0 ],
									"varname" : "bbdmi.granulator~",
									"viewvisibility" : 1
								}

							}
, 							{
								"box" : 								{
									"fontsize" : 14.0,
									"id" : "obj-76",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 26.0, 95.608035714286189, 157.0, 22.0 ],
									"presentation" : 1,
									"presentation_rect" : [ 26.0, 95.608035714286189, 157.0, 22.0 ],
									"text" : "2 Channel EMG Control"
								}

							}
, 							{
								"box" : 								{
									"fontface" : 0,
									"fontname" : "Ableton Sans Medium",
									"fontsize" : 54.0,
									"id" : "obj-46",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 23.0, 21.608035714286189, 588.0, 71.0 ],
									"presentation" : 1,
									"presentation_rect" : [ 23.0, 21.608035714286189, 626.0, 71.0 ],
									"text" : "AM'23 - EMG CONTROL"
								}

							}
, 							{
								"box" : 								{
									"candycane" : 16,
									"contdata" : 1,
									"id" : "obj-54",
									"maxclass" : "multislider",
									"numinlets" : 1,
									"numoutlets" : 2,
									"orientation" : 0,
									"outlettype" : [ "", "" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 1024.0, 1498.991071428571104, 169.0, 22.0 ],
									"presentation" : 1,
									"presentation_rect" : [ 230.0, 680.869642857142708, 147.0, 22.0 ],
									"setminmax" : [ 0.0, 1.0 ],
									"setstyle" : 1,
									"signed" : 1,
									"slidercolor" : [ 0.694117647058824, 0.0, 0.0, 1.0 ],
									"spacing" : 2,
									"varname" : "Multislider-MS"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-53",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 994.0, 1189.991071428571331, 70.0, 22.0 ],
									"text" : "loadmess 1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-51",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 1027.0, 1225.991071428571331, 45.0, 20.0 ],
									"presentation" : 1,
									"presentation_rect" : [ 233.0, 710.063571428570867, 45.0, 20.0 ],
									"text" : "on / off"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-37",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 1080.0, 1613.551785714285643, 23.0, 20.0 ],
									"presentation" : 1,
									"presentation_rect" : [ 290.0, 710.063571428570867, 23.0, 20.0 ],
									"text" : ">="
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-7",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1024.0, 1579.991071428571331, 80.0, 22.0 ],
									"text" : "loadmess 0.3"
								}

							}
, 							{
								"box" : 								{
									"args" : [ 9, "@time", 50 ],
									"bgcolor" : [ 0.772549019607843, 0.831372549019608, 0.294117647058824, 1.0 ],
									"bgmode" : 0,
									"border" : 1,
									"clickthrough" : 0,
									"enablehscroll" : 0,
									"enablevscroll" : 0,
									"id" : "obj-50",
									"lockeddragscroll" : 0,
									"lockedsize" : 0,
									"maxclass" : "bpatcher",
									"name" : "bbdmi.speedlim.maxpat",
									"numinlets" : 2,
									"numoutlets" : 1,
									"offset" : [ 0.0, 0.0 ],
									"outlettype" : [ "" ],
									"patching_rect" : [ 74.5, 2040.793452380951976, 199.0, 66.0 ],
									"varname" : "bbdmi.speedlim",
									"viewvisibility" : 1
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-41",
									"maxclass" : "toggle",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 994.0, 1223.991071428571331, 24.0, 24.0 ],
									"presentation" : 1,
									"presentation_rect" : [ 244.5, 732.063571428570867, 22.0, 22.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-38",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 994.0, 1301.991071428571331, 52.0, 22.0 ],
									"text" : "gate 1 1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-33",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 3,
									"outlettype" : [ "", "int", "int" ],
									"patching_rect" : [ 994.0, 1684.355357142856974, 48.0, 22.0 ],
									"text" : "change"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-32",
									"maxclass" : "led",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 994.0, 1722.551785714285643, 24.0, 24.0 ],
									"presentation" : 1,
									"presentation_rect" : [ 338.0, 714.563571428570867, 39.0, 39.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-21",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"patching_rect" : [ 994.0, 1650.551785714285643, 49.0, 22.0 ],
									"text" : ">= 0."
								}

							}
, 							{
								"box" : 								{
									"format" : 6,
									"id" : "obj-20",
									"maxclass" : "flonum",
									"maximum" : 1.0,
									"minimum" : 0.0,
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 1024.0, 1612.551785714285643, 50.0, 22.0 ],
									"presentation" : 1,
									"presentation_rect" : [ 278.0, 732.063571428570867, 49.0, 22.0 ]
								}

							}
, 							{
								"box" : 								{
									"args" : [ 4 ],
									"bgcolor" : [ 0.772549019607843, 0.831372549019608, 0.294117647058824, 1.0 ],
									"bgmode" : 0,
									"border" : 1,
									"clickthrough" : 0,
									"enablehscroll" : 0,
									"enablevscroll" : 0,
									"id" : "obj-12",
									"lockeddragscroll" : 0,
									"lockedsize" : 0,
									"maxclass" : "bpatcher",
									"name" : "bbdmi.average.maxpat",
									"numinlets" : 1,
									"numoutlets" : 1,
									"offset" : [ 0.0, 0.0 ],
									"outlettype" : [ "float" ],
									"patching_rect" : [ 994.0, 1375.794642857142662, 199.0, 35.0 ],
									"presentation" : 1,
									"presentation_rect" : [ 230.0, 646.369642857142708, 198.0, 35.0 ],
									"varname" : "bbdmi.average",
									"viewvisibility" : 1
								}

							}
, 							{
								"box" : 								{
									"annotation" : "",
									"args" : [ 9 ],
									"bgcolor" : [ 0.180392156862745, 0.423529411764706, 0.776470588235294, 1.0 ],
									"bgmode" : 0,
									"border" : 1,
									"clickthrough" : 0,
									"enablehscroll" : 0,
									"enablevscroll" : 0,
									"hint" : "",
									"id" : "obj-2",
									"lockeddragscroll" : 0,
									"lockedsize" : 0,
									"maxclass" : "bpatcher",
									"name" : "bbdmi.2max.maxpat",
									"numinlets" : 1,
									"numoutlets" : 0,
									"offset" : [ 0.0, 0.0 ],
									"patching_rect" : [ 74.5, 2429.1875, 199.0, 281.439285714285234 ],
									"presentation" : 1,
									"presentation_rect" : [ 1100.5, 141.6875, 199.0, 281.439285714285234 ],
									"varname" : "bbdmi.2max",
									"viewvisibility" : 1
								}

							}
, 							{
								"box" : 								{
									"bubble" : 1,
									"id" : "obj-14",
									"linecount" : 2,
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 294.0, 1506.051785714285415, 127.0, 37.0 ],
									"text" : "watch\noutput / predictions"
								}

							}
, 							{
								"box" : 								{
									"annotation" : "",
									"args" : [ 2 ],
									"bgcolor" : [ 0.717647058823529, 0.0, 0.0, 1.0 ],
									"bgmode" : 0,
									"border" : 1,
									"clickthrough" : 0,
									"enablehscroll" : 0,
									"enablevscroll" : 0,
									"hint" : "",
									"id" : "obj-9",
									"lockeddragscroll" : 0,
									"lockedsize" : 0,
									"maxclass" : "bpatcher",
									"name" : "bbdmi.multislider.maxpat",
									"numinlets" : 2,
									"numoutlets" : 1,
									"offset" : [ 0.0, 0.0 ],
									"outlettype" : [ "" ],
									"patching_rect" : [ 216.0, 840.991071428571331, 198.0, 113.0 ],
									"presentation" : 1,
									"presentation_rect" : [ 26.0, 646.369642857142708, 198.999999999999972, 113.0 ],
									"varname" : "bbdmi.multislider[2]",
									"viewvisibility" : 1
								}

							}
, 							{
								"box" : 								{
									"bubble" : 1,
									"id" : "obj-40",
									"linecount" : 3,
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 697.0, 1060.126785714285234, 128.0, 51.0 ],
									"text" : "1. record examples\n2. train model\n3. run"
								}

							}
, 							{
								"box" : 								{
									"bgcolor" : [ 1.0, 0.788235, 0.470588, 1.0 ],
									"fontname" : "Arial Bold",
									"hint" : "",
									"id" : "obj-34",
									"ignoreclick" : 1,
									"legacytextcolor" : 1,
									"maxclass" : "textbutton",
									"numinlets" : 1,
									"numoutlets" : 3,
									"outlettype" : [ "", "", "int" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 669.5, 1075.626785714285234, 20.0, 20.0 ],
									"rounded" : 60.0,
									"text" : "5",
									"textcolor" : [ 0.34902, 0.34902, 0.34902, 1.0 ]
								}

							}
, 							{
								"box" : 								{
									"bgcolor" : [ 1.0, 0.788235, 0.470588, 1.0 ],
									"fontname" : "Arial Bold",
									"hint" : "",
									"id" : "obj-16",
									"ignoreclick" : 1,
									"legacytextcolor" : 1,
									"maxclass" : "textbutton",
									"numinlets" : 1,
									"numoutlets" : 3,
									"outlettype" : [ "", "", "int" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 569.0, 1587.051785714285643, 20.0, 20.0 ],
									"presentation" : 1,
									"presentation_rect" : [ 857.333333333333485, 47.108035714286189, 20.0, 20.0 ],
									"rounded" : 60.0,
									"text" : "4",
									"textcolor" : [ 0.34902, 0.34902, 0.34902, 1.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-138",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 918.423212852639381, 135.026785714285666, 37.0, 22.0 ],
									"text" : "list2~"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-137",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 963.846425705278762, 135.026785714285666, 35.0, 22.0 ],
									"text" : "rms~"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-136",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1007.269638557918142, 135.026785714285666, 54.0, 22.0 ],
									"text" : "calibrate"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-130",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 751.0, 1403.051785714285643, 63.0, 22.0 ],
									"text" : "multislider"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-131",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "", "" ],
									"patching_rect" : [ 602.0, 1436.051785714285643, 168.0, 22.0 ],
									"text" : "combine bbdmi. s @triggers 1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-132",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 602.0, 1466.526785714286007, 77.0, 22.0 ],
									"text" : "subscribe $1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-121",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "", "" ],
									"patching_rect" : [ 921.0, 182.626785714285688, 168.0, 22.0 ],
									"text" : "combine bbdmi. s @triggers 1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-118",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 921.0, 213.101785714286052, 77.0, 22.0 ],
									"text" : "subscribe $1"
								}

							}
, 							{
								"box" : 								{
									"bubble" : 1,
									"id" : "obj-104",
									"linecount" : 2,
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 602.0, 1578.551785714285643, 198.0, 37.0 ],
									"presentation" : 1,
									"presentation_linecount" : 2,
									"presentation_rect" : [ 890.333333333333485, 38.608035714286189, 198.0, 37.0 ],
									"text" : "1. set / adjust output parameters\n(optional) recall / save preset"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-106",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 453.5, 1466.526785714286007, 63.0, 22.0 ],
									"text" : "writeagain"
								}

							}
, 							{
								"box" : 								{
									"bubblesize" : 20,
									"id" : "obj-107",
									"maxclass" : "preset",
									"numinlets" : 1,
									"numoutlets" : 5,
									"outlettype" : [ "preset", "int", "preset", "int", "" ],
									"patching_rect" : [ 453.5, 1570.551785714285643, 101.0, 53.0 ],
									"pattrstorage" : "nime-output",
									"presentation" : 1,
									"presentation_rect" : [ 741.833333333333485, 30.608035714286189, 101.0, 53.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-108",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 453.5, 1513.551785714285643, 427.0, 22.0 ],
									"priority" : 									{
										"bbdmi.multislider::Multislider" : 1
									}
,
									"saved_object_attributes" : 									{
										"client_rect" : [ 4, 44, 358, 172 ],
										"parameter_enable" : 0,
										"parameter_mappable" : 0,
										"storage_rect" : [ 583, 69, 1034, 197 ]
									}
,
									"subscribe" : [ "bbdmi.multislider" ],
									"text" : "pattrstorage nime-output @autorestore 1 @changemode 1 @subscribemode 1",
									"varname" : "nime-output"
								}

							}
, 							{
								"box" : 								{
									"annotation" : "",
									"args" : [ 2, "@time", 20 ],
									"bgcolor" : [ 0.772549019607843, 0.831372549019608, 0.294117647058824, 1.0 ],
									"bgmode" : 0,
									"border" : 1,
									"clickthrough" : 0,
									"enablehscroll" : 0,
									"enablevscroll" : 0,
									"hint" : "",
									"id" : "obj-93",
									"lockeddragscroll" : 0,
									"lockedsize" : 0,
									"maxclass" : "bpatcher",
									"name" : "bbdmi.calibrate.maxpat",
									"numinlets" : 2,
									"numoutlets" : 1,
									"offset" : [ 0.0, 0.0 ],
									"outlettype" : [ "" ],
									"patching_rect" : [ 216.0, 634.991071428571331, 198.0, 113.0 ],
									"presentation" : 1,
									"presentation_rect" : [ 26.0, 529.321071428571145, 199.0, 113.0 ],
									"varname" : "bbdmi.calibrate",
									"viewvisibility" : 1
								}

							}
, 							{
								"box" : 								{
									"annotation" : "",
									"args" : [ 9, "@index", 1 ],
									"bgcolor" : [ 0.180392156862745, 0.423529411764706, 0.776470588235294, 1.0 ],
									"bgmode" : 0,
									"border" : 1,
									"clickthrough" : 0,
									"enablehscroll" : 0,
									"enablevscroll" : 0,
									"hint" : "",
									"id" : "obj-59",
									"lockeddragscroll" : 0,
									"lockedsize" : 0,
									"maxclass" : "bpatcher",
									"name" : "bbdmi.multislider.maxpat",
									"numinlets" : 2,
									"numoutlets" : 1,
									"offset" : [ 0.0, 0.0 ],
									"outlettype" : [ "" ],
									"patching_rect" : [ 74.5, 1383.832142857142799, 199.0, 281.439285714285234 ],
									"presentation" : 1,
									"presentation_rect" : [ 692.833333333333371, 141.6875, 199.0, 281.439285714285234 ],
									"varname" : "bbdmi.multislider",
									"viewvisibility" : 1
								}

							}
, 							{
								"box" : 								{
									"fontsize" : 12.0,
									"id" : "obj-26",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 294.0, 1383.832142857142799, 290.0, 20.0 ],
									"text" : " • Set sound synthesis parameters using a multislider"
								}

							}
, 							{
								"box" : 								{
									"fontsize" : 12.0,
									"id" : "obj-22",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 697.0, 1027.991071428571331, 237.0, 20.0 ],
									"text" : " • Record, train and run a regression model"
								}

							}
, 							{
								"box" : 								{
									"annotation" : "",
									"args" : [ 2, 9 ],
									"bgmode" : 0,
									"border" : 1,
									"clickthrough" : 0,
									"enablehscroll" : 0,
									"enablevscroll" : 0,
									"hint" : "",
									"id" : "obj-13",
									"lockeddragscroll" : 0,
									"lockedsize" : 0,
									"maxclass" : "bpatcher",
									"name" : "bbdmi.regress.maxpat",
									"numinlets" : 3,
									"numoutlets" : 2,
									"offset" : [ 0.0, 0.0 ],
									"outlettype" : [ "", "" ],
									"patching_rect" : [ 453.5, 1027.991071428571331, 199.0, 201.0 ],
									"presentation" : 1,
									"presentation_rect" : [ 230.0, 233.836071428571586, 199.0, 201.0 ],
									"varname" : "bbdmi.regress",
									"viewvisibility" : 1
								}

							}
, 							{
								"box" : 								{
									"args" : [ 2 ],
									"bgcolor" : [ 0.772549019607843, 0.831372549019608, 0.294117647058824, 1.0 ],
									"bgmode" : 0,
									"border" : 1,
									"clickthrough" : 0,
									"enablehscroll" : 0,
									"enablevscroll" : 0,
									"id" : "obj-23",
									"lockeddragscroll" : 0,
									"lockedsize" : 0,
									"maxclass" : "bpatcher",
									"name" : "bbdmi.list2~.maxpat",
									"numinlets" : 2,
									"numoutlets" : 2,
									"offset" : [ 0.0, 0.0 ],
									"outlettype" : [ "multichannelsignal", "" ],
									"patching_rect" : [ 216.0, 454.955357142856769, 199.0, 35.0 ],
									"presentation" : 1,
									"presentation_rect" : [ 26.0, 399.223928571427962, 199.0, 35.0 ],
									"varname" : "bbdmi.list2~",
									"viewvisibility" : 1
								}

							}
, 							{
								"box" : 								{
									"bubble" : 1,
									"id" : "obj-47",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 453.5, 173.1875, 120.0, 24.0 ],
									"text" : "turn board on / off"
								}

							}
, 							{
								"box" : 								{
									"bgcolor" : [ 1.0, 0.788235, 0.470588, 1.0 ],
									"fontname" : "Arial Bold",
									"hint" : "",
									"id" : "obj-52",
									"ignoreclick" : 1,
									"legacytextcolor" : 1,
									"maxclass" : "textbutton",
									"numinlets" : 1,
									"numoutlets" : 3,
									"outlettype" : [ "", "", "int" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 425.0, 175.1875, 20.0, 20.0 ],
									"rounded" : 60.0,
									"text" : "4",
									"textcolor" : [ 0.34902, 0.34902, 0.34902, 1.0 ]
								}

							}
, 							{
								"box" : 								{
									"bubble" : 1,
									"id" : "obj-58",
									"linecount" : 3,
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 453.5, 549.6875, 149.0, 51.0 ],
									"text" : "(optional) adjust window size for more or less responsiveness"
								}

							}
, 							{
								"box" : 								{
									"candycane" : 8,
									"id" : "obj-29",
									"maxclass" : "multislider",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 453.5, 261.601785714285995, 352.0, 161.0 ],
									"presentation" : 1,
									"presentation_rect" : [ 230.0, 141.6875, 199.0, 88.0 ],
									"setstyle" : 3,
									"size" : 2
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-28",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 74.5, 3288.626785714285234, 150.0, 20.0 ]
								}

							}
, 							{
								"box" : 								{
									"bubble" : 1,
									"id" : "obj-18",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 971.5, 305.626785714285688, 112.0, 24.0 ],
									"presentation" : 1,
									"presentation_rect" : [ 1249.0, 46.108035714286189, 112.0, 24.0 ],
									"text" : "load preset (init)"
								}

							}
, 							{
								"box" : 								{
									"bgcolor" : [ 1.0, 0.788235, 0.470588, 1.0 ],
									"fontname" : "Arial Bold",
									"hint" : "",
									"id" : "obj-25",
									"ignoreclick" : 1,
									"legacytextcolor" : 1,
									"maxclass" : "textbutton",
									"numinlets" : 1,
									"numoutlets" : 3,
									"outlettype" : [ "", "", "int" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 944.0, 307.626785714285688, 20.0, 20.0 ],
									"presentation" : 1,
									"presentation_rect" : [ 1219.07372727272741, 48.108035714286189, 20.0, 20.0 ],
									"rounded" : 60.0,
									"text" : "2",
									"textcolor" : [ 0.34902, 0.34902, 0.34902, 1.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-19",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 835.0, 213.101785714286052, 63.0, 22.0 ],
									"text" : "writeagain"
								}

							}
, 							{
								"box" : 								{
									"bubblesize" : 20,
									"id" : "obj-17",
									"maxclass" : "preset",
									"numinlets" : 1,
									"numoutlets" : 5,
									"outlettype" : [ "preset", "int", "preset", "int", "" ],
									"patching_rect" : [ 835.0, 303.126785714285688, 53.0, 29.0 ],
									"pattrstorage" : "am-init",
									"presentation" : 1,
									"presentation_rect" : [ 1156.375, 42.608035714286189, 53.25, 29.0 ]
								}

							}
, 							{
								"box" : 								{
									"autorestore" : "am-init.json",
									"id" : "obj-3",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 835.0, 261.126785714285688, 481.0, 22.0 ],
									"priority" : 									{
										"bbdmi.crosspatch[2]::Crosspatch" : 1,
										"bbdmi.crosspatch::Crosspatch" : 1,
										"bbdmi.crosspatch[1]::Crosspatch" : 1
									}
,
									"saved_object_attributes" : 									{
										"client_rect" : [ 4, 44, 358, 172 ],
										"parameter_enable" : 0,
										"parameter_mappable" : 0,
										"storage_rect" : [ 583, 69, 1034, 197 ]
									}
,
									"subscribe" : [ "bbdmi.calibrate", "bbdmi.speedlim", "bbdmi.rms~", "bbdmi.crosspatch", "bbdmi.scale", "bbdmi.2max", "bbdmi.crosspatch[2]", "bbdmi.list2~", "bbdmi.crosspatch[1]" ],
									"text" : "pattrstorage am-init @autorestore 1 @changemode 1 @subscribemode 1 @savemode 3",
									"varname" : "am-init"
								}

							}
, 							{
								"box" : 								{
									"bubble" : 1,
									"id" : "obj-27",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 929.5, 383.351785714286052, 72.0, 24.0 ],
									"presentation" : 1,
									"presentation_rect" : [ 795.129803030303037, 764.876071428570867, 72.0, 24.0 ],
									"text" : "start dsp"
								}

							}
, 							{
								"box" : 								{
									"bgcolor" : [ 1.0, 0.788235, 0.470588, 1.0 ],
									"fontname" : "Arial Bold",
									"hint" : "",
									"id" : "obj-35",
									"ignoreclick" : 1,
									"legacytextcolor" : 1,
									"maxclass" : "textbutton",
									"numinlets" : 1,
									"numoutlets" : 3,
									"outlettype" : [ "", "", "int" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 902.0, 385.351785714286052, 20.0, 20.0 ],
									"presentation" : 1,
									"presentation_rect" : [ 767.629803030303037, 766.876071428570867, 20.0, 20.0 ],
									"rounded" : 60.0,
									"text" : "1",
									"textcolor" : [ 0.34902, 0.34902, 0.34902, 1.0 ]
								}

							}
, 							{
								"box" : 								{
									"arrows" : 2,
									"border" : 2.0,
									"id" : "obj-30",
									"maxclass" : "live.line",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 19.0, 517.6875, 10.0, 87.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-11",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 460.5, 840.991071428571331, 370.0, 20.0 ],
									"text" : " • Calibrate dynamic minimum–maximum input over a window of 10''"
								}

							}
, 							{
								"box" : 								{
									"fontsize" : 12.0,
									"id" : "obj-10",
									"linecount" : 2,
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 26.0, 517.6875, 168.0, 33.0 ],
									"text" : "• Calculate root-mean-square \n(RMS) on a sliding window",
									"textjustification" : 1
								}

							}
, 							{
								"box" : 								{
									"fontsize" : 14.0,
									"id" : "obj-1",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 26.0, 493.6875, 167.0, 22.0 ],
									"text" : "FEATURE EXTRACTION",
									"underline" : 1
								}

							}
, 							{
								"box" : 								{
									"fontsize" : 14.0,
									"id" : "obj-36",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 460.5, 816.991071428571331, 174.0, 22.0 ],
									"text" : "CONTROL PROCESSING",
									"underline" : 1
								}

							}
, 							{
								"box" : 								{
									"arrows" : 2,
									"border" : 2.0,
									"id" : "obj-90",
									"maxclass" : "live.line",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 453.5, 840.991071428571331, 11.0, 113.0 ]
								}

							}
, 							{
								"box" : 								{
									"candycane" : 8,
									"id" : "obj-87",
									"maxclass" : "multislider",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 453.5, 634.991071428571331, 352.0, 113.0 ],
									"presentation" : 1,
									"presentation_rect" : [ 230.0, 438.572499999999536, 199.0, 204.0 ],
									"setminmax" : [ 0.0, 1.0 ],
									"setstyle" : 3,
									"size" : 2
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-213",
									"maxclass" : "ezdac~",
									"numinlets" : 2,
									"numoutlets" : 0,
									"patching_rect" : [ 835.0, 368.101785714286052, 54.5, 54.5 ],
									"presentation" : 1,
									"presentation_rect" : [ 700.629803030303037, 749.626071428570867, 52.5, 52.5 ]
								}

							}
, 							{
								"box" : 								{
									"args" : [ 2, "@winsize", 1.0, "@clock", 10 ],
									"bgcolor" : [ 0.772549019607843, 0.831372549019608, 0.294117647058824, 1.0 ],
									"bgmode" : 0,
									"border" : 1,
									"clickthrough" : 0,
									"enablehscroll" : 0,
									"enablevscroll" : 0,
									"id" : "obj-5",
									"lockeddragscroll" : 0,
									"lockedsize" : 0,
									"maxclass" : "bpatcher",
									"name" : "bbdmi.rms~.maxpat",
									"numinlets" : 2,
									"numoutlets" : 1,
									"offset" : [ 0.0, 0.0 ],
									"outlettype" : [ "" ],
									"patching_rect" : [ 216.0, 521.869642857142708, 198.0, 87.0 ],
									"presentation" : 1,
									"presentation_rect" : [ 26.0, 438.272499999999525, 199.0, 87.0 ],
									"varname" : "bbdmi.rms~",
									"viewvisibility" : 1
								}

							}
, 							{
								"box" : 								{
									"angle" : 270.0,
									"bgcolor" : [ 0.2, 0.2, 0.2, 0.0 ],
									"border" : 1,
									"bordercolor" : [ 0.0, 0.0, 0.0, 1.0 ],
									"id" : "obj-62",
									"maxclass" : "panel",
									"mode" : 0,
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 1024.0, 1498.991071428571331, 169.0, 22.0 ],
									"presentation" : 1,
									"presentation_rect" : [ 230.0, 646.369642857142708, 198.0, 113.196428571428669 ],
									"proportion" : 0.5,
									"rounded" : 0
								}

							}
 ],
						"lines" : [ 							{
								"patchline" : 								{
									"destination" : [ "obj-108", 0 ],
									"midpoints" : [ 463.0, 1491.0, 463.0, 1491.0 ],
									"source" : [ "obj-106", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-3", 0 ],
									"midpoints" : [ 930.5, 246.0, 844.5, 246.0 ],
									"source" : [ "obj-118", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-21", 0 ],
									"midpoints" : [ 1003.5, 1413.0, 1003.5, 1413.0 ],
									"order" : 1,
									"source" : [ "obj-12", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-4", 0 ],
									"midpoints" : [ 1003.5, 1446.0, 1033.5, 1446.0 ],
									"order" : 0,
									"source" : [ "obj-12", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-118", 0 ],
									"midpoints" : [ 930.5, 207.0, 930.5, 207.0 ],
									"source" : [ "obj-121", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-86", 2 ],
									"midpoints" : [ 463.0, 1326.0, 367.0, 1326.0 ],
									"source" : [ "obj-13", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-131", 1 ],
									"midpoints" : [ 760.5, 1428.0, 760.5, 1428.0 ],
									"source" : [ "obj-130", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-132", 0 ],
									"midpoints" : [ 611.5, 1461.0, 611.5, 1461.0 ],
									"source" : [ "obj-131", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-108", 0 ],
									"midpoints" : [ 611.5, 1500.0, 463.0, 1500.0 ],
									"source" : [ "obj-132", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-121", 1 ],
									"midpoints" : [ 1016.769638557918142, 168.0, 1079.5, 168.0 ],
									"source" : [ "obj-136", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-121", 1 ],
									"midpoints" : [ 973.346425705278762, 168.0, 1079.5, 168.0 ],
									"source" : [ "obj-137", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-121", 1 ],
									"midpoints" : [ 927.923212852639381, 168.0, 1079.5, 168.0 ],
									"source" : [ "obj-138", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-3", 0 ],
									"midpoints" : [ 844.5, 237.0, 844.5, 237.0 ],
									"source" : [ "obj-19", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-21", 1 ],
									"midpoints" : [ 1033.5, 1635.0, 1033.5, 1635.0 ],
									"source" : [ "obj-20", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-33", 0 ],
									"midpoints" : [ 1003.5, 1674.0, 1003.5, 1674.0 ],
									"source" : [ "obj-21", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-5", 0 ],
									"midpoints" : [ 225.5, 492.0, 225.5, 492.0 ],
									"source" : [ "obj-23", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-8", 0 ],
									"midpoints" : [ 225.5, 231.0, 225.5, 231.0 ],
									"source" : [ "obj-31", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-61", 0 ],
									"midpoints" : [ 1003.5, 1749.0, 1003.5, 1749.0 ],
									"source" : [ "obj-32", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-32", 0 ],
									"midpoints" : [ 1003.5, 1707.0, 1003.5, 1707.0 ],
									"source" : [ "obj-33", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-12", 0 ],
									"midpoints" : [ 1003.5, 1326.0, 1003.5, 1326.0 ],
									"source" : [ "obj-38", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-54", 0 ],
									"midpoints" : [ 1033.5, 1485.0, 1033.5, 1485.0 ],
									"source" : [ "obj-4", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-38", 0 ],
									"midpoints" : [ 1003.5, 1248.0, 1003.5, 1248.0 ],
									"source" : [ "obj-41", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-121", 1 ],
									"midpoints" : [ 1079.192851410557523, 159.0, 1079.5, 159.0 ],
									"source" : [ "obj-42", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-9", 0 ],
									"midpoints" : [ 225.5, 831.0, 225.5, 831.0 ],
									"source" : [ "obj-48", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-93", 0 ],
									"midpoints" : [ 225.5, 609.0, 225.5, 609.0 ],
									"source" : [ "obj-5", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-83", 0 ],
									"midpoints" : [ 84.0, 2109.0, 84.0, 2109.0 ],
									"source" : [ "obj-50", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-41", 0 ],
									"midpoints" : [ 1003.5, 1212.0, 1003.5, 1212.0 ],
									"source" : [ "obj-53", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-97", 0 ],
									"midpoints" : [ 1033.5, 1521.0, 1033.5, 1521.0 ],
									"source" : [ "obj-54", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-121", 1 ],
									"midpoints" : [ 1256.039277115836285, 168.0, 1079.5, 168.0 ],
									"source" : [ "obj-55", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-136", 0 ],
									"midpoints" : [ 852.5, 120.0, 1016.769638557918142, 120.0 ],
									"order" : 5,
									"source" : [ "obj-57", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-137", 0 ],
									"midpoints" : [ 852.5, 120.0, 973.346425705278762, 120.0 ],
									"order" : 6,
									"source" : [ "obj-57", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-138", 0 ],
									"midpoints" : [ 852.5, 120.0, 927.923212852639381, 120.0 ],
									"order" : 7,
									"source" : [ "obj-57", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-42", 0 ],
									"midpoints" : [ 852.5, 120.0, 1079.192851410557523, 120.0 ],
									"order" : 4,
									"source" : [ "obj-57", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-55", 0 ],
									"midpoints" : [ 852.5, 120.0, 1256.039277115836285, 120.0 ],
									"order" : 2,
									"source" : [ "obj-57", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-73", 0 ],
									"midpoints" : [ 852.5, 105.0, 852.5, 105.0 ],
									"order" : 8,
									"source" : [ "obj-57", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-74", 0 ],
									"midpoints" : [ 852.5, 120.0, 1366.885702821115046, 120.0 ],
									"order" : 0,
									"source" : [ "obj-57", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-84", 0 ],
									"midpoints" : [ 852.5, 120.0, 1321.462489968475666, 120.0 ],
									"order" : 1,
									"source" : [ "obj-57", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-94", 0 ],
									"midpoints" : [ 852.5, 120.0, 1167.616064263196904, 120.0 ],
									"order" : 3,
									"source" : [ "obj-57", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-72", 0 ],
									"midpoints" : [ 84.0, 1668.0, 84.0, 1668.0 ],
									"order" : 1,
									"source" : [ "obj-59", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-91", 0 ],
									"midpoints" : [ 84.0, 1680.0, 232.0, 1680.0 ],
									"order" : 0,
									"source" : [ "obj-59", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-130", 0 ],
									"midpoints" : [ 760.5, 1395.0, 760.5, 1395.0 ],
									"source" : [ "obj-60", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-56", 0 ],
									"midpoints" : [ 1003.5, 1791.0, 1003.5, 1791.0 ],
									"source" : [ "obj-61", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-38", 1 ],
									"midpoints" : [ 1036.5, 1284.0, 1036.5, 1284.0 ],
									"source" : [ "obj-64", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-20", 0 ],
									"midpoints" : [ 1033.5, 1602.0, 1033.5, 1602.0 ],
									"source" : [ "obj-7", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-69", 0 ],
									"midpoints" : [ 84.0, 3108.0, 84.0, 3108.0 ],
									"source" : [ "obj-70", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-50", 0 ],
									"midpoints" : [ 84.0, 2022.0, 84.0, 2022.0 ],
									"source" : [ "obj-72", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-121", 1 ],
									"midpoints" : [ 852.5, 168.0, 1079.5, 168.0 ],
									"source" : [ "obj-73", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-121", 1 ],
									"midpoints" : [ 1366.885702821115046, 168.0, 1079.5, 168.0 ],
									"source" : [ "obj-74", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-88", 0 ],
									"midpoints" : [ 84.5, 954.0, 84.5, 954.0 ],
									"source" : [ "obj-75", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-70", 2 ],
									"midpoints" : [ 672.0, 2781.0, 672.0, 2781.0 ],
									"source" : [ "obj-78", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-70", 2 ],
									"midpoints" : [ 650.0, 2790.0, 672.0, 2790.0 ],
									"source" : [ "obj-79", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-23", 0 ],
									"midpoints" : [ 225.5, 426.0, 225.5, 426.0 ],
									"order" : 1,
									"source" : [ "obj-8", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-29", 0 ],
									"hidden" : 1,
									"midpoints" : [ 225.5, 432.0, 438.0, 432.0, 438.0, 255.0, 463.0, 255.0 ],
									"order" : 0,
									"source" : [ "obj-8", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-86", 1 ],
									"midpoints" : [ 225.5, 1311.0, 225.5, 1311.0 ],
									"source" : [ "obj-80", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-2", 0 ],
									"midpoints" : [ 84.0, 2409.0, 84.0, 2409.0 ],
									"source" : [ "obj-83", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-121", 1 ],
									"midpoints" : [ 1321.462489968475666, 168.0, 1079.5, 168.0 ],
									"source" : [ "obj-84", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-59", 0 ],
									"midpoints" : [ 84.0, 1362.0, 84.0, 1362.0 ],
									"source" : [ "obj-86", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-86", 0 ],
									"midpoints" : [ 84.5, 1005.0, 84.0, 1005.0 ],
									"source" : [ "obj-88", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-75", 0 ],
									"midpoints" : [ 84.5, 795.0, 84.5, 795.0 ],
									"source" : [ "obj-89", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-13", 0 ],
									"midpoints" : [ 225.5, 1014.0, 463.0, 1014.0 ],
									"order" : 2,
									"source" : [ "obj-9", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-63", 0 ],
									"midpoints" : [ 225.5, 966.0, 354.5, 966.0 ],
									"order" : 0,
									"source" : [ "obj-9", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-80", 0 ],
									"midpoints" : [ 225.5, 954.0, 225.5, 954.0 ],
									"order" : 1,
									"source" : [ "obj-9", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-13", 1 ],
									"midpoints" : [ 553.0, 1005.0, 553.0, 1005.0 ],
									"source" : [ "obj-92", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-48", 0 ],
									"midpoints" : [ 225.5, 750.0, 225.5, 750.0 ],
									"order" : 1,
									"source" : [ "obj-93", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-87", 0 ],
									"hidden" : 1,
									"midpoints" : [ 225.5, 750.0, 201.0, 750.0, 201.0, 621.0, 463.0, 621.0 ],
									"order" : 0,
									"source" : [ "obj-93", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-121", 1 ],
									"midpoints" : [ 1167.616064263196904, 168.0, 1079.5, 168.0 ],
									"source" : [ "obj-94", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-21", 0 ],
									"midpoints" : [ 1033.5, 1566.0, 1003.5, 1566.0 ],
									"source" : [ "obj-97", 0 ]
								}

							}
 ]
					}
,
					"patching_rect" : [ 24.0, 73.0, 77.0, 22.0 ],
					"saved_object_attributes" : 					{
						"description" : "",
						"digest" : "",
						"globalpatchername" : "",
						"tags" : ""
					}
,
					"text" : "p CONTROL",
					"varname" : "control"
				}

			}
 ],
		"lines" : [  ],
		"parameters" : 		{
			"obj-1::obj-13::obj-25" : [ "live.numbox[2]", "live.numbox", 0 ],
			"obj-1::obj-13::obj-96" : [ "live.numbox[6]", "live.numbox", 0 ],
			"obj-1::obj-2::obj-107::obj-27::obj-18" : [ "toggle[16]", "toggle", 0 ],
			"obj-1::obj-2::obj-107::obj-48" : [ "SendTo-TXT[8]", "SendTo-TXT", 0 ],
			"obj-1::obj-2::obj-107::obj-8" : [ "tab[78]", "tab[1]", 0 ],
			"obj-1::obj-2::obj-123::obj-27::obj-18" : [ "toggle[64]", "toggle", 0 ],
			"obj-1::obj-2::obj-123::obj-48" : [ "SendTo-TXT[43]", "SendTo-TXT", 0 ],
			"obj-1::obj-2::obj-123::obj-8" : [ "tab[141]", "tab[1]", 0 ],
			"obj-1::obj-2::obj-34::obj-27::obj-18" : [ "toggle[17]", "toggle", 0 ],
			"obj-1::obj-2::obj-34::obj-48" : [ "SendTo-TXT[12]", "SendTo-TXT", 0 ],
			"obj-1::obj-2::obj-34::obj-8" : [ "tab[79]", "tab[1]", 0 ],
			"obj-1::obj-2::obj-36::obj-27::obj-18" : [ "toggle[18]", "toggle", 0 ],
			"obj-1::obj-2::obj-36::obj-48" : [ "SendTo-TXT[14]", "SendTo-TXT", 0 ],
			"obj-1::obj-2::obj-36::obj-8" : [ "tab[80]", "tab[1]", 0 ],
			"obj-1::obj-2::obj-40::obj-27::obj-18" : [ "toggle[19]", "toggle", 0 ],
			"obj-1::obj-2::obj-40::obj-48" : [ "SendTo-TXT[16]", "SendTo-TXT", 0 ],
			"obj-1::obj-2::obj-40::obj-8" : [ "tab[81]", "tab[1]", 0 ],
			"obj-1::obj-2::obj-41::obj-27::obj-18" : [ "toggle[65]", "toggle", 0 ],
			"obj-1::obj-2::obj-41::obj-48" : [ "SendTo-TXT[9]", "SendTo-TXT", 0 ],
			"obj-1::obj-2::obj-41::obj-8" : [ "tab[82]", "tab[1]", 0 ],
			"obj-1::obj-2::obj-42::obj-27::obj-18" : [ "toggle[66]", "toggle", 0 ],
			"obj-1::obj-2::obj-42::obj-48" : [ "SendTo-TXT[13]", "SendTo-TXT", 0 ],
			"obj-1::obj-2::obj-42::obj-8" : [ "tab[83]", "tab[1]", 0 ],
			"obj-1::obj-2::obj-43::obj-27::obj-18" : [ "toggle[67]", "toggle", 0 ],
			"obj-1::obj-2::obj-43::obj-48" : [ "SendTo-TXT[17]", "SendTo-TXT", 0 ],
			"obj-1::obj-2::obj-43::obj-8" : [ "tab[84]", "tab[1]", 0 ],
			"obj-1::obj-2::obj-44::obj-27::obj-18" : [ "toggle[20]", "toggle", 0 ],
			"obj-1::obj-2::obj-44::obj-48" : [ "SendTo-TXT[18]", "SendTo-TXT", 0 ],
			"obj-1::obj-2::obj-44::obj-8" : [ "tab[85]", "tab[1]", 0 ],
			"obj-1::obj-2::obj-45::obj-27::obj-18" : [ "toggle[21]", "toggle", 0 ],
			"obj-1::obj-2::obj-45::obj-48" : [ "SendTo-TXT[19]", "SendTo-TXT", 0 ],
			"obj-1::obj-2::obj-45::obj-8" : [ "tab[86]", "tab[1]", 0 ],
			"obj-1::obj-2::obj-46::obj-27::obj-18" : [ "toggle[22]", "toggle", 0 ],
			"obj-1::obj-2::obj-46::obj-48" : [ "SendTo-TXT[20]", "SendTo-TXT", 0 ],
			"obj-1::obj-2::obj-46::obj-8" : [ "tab[87]", "tab[1]", 0 ],
			"obj-1::obj-2::obj-47::obj-27::obj-18" : [ "toggle[23]", "toggle", 0 ],
			"obj-1::obj-2::obj-47::obj-48" : [ "SendTo-TXT[21]", "SendTo-TXT", 0 ],
			"obj-1::obj-2::obj-47::obj-8" : [ "tab[88]", "tab[1]", 0 ],
			"obj-1::obj-2::obj-48::obj-27::obj-18" : [ "toggle[24]", "toggle", 0 ],
			"obj-1::obj-2::obj-48::obj-48" : [ "SendTo-TXT[22]", "SendTo-TXT", 0 ],
			"obj-1::obj-2::obj-48::obj-8" : [ "tab[89]", "tab[1]", 0 ],
			"obj-1::obj-2::obj-49::obj-27::obj-18" : [ "toggle[25]", "toggle", 0 ],
			"obj-1::obj-2::obj-49::obj-48" : [ "SendTo-TXT[23]", "SendTo-TXT", 0 ],
			"obj-1::obj-2::obj-49::obj-8" : [ "tab[90]", "tab[1]", 0 ],
			"obj-1::obj-2::obj-50::obj-27::obj-18" : [ "toggle[26]", "toggle", 0 ],
			"obj-1::obj-2::obj-50::obj-48" : [ "SendTo-TXT[24]", "SendTo-TXT", 0 ],
			"obj-1::obj-2::obj-50::obj-8" : [ "tab[116]", "tab[1]", 0 ],
			"obj-1::obj-2::obj-74::obj-27::obj-18" : [ "toggle[15]", "toggle", 0 ],
			"obj-1::obj-2::obj-74::obj-48" : [ "SendTo-TXT[15]", "SendTo-TXT", 0 ],
			"obj-1::obj-2::obj-74::obj-8" : [ "tab[77]", "tab[1]", 0 ],
			"obj-1::obj-48::obj-122" : [ "number[1]", "number[1]", 0 ],
			"obj-1::obj-48::obj-16" : [ "number[3]", "number[1]", 0 ],
			"obj-1::obj-69::obj-45" : [ "live.gain~[2]", "live.gain~", 0 ],
			"obj-1::obj-83::obj-107::obj-33" : [ "tab[93]", "tab[1]", 0 ],
			"obj-1::obj-83::obj-123::obj-33" : [ "tab[91]", "tab[1]", 0 ],
			"obj-1::obj-83::obj-34::obj-33" : [ "tab[94]", "tab[1]", 0 ],
			"obj-1::obj-83::obj-36::obj-33" : [ "tab[95]", "tab[1]", 0 ],
			"obj-1::obj-83::obj-40::obj-33" : [ "tab[96]", "tab[1]", 0 ],
			"obj-1::obj-83::obj-41::obj-33" : [ "tab[97]", "tab[1]", 0 ],
			"obj-1::obj-83::obj-42::obj-33" : [ "tab[98]", "tab[1]", 0 ],
			"obj-1::obj-83::obj-43::obj-33" : [ "tab[99]", "tab[1]", 0 ],
			"obj-1::obj-83::obj-44::obj-33" : [ "tab[100]", "tab[1]", 0 ],
			"obj-1::obj-83::obj-45::obj-33" : [ "tab[101]", "tab[1]", 0 ],
			"obj-1::obj-83::obj-46::obj-33" : [ "tab[126]", "tab[1]", 0 ],
			"obj-1::obj-83::obj-47::obj-33" : [ "tab[127]", "tab[1]", 0 ],
			"obj-1::obj-83::obj-48::obj-33" : [ "tab[128]", "tab[1]", 0 ],
			"obj-1::obj-83::obj-49::obj-33" : [ "tab[129]", "tab[1]", 0 ],
			"obj-1::obj-83::obj-50::obj-33" : [ "tab[130]", "tab[1]", 0 ],
			"obj-1::obj-83::obj-74::obj-33" : [ "tab[92]", "tab[1]", 0 ],
			"obj-1::obj-93::obj-107::obj-33" : [ "tab[63]", "tab[1]", 0 ],
			"obj-1::obj-93::obj-123::obj-33" : [ "tab[26]", "tab[1]", 0 ],
			"obj-1::obj-93::obj-34::obj-33" : [ "tab[64]", "tab[1]", 0 ],
			"obj-1::obj-93::obj-36::obj-33" : [ "tab[65]", "tab[1]", 0 ],
			"obj-1::obj-93::obj-40::obj-33" : [ "tab[66]", "tab[1]", 0 ],
			"obj-1::obj-93::obj-41::obj-33" : [ "tab[67]", "tab[1]", 0 ],
			"obj-1::obj-93::obj-42::obj-33" : [ "tab[68]", "tab[1]", 0 ],
			"obj-1::obj-93::obj-43::obj-33" : [ "tab[69]", "tab[1]", 0 ],
			"obj-1::obj-93::obj-44::obj-33" : [ "tab[70]", "tab[1]", 0 ],
			"obj-1::obj-93::obj-45::obj-33" : [ "tab[71]", "tab[1]", 0 ],
			"obj-1::obj-93::obj-46::obj-33" : [ "tab[72]", "tab[1]", 0 ],
			"obj-1::obj-93::obj-47::obj-33" : [ "tab[73]", "tab[1]", 0 ],
			"obj-1::obj-93::obj-48::obj-33" : [ "tab[74]", "tab[1]", 0 ],
			"obj-1::obj-93::obj-49::obj-33" : [ "tab[75]", "tab[1]", 0 ],
			"obj-1::obj-93::obj-50::obj-33" : [ "tab[76]", "tab[1]", 0 ],
			"obj-1::obj-93::obj-74::obj-33" : [ "tab[62]", "tab[1]", 0 ],
			"obj-2::obj-175::obj-107::obj-33" : [ "tab[48]", "tab[1]", 0 ],
			"obj-2::obj-175::obj-123::obj-33" : [ "tab[125]", "tab[1]", 0 ],
			"obj-2::obj-175::obj-36::obj-33" : [ "tab[50]", "tab[1]", 0 ],
			"obj-2::obj-175::obj-40::obj-33" : [ "tab[51]", "tab[1]", 0 ],
			"obj-2::obj-175::obj-41::obj-33" : [ "tab[52]", "tab[1]", 0 ],
			"obj-2::obj-175::obj-44::obj-33" : [ "tab[55]", "tab[1]", 0 ],
			"obj-2::obj-175::obj-45::obj-33" : [ "tab[56]", "tab[1]", 0 ],
			"obj-2::obj-175::obj-46::obj-33" : [ "tab[57]", "tab[1]", 0 ],
			"obj-2::obj-175::obj-47::obj-33" : [ "tab[58]", "tab[1]", 0 ],
			"obj-2::obj-175::obj-48::obj-33" : [ "tab[59]", "tab[1]", 0 ],
			"obj-2::obj-175::obj-49::obj-33" : [ "tab[60]", "tab[1]", 0 ],
			"obj-2::obj-175::obj-50::obj-33" : [ "tab[61]", "tab[1]", 0 ],
			"obj-2::obj-175::obj-6::obj-33" : [ "tab[54]", "tab[1]", 0 ],
			"obj-2::obj-175::obj-74::obj-33" : [ "tab[47]", "tab[1]", 0 ],
			"obj-2::obj-175::obj-8::obj-33" : [ "tab[53]", "tab[1]", 0 ],
			"obj-2::obj-175::obj-9::obj-33" : [ "tab[49]", "tab[1]", 0 ],
			"obj-2::obj-6::obj-45" : [ "live.gain~[3]", "live.gain~", 0 ],
			"parameterbanks" : 			{
				"0" : 				{
					"index" : 0,
					"name" : "",
					"parameters" : [ "-", "-", "-", "-", "-", "-", "-", "-" ]
				}

			}
,
			"parameter_overrides" : 			{
				"obj-1::obj-69::obj-45" : 				{
					"parameter_longname" : "live.gain~[2]"
				}
,
				"obj-2::obj-6::obj-45" : 				{
					"parameter_longname" : "live.gain~[3]"
				}

			}
,
			"inherited_shortname" : 1
		}
,
		"dependency_cache" : [ 			{
				"name" : "am-init.json",
				"bootpath" : "~/Documents/GitHub/bbdmi/events/2023-08-31_AM/presets",
				"patcherrelativepath" : "./presets",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "am-sonif.json",
				"bootpath" : "~/Documents/GitHub/bbdmi/events/2023-08-31_AM/presets",
				"patcherrelativepath" : "./presets",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bbdmi.2max.maxpat",
				"bootpath" : "~/Documents/GitHub/bbdmi/max/output/2max",
				"patcherrelativepath" : "../../max/output/2max",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bbdmi.average.maxpat",
				"bootpath" : "~/Documents/GitHub/bbdmi/max/control_processing/average",
				"patcherrelativepath" : "../../max/control_processing/average",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bbdmi.calibrate.maxpat",
				"bootpath" : "~/Documents/GitHub/bbdmi/max/control_processing/calibrate",
				"patcherrelativepath" : "../../max/control_processing/calibrate",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bbdmi.crosspatch.maxpat",
				"bootpath" : "~/Documents/GitHub/bbdmi/max/utilities/crosspatch",
				"patcherrelativepath" : "../../max/utilities/crosspatch",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bbdmi.crosspatch~.maxpat",
				"bootpath" : "~/Documents/GitHub/bbdmi/max/utilities/crosspatch~",
				"patcherrelativepath" : "../../max/utilities/crosspatch~",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bbdmi.dac~.maxpat",
				"bootpath" : "~/Documents/GitHub/bbdmi/max/utilities/dac~",
				"patcherrelativepath" : "../../max/utilities/dac~",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bbdmi.eavi.maxpat",
				"bootpath" : "~/Documents/GitHub/bbdmi/max/input/eavi",
				"patcherrelativepath" : "../../max/input/eavi",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bbdmi.granulator~.maxpat",
				"bootpath" : "~/Documents/GitHub/bbdmi/max/sound_synthesis/granulator~",
				"patcherrelativepath" : "../../max/sound_synthesis/granulator~",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bbdmi.list2~.maxpat",
				"bootpath" : "~/Documents/GitHub/bbdmi/max/utilities/list2~",
				"patcherrelativepath" : "../../max/utilities/list2~",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bbdmi.multislider.maxpat",
				"bootpath" : "~/Documents/GitHub/bbdmi/max/utilities/multislider",
				"patcherrelativepath" : "../../max/utilities/multislider",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bbdmi.onepole~.maxpat",
				"bootpath" : "~/Documents/GitHub/bbdmi/max/utilities/onepole~",
				"patcherrelativepath" : "../../max/utilities/onepole~",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bbdmi.regress.maxpat",
				"bootpath" : "~/Documents/GitHub/bbdmi/max/control_processing/regress",
				"patcherrelativepath" : "../../max/control_processing/regress",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bbdmi.rms~.maxpat",
				"bootpath" : "~/Documents/GitHub/bbdmi/max/feature_extraction/rms~",
				"patcherrelativepath" : "../../max/feature_extraction/rms~",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bbdmi.scale.maxpat",
				"bootpath" : "~/Documents/GitHub/bbdmi/max/control_processing/scale",
				"patcherrelativepath" : "../../max/control_processing/scale",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bbdmi.smooth.maxpat",
				"bootpath" : "~/Documents/GitHub/bbdmi/max/control_processing/smooth",
				"patcherrelativepath" : "../../max/control_processing/smooth",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bbdmi.speedlim.maxpat",
				"bootpath" : "~/Documents/GitHub/bbdmi/max/control_processing/speedlim",
				"patcherrelativepath" : "../../max/control_processing/speedlim",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "name.js",
				"bootpath" : "~/Documents/GitHub/bbdmi/max/source/js",
				"patcherrelativepath" : "../../max/source/js",
				"type" : "TEXT",
				"implicit" : 1
			}
, 			{
				"name" : "p.2max.maxpat",
				"bootpath" : "~/Documents/GitHub/bbdmi/max/output/2max",
				"patcherrelativepath" : "../../max/output/2max",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "p.calibrate.maxpat",
				"bootpath" : "~/Documents/GitHub/bbdmi/max/control_processing/calibrate",
				"patcherrelativepath" : "../../max/control_processing/calibrate",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "p.exposer.maxpat",
				"bootpath" : "~/Documents/GitHub/bbdmi/max/source/patchers",
				"patcherrelativepath" : "../../max/source/patchers",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "p.gran~.maxpat",
				"bootpath" : "~/Documents/GitHub/bbdmi/max/sound_synthesis/granulator~",
				"patcherrelativepath" : "../../max/sound_synthesis/granulator~",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "p.onepole~.maxpat",
				"bootpath" : "~/Documents/GitHub/bbdmi/max/utilities/onepole~",
				"patcherrelativepath" : "../../max/utilities/onepole~",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "p.scale.maxpat",
				"bootpath" : "~/Documents/GitHub/bbdmi/max/control_processing/scale",
				"patcherrelativepath" : "../../max/control_processing/scale",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "qrcode-repo.png",
				"bootpath" : "~/Documents/GitHub/bbdmi/source/images",
				"patcherrelativepath" : "../../source/images",
				"type" : "PNG",
				"implicit" : 1
			}
, 			{
				"name" : "qrcode-website.png",
				"bootpath" : "~/Documents/GitHub/bbdmi/source/images",
				"patcherrelativepath" : "../../source/images",
				"type" : "PNG",
				"implicit" : 1
			}
, 			{
				"name" : "rapid.regression.mxo",
				"type" : "iLaX"
			}
 ],
		"autosave" : 0
	}

}
