# BBDMI Faust EAVI/faust repository

## About

This folder contains [faust](https://faust.grame.fr/)* DSP patches for sonifying and processing electromyogram (EMG) and electroencephalogram (EEG) data using the [EAVI board](https://research.gold.ac.uk/id/eprint/26476/) running an [OpenWare](https://github.com/RebelTechnology/OpenWare) firmware codebase.


*"Faust (Functional Audio Stream) is a functional programming language for sound synthesis and audio processing with a strong focus on the design of synthesizers, musical instruments, audio effects, etc. Faust targets high-performance signal processing applications and audio plug-ins for a variety of platforms and standards." (https://faust.grame.fr/)"

# Copyright and license
Code and documentation is copyright (c) 2021–2022 by [BBDMI](https://bbdmi.nakala.fr/), released for free under the [MIT License](https://gitlab.huma-num.fr/bbdmi/bbdmi/-/blob/main/LICENSE).
