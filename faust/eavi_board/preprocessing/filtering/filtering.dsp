// dcblock : notch : lowpass : gain : envelope

import("stdfaust.lib");

channels = 4;

gain = hslider("Gain[OWL:A]",100,1,1000,0.01) : si.smooth(0.999);

dc = fi.dcblockerat(10); // -3dB cutoff frequency
notch = fi.notchw(2, 50); // width, freq--------------------------
lpf = fi.lowpass(4, 600); // order, cutoff frequency


emg = dc : notch : lpf * gain;
process = par(i, channels, emg);


