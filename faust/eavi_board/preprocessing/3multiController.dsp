import("stdfaust.lib");

channels = 4;

gain = hslider("Gain[OWL:A]",100,1,1000,0.01) : si.smooth(0.999);

dc = fi.dcblockerat(10); // -3dB cutoff frequency
notch = fi.notchw(2, 50); // width, freq--------------------------
lpf = fi.lowpass(4, 600); // order, cutoff frequency


emg = dc : notch : lpf * gain;

//BBDMI library-------------------
declare name "BBDMI Faust Lib";
declare author "Alain Bonardi";
declare author "David Fierro";
declare author "Anne Sedes";
declare author "Atau Tanaka";
declare author "Stephen Whitmarsch";
declare author "Francesco Di Maggio";
declare copyright "2022-2025 BBDMI TEAM";

declare licence "LGPLv3";

import("stdfaust.lib");
import("signals.lib");
import("maths.lib");
import("basics.lib");

//-------`bbdmi_simpleLinearEnv`----------
// Simple Linear Envelope as rand~ in Max
// Generates random values between -1 and 1 at a certain frequency
// and interpolates between them
// the input should be a noise generator
//
// #### Usage
//
// ```
// _ : bbdmi_simpleLinearEnv(f) : _
// ```
//
// Where:
//
// * `f`: the frequency of generation of interpolated random values between -1 and 1
//-----------------------------
bbdmi_simpleLinearEnv(f) = randVal with {
		randVal = ba.sAndH(th) : ba.line(ma.SR/f);
		ramp = os.phasor(1, f) : ma.decimal;
		th = (ramp > 0.001) * (ramp@1 <= 0.001);
};

//-------`bbdmi_scaledLinearEnv`----------
// Simple Linear Envelope as rand~ in Max but scaled
// Generates random values between -amp and amp at a certain frequency
// and interpolates between them
// the input should be a noise generator
//
// #### Usage
//
// ```
// _ : bbdmi_scaledLinearEnv(f, amp) : _
// ```
//
// Where:
//
// * `f`: the frequency of generation of interpolated random values between -1 and 1
// * 'amp' : the amplitude of the randomized generation (between 0 and 1)
//-----------------------------
bbdmi_scaledLinearEnv(f, amp) = bbdmi_simpleLinearEnv(f) * amp;


//-------`bbdmi_multipleScaledLinearEnv`----------
// Multiple Scaled Linear Envelopes
// Generates N parallel interpolated random values between -amp and amp at a certain frequency
// the input should be a multinoise generator (N decorrelated channels)
//
// #### Usage
//
// ```
// _ : bbdmi_multipleScaledLinearEnv(N, f, amp) : _
// ```
//
// Where:
//
// * `N`: the number of instances
// * `f`: the frequency of generation of interpolated random values between -1 and 1
// * 'amp' : the amplitude of the randomized generation (between 0 and 1)
//-----------------------------
bbdmi_multipleScaledLinearEnv(N, f, amp) = par(i, N, bbdmi_scaledLinearEnv(f, amp));

//-------`bbdmi_multiRand`----------
// Multiple Scaled Linear Envelopes with on/off function
// Generates N parallel interpolated random values between -amp and amp at a certain frequency
// the input should be a multinoise generator (N decorrelated channels)
//
// #### Usage
//
// ```
// _ : bbdmi_multiRand(N, f, amp, onoff) : _
// ```
//
// Where:
//
// * `N`: the number of instances
// * `f`: the frequency of generation of interpolated random values between -1 and 1
// * `amp` : the amplitude of the randomized generation (between 0 and 1)
// * `onoff` : switch on (1) or off (0)
//-----------------------------
bbdmi_multiRand(N, f, amp, onoff) = no.multinoise(N) : bbdmi_multipleScaledLinearEnv(N, f, amp * onoff);

//-------`bbdmi_multiRand_ui`----------
// Multiple Scaled Linear Envelopes with on/off function including the ui controls
// Generates N parallel interpolated random values between -amp and amp at a certain frequency
// the input should be a multinoise generator (N decorrelated channels)
//
// #### Usage
//
// ```
// _ : bbdmi_multiRand_ui(N) : _
// ```
//
// Where:
//
// * `N`: the number of instances
//-----------------------------
bbdmi_multiRand_ui(N) = bbdmi_multiRand(N, rate, amount, onoff) with {
    rate = nentry("v:rand/rate [unit:Hz]", 1, 0.0001, 1000, 0.0001);
    amount = nentry("v:rand/amount", 1, 0., 1., 0.0001) : si.smoo;
    onoff = nentry("v:rand/onoff", 1, 0, 1, 1) : si.smoo;
};

//-------`bbdmi_adsr_pulse`----------
// Generates a pulse with an ADSR output.
// Instead of providing an square 0 to 1 ouput
// this object will generate a controlled pulse
// taking as input parameters the attack, decay
// sustain and release.
//
// #### Usage
//
// ```
// bbdmi_adsr_pulse(a,d,r,ps) : _
// ```
//
// Where:
//
// * `a`: attack of the pulse in seconds
// * `d`: decay of the pulse in seconds
// * `r`: release of the pulse in seconds
// * `ps`: pulse spacing, how much time until the new pulse starts after
//         the last pulse has finished.
//-----------------------------

bbdmi_adsr_pulse(a,d,r,ps) = adsr_pulse with{
        env(t) = en.adsr(a,d,1,r,t);
        //Envelope duration:
        envD = ba.pulsen(ma.SR*(a+d+r),ma.SR*(ps+a+d+r));
        adsr_pulse = env(envD);
};

//-------`bbdmi_bw_pulse`----------
// Generates band limited pulse
// with an ADSR envelope.
//
// #### Usage
//
// ```
// bbdmi_bw_pulse(q,f,bw,a,d,r,ps) : _
// ```
//
// Where:
//
// * `q`: the order of the lowpass and highpass filters
// * `f`: the minimum frequency to be filtered
// * `bw`: the bandwidth to be filtered, starting from the minimum frequency
// * `a`: attack of the pulse in seconds
// * `d`: decay of the pulse in seconds
// * `r`: release of the pulse in seconds
// * `ps`: pulse spacing, how much time until the new pulse starts after
//         the last pulse has finished.
//-----------------------------

bbdmi_bw_pulse(a,d,r,ps,q,f,bw) = bbdmi_adsr_pulse(a,d,r,ps),(no.noise : fi.bandpass(q,f,f+bw)) : *;

//-------`bbdmi_bw_pulse_ui`----------
// Multiple bandwidth limited pulses
// see the "bbdmi_bw_pulse" function
//
// #### Usage
//
// ```
// bbdmi_bw_pulse_ui(N) : _
// ```
//
// Where:
//
// * `N`: the number of instances
//-----------------------------

bbdmi_bw_pulse_ui(N) = hgroup("Multi BW pulse", par(i,N,bbdmi_bw_pulse(a,d,r,ps,5,f,bw) with {
    a = vslider("h:bwPulse %2i/[0]attack%2i [unit:secs]", 0.3, 0, 20, 0.1);
    d = vslider("h:bwPulse %2i/[1]decay%2i [unit:secs]", 0.3, 0, 20, 0.1);
    r = vslider("h:bwPulse %2i/[2]release%2i [unit:secs]", 0.3, 0, 20, 0.1);
    ps = vslider("h:bwPulse %2i/[3]pulseSpacing%2i [unit:secs]", 0.3, 0, 20, 0.1);
    f = nentry("v:bwPulse %2i/[4]freq%2i [unit:Hz]", 1000, 5, 20000, 1);
    bw = nentry("v:bwPulse %2i/[5]bw%2i [unit:Hz]", 1000, 1, 20000, 1);
    //q = nentry("v:bwPulse %i/[6]filterorder", 2, 1, 50, 1);
    
}));

//-------`bbdmi_emg_band`----------
// EMG signal bandwidth spectrum
// generator
//
// #### Usage
//
// ```
// bbdmi_emg_band(g) : _
// ```
//
// Where:
//
// * `g`: gain of the synthesized spectrum
//-----------------------------

bbdmi_emg_band(g) =  emgBW * g with {
        emgBW = no.noise:fi.highpass(5,5):fi.lowpass(1,500):fi.lowpass(1,100):fi.lowpass(2,170):fi.lowpass(2,220);
};
//-------`bbdmi_emg_pulse`----------
// EMG signal bandwidth pulse generator
// 
//
// #### Usage
//
// ```
// bbdmi_emg_pulse(a,d,r,ps,g) : _
// ```
//
// Where:
//
// * `a`: attack of the pulse in seconds
// * `d`: decay of the pulse in seconds
// * `r`: release of the pulse in seconds
// * `ps`: pulse spacing, how much time until the new pulse starts after
//         the last pulse has finished.
// * `g`: gain of the synthesized spectrum
//-----------------------------

bbdmi_emg_pulse(a,d,r,ps,g) = bbdmi_adsr_pulse(a,d,r,ps),bbdmi_emg_band(g) : *;

//-------`bbdmi_emg_pulse_ui`----------
// Multiple emg bandwidth  pulses
// 
//
// #### Usage
//
// ```
// bbdmi_emg_pulse_ui(N) : _
// ```
//
// Where:
//
// * `N`: the number of instances
//-----------------------------

bbdmi_emg_pulse_ui(N) = hgroup("Multi EMG pulse", par(i,N,bbdmi_emg_pulse(a,d,r,ps,g) with {
    g = vslider("v:emgPulse %i/[0]gain [unit:secs] [style:knob]", 1, 0, 20, 0.01);
    a = vslider("h:emgPulse %i/[0]attack [unit:secs]", 0.3, 0, 20, 0.1);
    d = vslider("h:emgPulse %i/[1]decay [unit:secs]", 0.3, 0, 20, 0.1);
    r = vslider("h:emgPulse %i/[2]release [unit:secs]", 0.3, 0, 20, 0.1);
    ps = vslider("h:emgPulse %i/[3]pulseSpacing [unit:secs]", 0.3, 0, 20, 0.1);
}));


//-------`bbdmi_notch`----------
// A second order notch filter designed to filter
// 50 and 60 hz electrcical noises.
//
// #### Usage
//
// ```
// bbdmi_notch(f) : _
// ```
//
//-----------------------------

bbdmi_notch(f) =  fi.notchw(2, f);

//-------`bbdmi_multi_50notch(N)`----------
// Generates multiple instances of the bbdmi_notch filter centered at 50hz
//
// #### Usage
//
// ```
// bbdmi_multi_50notch(N) : _
// ```
//
// Where:
//
// * `N`: the number of instances
//-----------------------------

bbdmi_multi_50notch(N) =  par(i,N, _ : fi.notchw(3, 50): _);

//-------`bbdmi_electric_noise`----------
// An electric noise generator, made specifically for
// 50hz and 60hz noise.
//
// #### Usage
//
// ```
// bbdmi_electric_noise(f) : _
// ```
//
// Where:
//
// * `f`: noise fundamental frequency
//-----------------------------

bbdmi_electric_noise(f) = 0.95 * os.osc(f), par(i,5,(1/(25*(i+1))) * os.osc((2*(i+1)+1)*f)):+,+,+:+,_:+;


//-------`bbdmi_eeg_band`----------
// Generates an static band limited signal
// from 1hz to 500hz with a similar distribution 
// of EEG signals.
//
// #### Usage
//
// ```
// bbdmi_eeg_band : _
// ```
//
//-----------------------------

bbdmi_eeg_band =  no.noise : fi.lowpass(1,10) <: fi.lowpass(1,20),fi.lowpass(1,20) :> _;

//-------`bbdmi_eeg_alpha_band`----------
// Generates a limited band signal from 9hz to 11hz #(nothing to do with the 9-11 thing)
// with an small component (1/100) of a 20hz cenetered band
// simulating beta band power rising with alpha band
//
// #### Usage
//
// ```
// bbdmi_eeg_alpha_band : _
// ```
//
//-----------------------------

bbdmi_eeg_alpha_band =  no.noise <: fi.bandpass(4,9,11),(fi.bandpass(4,18,22)/10) :> _;


//-------`bbdmi_eeg_simulator`----------
// Simulates EEG signal with a controlled
// alpha band power. The control is a PWM model.
//
// #### Usage
//
// ```
// bbdmi_eeg_simulator(v,f,p) : _
// ```
//
// Where:
//
// * `g`: level of max alpha band
// * `f`: frequency to check if the signal appears or not.
//        it generates a random number at that frequency to be compared with
//        the probability vafiable.
// * `p`: propability of having an alpha burst
//-----------------------------

bbdmi_eeg_simulator(g,f,p) =  eeg * 5
with {
		ramp = os.phasor(1, f) : ma.decimal;
		th = (ramp > 0.001) * (ramp@1 <= 0.001);
        randVal = no.noise : ba.sAndH(th) : +(1) : /(2);
		env = si.smooth(ba.tau2pole(0.5));//0.5 seconds to rise and fall
        alphapower = (randVal , p : >) * 5 * g : env;
        alpha = bbdmi_eeg_alpha_band * alphapower;
        eeg = bbdmi_eeg_band + alpha;
};


//-------`bbdmi_eeg_simulator_ui`----------
// Multiple instances of bbdmi_eeg_simulator with a GUI
// 
//
// #### Usage
//
// ```
// bbdmi_eeg_simulator_ui(N) : _
// ```
//
// Where:
//
// * `N`: number of instances
//
//-----------------------------

bbdmi_eeg_simulator_ui(N) = hgroup("Multi EEG simulator", par(i,N,bbdmi_eeg_simulator(g,f,p) 
with {
    g = vslider("h:eegSimulator %i/[0]alphalevel ", 1, 0, 10, 0.01);
    f = vslider("h:eegSimulator %i/[1]frequency [unit:hz]", 1, 0, 20, 0.01);
    p = vslider("h:eegSimulator %i/[2]probability [unit:%]", 0.5, 0, 1, 0.01);
}));


//-------`granulator`----------
// Multiple instances of bbdmi_eeg_simulator with a GUI
// 
//
// #### Usage
//
// ```
// _ : granulator(gs,go, d, r, sp, m, modf,modmorph, tg,  g, fd, to,flpf,fhpf,index,indexdistr,N) : _
// ```
//
// Where:
//
// * EXPLANATION COMING SOON
//-----------------------------
//--------------------------------------------------------------------------------------//
// GRANULATOR ON DELAY LINE WITH GS GRAIN SIZE, D AS MAXIMUM DELAY, 
// the capacity of storage of the delay line is 10 seconds (10 * ma.SR)---------RECHECK POWER OF 2
//--------------------------------------------------------------------------------------//
bbdmi_granulator(grainenvmorph,gs,go, d, r, sp, m, modf,modmorph, modfreqmod, tg,  g, fd, to,flpf,fhpf,index,indexdistr,N) = granulate
	with {
            feedback = fd * 0.6;
            random1 = + (90000 +index) ~ *(1410065407);
            noise1 = random1/2147483647.0;
            random2 = + (90000 +index*20) ~ *(1410065407);
            noise2 = random2/2147483647.0;
            triggerdelgrain = ba.pulsen(1,(gs + sp)*(ma.SR / 1000) : int);
            grainsize = gs + go*r*(noise1 : +(1) : *(0.5) ): int : ba.sAndH(triggerdelgrain);
            finalspacing = 2*grainsize + 2*sp - sp*r*(noise1 : +(1) : *(0.5) ): ba.sAndH(triggerdelgrain);
            triggergrain = ba.pulsen((grainsize)*(ma.SR / 1000) : int,(finalspacing)*(ma.SR / 1000) : int);
            diractriggergrain = (triggergrain > 0.001) * (triggergrain' <= 0.001);
            delsample = (d - d*r*(noise2 : +(1) : *(0.5)))*(ma.SR / 1000) : int: ba.sAndH(diractriggergrain);
            getindfactor(ind) = (indexdistr == ind) * th(ind, index, N);
            indexfactor = sum(ind, 22, getindfactor(ind));
            modfreq = modf : ba.sAndH(diractriggergrain);
            envmodf = modfreq+indexfactor*modfreq,20000 : min;
            fromsintosquare = os.oscp(0) <: (_,0 : >),(_,0 : < : *(-1)) :> _;
            fromsintotriangle = +(ma.PI/2):os.oscp(0) : aa.arcsin : /(ma.PI):*(2);
            drywet4 = ((*(1-(modmorph,0:max))),(*(modmorph),1:min)),_,_: +,_,_ : ((*(1-(modmorph-1,0:max,1:min))),*((modmorph-1,0:max,1:min))) ,_ : +,_:((*(1-(modmorph-2,0:max,1:min))),(*((modmorph-2,0:max,1:min)))) :> _;
            modwaveshape =  _<: _,_ <: os.oscp(0),fromsintotriangle,(fromsintosquare <: _,_),os.oscp(0) : _,*,_,_: drywet4;
            envmodphase = diractriggergrain <: _,de.delay(10 * ma.SR,2*grainsize*(ma.SR / 1000)-1) : ba.on_and_off : empmorph;
            empmorph = _ <: (_ <: si.smooth(ba.tau2pole( _ * grainsize/2000.))), (ba.line((grainsize* _)*(ma.SR / 500) : int))^3: _*(1-modfreqmod) , _*modfreqmod :> _;
            envmod =  (0.7+indexfactor*0.3)*(1.-(m)*(m+(1-m)* envmodphase)*((envmodphase : *((envmodf + (0.7+indexfactor*0.3)*m*0.01*envmodf*envmodphase)*2*ma.PI) : modwaveshape)+1)/2);
            envlog = triggergrain <: si.smooth(ba.tau2pole((grainsize-0.5*grainsize*(1- _))/2000.));
            //add sustain time to each grain envelope?:
            envnormal = diractriggergrain <: _,de.delay(10 * ma.SR,2*grainsize*(ma.SR / 1000)-1) : ba.on_and_off <: ba.line((grainsize* _)*(ma.SR / 500) : int) : ma.E^(-1*((2*_-1)^2)/0.2);
            finalenv = (envlog*grainenvmorph,envnormal*(1-grainenvmorph)) :> _*envmod;
            transpout = ef.transpose(10000,10000,(to + to*indexfactor,48 : min));
            transpgrain = ef.transpose(10000,10000,(tg + tg*indexfactor,48 : min));
            filters = fi.lowpass(2,(flpf+flpf*indexfactor,20000 : min)) :fi.highpass(1,(fhpf+fhpf*indexfactor,20000 : min));
            fddelai = (+:de.sdelay(10 * ma.SR,1024,delsample+delsample*indexfactor))~(_*feedback);
            graincollector = +~(de.sdelay(10 * ma.SR,1024,delsample+delsample*indexfactor)*feedback : transpout);            
            granulate = filters : fddelai * finalenv * g : transpgrain : graincollector : fi.lowpass(2,ma.SR/4);
        };


//-------` multi_granulator`----------
// Multiple instances of bbdmi_eeg_simulator with a GUI
// 
//
// #### Usage
//
// ```
// _ : multi_granulator(N) : _
// ```
//
// Where:
//
// * `N`: number of instances
// * EXPLANATION COMING SOON
//-----------------------------
bbdmi_multi_granulator(N) = hgroup("Multi Granulator", par(index,N,bbdmi_granulator(grainenvmorph,grainsize,grainoffset,maxdelay,variability,spacing,modfactor,modfreq,modmorph,modfreqmod,transpgrain,gain,feedback,transpout,lpffreq,hpffreq,index,indexdistr,N)
    with {
        indexdistr = vslider("h:granulator/v:Grain/h:GrainEnvelope/[01]indexdistr ",0,0,21,1);
        grainenvmorph = vslider("h:granulator/v:Grain/h:GrainEnvelope/[02]grainenvmorph ",0.5,0,1,0.01) : si.smoo;
        grainsize = vslider("h:granulator/v:Grain/h:GrainEnvelope/[03]grainsize [unit:msec]",22,1,2000,1) : si.smoo;
        grainoffset = vslider("h:granulator/v:Grain/h:GrainEnvelope/[04]grainoffset [unit:msec]",0,0,1000,1) : si.smoo;
        spacing = vslider("h:granulator/v:Grain/h:GrainEnvelope/[05]spacing [unit:msec]",25,1,1000,1) : si.smoo;
        modfactor = vslider("h:granulator/v:Grain/h:GrainEnvelope/[06]modfactor",0.,0,1,0.001) : si.smoo;
        modfreq = vslider("h:granulator/v:Grain/h:GrainEnvelope/[07]modfreq[unit:hz]",4000,1,15000,1) : si.smoo;
        modmorph = vslider("h:granulator/v:Grain/h:GrainEnvelope/[08]modmorph ",0,0,3,0.001) : si.smoo;
        modfreqmod = vslider("h:granulator/v:Grain/h:GrainEnvelope/[09]modfreqmod ",0.5,0,1,0.001) : si.smoo;
        transpgrain = vslider("h:granulator/v:Grain/h:GrainEnvelope/[10]transpgrain[unit:]",0.,-48,48,0.01) : si.smoo;
        variability = vslider("h:granulator/v:Grain/h:GrainEnvelope/[11]variability [unit:%]",0.7,0,1,0.01) : si.smoo;
        maxdelay = hslider("h:granulator/v:Grain/[12]maxdelay [unit:msec]",2000,1,10000,1) : si.smoo;
        gain = vslider("h:granulator/h:loop/[1]gain",1,0,1,0.001) : si.smoo;
        feedback = vslider("h:granulator/h:loop/[2]feedback [unit:%]",0.5,0,1,0.01) : si.smoo;
        transpout = vslider("h:granulator/h:loop/[3]transpout[unit:]",0,-48,48,0.01) : si.smoo;
        lpffreq = vslider("h:granulator/h:loop/[3]lpffreq[unit:]",20000,20,20000,1) : si.smoo;
        hpffreq = vslider("h:granulator/h:loop/[3]hpffreq[unit:]",20,20,20000,1) : si.smoo;
    }));



//---------------- ATTENTION: Integrate the hoa2.lib to replace this code:
//TYPES OF DISTRIBUTIONS: 22 EASING FUNCTIONS FROM [0, 1] to [0,1]
//(i+1)/p belongs to [0, 1] and its image by any function in the list also belongs to the interval
    th(0, i, p) = (i+1) / p;
    th(1, i, p) = ((i+1) / p)^2;
    th(2, i, p) = sin(ma.PI * 0.5 * (i+1) / p);
    th(3, i, p) = log10(1 + (i+1) / p) / log10(2);
    th(4, i, p) = sqrt((i+1) / p);
    th(5, i, p) = 1 - cos(ma.PI * 0.5 * (i+1) / p);
    th(6, i, p) = (1 - cos(ma.PI * (i+1) / p)) * 0.5;
    th(7, i, p) = 1 - (1 - (i+1) / p )^2;
    th(8, i, p) = ((i+1) / p < 0.5) * 2 * ((i+1) / p)^2 + ((i+1) / p >= 0.5) * (1 - (-2 * (i+1) / p + 2)^2 * 0.5);
    th(9, i, p) = ((i+1) / p)^3;
    th(10, i, p) = 1 - (1 - (i+1) / p)^3;
    th(11, i, p) = ((i+1) / p < 0.5) * 4 * ((i+1) / p)^3 + ((i+1) / p >= 0.5) * (1 - (-2 * (i+1) / p + 2)^3 * 0.5);
    th(12, i, p) = ((i+1) / p)^4;
    th(13, i, p) = 1 - (1 - (i+1) / p)^4;
    th(14, i, p) = ((i+1) / p < 0.5) * 8 * ((i+1) / p)^4 + ((i+1) / p >= 0.5) * (1 - (-2 * (i+1) / p + 2)^4 * 0.5);
    th(15, i, p) = ((i+1) / p)^5;
    th(16, i, p) = 1 - (1 - (i+1) / p)^5;
    th(17, i, p) = ((i+1) / p < 0.5) * 16 * ((i+1) / p)^5 + ((i+1) / p >= 0.5) * (1 - (-2 * (i+1) / p + 2)^5 * 0.5);
    th(18, i, p) = 2^(10 * (i+1) / p - 10);
    th(19, i, p) = ((i+1) / p < 1) * (1 - 2^(-10 * (i+1) / p)) + ((i+1) / p == 1);
    th(20, i, p) = 1 - sqrt(1 - ((i+1) / p)^2);
    th(21, i, p) = sqrt(1 - ((i+1) / p - 1)^2);


//-------`bbdmi_calibrate`----------
// Normalizes a 0-1 signal removing the noise floor
//
// #### Usage
//
// ```
// _ : bbdmi_calibrate(bufferTime) : _
// ```
//
// Where:
//
// * `bufferTime`: Buffer time for the max and min values to get
//         close to the new values.
// * `onoff`: To save the actual values or keep calibrating
// 
//-----------------------------

bbdmi_calibrate(bt,onoff) = _ <: _,(_ <: _,_) :(maxvalue: ba.sAndH(onoff)), (minvalue : ba.sAndH(onoff): _ <: _,_),_ : _ , _, (-)*(-1) : -,_ : 1/(_)*_ : clipper
with{
maxvalue =  an.amp_follower(bt);
minvalue =  1- _ : an.amp_follower(bt) : 1 - _;
clipper = _, 0 : max : _, 1 : min;
};
/*
bbdmi_calibrate(bufferTime,onoff) = _ <: _,(_ <: _,_) :(maxvalue: ba.sAndH(onoff : ba.toggle : -(1)*(-1))), (minvalue : ba.sAndH(onoff : ba.toggle : -(1)*(-1)): _ <: _,_),_ : _ , _, (-)*(-1) : -,_ : 1/(_)*_ : clipper
with{
maxvalue =  an.amp_follower(bufferTime);
minvalue =  1- _ : an.amp_follower(bufferTime) : 1 - _;
clipper = _, 0 : max : _, 1 : min;
};*/

//-------`bbdmi_calibrate_ui`----------
// Normalizes a 0-1 signal removing the noise floor
//
// #### Usage
//
// ```
// _ : bbdmi_calibrate_ui(N) : _
// ```
//
// Where:
//
// * `N`: Number of instances
// 
//-----------------------------

bbdmi_calibrate_ui(N) = hgroup("Calibrate", par(i,N,bbdmi_calibrate(bufferTime,onoff) 
with{
bufferTime = hslider("h: bbdmi_calibrate %i/[0]bufferTime [unit:secs]", 40, 1, 1200, 1): si.smoo;
onoff = hslider("h: bbdmi_calibrate %i/[1]OnOff", 1,0,1,1);
}));

//-------`bbdmi_rms`----------
// Calculates the Root Mean Square on a configurable
// period.
//
// #### Usage
//
// ```
// _ : bbdmi_rms(p) : _
// ```
//
// Where:
//
// * `p`: Period to be analized
// 
//-----------------------------

bbdmi_rms(p) = an.rms_envelope_rect(p);

//-------`bbdmi_rms_ui`----------
// Calculates the Root Mean Square on a configurable
// period for multiple instances.
//
// #### Usage
//
// ```
// _ : bbdmi_rms_ui(N) : _
// ```
//
// Where:
//
// * `N`: Number of instances
// 
//-----------------------------

bbdmi_rms_ui(N) = hgroup("RMS", par(i,N,bbdmi_rms(period)
with{
period = vslider("h: bbdmi_rms %i/[0]period [unit:secs]", 0.5, 0.01, 10, 0.001);
}));


//-------`bbdmi_eulerIntegrator`----------
// EulerIntegrator starting from zero.
//
// #### Usage
//
// ```
// _ : bbdmi_eulerIntegrator(n) : _
// ```
//
// Where:
//
// * `n`: Period
// 
//-----------------------------

eulerIntegrator(n) = (_, *(ba.pulse(n)) : + : clipper) ~ (_) with {
    clipper = _, -1 : max : _, 1 : min;
};

//-------`bbdmi_joystick`----------
// Transfer function that works as a joystick
// if the value is higher than the middle point 
// plus the offset value the output signal starts
// augmenting. If the input value is inferior than
// the middle point minus the offset value the ouput
// signal starts reducing its value.
//
// #### Usage
//
// ```
// _ : bbdmi_joystick(restVal, restDelta, derivativeDepth) : _
// ```
//
// Where:
//
// * `restVal`: 
// * `restDelta`: 
// * `derivativeDepth`: 
// * `dt`: 
// 
//-----------------------------


bbdmi_joystick(restVal, restDelta, derivativeDepth,dt) = h(restVal, restDelta, derivativeDepth) : eulerIntegrator(dt) : +(1) : *(0.5);
//
//transfer function from the input between 0 and 1 to the derivative between -derivativeDepth and +derivativeDepth
//
h(rv, rd, dd, x) = (h1 + h2) * dd 
with {
    //we limit restVal-restDelta (that should remain > 0) and restVal+restDelta (that should remain < 1)
    //the threshold epsilon is set to 0.01
    epsilon = 0.01;
    bm1 = (rv - rd, epsilon) : max;
    bm2 = (rv + rd, 1 - epsilon) : min;
    //h1 is a linear transfer function to get the derivative when x between 0 and rv-rd (minimum -1, maximum 0)
    h1 = (x < bm1) * (x / bm1 - 1);
    //h2 is a linear transfer function to get the derivative when x between rv+rd and 1 (minimum 0, maximum 1)
    h2 = (x > bm2) * (x - bm2) / (1 - bm2);
};

//-------`bbdmi_joystick_ui`----------
// Multiple instances of the bbdmi_joystick object
// with GUI included
//
// #### Usage
//
// ```
// _ : bbdmi_joystick(N) : _
// ```
//
// Where:
//
// * `N`: Number of instances
// 
//-----------------------------

bbdmi_joystick_ui(N) = hgroup("Joystick", par(i,N,bbdmi_joystick(restVal, restDelta, derivativeDepth,dt)
with{
restVal = hslider("v: bbdmi_joystick %i/[0]restVal", 0.5, 0, 1, 0.01);
restDelta = hslider("v: bbdmi_joystick %i/[1]restDelta", 0.1, 0, 0.5, 0.01);
derivativeDepth = hslider("v: bbdmi_joystick %i/[2]derivativeDepth", 0.1, 0, 100, 0.01) : si.smoo;
dt = int(hslider("v: bbdmi_joystick %i/[3]dt [unit:msec]", 50, 1, 1000, 1) * ma.SR / 1000);
}));

//-------`bbdmi_joystick_loop`----------
// Joystick with a feedback approach :
// Transfer function that works as a joystick
// if the value is higher than the middle point 
// plus the offset value the output signal starts
// augmenting. If the input value is inferior than
// the middle point minus the offset value the ouput
// signal starts reducing its value.
// This object can also be used as integrator with a leaking option
// if the stablePoint is setted slightly over zero plus the threshold value.
// This way the output will increase as an integral and it's speed will depend
// on the input magnitude. When the input goes back to zero the output value will
// start going down again.
//
// #### Usage
//
// ```
// _ : bbdmi_joystick_loop(stablePoint, offset, smoothAmount) : _
// ```
//
// Where:
//
// * `stablePoint`:  Middle point
// * `offset`:       Offset from the middle point to start
//                   going up or down.
// * `smoothAmount`: The amount of smoothing applied
//                   to the ouput signal. The higher the value
//                   the slower the output signal will change.
// 
//-----------------------------

bbdmi_joystick_loop(stablePoint,offset,smoothAmount) = speed : /(smoothAmount*200000) : (+:_:limiter)~(_)
with{
    speed = _-(stablePoint) <: _,(_ : abs,offset : >) : *;
    limiter = _,1 : min :_,0 : max;
    };

//-------`bbdmi_joystick_loop_ui`----------
// Multiple instances of the bbdmi_joystick_loop object
// with GUI included
//
// #### Usage
//
// ```
// _ : bbdmi_joystick_loop_ui(N) : _
// ```
//
// Where:
//
// * `N`: Number of instances
// 
//-----------------------------


bbdmi_joystick_loop_ui(N) = hgroup("Joystick_loop", par(i,N,bbdmi_joystick_loop(stablePoint, offset, smoothAmount)
with{
offset = hslider("v: bbdmi_joystick_loop %i/[0]offset", 0.1, 0.01, 1, 0.01): si.smoo;
stablePoint = hslider("v: bbdmi_joystick_loop %i/[1]stablePoint", 0.5, 0, 1, 0.01): si.smoo;
smoothAmount = hslider("v: bbdmi_joystick_loop %i/[2]smoothAmount ", 0.5, 0.0001, 1, 0.0001): si.smoo;
}));

//-------`bbdmi_derivate`----------
// Calculates the first derivate of the input signal
//
// #### Usage
//
// ```
// _ : bbdmi_derivate(dt) : _
// ```
//
// Where:
//
// * `dt`:  Delta time to be used for the derivate calculation
// 
//-----------------------------

bbdmi_derivate(dt) = derivate
with{
    derivate = _ <: _,fx0 : - : /(dt);
    fx0 = de.delay(ma.SR, dt * ma.SR :int);
    };

//-------`bbdmi_derivate_ui`----------
// Multiple instances of the bbdmi_derivate module. The speed.
// with GUI included
//
// #### Usage
//
// ```
// _ : bbdmi_derivate_ui(N) : _
// ```
//
// Where:
//
// * `N`: Number of instances
// 
//-----------------------------


bbdmi_derivate_ui(N) = hgroup("bbdmi_derivate", par(i,N,bbdmi_derivate(dt)
with{
dt = hslider("v: bbdmi_derivate %i/[0]dt [unit:sec]", 0.2, 0.01, 10, 0.01): si.smoo;
}));



//-------`bbdmi_2ndDerivate`----------
// Calculates the second derivate of the input signal. The acceleration.
//
// #### Usage
//
// ```
// _ : bbdmi_2ndDerivate(dt) : _
// ```
//
// Where:
//
// * `dt`:  Delta time to be used for the derivate calculation
// 
//-----------------------------

bbdmi_2ndDerivate(dt) = derivate :derivate
with{
    derivate = _ <: _,fx0 : - : /(dt);
    fx0 = de.delay(ma.SR, 0.2 * ma.SR :int);
    };

//-------`bbdmi_2ndDerivate_ui`----------
// Multiple instances of the bbdmi_2ndDerivate module
// with GUI included
//
// #### Usage
//
// ```
// _ : bbdmi_2ndDerivate_ui(N) : _
// ```
//
// Where:
//
// * `N`: Number of instances
// 
//-----------------------------

bbdmi_2ndDerivate_ui(N) = hgroup("bbdmi_2ndDerivate", par(i,N,bbdmi_2ndDerivate(dt)
with{
dt = hslider("v: bbdmi_2ndDerivate %i/[0]dt [unit:sec]", 0.2, 0.01, 10, 0.01): si.smoo;
}));

//-------`bbdmi_differential`----------
// Transfer fonction to be applied to input control signals:
// The output value will change taking in account the first
// derivate of the input signal, which means it's speed. 
// The output signal will change in the amount of the rate of 
// change of the input signal. To go up, the input signal should 
// grow fast enough. To go down, the input signal should decrease
// fast enough. The threshold to determine if the output signal changes
// is controlled by the variable minChange.
// This object can also be used as integrator with a leaking option
// if the stablePoint is setted slightly over zero plus the threshold value.
// This way the output will increase as an integral and it's speed will depend
// on the input magnitude. When the input goes back to zero the output value will
// start going down again.
//
// #### Usage
//
// ```
// _ : bbdmi_differential(minChange) : _
// ```
//
// Where:
//
// * `minChange`: Minimum speed of the input signal in order to
//                change the output value.
// 
//-----------------------------

bbdmi_differential(minChange,smoother) = bbdmi_derivate(0.2) <: biggerOrSmaller,_ : * : /(smoother*100000):(+:_:clipper)~(_)
with{
    biggerOrSmaller = _ <: (_,minChange : > ), (_,(minChange * (-1)) : < ) : +;
    clipper = _,1 : min :_,0 : max;
    };


//-------`bbdmi_differential_ui`----------
// Multiple instances of the bbdmi_differential
// with a GUI.
//
// #### Usage
//
// ```
// _ : bbdmi_differential_ui(N) : _
// ```
//
// Where:
//
// * `N`: Number of instances.
// 
//-----------------------------

bbdmi_differential_ui(N) = hgroup("bbdmi_differential", par(i,N,bbdmi_differential(minChange,smoother)
with{
minChange = hslider("v: bbdmi_differential %i/[0]minChange", 0.5, 0, 5, 0.01): si.smoo;
smoother = hslider("v: bbdmi_differential %i/[1]smoother", 0.5, 0.01, 5, 0.01): si.smoo;
}));


//-------`bbdmi_averager`----------
// Works like a leakyIntegrator with a time variable.
// The period variable will define how long the output will 
// take to be equal to the input.
//
// #### Usage
//
// ```
// _ : bbdmi_averager(period) : _
// ```
//
// Where:
//
// * `period`: Time to get to the same value as the input signal.
// 
//-----------------------------

bbdmi_averager(period) = fi.avg_tau(period);


//-------`bbdmi_averager_ui`----------
// Multiple instances of the bbdmi_averager
// with a GUI.
//
// #### Usage
//
// ```
// _ : bbdmi_averager_ui(N) : _
// ```
//
// Where:
//
// * `N`: Number of instances.
// 
//-----------------------------

bbdmi_averager_ui(N) = hgroup("Averager", par(i,N,fi.avg_tau(period)
with{
period = hslider("v: bbdmi_averager %i/[0]period", 1, 0, 10, 0.01): si.smoo;
}));


//-------`bbdmi_clickUpCounter`----------
// This module will add a number to it's output every time the input signal
// has a sudden change growing. The speed of the signal is the
// control parameter. The minimum change speed can be setted by the minChange variable.
//
// #### Usage
//
// ```
// _ : bbdmi_clickUpCounter(minChange) : _
// ```
//
// Where:
//
// * `minChange`: Minimum change on speed of the input signal in order to start counting.
// 
//-----------------------------

bbdmi_clickUpCounter(minChange) = bbdmi_derivate(0.2) : biggerOrSmaller :  ba.counter 
with{
    biggerOrSmaller = _,minChange : >;
    };


//-------`bbdmi_clickUpCounter_ui`----------
// Multiple instances of the bbdmi_clickUpCounter
// with a GUI.
//
// #### Usage
//
// ```
// _ : bbdmi_clickUpCounter_ui(N) : _
// ```
//
// Where:
//
// * `N`: Number of instances.
// 
//-----------------------------

bbdmi_clickUpCounter_ui(N) = hgroup("bbdmi_clickUpCounter", par(i,N,bbdmi_clickUpCounter(minChange)
with{
minChange = hslider("v: bbdmi_clickUpCounter %i/[0]minChange", 0.5, 0, 5, 0.01): si.smoo;
}));


//-------`bbdmi_clickDownCounter`----------
// This module will add a number to it's output every time the input signal
// has a sudden change decreasing. The speed of the signal is the
// control parameter. The minimum change speed can be setted by the minChange variable.
//
// #### Usage
//
// ```
// _ : bbdmi_clickDownCounter(minChange) : _
// ```
//
// Where:
//
// * `minChange`: Minimum change on speed of the input signal in order to start counting.
// 
//-----------------------------

bbdmi_clickDownCounter(minChange) = bbdmi_derivate(0.2) : biggerOrSmaller :  ba.counter 
with{
    biggerOrSmaller = _,(minChange * (-1)) : <;
    };


//-------`bbdmi_clickDownCounter_ui`----------
// Multiple instances of the bbdmi_clickDownCounter
// with a GUI.
//
// #### Usage
//
// ```
// _ : bbdmi_clickDownCounter_ui(N) : _
// ```
//
// Where:
//
// * `N`: Number of instances.
// 
//-----------------------------

bbdmi_clickDownCounter_ui(N) = hgroup("bbdmi_clickDownCounter", par(i,N,bbdmi_clickDownCounter(minChange)
with{
minChange = hslider("v: bbdmi_clickDownCounter %i/[0]minChange", 0.5, 0, 5, 0.01): si.smoo;
}));


//-------`bbdmi_clickUpDownCounter`----------
// This module will add a number to it's output every time the input signal
// has a sudden change growing or decresing. The speed of the signal is the
// control parameter. The minimum change speed can be setted by the minChange variable.
//
// #### Usage
//
// ```
// _ : bbdmi_clickUpDownCounter(minChange) : _
// ```
//
// Where:
//
// * `minChange`: Minimum change on speed of the input signal in order to start counting.
// 
//-----------------------------

bbdmi_clickUpDownCounter(minChange) = bbdmi_derivate(0.2) : biggerOrSmaller :  ba.counter 
with{
    biggerOrSmaller = _ <: (_,minChange : > ), (_,(minChange * (-1)) : < ) : +;
    };


//-------`bbdmi_clickUpDownCounter_ui`----------
// Multiple instances of the bbdmi_clickUpDownCounter
// with a GUI.
//
// #### Usage
//
// ```
// _ : bbdmi_clickUpDownCounter_ui(N) : _
// ```
//
// Where:
//
// * `N`: Number of instances.
// 
//-----------------------------

bbdmi_clickUpDownCounter_ui(N) = hgroup("bbdmi_clickUpDownCounter", par(i,N,bbdmi_clickUpDownCounter(minChange)
with{
minChange = hslider("v: bbdmi_clickUpDownCounter %i/[0]minChange", 0.5, 0, 5, 0.01): si.smoo;
}));


//-------`bbdmi_clickCTL`----------
// Will count up if there is a sudden increase of the input signal. 
// Will count down if there is a sudden decrease of the input signal.
//
// #### Usage
//
// ```
// _ : bbdmi_clickCTL(minChange) : _
// ```
//
// Where:
//
// * `minChange`: Minimum change on speed of the input signal in order to start counting.
// 
//-----------------------------

bbdmi_clickCTL(minChange) = _ <: bbdmi_clickUpCounter(minChange),bbdmi_clickDownCounter(minChange) : -
with{
    biggerOrSmaller = _ <: (_,minChange : > ), (_,(minChange * (-1)) : < ) : +;
    };


//-------`bbdmi_clickCTL_ui`----------
// Multiple instances of the bbdmi_clickCTL
// with a GUI.
//
// #### Usage
//
// ```
// _ : bbdmi_clickCTL_ui(N) : _
// ```
//
// Where:
//
// * `N`: Number of instances.
// 
//-----------------------------

bbdmi_clickCTL_ui(N) = hgroup("bbdmi_clickCTL", par(i,N,bbdmi_clickCTL(minChange)
with{
minChange = hslider("v: bbdmi_clickCTL %i/[0]minChange", 0.5, 0, 5, 0.01): si.smoo;
}));


//-------`bbdmi_clickTimedCTL`----------
// Will count the amount of rising speed changes in the period amount of time.
// The counter will reset to zero after the 'period' time is passed.
//
// #### Usage
//
// ```
// _ : bbdmi_clickTimedCTL(minChange,period) : _
// ```
//
// Where:
//
// * `minChange`: Minimum change on rising speed of the input signal in order to start counting.
// * `period`: Time to record the input clicks and to reset the counter.
//
//-----------------------------

bbdmi_clickTimedCTL(minChange,period) = bbdmi_clickUpCounter(minChange) <:_,(de.delay(10 * ma.SR,period*ma.SR)) : (_<:(_,_)),_ : (_<:(_,_')),- : !=,_ :_,(_<:((_',0:==),_)) : +,_:ba.sAndH;


//-------`bbdmi_clickTimedCTL_ui`----------
// Multiple instances of the bbdmi_clickTimedCTL
// with a GUI.
//
// #### Usage
//
// ```
// _ : bbdmi_clickTimedCTL_ui(N) : _
// ```
//
// Where:
//
// * `N`: Number of instances.
// 
//-----------------------------

bbdmi_clickTimedCTL_ui(N) = hgroup("bbdmi_clickTimedCTL", par(i,N,bbdmi_clickTimedCTL(minChange,period)
with{
minChange = hslider("v: bbdmi_clickTimedCTL %i/[0]minChange", 0.5, 0, 5, 0.01): si.smoo;
period = hslider("v: bbdmi_clickTimedCTL %i/[1]period[unit:secs]", 3, 1, 10, 1);
}));



//-------`bbdmi_eventTimer`----------
// Counts the amount of time since the input signal has changed in seconds.
//
// #### Usage
//
// ```
// _ : bbdmi_eventTimer : _
// ```
//
//
//-----------------------------

bbdmi_eventTimer = (_:change),((ba.time/ma.SR) <:_,_):ba.sAndH,_:ro.cross(2):-
with{
    change = _<: _,_':!=;
    };

//-------`bbdmi_multipleEventTimer`----------
// Multiple instances of the bbdmi_eventTimer
// with a GUI.
//
// #### Usage
//
// ```
// _ : bbdmi_multipleEventTimer(N) : _
// ```
//
// Where:
//
// * `N`: Number of instances.
// 
//-----------------------------

bbdmi_multipleEventTimer(N) = hgroup("bbdmi_eventTimer", par(i,N,bbdmi_eventTimer));


//-------`bbdmi_timedSelect`----------
//To enter setup mode is necessary to do the 'setupModeNumber' of burst and stay pushing (more than 70% of the maximal effort) for 'timeWindow' time.
//After entering the setup mode the user will have the duble of the 'timeWindow' to select the output to route the sound to
//
// #### Usage
//
// ```
// _ : bbdmi_timedSelect(setupModeNumber,timeWindow,minChange) : _
// ```
//
// Where:
//
// * `setupModeNumber`: The number to get to and wait the 'timeWindow' time to enter Setup mode.
// * `timeWindow`: Time from the first burst to select the Setup mode and then the same ammount of time to choose the output value.
// * `minChange`: Minimum change on speed of the input signal in order to start counting.
// 
//-----------------------------

bbdmi_timedSelect(setupModeNumber,timeWindow,minChange) = _ <: (bbdmi_clickTimedCTL(minChange,timeWindow ) <: _,bbdmi_eventTimer),(_ <:_,_'):(_,setupModeNumber:==),(_,(timeWindow-0.01): >),(_,0.7 : >),_ : *,_,_:*,_:(bbdmi_eventTimer <: (_,_)),_ : _,(_,timeWindow : <),_:_,*:_,bbdmi_clickTimedCTL(minChange,timeWindow):(_,1*timeWindow : > : upChange),_-(1):ba.sAndH : clipper
with{
    upChange = _<: (_ <: _,_' : >),(_ <: _,_':!=) : *;
    clipper = _, 0 : max;//Using this because the output does not passes throw 0, goes from -1 to 1, 2,3 etc
    };

//-------`bbdmi_timedSelect_ui`----------
// Multiple instances of the bbdmi_timedSelect
// with a GUI.
//
// #### Usage
//
// ```
// _ : bbdmi_timedSelect_ui(N) : _
// ```
//
// Where:
//
// * `N`: Number of instances.
// 
//-----------------------------

bbdmi_timedSelect_ui(N) = hgroup("bbdmi_timedSelect", par(i,N,bbdmi_timedSelect(setupModeNumber,timeWindow,minChange)
with{
minChange = hslider("v: bbdmi_timedSelect_ui %i/[0]minChange", 0.5, 0, 5, 0.01): si.smoo;
setupModeNumber = hslider("v: bbdmi_timedSelect_ui %i/[1]setupModeNumber", 3, 1, 10, 1);
timeWindow = hslider("v: bbdmi_timedSelect_ui %i/[2]timeWindow[unit:secs]", 3, 1, 10, 1);
}));



//-------`bbdmi_3multiController`----------
//To enter setup mode is necessary to do the 'setupModeNumber' of burst and stay pushing (more than 70% of the maximal effort) for 'timeWindow' time.
//After entering the setup mode the user will have the duble of the 'timeWindow' to select the output to route the sound to
//
// #### Usage
//
// ```
// _ : bbdmi_3multiController : _
// ```
//
//
//-----------------------------

bbdmi_3multiController = _ <: si.bus(4):(bbdmi_timedSelect_ui(1):_, 0 : max : _, 2 : min),_,bbdmi_joystick_loop_ui(1),bbdmi_differential_ui(1) : (_ <: si.bus(3)),_,_,_ :((_,0:==),(_,1:==),(_,2:==)),_,_,_ : p : *,*,* :> _
with{
    p(a,b,c,d,e,f) = a,d,b,e,c,f;
};

//-------`bbdmi_3multiController_ui`----------
// Multiple instances of the bbdmi_3multiController
// with a GUI.
//
// #### Usage
//
// ```
// _ : bbdmi_3multiController_ui(N) : _
// ```
//
// Where:
//
// * `N`: Number of instances.
// 
//-----------------------------

bbdmi_3multiController_ui(N) = hgroup("bbdmi_3multiController", par(i,N,bbdmi_3multiController));



process = par(i, channels, emg : bbdmi_rms_ui(1):bbdmi_calibrate_ui(1)): bbdmi_3multiController_ui(channels);
