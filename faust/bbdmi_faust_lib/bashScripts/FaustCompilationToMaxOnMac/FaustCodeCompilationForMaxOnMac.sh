#!/bin/bash
#BBDMI FAUST CODE COMPILATION FOR MAX ON MAC
#creates a provisional bbdmiCompilationInProgress folder on the desktop
cd $HOME/Desktop
rm -R bbdmiCompilationInProgress
mkdir bbdmiCompilationInProgress
cd $HOME/Documents/Gitlab/bbdmi/faust/bbdmi_faust_lib/faustCodes
destpath="$HOME/Desktop/bbdmiCompilationInProgress"
#copies all .dsp and .lib files to the provisional bbdmiCompilationInProgress folder on the Desktop
for f in `ls`
do
    if [ -d "$f" ]
        then
        if [ $f != "library" ]
            then
            echo "------------------------------------"
            echo "faust code directory : $f"
            cd $f
            ls
            cp *.dsp "$destpath"
            cd ../
        fi
        if [ $f == "library" ]
            then
            echo "------------------------------------"
            echo "bbdmi library directory : $f"
            cd $f
            ls
            cp *.lib "$destpath"
            cd ../
        fi
    fi
done
#Compilation of all dsp files to Max target
#Deletes the original .dsp files in the provisional bbdmiCompilationInProgress folder on the Desktop
cd $HOME/Desktop/bbdmiCompilationInProgress
for f in `ls`
do
    echo "------------------------------------"
    extension=$(echo "$f" | cut -d'.' -f2)
    #echo $extension
    if [ $extension == "dsp" ]
        then
         echo "compiling file : $f"
         faust2max6 -universal $f
         rm -R $f
    fi
done
#Copies the .mxo files to BBDMI externals folder and .maxpat files to BBDM test_patches folder
destPathExternals = "$HOME/Documents/Gitlab/bbdmi/faust/bbdmi_faust_lib/externals"
destPathMaxpat = "$HOME/Documents/Gitlab/bbdmi/faust/bbdmi_faust_lib/test_patches"
#cp *.mxo "$destPathExternals"
#cp *.maxpat "$destPathMaxpat"
#cp ui.js "$destPathMaxpat"
#Deletes the provisional bbdmiCompilationInProgress folder on the Desktop
cd $HOME/Desktop
#rm -R bbdmiCompilationInProgress