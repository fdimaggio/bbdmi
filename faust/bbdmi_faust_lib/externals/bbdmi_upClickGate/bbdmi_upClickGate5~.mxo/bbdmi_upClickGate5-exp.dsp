declare version "2.72.14";
declare compile_options "-double -scal -e ./bbdmi_upClickGate5.dsp -o faust.8Lz/bbdmi_upClickGate5/bbdmi_upClickGate5-exp.dsp";
declare library_path0 "/Users/davidfierro/Desktop/bbdmiCompilationInProgress/bbdmi.lib";
declare library_path1 "/Applications/Faust-2.72.14/share/faust/basics.lib";
declare library_path2 "/Applications/Faust-2.72.14/share/faust/maths.lib";
declare library_path3 "/Applications/Faust-2.72.14/share/faust/signals.lib";
declare library_path4 "/Applications/Faust-2.72.14/share/faust/stdfaust.lib";
declare library_path5 "/Applications/Faust-2.72.14/share/faust/platform.lib";
declare library_path6 "/Applications/Faust-2.72.14/share/faust/delays.lib";
declare library_path7 "/Applications/Faust-2.72.14/share/faust/routes.lib";
declare author "Alain Bonardi";
declare contributor "David Fierro";
declare contributor "Anne Sedes";
declare contributor "Atau Tanaka";
declare contributor "Stephen Whitmarsch";
declare contributor "Francesco Di Maggio";
declare basics_lib_counter_author "Stephane Letz";
declare basics_lib_name "Faust Basic Element Library";
declare basics_lib_sAndH_author "Romain Michon";
declare basics_lib_tabulateNd "Copyright (C) 2023 Bart Brouns <bart@magnetophon.nl>";
declare basics_lib_version "1.15.0";
declare bbdmi_lib_author "Alain Bonardi";
declare bbdmi_lib_copyright "2022-2025 BBDMI TEAM";
declare bbdmi_lib_licence "LGPLv3";
declare bbdmi_lib_name "BBDMI Faust Lib";
declare copyright "2022-2025 BBDMI TEAM";
declare delays_lib_name "Faust Delay Library";
declare delays_lib_version "1.1.0";
declare filename "bbdmi_upClickGate5.dsp";
declare maths_lib_author "GRAME";
declare maths_lib_copyright "GRAME";
declare maths_lib_license "LGPL with exception";
declare maths_lib_name "Faust Math Library";
declare maths_lib_version "2.8.0";
declare name "BBDMI Faust Lib";
declare platform_lib_name "Generic Platform Library";
declare platform_lib_version "1.3.0";
declare routes_lib_name "Faust Signal Routing Library";
declare routes_lib_version "1.2.0";
declare signals_lib_name "Faust Signal Routing Library";
declare signals_lib_version "1.5.0";
ID_0 = _, _;
ID_1 = _, ID_0;
ID_2 = _, ID_1;
ID_3 = _, ID_2;
ID_4 = _, ID_3;
ID_5 = _, \(x7).(x7,((1.92e+05,(1.0,fconstant(int fSamplingFreq, <math.h>) : max) : min),(0,(0.2,(1.92e+05,(1.0,fconstant(int fSamplingFreq, <math.h>) : max) : min) : * : int) : max) : min) : @);
ID_6 = _, 0.2;
ID_7 = ID_6 : /;
ID_8 = - : ID_7;
ID_9 = ID_5 : ID_8;
ID_10 = _ <: ID_9;
ID_11 = hslider("v: bbdmi_timedSelect_ui 0/[0]minChange", 0.5, 0.0, 5.0, 0.01);
ID_12 = (ID_11 : \(x8).(\(x9).(((1.0,(1,(44.1,(1.92e+05,(1.0,fconstant(int fSamplingFreq, <math.h>) : max) : min) : /) : -) : -),x8 : *),((1,(44.1,(1.92e+05,(1.0,fconstant(int fSamplingFreq, <math.h>) : max) : min) : /) : -),x9 : *) : +)~_));
ID_13 = _, ID_12;
ID_14 = ID_13 : >;
ID_15 = ID_14 : \(x10).(x10,(x10 : mem) : > : +~_);
ID_16 = ID_10 : ID_15;
ID_17 = _, \(x11).(x11,((10,(1.92e+05,(1.0,fconstant(int fSamplingFreq, <math.h>) : max) : min) : *),(0,(hslider("v: bbdmi_timedSelect_ui 0/[2]timeWindow[unit:secs]", 3.0, 1.0, 1e+01, 1.0),(1.92e+05,(1.0,fconstant(int fSamplingFreq, <math.h>) : max) : min) : *) : max) : min) : @);
ID_18 = (_ <: ID_0);
ID_19 = ID_18, _;
ID_20 = (_ : mem);
ID_21 = _, ID_20;
ID_22 = (_ <: ID_21);
ID_23 = ID_22, -;
ID_24 = !=, _;
ID_25 = ID_20, 0;
ID_26 = (ID_25 : ==);
ID_27 = ID_26, _;
ID_28 = (_ <: ID_27);
ID_29 = _, ID_28;
ID_30 = +, _;
ID_31 = ID_30 : \(x12).((x12,_,_ : select2)~_);
ID_32 = ID_29 : ID_31;
ID_33 = ID_24 : ID_32;
ID_34 = ID_23 : ID_33;
ID_35 = ID_19 : ID_34;
ID_36 = ID_17 : ID_35;
ID_37 = ID_16 <: ID_36;
ID_38 = ID_21 : !=;
ID_39 = _ <: ID_38;
ID_40 = (_ : ID_39);
ID_41 = _, 1;
ID_42 = (ID_41 : +);
ID_43 = ID_42 ~ _;
ID_44 = ID_43, 1;
ID_45 = (ID_44 : -);
ID_46 = fconstant(int fSamplingFreq, <math.h>);
ID_47 = 1.0, ID_46;
ID_48 = (ID_47 : max);
ID_49 = 1.92e+05, ID_48;
ID_50 = (ID_49 : min);
ID_51 = ID_45, ID_50;
ID_52 = ID_51 : /;
ID_53 = (ID_52 <: ID_0);
ID_54 = ID_40, ID_53;
ID_55 = \(x12).((x12,_,_ : select2)~_), _;
ID_56 = 2, 1;
ID_57 = 2, ID_56;
ID_58 = 1, ID_57;
ID_59 = route(2,2,ID_58);
ID_60 = ID_59 : -;
ID_61 = ID_55 : ID_60;
ID_62 = (ID_54 : ID_61);
ID_63 = _, ID_62;
ID_64 = (ID_37 <: ID_63);
ID_65 = ID_64, ID_22;
ID_66 = hslider("v: bbdmi_timedSelect_ui 0/[1]setupModeNumber", 3.0, 1.0, 1e+01, 1.0);
ID_67 = _, ID_66;
ID_68 = (ID_67 : ==);
ID_69 = hslider("v: bbdmi_timedSelect_ui 0/[2]timeWindow[unit:secs]", 3.0, 1.0, 1e+01, 1.0);
ID_70 = ID_69, 0.01;
ID_71 = (ID_70 : -);
ID_72 = _, ID_71;
ID_73 = (ID_72 : >);
ID_74 = _, 0.7;
ID_75 = (ID_74 : >);
ID_76 = ID_75, _;
ID_77 = ID_73, ID_76;
ID_78 = ID_68, ID_77;
ID_79 = *, ID_0;
ID_80 = *, _;
ID_81 = (ID_62 <: ID_0);
ID_82 = ID_81, _;
ID_83 = _, ID_69;
ID_84 = (ID_83 : <);
ID_85 = ID_84, _;
ID_86 = _, ID_85;
ID_87 = _, *;
ID_88 = _, ID_37;
ID_89 = 1, ID_69;
ID_90 = (ID_89 : *);
ID_91 = _, ID_90;
ID_92 = ID_21 : >;
ID_93 = (_ <: ID_92);
ID_94 = ID_93, ID_39;
ID_95 = ID_94 : *;
ID_96 = _ <: ID_95;
ID_97 = > : ID_96;
ID_98 = (ID_91 : ID_97);
ID_99 = (ID_41 : -);
ID_100 = ID_98, ID_99;
ID_101 = _, 0;
ID_102 = ID_101 : max;
ID_103 = \(x12).((x12,_,_ : select2)~_) : ID_102;
ID_104 = ID_100 : ID_103;
ID_105 = ID_88 : ID_104;
ID_106 = ID_87 : ID_105;
ID_107 = ID_86 : ID_106;
ID_108 = ID_82 : ID_107;
ID_109 = ID_80 : ID_108;
ID_110 = ID_79 : ID_109;
ID_111 = ID_78 : ID_110;
ID_112 = ID_65 : ID_111;
ID_113 = _ <: ID_112;
ID_114 = hgroup("bbdmi_timedSelect", ID_113);
ID_115 = (ID_114 <: ID_3);
ID_116 = ID_115, ID_3;
ID_117 = (ID_101 : ==);
ID_118 = (ID_41 : ==);
ID_119 = _, 2;
ID_120 = (ID_119 : ==);
ID_121 = _, 3;
ID_122 = (ID_121 : ==);
ID_123 = _, 4;
ID_124 = (ID_123 : ==);
ID_125 = ID_122, ID_124;
ID_126 = ID_120, ID_125;
ID_127 = ID_118, ID_126;
ID_128 = ID_117, ID_127;
ID_129 = ID_128, ID_3;
ID_130 = 10, 10;
ID_131 = 8, ID_130;
ID_132 = 9, ID_131;
ID_133 = 6, ID_132;
ID_134 = 8, ID_133;
ID_135 = 4, ID_134;
ID_136 = 7, ID_135;
ID_137 = 2, ID_136;
ID_138 = 6, ID_137;
ID_139 = 9, ID_138;
ID_140 = 5, ID_139;
ID_141 = 7, ID_140;
ID_142 = 4, ID_141;
ID_143 = 5, ID_142;
ID_144 = 3, ID_143;
ID_145 = 3, ID_144;
ID_146 = 2, ID_145;
ID_147 = 1, ID_146;
ID_148 = 1, ID_147;
ID_149 = route(10,10,ID_148);
ID_150 = \(x12).((x12,_,_ : select2)~_), \(x12).((x12,_,_ : select2)~_);
ID_151 = \(x12).((x12,_,_ : select2)~_), ID_150;
ID_152 = \(x12).((x12,_,_ : select2)~_), ID_151;
ID_153 = \(x12).((x12,_,_ : select2)~_), ID_152;
ID_154 = ID_149 : ID_153;
ID_155 = ID_129 : ID_154;
ID_156 = ID_116 : ID_155;
ID_157 = ID_4 : ID_156;
ID_158 = _ <: ID_157;
process = ID_158;
