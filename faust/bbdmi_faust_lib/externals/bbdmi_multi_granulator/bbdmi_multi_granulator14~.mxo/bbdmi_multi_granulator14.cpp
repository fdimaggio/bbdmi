/* ------------------------------------------------------------
author: "Alain Bonardi", "Francesco Di Maggio", "David Fierro", "Anne Sedes", "Atau Tanaka", "Stephen Whitmarsch"
copyright: "2022-2025 BBDMI TEAM"
name: "BBDMI Faust Lib", "bbdmi_multi_granulator14"
Code generated with Faust 2.72.14 (https://faust.grame.fr)
Compilation options: -a /Applications/Faust-2.72.14/share/faust/max-msp/max-msp64.cpp -lang cpp -i -ct 1 -cn bbdmi_multi_granulator14 -es 1 -mcd 16 -mdd 1024 -mdy 33 -uim -double -ftz 0
------------------------------------------------------------ */

#ifndef  __bbdmi_multi_granulator14_H__
#define  __bbdmi_multi_granulator14_H__

/************************************************************************
 
 IMPORTANT NOTE : this file contains two clearly delimited sections :
 the ARCHITECTURE section (in two parts) and the USER section. Each section
 is governed by its own copyright and license. Please check individually
 each section for license and copyright information.
 *************************************************************************/

/******************* BEGIN max-msp64.cpp ****************/
/************************************************************************
 FAUST Architecture File
 Copyright (C) 2004-2020 GRAME, Centre National de Creation Musicale
 ---------------------------------------------------------------------
 This Architecture section is free software; you can redistribute it
 and/or modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either version 3
 of the License, or (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public License
 along with this program; If not, see <http://www.gnu.org/licenses/>.
 
 EXCEPTION : As a special exception, you may create a larger work
 that contains this FAUST architecture section and distribute
 that work under terms of your choice, so long as this FAUST
 architecture section is not modified.
 
 MAX MSP SDK : in order to compile a MaxMSP external with this
 architecture file you will need the official MaxMSP SDK from
 cycling'74. Please check the corresponding license.
 
 ************************************************************************
 ************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <limits.h>
#include <errno.h>
#include <time.h>
#include <unistd.h>
#include <fcntl.h>
#include <assert.h>
#include <string>
#include <vector>
#include <map>
#include <iostream>
#include <fstream>
#include <sstream>

#ifdef __APPLE__
#include <Carbon/Carbon.h>
#include <unistd.h>
#endif

#ifdef WIN32
#ifndef NAN
    static const unsigned long __nan[2] = {0xffffffff, 0x7fffffff};
    #define NAN (*(const float *) __nan)
#endif
#endif

// FAUSTFLOAT is setup by faust2max6

#ifndef DOWN_SAMPLING
#define DOWN_SAMPLING 0
#endif
#ifndef UP_SAMPLING
#define UP_SAMPLING 0
#endif
#ifndef FILTER_TYPE
#define FILTER_TYPE 0
#endif

/************************** BEGIN UI.h *****************************
 FAUST Architecture File
 Copyright (C) 2003-2022 GRAME, Centre National de Creation Musicale
 ---------------------------------------------------------------------
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation; either version 2.1 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 
 EXCEPTION : As a special exception, you may create a larger work
 that contains this FAUST architecture section and distribute
 that work under terms of your choice, so long as this FAUST
 architecture section is not modified.
 ********************************************************************/

#ifndef __UI_H__
#define __UI_H__

/************************************************************************
 FAUST Architecture File
 Copyright (C) 2003-2022 GRAME, Centre National de Creation Musicale
 ---------------------------------------------------------------------
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation; either version 2.1 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 
 EXCEPTION : As a special exception, you may create a larger work
 that contains this FAUST architecture section and distribute
 that work under terms of your choice, so long as this FAUST
 architecture section is not modified.
 ***************************************************************************/

#ifndef __export__
#define __export__

// Version as a global string
#define FAUSTVERSION "2.72.14"

// Version as separated [major,minor,patch] values
#define FAUSTMAJORVERSION 2
#define FAUSTMINORVERSION 72
#define FAUSTPATCHVERSION 14

// Use FAUST_API for code that is part of the external API but is also compiled in faust and libfaust
// Use LIBFAUST_API for code that is compiled in faust and libfaust

#ifdef _WIN32
    #pragma warning (disable: 4251)
    #ifdef FAUST_EXE
        #define FAUST_API
        #define LIBFAUST_API
    #elif FAUST_LIB
        #define FAUST_API __declspec(dllexport)
        #define LIBFAUST_API __declspec(dllexport)
    #else
        #define FAUST_API
        #define LIBFAUST_API 
    #endif
#else
    #ifdef FAUST_EXE
        #define FAUST_API
        #define LIBFAUST_API
    #else
        #define FAUST_API __attribute__((visibility("default")))
        #define LIBFAUST_API __attribute__((visibility("default")))
    #endif
#endif

#endif

#ifndef FAUSTFLOAT
#define FAUSTFLOAT float
#endif

/*******************************************************************************
 * UI : Faust DSP User Interface
 * User Interface as expected by the buildUserInterface() method of a DSP.
 * This abstract class contains only the method that the Faust compiler can
 * generate to describe a DSP user interface.
 ******************************************************************************/

struct Soundfile;

template <typename REAL>
struct FAUST_API UIReal {
    
    UIReal() {}
    virtual ~UIReal() {}
    
    // -- widget's layouts
    
    virtual void openTabBox(const char* label) = 0;
    virtual void openHorizontalBox(const char* label) = 0;
    virtual void openVerticalBox(const char* label) = 0;
    virtual void closeBox() = 0;
    
    // -- active widgets
    
    virtual void addButton(const char* label, REAL* zone) = 0;
    virtual void addCheckButton(const char* label, REAL* zone) = 0;
    virtual void addVerticalSlider(const char* label, REAL* zone, REAL init, REAL min, REAL max, REAL step) = 0;
    virtual void addHorizontalSlider(const char* label, REAL* zone, REAL init, REAL min, REAL max, REAL step) = 0;
    virtual void addNumEntry(const char* label, REAL* zone, REAL init, REAL min, REAL max, REAL step) = 0;
    
    // -- passive widgets
    
    virtual void addHorizontalBargraph(const char* label, REAL* zone, REAL min, REAL max) = 0;
    virtual void addVerticalBargraph(const char* label, REAL* zone, REAL min, REAL max) = 0;
    
    // -- soundfiles
    
    virtual void addSoundfile(const char* label, const char* filename, Soundfile** sf_zone) = 0;
    
    // -- metadata declarations
    
    virtual void declare(REAL* /*zone*/, const char* /*key*/, const char* /*val*/) {}

    // To be used by LLVM client
    virtual int sizeOfFAUSTFLOAT() { return sizeof(FAUSTFLOAT); }
};

struct FAUST_API UI : public UIReal<FAUSTFLOAT> {
    UI() {}
    virtual ~UI() {}
};

#endif
/**************************  END  UI.h **************************/
/************************** BEGIN SimpleParser.h *********************
 FAUST Architecture File
 Copyright (C) 2003-2022 GRAME, Centre National de Creation Musicale
 ---------------------------------------------------------------------
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation; either version 2.1 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 
 EXCEPTION : As a special exception, you may create a larger work
 that contains this FAUST architecture section and distribute
 that work under terms of your choice, so long as this FAUST
 architecture section is not modified.
 ********************************************************************/

#ifndef SIMPLEPARSER_H
#define SIMPLEPARSER_H

// ---------------------------------------------------------------------
//                          Simple Parser
// A parser returns true if it was able to parse what it is
// supposed to parse and advance the pointer. Otherwise it returns false
// and the pointer is not advanced so that another parser can be tried.
// ---------------------------------------------------------------------

#include <vector>
#include <map>
#include <string>
#include <cmath>
#include <fstream>
#include <sstream>
#include <stdio.h> // We use the lighter fprintf code
#include <ctype.h>
#include <assert.h>

#ifndef _WIN32
# pragma GCC diagnostic ignored "-Wunused-function"
#endif

struct itemInfo {
    std::string type;
    std::string label;
    std::string shortname;
    std::string address;
    std::string url;
    int index;
    double init;
    double fmin;
    double fmax;
    double step;
    std::vector<std::pair<std::string, std::string> > meta;
    
    itemInfo():index(0), init(0.), fmin(0.), fmax(0.), step(0.)
    {}
};

// ---------------------------------------------------------------------
//                          Elementary parsers
// ---------------------------------------------------------------------

// Report a parsing error
static bool parseError(const char*& p, const char* errmsg)
{
    fprintf(stderr, "Parse error : %s here : %s\n", errmsg, p);
    return true;
}

/**
 * @brief skipBlank : advance pointer p to the first non blank character
 * @param p the string to parse, then the remaining string
 */
static void skipBlank(const char*& p)
{
    while (isspace(*p)) { p++; }
}

// Parse character x, but don't report error if fails
static bool tryChar(const char*& p, char x)
{
    skipBlank(p);
    if (x == *p) {
        p++;
        return true;
    } else {
        return false;
    }
}

/**
 * @brief parseChar : parse a specific character x
 * @param p the string to parse, then the remaining string
 * @param x the character to recognize
 * @return true if x was found at the begin of p
 */
static bool parseChar(const char*& p, char x)
{
    skipBlank(p);
    if (x == *p) {
        p++;
        return true;
    } else {
        return false;
    }
}

/**
 * @brief parseWord : parse a specific string w
 * @param p the string to parse, then the remaining string
 * @param w the string to recognize
 * @return true if string w was found at the begin of p
 */
static bool parseWord(const char*& p, const char* w)
{
    skipBlank(p);
    const char* saved = p;  // to restore position if we fail
    while ((*w == *p) && (*w)) {++w; ++p;}
    if (*w) {
        p = saved;
        return false;
    } else {
        return true;
    }
}

/**
 * @brief parseDouble : parse number [s]dddd[.dddd] or [s]d[.dddd][E|e][s][dddd] and store the result in x
 * @param p the string to parse, then the remaining string
 * @param x the float number found if any
 * @return true if a float number was found at the begin of p
 */
static bool parseDouble(const char*& p, double& x)
{
    double sign = 1.0;     // sign of the number
    double ipart = 0;      // integral part of the number
    double dpart = 0;      // decimal part of the number before division
    double dcoef = 1.0;    // division factor for the decimal part
    double expsign = 1.0;  // sign of the E|e part
    double expcoef = 0.0;  // multiplication factor of E|e part
    
    bool valid = false;    // true if the number contains at least one digit
    
    skipBlank(p);
    const char* saved = p;  // to restore position if we fail
    
    // Sign
    if (parseChar(p, '+')) {
        sign = 1.0;
    } else if (parseChar(p, '-')) {
        sign = -1.0;
    }
    
    // Integral part
    while (isdigit(*p)) {
        valid = true;
        ipart = ipart*10 + (*p - '0');
        p++;
    }
    
    // Possible decimal part
    if (parseChar(p, '.')) {
        while (isdigit(*p)) {
            valid = true;
            dpart = dpart*10 + (*p - '0');
            dcoef *= 10.0;
            p++;
        }
    }
    
    // Possible E|e part
    if (parseChar(p, 'E') || parseChar(p, 'e')) {
        if (parseChar(p, '+')) {
            expsign = 1.0;
        } else if (parseChar(p, '-')) {
            expsign = -1.0;
        }
        while (isdigit(*p)) {
            expcoef = expcoef*10 + (*p - '0');
            p++;
        }
    }
    
    if (valid)  {
        x = (sign*(ipart + dpart/dcoef)) * std::pow(10.0, expcoef*expsign);
    } else {
        p = saved;
    }
    return valid;
}

/**
 * @brief parseString, parse an arbitrary quoted string q...q and store the result in s
 * @param p the string to parse, then the remaining string
 * @param quote the character used to quote the string
 * @param s the (unquoted) string found if any
 * @return true if a string was found at the begin of p
 */
static bool parseString(const char*& p, char quote, std::string& s)
{
    std::string str;
    skipBlank(p);
    
    const char* saved = p;  // to restore position if we fail
    if (*p++ == quote) {
        while ((*p != 0) && (*p != quote)) {
            str += *p++;
        }
        if (*p++ == quote) {
            s = str;
            return true;
        }
    }
    p = saved;
    return false;
}

/**
 * @brief parseSQString, parse a single quoted string '...' and store the result in s
 * @param p the string to parse, then the remaining string
 * @param s the (unquoted) string found if any
 * @return true if a string was found at the begin of p
 */
static bool parseSQString(const char*& p, std::string& s)
{
    return parseString(p, '\'', s);
}

/**
 * @brief parseDQString, parse a double quoted string "..." and store the result in s
 * @param p the string to parse, then the remaining string
 * @param s the (unquoted) string found if any
 * @return true if a string was found at the begin of p
 */
static bool parseDQString(const char*& p, std::string& s)
{
    return parseString(p, '"', s);
}

// ---------------------------------------------------------------------
//
//                          IMPLEMENTATION
// 
// ---------------------------------------------------------------------

/**
 * @brief parseMenuItem, parse a menu item ...'low':440.0...
 * @param p the string to parse, then the remaining string
 * @param name the name found
 * @param value the value found
 * @return true if a nemu item was found
 */
static bool parseMenuItem(const char*& p, std::string& name, double& value)
{
    const char* saved = p;  // to restore position if we fail
    if (parseSQString(p, name) && parseChar(p, ':') && parseDouble(p, value)) {
        return true;
    } else {
        p = saved;
        return false;
    }
}

static bool parseMenuItem2(const char*& p, std::string& name)
{
    const char* saved = p;  // to restore position if we fail
    // single quoted
    if (parseSQString(p, name)) {
        return true;
    } else {
        p = saved;
        return false;
    }
}

/**
 * @brief parseMenuList, parse a menu list {'low' : 440.0; 'mid' : 880.0; 'hi' : 1760.0}...
 * @param p the string to parse, then the remaining string
 * @param names the vector of names found
 * @param values the vector of values found
 * @return true if a menu list was found
 */
static bool parseMenuList(const char*& p, std::vector<std::string>& names, std::vector<double>& values)
{
    std::vector<std::string> tmpnames;
    std::vector<double> tmpvalues;
    const char* saved = p; // to restore position if we fail

    if (parseChar(p, '{')) {
        do {
            std::string n;
            double v;
            if (parseMenuItem(p, n, v)) {
                tmpnames.push_back(n);
                tmpvalues.push_back(v);
            } else {
                p = saved;
                return false;
            }
        } while (parseChar(p, ';'));
        if (parseChar(p, '}')) {
            // we suceeded
            names = tmpnames;
            values = tmpvalues;
            return true;
        }
    }
    p = saved;
    return false;
}

static bool parseMenuList2(const char*& p, std::vector<std::string>& names, bool debug)
{
    std::vector<std::string> tmpnames;
    const char* saved = p;  // to restore position if we fail
    
    if (parseChar(p, '{')) {
        do {
            std::string n;
            if (parseMenuItem2(p, n)) {
                tmpnames.push_back(n);
            } else {
                goto error;
            }
        } while (parseChar(p, ';'));
        if (parseChar(p, '}')) {
            // we suceeded
            names = tmpnames;
            return true;
        }
    }
    
error:
    if (debug) { fprintf(stderr, "parseMenuList2 : (%s) is not a valid list !\n", p); }
    p = saved;
    return false;
}

/// ---------------------------------------------------------------------
// Parse list of strings
/// ---------------------------------------------------------------------
static bool parseList(const char*& p, std::vector<std::string>& items)
{
    const char* saved = p;  // to restore position if we fail
    if (parseChar(p, '[')) {
        do {
            std::string item;
            if (!parseDQString(p, item)) {
                p = saved;
                return false;
            }
            items.push_back(item);
        } while (tryChar(p, ','));
        return parseChar(p, ']');
    } else {
        p = saved;
        return false;
    }
}

static bool parseMetaData(const char*& p, std::map<std::string, std::string>& metadatas)
{
    const char* saved = p; // to restore position if we fail
    std::string metaKey, metaValue;
    if (parseChar(p, ':') && parseChar(p, '[')) {
        do { 
            if (parseChar(p, '{') && parseDQString(p, metaKey) && parseChar(p, ':') && parseDQString(p, metaValue) && parseChar(p, '}')) {
                metadatas[metaKey] = metaValue;
            }
        } while (tryChar(p, ','));
        return parseChar(p, ']');
    } else {
        p = saved;
        return false;
    }
}

static bool parseItemMetaData(const char*& p, std::vector<std::pair<std::string, std::string> >& metadatas)
{
    const char* saved = p; // to restore position if we fail
    std::string metaKey, metaValue;
    if (parseChar(p, ':') && parseChar(p, '[')) {
        do { 
            if (parseChar(p, '{') && parseDQString(p, metaKey) && parseChar(p, ':') && parseDQString(p, metaValue) && parseChar(p, '}')) {
                metadatas.push_back(std::make_pair(metaKey, metaValue));
            }
        } while (tryChar(p, ','));
        return parseChar(p, ']');
    } else {
        p = saved;
        return false;
    }
}

// ---------------------------------------------------------------------
// Parse metadatas of the interface:
// "name" : "...", "inputs" : "...", "outputs" : "...", ...
// and store the result as key/value
/// ---------------------------------------------------------------------
static bool parseGlobalMetaData(const char*& p, std::string& key, std::string& value, double& dbl, std::map<std::string, std::string>& metadatas, std::vector<std::string>& items)
{
    const char* saved = p; // to restore position if we fail
    if (parseDQString(p, key)) {
        if (key == "meta") {
            return parseMetaData(p, metadatas);
        } else {
            return parseChar(p, ':') && (parseDQString(p, value) || parseList(p, items) || parseDouble(p, dbl));
        }
    } else {
        p = saved;
        return false;
    }
}

// ---------------------------------------------------------------------
// Parse gui:
// "type" : "...", "label" : "...", "address" : "...", ...
// and store the result in uiItems Vector
/// ---------------------------------------------------------------------
static bool parseUI(const char*& p, std::vector<itemInfo>& uiItems, int& numItems)
{
    const char* saved = p; // to restore position if we fail
    if (parseChar(p, '{')) {
   
        std::string label;
        std::string value;
        double dbl = 0;
        
        do {
            if (parseDQString(p, label)) {
                if (label == "type") {
                    if (uiItems.size() != 0) {
                        numItems++;
                    }
                    if (parseChar(p, ':') && parseDQString(p, value)) {   
                        itemInfo item;
                        item.type = value;
                        uiItems.push_back(item);
                    }
                }
                
                else if (label == "label") {
                    if (parseChar(p, ':') && parseDQString(p, value)) {
                        uiItems[numItems].label = value;
                    }
                }
                
                else if (label == "shortname") {
                    if (parseChar(p, ':') && parseDQString(p, value)) {
                        uiItems[numItems].shortname = value;
                    }
                }
                
                else if (label == "address") {
                    if (parseChar(p, ':') && parseDQString(p, value)) {
                        uiItems[numItems].address = value;
                    }
                }
                
                else if (label == "url") {
                    if (parseChar(p, ':') && parseDQString(p, value)) {
                        uiItems[numItems].url = value;
                    }
                }
                
                else if (label == "index") {
                    if (parseChar(p, ':') && parseDouble(p, dbl)) {
                        uiItems[numItems].index = int(dbl);
                    }
                }
                
                else if (label == "meta") {
                    if (!parseItemMetaData(p, uiItems[numItems].meta)) {
                        return false;
                    }
                }
                
                else if (label == "init") {
                    if (parseChar(p, ':') && parseDouble(p, dbl)) {
                        uiItems[numItems].init = dbl;
                    }
                }
                
                else if (label == "min") {
                    if (parseChar(p, ':') && parseDouble(p, dbl)) {
                        uiItems[numItems].fmin = dbl;
                    }
                }
                
                else if (label == "max") {
                    if (parseChar(p, ':') && parseDouble(p, dbl)) {
                        uiItems[numItems].fmax = dbl;
                    }
                }
                
                else if (label == "step") {
                    if (parseChar(p, ':') && parseDouble(p, dbl)) {
                        uiItems[numItems].step = dbl;
                    }
                }
                
                else if (label == "items") {
                    if (parseChar(p, ':') && parseChar(p, '[')) {
                        do {
                            if (!parseUI(p, uiItems, numItems)) {
                                p = saved;
                                return false;
                            }
                        } while (tryChar(p, ','));
                        if (parseChar(p, ']')) {
                            itemInfo item;
                            item.type = "close";
                            uiItems.push_back(item);
                            numItems++;
                        }
                    }
            
                } else {
                    fprintf(stderr, "Parse error unknown : %s \n", label.c_str());
                    assert(false);
                }
            } else {
                p = saved;
                return false;
            }
            
        } while (tryChar(p, ','));
    
        return parseChar(p, '}');
    } else {
        return true; // "items": [] is valid
    }
}

// ---------------------------------------------------------------------
// Parse full JSON record describing a JSON/Faust interface :
// {"metadatas": "...", "ui": [{ "type": "...", "label": "...", "items": [...], "address": "...","init": "...", "min": "...", "max": "...","step": "..."}]}
//
// and store the result in map Metadatas and vector containing the items of the interface. Returns true if parsing was successfull.
/// ---------------------------------------------------------------------
static bool parseJson(const char*& p,
                      std::map<std::string, std::pair<std::string, double> >& metaDatas0,
                      std::map<std::string, std::string>& metaDatas1,
                      std::map<std::string, std::vector<std::string> >& metaDatas2,
                      std::vector<itemInfo>& uiItems)
{
    parseChar(p, '{');
    
    do {
        std::string key;
        std::string value;
        double dbl = 0;
        std::vector<std::string> items;
        if (parseGlobalMetaData(p, key, value, dbl, metaDatas1, items)) {
            if (key != "meta") {
                // keep "name", "inputs", "outputs" key/value pairs
                if (items.size() > 0) {
                    metaDatas2[key] = items;
                    items.clear();
                } else if (value != "") {
                    metaDatas0[key].first = value;
                } else {
                    metaDatas0[key].second = dbl;
                }
            }
        } else if (key == "ui") {
            int numItems = 0;
            parseChar(p, '[') && parseUI(p, uiItems, numItems);
        }
    } while (tryChar(p, ','));
    
    return parseChar(p, '}');
}

#endif // SIMPLEPARSER_H
/**************************  END  SimpleParser.h **************************/
/************************** BEGIN PathBuilder.h **************************
 FAUST Architecture File
 Copyright (C) 2003-2022 GRAME, Centre National de Creation Musicale
 ---------------------------------------------------------------------
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation; either version 2.1 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 
 EXCEPTION : As a special exception, you may create a larger work
 that contains this FAUST architecture section and distribute
 that work under terms of your choice, so long as this FAUST
 architecture section is not modified.
 ************************************************************************/

#ifndef __PathBuilder__
#define __PathBuilder__

#include <vector>
#include <set>
#include <map>
#include <string>
#include <algorithm>


/*******************************************************************************
 * PathBuilder : Faust User Interface
 * Helper class to build complete hierarchical path for UI items.
 ******************************************************************************/

class FAUST_API PathBuilder {

    protected:
    
        std::vector<std::string> fControlsLevel;
        std::vector<std::string> fFullPaths;
        std::map<std::string, std::string> fFull2Short;  // filled by computeShortNames()
    
        /**
         * @brief check if a character is acceptable for an ID
         *
         * @param c
         * @return true is the character is acceptable for an ID
         */
        bool isIDChar(char c) const
        {
            return ((c >= 'a') && (c <= 'z')) || ((c >= 'A') && (c <= 'Z')) || ((c >= '0') && (c <= '9'));
        }
    
        /**
         * @brief remove all "/0x00" parts
         *
         * @param src
         * @return modified string
         */
        std::string remove0x00(const std::string& src_aux) const
        {
            std::string src = src_aux;
            std::string from = "/0x00";
            std::string to = "";
            size_t pos = std::string::npos;
            while ((pos = src.find(from)) && (pos != std::string::npos)) {
                src = src.replace(pos, from.length(), to);
            }
            return src;
        }
    
        /**
         * @brief replace all non ID char with '_' (one '_' may replace several non ID char)
         *
         * @param src
         * @return modified string
         */
        std::string str2ID(const std::string& src) const
        {
            std::string dst;
            bool need_underscore = false;
            for (char c : src) {
                if (isIDChar(c) || (c == '/')) {
                    if (need_underscore) {
                        dst.push_back('_');
                        need_underscore = false;
                    }
                    dst.push_back(c);
                } else {
                    need_underscore = true;
                }
            }
            return dst;
        }
    
        /**
         * @brief Keep only the last n slash-parts
         *
         * @param src
         * @param n : 1 indicates the last slash-part
         * @return modified string
         */
        std::string cut(const std::string& src, int n) const
        {
            std::string rdst;
            for (int i = int(src.length())-1; i >= 0; i--) {
                char c = src[i];
                if (c != '/') {
                    rdst.push_back(c);
                } else if (n == 1) {
                    std::string dst;
                    for (int j = int(rdst.length())-1; j >= 0; j--) {
                        dst.push_back(rdst[j]);
                    }
                    return dst;
                } else {
                    n--;
                    rdst.push_back(c);
                }
            }
            return src;
        }
    
        void addFullPath(const std::string& label) { fFullPaths.push_back(buildPath(label)); }
    
        /**
         * @brief Compute the mapping between full path and short names
         */
        void computeShortNames()
        {
            std::vector<std::string>           uniquePaths;  // all full paths transformed but made unique with a prefix
            std::map<std::string, std::string> unique2full;  // all full paths transformed but made unique with a prefix
            char num_buffer[16];
            int pnum = 0;
            
            for (const auto& s : fFullPaths) {
                // Using snprintf since Teensy does not have the std::to_string function
                snprintf(num_buffer, 16, "%d", pnum++);
                std::string u = "/P" + std::string(num_buffer) + str2ID(remove0x00(s));
                uniquePaths.push_back(u);
                unique2full[u] = s;  // remember the full path associated to a unique path
            }
        
            std::map<std::string, int> uniquePath2level;                // map path to level
            for (const auto& s : uniquePaths) uniquePath2level[s] = 1;   // we init all levels to 1
            bool have_collisions = true;
        
            while (have_collisions) {
                // compute collision list
                std::set<std::string>              collisionSet;
                std::map<std::string, std::string> short2full;
                have_collisions = false;
                for (const auto& it : uniquePath2level) {
                    std::string u = it.first;
                    int n = it.second;
                    std::string shortName = cut(u, n);
                    auto p = short2full.find(shortName);
                    if (p == short2full.end()) {
                        // no collision
                        short2full[shortName] = u;
                    } else {
                        // we have a collision, add the two paths to the collision set
                        have_collisions = true;
                        collisionSet.insert(u);
                        collisionSet.insert(p->second);
                    }
                }
                for (const auto& s : collisionSet) uniquePath2level[s]++;  // increase level of colliding path
            }
        
            for (const auto& it : uniquePath2level) {
                std::string u = it.first;
                int n = it.second;
                std::string shortName = replaceCharList(cut(u, n), {'/'}, '_');
                fFull2Short[unique2full[u]] = shortName;
            }
        }
    
        std::string replaceCharList(const std::string& str, const std::vector<char>& ch1, char ch2)
        {
            auto beg = ch1.begin();
            auto end = ch1.end();
            std::string res = str;
            for (size_t i = 0; i < str.length(); ++i) {
                if (std::find(beg, end, str[i]) != end) res[i] = ch2;
            }
            return res;
        }
     
    public:
    
        PathBuilder() {}
        virtual ~PathBuilder() {}
    
        // Return true for the first level of groups
        bool pushLabel(const std::string& label) { fControlsLevel.push_back(label); return fControlsLevel.size() == 1; }
    
        // Return true for the last level of groups
        bool popLabel() { fControlsLevel.pop_back(); return fControlsLevel.size() == 0; }
    
        // Return a complete path built from a label
        std::string buildPath(const std::string& label)
        {
            std::string res = "/";
            for (size_t i = 0; i < fControlsLevel.size(); i++) {
                res = res + fControlsLevel[i] + "/";
            }
            res += label;
            return replaceCharList(res, {' ', '#', '*', ',', '?', '[', ']', '{', '}', '(', ')'}, '_');
        }
    
        // Assuming shortnames have been built, return the shortname from a label
        std::string buildShortname(const std::string& label)
        {
            return (hasShortname()) ? fFull2Short[buildPath(label)] : "";
        }
    
        bool hasShortname() { return fFull2Short.size() > 0; }
    
};

#endif  // __PathBuilder__
/**************************  END  PathBuilder.h **************************/
/************************** BEGIN dsp-combiner.h **************************
FAUST Architecture File
Copyright (C) 2003-2022 GRAME, Centre National de Creation Musicale
---------------------------------------------------------------------
This program is free software; you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation; either version 2.1 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

EXCEPTION : As a special exception, you may create a larger work
that contains this FAUST architecture section and distribute
that work under terms of your choice, so long as this FAUST
architecture section is not modified.
************************************************************************/

#ifndef __dsp_combiner__
#define __dsp_combiner__

#include <string.h>
#include <string>
#include <assert.h>
#include <sstream>

/************************** BEGIN dsp.h ********************************
 FAUST Architecture File
 Copyright (C) 2003-2022 GRAME, Centre National de Creation Musicale
 ---------------------------------------------------------------------
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation; either version 2.1 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 
 EXCEPTION : As a special exception, you may create a larger work
 that contains this FAUST architecture section and distribute
 that work under terms of your choice, so long as this FAUST
 architecture section is not modified.
 ************************************************************************/

#ifndef __dsp__
#define __dsp__

#include <string>
#include <vector>
#include <cstdint>


#ifndef FAUSTFLOAT
#define FAUSTFLOAT float
#endif

struct FAUST_API UI;
struct FAUST_API Meta;

/**
 * DSP memory manager.
 */

struct FAUST_API dsp_memory_manager {
    
    virtual ~dsp_memory_manager() {}
    
    /**
     * Inform the Memory Manager with the number of expected memory zones.
     * @param count - the number of expected memory zones
     */
    virtual void begin(size_t /*count*/) {}
    
    /**
     * Give the Memory Manager information on a given memory zone.
     * @param size - the size in bytes of the memory zone
     * @param reads - the number of Read access to the zone used to compute one frame
     * @param writes - the number of Write access to the zone used to compute one frame
     */
    virtual void info(size_t /*size*/, size_t /*reads*/, size_t /*writes*/) {}

    /**
     * Inform the Memory Manager that all memory zones have been described,
     * to possibly start a 'compute the best allocation strategy' step.
     */
    virtual void end() {}
    
    /**
     * Allocate a memory zone.
     * @param size - the memory zone size in bytes
     */
    virtual void* allocate(size_t size) = 0;
    
    /**
     * Destroy a memory zone.
     * @param ptr - the memory zone pointer to be deallocated
     */
    virtual void destroy(void* ptr) = 0;
    
};

/**
* Signal processor definition.
*/

class FAUST_API dsp {

    public:

        dsp() {}
        virtual ~dsp() {}

        /* Return instance number of audio inputs */
        virtual int getNumInputs() = 0;
    
        /* Return instance number of audio outputs */
        virtual int getNumOutputs() = 0;
    
        /**
         * Trigger the ui_interface parameter with instance specific calls
         * to 'openTabBox', 'addButton', 'addVerticalSlider'... in order to build the UI.
         *
         * @param ui_interface - the user interface builder
         */
        virtual void buildUserInterface(UI* ui_interface) = 0;
    
        /* Return the sample rate currently used by the instance */
        virtual int getSampleRate() = 0;
    
        /**
         * Global init, calls the following methods:
         * - static class 'classInit': static tables initialization
         * - 'instanceInit': constants and instance state initialization
         *
         * @param sample_rate - the sampling rate in Hz
         */
        virtual void init(int sample_rate) = 0;

        /**
         * Init instance state
         *
         * @param sample_rate - the sampling rate in Hz
         */
        virtual void instanceInit(int sample_rate) = 0;
    
        /**
         * Init instance constant state
         *
         * @param sample_rate - the sampling rate in Hz
         */
        virtual void instanceConstants(int sample_rate) = 0;
    
        /* Init default control parameters values */
        virtual void instanceResetUserInterface() = 0;
    
        /* Init instance state (like delay lines...) but keep the control parameter values */
        virtual void instanceClear() = 0;
 
        /**
         * Return a clone of the instance.
         *
         * @return a copy of the instance on success, otherwise a null pointer.
         */
        virtual dsp* clone() = 0;
    
        /**
         * Trigger the Meta* parameter with instance specific calls to 'declare' (key, value) metadata.
         *
         * @param m - the Meta* meta user
         */
        virtual void metadata(Meta* m) = 0;
    
        /**
         * DSP instance computation, to be called with successive in/out audio buffers.
         *
         * Note that by default inputs and outputs buffers are supposed to be distinct memory zones,
         * so one cannot safely write compute(count, inputs, inputs).
         * The -inpl compilation option can be used for that, but only in scalar mode for now.
         *
         * @param count - the number of frames to compute
         * @param inputs - the input audio buffers as an array of non-interleaved FAUSTFLOAT samples (eiher float, double or quad)
         * @param outputs - the output audio buffers as an array of non-interleaved FAUSTFLOAT samples (eiher float, double or quad)
         *
         */
        virtual void compute(int count, FAUSTFLOAT** inputs, FAUSTFLOAT** outputs) = 0;
    
        /**
         * Alternative DSP instance computation method for use by subclasses, incorporating an additional `date_usec` parameter,
         * which specifies the timestamp of the first sample in the audio buffers.
         *
         * @param date_usec - the timestamp in microsec given by audio driver. By convention timestamp of -1 means 'no timestamp conversion',
         * events already have a timestamp expressed in frames.
         * @param count - the number of frames to compute
         * @param inputs - the input audio buffers as an array of non-interleaved FAUSTFLOAT samples (either float, double or quad)
         * @param outputs - the output audio buffers as an array of non-interleaved FAUSTFLOAT samples (either float, double or quad)
         *
         */
        virtual void compute(double /*date_usec*/, int count, FAUSTFLOAT** inputs, FAUSTFLOAT** outputs) { compute(count, inputs, outputs); }
       
};

/**
 * Generic DSP decorator.
 */

class FAUST_API decorator_dsp : public dsp {

    protected:

        dsp* fDSP;

    public:

        decorator_dsp(dsp* dsp = nullptr):fDSP(dsp) {}
        virtual ~decorator_dsp() { delete fDSP; }

        virtual int getNumInputs() { return fDSP->getNumInputs(); }
        virtual int getNumOutputs() { return fDSP->getNumOutputs(); }
        virtual void buildUserInterface(UI* ui_interface) { fDSP->buildUserInterface(ui_interface); }
        virtual int getSampleRate() { return fDSP->getSampleRate(); }
        virtual void init(int sample_rate) { fDSP->init(sample_rate); }
        virtual void instanceInit(int sample_rate) { fDSP->instanceInit(sample_rate); }
        virtual void instanceConstants(int sample_rate) { fDSP->instanceConstants(sample_rate); }
        virtual void instanceResetUserInterface() { fDSP->instanceResetUserInterface(); }
        virtual void instanceClear() { fDSP->instanceClear(); }
        virtual decorator_dsp* clone() { return new decorator_dsp(fDSP->clone()); }
        virtual void metadata(Meta* m) { fDSP->metadata(m); }
        // Beware: subclasses usually have to overload the two 'compute' methods
        virtual void compute(int count, FAUSTFLOAT** inputs, FAUSTFLOAT** outputs) { fDSP->compute(count, inputs, outputs); }
        virtual void compute(double date_usec, int count, FAUSTFLOAT** inputs, FAUSTFLOAT** outputs) { fDSP->compute(date_usec, count, inputs, outputs); }
    
};

/**
 * DSP factory class, used with LLVM and Interpreter backends
 * to create DSP instances from a compiled DSP program.
 */

class FAUST_API dsp_factory {
    
    protected:
    
        // So that to force sub-classes to use deleteDSPFactory(dsp_factory* factory);
        virtual ~dsp_factory() {}
    
    public:
    
        /* Return factory name */
        virtual std::string getName() = 0;
    
        /* Return factory SHA key */
        virtual std::string getSHAKey() = 0;
    
        /* Return factory expanded DSP code */
        virtual std::string getDSPCode() = 0;
    
        /* Return factory compile options */
        virtual std::string getCompileOptions() = 0;
    
        /* Get the Faust DSP factory list of library dependancies */
        virtual std::vector<std::string> getLibraryList() = 0;
    
        /* Get the list of all used includes */
        virtual std::vector<std::string> getIncludePathnames() = 0;
    
        /* Get warning messages list for a given compilation */
        virtual std::vector<std::string> getWarningMessages() = 0;
    
        /* Create a new DSP instance, to be deleted with C++ 'delete' */
        virtual dsp* createDSPInstance() = 0;
    
        /* Static tables initialization, possibly implemened in sub-classes*/
        virtual void classInit(int sample_rate) {};
    
        /* Set a custom memory manager to be used when creating instances */
        virtual void setMemoryManager(dsp_memory_manager* manager) = 0;
    
        /* Return the currently set custom memory manager */
        virtual dsp_memory_manager* getMemoryManager() = 0;
    
};

// Denormal handling

#if defined (__SSE__)
#include <xmmintrin.h>
#endif

class FAUST_API ScopedNoDenormals {
    
    private:
    
        intptr_t fpsr = 0;
        
        void setFpStatusRegister(intptr_t fpsr_aux) noexcept
        {
        #if defined (__arm64__) || defined (__aarch64__)
            asm volatile("msr fpcr, %0" : : "ri" (fpsr_aux));
        #elif defined (__SSE__)
            // The volatile keyword here is needed to workaround a bug in AppleClang 13.0
            // which aggressively optimises away the variable otherwise
            volatile uint32_t fpsr_w = static_cast<uint32_t>(fpsr_aux);
            _mm_setcsr(fpsr_w);
        #endif
        }
        
        void getFpStatusRegister() noexcept
        {
        #if defined (__arm64__) || defined (__aarch64__)
            asm volatile("mrs %0, fpcr" : "=r" (fpsr));
        #elif defined (__SSE__)
            fpsr = static_cast<intptr_t>(_mm_getcsr());
        #endif
        }
    
    public:
    
        ScopedNoDenormals() noexcept
        {
        #if defined (__arm64__) || defined (__aarch64__)
            intptr_t mask = (1 << 24 /* FZ */);
        #elif defined (__SSE__)
        #if defined (__SSE2__)
            intptr_t mask = 0x8040;
        #else
            intptr_t mask = 0x8000;
        #endif
        #else
            intptr_t mask = 0x0000;
        #endif
            getFpStatusRegister();
            setFpStatusRegister(fpsr | mask);
        }
        
        ~ScopedNoDenormals() noexcept
        {
            setFpStatusRegister(fpsr);
        }

};

#define AVOIDDENORMALS ScopedNoDenormals ftz_scope;

#endif

/************************** END dsp.h **************************/

/**
 * @file dsp-combiner.h
 * @brief DSP Combiner Library
 *
 * This library provides classes for combining DSP modules.
 * It includes classes for sequencing, parallelizing, splitting, merging, recursing, and crossfading DSP modules.
 *
 */

enum Layout { kVerticalGroup, kHorizontalGroup, kTabGroup };

/**
 * @class dsp_binary_combiner
 * @brief Base class and common code for binary combiners
 *
 * This class serves as the base class for various DSP combiners that work with two DSP modules.
 * It provides common methods for building user interfaces, allocating and deleting channels, and more.
 */
class dsp_binary_combiner : public dsp {

    protected:

        dsp* fDSP1;
        dsp* fDSP2;
        int fBufferSize;
        Layout fLayout;
        std::string fLabel;

        void buildUserInterfaceAux(UI* ui_interface)
        {
            switch (fLayout) {
                case kHorizontalGroup:
                    ui_interface->openHorizontalBox(fLabel.c_str());
                    fDSP1->buildUserInterface(ui_interface);
                    fDSP2->buildUserInterface(ui_interface);
                    ui_interface->closeBox();
                    break;
                case kVerticalGroup:
                    ui_interface->openVerticalBox(fLabel.c_str());
                    fDSP1->buildUserInterface(ui_interface);
                    fDSP2->buildUserInterface(ui_interface);
                    ui_interface->closeBox();
                    break;
                case kTabGroup:
                    ui_interface->openTabBox(fLabel.c_str());
                    ui_interface->openVerticalBox("DSP1");
                    fDSP1->buildUserInterface(ui_interface);
                    ui_interface->closeBox();
                    ui_interface->openVerticalBox("DSP2");
                    fDSP2->buildUserInterface(ui_interface);
                    ui_interface->closeBox();
                    ui_interface->closeBox();
                    break;
            }
        }

        FAUSTFLOAT** allocateChannels(int num)
        {
            FAUSTFLOAT** channels = new FAUSTFLOAT*[num];
            for (int chan = 0; chan < num; chan++) {
                channels[chan] = new FAUSTFLOAT[fBufferSize];
                memset(channels[chan], 0, sizeof(FAUSTFLOAT) * fBufferSize);
            }
            return channels;
        }

        void deleteChannels(FAUSTFLOAT** channels, int num)
        {
            for (int chan = 0; chan < num; chan++) {
                delete [] channels[chan];
            }
            delete [] channels;
        }

     public:

        dsp_binary_combiner(dsp* dsp1, dsp* dsp2, int buffer_size, Layout layout, const std::string& label)
        :fDSP1(dsp1), fDSP2(dsp2), fBufferSize(buffer_size), fLayout(layout), fLabel(label)
        {}

        virtual ~dsp_binary_combiner()
        {
            delete fDSP1;
            delete fDSP2;
        }

        virtual int getSampleRate()
        {
            return fDSP1->getSampleRate();
        }
        virtual void init(int sample_rate)
        {
            fDSP1->init(sample_rate);
            fDSP2->init(sample_rate);
        }
        virtual void instanceInit(int sample_rate)
        {
            fDSP1->instanceInit(sample_rate);
            fDSP2->instanceInit(sample_rate);
        }
        virtual void instanceConstants(int sample_rate)
        {
            fDSP1->instanceConstants(sample_rate);
            fDSP2->instanceConstants(sample_rate);
        }

        virtual void instanceResetUserInterface()
        {
            fDSP1->instanceResetUserInterface();
            fDSP2->instanceResetUserInterface();
        }

        virtual void instanceClear()
        {
            fDSP1->instanceClear();
            fDSP2->instanceClear();
        }

        virtual void metadata(Meta* m)
        {
            fDSP1->metadata(m);
            fDSP2->metadata(m);
        }

};

/**
 * @class dsp_sequencer
 * @brief Combine two 'compatible' DSP modules in sequence
 *
 * This class allows you to combine two DSP modules in sequence.
 * It computes the first DSP module's outputs and uses them as inputs for the second DSP module.
 */
class dsp_sequencer : public dsp_binary_combiner {

    private:

        FAUSTFLOAT** fDSP1Outputs;

    public:

        dsp_sequencer(dsp* dsp1, dsp* dsp2,
                      int buffer_size = 4096,
                      Layout layout = Layout::kTabGroup,
                      const std::string& label = "Sequencer")
        :dsp_binary_combiner(dsp1, dsp2, buffer_size, layout, label)
        {
            fDSP1Outputs = allocateChannels(fDSP1->getNumOutputs());
        }

        virtual ~dsp_sequencer()
        {
            deleteChannels(fDSP1Outputs, fDSP1->getNumOutputs());
        }

        virtual int getNumInputs() { return fDSP1->getNumInputs(); }
        virtual int getNumOutputs() { return fDSP2->getNumOutputs(); }

        virtual void buildUserInterface(UI* ui_interface)
        {
            buildUserInterfaceAux(ui_interface);
        }

        virtual dsp* clone()
        {
            return new dsp_sequencer(fDSP1->clone(), fDSP2->clone(), fBufferSize, fLayout, fLabel);
        }

        virtual void compute(int count, FAUSTFLOAT** inputs, FAUSTFLOAT** outputs)
        {
            fDSP1->compute(count, inputs, fDSP1Outputs);
            fDSP2->compute(count, fDSP1Outputs, outputs);
        }

        virtual void compute(double date_usec, int count, FAUSTFLOAT** inputs, FAUSTFLOAT** outputs) { compute(count, inputs, outputs); }

};

/**
 * @class dsp_parallelizer
 * @brief Combine two DSP modules in parallel
 *
 * This class combines two DSP modules in parallel.
 * It computes both DSP modules separately and combines their outputs.
 */
class dsp_parallelizer : public dsp_binary_combiner {

    private:

        FAUSTFLOAT** fDSP2Inputs;
        FAUSTFLOAT** fDSP2Outputs;

    public:

        dsp_parallelizer(dsp* dsp1, dsp* dsp2,
                     int buffer_size = 4096,
                     Layout layout = Layout::kTabGroup,
                     const std::string& label = "Parallelizer")
        :dsp_binary_combiner(dsp1, dsp2, buffer_size, layout, label)
        {
            fDSP2Inputs = new FAUSTFLOAT*[fDSP2->getNumInputs()];
            fDSP2Outputs = new FAUSTFLOAT*[fDSP2->getNumOutputs()];
        }

        virtual ~dsp_parallelizer()
        {
            delete [] fDSP2Inputs;
            delete [] fDSP2Outputs;
        }

        virtual int getNumInputs() { return fDSP1->getNumInputs() + fDSP2->getNumInputs(); }
        virtual int getNumOutputs() { return fDSP1->getNumOutputs() + fDSP2->getNumOutputs(); }

        virtual void buildUserInterface(UI* ui_interface)
        {
            buildUserInterfaceAux(ui_interface);
        }

        virtual dsp* clone()
        {
            return new dsp_parallelizer(fDSP1->clone(), fDSP2->clone(), fBufferSize, fLayout, fLabel);
        }

        virtual void compute(int count, FAUSTFLOAT** inputs, FAUSTFLOAT** outputs)
        {
            fDSP1->compute(count, inputs, outputs);

            // Shift inputs/outputs channels for fDSP2
            for (int chan = 0; chan < fDSP2->getNumInputs(); chan++) {
                fDSP2Inputs[chan] = inputs[fDSP1->getNumInputs() + chan];
            }
            for (int chan = 0; chan < fDSP2->getNumOutputs(); chan++) {
                fDSP2Outputs[chan] = outputs[fDSP1->getNumOutputs() + chan];
            }

            fDSP2->compute(count, fDSP2Inputs, fDSP2Outputs);
        }

        virtual void compute(double date_usec, int count, FAUSTFLOAT** inputs, FAUSTFLOAT** outputs) { compute(count, inputs, outputs); }

};

/**
 * @class dsp_splitter
 * @brief Combine two 'compatible' DSP modules in a splitter
 *
 * This class combines two DSP modules in a splitter configuration.
 * The outputs of the first DSP module are connected to the inputs of the second DSP module.
 */
class dsp_splitter : public dsp_binary_combiner {

    private:

        FAUSTFLOAT** fDSP1Outputs;
        FAUSTFLOAT** fDSP2Inputs;

    public:

        dsp_splitter(dsp* dsp1, dsp* dsp2,
                     int buffer_size = 4096,
                     Layout layout = Layout::kTabGroup,
                     const std::string& label = "Splitter")
        :dsp_binary_combiner(dsp1, dsp2, buffer_size, layout, label)
        {
            fDSP1Outputs = allocateChannels(fDSP1->getNumOutputs());
            fDSP2Inputs = new FAUSTFLOAT*[fDSP2->getNumInputs()];
        }

        virtual ~dsp_splitter()
        {
            deleteChannels(fDSP1Outputs, fDSP1->getNumOutputs());
            delete [] fDSP2Inputs;
        }

        virtual int getNumInputs() { return fDSP1->getNumInputs(); }
        virtual int getNumOutputs() { return fDSP2->getNumOutputs(); }

        virtual void buildUserInterface(UI* ui_interface)
        {
            buildUserInterfaceAux(ui_interface);
        }

        virtual dsp* clone()
        {
            return new dsp_splitter(fDSP1->clone(), fDSP2->clone(), fBufferSize, fLayout, fLabel);
        }

        virtual void compute(int count, FAUSTFLOAT** inputs, FAUSTFLOAT** outputs)
        {
            fDSP1->compute(count, inputs, fDSP1Outputs);

            for (int chan = 0; chan < fDSP2->getNumInputs(); chan++) {
                 fDSP2Inputs[chan] = fDSP1Outputs[chan % fDSP1->getNumOutputs()];
            }

            fDSP2->compute(count, fDSP2Inputs, outputs);
        }
};

/**
 * @class dsp_merger
 * @brief Combine two 'compatible' DSP modules in a merger
 *
 * This class combines two DSP modules in a merger configuration.
 * The outputs of the first DSP module are combined with the inputs of the second DSP module.
 */
class dsp_merger : public dsp_binary_combiner {

    private:

        FAUSTFLOAT** fDSP1Inputs;
        FAUSTFLOAT** fDSP1Outputs;
        FAUSTFLOAT** fDSP2Inputs;

        void mix(int count, FAUSTFLOAT* dst, FAUSTFLOAT* src)
        {
            for (int frame = 0; frame < count; frame++) {
                dst[frame] += src[frame];
            }
        }

    public:

        dsp_merger(dsp* dsp1, dsp* dsp2,
                   int buffer_size = 4096,
                   Layout layout = Layout::kTabGroup,
                   const std::string& label = "Merger")
        :dsp_binary_combiner(dsp1, dsp2, buffer_size, layout, label)
        {
            fDSP1Inputs = allocateChannels(fDSP1->getNumInputs());
            fDSP1Outputs = allocateChannels(fDSP1->getNumOutputs());
            fDSP2Inputs = new FAUSTFLOAT*[fDSP2->getNumInputs()];
        }

        virtual ~dsp_merger()
        {
            deleteChannels(fDSP1Inputs, fDSP1->getNumInputs());
            deleteChannels(fDSP1Outputs, fDSP1->getNumOutputs());
            delete [] fDSP2Inputs;
        }

        virtual int getNumInputs() { return fDSP1->getNumInputs(); }
        virtual int getNumOutputs() { return fDSP2->getNumOutputs(); }

        virtual void buildUserInterface(UI* ui_interface)
        {
            buildUserInterfaceAux(ui_interface);
        }

        virtual dsp* clone()
        {
            return new dsp_merger(fDSP1->clone(), fDSP2->clone(), fBufferSize, fLayout, fLabel);
        }

        virtual void compute(int count, FAUSTFLOAT** inputs, FAUSTFLOAT** outputs)
        {
            fDSP1->compute(count, fDSP1Inputs, fDSP1Outputs);

            memset(fDSP2Inputs, 0, sizeof(FAUSTFLOAT*) * fDSP2->getNumInputs());

            for (int chan = 0; chan < fDSP1->getNumOutputs(); chan++) {
                int mchan = chan % fDSP2->getNumInputs();
                if (fDSP2Inputs[mchan]) {
                    mix(count, fDSP2Inputs[mchan], fDSP1Outputs[chan]);
                } else {
                    fDSP2Inputs[mchan] = fDSP1Outputs[chan];
                }
            }

            fDSP2->compute(count, fDSP2Inputs, outputs);
        }
};

/**
 * @class dsp_recursiver
 * @brief Combine two 'compatible' DSP modules in a recursive way
 *
 * This class recursively combines two DSP modules.
 * The outputs of each module are fed as inputs to the other module in a recursive manner.
 */
class dsp_recursiver : public dsp_binary_combiner {

    private:

        FAUSTFLOAT** fDSP1Inputs;
        FAUSTFLOAT** fDSP1Outputs;

        FAUSTFLOAT** fDSP2Inputs;
        FAUSTFLOAT** fDSP2Outputs;

    public:

        dsp_recursiver(dsp* dsp1, dsp* dsp2,
                       Layout layout = Layout::kTabGroup,
                       const std::string& label = "Recursiver")
        :dsp_binary_combiner(dsp1, dsp2, 1, layout, label)
        {
            fDSP1Inputs = allocateChannels(fDSP1->getNumInputs());
            fDSP1Outputs = allocateChannels(fDSP1->getNumOutputs());
            fDSP2Inputs = allocateChannels(fDSP2->getNumInputs());
            fDSP2Outputs = allocateChannels(fDSP2->getNumOutputs());
        }

        virtual ~dsp_recursiver()
        {
            deleteChannels(fDSP1Inputs, fDSP1->getNumInputs());
            deleteChannels(fDSP1Outputs, fDSP1->getNumOutputs());
            deleteChannels(fDSP2Inputs, fDSP2->getNumInputs());
            deleteChannels(fDSP2Outputs, fDSP2->getNumOutputs());
        }

        virtual int getNumInputs() { return fDSP1->getNumInputs() - fDSP2->getNumOutputs(); }
        virtual int getNumOutputs() { return fDSP1->getNumOutputs(); }

        virtual void buildUserInterface(UI* ui_interface)
        {
            buildUserInterfaceAux(ui_interface);
        }

        virtual dsp* clone()
        {
            return new dsp_recursiver(fDSP1->clone(), fDSP2->clone(), fLayout, fLabel);
        }

        virtual void compute(int count, FAUSTFLOAT** inputs, FAUSTFLOAT** outputs)
        {
            for (int frame = 0; (frame < count); frame++) {

                for (int chan = 0; chan < fDSP2->getNumOutputs(); chan++) {
                    fDSP1Inputs[chan][0] = fDSP2Outputs[chan][0];
                }

                for (int chan = 0; chan < fDSP1->getNumInputs() - fDSP2->getNumOutputs(); chan++) {
                    fDSP1Inputs[chan + fDSP2->getNumOutputs()][0] = inputs[chan][frame];
                }

                fDSP1->compute(1, fDSP1Inputs, fDSP1Outputs);

                for (int chan = 0; chan < fDSP1->getNumOutputs(); chan++) {
                    outputs[chan][frame] = fDSP1Outputs[chan][0];
                }

                for (int chan = 0; chan < fDSP2->getNumInputs(); chan++) {
                    fDSP2Inputs[chan][0] = fDSP1Outputs[chan][0];
                }

                fDSP2->compute(1, fDSP2Inputs, fDSP2Outputs);
            }
        }

        virtual void compute(double date_usec, int count, FAUSTFLOAT** inputs, FAUSTFLOAT** outputs) { compute(count, inputs, outputs); }

};

/**
 * @class dsp_crossfader
 * @brief Crossfade between two DSP modules
 *
 * This class allows you to crossfade between two DSP modules.
 * The crossfade parameter (as a slider) controls the mix between the two modules' outputs.
 * When Crossfade = 1, the first DSP only is computed, when Crossfade = 0,
 * the second DSP only is computed, otherwise both DSPs are computed and mixed.
 */
class dsp_crossfader: public dsp_binary_combiner {

    private:
    
        FAUSTFLOAT fCrossfade;
        FAUSTFLOAT** fDSPOutputs1;
        FAUSTFLOAT** fDSPOutputs2;
    
    public:
    
        dsp_crossfader(dsp* dsp1, dsp* dsp2,
                       Layout layout = Layout::kTabGroup,
                       const std::string& label = "Crossfade")
        :dsp_binary_combiner(dsp1, dsp2, 4096, layout, label),fCrossfade(FAUSTFLOAT(0.5))
        {
            fDSPOutputs1 = allocateChannels(fDSP1->getNumOutputs());
            fDSPOutputs2 = allocateChannels(fDSP1->getNumOutputs());
        }
    
        virtual ~dsp_crossfader()
        {
            deleteChannels(fDSPOutputs1, fDSP1->getNumInputs());
            deleteChannels(fDSPOutputs2, fDSP1->getNumOutputs());
        }
    
        virtual int getNumInputs() { return fDSP1->getNumInputs(); }
        virtual int getNumOutputs() { return fDSP1->getNumOutputs(); }

        void buildUserInterface(UI* ui_interface)
        {
            switch (fLayout) {
                case kHorizontalGroup:
                    ui_interface->openHorizontalBox(fLabel.c_str());
                    ui_interface->addHorizontalSlider("Crossfade", &fCrossfade, FAUSTFLOAT(0.5), FAUSTFLOAT(0), FAUSTFLOAT(1), FAUSTFLOAT(0.01));
                    fDSP1->buildUserInterface(ui_interface);
                    fDSP2->buildUserInterface(ui_interface);
                    ui_interface->closeBox();
                    break;
                case kVerticalGroup:
                    ui_interface->openVerticalBox(fLabel.c_str());
                    ui_interface->addHorizontalSlider("Crossfade", &fCrossfade, FAUSTFLOAT(0.5), FAUSTFLOAT(0), FAUSTFLOAT(1), FAUSTFLOAT(0.01));
                    fDSP1->buildUserInterface(ui_interface);
                    fDSP2->buildUserInterface(ui_interface);
                    ui_interface->closeBox();
                    break;
                case kTabGroup:
                    ui_interface->openTabBox(fLabel.c_str());
                    ui_interface->openVerticalBox("Crossfade");
                    ui_interface->addHorizontalSlider("Crossfade", &fCrossfade, FAUSTFLOAT(0.5), FAUSTFLOAT(0), FAUSTFLOAT(1), FAUSTFLOAT(0.01));
                    ui_interface->closeBox();
                    ui_interface->openVerticalBox("DSP1");
                    fDSP1->buildUserInterface(ui_interface);
                    ui_interface->closeBox();
                    ui_interface->openVerticalBox("DSP2");
                    fDSP2->buildUserInterface(ui_interface);
                    ui_interface->closeBox();
                    ui_interface->closeBox();
                    break;
            }
        }
    
        virtual dsp* clone()
        {
            return new dsp_crossfader(fDSP1->clone(), fDSP2->clone(), fLayout, fLabel);
        }
    
        virtual void compute(int count, FAUSTFLOAT** inputs, FAUSTFLOAT** outputs)
        {
            if (fCrossfade == FAUSTFLOAT(1)) {
                fDSP1->compute(count, inputs, outputs);
            } else if (fCrossfade == FAUSTFLOAT(0)) {
                fDSP2->compute(count, inputs, outputs);
            } else {
                // Compute each effect
                fDSP1->compute(count, inputs, fDSPOutputs1);
                fDSP2->compute(count, inputs, fDSPOutputs2);
                // Mix between the two effects
                FAUSTFLOAT gain1 = fCrossfade;
                FAUSTFLOAT gain2 = FAUSTFLOAT(1) - gain1;
                for (int frame = 0; (frame < count); frame++) {
                    for (int chan = 0; chan < fDSP1->getNumOutputs(); chan++) {
                        outputs[chan][frame] = fDSPOutputs1[chan][frame] * gain1 + fDSPOutputs2[chan][frame] * gain2;
                    }
                }
            }
        }
    
        virtual void compute(double date_usec, int count, FAUSTFLOAT** inputs, FAUSTFLOAT** outputs) { compute(count, inputs, outputs); }
};

#ifndef __dsp_algebra_api__
#define __dsp_algebra_api__

/**
 * DSP algebra API allowing to combine DSPs using the 5 operators Faust block algebra and an additional crossfader combiner.
 * The two arguments GUI are composed in a group, either kVerticalGroup, kHorizontalGroup or kTabGroup with a label.
 *
 * Each operation takes two DSP and a optional layout and label parameters, returns the combined DSPs,
 * or null if failure with an error message.
 * 
 * It includes methods to create sequencers, parallelizers, splitters, mergers, recursivers, and crossfaders.
 */

/**
 * Create a DSP Sequencer
 *
 * This method creates a DSP Sequencer, which combines two DSP modules in a sequencer configuration.
 * The outputs of the first DSP module are connected to the inputs of the second DSP module.
 *
 * @param dsp1 The first DSP module to combine
 * @param dsp2 The second DSP module to combine
 * @param error A reference to a string to store error messages (if any)
 * @param layout The layout for the combined user interface (default: kTabGroup)
 * @param label The label for the combiner (default: "Sequencer")
 * @return A pointer to the created DSP Sequencer, or nullptr if an error occurs
 */
static dsp* createDSPSequencer(dsp* dsp1, dsp* dsp2,
                               std::string& error,
                               Layout layout = Layout::kTabGroup,
                               const std::string& label = "Sequencer")
{
    if (dsp1->getNumOutputs() != dsp2->getNumInputs()) {
        std::stringstream error_aux;
        error_aux << "Connection error in dsp_sequencer : the number of outputs ("
                  << dsp1->getNumOutputs() << ") of A "
                  << "must be equal to the number of inputs (" << dsp2->getNumInputs() << ") of B" << std::endl;
        error = error_aux.str();
        return nullptr;
    } else {
        return new dsp_sequencer(dsp1, dsp2, 4096, layout, label);
    }
}

/**
 * Create a DSP Parallelizer
 *
 * This method creates a DSP Parallelizer, which combines two DSP modules in parallel.
 * The resulting DSP module computes both input modules separately and combines their outputs.
 *
 * @param dsp1 The first DSP module to combine
 * @param dsp2 The second DSP module to combine
 * @param error A reference to a string to store error messages (if any)
 * @param layout The layout for the combined user interface (default: kTabGroup)
 * @param label The label for the combiner (default: "Parallelizer")
 * @return A pointer to the created DSP Parallelizer, or nullptr if an error occurs
 */
static dsp* createDSPParallelizer(dsp* dsp1, dsp* dsp2,
                                  std::string& error,
                                  Layout layout = Layout::kTabGroup,
                                  const std::string& label = "Parallelizer")
{
    return new dsp_parallelizer(dsp1, dsp2, 4096, layout, label);
}

/**
 * Create a DSP Splitter
 *
 * This method creates a DSP Splitter, which combines two 'compatible' DSP modules in a splitter configuration.
 * The outputs of the first DSP module are connected to the inputs of the second DSP module.
 *
 * @param dsp1 The first DSP module to combine
 * @param dsp2 The second DSP module to combine
 * @param error A reference to a string to store error messages (if any)
 * @param layout The layout for the combined user interface (default: kTabGroup)
 * @param label The label for the combiner (default: "Splitter")
 * @return A pointer to the created DSP Splitter, or nullptr if an error occurs
 */
static dsp* createDSPSplitter(dsp* dsp1, dsp* dsp2, std::string& error, Layout layout = Layout::kTabGroup, const std::string& label = "Splitter")
{
    if (dsp1->getNumOutputs() == 0) {
        error = "Connection error in dsp_splitter : the first expression has no outputs\n";
        return nullptr;
    } else if (dsp2->getNumInputs() == 0) {
        error = "Connection error in dsp_splitter : the second expression has no inputs\n";
        return nullptr;
    } else if (dsp2->getNumInputs() % dsp1->getNumOutputs() != 0) {
        std::stringstream error_aux;
        error_aux << "Connection error in dsp_splitter : the number of outputs (" << dsp1->getNumOutputs()
                  << ") of the first expression should be a divisor of the number of inputs ("
                  << dsp2->getNumInputs()
                  << ") of the second expression" << std::endl;
        error = error_aux.str();
        return nullptr;
    } else if (dsp2->getNumInputs() == dsp1->getNumOutputs()) {
        return new dsp_sequencer(dsp1, dsp2, 4096, layout, label);
    } else {
        return new dsp_splitter(dsp1, dsp2, 4096, layout, label);
    }
}

/**
 * Create a DSP Merger
 *
 * This method creates a DSP Merger, which combines two 'compatible' DSP modules in a merger configuration.
 * The outputs of the first DSP module are combined with the inputs of the second DSP module.
 *
 * @param dsp1 The first DSP module to combine
 * @param dsp2 The second DSP module to combine
 * @param error A reference to a string to store error messages (if any)
 * @param layout The layout for the combined user interface (default: kTabGroup)
 * @param label The label for the combiner (default: "Merger")
 * @return A pointer to the created DSP Merger, or nullptr if an error occurs
 */
static dsp* createDSPMerger(dsp* dsp1, dsp* dsp2,
                            std::string& error,
                            Layout layout = Layout::kTabGroup,
                            const std::string& label = "Merger")
{
    if (dsp1->getNumOutputs() == 0) {
        error = "Connection error in dsp_merger : the first expression has no outputs\n";
        return nullptr;
    } else if (dsp2->getNumInputs() == 0) {
        error = "Connection error in dsp_merger : the second expression has no inputs\n";
        return nullptr;
    } else if (dsp1->getNumOutputs() % dsp2->getNumInputs() != 0) {
        std::stringstream error_aux;
        error_aux << "Connection error in dsp_merger : the number of outputs (" << dsp1->getNumOutputs()
                  << ") of the first expression should be a multiple of the number of inputs ("
                  << dsp2->getNumInputs()
                  << ") of the second expression" << std::endl;
        error = error_aux.str();
        return nullptr;
    } else if (dsp2->getNumInputs() == dsp1->getNumOutputs()) {
        return new dsp_sequencer(dsp1, dsp2, 4096, layout, label);
    } else {
        return new dsp_merger(dsp1, dsp2, 4096, layout, label);
    }
}

/**
 * Create a DSP Recursiver
 *
 * This method creates a DSP Recursiver, which combines two 'compatible' DSP modules in a recursive way.
 * The outputs of each module are fed as inputs to the other module in a recursive manner.
 *
 * @param dsp1 The first DSP module to combine
 * @param dsp2 The second DSP module to combine
 * @param error A reference to a string to store error messages (if any)
 * @param layout The layout for the combined user interface (default: kTabGroup)
 * @param label The label for the combiner (default: "Recursiver")
 * @return A pointer to the created DSP Recursiver, or nullptr if an error occurs
 */
static dsp* createDSPRecursiver(dsp* dsp1, dsp* dsp2,
                                std::string& error,
                                Layout layout = Layout::kTabGroup,
                                const std::string& label = "Recursiver")
{
    if ((dsp2->getNumInputs() > dsp1->getNumOutputs()) || (dsp2->getNumOutputs() > dsp1->getNumInputs())) {
        std::stringstream error_aux;
        error_aux << "Connection error in : dsp_recursiver" << std::endl;
        if (dsp2->getNumInputs() > dsp1->getNumOutputs()) {
            error_aux << "The number of outputs " << dsp1->getNumOutputs()
                      << " of the first expression should be greater or equal to the number of inputs ("
                      << dsp2->getNumInputs()
                      << ") of the second expression" << std::endl;
        }
        if (dsp2->getNumOutputs() > dsp1->getNumInputs()) {
            error_aux << "The number of inputs " << dsp1->getNumInputs()
                      << " of the first expression should be greater or equal to the number of outputs ("
                      << dsp2->getNumOutputs()
                      << ") of the second expression" << std::endl;
        }
        error = error_aux.str();
        return nullptr;
    } else {
        return new dsp_recursiver(dsp1, dsp2, layout, label);
    }
}

/**
 * Create a DSP Crossfader
 *
 * This method creates a DSP Crossfader, which allows you to crossfade between two DSP modules.
 * The crossfade parameter (as a slider) controls the mix between the two modules' outputs.
 * When Crossfade = 1, the first DSP only is computed, when Crossfade = 0,
 * the second DSP only is computed, otherwise both DSPs are computed and mixed.
 *
 * @param dsp1 The first DSP module to combine
 * @param dsp2 The second DSP module to combine
 * @param error A reference to a string to store error messages (if any)
 * @param layout The layout for the combined user interface (default: kTabGroup)
 * @param label The label for the crossfade slider (default: "Crossfade")
 * @return A pointer to the created DSP Crossfader, or nullptr if an error occurs
 */
static dsp* createDSPCrossfader(dsp* dsp1, dsp* dsp2,
                                std::string& error,
                                Layout layout = Layout::kTabGroup,
                                const std::string& label = "Crossfade")
{
    if (dsp1->getNumInputs() != dsp2->getNumInputs()) {
        std::stringstream error_aux;
        error_aux << "Error in dsp_crossfader : the number of inputs ("
        << dsp1->getNumInputs() << ") of A "
        << "must be equal to the number of inputs (" << dsp2->getNumInputs() << ") of B" << std::endl;
        error = error_aux.str();
        return nullptr;
    } else if (dsp1->getNumOutputs() != dsp2->getNumOutputs()) {
        std::stringstream error_aux;
        error_aux << "Error in dsp_crossfader : the number of outputs ("
        << dsp1->getNumOutputs() << ") of A "
        << "must be equal to the number of outputs (" << dsp2->getNumOutputs() << ") of B" << std::endl;
        error = error_aux.str();
        return nullptr;
    } else {
        return new dsp_crossfader(dsp1, dsp2, layout, label);
    }
}

#endif

#endif
/************************** END dsp-combiner.h **************************/
/************************** BEGIN dsp-adapter.h *************************
 FAUST Architecture File
 Copyright (C) 2003-2022 GRAME, Centre National de Creation Musicale
 ---------------------------------------------------------------------
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation; either version 2.1 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 
 EXCEPTION : As a special exception, you may create a larger work
 that contains this FAUST architecture section and distribute
 that work under terms of your choice, so long as this FAUST
 architecture section is not modified.
 ************************************************************************/

#ifndef __dsp_adapter__
#define __dsp_adapter__

#ifndef _WIN32
#include <alloca.h>
#endif
#include <string.h>
#include <cmath>
#include <assert.h>
#include <stdio.h>


// Adapts a DSP for a different number of inputs/outputs
class dsp_adapter : public decorator_dsp {
    
    private:
    
        FAUSTFLOAT** fAdaptedInputs;
        FAUSTFLOAT** fAdaptedOutputs;
        int fHWInputs;
        int fHWOutputs;
        int fBufferSize;
    
        void adaptBuffers(FAUSTFLOAT** inputs, FAUSTFLOAT** outputs)
        {
            for (int i = 0; i < fHWInputs; i++) {
                fAdaptedInputs[i] = inputs[i];
            }
            for (int i = 0; i < fHWOutputs; i++) {
                fAdaptedOutputs[i] = outputs[i];
            }
        }
    
    public:
    
        dsp_adapter(dsp* dsp, int hw_inputs, int hw_outputs, int buffer_size):decorator_dsp(dsp)
        {
            fHWInputs = hw_inputs;
            fHWOutputs = hw_outputs;
            fBufferSize = buffer_size;
            
            fAdaptedInputs = new FAUSTFLOAT*[dsp->getNumInputs()];
            for (int i = 0; i < dsp->getNumInputs() - fHWInputs; i++) {
                fAdaptedInputs[i + fHWInputs] = new FAUSTFLOAT[buffer_size];
                memset(fAdaptedInputs[i + fHWInputs], 0, sizeof(FAUSTFLOAT) * buffer_size);
            }
            
            fAdaptedOutputs = new FAUSTFLOAT*[dsp->getNumOutputs()];
            for (int i = 0; i < dsp->getNumOutputs() - fHWOutputs; i++) {
                fAdaptedOutputs[i + fHWOutputs] = new FAUSTFLOAT[buffer_size];
                memset(fAdaptedOutputs[i + fHWOutputs], 0, sizeof(FAUSTFLOAT) * buffer_size);
            }
        }
    
        virtual ~dsp_adapter()
        {
            for (int i = 0; i < fDSP->getNumInputs() - fHWInputs; i++) {
                delete [] fAdaptedInputs[i + fHWInputs];
            }
            delete [] fAdaptedInputs;
            
            for (int i = 0; i < fDSP->getNumOutputs() - fHWOutputs; i++) {
                delete [] fAdaptedOutputs[i + fHWOutputs];
            }
            delete [] fAdaptedOutputs;
        }
    
        virtual int getNumInputs() { return fHWInputs; }
        virtual int getNumOutputs() { return fHWOutputs; }
    
        virtual dsp_adapter* clone() { return new dsp_adapter(fDSP->clone(), fHWInputs, fHWOutputs, fBufferSize); }
    
        virtual void compute(double date_usec, int count, FAUSTFLOAT** inputs, FAUSTFLOAT** outputs)
        {
            adaptBuffers(inputs, outputs);
            fDSP->compute(date_usec, count, fAdaptedInputs, fAdaptedOutputs);
        }
    
        virtual void compute(int count, FAUSTFLOAT** inputs, FAUSTFLOAT** outputs)
        {
            adaptBuffers(inputs, outputs);
            fDSP->compute(count, fAdaptedInputs, fAdaptedOutputs);
        }
};

// Adapts a DSP for a different sample size
template <typename REAL_INT, typename REAL_EXT>
class dsp_sample_adapter : public decorator_dsp {
    
    private:
    
        REAL_INT** fAdaptedInputs;
        REAL_INT** fAdaptedOutputs;
    
        void adaptInputBuffers(int count, FAUSTFLOAT** inputs)
        {
            for (int chan = 0; chan < fDSP->getNumInputs(); chan++) {
                for (int frame = 0; frame < count; frame++) {
                    fAdaptedInputs[chan][frame] = REAL_INT(reinterpret_cast<REAL_EXT**>(inputs)[chan][frame]);
                }
            }
        }
    
        void adaptOutputsBuffers(int count, FAUSTFLOAT** outputs)
        {
            for (int chan = 0; chan < fDSP->getNumOutputs(); chan++) {
                for (int frame = 0; frame < count; frame++) {
                    reinterpret_cast<REAL_EXT**>(outputs)[chan][frame] = REAL_EXT(fAdaptedOutputs[chan][frame]);
                }
            }
        }
    
    public:
    
        dsp_sample_adapter(dsp* dsp):decorator_dsp(dsp)
        {
            fAdaptedInputs = new REAL_INT*[dsp->getNumInputs()];
            for (int i = 0; i < dsp->getNumInputs(); i++) {
                fAdaptedInputs[i] = new REAL_INT[4096];
            }
            
            fAdaptedOutputs = new REAL_INT*[dsp->getNumOutputs()];
            for (int i = 0; i < dsp->getNumOutputs(); i++) {
                fAdaptedOutputs[i] = new REAL_INT[4096];
            }
        }
    
        virtual ~dsp_sample_adapter()
        {
            for (int i = 0; i < fDSP->getNumInputs(); i++) {
                delete [] fAdaptedInputs[i];
            }
            delete [] fAdaptedInputs;
            
            for (int i = 0; i < fDSP->getNumOutputs(); i++) {
                delete [] fAdaptedOutputs[i];
            }
            delete [] fAdaptedOutputs;
        }
    
        virtual dsp_sample_adapter* clone() { return new dsp_sample_adapter(fDSP->clone()); }
    
        virtual void compute(int count, FAUSTFLOAT** inputs, FAUSTFLOAT** outputs)
        {
            assert(count <= 4096);
            adaptInputBuffers(count, inputs);
            // DSP base class uses FAUSTFLOAT** type, so reinterpret_cast has to be used even if the real DSP uses REAL_INT
            fDSP->compute(count, reinterpret_cast<FAUSTFLOAT**>(fAdaptedInputs), reinterpret_cast<FAUSTFLOAT**>(fAdaptedOutputs));
            adaptOutputsBuffers(count, outputs);
        }
    
        virtual void compute(double date_usec, int count, FAUSTFLOAT** inputs, FAUSTFLOAT** outputs)
        {
            assert(count <= 4096);
            adaptInputBuffers(count, inputs);
            // DSP base class uses FAUSTFLOAT** type, so reinterpret_cast has to be used even if the real DSP uses REAL_INT
            fDSP->compute(date_usec, count, reinterpret_cast<FAUSTFLOAT**>(fAdaptedInputs), reinterpret_cast<FAUSTFLOAT**>(fAdaptedOutputs));
            adaptOutputsBuffers(count, outputs);
        }
};

// Template used to specialize double parameters expressed as NUM/DENOM
template <int NUM, int DENOM>
struct Double {
    static constexpr double value() { return double(NUM)/double(DENOM); }
};

// Base class for filters
template <class fVslider0, int fVslider1>
struct Filter {
    inline int getFactor() { return fVslider1; }
};

// Identity filter: copy input to output
template <class fVslider0, int fVslider1>
struct Identity : public Filter<fVslider0, fVslider1> {
    inline int getFactor() { return fVslider1; }
    
    inline void compute(int count, FAUSTFLOAT* input0, FAUSTFLOAT* output0)
    {
        memcpy(output0, input0, count * sizeof(FAUSTFLOAT));
    }
};

// Generated with process = fi.lowpass(3, ma.SR*hslider("FCFactor", 0.4, 0.4, 0.5, 0.01)/hslider("Factor", 2, 2, 8, 1));
template <class fVslider0, int fVslider1, typename REAL>
struct LowPass3 : public Filter<fVslider0, fVslider1> {
    
    REAL fVec0[2];
    REAL fRec1[2];
    REAL fRec0[3];
    
    inline REAL LowPass3_faustpower2_f(REAL value)
    {
        return (value * value);
    }
    
    LowPass3()
    {
        for (int l0 = 0; (l0 < 2); l0 = (l0 + 1)) {
            fVec0[l0] = 0.0;
        }
        for (int l1 = 0; (l1 < 2); l1 = (l1 + 1)) {
            fRec1[l1] = 0.0;
        }
        for (int l2 = 0; (l2 < 3); l2 = (l2 + 1)) {
            fRec0[l2] = 0.0;
        }
    }
    
    inline void compute(int count, FAUSTFLOAT* input0, FAUSTFLOAT* output0)
    {
        // Computed at template specialization time
        REAL fSlow0 = std::tan((3.1415926535897931 * (REAL(fVslider0::value()) / REAL(fVslider1))));
        REAL fSlow1 = (1.0 / fSlow0);
        REAL fSlow2 = (1.0 / (((fSlow1 + 1.0000000000000002) / fSlow0) + 1.0));
        REAL fSlow3 = (1.0 / (fSlow1 + 1.0));
        REAL fSlow4 = (1.0 - fSlow1);
        REAL fSlow5 = (((fSlow1 + -1.0000000000000002) / fSlow0) + 1.0);
        REAL fSlow6 = (2.0 * (1.0 - (1.0 / LowPass3_faustpower2_f(fSlow0))));
        // Computed at runtime
        for (int i = 0; (i < count); i = (i + 1)) {
            REAL fTemp0 = REAL(input0[i]);
            fVec0[0] = fTemp0;
            fRec1[0] = (0.0 - (fSlow3 * ((fSlow4 * fRec1[1]) - (fTemp0 + fVec0[1]))));
            fRec0[0] = (fRec1[0] - (fSlow2 * ((fSlow5 * fRec0[2]) + (fSlow6 * fRec0[1]))));
            output0[i] = FAUSTFLOAT((fSlow2 * (fRec0[2] + (fRec0[0] + (2.0 * fRec0[1])))));
            fVec0[1] = fVec0[0];
            fRec1[1] = fRec1[0];
            fRec0[2] = fRec0[1];
            fRec0[1] = fRec0[0];
        }
    }
};

// Generated with process = fi.lowpass(4, ma.SR*hslider("FCFactor", 0.4, 0.4, 0.5, 0.01)/hslider("Factor", 2, 2, 8, 1));
template <class fVslider0, int fVslider1, typename REAL>
struct LowPass4 : public Filter<fVslider0, fVslider1> {
    
    REAL fRec1[3];
    REAL fRec0[3];
    
    inline REAL LowPass4_faustpower2_f(REAL value)
    {
        return (value * value);
    }
    
    LowPass4()
    {
        for (int l0 = 0; (l0 < 3); l0 = (l0 + 1)) {
            fRec1[l0] = 0.0f;
        }
        for (int l1 = 0; (l1 < 3); l1 = (l1 + 1)) {
            fRec0[l1] = 0.0f;
        }
    }
    
    inline void compute(int count, FAUSTFLOAT* input0, FAUSTFLOAT* output0)
    {
        // Computed at template specialization time
        REAL fSlow0 = std::tan((3.1415926535897931 * (REAL(fVslider0::value()) / REAL(fVslider1))));
        REAL fSlow1 = (1.0 / fSlow0);
        REAL fSlow2 = (1.0 / (((fSlow1 + 0.76536686473017945) / fSlow0) + 1.0));
        REAL fSlow3 = (1.0 / (((fSlow1 + 1.8477590650225735) / fSlow0) + 1.0));
        REAL fSlow4 = (((fSlow1 + -1.8477590650225735) / fSlow0) + 1.0);
        REAL fSlow5 = (2.0 * (1.0 - (1.0 / LowPass4_faustpower2_f(fSlow0))));
        REAL fSlow6 = (((fSlow1 + -0.76536686473017945) / fSlow0) + 1.0);
        // Computed at runtime
        for (int i = 0; (i < count); i = (i + 1)) {
            fRec1[0] = (REAL(input0[i]) - (fSlow3 * ((fSlow4 * fRec1[2]) + (fSlow5 * fRec1[1]))));
            fRec0[0] = ((fSlow3 * (fRec1[2] + (fRec1[0] + (2.0 * fRec1[1])))) - (fSlow2 * ((fSlow6 * fRec0[2]) + (fSlow5 * fRec0[1]))));
            output0[i] = FAUSTFLOAT((fSlow2 * (fRec0[2] + (fRec0[0] + (2.0 * fRec0[1])))));
            fRec1[2] = fRec1[1];
            fRec1[1] = fRec1[0];
            fRec0[2] = fRec0[1];
            fRec0[1] = fRec0[0];
        }
    }
};

// Generated with process = fi.lowpass3e(ma.SR*hslider("FCFactor", 0.4, 0.4, 0.5, 0.01)/hslider("Factor", 2, 2, 8, 1));
template <class fVslider0, int fVslider1, typename REAL>
struct LowPass3e : public Filter<fVslider0, fVslider1> {
    
    REAL fRec1[3];
    REAL fVec0[2];
    REAL fRec0[2];
    
    inline REAL LowPass3e_faustpower2_f(REAL value)
    {
        return (value * value);
    }
    
    LowPass3e()
    {
        for (int l0 = 0; (l0 < 3); l0 = (l0 + 1)) {
            fRec1[l0] = 0.0;
        }
        for (int l1 = 0; (l1 < 2); l1 = (l1 + 1)) {
            fVec0[l1] = 0.0;
        }
        for (int l2 = 0; (l2 < 2); l2 = (l2 + 1)) {
            fRec0[l2] = 0.0;
        }
    }
    
    inline void compute(int count, FAUSTFLOAT* input0, FAUSTFLOAT* output0)
    {
        // Computed at template specialization time
        REAL fSlow0 = std::tan((3.1415926535897931 * (REAL(fVslider0::value()) / REAL(fVslider1))));
        REAL fSlow1 = (1.0 / fSlow0);
        REAL fSlow2 = (1.0 / (fSlow1 + 0.82244590899881598));
        REAL fSlow3 = (0.82244590899881598 - fSlow1);
        REAL fSlow4 = (1.0 / (((fSlow1 + 0.80263676416103003) / fSlow0) + 1.4122708937742039));
        REAL fSlow5 = LowPass3e_faustpower2_f(fSlow0);
        REAL fSlow6 = (0.019809144837788999 / fSlow5);
        REAL fSlow7 = (fSlow6 + 1.1615164189826961);
        REAL fSlow8 = (((fSlow1 + -0.80263676416103003) / fSlow0) + 1.4122708937742039);
        REAL fSlow9 = (2.0 * (1.4122708937742039 - (1.0 / fSlow5)));
        REAL fSlow10 = (2.0 * (1.1615164189826961 - fSlow6));
        // Computed at runtime
        for (int i = 0; (i < count); i = (i + 1)) {
            fRec1[0] = (REAL(input0[i]) - (fSlow4 * ((fSlow8 * fRec1[2]) + (fSlow9 * fRec1[1]))));
            REAL fTemp0 = (fSlow4 * (((fSlow7 * fRec1[0]) + (fSlow10 * fRec1[1])) + (fSlow7 * fRec1[2])));
            fVec0[0] = fTemp0;
            fRec0[0] = (0.0 - (fSlow2 * ((fSlow3 * fRec0[1]) - (fTemp0 + fVec0[1]))));
            output0[i] = FAUSTFLOAT(fRec0[0]);
            fRec1[2] = fRec1[1];
            fRec1[1] = fRec1[0];
            fVec0[1] = fVec0[0];
            fRec0[1] = fRec0[0];
        }
    }
};

// Generated with process = fi.lowpass6e(ma.SR*hslider("FCFactor", 0.4, 0.4, 0.5, 0.01)/hslider("Factor", 2, 2, 8, 1));
template <class fVslider0, int fVslider1, typename REAL>
struct LowPass6e : public Filter<fVslider0, fVslider1> {
    
    REAL fRec2[3];
    REAL fRec1[3];
    REAL fRec0[3];
    
    inline REAL LowPass6e_faustpower2_f(REAL value)
    {
        return (value * value);
    }
    
    LowPass6e()
    {
        for (int l0 = 0; (l0 < 3); l0 = (l0 + 1)) {
            fRec2[l0] = 0.0;
        }
        for (int l1 = 0; (l1 < 3); l1 = (l1 + 1)) {
            fRec1[l1] = 0.0;
        }
        for (int l2 = 0; (l2 < 3); l2 = (l2 + 1)) {
            fRec0[l2] = 0.0;
        }
    }
    
    inline void compute(int count, FAUSTFLOAT* input0, FAUSTFLOAT* output0)
    {
        // Computed at template specialization time
        REAL fSlow0 = std::tan((3.1415926535897931 * (REAL(fVslider0::value()) / REAL(fVslider1))));
        REAL fSlow1 = (1.0 / fSlow0);
        REAL fSlow2 = (1.0 / (((fSlow1 + 0.16840487111358901) / fSlow0) + 1.0693584077073119));
        REAL fSlow3 = LowPass6e_faustpower2_f(fSlow0);
        REAL fSlow4 = (1.0 / fSlow3);
        REAL fSlow5 = (fSlow4 + 53.536152954556727);
        REAL fSlow6 = (1.0 / (((fSlow1 + 0.51247864188914105) / fSlow0) + 0.68962136448467504));
        REAL fSlow7 = (fSlow4 + 7.6217312988706034);
        REAL fSlow8 = (1.0 / (((fSlow1 + 0.78241304682164503) / fSlow0) + 0.24529150870616001));
        REAL fSlow9 = (9.9999997054999994e-05 / fSlow3);
        REAL fSlow10 = (fSlow9 + 0.00043322720055500002);
        REAL fSlow11 = (((fSlow1 + -0.78241304682164503) / fSlow0) + 0.24529150870616001);
        REAL fSlow12 = (2.0 * (0.24529150870616001 - fSlow4));
        REAL fSlow13 = (2.0 * (0.00043322720055500002 - fSlow9));
        REAL fSlow14 = (((fSlow1 + -0.51247864188914105) / fSlow0) + 0.68962136448467504);
        REAL fSlow15 = (2.0 * (0.68962136448467504 - fSlow4));
        REAL fSlow16 = (2.0 * (7.6217312988706034 - fSlow4));
        REAL fSlow17 = (((fSlow1 + -0.16840487111358901) / fSlow0) + 1.0693584077073119);
        REAL fSlow18 = (2.0 * (1.0693584077073119 - fSlow4));
        REAL fSlow19 = (2.0 * (53.536152954556727 - fSlow4));
        // Computed at runtime
        for (int i = 0; (i < count); i = (i + 1)) {
            fRec2[0] = (REAL(input0[i]) - (fSlow8 * ((fSlow11 * fRec2[2]) + (fSlow12 * fRec2[1]))));
            fRec1[0] = ((fSlow8 * (((fSlow10 * fRec2[0]) + (fSlow13 * fRec2[1])) + (fSlow10 * fRec2[2]))) - (fSlow6 * ((fSlow14 * fRec1[2]) + (fSlow15 * fRec1[1]))));
            fRec0[0] = ((fSlow6 * (((fSlow7 * fRec1[0]) + (fSlow16 * fRec1[1])) + (fSlow7 * fRec1[2]))) - (fSlow2 * ((fSlow17 * fRec0[2]) + (fSlow18 * fRec0[1]))));
            output0[i] = FAUSTFLOAT((fSlow2 * (((fSlow5 * fRec0[0]) + (fSlow19 * fRec0[1])) + (fSlow5 * fRec0[2]))));
            fRec2[2] = fRec2[1];
            fRec2[1] = fRec2[0];
            fRec1[2] = fRec1[1];
            fRec1[1] = fRec1[0];
            fRec0[2] = fRec0[1];
            fRec0[1] = fRec0[0];
        }
    }
};

// A "si.bus(N)" like hard-coded class
struct dsp_bus : public dsp {
    
    int fChannels;
    int fSampleRate;
    
    dsp_bus(int channels):fChannels(channels), fSampleRate(-1)
    {}
    
    virtual int getNumInputs() { return fChannels; }
    virtual int getNumOutputs() { return fChannels; }
    
    virtual int getSampleRate() { return fSampleRate; }
    
    virtual void buildUserInterface(UI* ui_interface) {}
    virtual void init(int sample_rate)
    {
        //classInit(sample_rate);
        instanceInit(sample_rate);
    }
    
    virtual void instanceInit(int sample_rate)
    {
        fSampleRate = sample_rate;
        instanceConstants(sample_rate);
        instanceResetUserInterface();
        instanceClear();
    }
    
    virtual void instanceConstants(int sample_rate) {}
    virtual void instanceResetUserInterface() {}
    virtual void instanceClear() {}
    
    virtual dsp* clone() { return new dsp_bus(fChannels); }
    
    virtual void metadata(Meta* m) {}
    
    virtual void compute(int count, FAUSTFLOAT** inputs, FAUSTFLOAT** outputs)
    {
        for (int chan = 0; chan < fChannels; chan++) {
            memcpy(outputs[chan], inputs[chan], sizeof(FAUSTFLOAT) * count);
        }
    }
    
    virtual void compute(double /*date_usec*/, int count, FAUSTFLOAT** inputs, FAUSTFLOAT** outputs)
    {
        compute(count, inputs, outputs);
    }
    
};

// Base class for sample-rate adapter
template <typename FILTER>
class sr_sampler : public decorator_dsp {
    
    protected:
    
        std::vector<FILTER> fInputLowPass;
        std::vector<FILTER> fOutputLowPass;
    
        inline int getFactor() { return this->fOutputLowPass[0].getFactor(); }
    
    public:
    
        sr_sampler(dsp* dsp):decorator_dsp(dsp)
        {
            for (int chan = 0; chan < fDSP->getNumInputs(); chan++) {
                fInputLowPass.push_back(FILTER());
            }
            for (int chan = 0; chan < fDSP->getNumOutputs(); chan++) {
                fOutputLowPass.push_back(FILTER());
            }
        }
};

// Down sample-rate adapter
template <typename FILTER>
class dsp_down_sampler : public sr_sampler<FILTER> {
    
    public:
    
        dsp_down_sampler(dsp* dsp):sr_sampler<FILTER>(dsp)
        {}
    
        virtual void init(int sample_rate)
        {
            this->fDSP->init(sample_rate / this->getFactor());
        }
    
        virtual void instanceInit(int sample_rate)
        {
            this->fDSP->instanceInit(sample_rate / this->getFactor());
        }
    
        virtual void instanceConstants(int sample_rate)
        {
            this->fDSP->instanceConstants(sample_rate / this->getFactor());
        }
    
        virtual dsp_down_sampler* clone() { return new dsp_down_sampler(decorator_dsp::clone()); }
    
        virtual void compute(int count, FAUSTFLOAT** inputs, FAUSTFLOAT** outputs)
        {
            int real_count = count / this->getFactor();
            
            // Adapt inputs
            FAUSTFLOAT** fInputs = (FAUSTFLOAT**)alloca(this->fDSP->getNumInputs() * sizeof(FAUSTFLOAT*));
            for (int chan = 0; chan < this->fDSP->getNumInputs(); chan++) {
                // Lowpass filtering in place on 'inputs'
                this->fInputLowPass[chan].compute(count, inputs[chan], inputs[chan]);
                // Allocate fInputs with 'real_count' frames
                fInputs[chan] = (FAUSTFLOAT*)alloca(sizeof(FAUSTFLOAT) * real_count);
                // Decimate
                for (int frame = 0; frame < real_count; frame++) {
                    fInputs[chan][frame] = inputs[chan][frame * this->getFactor()];
                }
            }
            
            // Allocate fOutputs with 'real_count' frames
            FAUSTFLOAT** fOutputs = (FAUSTFLOAT**)alloca(this->fDSP->getNumOutputs() * sizeof(FAUSTFLOAT*));
            for (int chan = 0; chan < this->fDSP->getNumOutputs(); chan++) {
                fOutputs[chan] = (FAUSTFLOAT*)alloca(sizeof(FAUSTFLOAT) * real_count);
            }
            
            // Compute at lower rate
            this->fDSP->compute(real_count, fInputs, fOutputs);
            
            // Adapt outputs
            for (int chan = 0; chan < this->fDSP->getNumOutputs(); chan++) {
                // Puts zeros
                memset(outputs[chan], 0, sizeof(FAUSTFLOAT) * count);
                for (int frame = 0; frame < real_count; frame++) {
                    // Copy one sample every 'DownFactor'
                    // Apply volume
                    //outputs[chan][frame * this->getFactor()] = fOutputs[chan][frame] * this->getFactor();
                    outputs[chan][frame * this->getFactor()] = fOutputs[chan][frame];
                }
                // Lowpass filtering in place on 'outputs'
                this->fOutputLowPass[chan].compute(count, outputs[chan], outputs[chan]);
            }
        }
    
        virtual void compute(double /*date_usec*/, int count, FAUSTFLOAT** inputs, FAUSTFLOAT** outputs) { compute(count, inputs, outputs); }
};

// Up sample-rate adapter
template <typename FILTER>
class dsp_up_sampler : public sr_sampler<FILTER> {
    
    public:
    
        dsp_up_sampler(dsp* dsp):sr_sampler<FILTER>(dsp)
        {}
    
        virtual void init(int sample_rate)
        {
            this->fDSP->init(sample_rate * this->getFactor());
        }
    
        virtual void instanceInit(int sample_rate)
        {
            this->fDSP->instanceInit(sample_rate * this->getFactor());
        }
    
        virtual void instanceConstants(int sample_rate)
        {
            this->fDSP->instanceConstants(sample_rate * this->getFactor());
        }
    
        virtual dsp_up_sampler* clone() { return new dsp_up_sampler(decorator_dsp::clone()); }
    
        virtual void compute(int count, FAUSTFLOAT** inputs, FAUSTFLOAT** outputs)
        {
            int real_count = count * this->getFactor();
            
            // Adapt inputs
            FAUSTFLOAT** fInputs = (FAUSTFLOAT**)alloca(this->fDSP->getNumInputs() * sizeof(FAUSTFLOAT*));
            
            for (int chan = 0; chan < this->fDSP->getNumInputs(); chan++) {
                // Allocate fInputs with 'real_count' frames
                fInputs[chan] = (FAUSTFLOAT*)alloca(sizeof(FAUSTFLOAT) * real_count);
                // Puts zeros
                memset(fInputs[chan], 0, sizeof(FAUSTFLOAT) * real_count);
                for (int frame = 0; frame < count; frame++) {
                    // Copy one sample every 'UpFactor'
                    fInputs[chan][frame * this->getFactor()] = inputs[chan][frame];
                }
                // Lowpass filtering in place on 'fInputs'
                this->fInputLowPass[chan].compute(real_count, fInputs[chan], fInputs[chan]);
            }
            
            // Allocate fOutputs with 'real_count' frames
            FAUSTFLOAT** fOutputs = (FAUSTFLOAT**)alloca(this->fDSP->getNumOutputs() * sizeof(FAUSTFLOAT*));
            
            for (int chan = 0; chan < this->fDSP->getNumOutputs(); chan++) {
                fOutputs[chan] = (FAUSTFLOAT*)alloca(sizeof(FAUSTFLOAT) * real_count);
            }
            
            // Compute at upper rate
            this->fDSP->compute(real_count, fInputs, fOutputs);
            
            // Adapt outputs
            for (int chan = 0; chan < this->fDSP->getNumOutputs(); chan++) {
                // Lowpass filtering in place on 'fOutputs'
                this->fOutputLowPass[chan].compute(real_count, fOutputs[chan], fOutputs[chan]);
                // Decimate
                for (int frame = 0; frame < count; frame++) {
                    // Apply volume
                    //outputs[chan][frame] = fOutputs[chan][frame * this->getFactor()] * this->getFactor();
                    outputs[chan][frame] = fOutputs[chan][frame * this->getFactor()];
                }
            }
        }
    
        virtual void compute(double /*date_usec*/, int count, FAUSTFLOAT** inputs, FAUSTFLOAT** outputs) { compute(count, inputs, outputs); }
};

// Create a UP/DS + Filter adapted DSP
template <typename REAL>
dsp* createSRAdapter(dsp* DSP, std::string& error, int ds = 0, int us = 0, int filter = 0)
{
    if (ds >= 2) {
        switch (filter) {
            case 0:
                if (ds == 2) {
                    return new dsp_down_sampler<Identity<Double<1,1>, 2>>(DSP);
                } else if (ds == 3) {
                    return new dsp_down_sampler<Identity<Double<1,1>, 3>>(DSP);
                } else if (ds == 4) {
                    return new dsp_down_sampler<Identity<Double<1,1>, 4>>(DSP);
                } else if (ds == 8) {
                    return new dsp_down_sampler<Identity<Double<1,1>, 8>>(DSP);
                } else if (ds == 16) {
                    return new dsp_down_sampler<Identity<Double<1,1>, 16>>(DSP);
                } else if (ds == 32) {
                    return new dsp_down_sampler<Identity<Double<1,1>, 32>>(DSP);
                } else {
                    error = "ERROR : ds factor type must be in [2..32] range\n";
                    return nullptr;
                }
            case 1:
                if (ds == 2) {
                    return new dsp_down_sampler<LowPass3<Double<45,100>, 2, REAL>>(DSP);
                } else if (ds == 3) {
                    return new dsp_down_sampler<LowPass3<Double<45,100>, 3, REAL>>(DSP);
                } else if (ds == 4) {
                    return new dsp_down_sampler<LowPass3<Double<45,100>, 4, REAL>>(DSP);
                } else if (ds == 8) {
                    return new dsp_down_sampler<LowPass3<Double<45,100>, 8, REAL>>(DSP);
                } else if (ds == 16) {
                    return new dsp_down_sampler<LowPass3<Double<45,100>, 16, REAL>>(DSP);
                } else if (ds == 32) {
                    return new dsp_down_sampler<LowPass3<Double<45,100>, 32, REAL>>(DSP);
                } else {
                    error = "ERROR : ds factor type must be in [2..32] range\n";
                    return nullptr;
                }
            case 2:
                if (ds == 2) {
                    return new dsp_down_sampler<LowPass4<Double<45,100>, 2, REAL>>(DSP);
                } else if (ds == 3) {
                    return new dsp_down_sampler<LowPass4<Double<45,100>, 3, REAL>>(DSP);
                } else if (ds == 4) {
                    return new dsp_down_sampler<LowPass4<Double<45,100>, 4, REAL>>(DSP);
                } else if (ds == 8) {
                    return new dsp_down_sampler<LowPass4<Double<45,100>, 8, REAL>>(DSP);
                } else if (ds == 16) {
                    return new dsp_down_sampler<LowPass4<Double<45,100>, 16, REAL>>(DSP);
                } else if (ds == 32) {
                    return new dsp_down_sampler<LowPass4<Double<45,100>, 32, REAL>>(DSP);
                } else {
                    error = "ERROR : ds factor type must be in [2..32] range\n";
                    return nullptr;
                }
            case 3:
                if (ds == 2) {
                    return new dsp_down_sampler<LowPass3e<Double<45,100>, 2, REAL>>(DSP);
                } else if (ds == 3) {
                    return new dsp_down_sampler<LowPass3e<Double<45,100>, 3, REAL>>(DSP);
                } else if (ds == 4) {
                    return new dsp_down_sampler<LowPass3e<Double<45,100>, 4, REAL>>(DSP);
                } else if (ds == 8) {
                    return new dsp_down_sampler<LowPass3e<Double<45,100>, 8, REAL>>(DSP);
                } else if (ds == 16) {
                    return new dsp_down_sampler<LowPass3e<Double<45,100>, 16, REAL>>(DSP);
                } else if (ds == 32) {
                    return new dsp_down_sampler<LowPass3e<Double<45,100>, 32, REAL>>(DSP);
                } else {
                    error = "ERROR : ds factor type must be in [2..32] range\n";
                    return nullptr;
                }
            case 4:
                if (ds == 2) {
                    return new dsp_down_sampler<LowPass6e<Double<45,100>, 2, REAL>>(DSP);
                } else if (ds == 3) {
                    return new dsp_down_sampler<LowPass6e<Double<45,100>, 3, REAL>>(DSP);
                } else if (ds == 4) {
                    return new dsp_down_sampler<LowPass6e<Double<45,100>, 4, REAL>>(DSP);
                } else if (ds == 8) {
                    return new dsp_down_sampler<LowPass6e<Double<45,100>, 8, REAL>>(DSP);
                } else if (ds == 16) {
                    return new dsp_down_sampler<LowPass6e<Double<45,100>, 16, REAL>>(DSP);
                } else if (ds == 32) {
                    return new dsp_down_sampler<LowPass6e<Double<45,100>, 32, REAL>>(DSP);
                } else {
                    error = "ERROR : ds factor type must be in [2..32] range\n";
                    return nullptr;
                }
            default:
                error = "ERROR : filter type must be in [0..4] range\n";
                return nullptr;
        }
    } else if (us >= 2) {
        
        switch (filter) {
            case 0:
                if (us == 2) {
                    return new dsp_up_sampler<Identity<Double<1,1>, 2>>(DSP);
                } else if (us == 3) {
                    return new dsp_up_sampler<Identity<Double<1,1>, 3>>(DSP);
                } else if (us == 4) {
                    return new dsp_up_sampler<Identity<Double<1,1>, 4>>(DSP);
                } else if (us == 8) {
                    return new dsp_up_sampler<Identity<Double<1,1>, 8>>(DSP);
                } else if (us == 16) {
                    return new dsp_up_sampler<Identity<Double<1,1>, 16>>(DSP);
                } else if (us == 32) {
                    return new dsp_up_sampler<Identity<Double<1,1>, 32>>(DSP);
                } else {
                    error = "ERROR : us factor type must be in [2..32] range\n";
                    return nullptr;
                }
            case 1:
                if (us == 2) {
                    return new dsp_up_sampler<LowPass3<Double<45,100>, 2, REAL>>(DSP);
                } else if (us == 3) {
                    return new dsp_up_sampler<LowPass3<Double<45,100>, 3, REAL>>(DSP);
                } else if (us == 4) {
                    return new dsp_up_sampler<LowPass3<Double<45,100>, 4, REAL>>(DSP);
                } else if (us == 8) {
                    return new dsp_up_sampler<LowPass3<Double<45,100>, 8, REAL>>(DSP);
                } else if (us == 16) {
                    return new dsp_up_sampler<LowPass3<Double<45,100>, 16, REAL>>(DSP);
                } else if (us == 32) {
                    return new dsp_up_sampler<LowPass3<Double<45,100>, 32, REAL>>(DSP);
                } else {
                    error = "ERROR : us factor type must be in [2..32] range\n";
                    return nullptr;
                }
            case 2:
                if (us == 2) {
                    return new dsp_up_sampler<LowPass4<Double<45,100>, 2, REAL>>(DSP);
                } else if (us == 3) {
                    return new dsp_up_sampler<LowPass4<Double<45,100>, 3, REAL>>(DSP);
                } else if (us == 4) {
                    return new dsp_up_sampler<LowPass4<Double<45,100>, 4, REAL>>(DSP);
                } else if (us == 8) {
                    return new dsp_up_sampler<LowPass4<Double<45,100>, 8, REAL>>(DSP);
                } else if (us == 16) {
                    return new dsp_up_sampler<LowPass4<Double<45,100>, 16, REAL>>(DSP);
                } else if (us == 32) {
                    return new dsp_up_sampler<LowPass4<Double<45,100>, 32, REAL>>(DSP);
                } else {
                    error = "ERROR : us factor type must be in [2..32] range\n";
                    return nullptr;
                }
            case 3:
                if (us == 2) {
                    return new dsp_up_sampler<LowPass3e<Double<45,100>, 2, REAL>>(DSP);
                } else if (us == 3) {
                    return new dsp_up_sampler<LowPass3e<Double<45,100>, 3, REAL>>(DSP);
                } else if (us == 4) {
                    return new dsp_up_sampler<LowPass3e<Double<45,100>, 4, REAL>>(DSP);
                } else if (us == 8) {
                    return new dsp_up_sampler<LowPass3e<Double<45,100>, 8, REAL>>(DSP);
                } else if (us == 16) {
                    return new dsp_up_sampler<LowPass3e<Double<45,100>, 16, REAL>>(DSP);
                } else if (us == 32) {
                    return new dsp_up_sampler<LowPass3e<Double<45,100>, 32, REAL>>(DSP);
                } else {
                    error = "ERROR : us factor type must be in [2..32] range\n";
                    return nullptr;
                }
            case 4:
                if (us == 2) {
                    return new dsp_up_sampler<LowPass6e<Double<45,100>, 2, REAL>>(DSP);
                } else if (us == 3) {
                    return new dsp_up_sampler<LowPass6e<Double<45,100>, 3, REAL>>(DSP);
                } else if (us == 4) {
                    return new dsp_up_sampler<LowPass6e<Double<45,100>, 4, REAL>>(DSP);
                } else if (us == 8) {
                    return new dsp_up_sampler<LowPass6e<Double<45,100>, 8, REAL>>(DSP);
                } else if (us == 16) {
                    return new dsp_up_sampler<LowPass6e<Double<45,100>, 16, REAL>>(DSP);
                } else if (us == 32) {
                    return new dsp_up_sampler<LowPass6e<Double<45,100>, 32, REAL>>(DSP);
                } else {
                    error = "ERROR : us factor type must be in [2..32] range\n";
                    return nullptr;
                }
            default:
                error = "ERROR : filter type must be in [0..4] range\n";
                return nullptr;
        }
    } else {
        return DSP;
    }
}
    
#endif
/************************** END dsp-adapter.h **************************/
/************************** BEGIN misc.h *******************************
FAUST Architecture File
Copyright (C) 2003-2022 GRAME, Centre National de Creation Musicale
---------------------------------------------------------------------
This program is free software; you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation; either version 2.1 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

EXCEPTION : As a special exception, you may create a larger work
that contains this FAUST architecture section and distribute
that work under terms of your choice, so long as this FAUST
architecture section is not modified.
***************************************************************************/

#ifndef __misc__
#define __misc__

#include <algorithm>
#include <map>
#include <cstdlib>
#include <string.h>
#include <fstream>
#include <string>

/************************** BEGIN meta.h *******************************
 FAUST Architecture File
 Copyright (C) 2003-2022 GRAME, Centre National de Creation Musicale
 ---------------------------------------------------------------------
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation; either version 2.1 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 
 EXCEPTION : As a special exception, you may create a larger work
 that contains this FAUST architecture section and distribute
 that work under terms of your choice, so long as this FAUST
 architecture section is not modified.
 ************************************************************************/

#ifndef __meta__
#define __meta__


/**
 The base class of Meta handler to be used in dsp::metadata(Meta* m) method to retrieve (key, value) metadata.
 */
struct FAUST_API Meta {
    virtual ~Meta() {}
    virtual void declare(const char* key, const char* value) = 0;
};

#endif
/**************************  END  meta.h **************************/

struct MY_Meta : Meta, std::map<const char*, const char*>
{
    void declare(const char* key, const char* value) { (*this)[key] = value; }
};

static int lsr(int x, int n) { return int(((unsigned int)x) >> n); }

static int int2pow2(int x) { int r = 0; while ((1<<r) < x) r++; return r; }

static long lopt(char* argv[], const char* name, long def)
{
    for (int i = 0; argv[i]; i++) if (!strcmp(argv[i], name)) return std::atoi(argv[i+1]);
    return def;
}

static long lopt1(int argc, char* argv[], const char* longname, const char* shortname, long def)
{
    for (int i = 2; i < argc; i++) {
        if (strcmp(argv[i-1], shortname) == 0 || strcmp(argv[i-1], longname) == 0) {
            return atoi(argv[i]);
        }
    }
    return def;
}

static const char* lopts(char* argv[], const char* name, const char* def)
{
    for (int i = 0; argv[i]; i++) if (!strcmp(argv[i], name)) return argv[i+1];
    return def;
}

static const char* lopts1(int argc, char* argv[], const char* longname, const char* shortname, const char* def)
{
    for (int i = 2; i < argc; i++) {
        if (strcmp(argv[i-1], shortname) == 0 || strcmp(argv[i-1], longname) == 0) {
            return argv[i];
        }
    }
    return def;
}

static bool isopt(char* argv[], const char* name)
{
    for (int i = 0; argv[i]; i++) if (!strcmp(argv[i], name)) return true;
    return false;
}

static std::string pathToContent(const std::string& path)
{
    std::ifstream file(path.c_str(), std::ifstream::binary);
    
    file.seekg(0, file.end);
    int size = int(file.tellg());
    file.seekg(0, file.beg);
    
    // And allocate buffer to that a single line can be read...
    char* buffer = new char[size + 1];
    file.read(buffer, size);
    
    // Terminate the string
    buffer[size] = 0;
    std::string result = buffer;
    file.close();
    delete [] buffer;
    return result;
}

#endif

/**************************  END  misc.h **************************/
/************************** BEGIN SaveUI.h **************************
 FAUST Architecture File
 Copyright (C) 2003-2022 GRAME, Centre National de Creation Musicale
 ---------------------------------------------------------------------
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation; either version 2.1 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 
 EXCEPTION : As a special exception, you may create a larger work
 that contains this FAUST architecture section and distribute
 that work under terms of your choice, so long as this FAUST
 architecture section is not modified.
 ********************************************************************/

#ifndef FAUST_SAVEUI_H
#define FAUST_SAVEUI_H

/************************** BEGIN DecoratorUI.h **************************
 FAUST Architecture File
Copyright (C) 2003-2022 GRAME, Centre National de Creation Musicale
---------------------------------------------------------------------
This program is free software; you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation; either version 2.1 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

EXCEPTION : As a special exception, you may create a larger work
that contains this FAUST architecture section and distribute
that work under terms of your choice, so long as this FAUST
architecture section is not modified.
*************************************************************************/

#ifndef Decorator_UI_H
#define Decorator_UI_H


//----------------------------------------------------------------
//  Generic UI empty implementation
//----------------------------------------------------------------

class FAUST_API GenericUI : public UI
{
    
    public:
        
        GenericUI() {}
        virtual ~GenericUI() {}
        
        // -- widget's layouts
        virtual void openTabBox(const char* label) {}
        virtual void openHorizontalBox(const char* label) {}
        virtual void openVerticalBox(const char* label) {}
        virtual void closeBox() {}
        
        // -- active widgets
        virtual void addButton(const char* label, FAUSTFLOAT* zone) {}
        virtual void addCheckButton(const char* label, FAUSTFLOAT* zone) {}
        virtual void addVerticalSlider(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step) {}
        virtual void addHorizontalSlider(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step) {}
        virtual void addNumEntry(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step) {}
    
        // -- passive widgets
        virtual void addHorizontalBargraph(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT min, FAUSTFLOAT max) {}
        virtual void addVerticalBargraph(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT min, FAUSTFLOAT max) {}
    
        // -- soundfiles
        virtual void addSoundfile(const char* label, const char* soundpath, Soundfile** sf_zone) {}
    
        virtual void declare(FAUSTFLOAT* zone, const char* key, const char* val) {}
    
};

//----------------------------------------------------------------
//  Generic UI decorator
//----------------------------------------------------------------

class FAUST_API DecoratorUI : public UI
{
    
    protected:
        
        UI* fUI;
        
    public:
        
        DecoratorUI(UI* ui = 0):fUI(ui) {}
        virtual ~DecoratorUI() { delete fUI; }
        
        // -- widget's layouts
        virtual void openTabBox(const char* label)          { fUI->openTabBox(label); }
        virtual void openHorizontalBox(const char* label)   { fUI->openHorizontalBox(label); }
        virtual void openVerticalBox(const char* label)     { fUI->openVerticalBox(label); }
        virtual void closeBox()                             { fUI->closeBox(); }
        
        // -- active widgets
        virtual void addButton(const char* label, FAUSTFLOAT* zone)         { fUI->addButton(label, zone); }
        virtual void addCheckButton(const char* label, FAUSTFLOAT* zone)    { fUI->addCheckButton(label, zone); }
        virtual void addVerticalSlider(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step)
        { fUI->addVerticalSlider(label, zone, init, min, max, step); }
        virtual void addHorizontalSlider(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step)
        { fUI->addHorizontalSlider(label, zone, init, min, max, step); }
        virtual void addNumEntry(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step)
        { fUI->addNumEntry(label, zone, init, min, max, step); }
        
        // -- passive widgets
        virtual void addHorizontalBargraph(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT min, FAUSTFLOAT max)
        { fUI->addHorizontalBargraph(label, zone, min, max); }
        virtual void addVerticalBargraph(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT min, FAUSTFLOAT max)
        { fUI->addVerticalBargraph(label, zone, min, max); }
    
        // -- soundfiles
        virtual void addSoundfile(const char* label, const char* filename, Soundfile** sf_zone) { fUI->addSoundfile(label, filename, sf_zone); }
    
        virtual void declare(FAUSTFLOAT* zone, const char* key, const char* val) { fUI->declare(zone, key, val); }
    
};

// Defined here to simplify header #include inclusion 
class FAUST_API SoundUIInterface : public GenericUI {};

#endif
/**************************  END  DecoratorUI.h **************************/

// Base class to handle controllers state save/load

class SaveUI : public GenericUI {

    protected:
    
        struct SavedZone {
            FAUSTFLOAT* fZone;
            FAUSTFLOAT fCurrent;
            FAUSTFLOAT fInit;
            
            SavedZone():fZone(nullptr), fCurrent(FAUSTFLOAT(0)), fInit(FAUSTFLOAT(0))
            {}
            SavedZone(FAUSTFLOAT* zone, FAUSTFLOAT current, FAUSTFLOAT init)
            :fZone(zone), fCurrent(current), fInit(init)
            {
                *fZone = current;
            }
            ~SavedZone()
            {}
        };
        
        std::map<std::string, SavedZone> fName2Zone;
    
        virtual void addItem(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init) = 0;
    
    public:
    
        SaveUI() {}
        virtual ~SaveUI() {}
    
        void addButton(const char* label, FAUSTFLOAT* zone)
        {
            addItem(label, zone, FAUSTFLOAT(0));
        }
        void addCheckButton(const char* label, FAUSTFLOAT* zone)
        {
            addItem(label, zone, FAUSTFLOAT(0));
        }
        void addVerticalSlider(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step)
        {
            addItem(label, zone, init);
        }
        void addHorizontalSlider(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step)
        {
            addItem(label, zone, init);
        }
        void addNumEntry(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step)
        {
            addItem(label, zone, init);
        }

        void reset()
        {
            for (const auto& it : fName2Zone) {
                *it.second.fZone = it.second.fInit;
            }
        }
        
        void display()
        {
            for (const auto& it : fName2Zone) {
                std::cout << "SaveUI::display path = " << it.first << " value = " << *it.second.fZone << std::endl;
            }
        }
        
        void save()
        {
            for (auto& it : fName2Zone) {
                it.second.fCurrent = *it.second.fZone;
            }
        }
};

/*
 Save/load current value using the label, reset to init value
 */

class SaveLabelUI : public SaveUI {
    
    protected:
    
        void addItem(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init)
        {
            if (fName2Zone.find(label) != fName2Zone.end()) {
                FAUSTFLOAT current = fName2Zone[label].fCurrent;
                fName2Zone[label] = SavedZone(zone, current, init);
            } else {
                fName2Zone[label] = SavedZone(zone, init, init);
            }
        }
        
    public:
        
        SaveLabelUI() : SaveUI() {}
        virtual ~SaveLabelUI() {}        
   
};

/*
 Save/load current value using the complete path, reset to init value
*/

class SavePathUI : public SaveUI, public PathBuilder {
    
    protected:
    
        void openTabBox(const char* label) { pushLabel(label); }
        void openHorizontalBox(const char* label) { pushLabel(label);; }
        void openVerticalBox(const char* label) { pushLabel(label); }
        void closeBox() { popLabel(); };
    
        void addItem(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init)
        {
            std::string path = buildPath(label);
            if (fName2Zone.find(path) != fName2Zone.end()) {
                FAUSTFLOAT current = fName2Zone[path].fCurrent;
                fName2Zone[path] = SavedZone(zone, current, init);
            } else {
                fName2Zone[path] = SavedZone(zone, init, init);
            }
        }
   
    public:

        SavePathUI(): SaveUI() {}
        virtual ~SavePathUI() {}

};

#endif

/**************************  END  SaveUI.h **************************/

// Always included
/************************** BEGIN OSCUI.h **************************
 FAUST Architecture File
 Copyright (C) 2003-2022 GRAME, Centre National de Creation Musicale
 ---------------------------------------------------------------------
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation; either version 2.1 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 
 EXCEPTION : As a special exception, you may create a larger work
 that contains this FAUST architecture section and distribute
 that work under terms of your choice, so long as this FAUST
 architecture section is not modified.
 *********************************************************************/

#ifndef __OSCUI__
#define __OSCUI__

#include <vector>
#include <string>

/*

  Faust Project

  Copyright (C) 2011 Grame

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

  Grame Research Laboratory, 9 rue du Garet, 69001 Lyon - France
  research@grame.fr

*/

#ifndef __OSCControler__
#define __OSCControler__

#include <string>
/*

  Copyright (C) 2011 Grame

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

  Grame Research Laboratory, 9 rue du Garet, 69001 Lyon - France
  research@grame.fr

*/

#ifndef __FaustFactory__
#define __FaustFactory__

#include <stack>
#include <string>
#include <sstream>

/*

  Copyright (C) 2011 Grame

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

  Grame Research Laboratory, 9 rue du Garet, 69001 Lyon - France
  research@grame.fr

*/

#ifndef __FaustNode__
#define __FaustNode__

#include <string>
#include <vector>

/*

  Copyright (C) 2011 Grame

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

  Grame Research Laboratory, 9 rue du Garet, 69001 Lyon - France
  research@grame.fr

*/

#ifndef __MessageDriven__
#define __MessageDriven__

#include <string>
#include <vector>

/*

  Copyright (C) 2010  Grame

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

  Grame Research Laboratory, 9 rue du Garet, 69001 Lyon - France
  research@grame.fr

*/

#ifndef __MessageProcessor__
#define __MessageProcessor__

namespace oscfaust
{

class Message;
//--------------------------------------------------------------------------
/*!
	\brief an abstract class for objects able to process OSC messages	
*/
class MessageProcessor
{
	public:
		virtual		~MessageProcessor() {}
		virtual void processMessage( const Message* msg ) = 0;
};

} // end namespoace

#endif
/*

  Copyright (C) 2011 Grame

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

  Grame Research Laboratory, 9 rue du Garet, 69001 Lyon - France
  research@grame.fr

*/

#ifndef __smartpointer__
#define __smartpointer__

#include <cassert>

namespace oscfaust
{

/*!
\brief the base class for smart pointers implementation

	Any object that want to support smart pointers should
	inherit from the smartable class which provides reference counting
	and automatic delete when the reference count drops to zero.
*/
class smartable {
	private:
		unsigned 	refCount;		
	public:
		//! gives the reference count of the object
		unsigned refs() const         { return refCount; }
		//! addReference increments the ref count and checks for refCount overflow
		void addReference()           { refCount++; assert(refCount != 0); }
		//! removeReference delete the object when refCount is zero		
		void removeReference()		  { if (--refCount == 0) delete this; }
		
	protected:
		smartable() : refCount(0) {}
		smartable(const smartable&): refCount(0) {}
		//! destructor checks for non-zero refCount
		virtual ~smartable()    
        { 
            /* 
                See "Static SFaustNode create (const char* name, C* zone, C init, C min, C max, const char* prefix, GUI* ui)" comment.
                assert (refCount == 0); 
            */
        }
		smartable& operator=(const smartable&) { return *this; }
};

/*!
\brief the smart pointer implementation

	A smart pointer is in charge of maintaining the objects reference count 
	by the way of pointers operators overloading. It supports class 
	inheritance and conversion whenever possible.
\n	Instances of the SMARTP class are supposed to use \e smartable types (or at least
	objects that implements the \e addReference and \e removeReference
	methods in a consistent way).
*/
template<class T> class SMARTP {
	private:
		//! the actual pointer to the class
		T* fSmartPtr;

	public:
		//! an empty constructor - points to null
		SMARTP()	: fSmartPtr(0) {}
		//! build a smart pointer from a class pointer
		SMARTP(T* rawptr) : fSmartPtr(rawptr)              { if (fSmartPtr) fSmartPtr->addReference(); }
		//! build a smart pointer from an convertible class reference
		template<class T2> 
		SMARTP(const SMARTP<T2>& ptr) : fSmartPtr((T*)ptr) { if (fSmartPtr) fSmartPtr->addReference(); }
		//! build a smart pointer from another smart pointer reference
		SMARTP(const SMARTP& ptr) : fSmartPtr((T*)ptr)     { if (fSmartPtr) fSmartPtr->addReference(); }

		//! the smart pointer destructor: simply removes one reference count
		~SMARTP()  { if (fSmartPtr) fSmartPtr->removeReference(); }
		
		//! cast operator to retrieve the actual class pointer
		operator T*() const  { return fSmartPtr;	}

		//! '*' operator to access the actual class pointer
		T& operator*() const {
			// checks for null dereference
			assert (fSmartPtr != 0);
			return *fSmartPtr;
		}

		//! operator -> overloading to access the actual class pointer
		T* operator->() const	{ 
			// checks for null dereference
			assert (fSmartPtr != 0);
			return fSmartPtr;
		}

		//! operator = that moves the actual class pointer
		template <class T2>
		SMARTP& operator=(T2 p1_)	{ *this=(T*)p1_; return *this; }

		//! operator = that moves the actual class pointer
		SMARTP& operator=(T* p_)	{
			// check first that pointers differ
			if (fSmartPtr != p_) {
				// increments the ref count of the new pointer if not null
				if (p_ != 0) p_->addReference();
				// decrements the ref count of the old pointer if not null
				if (fSmartPtr != 0) fSmartPtr->removeReference();
				// and finally stores the new actual pointer
				fSmartPtr = p_;
			}
			return *this;
		}
		//! operator < to support SMARTP map with Visual C++
		bool operator<(const SMARTP<T>& p_)	const			  { return fSmartPtr < ((T *) p_); }
		//! operator = to support inherited class reference
		SMARTP& operator=(const SMARTP<T>& p_)                { return operator=((T *) p_); }
		//! dynamic cast support
		template<class T2> SMARTP& cast(T2* p_)               { return operator=(dynamic_cast<T*>(p_)); }
		//! dynamic cast support
		template<class T2> SMARTP& cast(const SMARTP<T2>& p_) { return operator=(dynamic_cast<T*>(p_)); }
};

}

#endif

namespace oscfaust
{

class Message;
class OSCRegexp;
class MessageDriven;
typedef class SMARTP<MessageDriven>	SMessageDriven;

//--------------------------------------------------------------------------
/*!
	\brief a base class for objects accepting OSC messages
	
	Message driven objects are hierarchically organized in a tree.
	They provides the necessary to dispatch an OSC message to its destination
	node, according to the message OSC address. 
	
	The principle of the dispatch is the following:
	- first the processMessage() method should be called on the top level node
	- next processMessage call propose 
*/
class MessageDriven : public MessageProcessor, public smartable
{
	std::string						fName;			///< the node name
	std::string						fOSCPrefix;		///< the node OSC address prefix (OSCAddress = fOSCPrefix + '/' + fName)
	std::vector<SMessageDriven>		fSubNodes;		///< the subnodes of the current node

	protected:
				 MessageDriven(const char *name, const char *oscprefix) : fName (name), fOSCPrefix(oscprefix) {}
		virtual ~MessageDriven() {}

	public:
		static SMessageDriven create(const char* name, const char *oscprefix)	{ return new MessageDriven(name, oscprefix); }

		/*!
			\brief OSC message processing method.
			\param msg the osc message to be processed
			The method should be called on the top level node.
		*/
		virtual void	processMessage(const Message* msg);

		/*!
			\brief propose an OSc message at a given hierarchy level.
			\param msg the osc message currently processed
			\param regexp a regular expression based on the osc address head
			\param addrTail the osc address tail
			
			The method first tries to match the regular expression with the object name. 
			When it matches:
			- it calls \c accept when \c addrTail is empty 
			- or it \c propose the message to its subnodes when \c addrTail is not empty. 
			  In this case a new \c regexp is computed with the head of \c addrTail and a new \c addrTail as well.
		*/
		virtual void	propose(const Message* msg, const OSCRegexp* regexp, const std::string& addrTail);

		/*!
			\brief accept an OSC message. 
			\param msg the osc message currently processed
			\return true when the message is processed by the node
			
			The method is called only for the destination nodes. The real message acceptance is the node 
			responsability and may depend on the message content.
		*/
		virtual bool	accept(const Message* msg);

		/*!
			\brief handler for the \c 'get' message
			\param ipdest the output message destination IP
			
			The \c 'get' message is supported by every node:
			- it is propagated to the subnodes until it reaches terminal nodes
			- a terminal node send its state on \c 'get' request to the IP address given as parameter.
			The \c get method is basically called by the accept method.
		*/
		virtual void	get(unsigned long ipdest) const;

		/*!
			\brief handler for the \c 'get' 'attribute' message
			\param ipdest the output message destination IP
			\param what the requested attribute
			
			The \c 'get' message is supported by every node:
			- it is propagated to the subnodes until it reaches terminal nodes
			- a terminal node send its state on \c 'get' request to the IP address given as parameter.
			The \c get method is basically called by the accept method.
		*/
		virtual void	get(unsigned long ipdest, const std::string& what) const {}

		void			add(SMessageDriven node)	{ fSubNodes.push_back (node); }
		const char*		getName() const				{ return fName.c_str(); }
		std::string		getOSCAddress() const;
		int				size() const				{ return (int)fSubNodes.size (); }
		
		const std::string&	name() const			{ return fName; }
		SMessageDriven	subnode(int i)              { return fSubNodes[i]; }
};

} // end namespoace

#endif
/*

  Copyright (C) 2011  Grame

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

  Grame Research Laboratory, 9 rue du Garet, 69001 Lyon - France
  research@grame.fr

*/


#ifndef __Message__
#define __Message__

#include <string>
#include <vector>

namespace oscfaust
{

class OSCStream;
template <typename T> class MsgParam;
class baseparam;
typedef SMARTP<baseparam>	Sbaseparam;

//--------------------------------------------------------------------------
/*!
	\brief base class of a message parameters
*/
class baseparam : public smartable
{
	public:
		virtual ~baseparam() {}

		/*!
		 \brief utility for parameter type checking
		*/
		template<typename X> bool isType() const { return dynamic_cast<const MsgParam<X>*> (this) != 0; }
		/*!
		 \brief utility for parameter convertion
		 \param errvalue the returned value when no conversion applies
		 \return the parameter value when the type matches
		*/
		template<typename X> X	value(X errvalue) const 
			{ const MsgParam<X>* o = dynamic_cast<const MsgParam<X>*> (this); return o ? o->getValue() : errvalue; }
		/*!
		 \brief utility for parameter comparison
		*/
		template<typename X> bool	equal(const baseparam& p) const 
			{ 
				const MsgParam<X>* a = dynamic_cast<const MsgParam<X>*> (this); 
				const MsgParam<X>* b = dynamic_cast<const MsgParam<X>*> (&p);
				return a && b && (a->getValue() == b->getValue());
			}
		/*!
		 \brief utility for parameter comparison
		*/
		bool operator==(const baseparam& p) const 
			{ 
				return equal<float>(p) || equal<int>(p) || equal<std::string>(p);
			}
		bool operator!=(const baseparam& p) const
			{ 
				return !equal<float>(p) && !equal<int>(p) && !equal<std::string>(p);
			}
			
		virtual SMARTP<baseparam> copy() const = 0;
};

//--------------------------------------------------------------------------
/*!
	\brief template for a message parameter
*/
template <typename T> class MsgParam : public baseparam
{
	T fParam;
	public:
				 MsgParam(T val) : fParam(val)	{}
		virtual ~MsgParam() {}
		
		T getValue() const { return fParam; }
		
		virtual Sbaseparam copy() const { return new MsgParam<T>(fParam); }
};

//--------------------------------------------------------------------------
/*!
	\brief a message description
	
	A message is composed of an address (actually an OSC address),
	a message string that may be viewed as a method name
	and a list of message parameters.
*/
class Message
{
    public:
        typedef SMARTP<baseparam>		argPtr;		///< a message argument ptr type
        typedef std::vector<argPtr>		argslist;	///< args list type

    private:
        unsigned long	fSrcIP;			///< the message source IP number
        std::string	fAddress;			///< the message osc destination address
        std::string	fAlias;             ///< the message alias osc destination address
        argslist	fArguments;			///< the message arguments

    public:
            /*!
                \brief an empty message constructor
            */
             Message() {}
            /*!
                \brief a message constructor
                \param address the message destination address
            */
            Message(const std::string& address) : fAddress(address), fAlias("") {}
             
            Message(const std::string& address, const std::string& alias) : fAddress(address), fAlias(alias) {}
            /*!
                \brief a message constructor
                \param address the message destination address
                \param args the message parameters
            */
            Message(const std::string& address, const argslist& args) 
                : fAddress(address), fArguments(args) {}
            /*!
                \brief a message constructor
                \param msg a message
            */
             Message(const Message& msg);
    virtual ~Message() {} //{ freed++; std::cout << "running messages: " << (allocated - freed) << std::endl; }

    /*!
        \brief adds a parameter to the message
        \param val the parameter
    */
    template <typename T> void add(T val)	{ fArguments.push_back(new MsgParam<T>(val)); }
    /*!
        \brief adds a float parameter to the message
        \param val the parameter value
    */
    void	add(float val)					{ add<float>(val); }
    
    /*!
     \brief adds a double parameter to the message
     \param val the parameter value
     */
    void	add(double val)					{ add<double>(val); }
    
    /*!
        \brief adds an int parameter to the message
        \param val the parameter value
    */
    void	add(int val)					{ add<int>(val); }
    
    /*!
        \brief adds a string parameter to the message
        \param val the parameter value
    */
    void	add(const std::string& val)		{ add<std::string>(val); }

    /*!
        \brief adds a parameter to the message
        \param val the parameter
    */
    void	add(argPtr val)                 { fArguments.push_back( val ); }

    /*!
        \brief sets the message address
        \param addr the address
    */
    void				setSrcIP(unsigned long addr)		{ fSrcIP = addr; }

    /*!
        \brief sets the message address
        \param addr the address
    */
    void				setAddress(const std::string& addr)		{ fAddress = addr; }
    /*!
        \brief print the message
        \param out the output stream
    */
    void				print(std::ostream& out) const;
    /*!
        \brief send the message to OSC
        \param out the OSC output stream
    */
    void				print(OSCStream& out) const;
    /*!
        \brief print message arguments
        \param out the OSC output stream
    */
    void				printArgs(OSCStream& out) const;

    /// \brief gives the message address
    const std::string&	address() const		{ return fAddress; }
    /// \brief gives the message alias
    const std::string&	alias() const		{ return fAlias; }
    /// \brief gives the message parameters list
    const argslist&		params() const		{ return fArguments; }
    /// \brief gives the message parameters list
    argslist&			params()			{ return fArguments; }
    /// \brief gives the message source IP 
    unsigned long		src() const			{ return fSrcIP; }
    /// \brief gives the message parameters count
    int					size() const		{ return (int)fArguments.size(); }

    bool operator == (const Message& other) const;	

    /*!
        \brief gives a message float parameter
        \param i the parameter index (0 <= i < size())
        \param val on output: the parameter value when the parameter type matches
        \return false when types don't match
    */
    bool	param(int i, float& val) const		{ val = params()[i]->value<float>(val); return params()[i]->isType<float>(); }
    
    /*!
     \brief gives a message double parameter
     \param i the parameter index (0 <= i < size())
     \param val on output: the parameter value when the parameter type matches
     \return false when types don't match
     */
    bool	param(int i, double& val) const		{ val = params()[i]->value<double>(val); return params()[i]->isType<double>(); }
    
    /*!
        \brief gives a message int parameter
        \param i the parameter index (0 <= i < size())
        \param val on output: the parameter value when the parameter type matches
        \return false when types don't match
    */
    bool	param(int i, int& val) const		{ val = params()[i]->value<int>(val); return params()[i]->isType<int>(); }
    /*!
        \brief gives a message int parameter
        \param i the parameter index (0 <= i < size())
        \param val on output: the parameter value when the parameter type matches
        \return false when types don't match
    */
    bool	param(int i, unsigned int& val) const		{ val = params()[i]->value<int>(val); return params()[i]->isType<int>(); }
    /*!
        \brief gives a message int parameter
        \param i the parameter index (0 <= i < size())
        \param val on output: the parameter value when the parameter type matches
        \return false when types don't match
        \note a boolean value is handled as integer
    */
    bool	param(int i, bool& val) const		{ int ival = 0; ival = params()[i]->value<int>(ival); val = ival!=0; return params()[i]->isType<int>(); }
    /*!
        \brief gives a message int parameter
        \param i the parameter index (0 <= i < size())
        \param val on output: the parameter value when the parameter type matches
        \return false when types don't match
    */
    bool	param(int i, long int& val) const	{ val = long(params()[i]->value<int>(val)); return params()[i]->isType<int>(); }
    /*!
        \brief gives a message string parameter
        \param i the parameter index (0 <= i < size())
        \param val on output: the parameter value when the parameter type matches
        \return false when types don't match
    */
    bool	param(int i, std::string& val) const { val = params()[i]->value<std::string>(val); return params()[i]->isType<std::string>(); }
};


} // end namespoace

#endif
/************************** BEGIN GUI.h **********************************
 FAUST Architecture File
 Copyright (C) 2003-2022 GRAME, Centre National de Creation Musicale
 ---------------------------------------------------------------------
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation; either version 2.1 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 
 EXCEPTION : As a special exception, you may create a larger work
 that contains this FAUST architecture section and distribute
 that work under terms of your choice, so long as this FAUST
 architecture section is not modified.
 *************************************************************************/

#ifndef __GUI_H__
#define __GUI_H__

#include <list>
#include <map>
#include <vector>
#include <assert.h>

#ifdef _WIN32
# pragma warning (disable: 4100)
#else
# pragma GCC diagnostic ignored "-Wunused-parameter"
#endif

/************************** BEGIN ValueConverter.h ********************
 FAUST Architecture File
 Copyright (C) 2003-2022 GRAME, Centre National de Creation Musicale
 ---------------------------------------------------------------------
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation; either version 2.1 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 
 EXCEPTION : As a special exception, you may create a larger work
 that contains this FAUST architecture section and distribute
 that work under terms of your choice, so long as this FAUST
 architecture section is not modified.
 ********************************************************************/

#ifndef __ValueConverter__
#define __ValueConverter__

/***************************************************************************************
 ValueConverter.h
 (GRAME, Copyright 2015-2019)
 
 Set of conversion objects used to map user interface values (for example a gui slider
 delivering values between 0 and 1) to faust values (for example a vslider between
 20 and 20000) using a log scale.
 
 -- Utilities
 
 Range(lo,hi) : clip a value x between lo and hi
 Interpolator(lo,hi,v1,v2) : Maps a value x between lo and hi to a value y between v1 and v2
 Interpolator3pt(lo,mi,hi,v1,vm,v2) : Map values between lo mid hi to values between v1 vm v2
 
 -- Value Converters
 
 ValueConverter::ui2faust(x)
 ValueConverter::faust2ui(x)
 
 -- ValueConverters used for sliders depending of the scale
 
 LinearValueConverter(umin, umax, fmin, fmax)
 LinearValueConverter2(lo, mi, hi, v1, vm, v2) using 2 segments
 LogValueConverter(umin, umax, fmin, fmax)
 ExpValueConverter(umin, umax, fmin, fmax)
 
 -- ValueConverters used for accelerometers based on 3 points
 
 AccUpConverter(amin, amid, amax, fmin, fmid, fmax)        -- curve 0
 AccDownConverter(amin, amid, amax, fmin, fmid, fmax)      -- curve 1
 AccUpDownConverter(amin, amid, amax, fmin, fmid, fmax)    -- curve 2
 AccDownUpConverter(amin, amid, amax, fmin, fmid, fmax)    -- curve 3
 
 -- lists of ZoneControl are used to implement accelerometers metadata for each axes
 
 ZoneControl(zone, valueConverter) : a zone with an accelerometer data converter
 
 -- ZoneReader are used to implement screencolor metadata
 
 ZoneReader(zone, valueConverter) : a zone with a data converter

****************************************************************************************/

#include <float.h>
#include <algorithm>    // std::max
#include <cmath>
#include <vector>
#include <assert.h>


//--------------------------------------------------------------------------------------
// Interpolator(lo,hi,v1,v2)
// Maps a value x between lo and hi to a value y between v1 and v2
// y = v1 + (x-lo)/(hi-lo)*(v2-v1)
// y = v1 + (x-lo) * coef           with coef = (v2-v1)/(hi-lo)
// y = v1 + x*coef - lo*coef
// y = v1 - lo*coef + x*coef
// y = offset + x*coef              with offset = v1 - lo*coef
//--------------------------------------------------------------------------------------
class FAUST_API Interpolator {
    
    private:

        //--------------------------------------------------------------------------------------
        // Range(lo,hi) clip a value between lo and hi
        //--------------------------------------------------------------------------------------
        struct Range
        {
            double fLo;
            double fHi;

            Range(double x, double y) : fLo(std::min<double>(x,y)), fHi(std::max<double>(x,y)) {}
            double operator()(double x) { return (x<fLo) ? fLo : (x>fHi) ? fHi : x; }
        };

        Range fRange;
        double fCoef;
        double fOffset;

    public:

        Interpolator(double lo, double hi, double v1, double v2) : fRange(lo,hi)
        {
            if (hi != lo) {
                // regular case
                fCoef = (v2-v1)/(hi-lo);
                fOffset = v1 - lo*fCoef;
            } else {
                // degenerate case, avoids division by zero
                fCoef = 0;
                fOffset = (v1+v2)/2;
            }
        }
        double operator()(double v)
        {
            double x = fRange(v);
            return  fOffset + x*fCoef;
        }

        void getLowHigh(double& amin, double& amax)
        {
            amin = fRange.fLo;
            amax = fRange.fHi;
        }
};

//--------------------------------------------------------------------------------------
// Interpolator3pt(lo,mi,hi,v1,vm,v2)
// Map values between lo mid hi to values between v1 vm v2
//--------------------------------------------------------------------------------------
class FAUST_API Interpolator3pt {

    private:

        Interpolator fSegment1;
        Interpolator fSegment2;
        double fMid;

    public:

        Interpolator3pt(double lo, double mi, double hi, double v1, double vm, double v2) :
            fSegment1(lo, mi, v1, vm),
            fSegment2(mi, hi, vm, v2),
            fMid(mi) {}
        double operator()(double x) { return  (x < fMid) ? fSegment1(x) : fSegment2(x); }

        void getMappingValues(double& amin, double& amid, double& amax)
        {
            fSegment1.getLowHigh(amin, amid);
            fSegment2.getLowHigh(amid, amax);
        }
};

//--------------------------------------------------------------------------------------
// Abstract ValueConverter class. Converts values between UI and Faust representations
//--------------------------------------------------------------------------------------
class FAUST_API ValueConverter {

    public:

        virtual ~ValueConverter() {}
        virtual double ui2faust(double x) { return x; };
        virtual double faust2ui(double x) { return x; };
};

//--------------------------------------------------------------------------------------
// A converter than can be updated
//--------------------------------------------------------------------------------------

class FAUST_API UpdatableValueConverter : public ValueConverter {
    
    protected:
        
        bool fActive;
        
    public:
        
        UpdatableValueConverter():fActive(true)
        {}
        virtual ~UpdatableValueConverter()
        {}
        
        virtual void setMappingValues(double amin, double amid, double amax, double min, double init, double max) = 0;
        virtual void getMappingValues(double& amin, double& amid, double& amax) = 0;
        
        void setActive(bool on_off) { fActive = on_off; }
        bool getActive() { return fActive; }
    
};

//--------------------------------------------------------------------------------------
// Linear conversion between ui and Faust values
//--------------------------------------------------------------------------------------
class FAUST_API LinearValueConverter : public ValueConverter {
    
    private:
        
        Interpolator fUI2F;
        Interpolator fF2UI;
        
    public:
        
        LinearValueConverter(double umin, double umax, double fmin, double fmax) :
            fUI2F(umin,umax,fmin,fmax), fF2UI(fmin,fmax,umin,umax)
        {}
        
        LinearValueConverter() : fUI2F(0.,0.,0.,0.), fF2UI(0.,0.,0.,0.)
        {}
        virtual double ui2faust(double x) { return fUI2F(x); }
        virtual double faust2ui(double x) { return fF2UI(x); }
    
};

//--------------------------------------------------------------------------------------
// Two segments linear conversion between ui and Faust values
//--------------------------------------------------------------------------------------
class FAUST_API LinearValueConverter2 : public UpdatableValueConverter {
    
    private:
    
        Interpolator3pt fUI2F;
        Interpolator3pt fF2UI;
        
    public:
    
        LinearValueConverter2(double amin, double amid, double amax, double min, double init, double max) :
            fUI2F(amin, amid, amax, min, init, max), fF2UI(min, init, max, amin, amid, amax)
        {}
        
        LinearValueConverter2() : fUI2F(0.,0.,0.,0.,0.,0.), fF2UI(0.,0.,0.,0.,0.,0.)
        {}
    
        virtual double ui2faust(double x) { return fUI2F(x); }
        virtual double faust2ui(double x) { return fF2UI(x); }
    
        virtual void setMappingValues(double amin, double amid, double amax, double min, double init, double max)
        {
            fUI2F = Interpolator3pt(amin, amid, amax, min, init, max);
            fF2UI = Interpolator3pt(min, init, max, amin, amid, amax);
        }

        virtual void getMappingValues(double& amin, double& amid, double& amax)
        {
            fUI2F.getMappingValues(amin, amid, amax);
        }
    
};

//--------------------------------------------------------------------------------------
// Logarithmic conversion between ui and Faust values
//--------------------------------------------------------------------------------------
class FAUST_API LogValueConverter : public LinearValueConverter {

    public:
    
        // We use DBL_EPSILON which is bigger than DBL_MIN (safer)
        LogValueConverter(double umin, double umax, double fmin, double fmax) :
            LinearValueConverter(umin, umax, std::log(std::max<double>(DBL_EPSILON, fmin)), std::log(std::max<double>(DBL_EPSILON, fmax)))
        {}

        virtual double ui2faust(double x) { return std::exp(LinearValueConverter::ui2faust(x)); }
        virtual double faust2ui(double x) { return LinearValueConverter::faust2ui(std::log(std::max<double>(DBL_EPSILON, x))); }

};

//--------------------------------------------------------------------------------------
// Exponential conversion between ui and Faust values
//--------------------------------------------------------------------------------------
class FAUST_API ExpValueConverter : public LinearValueConverter {

    public:

        ExpValueConverter(double umin, double umax, double fmin, double fmax) :
            LinearValueConverter(umin, umax, std::min<double>(DBL_MAX, std::exp(fmin)), std::min<double>(DBL_MAX, std::exp(fmax)))
        {}

        virtual double ui2faust(double x) { return std::log(LinearValueConverter::ui2faust(x)); }
        virtual double faust2ui(double x) { return LinearValueConverter::faust2ui(std::min<double>(DBL_MAX, std::exp(x))); }

};

//--------------------------------------------------------------------------------------
// Convert accelerometer or gyroscope values to Faust values
// Using an Up curve (curve 0)
//--------------------------------------------------------------------------------------
class FAUST_API AccUpConverter : public UpdatableValueConverter {

    private:

        Interpolator3pt fA2F;
        Interpolator3pt fF2A;

    public:

        AccUpConverter(double amin, double amid, double amax, double fmin, double fmid, double fmax) :
            fA2F(amin,amid,amax,fmin,fmid,fmax),
            fF2A(fmin,fmid,fmax,amin,amid,amax)
        {}

        virtual double ui2faust(double x) { return fA2F(x); }
        virtual double faust2ui(double x) { return fF2A(x); }

        virtual void setMappingValues(double amin, double amid, double amax, double fmin, double fmid, double fmax)
        {
            //__android_log_print(ANDROID_LOG_ERROR, "Faust", "AccUpConverter update %f %f %f %f %f %f", amin,amid,amax,fmin,fmid,fmax);
            fA2F = Interpolator3pt(amin, amid, amax, fmin, fmid, fmax);
            fF2A = Interpolator3pt(fmin, fmid, fmax, amin, amid, amax);
        }

        virtual void getMappingValues(double& amin, double& amid, double& amax)
        {
            fA2F.getMappingValues(amin, amid, amax);
        }

};

//--------------------------------------------------------------------------------------
// Convert accelerometer or gyroscope values to Faust values
// Using a Down curve (curve 1)
//--------------------------------------------------------------------------------------
class FAUST_API AccDownConverter : public UpdatableValueConverter {

    private:

        Interpolator3pt	fA2F;
        Interpolator3pt	fF2A;

    public:

        AccDownConverter(double amin, double amid, double amax, double fmin, double fmid, double fmax) :
            fA2F(amin,amid,amax,fmax,fmid,fmin),
            fF2A(fmin,fmid,fmax,amax,amid,amin)
        {}

        virtual double ui2faust(double x) { return fA2F(x); }
        virtual double faust2ui(double x) { return fF2A(x); }

        virtual void setMappingValues(double amin, double amid, double amax, double fmin, double fmid, double fmax)
        {
             //__android_log_print(ANDROID_LOG_ERROR, "Faust", "AccDownConverter update %f %f %f %f %f %f", amin,amid,amax,fmin,fmid,fmax);
            fA2F = Interpolator3pt(amin, amid, amax, fmax, fmid, fmin);
            fF2A = Interpolator3pt(fmin, fmid, fmax, amax, amid, amin);
        }

        virtual void getMappingValues(double& amin, double& amid, double& amax)
        {
            fA2F.getMappingValues(amin, amid, amax);
        }
};

//--------------------------------------------------------------------------------------
// Convert accelerometer or gyroscope values to Faust values
// Using an Up-Down curve (curve 2)
//--------------------------------------------------------------------------------------
class FAUST_API AccUpDownConverter : public UpdatableValueConverter {

    private:

        Interpolator3pt	fA2F;
        Interpolator fF2A;

    public:

        AccUpDownConverter(double amin, double amid, double amax, double fmin, double fmid, double fmax) :
            fA2F(amin,amid,amax,fmin,fmax,fmin),
            fF2A(fmin,fmax,amin,amax)				// Special, pseudo inverse of a non monotonic function
        {}

        virtual double ui2faust(double x) { return fA2F(x); }
        virtual double faust2ui(double x) { return fF2A(x); }

        virtual void setMappingValues(double amin, double amid, double amax, double fmin, double fmid, double fmax)
        {
            //__android_log_print(ANDROID_LOG_ERROR, "Faust", "AccUpDownConverter update %f %f %f %f %f %f", amin,amid,amax,fmin,fmid,fmax);
            fA2F = Interpolator3pt(amin, amid, amax, fmin, fmax, fmin);
            fF2A = Interpolator(fmin, fmax, amin, amax);
        }

        virtual void getMappingValues(double& amin, double& amid, double& amax)
        {
            fA2F.getMappingValues(amin, amid, amax);
        }
};

//--------------------------------------------------------------------------------------
// Convert accelerometer or gyroscope values to Faust values
// Using a Down-Up curve (curve 3)
//--------------------------------------------------------------------------------------
class FAUST_API AccDownUpConverter : public UpdatableValueConverter {

    private:

        Interpolator3pt	fA2F;
        Interpolator fF2A;

    public:

        AccDownUpConverter(double amin, double amid, double amax, double fmin, double fmid, double fmax) :
            fA2F(amin,amid,amax,fmax,fmin,fmax),
            fF2A(fmin,fmax,amin,amax)				// Special, pseudo inverse of a non monotonic function
        {}

        virtual double ui2faust(double x) { return fA2F(x); }
        virtual double faust2ui(double x) { return fF2A(x); }

        virtual void setMappingValues(double amin, double amid, double amax, double fmin, double fmid, double fmax)
        {
            //__android_log_print(ANDROID_LOG_ERROR, "Faust", "AccDownUpConverter update %f %f %f %f %f %f", amin,amid,amax,fmin,fmid,fmax);
            fA2F = Interpolator3pt(amin, amid, amax, fmax, fmin, fmax);
            fF2A = Interpolator(fmin, fmax, amin, amax);
        }

        virtual void getMappingValues(double& amin, double& amid, double& amax)
        {
            fA2F.getMappingValues(amin, amid, amax);
        }
};

//--------------------------------------------------------------------------------------
// Base class for ZoneControl
//--------------------------------------------------------------------------------------
class FAUST_API ZoneControl {

    protected:

        FAUSTFLOAT*	fZone;

    public:

        ZoneControl(FAUSTFLOAT* zone) : fZone(zone) {}
        virtual ~ZoneControl() {}

        virtual void update(double v) const {}

        virtual void setMappingValues(int curve, double amin, double amid, double amax, double min, double init, double max) {}
        virtual void getMappingValues(double& amin, double& amid, double& amax) {}

        FAUSTFLOAT* getZone() { return fZone; }

        virtual void setActive(bool on_off) {}
        virtual bool getActive() { return false; }

        virtual int getCurve() { return -1; }

};

//--------------------------------------------------------------------------------------
//  Useful to implement accelerometers metadata as a list of ZoneControl for each axes
//--------------------------------------------------------------------------------------
class FAUST_API ConverterZoneControl : public ZoneControl {

    protected:

        ValueConverter* fValueConverter;

    public:

        ConverterZoneControl(FAUSTFLOAT* zone, ValueConverter* converter) : ZoneControl(zone), fValueConverter(converter) {}
        virtual ~ConverterZoneControl() { delete fValueConverter; } // Assuming fValueConverter is not kept elsewhere...

        virtual void update(double v) const { *fZone = FAUSTFLOAT(fValueConverter->ui2faust(v)); }

        ValueConverter* getConverter() { return fValueConverter; }

};

//--------------------------------------------------------------------------------------
// Association of a zone and a four value converter, each one for each possible curve.
// Useful to implement accelerometers metadata as a list of ZoneControl for each axes
//--------------------------------------------------------------------------------------
class FAUST_API CurveZoneControl : public ZoneControl {

    private:

        std::vector<UpdatableValueConverter*> fValueConverters;
        int fCurve;

    public:

        CurveZoneControl(FAUSTFLOAT* zone, int curve, double amin, double amid, double amax, double min, double init, double max) : ZoneControl(zone), fCurve(0)
        {
            assert(curve >= 0 && curve <= 3);
            fValueConverters.push_back(new AccUpConverter(amin, amid, amax, min, init, max));
            fValueConverters.push_back(new AccDownConverter(amin, amid, amax, min, init, max));
            fValueConverters.push_back(new AccUpDownConverter(amin, amid, amax, min, init, max));
            fValueConverters.push_back(new AccDownUpConverter(amin, amid, amax, min, init, max));
            fCurve = curve;
        }
        virtual ~CurveZoneControl()
        {
            for (const auto& it : fValueConverters) { delete it; }
        }
        void update(double v) const { if (fValueConverters[fCurve]->getActive()) *fZone = FAUSTFLOAT(fValueConverters[fCurve]->ui2faust(v)); }

        void setMappingValues(int curve, double amin, double amid, double amax, double min, double init, double max)
        {
            fValueConverters[curve]->setMappingValues(amin, amid, amax, min, init, max);
            fCurve = curve;
        }

        void getMappingValues(double& amin, double& amid, double& amax)
        {
            fValueConverters[fCurve]->getMappingValues(amin, amid, amax);
        }

        void setActive(bool on_off)
        {
            for (const auto& it : fValueConverters) { it->setActive(on_off); }
        }

        int getCurve() { return fCurve; }
};

class FAUST_API ZoneReader {

    private:

        FAUSTFLOAT* fZone;
        Interpolator fInterpolator;

    public:

        ZoneReader(FAUSTFLOAT* zone, double lo, double hi) : fZone(zone), fInterpolator(lo, hi, 0, 255) {}

        virtual ~ZoneReader() {}

        int getValue()
        {
            return (fZone != nullptr) ? int(fInterpolator(*fZone)) : 127;
        }

};

#endif
/**************************  END  ValueConverter.h **************************/
/************************** BEGIN MetaDataUI.h **************************
 FAUST Architecture File
 Copyright (C) 2003-2022 GRAME, Centre National de Creation Musicale
 ---------------------------------------------------------------------
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation; either version 2.1 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 
 EXCEPTION : As a special exception, you may create a larger work
 that contains this FAUST architecture section and distribute
 that work under terms of your choice, so long as this FAUST
 architecture section is not modified.
 ************************************************************************/

#ifndef MetaData_UI_H
#define MetaData_UI_H

#ifndef FAUSTFLOAT
#define FAUSTFLOAT float
#endif

#include <map>
#include <set>
#include <string>
#include <string.h>
#include <assert.h>
#include <stdio.h> // We use the lighter fprintf code


static bool startWith(const std::string& str, const std::string& prefix)
{
    return (str.substr(0, prefix.size()) == prefix);
}

/**
 * Convert a dB value into a scale between 0 and 1 (following IEC standard ?)
 */
static FAUSTFLOAT dB2Scale(FAUSTFLOAT dB)
{
    FAUSTFLOAT scale = FAUSTFLOAT(1.0);
    
    /*if (dB < -70.0f)
     scale = 0.0f;
     else*/
    if (dB < FAUSTFLOAT(-60.0))
        scale = (dB + FAUSTFLOAT(70.0)) * FAUSTFLOAT(0.0025);
    else if (dB < FAUSTFLOAT(-50.0))
        scale = (dB + FAUSTFLOAT(60.0)) * FAUSTFLOAT(0.005) + FAUSTFLOAT(0.025);
    else if (dB < FAUSTFLOAT(-40.0))
        scale = (dB + FAUSTFLOAT(50.0)) * FAUSTFLOAT(0.0075) + FAUSTFLOAT(0.075);
    else if (dB < FAUSTFLOAT(-30.0))
        scale = (dB + FAUSTFLOAT(40.0)) * FAUSTFLOAT(0.015) + FAUSTFLOAT(0.15);
    else if (dB < FAUSTFLOAT(-20.0))
        scale = (dB + FAUSTFLOAT(30.0)) * FAUSTFLOAT(0.02) + FAUSTFLOAT(0.3);
    else if (dB < FAUSTFLOAT(-0.001) || dB > FAUSTFLOAT(0.001))  /* if (dB < 0.0) */
        scale = (dB + FAUSTFLOAT(20.0)) * FAUSTFLOAT(0.025) + FAUSTFLOAT(0.5);
    
    return scale;
}

/*******************************************************************************
 * MetaDataUI : Common class for MetaData handling
 ******************************************************************************/

//============================= BEGIN GROUP LABEL METADATA===========================
// Unlike widget's label, metadata inside group's label are not extracted directly by
// the Faust compiler. Therefore they must be extracted within the architecture file
//-----------------------------------------------------------------------------------

class MetaDataUI {
    
    protected:
        
        std::string                         fGroupTooltip;
        std::map<FAUSTFLOAT*, FAUSTFLOAT>   fGuiSize;            // map widget zone with widget size coef
        std::map<FAUSTFLOAT*, std::string>  fTooltip;            // map widget zone with tooltip strings
        std::map<FAUSTFLOAT*, std::string>  fUnit;               // map widget zone to unit string (i.e. "dB")
        std::map<FAUSTFLOAT*, std::string>  fRadioDescription;   // map zone to {'low':440; ...; 'hi':1000.0}
        std::map<FAUSTFLOAT*, std::string>  fMenuDescription;    // map zone to {'low':440; ...; 'hi':1000.0}
        std::set<FAUSTFLOAT*>               fKnobSet;            // set of widget zone to be knobs
        std::set<FAUSTFLOAT*>               fLedSet;             // set of widget zone to be LEDs
        std::set<FAUSTFLOAT*>               fNumSet;             // set of widget zone to be numerical bargraphs
        std::set<FAUSTFLOAT*>               fLogSet;             // set of widget zone having a log UI scale
        std::set<FAUSTFLOAT*>               fExpSet;             // set of widget zone having an exp UI scale
        std::set<FAUSTFLOAT*>               fHiddenSet;          // set of hidden widget zone
        
        void clearMetadata()
        {
            fGuiSize.clear();
            fTooltip.clear();
            fUnit.clear();
            fRadioDescription.clear();
            fMenuDescription.clear();
            fKnobSet.clear();
            fLedSet.clear();
            fNumSet.clear();
            fLogSet.clear();
            fExpSet.clear();
            fHiddenSet.clear();
            fGroupTooltip = "";
        }
        
        /**
         * rmWhiteSpaces(): Remove the leading and trailing white spaces of a string
         * (but not those in the middle of the string)
         */
        static std::string rmWhiteSpaces(const std::string& s)
        {
            size_t i = s.find_first_not_of(" \t");
            size_t j = s.find_last_not_of(" \t");
            if ((i != std::string::npos) && (j != std::string::npos)) {
                return s.substr(i, 1+j-i);
            } else {
                return "";
            }
        }
        
        /**
         * Format tooltip string by replacing some white spaces by
         * return characters so that line width doesn't exceed n.
         * Limitation : long words exceeding n are not cut.
         */
        std::string formatTooltip(int n, const std::string& tt)
        {
            std::string ss = tt;  // ss string we are going to format
            int lws = 0;          // last white space encountered
            int lri = 0;          // last return inserted
            for (int i = 0; i < (int)tt.size(); i++) {
                if (tt[i] == ' ') lws = i;
                if (((i-lri) >= n) && (lws > lri)) {
                    // insert return here
                    ss[lws] = '\n';
                    lri = lws;
                }
            }
            return ss;
        }
        
    public:
        
        virtual ~MetaDataUI()
        {}
        
        enum Scale {
            kLin,
            kLog,
            kExp
        };
        
        Scale getScale(FAUSTFLOAT* zone)
        {
            if (fLogSet.count(zone) > 0) return kLog;
            if (fExpSet.count(zone) > 0) return kExp;
            return kLin;
        }
        
        bool isKnob(FAUSTFLOAT* zone)
        {
            return fKnobSet.count(zone) > 0;
        }
        
        bool isRadio(FAUSTFLOAT* zone)
        {
            return fRadioDescription.count(zone) > 0;
        }
        
        bool isMenu(FAUSTFLOAT* zone)
        {
            return fMenuDescription.count(zone) > 0;
        }
        
        bool isLed(FAUSTFLOAT* zone)
        {
            return fLedSet.count(zone) > 0;
        }
        
        bool isNumerical(FAUSTFLOAT* zone)
        {
            return fNumSet.count(zone) > 0;
        }
        
        bool isHidden(FAUSTFLOAT* zone)
        {
            return fHiddenSet.count(zone) > 0;
        }
        
        /**
         * Extracts metadata from a label : 'vol [unit: dB]' -> 'vol' + metadata(unit=dB)
         */
        static void extractMetadata(const std::string& fulllabel, std::string& label, std::map<std::string, std::string>& metadata)
        {
            enum {kLabel, kEscape1, kEscape2, kEscape3, kKey, kValue};
            int state = kLabel; int deep = 0;
            std::string key, value;
            
            for (unsigned int i = 0; i < fulllabel.size(); i++) {
                char c = fulllabel[i];
                switch (state) {
                    case kLabel :
                        assert(deep == 0);
                        switch (c) {
                            case '\\' : state = kEscape1; break;
                            case '[' : state = kKey; deep++; break;
                            default : label += c;
                        }
                        break;
                        
                    case kEscape1:
                        label += c;
                        state = kLabel;
                        break;
                        
                    case kEscape2:
                        key += c;
                        state = kKey;
                        break;
                        
                    case kEscape3:
                        value += c;
                        state = kValue;
                        break;
                        
                    case kKey:
                        assert(deep > 0);
                        switch (c) {
                            case '\\':
                                state = kEscape2;
                                break;
                                
                            case '[':
                                deep++;
                                key += c;
                                break;
                                
                            case ':':
                                if (deep == 1) {
                                    state = kValue;
                                } else {
                                    key += c;
                                }
                                break;
                            case ']':
                                deep--;
                                if (deep < 1) {
                                    metadata[rmWhiteSpaces(key)] = "";
                                    state = kLabel;
                                    key = "";
                                    value = "";
                                } else {
                                    key += c;
                                }
                                break;
                            default : key += c;
                        }
                        break;
                        
                    case kValue:
                        assert(deep > 0);
                        switch (c) {
                            case '\\':
                                state = kEscape3;
                                break;
                                
                            case '[':
                                deep++;
                                value += c;
                                break;
                                
                            case ']':
                                deep--;
                                if (deep < 1) {
                                    metadata[rmWhiteSpaces(key)] = rmWhiteSpaces(value);
                                    state = kLabel;
                                    key = "";
                                    value = "";
                                } else {
                                    value += c;
                                }
                                break;
                            default : value += c;
                        }
                        break;
                        
                    default:
                        fprintf(stderr, "ERROR unrecognized state %d\n", state);
                }
            }
            label = rmWhiteSpaces(label);
        }
        
        /**
         * Analyses the widget zone metadata declarations and takes appropriate actions.
         */
        void declare(FAUSTFLOAT* zone, const char* key, const char* value)
        {
            if (zone == 0) {
                // special zone 0 means group metadata
                if (strcmp(key, "tooltip") == 0) {
                    // only group tooltip are currently implemented
                    fGroupTooltip = formatTooltip(30, value);
                } else if ((strcmp(key, "hidden") == 0) && (strcmp(value, "1") == 0)) {
                    fHiddenSet.insert(zone);
                }
            } else {
                if (strcmp(key, "size") == 0) {
                    fGuiSize[zone] = atof(value);
                }
                else if (strcmp(key, "tooltip") == 0) {
                    fTooltip[zone] = formatTooltip(30, value);
                }
                else if (strcmp(key, "unit") == 0) {
                    fUnit[zone] = value;
                }
                else if ((strcmp(key, "hidden") == 0) && (strcmp(value, "1") == 0)) {
                    fHiddenSet.insert(zone);
                }
                else if (strcmp(key, "scale") == 0) {
                    if (strcmp(value, "log") == 0) {
                        fLogSet.insert(zone);
                    } else if (strcmp(value, "exp") == 0) {
                        fExpSet.insert(zone);
                    }
                }
                else if (strcmp(key, "style") == 0) {
                    if (strcmp(value, "knob") == 0) {
                        fKnobSet.insert(zone);
                    } else if (strcmp(value, "led") == 0) {
                        fLedSet.insert(zone);
                    } else if (strcmp(value, "numerical") == 0) {
                        fNumSet.insert(zone);
                    } else {
                        const char* p = value;
                        if (parseWord(p, "radio")) {
                            fRadioDescription[zone] = std::string(p);
                        } else if (parseWord(p, "menu")) {
                            fMenuDescription[zone] = std::string(p);
                        }
                    }
                }
            }
        }
    
};

#endif
/**************************  END  MetaDataUI.h **************************/
/************************** BEGIN ring-buffer.h **************************/
/*
  Copyright (C) 2000 Paul Davis
  Copyright (C) 2003 Rohan Drape
  Copyright (C) 2016 GRAME (renaming for internal use)

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation; either version 2.1 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

  ISO/POSIX C version of Paul Davis's lock free ringbuffer C++ code.
  This is safe for the case of one read thread and one write thread.
*/

#ifndef __ring_buffer__
#define __ring_buffer__

#include <stdlib.h>
#include <string.h>

#ifdef WIN32
# pragma warning (disable: 4334)
#else
# pragma GCC diagnostic ignored "-Wunused-function"
#endif

typedef struct {
    char *buf;
    size_t len;
}
ringbuffer_data_t;

typedef struct {
    char *buf;
    volatile size_t write_ptr;
    volatile size_t read_ptr;
    size_t	size;
    size_t	size_mask;
    int	mlocked;
}
ringbuffer_t;

static ringbuffer_t *ringbuffer_create(size_t sz);
static void ringbuffer_free(ringbuffer_t *rb);
static void ringbuffer_get_read_vector(const ringbuffer_t *rb,
                                         ringbuffer_data_t *vec);
static void ringbuffer_get_write_vector(const ringbuffer_t *rb,
                                          ringbuffer_data_t *vec);
static size_t ringbuffer_read(ringbuffer_t *rb, char *dest, size_t cnt);
static size_t ringbuffer_peek(ringbuffer_t *rb, char *dest, size_t cnt);
static void ringbuffer_read_advance(ringbuffer_t *rb, size_t cnt);
static size_t ringbuffer_read_space(const ringbuffer_t *rb);
static int ringbuffer_mlock(ringbuffer_t *rb);
static void ringbuffer_reset(ringbuffer_t *rb);
static void ringbuffer_reset_size (ringbuffer_t * rb, size_t sz);
static size_t ringbuffer_write(ringbuffer_t *rb, const char *src,
                                 size_t cnt);
static void ringbuffer_write_advance(ringbuffer_t *rb, size_t cnt);
static size_t ringbuffer_write_space(const ringbuffer_t *rb);

/* Create a new ringbuffer to hold at least `sz' bytes of data. The
   actual buffer size is rounded up to the next power of two. */

static ringbuffer_t *
ringbuffer_create (size_t sz)
{
	size_t power_of_two;
	ringbuffer_t *rb;

	if ((rb = (ringbuffer_t *) malloc (sizeof (ringbuffer_t))) == NULL) {
		return NULL;
	}

	for (power_of_two = 1u; 1u << power_of_two < sz; power_of_two++);

	rb->size = 1u << power_of_two;
	rb->size_mask = rb->size;
	rb->size_mask -= 1;
	rb->write_ptr = 0;
	rb->read_ptr = 0;
	if ((rb->buf = (char *) malloc (rb->size)) == NULL) {
		free (rb);
		return NULL;
	}
	rb->mlocked = 0;

	return rb;
}

/* Free all data associated with the ringbuffer `rb'. */

static void
ringbuffer_free (ringbuffer_t * rb)
{
#ifdef USE_MLOCK
	if (rb->mlocked) {
		munlock (rb->buf, rb->size);
	}
#endif /* USE_MLOCK */
	free (rb->buf);
	free (rb);
}

/* Lock the data block of `rb' using the system call 'mlock'.  */

static int
ringbuffer_mlock (ringbuffer_t * rb)
{
#ifdef USE_MLOCK
	if (mlock (rb->buf, rb->size)) {
		return -1;
	}
#endif /* USE_MLOCK */
	rb->mlocked = 1;
	return 0;
}

/* Reset the read and write pointers to zero. This is not thread
   safe. */

static void
ringbuffer_reset (ringbuffer_t * rb)
{
	rb->read_ptr = 0;
	rb->write_ptr = 0;
    memset(rb->buf, 0, rb->size);
}

/* Reset the read and write pointers to zero. This is not thread
   safe. */

static void
ringbuffer_reset_size (ringbuffer_t * rb, size_t sz)
{
    rb->size = sz;
    rb->size_mask = rb->size;
    rb->size_mask -= 1;
    rb->read_ptr = 0;
    rb->write_ptr = 0;
}

/* Return the number of bytes available for reading. This is the
   number of bytes in front of the read pointer and behind the write
   pointer.  */

static size_t
ringbuffer_read_space (const ringbuffer_t * rb)
{
	size_t w, r;

	w = rb->write_ptr;
	r = rb->read_ptr;

	if (w > r) {
		return w - r;
	} else {
		return (w - r + rb->size) & rb->size_mask;
	}
}

/* Return the number of bytes available for writing. This is the
   number of bytes in front of the write pointer and behind the read
   pointer.  */

static size_t
ringbuffer_write_space (const ringbuffer_t * rb)
{
	size_t w, r;

	w = rb->write_ptr;
	r = rb->read_ptr;

	if (w > r) {
		return ((r - w + rb->size) & rb->size_mask) - 1;
	} else if (w < r) {
		return (r - w) - 1;
	} else {
		return rb->size - 1;
	}
}

/* The copying data reader. Copy at most `cnt' bytes from `rb' to
   `dest'.  Returns the actual number of bytes copied. */

static size_t
ringbuffer_read (ringbuffer_t * rb, char *dest, size_t cnt)
{
	size_t free_cnt;
	size_t cnt2;
	size_t to_read;
	size_t n1, n2;

	if ((free_cnt = ringbuffer_read_space (rb)) == 0) {
		return 0;
	}

	to_read = cnt > free_cnt ? free_cnt : cnt;

	cnt2 = rb->read_ptr + to_read;

	if (cnt2 > rb->size) {
		n1 = rb->size - rb->read_ptr;
		n2 = cnt2 & rb->size_mask;
	} else {
		n1 = to_read;
		n2 = 0;
	}

	memcpy (dest, &(rb->buf[rb->read_ptr]), n1);
	rb->read_ptr = (rb->read_ptr + n1) & rb->size_mask;

	if (n2) {
		memcpy (dest + n1, &(rb->buf[rb->read_ptr]), n2);
		rb->read_ptr = (rb->read_ptr + n2) & rb->size_mask;
	}

	return to_read;
}

/* The copying data reader w/o read pointer advance. Copy at most
   `cnt' bytes from `rb' to `dest'.  Returns the actual number of bytes
   copied. */

static size_t
ringbuffer_peek (ringbuffer_t * rb, char *dest, size_t cnt)
{
	size_t free_cnt;
	size_t cnt2;
	size_t to_read;
	size_t n1, n2;
	size_t tmp_read_ptr;

	tmp_read_ptr = rb->read_ptr;

	if ((free_cnt = ringbuffer_read_space (rb)) == 0) {
		return 0;
	}

	to_read = cnt > free_cnt ? free_cnt : cnt;

	cnt2 = tmp_read_ptr + to_read;

	if (cnt2 > rb->size) {
		n1 = rb->size - tmp_read_ptr;
		n2 = cnt2 & rb->size_mask;
	} else {
		n1 = to_read;
		n2 = 0;
	}

	memcpy (dest, &(rb->buf[tmp_read_ptr]), n1);
	tmp_read_ptr = (tmp_read_ptr + n1) & rb->size_mask;

	if (n2) {
		memcpy (dest + n1, &(rb->buf[tmp_read_ptr]), n2);
	}

	return to_read;
}

/* The copying data writer. Copy at most `cnt' bytes to `rb' from
   `src'.  Returns the actual number of bytes copied. */

static size_t
ringbuffer_write (ringbuffer_t * rb, const char *src, size_t cnt)
{
	size_t free_cnt;
	size_t cnt2;
	size_t to_write;
	size_t n1, n2;

	if ((free_cnt = ringbuffer_write_space (rb)) == 0) {
		return 0;
	}

	to_write = cnt > free_cnt ? free_cnt : cnt;

	cnt2 = rb->write_ptr + to_write;

	if (cnt2 > rb->size) {
		n1 = rb->size - rb->write_ptr;
		n2 = cnt2 & rb->size_mask;
	} else {
		n1 = to_write;
		n2 = 0;
	}

	memcpy (&(rb->buf[rb->write_ptr]), src, n1);
	rb->write_ptr = (rb->write_ptr + n1) & rb->size_mask;

	if (n2) {
		memcpy (&(rb->buf[rb->write_ptr]), src + n1, n2);
		rb->write_ptr = (rb->write_ptr + n2) & rb->size_mask;
	}

	return to_write;
}

/* Advance the read pointer `cnt' places. */

static void
ringbuffer_read_advance (ringbuffer_t * rb, size_t cnt)
{
	size_t tmp = (rb->read_ptr + cnt) & rb->size_mask;
	rb->read_ptr = tmp;
}

/* Advance the write pointer `cnt' places. */

static void
ringbuffer_write_advance (ringbuffer_t * rb, size_t cnt)
{
	size_t tmp = (rb->write_ptr + cnt) & rb->size_mask;
	rb->write_ptr = tmp;
}

/* The non-copying data reader. `vec' is an array of two places. Set
   the values at `vec' to hold the current readable data at `rb'. If
   the readable data is in one segment the second segment has zero
   length. */

static void
ringbuffer_get_read_vector (const ringbuffer_t * rb,
				 ringbuffer_data_t * vec)
{
	size_t free_cnt;
	size_t cnt2;
	size_t w, r;

	w = rb->write_ptr;
	r = rb->read_ptr;

	if (w > r) {
		free_cnt = w - r;
	} else {
		free_cnt = (w - r + rb->size) & rb->size_mask;
	}

	cnt2 = r + free_cnt;

	if (cnt2 > rb->size) {

		/* Two part vector: the rest of the buffer after the current write
		   ptr, plus some from the start of the buffer. */

		vec[0].buf = &(rb->buf[r]);
		vec[0].len = rb->size - r;
		vec[1].buf = rb->buf;
		vec[1].len = cnt2 & rb->size_mask;

	} else {

		/* Single part vector: just the rest of the buffer */

		vec[0].buf = &(rb->buf[r]);
		vec[0].len = free_cnt;
		vec[1].len = 0;
	}
}

/* The non-copying data writer. `vec' is an array of two places. Set
   the values at `vec' to hold the current writeable data at `rb'. If
   the writeable data is in one segment the second segment has zero
   length. */

static void
ringbuffer_get_write_vector (const ringbuffer_t * rb,
				  ringbuffer_data_t * vec)
{
	size_t free_cnt;
	size_t cnt2;
	size_t w, r;

	w = rb->write_ptr;
	r = rb->read_ptr;

	if (w > r) {
		free_cnt = ((r - w + rb->size) & rb->size_mask) - 1;
	} else if (w < r) {
		free_cnt = (r - w) - 1;
	} else {
		free_cnt = rb->size - 1;
	}

	cnt2 = w + free_cnt;

	if (cnt2 > rb->size) {

		/* Two part vector: the rest of the buffer after the current write
		   ptr, plus some from the start of the buffer. */

		vec[0].buf = &(rb->buf[w]);
		vec[0].len = rb->size - w;
		vec[1].buf = rb->buf;
		vec[1].len = cnt2 & rb->size_mask;
	} else {
		vec[0].buf = &(rb->buf[w]);
		vec[0].len = free_cnt;
		vec[1].len = 0;
	}
}

#endif // __ring_buffer__
/**************************  END  ring-buffer.h **************************/

/*******************************************************************************
 * GUI : Abstract Graphic User Interface
 * Provides additional mechanisms to synchronize widgets and zones. Widgets
 * should both reflect the value of a zone and allow to change this value.
 ******************************************************************************/

class uiItem;
class GUI;
struct clist;

typedef void (*uiCallback)(FAUSTFLOAT val, void* data);

/**
 * Base class for uiTypedItem: memory zones that can be grouped and synchronized, using an internal cache.
 */
struct uiItemBase
{
    
    uiItemBase(GUI* ui, FAUSTFLOAT* zone)
    {
        assert(ui);
        assert(zone);
    }
    
    virtual ~uiItemBase()
    {}
    
    /**
     * This method will be called when the value changes externally,
     * and will signal the new value to all linked uItem
     * when the value is different from the cached one.
     *
     * @param v - the new value
     */
    virtual void modifyZone(FAUSTFLOAT v) = 0;
    
    /**
     * This method will be called when the value changes externally,
     * and will signal the new value to all linked uItem
     * when the value is different from the cached one.
     *
     * @param date - the timestamp of the received value in usec
     * @param v - the new value
     */
    virtual void modifyZone(double date, FAUSTFLOAT v) {}
    
    /**
     * This method is called by the synchronisation mecanism and is expected
     * to 'reflect' the new value, by changing the Widget layout for instance,
     * or sending a message (OSC, MIDI...)
     */
    virtual void reflectZone() = 0;
    
    /**
     * Return the cached value.
     *
     * @return - the cached value
     */
    virtual double cache() = 0;
    
};

// Declared as 'static' to avoid code duplication at link time
static void deleteClist(clist* cl);

/**
 * A list containing all groupe uiItemBase objects.
 */
struct clist : public std::list<uiItemBase*>
{
    
    virtual ~clist()
    {
        deleteClist(this);
    }
        
};

static void createUiCallbackItem(GUI* ui, FAUSTFLOAT* zone, uiCallback foo, void* data);

typedef std::map<FAUSTFLOAT*, clist*> zmap;

typedef std::map<FAUSTFLOAT*, ringbuffer_t*> ztimedmap;

class GUI : public UI
{
		
    private:
     
        static std::list<GUI*> fGuiList;
        zmap fZoneMap;
        bool fStopped;
    
     public:
            
        GUI():fStopped(false)
        {	
            fGuiList.push_back(this);
        }
        
        virtual ~GUI() 
        {   
            // delete all items
            for (const auto& it : fZoneMap) {
                delete it.second;
            }
            // suppress 'this' in static fGuiList
            fGuiList.remove(this);
        }

        // -- registerZone(z,c) : zone management
        
        void registerZone(FAUSTFLOAT* z, uiItemBase* c)
        {
            if (fZoneMap.find(z) == fZoneMap.end()) fZoneMap[z] = new clist();
            fZoneMap[z]->push_back(c);
        }
    
        void updateZone(FAUSTFLOAT* z)
        {
            FAUSTFLOAT v = *z;
            clist* cl = fZoneMap[z];
            for (const auto& c : *cl) {
                if (c->cache() != v) c->reflectZone();
            }
        }
    
        void updateAllZones()
        {
            for (const auto& m : fZoneMap) {
                updateZone(m.first);
            }
        }
    
        static void updateAllGuis()
        {
            for (const auto& g : fGuiList) {
                g->updateAllZones();
            }
        }
    
        void addCallback(FAUSTFLOAT* zone, uiCallback foo, void* data)
        {
            createUiCallbackItem(this, zone, foo, data);
        }

        // Start event or message processing
        virtual bool run() { return false; };
        // Stop event or message processing
        virtual void stop() { fStopped = true; }
        bool stopped() { return fStopped; }
    
        // -- widget's layouts
        
        virtual void openTabBox(const char* label) {}
        virtual void openHorizontalBox(const char* label) {}
        virtual void openVerticalBox(const char* label) {}
        virtual void closeBox() {}
        
        // -- active widgets
        
        virtual void addButton(const char* label, FAUSTFLOAT* zone) {}
        virtual void addCheckButton(const char* label, FAUSTFLOAT* zone) {}
        virtual void addVerticalSlider(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step) {}
        virtual void addHorizontalSlider(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step) {}
        virtual void addNumEntry(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step) {}
    
        // -- passive widgets
        
        virtual void addHorizontalBargraph(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT min, FAUSTFLOAT max) {}
        virtual void addVerticalBargraph(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT min, FAUSTFLOAT max) {}
    
        // -- soundfiles
    
        virtual void addSoundfile(const char* label, const char* filename, Soundfile** sf_zone) {}
    
        // -- metadata declarations

        virtual void declare(FAUSTFLOAT*, const char*, const char*) {}
    
        // Static global for timed zones, shared between all UI that will set timed values
        static ztimedmap gTimedZoneMap;

};

/**
 * User Interface Item: abstract definition.
 */
template <typename REAL>
class uiTypedItemReal : public uiItemBase
{
    protected:
        
        GUI* fGUI;
        REAL* fZone;
        REAL fCache;
        
        uiTypedItemReal(GUI* ui, REAL* zone):uiItemBase(ui, static_cast<FAUSTFLOAT*>(zone)),
        fGUI(ui), fZone(zone), fCache(REAL(-123456.654321))
        {
            ui->registerZone(zone, this);
        }
        
    public:
        
        virtual ~uiTypedItemReal()
        {}
    
        void modifyZone(REAL v)
        {
            fCache = v;
            if (*fZone != v) {
                *fZone = v;
                fGUI->updateZone(fZone);
            }
        }
    
        double cache() { return fCache; }
    
};

class uiItem : public uiTypedItemReal<FAUSTFLOAT> {
    
    protected:
    
        uiItem(GUI* ui, FAUSTFLOAT* zone):uiTypedItemReal<FAUSTFLOAT>(ui, zone)
        {}

    public:

        virtual ~uiItem() 
        {}

		void modifyZone(FAUSTFLOAT v)
		{
			fCache = v;
			if (*fZone != v) {
				*fZone = v;
				fGUI->updateZone(fZone);
			}
		}

};

/**
 * Base class for items with a value converter.
 */
struct uiConverter {
    
    ValueConverter* fConverter;
    
    uiConverter(MetaDataUI::Scale scale, FAUSTFLOAT umin, FAUSTFLOAT umax, FAUSTFLOAT fmin, FAUSTFLOAT fmax)
    {
        // Select appropriate converter according to scale mode
        if (scale == MetaDataUI::kLog) {
            fConverter = new LogValueConverter(umin, umax, fmin, fmax);
        } else if (scale == MetaDataUI::kExp) {
            fConverter = new ExpValueConverter(umin, umax, fmin, fmax);
        } else {
            fConverter = new LinearValueConverter(umin, umax, fmin, fmax);
        }
    }
    
    virtual ~uiConverter()
    {
        delete fConverter;
    }
};

/**
 * User Interface item owned (and so deleted) by external code.
 */
class uiOwnedItem : public uiItem {
    
    protected:
    
        uiOwnedItem(GUI* ui, FAUSTFLOAT* zone):uiItem(ui, zone)
        {}
    
     public:
    
        virtual ~uiOwnedItem()
        {}
    
        virtual void reflectZone() {}
};

/**
 * Callback Item.
 */
class uiCallbackItem : public uiItem {
    
    protected:
    
        uiCallback fCallback;
        void* fData;
    
    public:
    
        uiCallbackItem(GUI* ui, FAUSTFLOAT* zone, uiCallback foo, void* data)
        : uiItem(ui, zone), fCallback(foo), fData(data) {}
        
        virtual void reflectZone() 
        {		
            FAUSTFLOAT v = *fZone;
            fCache = v; 
            fCallback(v, fData);	
        }
};

/**
 *  For timestamped control.
 */
struct DatedControl {
    
    double fDate;
    FAUSTFLOAT fValue;
    
    DatedControl(double d = 0., FAUSTFLOAT v = FAUSTFLOAT(0)):fDate(d), fValue(v) {}
    
};

/**
 * Base class for timed items.
 */
class uiTimedItem : public uiItem
{
    
    protected:
        
        bool fDelete;
        
    public:
    
        using uiItem::modifyZone;
        
        uiTimedItem(GUI* ui, FAUSTFLOAT* zone):uiItem(ui, zone)
        {
            if (GUI::gTimedZoneMap.find(fZone) == GUI::gTimedZoneMap.end()) {
                GUI::gTimedZoneMap[fZone] = ringbuffer_create(8192);
                fDelete = true;
            } else {
                fDelete = false;
            }
        }
        
        virtual ~uiTimedItem()
        {
            ztimedmap::iterator it;
            if (fDelete && ((it = GUI::gTimedZoneMap.find(fZone)) != GUI::gTimedZoneMap.end())) {
                ringbuffer_free((*it).second);
                GUI::gTimedZoneMap.erase(it);
            }
        }
        
        virtual void modifyZone(double date, FAUSTFLOAT v)
        {
            size_t res;
            DatedControl dated_val(date, v);
            if ((res = ringbuffer_write(GUI::gTimedZoneMap[fZone], (const char*)&dated_val, sizeof(DatedControl))) != sizeof(DatedControl)) {
                fprintf(stderr, "ringbuffer_write error DatedControl\n");
            }
        }
    
};

/**
 * Allows to group a set of zones.
 */
class uiGroupItem : public uiItem
{
    protected:
    
        std::vector<FAUSTFLOAT*> fZoneMap;

    public:
    
        uiGroupItem(GUI* ui, FAUSTFLOAT* zone):uiItem(ui, zone)
        {}
        virtual ~uiGroupItem() 
        {}
        
        virtual void reflectZone() 
        {
            FAUSTFLOAT v = *fZone;
            fCache = v;
            
            // Update all zones of the same group
            for (const auto& it : fZoneMap) {
                *it = v;
            }
        }
        
        void addZone(FAUSTFLOAT* zone) { fZoneMap.push_back(zone); }

};

// Cannot be defined as method in the classes.

static void createUiCallbackItem(GUI* ui, FAUSTFLOAT* zone, uiCallback foo, void* data)
{
    new uiCallbackItem(ui, zone, foo, data);
}

static void deleteClist(clist* cl)
{
    for (const auto& it : *cl) {
        // This specific code is only used in JUCE context. TODO: use proper 'shared_ptr' based memory management.
    #if defined(JUCE_32BIT) || defined(JUCE_64BIT)
        uiOwnedItem* owned = dynamic_cast<uiOwnedItem*>(it);
        // owned items are deleted by external code
        if (!owned) {
            delete it;
        }
    #else
        delete it;
    #endif
    }
}

#endif
/**************************  END  GUI.h **************************/
/*

  Copyright (C) 2011 Grame

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

  Grame Research Laboratory, 9 rue du Garet, 69001 Lyon - France
  research@grame.fr

*/

#ifndef __RootNode__
#define __RootNode__

#include <map>
#include <string>
#include <vector>

/************************** BEGIN JSONUI.h *****************************
 FAUST Architecture File
 Copyright (C) 2003-2022 GRAME, Centre National de Creation Musicale
 ---------------------------------------------------------------------
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation; either version 2.1 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 
 EXCEPTION : As a special exception, you may create a larger work
 that contains this FAUST architecture section and distribute
 that work under terms of your choice, so long as this FAUST
 architecture section is not modified.
 ************************************************************************/

#ifndef FAUST_JSONUI_H
#define FAUST_JSONUI_H

#include <vector>
#include <map>
#include <string>
#include <iomanip>
#include <sstream>
#include <algorithm>
#include <limits>


/*******************************************************************************
 * JSONUI : Faust User Interface
 * This class produce a complete JSON decription of the DSP instance.
 *
 * Since 'shortname' can only be computed when all paths have been created,
 * the fAllUI vector is progressively filled with partially built UI items,
 * which are finally created in the JSON(...) method.
 ******************************************************************************/

// Instruction complexity statistics
struct InstComplexity {
    
    int fLoad = 0;
    int fStore = 0;
    int fBinop = 0;
    int fMathop = 0;
    int fNumbers = 0;
    int fDeclare = 0;
    int fCast = 0;
    int fSelect = 0;
    int fLoop = 0;
    
    std::map<std::string, int> fFunctionSymbolTable;
    std::map<std::string, int> fBinopSymbolTable;
   
    InstComplexity operator+(const InstComplexity& icomp)
    {
        fLoad += icomp.fLoad;
        fStore += icomp.fStore;
        fBinop += icomp.fBinop;
        fMathop += icomp.fMathop;
        fNumbers += icomp.fNumbers;
        fDeclare += icomp.fDeclare;
        fCast += icomp.fCast;
        fSelect += icomp.fSelect;
        fLoop += icomp.fLoop;
        return *this;
    }
};

// DSP or field name, type, size, size-in-bytes, reads, writes
typedef std::tuple<std::string, std::string, int, int, int, int> MemoryLayoutItem;
typedef std::vector<MemoryLayoutItem> MemoryLayoutType;
typedef std::map<std::string, int> PathTableType;

/*
    Build a JSON description of the DSP.
 */
template <typename REAL>
class FAUST_API JSONUIReal : public PathBuilder, public Meta, public UIReal<REAL> {

    protected:
    
        std::stringstream fUI;
        std::vector<std::string> fAllUI;
        std::stringstream fMeta;
        std::vector<std::pair <std::string, std::string> > fMetaAux;
        std::string fVersion;           // Compiler version
        std::string fCompileOptions;    // Compilation options
        std::vector<std::string> fLibraryList;
        std::vector<std::string> fIncludePathnames;
        std::string fName;
        std::string fFileName;
        std::string fExpandedCode;
        std::string fSHAKey;
        std::string fJSON;
        int fDSPSize;                   // In bytes
        PathTableType fPathTable;
        MemoryLayoutType fMemoryLayout;
        InstComplexity fIComp;
        bool fExtended;
    
        char fCloseUIPar;
        char fCloseMetaPar;
        int fTab;
    
        int fInputs, fOutputs, fSRIndex;
         
        void tab(int n, std::ostream& fout)
        {
            fout << '\n';
            while (n-- > 0) {
                fout << '\t';
            }
        }
    
        std::string flatten(const std::string& src)
        {
            std::string dst;
            for (size_t i = 0; i < src.size(); i++) {
                switch (src[i]) {
                    case '\n':
                    case '\t':
                        break;
                    default:
                        dst += src[i];
                        break;
                }
            }
            return dst;
        }
    
        void addMeta(int tab_val, bool quote = true)
        {
            if (fMetaAux.size() > 0) {
                tab(tab_val, fUI); fUI << "\"meta\": [";
                std::string sep = "";
                for (size_t i = 0; i < fMetaAux.size(); i++) {
                    fUI << sep;
                    tab(tab_val + 1, fUI); fUI << "{ \"" << fMetaAux[i].first << "\": \"" << fMetaAux[i].second << "\" }";
                    sep = ",";
                }
                tab(tab_val, fUI); fUI << ((quote) ? "],": "]");
                fMetaAux.clear();
            }
        }
    
        int getAddressIndex(const std::string& path)
        {
            return (fPathTable.find(path) != fPathTable.end()) ? fPathTable[path] : -1;
        }
      
     public:
     
        JSONUIReal(const std::string& name,
                  const std::string& filename,
                  int inputs,
                  int outputs,
                  int sr_index,
                  const std::string& sha_key,
                  const std::string& dsp_code,
                  const std::string& version,
                  const std::string& compile_options,
                  const std::vector<std::string>& library_list,
                  const std::vector<std::string>& include_pathnames,
                  int size,
                  const PathTableType& path_table,
                  MemoryLayoutType memory_layout,
                  InstComplexity inst_comp)
        {
            init(name, filename, inputs, outputs, sr_index, sha_key, dsp_code, version, compile_options, library_list, include_pathnames, size, path_table, memory_layout, inst_comp);
        }

        JSONUIReal(const std::string& name, const std::string& filename, int inputs, int outputs)
        {
            init(name, filename, inputs, outputs, -1, "", "", "", "", std::vector<std::string>(), std::vector<std::string>(), -1, PathTableType(), MemoryLayoutType(), InstComplexity());
        }

        JSONUIReal(int inputs, int outputs)
        {
            init("", "", inputs, outputs, -1, "", "","", "", std::vector<std::string>(), std::vector<std::string>(), -1, PathTableType(), MemoryLayoutType(), InstComplexity());
        }
        
        JSONUIReal()
        {
            init("", "", -1, -1, -1, "", "", "", "", std::vector<std::string>(), std::vector<std::string>(), -1, PathTableType(), MemoryLayoutType(), InstComplexity());
        }
 
        virtual ~JSONUIReal() {}
        
        void setInputs(int inputs) { fInputs = inputs; }
        void setOutputs(int outputs) { fOutputs = outputs; }
    
        void setSRIndex(int sr_index) { fSRIndex = sr_index; }
    
        // Init may be called multiple times so fMeta and fUI are reinitialized
        void init(const std::string& name,
                  const std::string& filename,
                  int inputs,
                  int outputs,
                  int sr_index,
                  const std::string& sha_key,
                  const std::string& dsp_code,
                  const std::string& version,
                  const std::string& compile_options,
                  const std::vector<std::string>& library_list,
                  const std::vector<std::string>& include_pathnames,
                  int size,
                  const PathTableType& path_table,
                  MemoryLayoutType memory_layout,
                  InstComplexity inst_comp,
                  bool extended = false)
        {
            fTab = 1;
            fExtended = extended;
            if (fExtended) {
                fUI << std::setprecision(std::numeric_limits<REAL>::max_digits10);
                fMeta << std::setprecision(std::numeric_limits<REAL>::max_digits10);
            }
        
            fIComp = inst_comp;
            
            // Start Meta generation
            fMeta.str("");
            tab(fTab, fMeta); fMeta << "\"meta\": [";
            fCloseMetaPar = ' ';
            
            // Start UI generation
            fUI.str("");
            tab(fTab, fUI); fUI << "\"ui\": [";
            fCloseUIPar = ' ';
            fTab += 1;
            
            fName = name;
            fFileName = filename;
            fInputs = inputs;
            fOutputs = outputs;
            fSRIndex = sr_index;
            fExpandedCode = dsp_code;
            fSHAKey = sha_key;
            fDSPSize = size;
            fPathTable = path_table;
            fVersion = version;
            fCompileOptions = compile_options;
            fLibraryList = library_list;
            fIncludePathnames = include_pathnames;
            fMemoryLayout = memory_layout;
        }
   
        // -- widget's layouts
    
        virtual void openGenericBox(const char* label, const char* name)
        {
            pushLabel(label);
            fUI << fCloseUIPar;
            tab(fTab, fUI); fUI << "{";
            fTab += 1;
            tab(fTab, fUI); fUI << "\"type\": \"" << name << "\",";
            tab(fTab, fUI); fUI << "\"label\": \"" << label << "\",";
            addMeta(fTab);
            tab(fTab, fUI); fUI << "\"items\": [";
            fCloseUIPar = ' ';
            fTab += 1;
        }

        virtual void openTabBox(const char* label)
        {
            openGenericBox(label, "tgroup");
        }
    
        virtual void openHorizontalBox(const char* label)
        {
            openGenericBox(label, "hgroup");
        }
    
        virtual void openVerticalBox(const char* label)
        {
            openGenericBox(label, "vgroup");
        }
    
        virtual void closeBox()
        {
            if (popLabel()) {
                // Shortnames can be computed when all fullnames are known
                computeShortNames();
            }
            fTab -= 1;
            tab(fTab, fUI); fUI << "]";
            fTab -= 1;
            tab(fTab, fUI); fUI << "}";
            fCloseUIPar = ',';
        }
    
        // -- active widgets
  
        virtual void addGenericButton(const char* label, const char* name)
        {
            std::string path = buildPath(label);
            fFullPaths.push_back(path);
            
            fUI << fCloseUIPar;
            tab(fTab, fUI); fUI << "{";
            fTab += 1;
            tab(fTab, fUI); fUI << "\"type\": \"" << name << "\",";
            tab(fTab, fUI); fUI << "\"label\": \"" << label << "\",";
        
            // Generate 'shortname' entry
            tab(fTab, fUI); fUI << "\"shortname\": \"";
        
            // Add fUI section
            fAllUI.push_back(fUI.str());
            fUI.str("");
        
            if (fPathTable.size() > 0) {
                tab(fTab, fUI); fUI << "\"address\": \"" << path << "\",";
                tab(fTab, fUI); fUI << "\"index\": " << getAddressIndex(path) << ((fMetaAux.size() > 0) ? "," : "");
            } else {
                tab(fTab, fUI); fUI << "\"address\": \"" << path << "\"" << ((fMetaAux.size() > 0) ? "," : "");
            }
            addMeta(fTab, false);
            fTab -= 1;
            tab(fTab, fUI); fUI << "}";
            fCloseUIPar = ',';
        }

        virtual void addButton(const char* label, REAL* zone)
        {
            addGenericButton(label, "button");
        }
    
        virtual void addCheckButton(const char* label, REAL* zone)
        {
            addGenericButton(label, "checkbox");
        }

        virtual void addGenericRange(const char* label, const char* name, REAL init, REAL min, REAL max, REAL step)
        {
            std::string path = buildPath(label);
            fFullPaths.push_back(path);
            
            fUI << fCloseUIPar;
            tab(fTab, fUI); fUI << "{";
            fTab += 1;
            tab(fTab, fUI); fUI << "\"type\": \"" << name << "\",";
            tab(fTab, fUI); fUI << "\"label\": \"" << label << "\",";
         
            // Generate 'shortname' entry
            tab(fTab, fUI); fUI << "\"shortname\": \"";
        
            // Add fUI section
            fAllUI.push_back(fUI.str());
            fUI.str("");
        
            tab(fTab, fUI); fUI << "\"address\": \"" << path << "\",";
            if (fPathTable.size() > 0) {
                tab(fTab, fUI); fUI << "\"index\": " << getAddressIndex(path) << ",";
            }
            addMeta(fTab);
            tab(fTab, fUI); fUI << "\"init\": " << init << ",";
            tab(fTab, fUI); fUI << "\"min\": " << min << ",";
            tab(fTab, fUI); fUI << "\"max\": " << max << ",";
            tab(fTab, fUI); fUI << "\"step\": " << step;
            fTab -= 1;
            tab(fTab, fUI); fUI << "}";
            fCloseUIPar = ',';
        }
    
        virtual void addVerticalSlider(const char* label, REAL* zone, REAL init, REAL min, REAL max, REAL step)
        {
            addGenericRange(label, "vslider", init, min, max, step);
        }
    
        virtual void addHorizontalSlider(const char* label, REAL* zone, REAL init, REAL min, REAL max, REAL step)
        {
            addGenericRange(label, "hslider", init, min, max, step);
        }
    
        virtual void addNumEntry(const char* label, REAL* zone, REAL init, REAL min, REAL max, REAL step)
        {
            addGenericRange(label, "nentry", init, min, max, step);
        }

        // -- passive widgets
    
        virtual void addGenericBargraph(const char* label, const char* name, REAL min, REAL max) 
        {
            std::string path = buildPath(label);
            fFullPaths.push_back(path);
            
            fUI << fCloseUIPar;
            tab(fTab, fUI); fUI << "{";
            fTab += 1;
            tab(fTab, fUI); fUI << "\"type\": \"" << name << "\",";
            tab(fTab, fUI); fUI << "\"label\": \"" << label << "\",";
         
            // Generate 'shortname' entry
            tab(fTab, fUI); fUI << "\"shortname\": \"";
        
            // Add fUI section
            fAllUI.push_back(fUI.str());
            fUI.str("");
            
            tab(fTab, fUI); fUI << "\"address\": \"" << path << "\",";
            if (fPathTable.size() > 0) {
                tab(fTab, fUI); fUI << "\"index\": " << getAddressIndex(path) << ",";
            }
            addMeta(fTab);
            tab(fTab, fUI); fUI << "\"min\": " << min << ",";
            tab(fTab, fUI); fUI << "\"max\": " << max;
            fTab -= 1;
            tab(fTab, fUI); fUI << "}";
            fCloseUIPar = ',';
        }

        virtual void addHorizontalBargraph(const char* label, REAL* zone, REAL min, REAL max) 
        {
            addGenericBargraph(label, "hbargraph", min, max);
        }
    
        virtual void addVerticalBargraph(const char* label, REAL* zone, REAL min, REAL max)
        {
            addGenericBargraph(label, "vbargraph", min, max);
        }
    
        virtual void addSoundfile(const char* label, const char* url, Soundfile** zone)
        {
            std::string path = buildPath(label);
            
            fUI << fCloseUIPar;
            tab(fTab, fUI); fUI << "{";
            fTab += 1;
            tab(fTab, fUI); fUI << "\"type\": \"" << "soundfile" << "\",";
            tab(fTab, fUI); fUI << "\"label\": \"" << label << "\"" << ",";
            tab(fTab, fUI); fUI << "\"url\": \"" << url << "\"" << ",";
            tab(fTab, fUI); fUI << "\"address\": \"" << path << "\"" << ((fPathTable.size() > 0) ? "," : "");
            if (fPathTable.size() > 0) {
                tab(fTab, fUI); fUI << "\"index\": " << getAddressIndex(path);
            }
            fTab -= 1;
            tab(fTab, fUI); fUI << "}";
            fCloseUIPar = ',';
        }

        // -- metadata declarations

        virtual void declare(REAL* zone, const char* key, const char* val)
        {
            fMetaAux.push_back(std::make_pair(key, val));
        }
    
        // Meta interface
        virtual void declare(const char* key, const char* value)
        {
            fMeta << fCloseMetaPar;
            // fName found in metadata
            if ((strcmp(key, "name") == 0) && (fName == "")) fName = value;
            // fFileName found in metadata
            if ((strcmp(key, "filename") == 0) && (fFileName == "")) fFileName = value;
            tab(fTab, fMeta); fMeta << "{ " << "\"" << key << "\"" << ": " << "\"" << value << "\" }";
            fCloseMetaPar = ',';
        }
    
        std::string JSON(bool flat = false)
        {
            if (fJSON.empty()) {
                fTab = 0;
                std::stringstream JSON;
                if (fExtended) {
                    JSON << std::setprecision(std::numeric_limits<REAL>::max_digits10);
                }
                JSON << "{";
                fTab += 1;
                tab(fTab, JSON); JSON << "\"name\": \"" << fName << "\",";
                tab(fTab, JSON); JSON << "\"filename\": \"" << fFileName << "\",";
                if (fVersion != "") { tab(fTab, JSON); JSON << "\"version\": \"" << fVersion << "\","; }
                if (fCompileOptions != "") { tab(fTab, JSON); JSON << "\"compile_options\": \"" <<  fCompileOptions << "\","; }
                if (fLibraryList.size() > 0) {
                    tab(fTab, JSON);
                    JSON << "\"library_list\": [";
                    for (size_t i = 0; i < fLibraryList.size(); i++) {
                        JSON << "\"" << fLibraryList[i] << "\"";
                        if (i < (fLibraryList.size() - 1)) JSON << ",";
                    }
                    JSON << "],";
                }
                if (fIncludePathnames.size() > 0) {
                    tab(fTab, JSON);
                    JSON << "\"include_pathnames\": [";
                    for (size_t i = 0; i < fIncludePathnames.size(); i++) {
                        JSON << "\"" << fIncludePathnames[i] << "\"";
                        if (i < (fIncludePathnames.size() - 1)) JSON << ",";
                    }
                    JSON << "],";
                }
                if (fDSPSize != -1) { tab(fTab, JSON); JSON << "\"size\": " << fDSPSize << ","; }
                if (fMemoryLayout.size() > 0) {
                    tab(fTab, JSON);
                    JSON << "\"memory_layout\": [";
                    for (size_t i = 0; i < fMemoryLayout.size(); i++) {
                        // DSP or field name, type, size, size-in-bytes, reads, writes
                        MemoryLayoutItem item = fMemoryLayout[i];
                        tab(fTab + 1, JSON);
                        JSON << "{ \"name\": \"" << std::get<0>(item) << "\", ";
                        JSON << "\"type\": \"" << std::get<1>(item) << "\", ";
                        JSON << "\"size\": " << std::get<2>(item) << ", ";
                        JSON << "\"size_bytes\": " << std::get<3>(item) << ", ";
                        JSON << "\"read\": " << std::get<4>(item) << ", ";
                        JSON << "\"write\": " << std::get<5>(item) << " }";
                        if (i < (fMemoryLayout.size() - 1)) JSON << ",";
                    }
                    tab(fTab, JSON);
                    JSON << "],";
                    
                    // Compute statistics
                    tab(fTab, JSON);
                    JSON << "\"compute_cost\": [{";
                    tab(fTab + 1, JSON);
                    JSON << "\"load\": " << fIComp.fLoad << ", ";
                    tab(fTab + 1, JSON);
                    JSON << "\"store\": " << fIComp.fStore << ", ";
                    tab(fTab + 1, JSON);
                    JSON << "\"declare\": " << fIComp.fDeclare << ", ";
                    tab(fTab + 1, JSON);
                    JSON << "\"number\": " << fIComp.fNumbers << ", ";
                    tab(fTab + 1, JSON);
                    JSON << "\"cast\": " << fIComp.fCast << ", ";
                    tab(fTab + 1, JSON);
                    JSON << "\"select\": " << fIComp.fSelect << ", ";
                    tab(fTab + 1, JSON);
                    JSON << "\"loop\": " << fIComp.fLoop << ", ";
                    tab(fTab + 1, JSON);
                    JSON << "\"binop\": [{ ";
                    JSON << "\"total\": " << fIComp.fBinop;
                    int size1 = (int)fIComp.fBinopSymbolTable.size();
                    if (size1 > 0) {
                        JSON << ", ";
                        for (const auto& it : fIComp.fBinopSymbolTable) {
                            JSON << "\"" << it.first << "\": " << it.second;
                            JSON << ((--size1 == 0) ? " }" : ", ");
                        }
                    } else {
                        JSON << " }";
                    }
                    JSON << "], ";
                    tab(fTab + 1, JSON);
                    JSON << "\"mathop\": [{ ";
                    JSON << "\"total\": " << fIComp.fMathop;
                    int size2 = (int)fIComp.fFunctionSymbolTable.size();
                    if (size2 > 0) {
                        JSON << ", ";
                        for (const auto& it : fIComp.fFunctionSymbolTable) {
                            JSON << "\"" << it.first << "\": " << it.second;
                            JSON << ((--size2 == 0) ? " }" : ", ");
                        }
                    } else {
                        JSON << " }";
                    }
                    JSON << "]";
                    tab(fTab, JSON);
                    JSON << "}],";
                }
                if (fSHAKey != "") { tab(fTab, JSON); JSON << "\"sha_key\": \"" << fSHAKey << "\","; }
                if (fExpandedCode != "") { tab(fTab, JSON); JSON << "\"code\": \"" << fExpandedCode << "\","; }
                tab(fTab, JSON); JSON << "\"inputs\": " << fInputs << ",";
                tab(fTab, JSON); JSON << "\"outputs\": " << fOutputs << ",";
                if (fSRIndex != -1) { tab(fTab, JSON); JSON << "\"sr_index\": " << fSRIndex << ","; }
                tab(fTab, fMeta); fMeta << "],";
              
                // Add last UI section
                fAllUI.push_back(fUI.str());
                // Finalize UI generation
                fUI.str("");
                // Add N-1 sections
                for (size_t i = 0; i < fAllUI.size()-1; i++) {
                    fUI << fAllUI[i] << fFull2Short[fFullPaths[i]] << "\",";
                }
                // And the last one
                fUI << fAllUI[fAllUI.size()-1];
                // Terminates the UI section
                tab(fTab, fUI); fUI << "]";
            
                fTab -= 1;
                if (fCloseMetaPar == ',') { // If "declare" has been called, fCloseMetaPar state is now ','
                    JSON << fMeta.str() << fUI.str();
                } else {
                    JSON << fUI.str();
                }
                
                tab(fTab, JSON); JSON << "}";
                
                // Keep result in fJSON
                fJSON = JSON.str();
            }
            return (flat) ? flatten(fJSON) : fJSON;
        }
    
};

// Externally available class using FAUSTFLOAT

struct FAUST_API JSONUI : public JSONUIReal<FAUSTFLOAT>, public UI {
    
    JSONUI(const std::string& name,
           const std::string& filename,
           int inputs,
           int outputs,
           int sr_index,
           const std::string& sha_key,
           const std::string& dsp_code,
           const std::string& version,
           const std::string& compile_options,
           const std::vector<std::string>& library_list,
           const std::vector<std::string>& include_pathnames,
           int size,
           const PathTableType& path_table,
           MemoryLayoutType memory_layout,
           InstComplexity inst_comp):
    JSONUIReal<FAUSTFLOAT>(name, filename,
                          inputs, outputs,
                          sr_index,
                          sha_key, dsp_code,
                          version, compile_options,
                          library_list, include_pathnames,
                          size, path_table,
                          memory_layout, inst_comp)
    {}
    
    JSONUI(const std::string& name, const std::string& filename, int inputs, int outputs):
    JSONUIReal<FAUSTFLOAT>(name, filename, inputs, outputs)
    {}
    
    JSONUI(int inputs, int outputs):JSONUIReal<FAUSTFLOAT>(inputs, outputs)
    {}
    
    JSONUI():JSONUIReal<FAUSTFLOAT>()
    {}

    virtual void openTabBox(const char* label)
    {
        JSONUIReal<FAUSTFLOAT>::openTabBox(label);
    }
    virtual void openHorizontalBox(const char* label)
    {
        JSONUIReal<FAUSTFLOAT>::openHorizontalBox(label);
    }
    virtual void openVerticalBox(const char* label)
    {
        JSONUIReal<FAUSTFLOAT>::openVerticalBox(label);
    }
    virtual void closeBox()
    {
        JSONUIReal<FAUSTFLOAT>::closeBox();
    }
    
    // -- active widgets
    
    virtual void addButton(const char* label, FAUSTFLOAT* zone)
    {
        JSONUIReal<FAUSTFLOAT>::addButton(label, zone);
    }
    virtual void addCheckButton(const char* label, FAUSTFLOAT* zone)
    {
        JSONUIReal<FAUSTFLOAT>::addCheckButton(label, zone);
    }
    virtual void addVerticalSlider(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step)
    {
        JSONUIReal<FAUSTFLOAT>::addVerticalSlider(label, zone, init, min, max, step);
    }
    virtual void addHorizontalSlider(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step)
    {
        JSONUIReal<FAUSTFLOAT>::addHorizontalSlider(label, zone, init, min, max, step);
    }
    virtual void addNumEntry(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step)
    {
        JSONUIReal<FAUSTFLOAT>::addNumEntry(label, zone, init, min, max, step);
    }
    
    // -- passive widgets
    
    virtual void addHorizontalBargraph(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT min, FAUSTFLOAT max)
    {
        JSONUIReal<FAUSTFLOAT>::addHorizontalBargraph(label, zone, min, max);
    }
    virtual void addVerticalBargraph(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT min, FAUSTFLOAT max)
    {
        JSONUIReal<FAUSTFLOAT>::addVerticalBargraph(label, zone, min, max);
    }
    
    // -- soundfiles
    
    virtual void addSoundfile(const char* label, const char* filename, Soundfile** sf_zone)
    {
        JSONUIReal<FAUSTFLOAT>::addSoundfile(label, filename, sf_zone);
    }
    
    // -- metadata declarations
    
    virtual void declare(FAUSTFLOAT* zone, const char* key, const char* val)
    {
        JSONUIReal<FAUSTFLOAT>::declare(zone, key, val);
    }

    virtual void declare(const char* key, const char* val)
    {
        JSONUIReal<FAUSTFLOAT>::declare(key, val);
    }

    virtual ~JSONUI() {}
    
};

#endif // FAUST_JSONUI_H
/**************************  END  JSONUI.h **************************/

namespace oscfaust
{

class OSCIO;
class RootNode;
typedef class SMARTP<RootNode> SRootNode;

/**
 * an alias target includes a map to rescale input values to output values
 * and a target osc address. The input values can be given in reversed order
 * to reverse the control
 */
struct aliastarget
{
	double      fMinIn;
	double      fMaxIn;
	double      fMinOut;
	double      fMaxOut;
	std::string fTarget;	// the real osc address

	aliastarget(const char* address, double imin, double imax, double omin, double omax)
		: fMinIn(imin), fMaxIn(imax), fMinOut(omin), fMaxOut(omax), fTarget(address) {}

    aliastarget(const aliastarget& t)
        : fMinIn(t.fMinIn), fMaxIn(t.fMaxIn), fMinOut(t.fMinOut), fMaxOut(t.fMaxOut), fTarget(t.fTarget) {}
    
    // explicit copy assignment operator
    aliastarget& operator=(const aliastarget& other) { return *this; }
    
	double scale(double x) const 
    {
        if (fMinIn < fMaxIn) {
            // increasing control
            double z = (x < fMinIn) ? fMinIn : (x > fMaxIn) ? fMaxIn : x;
            return fMinOut + (z-fMinIn)*(fMaxOut-fMinOut)/(fMaxIn-fMinIn);
            
        } else if (fMinIn > fMaxIn) {
            // reversed control
            double z = (x < fMaxIn) ? fMaxIn : (x > fMinIn) ? fMinIn : x;
            return fMinOut + (fMinIn-z)*(fMaxOut-fMinOut)/(fMinIn-fMaxIn);
            
        } else {
            // no control !
            return (fMinOut+fMaxOut)/2.0f;
        }
    }
    
    double invscale(double x) const
    {
        if (fMinOut < fMaxOut) {
            // increasing control
            double z = (x < fMinOut) ? fMinOut : (x > fMaxOut) ? fMaxOut : x;
            return fMinIn + (z-fMinOut)*(fMaxIn-fMinIn)/(fMaxOut-fMinOut);
            
        } else if (fMinOut > fMaxOut) {
            // reversed control
            double z = (x < fMaxOut) ? fMaxOut : (x > fMinOut) ? fMinOut : x;
            return fMinIn + (fMinOut-z)*(fMaxIn-fMinIn)/(fMinOut-fMaxOut);
            
        } else {
            // no control !
            return (fMinIn+fMaxIn)/2.0f;
        }
    }
};

//--------------------------------------------------------------------------
/*!
	\brief a faust root node

	A Faust root node handles the \c 'hello' message and provides support
	for incoming osc signal data. 
*/
class RootNode : public MessageDriven
{

    private:
        int *fUPDIn, *fUDPOut, *fUDPErr;    // the osc port numbers (required by the hello method)
        OSCIO* fIO;                         // an OSC IO controler
        JSONUI* fJSON;

        typedef std::map<std::string, std::vector<aliastarget> > TAliasMap;
        TAliasMap fAliases;

        template <typename T>
        void processAliasAux(const std::string& address, T val);
        void processAlias(const std::string& address, float val);
        void processAlias(const std::string& address, double val);
    
        void eraseAliases(const std::string& target);
        void eraseAlias(const std::string& target, const std::string& alias);
        bool aliasError(const Message* msg);

    protected:
        RootNode(const char *name, JSONUI* json, OSCIO* io = NULL) : MessageDriven(name, ""), fUPDIn(0), fUDPOut(0), fUDPErr(0), fIO(io), fJSON(json) {}
        virtual ~RootNode() {}

    public:
        static SRootNode create(const char* name, JSONUI* json, OSCIO* io = NULL) { return new RootNode(name, json, io); }

        virtual void processMessage(const Message* msg);
        virtual bool accept(const Message* msg);
        virtual void get(unsigned long ipdest) const;
        virtual void get(unsigned long ipdest, const std::string& what) const;

        template <typename T>
        bool aliasMsgAux(const Message* msg, T omin, T omax);
        bool aliasMsg(const Message* msg, double omin, double omax);
        bool aliasMsg(const Message* msg, float omin, float omax);
    
        template <typename T>
        void addAliasAux(const char* alias, const char* address, T imin, T imax, T omin, T omax);
        void addAlias(const char* alias, const char* address, float imin, float imax, float omin, float omax);
        void addAlias(const char* alias, const char* address, double imin, double imax, double omin, double omax);
    
        bool acceptSignal(const Message* msg);      ///< handler for signal data
        void hello(unsigned long ipdest) const;     ///< handler for the 'hello' message
        void setPorts(int* in, int* out, int* err);

        std::vector<std::pair<std::string, double> > getAliases(const std::string& address, double value);
};

} // end namespoace

#endif

namespace oscfaust
{

/**
 * map (rescale) input values to output values
 */
template <typename C> struct mapping
{
	const C fMinOut;
	const C fMaxOut;
	mapping(C omin, C omax) : fMinOut(omin), fMaxOut(omax) {}
	C clip (C x) { return (x < fMinOut) ? fMinOut : (x > fMaxOut) ? fMaxOut : x; }
};

//--------------------------------------------------------------------------
/*!
	\brief a faust node is a terminal node and represents a faust parameter controler
*/
template <typename C> class FaustNode : public MessageDriven, public uiTypedItemReal<C>
{
	mapping<C>	fMapping;
    RootNode*   fRoot;
    bool        fInput;  // true for input nodes (slider, button...)
	
	//---------------------------------------------------------------------
	// Warning !!!
	// The cast (C*)fZone is necessary because the real size allocated is
	// only known at execution time. When the library is compiled, fZone is
	// uniquely defined by FAUSTFLOAT.
	//---------------------------------------------------------------------
	bool store(C val) { *(C*)this->fZone = fMapping.clip(val); return true; }
	void sendOSC() const;

	protected:
		FaustNode(RootNode* root, const char *name, C* zone, C init, C min, C max, const char* prefix, GUI* ui, bool initZone, bool input) 
			: MessageDriven(name, prefix), uiTypedItemReal<C>(ui, zone), fMapping(min, max), fRoot(root), fInput(input)
			{
                if (initZone) {
                    *zone = init; 
                }
            }
			
		virtual ~FaustNode() {}

	public:
		typedef SMARTP<FaustNode<C> > SFaustNode;
		static SFaustNode create(RootNode* root, const char* name, C* zone, C init, C min, C max, const char* prefix, GUI* ui, bool initZone, bool input)	
        { 
            SFaustNode node = new FaustNode(root, name, zone, init, min, max, prefix, ui, initZone, input); 
            /*
                Since FaustNode is a subclass of uiItem, the pointer will also be kept in the GUI class, and it's desallocation will be done there.
                So we don't want to have smartpointer logic desallocate it and we increment the refcount.
            */
            node->addReference();
            return node; 
        }
    
		bool accept(const Message* msg);
		void get(unsigned long ipdest) const;		///< handler for the 'get' message
		virtual void reflectZone() { sendOSC(); this->fCache = *this->fZone; }
};

} // end namespace

#endif

class GUI;
namespace oscfaust
{

class OSCIO;
class RootNode;
typedef class SMARTP<RootNode> SRootNode;
class MessageDriven;
typedef class SMARTP<MessageDriven>	SMessageDriven;

//--------------------------------------------------------------------------
/*!
	\brief a factory to build a OSC UI hierarchy
	
	Actually, makes use of a stack to build the UI hierarchy.
	It includes a pointer to a OSCIO controler, but just to give it to the root node.
*/
class FaustFactory
{
    std::stack<SMessageDriven>  fNodes;		///< maintains the current hierarchy level
    SRootNode  fRoot;   ///< keep track of the root node
    OSCIO*     fIO;     ///< hack to support audio IO via OSC, actually the field is given to the root node
    GUI*       fGUI;    ///< a GUI pointer to support updateAllGuis(), required for bi-directionnal OSC
    JSONUI*    fJSON;
    
    private:
        std::string addressFirst(const std::string& address) const;
        std::string addressTail(const std::string& address) const;
        
    public:
        FaustFactory(GUI* ui, JSONUI* json, OSCIO * io = NULL);
        virtual ~FaustFactory();
        
        template <typename C> void addnode(const char* label, C* zone, C init, C min, C max, bool initZone, bool input);
        template <typename C> void addAlias(const std::string& fullpath, C* zone, C imin, C imax, C init, C min, C max, const char* label);
        
        void addAlias(const char* alias, const char* address, float imin, float imax, float omin, float omax);
        void addAlias(const char* alias, const char* address, double imin, double imax, double omin, double omax);
        void opengroup(const char* label);
        void closegroup();
        
        SRootNode root() const; 
};

/**
 * Add a node to the OSC UI tree in the current group at the top of the stack 
 */
template <typename C> void FaustFactory::addnode(const char* label, C* zone, C init, C min, C max, bool initZone, bool input) 
{
	SMessageDriven top;
	if (fNodes.size()) top = fNodes.top();
	if (top) {
		std::string prefix = top->getOSCAddress();
		top->add(FaustNode<C>::create(root(), label, zone, init, min, max, prefix.c_str(), fGUI, initZone, input));
	}
}

/**
 * Add an alias (actually stored and handled at root node level
 */
template <typename C> void FaustFactory::addAlias(const std::string& fullpath, C* zone, C imin, C imax, C init, C min, C max, const char* label)
{
	std::istringstream 	ss(fullpath);
	std::string 		realpath; 
 
	ss >> realpath >> imin >> imax;
	SMessageDriven top = fNodes.top();
	if (top) {
		std::string target = top->getOSCAddress() + "/" + label;
		addAlias(realpath.c_str(), target.c_str(), C(imin), C(imax), C(min), C(max));
	}
}

} // end namespoace

#endif

class GUI;

typedef void (*ErrorCallback)(void*);  

namespace oscfaust
{

class OSCIO;
class OSCSetup;
class OSCRegexp;
    
//--------------------------------------------------------------------------
/*!
	\brief the main Faust OSC Lib API
	
	The OSCControler is essentially a glue between the memory representation (in charge of the FaustFactory),
	and the network services (in charge of OSCSetup).
*/
class OSCControler
{
	int fUDPPort,   fUDPOut, fUPDErr;	// the udp ports numbers
	std::string     fDestAddress;		// the osc messages destination address, used at initialization only
										// to collect the address from the command line
	std::string     fBindAddress;		// when non empty, the address used to bind the socket for listening
	OSCSetup*		fOsc;				// the network manager (handles the udp sockets)
	OSCIO*			fIO;				// hack for OSC IO support (actually only relayed to the factory)
	FaustFactory*	fFactory;			// a factory to build the memory representation

    bool            fInit;
    
	public:
		/*
			base udp port is chosen in an unassigned range from IANA PORT NUMBERS (last updated 2011-01-24)
			see at http://www.iana.org/assignments/port-numbers
			5507-5552  Unassigned
		*/
		enum { kUDPBasePort = 5510 };
            
        OSCControler(int argc, char* argv[], GUI* ui, JSONUI* json, OSCIO* io = NULL, ErrorCallback errCallback = NULL, void* arg = NULL, bool init = true);

        virtual ~OSCControler();
	
		//--------------------------------------------------------------------------
		// addnode, opengroup and closegroup are simply relayed to the factory
		//--------------------------------------------------------------------------
		// Add a node in the current group (top of the group stack)
		template <typename T> void addnode(const char* label, T* zone, T init, T min, T max, bool input = true)
							{ fFactory->addnode(label, zone, init, min, max, fInit, input); }
		
		//--------------------------------------------------------------------------
		// This method is used for alias messages. The arguments imin and imax allow
		// to map incomming values from the alias input range to the actual range 
		template <typename T> void addAlias(const std::string& fullpath, T* zone, T imin, T imax, T init, T min, T max, const char* label)
							{ fFactory->addAlias(fullpath, zone, imin, imax, init, min, max, label); }

		void opengroup(const char* label)		{ fFactory->opengroup(label); }
		void closegroup()						{ fFactory->closegroup(); }
	   
		//--------------------------------------------------------------------------
		void run();				// starts the network services
		void endBundle();		// when bundle mode is on, close and send the current bundle (if any)
		void stop();			// stop the network services
		std::string getInfos() const; // gives information about the current environment (version, port numbers,...)

		int	getUDPPort() const			{ return fUDPPort; }
		int	getUDPOut()	const			{ return fUDPOut; }
		int	getUDPErr()	const			{ return fUPDErr; }
		const char*	getDestAddress() const { return fDestAddress.c_str(); }
		const char*	getRootName() const;	// probably useless, introduced for UI extension experiments
    
        void setUDPPort(int port) { fUDPPort = port; }
        void setUDPOut(int port) { fUDPOut = port; }
        void setUDPErr(int port) { fUPDErr = port; }
        void setDestAddress(const char* address) { fDestAddress = address; }

        // By default, an osc interface emits all parameters. You can filter specific params dynamically.
        static std::vector<OSCRegexp*>     fFilteredPaths; // filtered paths will not be emitted
        static void addFilteredPath(std::string path);
        static bool isPathFiltered(std::string path);
        static void resetFilteredPaths();
    
		static float version();				// the Faust OSC library version number
		static const char* versionstr();	// the Faust OSC library version number as a string
		static int gXmit;                   // a static variable to control the transmission of values
                                            // i.e. the use of the interface as a controler
		static int gBundle;                 // a static variable to control the osc bundle mode
};

#define kNoXmit     0
#define kAll        1
#define kAlias      2

}

#endif

#ifdef _WIN32
#define strcasecmp _stricmp
#endif

/******************************************************************************
 *******************************************************************************
 
 OSC (Open Sound Control) USER INTERFACE
 
 *******************************************************************************
 *******************************************************************************/
/*
 
 Note about the OSC addresses and the Faust UI names:
 ----------------------------------------------------
 There are potential conflicts between the Faust UI objects naming scheme and
 the OSC address space. An OSC symbolic names is an ASCII string consisting of
 printable characters other than the following:
	space
 #	number sign
 *	asterisk
 ,	comma
 /	forward
 ?	question mark
 [	open bracket
 ]	close bracket
 {	open curly brace
 }	close curly brace
 
 a simple solution to address the problem consists in replacing
 space or tabulation with '_' (underscore)
 all the other osc excluded characters with '-' (hyphen)
 
 This solution is implemented in the proposed OSC UI;
 */

class OSCUI : public GUI
{
    
    private:
        
        oscfaust::OSCControler*	fCtrl;
        std::vector<const char*> fAlias;
        JSONUI fJSON;
        
        const char* tr(const char* label) const
        {
            static char buffer[1024];
            char* ptr = buffer; int n = 1;
            while (*label && (n++ < 1024)) {
                switch (*label) {
                    case ' ': case '	':
                        *ptr++ = '_';
                        break;
                    case '#': case '*': case ',': case '/': case '?':
                    case '[': case ']': case '{': case '}': case '(': case ')':
                        *ptr++ = '_';
                        break;
                    default:
                        *ptr++ = *label;
                }
                label++;
            }
            *ptr = 0;
            return buffer;
        }
        
        // add all accumulated alias
        void addalias(FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, const char* label)
        {
            for (unsigned int i = 0; i < fAlias.size(); i++) {
                fCtrl->addAlias(fAlias[i], zone, FAUSTFLOAT(0), FAUSTFLOAT(1), init, min, max, label);
            }
            fAlias.clear();
        }
        
    public:
        
        OSCUI(const char* /*applicationname*/, int argc, char* argv[],
              oscfaust::OSCIO* io = NULL,
              ErrorCallback errCallback = NULL,
              void* arg = NULL,
              bool init = true) : GUI()
        {
            fCtrl = new oscfaust::OSCControler(argc, argv, this, &fJSON, io, errCallback, arg, init);
            // fCtrl->opengroup(applicationname);
        }
        
        virtual ~OSCUI() { delete fCtrl; }
        
        // -- widget's layouts
        
        virtual void openTabBox(const char* label)          { fCtrl->opengroup(tr(label)); fJSON.openTabBox(label); }
        virtual void openHorizontalBox(const char* label)   { fCtrl->opengroup(tr(label)); fJSON.openHorizontalBox(label); }
        virtual void openVerticalBox(const char* label)     { fCtrl->opengroup(tr(label)); fJSON.openVerticalBox(label); }
        virtual void closeBox()                             { fCtrl->closegroup(); fJSON.closeBox(); }
        
        // -- active widgets
        virtual void addButton(const char* label, FAUSTFLOAT* zone)
        {
            const char* l = tr(label);
            addalias(zone, 0, 0, 1, l);
            fCtrl->addnode(l, zone, FAUSTFLOAT(0), FAUSTFLOAT(0), FAUSTFLOAT(1));
            fJSON.addButton(label, zone);
        }
        virtual void addCheckButton(const char* label, FAUSTFLOAT* zone)
        {
            const char* l = tr(label);
            addalias(zone, 0, 0, 1, l);
            fCtrl->addnode(l, zone, FAUSTFLOAT(0), FAUSTFLOAT(0), FAUSTFLOAT(1));
            fJSON.addCheckButton(label, zone);
        }
        virtual void addVerticalSlider(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step)
        {
            const char* l = tr(label);
            addalias(zone, init, min, max, l);
            fCtrl->addnode(l, zone, init, min, max);
            fJSON.addVerticalSlider(label, zone, init, min, max, step);
        }
        virtual void addHorizontalSlider(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step)
        {
            const char* l = tr(label);
            addalias(zone, init, min, max, l);
            fCtrl->addnode(l, zone, init, min, max);
            fJSON.addHorizontalSlider(label, zone, init, min, max, step);
        }
        virtual void addNumEntry(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step)
        {
            const char* l = tr(label);
            addalias(zone, init, min, max, l);
            fCtrl->addnode(l, zone, init, min, max);
            fJSON.addNumEntry(label, zone, init, min, max, step);
        }
        
        // -- passive widgets
        
        virtual void addHorizontalBargraph(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT min, FAUSTFLOAT max)
        {
            const char* l = tr(label);
            addalias(zone, 0, min, max, l);
            fCtrl->addnode(l, zone, FAUSTFLOAT(0), min, max, false);
            fJSON.addHorizontalBargraph(label, zone, min, max);
        }
        virtual void addVerticalBargraph(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT min, FAUSTFLOAT max)
        {
            const char* l = tr(label);
            addalias(zone, 0, min, max, l);
            fCtrl->addnode(l, zone, FAUSTFLOAT(0), min, max, false);
            fJSON.addVerticalBargraph(label, zone, min, max);
        }
            
        // -- metadata declarations
        
        virtual void declare(FAUSTFLOAT* zone, const char* key, const char* alias)
        {
            if (strcasecmp(key, "OSC") == 0) fAlias.push_back(alias);
            fJSON.declare(zone, key, alias);
        }
        
        bool run()
        {
            fCtrl->run();
            return true;
        }
        
        void stop()			{ fCtrl->stop(); }
        void endBundle() 	{ fCtrl->endBundle(); }
        
        std::string getInfos()          { return fCtrl->getInfos(); }
        
        const char* getRootName()		{ return fCtrl->getRootName(); }
        int getUDPPort()                { return fCtrl->getUDPPort(); }
        int getUDPOut()                 { return fCtrl->getUDPOut(); }
        int getUDPErr()                 { return fCtrl->getUDPErr(); }
        const char* getDestAddress()    { return fCtrl->getDestAddress(); }
        
        void setUDPPort(int port)       { fCtrl->setUDPPort(port); }
        void setUDPOut(int port)        { fCtrl->setUDPOut(port); }
        void setUDPErr(int port)        { fCtrl->setUDPErr(port); }
        void setDestAddress(const char* address)    { return fCtrl->setDestAddress(address); }
    
};

#endif // __OSCUI__
/**************************  END  OSCUI.h **************************/

#ifdef POLY2
#include "effect.h"
#endif

#if SOUNDFILE
/************************** BEGIN SoundUI.h **************************
 FAUST Architecture File
 Copyright (C) 2003-2022 GRAME, Centre National de Creation Musicale
 ---------------------------------------------------------------------
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation; either version 2.1 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 
 EXCEPTION : As a special exception, you may create a larger work
 that contains this FAUST architecture section and distribute
 that work under terms of your choice, so long as this FAUST
 architecture section is not modified.
 ********************************************************************/
 
#ifndef __SoundUI_H__
#define __SoundUI_H__

#include <map>
#include <vector>
#include <string>
#include <iostream>
#include <memory>


#if defined(__APPLE__) && !defined(__VCVRACK__) && !defined(JUCE_32BIT) && !defined(JUCE_64BIT)
#include <CoreFoundation/CFBundle.h>
#endif

// Always included otherwise -i mode later on will not always include it (with the conditional includes)
/************************** BEGIN Soundfile.h **************************
 FAUST Architecture File
 Copyright (C) 2003-2022 GRAME, Centre National de Creation Musicale
 ---------------------------------------------------------------------
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation; either version 2.1 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 
 EXCEPTION : As a special exception, you may create a larger work
 that contains this FAUST architecture section and distribute
 that work under terms of your choice, so long as this FAUST
 architecture section is not modified.
 ********************************************************************/

#ifndef __Soundfile__
#define __Soundfile__

#include <string.h>
#include <string>
#include <vector>

#ifndef FAUSTFLOAT
#define FAUSTFLOAT float
#endif

#define BUFFER_SIZE 1024
#define SAMPLE_RATE 44100
#define MAX_CHAN 64
#define MAX_SOUNDFILE_PARTS 256

#ifdef _MSC_VER
#define PRE_PACKED_STRUCTURE __pragma(pack(push, 1))
#define POST_PACKED_STRUCTURE \
    ;                         \
    __pragma(pack(pop))
#else
#define PRE_PACKED_STRUCTURE
#define POST_PACKED_STRUCTURE __attribute__((__packed__))
#endif

/*
 The soundfile structure to be used by the DSP code. Soundfile has a MAX_SOUNDFILE_PARTS parts 
 (even a single soundfile or an empty soundfile). 
 The fLength, fOffset and fSR fields are filled accordingly by repeating the actual parts if needed.
 The fBuffers contains MAX_CHAN non-interleaved arrays of samples.
 
 It has to be 'packed' to that the LLVM backend can correctly access it.

 Index computation:
    - p is the current part number [0..MAX_SOUNDFILE_PARTS-1] (must be proved by the type system)
    - i is the current position in the part. It will be constrained between [0..length]
    - idx(p,i) = fOffset[p] + max(0, min(i, fLength[p]));
*/

PRE_PACKED_STRUCTURE
struct Soundfile {
    void* fBuffers; // will correspond to a double** or float** pointer chosen at runtime
    int* fLength;   // length of each part (so fLength[P] contains the length in frames of part P)
    int* fSR;       // sample rate of each part (so fSR[P] contains the SR of part P)
    int* fOffset;   // offset of each part in the global buffer (so fOffset[P] contains the offset in frames of part P)
    int fChannels;  // max number of channels of all concatenated files
    int fParts;     // the total number of loaded parts
    bool fIsDouble; // keep the sample format (float or double)

    Soundfile(int cur_chan, int length, int max_chan, int total_parts, bool is_double)
    {
        fLength   = new int[MAX_SOUNDFILE_PARTS];
        fSR       = new int[MAX_SOUNDFILE_PARTS];
        fOffset   = new int[MAX_SOUNDFILE_PARTS];
        fIsDouble = is_double;
        fChannels = cur_chan;
        fParts    = total_parts;
        if (fIsDouble) {
            fBuffers = allocBufferReal<double>(cur_chan, length, max_chan);
        } else {
            fBuffers = allocBufferReal<float>(cur_chan, length, max_chan);
        }
    }
    
    template <typename REAL>
    void* allocBufferReal(int cur_chan, int length, int max_chan)
    {
        REAL** buffers = new REAL*[max_chan];
        for (int chan = 0; chan < cur_chan; chan++) {
            buffers[chan] = new REAL[length];
            memset(buffers[chan], 0, sizeof(REAL) * length);
        }
        return buffers;
    }
    
    void copyToOut(int size, int channels, int max_channels, int offset, void* buffer)
    {
        if (fIsDouble) {
            copyToOutReal<double>(size, channels, max_channels, offset, buffer);
       } else {
            copyToOutReal<float>(size, channels, max_channels, offset, buffer);
        }
    }
    
    void shareBuffers(int cur_chan, int max_chan)
    {
        // Share the same buffers for all other channels so that we have max_chan channels available
        if (fIsDouble) {
            for (int chan = cur_chan; chan < max_chan; chan++) {
                static_cast<double**>(fBuffers)[chan] = static_cast<double**>(fBuffers)[chan % cur_chan];
            }
        } else {
            for (int chan = cur_chan; chan < max_chan; chan++) {
                static_cast<float**>(fBuffers)[chan] = static_cast<float**>(fBuffers)[chan % cur_chan];
            }
        }
    }
    
    template <typename REAL>
    void copyToOutReal(int size, int channels, int max_channels, int offset, void* buffer)
    {
        for (int sample = 0; sample < size; sample++) {
            for (int chan = 0; chan < channels; chan++) {
                static_cast<REAL**>(fBuffers)[chan][offset + sample] = static_cast<REAL*>(buffer)[sample * max_channels + chan];
            }
        }
    }
    
    template <typename REAL>
    void getBuffersOffsetReal(void* buffers, int offset)
    {
        for (int chan = 0; chan < fChannels; chan++) {
            static_cast<REAL**>(buffers)[chan] = &(static_cast<REAL**>(fBuffers))[chan][offset];
        }
    }
    
    void emptyFile(int part, int& offset)
    {
        fLength[part] = BUFFER_SIZE;
        fSR[part] = SAMPLE_RATE;
        fOffset[part] = offset;
        // Update offset
        offset += fLength[part];
    }
 
    ~Soundfile()
    {
        // Free the real channels only
        if (fIsDouble) {
            for (int chan = 0; chan < fChannels; chan++) {
                delete[] static_cast<double**>(fBuffers)[chan];
            }
            delete[] static_cast<double**>(fBuffers);
        } else {
            for (int chan = 0; chan < fChannels; chan++) {
                delete[] static_cast<float**>(fBuffers)[chan];
            }
            delete[] static_cast<float**>(fBuffers);
        }
        delete[] fLength;
        delete[] fSR;
        delete[] fOffset;
    }

    typedef std::vector<std::string> Directories;
    
} POST_PACKED_STRUCTURE;

/*
 The generic soundfile reader.
 */

class SoundfileReader {
    
   protected:
    
    int fDriverSR;
   
    // Check if a soundfile exists and return its real path_name
    std::string checkFile(const Soundfile::Directories& sound_directories, const std::string& file_name)
    {
        if (checkFile(file_name)) {
            return file_name;
        } else {
            for (size_t i = 0; i < sound_directories.size(); i++) {
                std::string path_name = sound_directories[i] + "/" + file_name;
                if (checkFile(path_name)) { return path_name; }
            }
            return "";
        }
    }
    
    bool isResampling(int sample_rate) { return (fDriverSR > 0 && fDriverSR != sample_rate); }
 
    // To be implemented by subclasses

    /**
     * Check the availability of a sound resource.
     *
     * @param path_name - the name of the file, or sound resource identified this way
     *
     * @return true if the sound resource is available, false otherwise.
     */
    virtual bool checkFile(const std::string& path_name) = 0;
    
    /**
     * Check the availability of a sound resource.
     *
     * @param buffer - the sound buffer
     * @param size - the sound buffer length
     *
     * @return true if the sound resource is available, false otherwise.
     */

    virtual bool checkFile(unsigned char* buffer, size_t size) { return true; }

    /**
     * Get the channels and length values of the given sound resource.
     *
     * @param path_name - the name of the file, or sound resource identified this way
     * @param channels - the channels value to be filled with the sound resource number of channels
     * @param length - the length value to be filled with the sound resource length in frames
     *
     */
    virtual void getParamsFile(const std::string& path_name, int& channels, int& length) = 0;
    
    /**
     * Get the channels and length values of the given sound resource.
     *
     * @param buffer - the sound buffer
     * @param size - the sound buffer length
     * @param channels - the channels value to be filled with the sound resource number of channels
     * @param length - the length value to be filled with the sound resource length in frames
     *
     */
    virtual void getParamsFile(unsigned char* buffer, size_t size, int& channels, int& length) {}

    /**
     * Read one sound resource and fill the 'soundfile' structure accordingly
     *
     * @param soundfile - the soundfile to be filled
     * @param path_name - the name of the file, or sound resource identified this way
     * @param part - the part number to be filled in the soundfile
     * @param offset - the offset value to be incremented with the actual sound resource length in frames
     * @param max_chan - the maximum number of mono channels to fill
     *
     */
    virtual void readFile(Soundfile* soundfile, const std::string& path_name, int part, int& offset, int max_chan) = 0;
    
    /**
     * Read one sound resource and fill the 'soundfile' structure accordingly
     *
     * @param soundfile - the soundfile to be filled
     * @param buffer - the sound buffer
     * @param size - the sound buffer length
     * @param part - the part number to be filled in the soundfile
     * @param offset - the offset value to be incremented with the actual sound resource length in frames
     * @param max_chan - the maximum number of mono channels to fill
     *
     */
    virtual void readFile(Soundfile* soundfile, unsigned char* buffer, size_t size, int part, int& offset, int max_chan) {}

  public:
    
    SoundfileReader() {}
    virtual ~SoundfileReader() {}
    
    void setSampleRate(int sample_rate) { fDriverSR = sample_rate; }
   
    Soundfile* createSoundfile(const std::vector<std::string>& path_name_list, int max_chan, bool is_double)
    {
        try {
            int cur_chan = 1; // At least one channel
            int total_length = 0;
            
            // Compute total length and channels max of all files
            for (size_t i = 0; i < path_name_list.size(); i++) {
                int chan, length;
                if (path_name_list[i] == "__empty_sound__") {
                    length = BUFFER_SIZE;
                    chan = 1;
                } else {
                    getParamsFile(path_name_list[i], chan, length);
                }
                cur_chan = std::max<int>(cur_chan, chan);
                total_length += length;
            }
           
            // Complete with empty parts
            total_length += (MAX_SOUNDFILE_PARTS - path_name_list.size()) * BUFFER_SIZE;
            
            // Create the soundfile
            Soundfile* soundfile = new Soundfile(cur_chan, total_length, max_chan, path_name_list.size(), is_double);
            
            // Init offset
            int offset = 0;
            
            // Read all files
            for (size_t i = 0; i < path_name_list.size(); i++) {
                if (path_name_list[i] == "__empty_sound__") {
                    soundfile->emptyFile(i, offset);
                } else {
                    readFile(soundfile, path_name_list[i], i, offset, max_chan);
                }
            }
            
            // Complete with empty parts
            for (size_t i = path_name_list.size(); i < MAX_SOUNDFILE_PARTS; i++) {
                soundfile->emptyFile(i, offset);
            }
            
            // Share the same buffers for all other channels so that we have max_chan channels available
            soundfile->shareBuffers(cur_chan, max_chan);
            return soundfile;
            
        } catch (...) {
            return nullptr;
        }
    }

    // Check if all soundfiles exist and return their real path_name
    std::vector<std::string> checkFiles(const Soundfile::Directories& sound_directories,
                                        const std::vector<std::string>& file_name_list)
    {
        std::vector<std::string> path_name_list;
        for (size_t i = 0; i < file_name_list.size(); i++) {
            std::string path_name = checkFile(sound_directories, file_name_list[i]);
            // If 'path_name' is not found, it is replaced by an empty sound (= silence)
            path_name_list.push_back((path_name == "") ? "__empty_sound__" : path_name);
        }
        return path_name_list;
    }

};

#endif
/**************************  END  Soundfile.h **************************/

#if defined(JUCE_32BIT) || defined(JUCE_64BIT)
/************************** BEGIN JuceReader.h **************************
 FAUST Architecture File
 Copyright (C) 2003-2022 GRAME, Centre National de Creation Musicale
 ---------------------------------------------------------------------
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation; either version 2.1 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 
 EXCEPTION : As a special exception, you may create a larger work
 that contains this FAUST architecture section and distribute
 that work under terms of your choice, so long as this FAUST
 architecture section is not modified.
 ************************************************************************/

#ifndef __JuceReader__
#define __JuceReader__

#include <assert.h>

#include "../JuceLibraryCode/JuceHeader.h"


struct JuceReader : public SoundfileReader {
    
    juce::AudioFormatManager fFormatManager;
    
    JuceReader() { fFormatManager.registerBasicFormats(); }
    virtual ~JuceReader()
    {}
    
    bool checkFile(const std::string& path_name) override
    {
        juce::File file = juce::File::getCurrentWorkingDirectory().getChildFile(path_name);
        if (file.existsAsFile()) {
            return true;
        } else {
            //std::cerr << "ERROR : cannot open '" << path_name << "'" << std::endl;
            return false;
        }
    }
    
    void getParamsFile(const std::string& path_name, int& channels, int& length) override
    {
        std::unique_ptr<juce::AudioFormatReader> formatReader (fFormatManager.createReaderFor (juce::File::getCurrentWorkingDirectory().getChildFile(path_name)));
        channels = int(formatReader->numChannels);
        length = int(formatReader->lengthInSamples);
    }
    
    void readFile(Soundfile* soundfile, const std::string& path_name, int part, int& offset, int max_chan) override
    {
        std::unique_ptr<juce::AudioFormatReader> formatReader (fFormatManager.createReaderFor (juce::File::getCurrentWorkingDirectory().getChildFile(path_name)));
        
        soundfile->fLength[part] = int(formatReader->lengthInSamples);
        soundfile->fSR[part] = int(formatReader->sampleRate);
        soundfile->fOffset[part] = offset;
        
        void* buffers;
        if (soundfile->fIsDouble) {
            buffers = alloca(soundfile->fChannels * sizeof(double*));
            soundfile->getBuffersOffsetReal<double>(buffers, offset);
        } else {
            buffers = alloca(soundfile->fChannels * sizeof(float*));
            soundfile->getBuffersOffsetReal<float>(buffers, offset);
        }
        
        if (formatReader->read(reinterpret_cast<int *const *>(buffers), int(formatReader->numChannels), 0, int(formatReader->lengthInSamples), false)) {
            
            // Possibly convert samples
            if (!formatReader->usesFloatingPointData) {
                for (int chan = 0; chan < int(formatReader->numChannels); ++chan) {
                    if (soundfile->fIsDouble) {
                        // TODO
                    } else {
                        float* buffer = &(static_cast<float**>(soundfile->fBuffers))[chan][soundfile->fOffset[part]];
                        juce::FloatVectorOperations::convertFixedToFloat(buffer, reinterpret_cast<const int*>(buffer),
                                                                         1.0f/0x7fffffff, int(formatReader->lengthInSamples));
                    }
                }
            }
            
        } else {
            std::cerr << "Error reading the file : " << path_name << std::endl;
        }
            
        // Update offset
        offset += soundfile->fLength[part];
    }
    
};

#endif
/**************************  END  JuceReader.h **************************/
static JuceReader gReader;
#elif defined(DAISY) || defined(SUPERCOLLIDER)
/************************** BEGIN WaveReader.h **************************
 FAUST Architecture File
 Copyright (C) 2003-2022 GRAME, Centre National de Creation Musicale
 ---------------------------------------------------------------------
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation; either version 2.1 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 
 EXCEPTION : As a special exception, you may create a larger work
 that contains this FAUST architecture section and distribute
 that work under terms of your choice, so long as this FAUST
 architecture section is not modified.
 ********************************************************************/

#ifndef __WaveReader__
#define __WaveReader__

#include <string.h>
#include <assert.h>
#include <stdio.h>


// WAVE file description
typedef struct {
    
    // The canonical WAVE format starts with the RIFF header
    
    /**
     Variable: chunk_id
     Contains the letters "RIFF" in ASCII form (0x52494646 big-endian form).
     **/
    int chunk_id;
    
    /**
     Variable: chunk_size
     36 + SubChunk2Size, or more precisely: 4 + (8 + SubChunk1Size) + (8 + SubChunk2Size)
     This is the size of the rest of the chunk following this number.
     This is the size of the entire file in bytes minus 8 bytes for the
     two fields not included in this count: ChunkID and ChunkSize.
     **/
    int chunk_size;
    
    /**
     Variable: format
     Contains the letters "WAVE" (0x57415645 big-endian form).
     **/
    int format;
    
    // The "WAVE" format consists of two subchunks: "fmt " and "data":
    // The "fmt " subchunk describes the sound data's format:
    
    /**
     Variable: subchunk_1_id
     Contains the letters "fmt " (0x666d7420 big-endian form).
     **/
    int subchunk_1_id;
    
    /**
     Variable: subchunk_1_size
     16 for PCM. This is the size of the rest of the Subchunk which follows this number.
     **/
    int subchunk_1_size;
    
    /**
     Variable: audio_format
     PCM = 1 (i.e. Linear quantization) Values other than 1 indicate some form of compression.
     **/
    short audio_format;
    
    /**
     Variable: num_channels
     Mono = 1, Stereo = 2, etc.
     **/
    short num_channels;
    
    /**
     Variable: sample_rate
     8000, 44100, etc.
     **/
    int sample_rate;
    
    /**
     Variable: byte_rate
     == SampleRate * NumChannels * BitsPerSample/8
     **/
    int byte_rate;
    
    /**
     Variable: block_align
     == NumChannels * BitsPerSample/8
     The number of bytes for one sample including all channels. I wonder what happens
     when this number isn't an integer?
     **/
    short block_align;
    
    /**
     Variable: bits_per_sample
     8 bits = 8, 16 bits = 16, etc.
     **/
    short bits_per_sample;
    
    /**
     Here should come some extra parameters which i will avoid.
     **/
    
    // The "data" subchunk contains the size of the data and the actual sound:
    
    /**
     Variable: subchunk_2_id
     Contains the letters "data" (0x64617461 big-endian form).
     **/
    int subchunk_2_id;
    
    /**
     Variable: subchunk_2_size
     == NumSamples * NumChannels * BitsPerSample/8
     This is the number of bytes in the data. You can also think of this as the size
     of the read of the subchunk following this number.
     **/
    int subchunk_2_size;
    
    /**
     Variable: data
     The actual sound data.
     **/
    char* data;
    
} wave_t;

// Base reader
struct Reader {
    
    wave_t* fWave;

    inline int is_big_endian()
    {
        int a = 1;
        return !((char*)&a)[0];
    }
    
    inline int convert_to_int(char* buffer, int len)
    {
        int a = 0;
        if (!is_big_endian()) {
            for(int i = 0; i < len; i++) {
                ((char*)&a)[i] = buffer[i];
            }
        } else {
            for(int i = 0; i < len; i++) {
                ((char*)&a)[3-i] = buffer[i];
            }
        }
        return a;
    }
    
    Reader()
    {
        fWave = (wave_t*)calloc(1, sizeof(wave_t));
    }

    virtual ~Reader()
    {
        free(fWave->data);
        free(fWave);
    }

    bool load_wave_header()
    {
        char buffer[4];
        
        read(buffer, 4);
        if (strncmp(buffer, "RIFF", 4) != 0) {
            fprintf(stderr, "This is not valid WAV file!\n");
            return false;
        }
        fWave->chunk_id = convert_to_int(buffer, 4);
        
        read(buffer, 4);
        fWave->chunk_size = convert_to_int(buffer, 4);
        
        read(buffer, 4);
        fWave->format = convert_to_int(buffer, 4);
        
        read(buffer, 4);
        fWave->subchunk_1_id = convert_to_int(buffer, 4);
        
        read(buffer, 4);
        fWave->subchunk_1_size = convert_to_int(buffer, 4);
        
        read(buffer, 2);
        fWave->audio_format = convert_to_int(buffer, 2);
        
        read(buffer, 2);
        fWave->num_channels = convert_to_int(buffer, 2);
        
        read(buffer, 4);
        fWave->sample_rate = convert_to_int(buffer, 4);
        
        read(buffer, 4);
        fWave->byte_rate = convert_to_int(buffer, 4);
        
        read(buffer, 2);
        fWave->block_align = convert_to_int(buffer, 2);
        
        read(buffer, 2);
        fWave->bits_per_sample = convert_to_int(buffer, 2);
        
        read(buffer, 4);
        if (strncmp(buffer, "data", 4) != 0) {
            read(buffer, 4);
            int _extra_size = convert_to_int(buffer, 4);
            char _extra_data[_extra_size];
            read(_extra_data, _extra_size);
            read(buffer, 4);
            fWave->subchunk_2_id = convert_to_int(buffer, 4);
        } else {
            fWave->subchunk_2_id = convert_to_int(buffer, 4);
        }
        
        read(buffer, 4);
        fWave->subchunk_2_size = convert_to_int(buffer, 4);
        return true;
    }
    
    void load_wave()
    {
        // Read sound data
        fWave->data = (char*)malloc(fWave->subchunk_2_size);
        read(fWave->data, fWave->subchunk_2_size);
    }

    virtual void read(char* buffer, unsigned int size) = 0;
   
};

struct FileReader : public Reader {
    
    FILE* fFile;
    
    FileReader(const std::string& file_path)
    {
        fFile = fopen(file_path.c_str(), "rb");
        if (!fFile) {
            fprintf(stderr, "FileReader : cannot open file!\n");
            throw -1;
        }
        if (!load_wave_header()) {
            fprintf(stderr, "FileReader : not a WAV file!\n");
            throw -1;
        }
    }
    
    virtual ~FileReader()
    {
        fclose(fFile);
    }
    
    void read(char* buffer, unsigned int size)
    {
        fread(buffer, 1, size, fFile);
    }
    
};

extern const uint8_t file_start[] asm("_binary_FILE_start");
extern const uint8_t file_end[]   asm("_binary_FILE_end");

struct MemoryReader : public Reader {
    
    int fPos;
    const uint8_t* fStart;
    const uint8_t* fEnd;
    
    MemoryReader(const uint8_t* start, const uint8_t* end):fPos(0)
    {
        fStart = start;
        fEnd = end;
        if (!load_wave_header()) {
            fprintf(stderr, "MemoryReader : not a WAV file!\n");
            throw -1;
        }
    }
    
    virtual ~MemoryReader()
    {}
    
    void read(char* buffer, unsigned int size)
    {
        memcpy(buffer, fStart + fPos, size);
        fPos += size;
    }
    
};

// Using a FileReader to implement SoundfileReader

struct WaveReader : public SoundfileReader {
    
    WaveReader() {}
    virtual ~WaveReader() {}
    
    bool checkFile(const std::string& path_name) override
    {
        try {
            FileReader reader(path_name);
            return true;
        } catch (...)  {
            return false;
        }
    }
    
    void getParamsFile(const std::string& path_name, int& channels, int& length) override
    {
        FileReader reader(path_name);
        channels = reader.fWave->num_channels;
        length = (reader.fWave->subchunk_2_size * 8) / (reader.fWave->num_channels * reader.fWave->bits_per_sample);
    }
    
    void readFile(Soundfile* soundfile, const std::string& path_name, int part, int& offset, int max_chan) override
    {
        FileReader reader(path_name);
        reader.load_wave();
        
        soundfile->fLength[part] = (reader.fWave->subchunk_2_size * 8) / (reader.fWave->num_channels * reader.fWave->bits_per_sample);
        soundfile->fSR[part] = reader.fWave->sample_rate;
        soundfile->fOffset[part] = offset;
        
        // Audio frames have to be written for each chan
        if (reader.fWave->bits_per_sample == 16) {
            float factor = 1.f/32767.f;
            for (int sample = 0; sample < soundfile->fLength[part]; sample++) {
                short* frame = (short*)&reader.fWave->data[reader.fWave->block_align * sample];
                if (soundfile->fIsDouble) {
                    for (int chan = 0; chan < reader.fWave->num_channels; chan++) {
                        static_cast<double**>(soundfile->fBuffers)[chan][offset + sample] = frame[chan] * factor;
                    }
                } else {
                    for (int chan = 0; chan < reader.fWave->num_channels; chan++) {
                        static_cast<float**>(soundfile->fBuffers)[chan][offset + sample] = frame[chan] * factor;
                    }
                }
            }
        } else if (reader.fWave->bits_per_sample == 32) {
            fprintf(stderr, "readFile : not implemented\n");
        }
        
        // Update offset
        offset += soundfile->fLength[part];
    }
};

#endif
/**************************  END  WaveReader.h **************************/
static WaveReader gReader;
#elif defined(ESP32)
/************************** BEGIN Esp32Reader.h **************************
 FAUST Architecture File
 Copyright (C) 2003-2022 GRAME, Centre National de Creation Musicale
 ---------------------------------------------------------------------
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation; either version 2.1 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 
 EXCEPTION : As a special exception, you may create a larger work
 that contains this FAUST architecture section and distribute
 that work under terms of your choice, so long as this FAUST
 architecture section is not modified.
 *************************************************************************/

#ifndef FAUST_ESP32READER_H
#define FAUST_ESP32READER_H

#include <stdio.h>
#include "esp_err.h"
#include "esp_log.h"
#include "esp_spi_flash.h"
#include "esp_vfs_fat.h"
#include "driver/sdspi_host.h"
#include "sdmmc_cmd.h"


#define TAG "Esp32Reader"

#define SD_PIN_NUM_MISO GPIO_NUM_2
#define SD_PIN_NUM_MOSI GPIO_NUM_15
#define SD_PIN_NUM_CLK  GPIO_NUM_14
#define SD_PIN_NUM_CS   GPIO_NUM_13

struct Esp32Reader : public WaveReader {
    
    void sdcard_init()
    {
        ESP_LOGI(TAG, "Initializing SD card");
        ESP_LOGI(TAG, "Using SPI peripheral");
        
        sdmmc_host_t host = SDSPI_HOST_DEFAULT();
        sdspi_slot_config_t slot_config = SDSPI_SLOT_CONFIG_DEFAULT();
        slot_config.gpio_miso = SD_PIN_NUM_MISO;
        slot_config.gpio_mosi = SD_PIN_NUM_MOSI;
        slot_config.gpio_sck  = SD_PIN_NUM_CLK;
        slot_config.gpio_cs   = SD_PIN_NUM_CS;
        // This initializes the slot without card detect (CD) and write protect (WP) signals.
        // Modify slot_config.gpio_cd and slot_config.gpio_wp if your board has these signals.
        
        // Options for mounting the filesystem.
        // If format_if_mount_failed is set to true, SD card will be partitioned and
        // formatted in case when mounting fails.
        esp_vfs_fat_sdmmc_mount_config_t mount_config = {
            .format_if_mount_failed = false,
            .max_files = 5,
            .allocation_unit_size = 16 * 1024
        };
        
        // Use settings defined above to initialize SD card and mount FAT filesystem.
        // Note: esp_vfs_fat_sdmmc_mount is an all-in-one convenience function.
        // Please check its source code and implement error recovery when developing
        // production applications.
        sdmmc_card_t* card;
        esp_err_t ret = esp_vfs_fat_sdmmc_mount("/sdcard", &host, &slot_config, &mount_config, &card);
        
        if (ret != ESP_OK) {
            if (ret == ESP_FAIL) {
                ESP_LOGE(TAG, "Failed to mount filesystem. "
                         "If you want the card to be formatted, set format_if_mount_failed = true.");
            } else {
                ESP_LOGE(TAG, "Failed to initialize the card (%s). "
                         "Make sure SD card lines have pull-up resistors in place.", esp_err_to_name(ret));
            }
            return;
        }
        
        // Card has been initialized, print its properties
        sdmmc_card_print_info(stdout, card);
        ESP_LOGI(TAG, "SD card initialized");
    }
    
    Esp32Reader()
    {
        sdcard_init();
    }
   
    // Access methods inherited from WaveReader
};

#endif // FAUST_ESP32READER_H
/**************************  END  Esp32Reader.h **************************/
static Esp32Reader gReader;
#elif defined(MEMORY_READER)
/************************** BEGIN MemoryReader.h ************************
 FAUST Architecture File
 Copyright (C) 2003-2022 GRAME, Centre National de Creation Musicale
 ---------------------------------------------------------------------
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation; either version 2.1 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 
 EXCEPTION : As a special exception, you may create a larger work
 that contains this FAUST architecture section and distribute
 that work under terms of your choice, so long as this FAUST
 architecture section is not modified.
 ************************************************************************/

#ifndef __MemoryReader__
#define __MemoryReader__


/*
 A 'MemoryReader' object can be used to prepare a set of sound resources in memory, to be used by SoundUI::addSoundfile.
 
 A Soundfile* object will have to be filled with a list of sound resources: the fLength, fOffset, fSampleRate and fBuffers fields 
 have to be completed with the appropriate values, and will be accessed in the DSP object while running.
 *
 */

// To adapt for a real case use

#define SOUND_CHAN      2
#define SOUND_LENGTH    4096
#define SOUND_SR        44100

struct MemoryReader : public SoundfileReader {
    
    MemoryReader()
    {}
    virtual ~MemoryReader()
    {}
    
    /**
     * Check the availability of a sound resource.
     *
     * @param path_name - the name of the file, or sound resource identified this way
     *
     * @return true if the sound resource is available, false otherwise.
     */
    virtual bool checkFile(const std::string& path_name) override { return true; }
    
    /**
     * Get the channels and length values of the given sound resource.
     *
     * @param path_name - the name of the file, or sound resource identified this way
     * @param channels - the channels value to be filled with the sound resource number of channels
     * @param length - the length value to be filled with the sound resource length in frames
     *
     */
    virtual void getParamsFile(const std::string& path_name, int& channels, int& length) override
    {
        channels = SOUND_CHAN;
        length = SOUND_LENGTH;
    }
    
    /**
     * Read one sound resource and fill the 'soundfile' structure accordingly
     *
     * @param path_name - the name of the file, or sound resource identified this way
     * @param part - the part number to be filled in the soundfile
     * @param offset - the offset value to be incremented with the actual sound resource length in frames
     * @param max_chan - the maximum number of mono channels to fill
     *
     */
    virtual void readFile(Soundfile* soundfile, const std::string& path_name, int part, int& offset, int max_chan) override
    {
        soundfile->fLength[part] = SOUND_LENGTH;
        soundfile->fSR[part] = SOUND_SR;
        soundfile->fOffset[part] = offset;
        
        // Audio frames have to be written for each chan
        if (soundfile->fIsDouble) {
            for (int sample = 0; sample < SOUND_LENGTH; sample++) {
                for (int chan = 0; chan < SOUND_CHAN; chan++) {
                    static_cast<double**>(soundfile->fBuffers)[chan][offset + sample] = 0.f;
                }
            }
        } else {
            for (int sample = 0; sample < SOUND_LENGTH; sample++) {
                for (int chan = 0; chan < SOUND_CHAN; chan++) {
                    static_cast<float**>(soundfile->fBuffers)[chan][offset + sample] = 0.f;
                }
            }
        }
        
        // Update offset
        offset += SOUND_LENGTH;
    }
    
};

#endif
/**************************  END  MemoryReader.h **************************/
static MemoryReader gReader;
#else
/************************** BEGIN LibsndfileReader.h *********************
 FAUST Architecture File
 Copyright (C) 2003-2022 GRAME, Centre National de Creation Musicale
 ---------------------------------------------------------------------
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation; either version 2.1 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 
 EXCEPTION : As a special exception, you may create a larger work
 that contains this FAUST architecture section and distribute
 that work under terms of your choice, so long as this FAUST
 architecture section is not modified.
 ************************************************************************/

#ifndef __LibsndfileReader__
#define __LibsndfileReader__

#ifdef _SAMPLERATE
#include <samplerate.h>
#endif
#include <sndfile.h>
#include <string.h>
#include <assert.h>
#include <iostream>
#include <fstream>


/*
// Deactivated for now, since the macOS remote cross-compiler fails with this code.
#if __has_include(<filesystem>) && __cplusplus >= 201703L
    #define HAS_FILESYSTEM
    #include <filesystem>
    namespace fs = std::filesystem;
#elif __has_include(<experimental/filesystem>) && __cplusplus >= 201103L
    #define HAS_FILESYSTEM
    #include <experimental/filesystem>
    namespace fs = std::experimental::filesystem;
#endif
*/

struct VFLibsndfile {
    
    #define SIGNED_SIZEOF(x) ((int)sizeof(x))
    
    unsigned char* fBuffer;
    size_t fLength;
    size_t fOffset;
    SF_VIRTUAL_IO fVIO;
    
    VFLibsndfile(unsigned char* buffer, size_t length):fBuffer(buffer), fLength(length), fOffset(0)
    {
        fVIO.get_filelen = vfget_filelen;
        fVIO.seek = vfseek;
        fVIO.read = vfread;
        fVIO.write = vfwrite;
        fVIO.tell = vftell;
    }
    
    static sf_count_t vfget_filelen(void* user_data)
    {
        VFLibsndfile* vf = static_cast<VFLibsndfile*>(user_data);
        return vf->fLength;
    }
  
    static sf_count_t vfseek(sf_count_t offset, int whence, void* user_data)
    {
        VFLibsndfile* vf = static_cast<VFLibsndfile*>(user_data);
        switch (whence) {
            case SEEK_SET:
                vf->fOffset = offset;
                break;
                
            case SEEK_CUR:
                vf->fOffset = vf->fOffset + offset;
                break;
                
            case SEEK_END:
                vf->fOffset = vf->fLength + offset;
                break;
                
            default:
                break;
        };
        
        return vf->fOffset;
    }
    
    static sf_count_t vfread(void* ptr, sf_count_t count, void* user_data)
    {
        VFLibsndfile* vf = static_cast<VFLibsndfile*>(user_data);
        
        /*
         **	This will break badly for files over 2Gig in length, but
         **	is sufficient for testing.
         */
        if (vf->fOffset + count > vf->fLength) {
            count = vf->fLength - vf->fOffset;
        }
        
        memcpy(ptr, vf->fBuffer + vf->fOffset, count);
        vf->fOffset += count;
        
        return count;
    }
    
    static sf_count_t vfwrite(const void* ptr, sf_count_t count, void* user_data)
    {
        VFLibsndfile* vf = static_cast<VFLibsndfile*>(user_data);
        
        /*
         **	This will break badly for files over 2Gig in length, but
         **	is sufficient for testing.
         */
        if (vf->fOffset >= SIGNED_SIZEOF(vf->fBuffer)) {
            return 0;
        }
        
        if (vf->fOffset + count > SIGNED_SIZEOF(vf->fBuffer)) {
            count = sizeof (vf->fBuffer) - vf->fOffset;
        }
        
        memcpy(vf->fBuffer + vf->fOffset, ptr, (size_t)count);
        vf->fOffset += count;
        
        if (vf->fOffset > vf->fLength) {
            vf->fLength = vf->fOffset;
        }
        
        return count;
    }
    
    static sf_count_t vftell(void* user_data)
    {
        VFLibsndfile* vf = static_cast<VFLibsndfile*>(user_data);
        return vf->fOffset;
    }
 
};

struct LibsndfileReader : public SoundfileReader {
	
    LibsndfileReader() {}
	
    typedef sf_count_t (* sample_read)(SNDFILE* sndfile, void* buffer, sf_count_t frames);
	
    // Check file
    bool checkFile(const std::string& path_name) override
    {
        /*
         // Better since it supports Unicode characters.
         #ifdef HAS_FILESYSTEM
         if (!fs::exists(path_name)) {
            std::cerr << "FILE NOT FOUND\n";
            return false;
         }
         #endif
        */
    
        std::ifstream ifs(path_name);
        if (!ifs.is_open()) {
            return false;
        } else {
            SF_INFO snd_info;
            snd_info.format = 0;
            SNDFILE* snd_file = sf_open(path_name.c_str(), SFM_READ, &snd_info);
            return checkFileAux(snd_file, path_name);
        }
    }
    
    bool checkFile(unsigned char* buffer, size_t length) override
    {
        SF_INFO snd_info;
        snd_info.format = 0;
        VFLibsndfile vio(buffer, length);
        SNDFILE* snd_file = sf_open_virtual(&vio.fVIO, SFM_READ, &snd_info, &vio);
        return checkFileAux(snd_file, "virtual file");
    }
    
    bool checkFileAux(SNDFILE* snd_file, const std::string& path_name)
    {
        if (snd_file) {
            sf_close(snd_file);
            return true;
        } else {
            std::cerr << "ERROR : cannot open '" << path_name << "' (" << sf_strerror(NULL) << ")" << std::endl;
            return false;
        }
    }

    // Open the file and returns its length and channels
    void getParamsFile(const std::string& path_name, int& channels, int& length) override
    {
        SF_INFO	snd_info;
        snd_info.format = 0;
        SNDFILE* snd_file = sf_open(path_name.c_str(), SFM_READ, &snd_info);
        getParamsFileAux(snd_file, snd_info, channels, length);
    }
    
    void getParamsFile(unsigned char* buffer, size_t size, int& channels, int& length) override
    {
        SF_INFO	snd_info;
        snd_info.format = 0;
        VFLibsndfile vio(buffer, size);
        SNDFILE* snd_file = sf_open_virtual(&vio.fVIO, SFM_READ, &snd_info, &vio);
        getParamsFileAux(snd_file, snd_info, channels, length);
    }
    
    void getParamsFileAux(SNDFILE* snd_file, const SF_INFO& snd_info, int& channels, int& length)
    {
        assert(snd_file);
        channels = int(snd_info.channels);
    #ifdef _SAMPLERATE
        length = (isResampling(snd_info.samplerate)) ? ((double(snd_info.frames) * double(fDriverSR) / double(snd_info.samplerate)) + BUFFER_SIZE) : int(snd_info.frames);
    #else
        length = int(snd_info.frames);
    #endif
        sf_close(snd_file);
    }
    
    // Read the file
    void readFile(Soundfile* soundfile, const std::string& path_name, int part, int& offset, int max_chan) override
    {
        SF_INFO	snd_info;
        snd_info.format = 0;
        SNDFILE* snd_file = sf_open(path_name.c_str(), SFM_READ, &snd_info);
        readFileAux(soundfile, snd_file, snd_info, part, offset, max_chan);
    }
    
    void readFile(Soundfile* soundfile, unsigned char* buffer, size_t length, int part, int& offset, int max_chan) override
    {
        SF_INFO	snd_info;
        snd_info.format = 0;
        VFLibsndfile vio(buffer, length);
        SNDFILE* snd_file = sf_open_virtual(&vio.fVIO, SFM_READ, &snd_info, &vio);
        readFileAux(soundfile, snd_file, snd_info, part, offset, max_chan);
    }
	
    // Will be called to fill all parts from 0 to MAX_SOUNDFILE_PARTS-1
    void readFileAux(Soundfile* soundfile, SNDFILE* snd_file, const SF_INFO& snd_info, int part, int& offset, int max_chan)
    {
        assert(snd_file);
        int channels = std::min<int>(max_chan, snd_info.channels);
    #ifdef _SAMPLERATE
        if (isResampling(snd_info.samplerate)) {
            soundfile->fLength[part] = int(double(snd_info.frames) * double(fDriverSR) / double(snd_info.samplerate));
            soundfile->fSR[part] = fDriverSR;
        } else {
            soundfile->fLength[part] = int(snd_info.frames);
            soundfile->fSR[part] = snd_info.samplerate;
        }
    #else
        soundfile->fLength[part] = int(snd_info.frames);
        soundfile->fSR[part] = snd_info.samplerate;
    #endif
        soundfile->fOffset[part] = offset;
		
        // Read and fill snd_info.channels number of channels
        sf_count_t nbf;
        
        sample_read reader;
        void* buffer_in = nullptr;
        if (soundfile->fIsDouble) {
            buffer_in = static_cast<double*>(alloca(BUFFER_SIZE * sizeof(double) * snd_info.channels));
            reader = reinterpret_cast<sample_read>(sf_readf_double);
        } else {
            buffer_in = static_cast<float*>(alloca(BUFFER_SIZE * sizeof(float) * snd_info.channels));
            reader = reinterpret_cast<sample_read>(sf_readf_float);
        }
        
    #ifdef _SAMPLERATE
        // Resampling
        SRC_STATE* resampler = nullptr;
        float* src_buffer_out = nullptr;
        float* src_buffer_in = nullptr;
        void* buffer_out = nullptr;
        if  (isResampling(snd_info.samplerate)) {
            int error;
            resampler = src_new(SRC_SINC_FASTEST, channels, &error);
            if (error != 0) {
                std::cerr << "ERROR : src_new " << src_strerror(error) << std::endl;
                throw -1;
            }
            if (soundfile->fIsDouble) {
                // Additional buffers for SRC resampling
                src_buffer_in = static_cast<float*>(alloca(BUFFER_SIZE * sizeof(float) * snd_info.channels));
                src_buffer_out = static_cast<float*>(alloca(BUFFER_SIZE * sizeof(float) * snd_info.channels));
                buffer_out = static_cast<double*>(alloca(BUFFER_SIZE * sizeof(double) * snd_info.channels));
            } else {
                buffer_out = static_cast<float*>(alloca(BUFFER_SIZE * sizeof(float) * snd_info.channels));
            }
        }
    #endif
        
        do {
            nbf = reader(snd_file, buffer_in, BUFFER_SIZE);
        #ifdef _SAMPLERATE
            // Resampling
            if  (isResampling(snd_info.samplerate)) {
                int in_offset = 0;
                SRC_DATA src_data;
                src_data.src_ratio = double(fDriverSR)/double(snd_info.samplerate);
                if (soundfile->fIsDouble) {
                    for (int frame = 0; frame < (BUFFER_SIZE * snd_info.channels); frame++) {
                        src_buffer_in[frame] = float(static_cast<float*>(buffer_in)[frame]);
                    }
                }
                do {
                    if (soundfile->fIsDouble) {
                        src_data.data_in = src_buffer_in;
                        src_data.data_out = src_buffer_out;
                    } else {
                        src_data.data_in = static_cast<const float*>(buffer_in);
                        src_data.data_out = static_cast<float*>(buffer_out);
                    }
                    src_data.input_frames = nbf - in_offset;
                    src_data.output_frames = BUFFER_SIZE;
                    src_data.end_of_input = (nbf < BUFFER_SIZE);
                    int res = src_process(resampler, &src_data);
                    if (res != 0) {
                        std::cerr << "ERROR : src_process " << src_strerror(res) << std::endl;
                        throw -1;
                    }
                    if (soundfile->fIsDouble) {
                        for (int frame = 0; frame < (BUFFER_SIZE * snd_info.channels); frame++) {
                            static_cast<double*>(buffer_out)[frame] = double(src_buffer_out[frame]);
                        }
                    }
                    soundfile->copyToOut(src_data.output_frames_gen, channels, snd_info.channels, offset, buffer_out);
                    in_offset += src_data.input_frames_used;
                    // Update offset
                    offset += src_data.output_frames_gen;
                } while (in_offset < nbf);
            } else {
                soundfile->copyToOut(nbf, channels, snd_info.channels, offset, buffer_in);
                // Update offset
                offset += nbf;
            }
        #else
            soundfile->copyToOut(nbf, channels, snd_info.channels, offset, buffer_in);
            // Update offset
            offset += nbf;
        #endif
        } while (nbf == BUFFER_SIZE);
		
        sf_close(snd_file);
    #ifdef _SAMPLERATE
        if (resampler) src_delete(resampler);
    #endif
    }

};

#endif
/**************************  END  LibsndfileReader.h **************************/
static LibsndfileReader gReader;
#endif

// To be used by DSP code if no SoundUI is used
static std::vector<std::string> gPathNameList;
static Soundfile* defaultsound = nullptr;

class SoundUI : public SoundUIInterface
{
		
    protected:
    
        // The soundfile directories
        Soundfile::Directories fSoundfileDir;
        // Map to share loaded soundfiles
        std::map<std::string, std::shared_ptr<Soundfile>> fSoundfileMap;
        // The soundfile reader
        std::shared_ptr<SoundfileReader> fSoundReader;
        bool fIsDouble;

     public:
    
        /**
         * Create a soundfile loader which will typically use a concrete SoundfileReader like LibsndfileReader or JuceReader to load soundfiles.
         *
         * @param sound_directory - the base directory to look for files, which paths will be relative to this one
         * @param sample_rate - the audio driver SR which may be different from the file SR, to possibly resample files
         * @param reader - an alternative soundfile reader
         * @param is_double - whether Faust code has been compiled in -double mode and soundfile buffers have to be in double
         *
         * @return the soundfile loader.
         */
        SoundUI(const std::string& sound_directory = "", int sample_rate = -1, SoundfileReader* reader = nullptr, bool is_double = false)
        {
            fSoundfileDir.push_back(sound_directory);
            fSoundReader = (reader)
                ? std::shared_ptr<SoundfileReader>(reader)
                // the static gReader should not be deleted, so use an empty destructor
                : std::shared_ptr<SoundfileReader>(std::shared_ptr<SoundfileReader>{}, &gReader);
            fSoundReader->setSampleRate(sample_rate);
            fIsDouble = is_double;
            if (!defaultsound) defaultsound = gReader.createSoundfile(gPathNameList, MAX_CHAN, is_double);
        }
    
        /**
         * Create a soundfile loader which will typically use a concrete SoundfileReader like LibsndfileReader or JuceReader to load soundfiles.
         *
         * @param sound_directories - a vector of base directories to look for files, which paths will be relative to these ones
         * @param sample_rate - the audio driver SR which may be different from the file SR, to possibly resample files
         * @param reader - an alternative soundfile reader
         * @param is_double - whether Faust code has been compiled in -double mode and soundfile buffers have to be in double
         *
         * @return the soundfile loader.
         */
        SoundUI(const Soundfile::Directories& sound_directories, int sample_rate = -1, SoundfileReader* reader = nullptr, bool is_double = false)
        :fSoundfileDir(sound_directories)
        {
            fSoundReader = (reader)
                ? std::shared_ptr<SoundfileReader>(reader)
                // the static gReader should not be deleted, so use an empty destructor
                : std::shared_ptr<SoundfileReader>(std::shared_ptr<SoundfileReader>{}, &gReader);
            fSoundReader->setSampleRate(sample_rate);
            fIsDouble = is_double;
            if (!defaultsound) defaultsound = gReader.createSoundfile(gPathNameList, MAX_CHAN, is_double);
        }
    
        virtual ~SoundUI()
        {}

        // -- soundfiles
        virtual void addSoundfile(const char* label, const char* url, Soundfile** sf_zone)
        {
            const char* saved_url = url; // 'url' is consumed by parseMenuList2
            std::vector<std::string> file_name_list;
            
            bool menu = parseMenuList2(url, file_name_list, true);
            // If not a list, we have as single file
            if (!menu) { file_name_list.push_back(saved_url); }
            
            // Parse the possible list
            std::string saved_url_real = std::string(saved_url) + "_" + std::to_string(fIsDouble); // fIsDouble is used in the key
            if (fSoundfileMap.find(saved_url_real) == fSoundfileMap.end()) {
                // Check all files and get their complete path
                std::vector<std::string> path_name_list = fSoundReader->checkFiles(fSoundfileDir, file_name_list);
                // Read them and create the Soundfile
                Soundfile* sound_file = fSoundReader->createSoundfile(path_name_list, MAX_CHAN, fIsDouble);
                if (sound_file) {
                    fSoundfileMap[saved_url_real] = std::shared_ptr<Soundfile>(sound_file);
                } else {
                    // If failure, use 'defaultsound'
                    std::cerr << "addSoundfile : soundfile for " << saved_url << " cannot be created !" << std::endl;
                    *sf_zone = defaultsound;
                    return;
                }
            }
            
            // Get the soundfile pointer
            *sf_zone = fSoundfileMap[saved_url_real].get();
        }
    
        /**
         * An OS dependant function to get the path of the running executable or plugin.
         * This will typically be used when creating a SoundUI soundfile loader, like new SoundUI(SoundUI::getBinaryPath());
         *
         * @return the running executable or plugin path.
         */
        static std::string getBinaryPath()
        {
            std::string bundle_path_str;
        #if defined(__APPLE__) && !defined(__VCVRACK__) && !defined(JUCE_32BIT) && !defined(JUCE_64BIT)
            CFURLRef bundle_ref = CFBundleCopyBundleURL(CFBundleGetMainBundle());
            if (!bundle_ref) { std::cerr << "getBinaryPath CFBundleCopyBundleURL error\n"; return ""; }
      
            UInt8 bundle_path[1024];
            if (CFURLGetFileSystemRepresentation(bundle_ref, true, bundle_path, 1024)) {
                bundle_path_str = std::string((char*)bundle_path);
            } else {
                std::cerr << "getBinaryPath CFURLGetFileSystemRepresentation error\n";
            }
        #endif
        #ifdef ANDROID_DRIVER
            bundle_path_str = "/data/data/__CURRENT_ANDROID_PACKAGE__/files";
        #endif
            return bundle_path_str;
        }
    
        /**
         * An OS dependant function to get the path of the running executable or plugin.
         * This will typically be used when creating a SoundUI soundfile loader, like new SoundUI(SoundUI::getBinaryPathFrom());
         *
         * @param path - entry point to start getting the path of the running executable or plugin.
         *
         * @return the running executable or plugin path.
         */
        static std::string getBinaryPathFrom(const std::string& path)
        {
            std::string bundle_path_str;
        #if defined(__APPLE__) && !defined(__VCVRACK__) && !defined(JUCE_32BIT) && !defined(JUCE_64BIT)
            CFBundleRef bundle = CFBundleGetBundleWithIdentifier(CFStringCreateWithCString(kCFAllocatorDefault, path.c_str(), CFStringGetSystemEncoding()));
            if (!bundle) { std::cerr << "getBinaryPathFrom CFBundleGetBundleWithIdentifier error '" << path << "'" << std::endl; return ""; }
         
            CFURLRef bundle_ref = CFBundleCopyBundleURL(bundle);
            if (!bundle_ref) { std::cerr << "getBinaryPathFrom CFBundleCopyBundleURL error\n"; return ""; }
            
            UInt8 bundle_path[1024];
            if (CFURLGetFileSystemRepresentation(bundle_ref, true, bundle_path, 1024)) {
                bundle_path_str = std::string((char*)bundle_path);
            } else {
                std::cerr << "getBinaryPathFrom CFURLGetFileSystemRepresentation error\n";
            }
        #endif
        #ifdef ANDROID_DRIVER
            bundle_path_str = "/data/data/__CURRENT_ANDROID_PACKAGE__/files";
        #endif
            return bundle_path_str;
        }
};

#endif
/**************************  END  SoundUI.h **************************/
#endif

// For FAUST_CLASS_NAME to be defined
#define FAUST_UIMACROS

// but we will ignore most of them
#define FAUST_ADDBUTTON(l,f)
#define FAUST_ADDCHECKBOX(l,f)
#define FAUST_ADDVERTICALSLIDER(l,f,i,a,b,s)
#define FAUST_ADDHORIZONTALSLIDER(l,f,i,a,b,s)
#define FAUST_ADDNUMENTRY(l,f,i,a,b,s)
#define FAUST_ADDVERTICALBARGRAPH(l,f,a,b)
#define FAUST_ADDHORIZONTALBARGRAPH(l,f,a,b)
#define FAUST_ADDSOUNDFILE(s,f)

using namespace std;

/******************************************************************************
*******************************************************************************

							       VECTOR INTRINSICS

*******************************************************************************
*******************************************************************************/


/********************END ARCHITECTURE SECTION (part 1/2)****************/

/**************************BEGIN USER SECTION **************************/

#ifndef FAUSTFLOAT
#define FAUSTFLOAT float
#endif 

#include <algorithm>
#include <cmath>
#include <cstdint>
#include <math.h>

#ifndef FAUSTCLASS 
#define FAUSTCLASS bbdmi_multi_granulator14
#endif

#ifdef __APPLE__ 
#define exp10f __exp10f
#define exp10 __exp10
#endif

#if defined(_WIN32)
#define RESTRICT __restrict
#else
#define RESTRICT __restrict__
#endif

class bbdmi_multi_granulator14SIG0 {
	
  public:
	
	int iVec7[2];
	int iRec35[2];
	
  public:
	
	int getNumInputsbbdmi_multi_granulator14SIG0() {
		return 0;
	}
	int getNumOutputsbbdmi_multi_granulator14SIG0() {
		return 1;
	}
	
	void instanceInitbbdmi_multi_granulator14SIG0(int sample_rate) {
		for (int l40 = 0; l40 < 2; l40 = l40 + 1) {
			iVec7[l40] = 0;
		}
		for (int l41 = 0; l41 < 2; l41 = l41 + 1) {
			iRec35[l41] = 0;
		}
	}
	
	void fillbbdmi_multi_granulator14SIG0(int count, double* table) {
		for (int i1 = 0; i1 < count; i1 = i1 + 1) {
			iVec7[0] = 1;
			iRec35[0] = (iVec7[1] + iRec35[1]) % 65536;
			table[i1] = std::sin(9.587379924285257e-05 * double(iRec35[0]));
			iVec7[1] = iVec7[0];
			iRec35[1] = iRec35[0];
		}
	}

};

static bbdmi_multi_granulator14SIG0* newbbdmi_multi_granulator14SIG0() { return (bbdmi_multi_granulator14SIG0*)new bbdmi_multi_granulator14SIG0(); }
static void deletebbdmi_multi_granulator14SIG0(bbdmi_multi_granulator14SIG0* dsp) { delete dsp; }

class bbdmi_multi_granulator14SIG1 {
	
  public:
	
	int iVec8[2];
	int iRec39[2];
	
  public:
	
	int getNumInputsbbdmi_multi_granulator14SIG1() {
		return 0;
	}
	int getNumOutputsbbdmi_multi_granulator14SIG1() {
		return 1;
	}
	
	void instanceInitbbdmi_multi_granulator14SIG1(int sample_rate) {
		for (int l45 = 0; l45 < 2; l45 = l45 + 1) {
			iVec8[l45] = 0;
		}
		for (int l46 = 0; l46 < 2; l46 = l46 + 1) {
			iRec39[l46] = 0;
		}
	}
	
	void fillbbdmi_multi_granulator14SIG1(int count, double* table) {
		for (int i2 = 0; i2 < count; i2 = i2 + 1) {
			iVec8[0] = 1;
			iRec39[0] = (iVec8[1] + iRec39[1]) % 65536;
			table[i2] = std::cos(9.587379924285257e-05 * double(iRec39[0]));
			iVec8[1] = iVec8[0];
			iRec39[1] = iRec39[0];
		}
	}

};

static bbdmi_multi_granulator14SIG1* newbbdmi_multi_granulator14SIG1() { return (bbdmi_multi_granulator14SIG1*)new bbdmi_multi_granulator14SIG1(); }
static void deletebbdmi_multi_granulator14SIG1(bbdmi_multi_granulator14SIG1* dsp) { delete dsp; }

static double bbdmi_multi_granulator14_faustpower2_f(double value) {
	return value * value;
}
static double bbdmi_multi_granulator14_faustpower3_f(double value) {
	return value * value * value;
}
static double ftbl0bbdmi_multi_granulator14SIG0[65536];
static double ftbl1bbdmi_multi_granulator14SIG1[65536];

class bbdmi_multi_granulator14 : public dsp {
	
 public:
	
	int fSampleRate;
	double fConst0;
	double fConst1;
	FAUSTFLOAT fVslider0;
	double fConst2;
	int iVec0[2];
	double fRec2[2];
	int IOTA0;
	double fConst3;
	FAUSTFLOAT fVslider1;
	double fConst4;
	FAUSTFLOAT fVslider2;
	double fRec11[2];
	FAUSTFLOAT fVslider3;
	double fRec12[2];
	int iRec10[2];
	FAUSTFLOAT fVslider4;
	double fRec14[2];
	FAUSTFLOAT fVslider5;
	double fRec15[2];
	int iRec16[2];
	int iRec13[2];
	double fRec9[2];
	int iRec8[2];
	int iVec1[2];
	int iVec2[2097152];
	FAUSTFLOAT fHslider0;
	double fRec17[2];
	int iRec7[2];
	double fRec3[2];
	double fRec4[2];
	double fRec5[2];
	double fRec6[2];
	double fVec3[131072];
	FAUSTFLOAT fVslider6;
	double fRec19[2];
	double fRec18[2];
	double fConst5;
	FAUSTFLOAT fVslider7;
	double fRec22[2];
	FAUSTFLOAT fVslider8;
	double fRec24[2];
	double fRec23[3];
	double fVec4[2];
	double fRec21[2];
	double fVec5[2097152];
	double fRec20[2];
	FAUSTFLOAT fVslider9;
	double fRec25[2];
	double fConst6;
	double fRec26[2];
	FAUSTFLOAT fVslider10;
	double fRec27[2];
	int iVec6[2];
	int iRec30[2];
	double fConst7;
	int iRec28[2];
	double fRec29[2];
	FAUSTFLOAT fVslider11;
	double fRec31[2];
	double fRec32[2];
	FAUSTFLOAT fVslider12;
	double fRec33[2];
	FAUSTFLOAT fVslider13;
	double fRec34[2];
	double fRec36[2];
	FAUSTFLOAT fVslider14;
	double fRec38[2];
	double fRec37[2];
	double fVec9[2];
	double fVec10[131072];
	FAUSTFLOAT fVslider15;
	double fRec41[2];
	double fRec40[2];
	double fRec1[2097152];
	double fRec0[3];
	int iRec52[2];
	int iRec51[2];
	double fRec50[2];
	int iRec49[2];
	int iVec11[2];
	int iVec12[2097152];
	int iRec53[2];
	int iRec48[2];
	double fRec44[2];
	double fRec45[2];
	double fRec46[2];
	double fRec47[2];
	double fVec13[131072];
	double fRec54[2];
	double fRec57[3];
	double fVec14[2];
	double fRec56[2];
	double fVec15[2097152];
	double fRec55[2];
	double fRec58[2];
	int iVec16[2];
	int iRec61[2];
	int iRec59[2];
	double fRec60[2];
	double fRec62[2];
	double fRec63[2];
	double fVec17[2];
	double fVec18[131072];
	double fRec64[2];
	double fRec43[2097152];
	double fRec42[3];
	int iRec75[2];
	int iRec74[2];
	double fRec73[2];
	int iRec72[2];
	int iVec19[2];
	int iVec20[2097152];
	int iRec76[2];
	int iRec71[2];
	double fRec67[2];
	double fRec68[2];
	double fRec69[2];
	double fRec70[2];
	double fVec21[131072];
	double fRec77[2];
	double fRec80[3];
	double fVec22[2];
	double fRec79[2];
	double fVec23[2097152];
	double fRec78[2];
	double fRec81[2];
	int iVec24[2];
	int iRec84[2];
	int iRec82[2];
	double fRec83[2];
	double fRec85[2];
	double fRec86[2];
	double fVec25[2];
	double fVec26[131072];
	double fRec87[2];
	double fRec66[2097152];
	double fRec65[3];
	double fRec92[3];
	double fVec27[2];
	double fRec91[2];
	double fVec28[2097152];
	int iRec101[2];
	int iRec100[2];
	double fRec99[2];
	int iRec98[2];
	int iVec29[2];
	int iVec30[2097152];
	int iRec102[2];
	int iRec97[2];
	double fRec93[2];
	double fRec94[2];
	double fRec95[2];
	double fRec96[2];
	double fRec90[2];
	double fRec103[2];
	int iVec31[2];
	int iRec106[2];
	int iRec104[2];
	double fRec105[2];
	double fRec107[2];
	double fRec108[2];
	double fVec32[2];
	double fVec33[131072];
	double fRec109[2];
	double fVec34[131072];
	double fRec110[2];
	double fRec89[2097152];
	double fRec88[3];
	double fRec115[3];
	double fVec35[2];
	double fRec114[2];
	double fVec36[2097152];
	int iRec124[2];
	int iRec123[2];
	double fRec122[2];
	int iRec121[2];
	int iVec37[2];
	int iVec38[2097152];
	int iRec125[2];
	int iRec120[2];
	double fRec116[2];
	double fRec117[2];
	double fRec118[2];
	double fRec119[2];
	double fRec113[2];
	double fRec126[2];
	int iVec39[2];
	int iRec129[2];
	int iRec127[2];
	double fRec128[2];
	double fRec130[2];
	double fRec131[2];
	double fVec40[2];
	double fVec41[131072];
	double fRec132[2];
	double fVec42[131072];
	double fRec133[2];
	double fRec112[2097152];
	double fRec111[3];
	int iRec144[2];
	int iRec143[2];
	double fRec142[2];
	int iRec141[2];
	int iVec43[2];
	int iVec44[2097152];
	int iRec145[2];
	int iRec140[2];
	double fRec136[2];
	double fRec137[2];
	double fRec138[2];
	double fRec139[2];
	double fVec45[131072];
	double fRec146[2];
	double fRec149[3];
	double fVec46[2];
	double fRec148[2];
	double fVec47[2097152];
	double fRec147[2];
	double fRec150[2];
	int iVec48[2];
	int iRec153[2];
	int iRec151[2];
	double fRec152[2];
	double fRec154[2];
	double fRec155[2];
	double fVec49[2];
	double fVec50[131072];
	double fRec156[2];
	double fRec135[2097152];
	double fRec134[3];
	int iRec167[2];
	int iRec166[2];
	double fRec165[2];
	int iRec164[2];
	int iVec51[2];
	int iVec52[2097152];
	int iRec168[2];
	int iRec163[2];
	double fRec159[2];
	double fRec160[2];
	double fRec161[2];
	double fRec162[2];
	double fVec53[131072];
	double fRec169[2];
	double fRec172[3];
	double fVec54[2];
	double fRec171[2];
	double fVec55[2097152];
	double fRec170[2];
	double fRec173[2];
	int iVec56[2];
	int iRec176[2];
	int iRec174[2];
	double fRec175[2];
	double fRec177[2];
	double fRec178[2];
	double fVec57[2];
	double fVec58[131072];
	double fRec179[2];
	double fRec158[2097152];
	double fRec157[3];
	double fRec184[3];
	double fVec59[2];
	double fRec183[2];
	double fVec60[2097152];
	int iRec193[2];
	int iRec192[2];
	double fRec191[2];
	int iRec190[2];
	int iVec61[2];
	int iVec62[2097152];
	int iRec194[2];
	int iRec189[2];
	double fRec185[2];
	double fRec186[2];
	double fRec187[2];
	double fRec188[2];
	double fRec182[2];
	double fRec195[2];
	int iVec63[2];
	int iRec198[2];
	int iRec196[2];
	double fRec197[2];
	double fRec199[2];
	double fRec200[2];
	double fVec64[2];
	double fVec65[131072];
	double fRec201[2];
	double fVec66[131072];
	double fRec202[2];
	double fRec181[2097152];
	double fRec180[3];
	double fRec207[3];
	double fVec67[2];
	double fRec206[2];
	double fVec68[2097152];
	int iRec216[2];
	int iRec215[2];
	double fRec214[2];
	int iRec213[2];
	int iVec69[2];
	int iVec70[2097152];
	int iRec217[2];
	int iRec212[2];
	double fRec208[2];
	double fRec209[2];
	double fRec210[2];
	double fRec211[2];
	double fRec205[2];
	double fRec218[2];
	int iVec71[2];
	int iRec221[2];
	int iRec219[2];
	double fRec220[2];
	double fRec222[2];
	double fRec223[2];
	double fVec72[2];
	double fVec73[131072];
	double fRec224[2];
	double fVec74[131072];
	double fRec225[2];
	double fRec204[2097152];
	double fRec203[3];
	int iRec236[2];
	int iRec235[2];
	double fRec234[2];
	int iRec233[2];
	int iVec75[2];
	int iVec76[2097152];
	int iRec237[2];
	int iRec232[2];
	double fRec228[2];
	double fRec229[2];
	double fRec230[2];
	double fRec231[2];
	double fVec77[131072];
	double fRec238[2];
	double fRec241[3];
	double fVec78[2];
	double fRec240[2];
	double fVec79[2097152];
	double fRec239[2];
	double fRec242[2];
	int iVec80[2];
	int iRec245[2];
	int iRec243[2];
	double fRec244[2];
	double fRec246[2];
	double fRec247[2];
	double fVec81[2];
	double fVec82[131072];
	double fRec248[2];
	double fRec227[2097152];
	double fRec226[3];
	int iRec259[2];
	int iRec258[2];
	double fRec257[2];
	int iRec256[2];
	int iVec83[2];
	int iVec84[2097152];
	int iRec260[2];
	int iRec255[2];
	double fRec251[2];
	double fRec252[2];
	double fRec253[2];
	double fRec254[2];
	double fVec85[131072];
	double fRec261[2];
	double fRec264[3];
	double fVec86[2];
	double fRec263[2];
	double fVec87[2097152];
	double fRec262[2];
	double fRec265[2];
	int iVec88[2];
	int iRec268[2];
	int iRec266[2];
	double fRec267[2];
	double fRec269[2];
	double fRec270[2];
	double fVec89[2];
	double fVec90[131072];
	double fRec271[2];
	double fRec250[2097152];
	double fRec249[3];
	double fRec276[3];
	double fVec91[2];
	double fRec275[2];
	double fVec92[2097152];
	int iRec285[2];
	int iRec284[2];
	double fRec283[2];
	int iRec282[2];
	int iVec93[2];
	int iVec94[2097152];
	int iRec286[2];
	int iRec281[2];
	double fRec277[2];
	double fRec278[2];
	double fRec279[2];
	double fRec280[2];
	double fRec274[2];
	double fRec287[2];
	int iVec95[2];
	int iRec290[2];
	int iRec288[2];
	double fRec289[2];
	double fRec291[2];
	double fRec292[2];
	double fVec96[2];
	double fVec97[131072];
	double fRec293[2];
	double fVec98[131072];
	double fRec294[2];
	double fRec273[2097152];
	double fRec272[3];
	double fRec299[3];
	double fVec99[2];
	double fRec298[2];
	double fVec100[2097152];
	int iRec308[2];
	int iRec307[2];
	double fRec306[2];
	int iRec305[2];
	int iVec101[2];
	int iVec102[2097152];
	int iRec309[2];
	int iRec304[2];
	double fRec300[2];
	double fRec301[2];
	double fRec302[2];
	double fRec303[2];
	double fRec297[2];
	double fRec310[2];
	int iVec103[2];
	int iRec313[2];
	int iRec311[2];
	double fRec312[2];
	double fRec314[2];
	double fRec315[2];
	double fVec104[2];
	double fVec105[131072];
	double fRec316[2];
	double fVec106[131072];
	double fRec317[2];
	double fRec296[2097152];
	double fRec295[3];
	double fRec322[3];
	double fVec107[2];
	double fRec321[2];
	double fVec108[2097152];
	int iRec331[2];
	int iRec330[2];
	double fRec329[2];
	int iRec328[2];
	int iVec109[2];
	int iVec110[2097152];
	int iRec332[2];
	int iRec327[2];
	double fRec323[2];
	double fRec324[2];
	double fRec325[2];
	double fRec326[2];
	double fRec320[2];
	double fRec333[2];
	int iVec111[2];
	int iRec336[2];
	int iRec334[2];
	double fRec335[2];
	double fRec337[2];
	double fRec338[2];
	double fVec112[2];
	double fVec113[131072];
	double fRec339[2];
	double fVec114[131072];
	double fRec340[2];
	double fRec319[2097152];
	double fRec318[3];
	
 public:
	bbdmi_multi_granulator14() {}

	void metadata(Meta* m) { 
		m->declare("aanl.lib/ADAA1:author", "Dario Sanfilippo");
		m->declare("aanl.lib/ADAA1:copyright", "Copyright (C) 2021 Dario Sanfilippo     <sanfilippo.dario@gmail.com>");
		m->declare("aanl.lib/ADAA1:license", "LGPL v3.0 license");
		m->declare("aanl.lib/arcsin:author", "Dario Sanfilippo");
		m->declare("aanl.lib/arcsin:copyright", "Copyright (C) 2021 Dario Sanfilippo     <sanfilippo.dario@gmail.com>");
		m->declare("aanl.lib/arcsin:license", "LGPL v3.0 license");
		m->declare("aanl.lib/name", "Faust Antialiased Nonlinearities");
		m->declare("aanl.lib/version", "1.3.0");
		m->declare("author", "Alain Bonardi");
		m->declare("contributor", "Francesco Di Maggio");
		m->declare("contributor", "David Fierro");
		m->declare("contributor", "Anne Sedes");
		m->declare("contributor", "Atau Tanaka");
		m->declare("contributor", "Stephen Whitmarsch");
		m->declare("basics.lib/name", "Faust Basic Element Library");
		m->declare("basics.lib/on_and_off:author", "Vince");
		m->declare("basics.lib/sAndH:author", "Romain Michon");
		m->declare("basics.lib/tabulateNd", "Copyright (C) 2023 Bart Brouns <bart@magnetophon.nl>");
		m->declare("basics.lib/version", "1.15.0");
		m->declare("bbdmi.lib/author", "Alain Bonardi");
		m->declare("bbdmi.lib/copyright", "2022-2025 BBDMI TEAM");
		m->declare("bbdmi.lib/licence", "LGPLv3");
		m->declare("bbdmi.lib/name", "BBDMI Faust Lib");
		m->declare("compile_options", "-a /Applications/Faust-2.72.14/share/faust/max-msp/max-msp64.cpp -lang cpp -i -ct 1 -cn bbdmi_multi_granulator14 -es 1 -mcd 16 -mdd 1024 -mdy 33 -uim -double -ftz 0");
		m->declare("copyright", "2022-2025 BBDMI TEAM");
		m->declare("delays.lib/name", "Faust Delay Library");
		m->declare("delays.lib/version", "1.1.0");
		m->declare("filename", "bbdmi_multi_granulator14.dsp");
		m->declare("filters.lib/fir:author", "Julius O. Smith III");
		m->declare("filters.lib/fir:copyright", "Copyright (C) 2003-2019 by Julius O. Smith III <jos@ccrma.stanford.edu>");
		m->declare("filters.lib/fir:license", "MIT-style STK-4.3 license");
		m->declare("filters.lib/highpass:author", "Julius O. Smith III");
		m->declare("filters.lib/highpass:copyright", "Copyright (C) 2003-2019 by Julius O. Smith III <jos@ccrma.stanford.edu>");
		m->declare("filters.lib/iir:author", "Julius O. Smith III");
		m->declare("filters.lib/iir:copyright", "Copyright (C) 2003-2019 by Julius O. Smith III <jos@ccrma.stanford.edu>");
		m->declare("filters.lib/iir:license", "MIT-style STK-4.3 license");
		m->declare("filters.lib/lowpass0_highpass1", "Copyright (C) 2003-2019 by Julius O. Smith III <jos@ccrma.stanford.edu>");
		m->declare("filters.lib/lowpass0_highpass1:author", "Julius O. Smith III");
		m->declare("filters.lib/lowpass:author", "Julius O. Smith III");
		m->declare("filters.lib/lowpass:copyright", "Copyright (C) 2003-2019 by Julius O. Smith III <jos@ccrma.stanford.edu>");
		m->declare("filters.lib/lowpass:license", "MIT-style STK-4.3 license");
		m->declare("filters.lib/name", "Faust Filters Library");
		m->declare("filters.lib/tf1:author", "Julius O. Smith III");
		m->declare("filters.lib/tf1:copyright", "Copyright (C) 2003-2019 by Julius O. Smith III <jos@ccrma.stanford.edu>");
		m->declare("filters.lib/tf1:license", "MIT-style STK-4.3 license");
		m->declare("filters.lib/tf1s:author", "Julius O. Smith III");
		m->declare("filters.lib/tf1s:copyright", "Copyright (C) 2003-2019 by Julius O. Smith III <jos@ccrma.stanford.edu>");
		m->declare("filters.lib/tf1s:license", "MIT-style STK-4.3 license");
		m->declare("filters.lib/tf2:author", "Julius O. Smith III");
		m->declare("filters.lib/tf2:copyright", "Copyright (C) 2003-2019 by Julius O. Smith III <jos@ccrma.stanford.edu>");
		m->declare("filters.lib/tf2:license", "MIT-style STK-4.3 license");
		m->declare("filters.lib/tf2s:author", "Julius O. Smith III");
		m->declare("filters.lib/tf2s:copyright", "Copyright (C) 2003-2019 by Julius O. Smith III <jos@ccrma.stanford.edu>");
		m->declare("filters.lib/tf2s:license", "MIT-style STK-4.3 license");
		m->declare("filters.lib/version", "1.3.0");
		m->declare("maths.lib/author", "GRAME");
		m->declare("maths.lib/copyright", "GRAME");
		m->declare("maths.lib/license", "LGPL with exception");
		m->declare("maths.lib/name", "Faust Math Library");
		m->declare("maths.lib/version", "2.8.0");
		m->declare("misceffects.lib/name", "Misc Effects Library");
		m->declare("misceffects.lib/version", "2.4.0");
		m->declare("name", "BBDMI Faust Lib");
		m->declare("oscillators.lib/name", "Faust Oscillator Library");
		m->declare("oscillators.lib/version", "1.5.1");
		m->declare("platform.lib/name", "Generic Platform Library");
		m->declare("platform.lib/version", "1.3.0");
		m->declare("signals.lib/name", "Faust Signal Routing Library");
		m->declare("signals.lib/version", "1.5.0");
	}

	virtual int getNumInputs() {
		return 14;
	}
	virtual int getNumOutputs() {
		return 14;
	}
	
	static void classInit(int sample_rate) {
		bbdmi_multi_granulator14SIG0* sig0 = newbbdmi_multi_granulator14SIG0();
		sig0->instanceInitbbdmi_multi_granulator14SIG0(sample_rate);
		sig0->fillbbdmi_multi_granulator14SIG0(65536, ftbl0bbdmi_multi_granulator14SIG0);
		bbdmi_multi_granulator14SIG1* sig1 = newbbdmi_multi_granulator14SIG1();
		sig1->instanceInitbbdmi_multi_granulator14SIG1(sample_rate);
		sig1->fillbbdmi_multi_granulator14SIG1(65536, ftbl1bbdmi_multi_granulator14SIG1);
		deletebbdmi_multi_granulator14SIG0(sig0);
		deletebbdmi_multi_granulator14SIG1(sig1);
	}
	
	virtual void instanceConstants(int sample_rate) {
		fSampleRate = sample_rate;
		fConst0 = std::min<double>(1.92e+05, std::max<double>(1.0, double(fSampleRate)));
		fConst1 = 44.1 / fConst0;
		fConst2 = 1.0 - fConst1;
		fConst3 = 1e+01 * fConst0;
		fConst4 = 0.001 * fConst0;
		fConst5 = 3.141592653589793 / fConst0;
		fConst6 = 1.0 / fConst0;
		fConst7 = 0.002 * fConst0;
	}
	
	virtual void instanceResetUserInterface() {
		fVslider0 = FAUSTFLOAT(0.5);
		fVslider1 = FAUSTFLOAT(0.0);
		fVslider2 = FAUSTFLOAT(22.0);
		fVslider3 = FAUSTFLOAT(25.0);
		fVslider4 = FAUSTFLOAT(0.0);
		fVslider5 = FAUSTFLOAT(0.7);
		fHslider0 = FAUSTFLOAT(2e+03);
		fVslider6 = FAUSTFLOAT(0.0);
		fVslider7 = FAUSTFLOAT(2e+01);
		fVslider8 = FAUSTFLOAT(2e+04);
		fVslider9 = FAUSTFLOAT(1.0);
		fVslider10 = FAUSTFLOAT(0.5);
		fVslider11 = FAUSTFLOAT(0.0);
		fVslider12 = FAUSTFLOAT(0.5);
		fVslider13 = FAUSTFLOAT(0.0);
		fVslider14 = FAUSTFLOAT(4e+03);
		fVslider15 = FAUSTFLOAT(0.0);
	}
	
	virtual void instanceClear() {
		for (int l0 = 0; l0 < 2; l0 = l0 + 1) {
			iVec0[l0] = 0;
		}
		for (int l1 = 0; l1 < 2; l1 = l1 + 1) {
			fRec2[l1] = 0.0;
		}
		IOTA0 = 0;
		for (int l2 = 0; l2 < 2; l2 = l2 + 1) {
			fRec11[l2] = 0.0;
		}
		for (int l3 = 0; l3 < 2; l3 = l3 + 1) {
			fRec12[l3] = 0.0;
		}
		for (int l4 = 0; l4 < 2; l4 = l4 + 1) {
			iRec10[l4] = 0;
		}
		for (int l5 = 0; l5 < 2; l5 = l5 + 1) {
			fRec14[l5] = 0.0;
		}
		for (int l6 = 0; l6 < 2; l6 = l6 + 1) {
			fRec15[l6] = 0.0;
		}
		for (int l7 = 0; l7 < 2; l7 = l7 + 1) {
			iRec16[l7] = 0;
		}
		for (int l8 = 0; l8 < 2; l8 = l8 + 1) {
			iRec13[l8] = 0;
		}
		for (int l9 = 0; l9 < 2; l9 = l9 + 1) {
			fRec9[l9] = 0.0;
		}
		for (int l10 = 0; l10 < 2; l10 = l10 + 1) {
			iRec8[l10] = 0;
		}
		for (int l11 = 0; l11 < 2; l11 = l11 + 1) {
			iVec1[l11] = 0;
		}
		for (int l12 = 0; l12 < 2097152; l12 = l12 + 1) {
			iVec2[l12] = 0;
		}
		for (int l13 = 0; l13 < 2; l13 = l13 + 1) {
			fRec17[l13] = 0.0;
		}
		for (int l14 = 0; l14 < 2; l14 = l14 + 1) {
			iRec7[l14] = 0;
		}
		for (int l15 = 0; l15 < 2; l15 = l15 + 1) {
			fRec3[l15] = 0.0;
		}
		for (int l16 = 0; l16 < 2; l16 = l16 + 1) {
			fRec4[l16] = 0.0;
		}
		for (int l17 = 0; l17 < 2; l17 = l17 + 1) {
			fRec5[l17] = 0.0;
		}
		for (int l18 = 0; l18 < 2; l18 = l18 + 1) {
			fRec6[l18] = 0.0;
		}
		for (int l19 = 0; l19 < 131072; l19 = l19 + 1) {
			fVec3[l19] = 0.0;
		}
		for (int l20 = 0; l20 < 2; l20 = l20 + 1) {
			fRec19[l20] = 0.0;
		}
		for (int l21 = 0; l21 < 2; l21 = l21 + 1) {
			fRec18[l21] = 0.0;
		}
		for (int l22 = 0; l22 < 2; l22 = l22 + 1) {
			fRec22[l22] = 0.0;
		}
		for (int l23 = 0; l23 < 2; l23 = l23 + 1) {
			fRec24[l23] = 0.0;
		}
		for (int l24 = 0; l24 < 3; l24 = l24 + 1) {
			fRec23[l24] = 0.0;
		}
		for (int l25 = 0; l25 < 2; l25 = l25 + 1) {
			fVec4[l25] = 0.0;
		}
		for (int l26 = 0; l26 < 2; l26 = l26 + 1) {
			fRec21[l26] = 0.0;
		}
		for (int l27 = 0; l27 < 2097152; l27 = l27 + 1) {
			fVec5[l27] = 0.0;
		}
		for (int l28 = 0; l28 < 2; l28 = l28 + 1) {
			fRec20[l28] = 0.0;
		}
		for (int l29 = 0; l29 < 2; l29 = l29 + 1) {
			fRec25[l29] = 0.0;
		}
		for (int l30 = 0; l30 < 2; l30 = l30 + 1) {
			fRec26[l30] = 0.0;
		}
		for (int l31 = 0; l31 < 2; l31 = l31 + 1) {
			fRec27[l31] = 0.0;
		}
		for (int l32 = 0; l32 < 2; l32 = l32 + 1) {
			iVec6[l32] = 0;
		}
		for (int l33 = 0; l33 < 2; l33 = l33 + 1) {
			iRec30[l33] = 0;
		}
		for (int l34 = 0; l34 < 2; l34 = l34 + 1) {
			iRec28[l34] = 0;
		}
		for (int l35 = 0; l35 < 2; l35 = l35 + 1) {
			fRec29[l35] = 0.0;
		}
		for (int l36 = 0; l36 < 2; l36 = l36 + 1) {
			fRec31[l36] = 0.0;
		}
		for (int l37 = 0; l37 < 2; l37 = l37 + 1) {
			fRec32[l37] = 0.0;
		}
		for (int l38 = 0; l38 < 2; l38 = l38 + 1) {
			fRec33[l38] = 0.0;
		}
		for (int l39 = 0; l39 < 2; l39 = l39 + 1) {
			fRec34[l39] = 0.0;
		}
		for (int l42 = 0; l42 < 2; l42 = l42 + 1) {
			fRec36[l42] = 0.0;
		}
		for (int l43 = 0; l43 < 2; l43 = l43 + 1) {
			fRec38[l43] = 0.0;
		}
		for (int l44 = 0; l44 < 2; l44 = l44 + 1) {
			fRec37[l44] = 0.0;
		}
		for (int l47 = 0; l47 < 2; l47 = l47 + 1) {
			fVec9[l47] = 0.0;
		}
		for (int l48 = 0; l48 < 131072; l48 = l48 + 1) {
			fVec10[l48] = 0.0;
		}
		for (int l49 = 0; l49 < 2; l49 = l49 + 1) {
			fRec41[l49] = 0.0;
		}
		for (int l50 = 0; l50 < 2; l50 = l50 + 1) {
			fRec40[l50] = 0.0;
		}
		for (int l51 = 0; l51 < 2097152; l51 = l51 + 1) {
			fRec1[l51] = 0.0;
		}
		for (int l52 = 0; l52 < 3; l52 = l52 + 1) {
			fRec0[l52] = 0.0;
		}
		for (int l53 = 0; l53 < 2; l53 = l53 + 1) {
			iRec52[l53] = 0;
		}
		for (int l54 = 0; l54 < 2; l54 = l54 + 1) {
			iRec51[l54] = 0;
		}
		for (int l55 = 0; l55 < 2; l55 = l55 + 1) {
			fRec50[l55] = 0.0;
		}
		for (int l56 = 0; l56 < 2; l56 = l56 + 1) {
			iRec49[l56] = 0;
		}
		for (int l57 = 0; l57 < 2; l57 = l57 + 1) {
			iVec11[l57] = 0;
		}
		for (int l58 = 0; l58 < 2097152; l58 = l58 + 1) {
			iVec12[l58] = 0;
		}
		for (int l59 = 0; l59 < 2; l59 = l59 + 1) {
			iRec53[l59] = 0;
		}
		for (int l60 = 0; l60 < 2; l60 = l60 + 1) {
			iRec48[l60] = 0;
		}
		for (int l61 = 0; l61 < 2; l61 = l61 + 1) {
			fRec44[l61] = 0.0;
		}
		for (int l62 = 0; l62 < 2; l62 = l62 + 1) {
			fRec45[l62] = 0.0;
		}
		for (int l63 = 0; l63 < 2; l63 = l63 + 1) {
			fRec46[l63] = 0.0;
		}
		for (int l64 = 0; l64 < 2; l64 = l64 + 1) {
			fRec47[l64] = 0.0;
		}
		for (int l65 = 0; l65 < 131072; l65 = l65 + 1) {
			fVec13[l65] = 0.0;
		}
		for (int l66 = 0; l66 < 2; l66 = l66 + 1) {
			fRec54[l66] = 0.0;
		}
		for (int l67 = 0; l67 < 3; l67 = l67 + 1) {
			fRec57[l67] = 0.0;
		}
		for (int l68 = 0; l68 < 2; l68 = l68 + 1) {
			fVec14[l68] = 0.0;
		}
		for (int l69 = 0; l69 < 2; l69 = l69 + 1) {
			fRec56[l69] = 0.0;
		}
		for (int l70 = 0; l70 < 2097152; l70 = l70 + 1) {
			fVec15[l70] = 0.0;
		}
		for (int l71 = 0; l71 < 2; l71 = l71 + 1) {
			fRec55[l71] = 0.0;
		}
		for (int l72 = 0; l72 < 2; l72 = l72 + 1) {
			fRec58[l72] = 0.0;
		}
		for (int l73 = 0; l73 < 2; l73 = l73 + 1) {
			iVec16[l73] = 0;
		}
		for (int l74 = 0; l74 < 2; l74 = l74 + 1) {
			iRec61[l74] = 0;
		}
		for (int l75 = 0; l75 < 2; l75 = l75 + 1) {
			iRec59[l75] = 0;
		}
		for (int l76 = 0; l76 < 2; l76 = l76 + 1) {
			fRec60[l76] = 0.0;
		}
		for (int l77 = 0; l77 < 2; l77 = l77 + 1) {
			fRec62[l77] = 0.0;
		}
		for (int l78 = 0; l78 < 2; l78 = l78 + 1) {
			fRec63[l78] = 0.0;
		}
		for (int l79 = 0; l79 < 2; l79 = l79 + 1) {
			fVec17[l79] = 0.0;
		}
		for (int l80 = 0; l80 < 131072; l80 = l80 + 1) {
			fVec18[l80] = 0.0;
		}
		for (int l81 = 0; l81 < 2; l81 = l81 + 1) {
			fRec64[l81] = 0.0;
		}
		for (int l82 = 0; l82 < 2097152; l82 = l82 + 1) {
			fRec43[l82] = 0.0;
		}
		for (int l83 = 0; l83 < 3; l83 = l83 + 1) {
			fRec42[l83] = 0.0;
		}
		for (int l84 = 0; l84 < 2; l84 = l84 + 1) {
			iRec75[l84] = 0;
		}
		for (int l85 = 0; l85 < 2; l85 = l85 + 1) {
			iRec74[l85] = 0;
		}
		for (int l86 = 0; l86 < 2; l86 = l86 + 1) {
			fRec73[l86] = 0.0;
		}
		for (int l87 = 0; l87 < 2; l87 = l87 + 1) {
			iRec72[l87] = 0;
		}
		for (int l88 = 0; l88 < 2; l88 = l88 + 1) {
			iVec19[l88] = 0;
		}
		for (int l89 = 0; l89 < 2097152; l89 = l89 + 1) {
			iVec20[l89] = 0;
		}
		for (int l90 = 0; l90 < 2; l90 = l90 + 1) {
			iRec76[l90] = 0;
		}
		for (int l91 = 0; l91 < 2; l91 = l91 + 1) {
			iRec71[l91] = 0;
		}
		for (int l92 = 0; l92 < 2; l92 = l92 + 1) {
			fRec67[l92] = 0.0;
		}
		for (int l93 = 0; l93 < 2; l93 = l93 + 1) {
			fRec68[l93] = 0.0;
		}
		for (int l94 = 0; l94 < 2; l94 = l94 + 1) {
			fRec69[l94] = 0.0;
		}
		for (int l95 = 0; l95 < 2; l95 = l95 + 1) {
			fRec70[l95] = 0.0;
		}
		for (int l96 = 0; l96 < 131072; l96 = l96 + 1) {
			fVec21[l96] = 0.0;
		}
		for (int l97 = 0; l97 < 2; l97 = l97 + 1) {
			fRec77[l97] = 0.0;
		}
		for (int l98 = 0; l98 < 3; l98 = l98 + 1) {
			fRec80[l98] = 0.0;
		}
		for (int l99 = 0; l99 < 2; l99 = l99 + 1) {
			fVec22[l99] = 0.0;
		}
		for (int l100 = 0; l100 < 2; l100 = l100 + 1) {
			fRec79[l100] = 0.0;
		}
		for (int l101 = 0; l101 < 2097152; l101 = l101 + 1) {
			fVec23[l101] = 0.0;
		}
		for (int l102 = 0; l102 < 2; l102 = l102 + 1) {
			fRec78[l102] = 0.0;
		}
		for (int l103 = 0; l103 < 2; l103 = l103 + 1) {
			fRec81[l103] = 0.0;
		}
		for (int l104 = 0; l104 < 2; l104 = l104 + 1) {
			iVec24[l104] = 0;
		}
		for (int l105 = 0; l105 < 2; l105 = l105 + 1) {
			iRec84[l105] = 0;
		}
		for (int l106 = 0; l106 < 2; l106 = l106 + 1) {
			iRec82[l106] = 0;
		}
		for (int l107 = 0; l107 < 2; l107 = l107 + 1) {
			fRec83[l107] = 0.0;
		}
		for (int l108 = 0; l108 < 2; l108 = l108 + 1) {
			fRec85[l108] = 0.0;
		}
		for (int l109 = 0; l109 < 2; l109 = l109 + 1) {
			fRec86[l109] = 0.0;
		}
		for (int l110 = 0; l110 < 2; l110 = l110 + 1) {
			fVec25[l110] = 0.0;
		}
		for (int l111 = 0; l111 < 131072; l111 = l111 + 1) {
			fVec26[l111] = 0.0;
		}
		for (int l112 = 0; l112 < 2; l112 = l112 + 1) {
			fRec87[l112] = 0.0;
		}
		for (int l113 = 0; l113 < 2097152; l113 = l113 + 1) {
			fRec66[l113] = 0.0;
		}
		for (int l114 = 0; l114 < 3; l114 = l114 + 1) {
			fRec65[l114] = 0.0;
		}
		for (int l115 = 0; l115 < 3; l115 = l115 + 1) {
			fRec92[l115] = 0.0;
		}
		for (int l116 = 0; l116 < 2; l116 = l116 + 1) {
			fVec27[l116] = 0.0;
		}
		for (int l117 = 0; l117 < 2; l117 = l117 + 1) {
			fRec91[l117] = 0.0;
		}
		for (int l118 = 0; l118 < 2097152; l118 = l118 + 1) {
			fVec28[l118] = 0.0;
		}
		for (int l119 = 0; l119 < 2; l119 = l119 + 1) {
			iRec101[l119] = 0;
		}
		for (int l120 = 0; l120 < 2; l120 = l120 + 1) {
			iRec100[l120] = 0;
		}
		for (int l121 = 0; l121 < 2; l121 = l121 + 1) {
			fRec99[l121] = 0.0;
		}
		for (int l122 = 0; l122 < 2; l122 = l122 + 1) {
			iRec98[l122] = 0;
		}
		for (int l123 = 0; l123 < 2; l123 = l123 + 1) {
			iVec29[l123] = 0;
		}
		for (int l124 = 0; l124 < 2097152; l124 = l124 + 1) {
			iVec30[l124] = 0;
		}
		for (int l125 = 0; l125 < 2; l125 = l125 + 1) {
			iRec102[l125] = 0;
		}
		for (int l126 = 0; l126 < 2; l126 = l126 + 1) {
			iRec97[l126] = 0;
		}
		for (int l127 = 0; l127 < 2; l127 = l127 + 1) {
			fRec93[l127] = 0.0;
		}
		for (int l128 = 0; l128 < 2; l128 = l128 + 1) {
			fRec94[l128] = 0.0;
		}
		for (int l129 = 0; l129 < 2; l129 = l129 + 1) {
			fRec95[l129] = 0.0;
		}
		for (int l130 = 0; l130 < 2; l130 = l130 + 1) {
			fRec96[l130] = 0.0;
		}
		for (int l131 = 0; l131 < 2; l131 = l131 + 1) {
			fRec90[l131] = 0.0;
		}
		for (int l132 = 0; l132 < 2; l132 = l132 + 1) {
			fRec103[l132] = 0.0;
		}
		for (int l133 = 0; l133 < 2; l133 = l133 + 1) {
			iVec31[l133] = 0;
		}
		for (int l134 = 0; l134 < 2; l134 = l134 + 1) {
			iRec106[l134] = 0;
		}
		for (int l135 = 0; l135 < 2; l135 = l135 + 1) {
			iRec104[l135] = 0;
		}
		for (int l136 = 0; l136 < 2; l136 = l136 + 1) {
			fRec105[l136] = 0.0;
		}
		for (int l137 = 0; l137 < 2; l137 = l137 + 1) {
			fRec107[l137] = 0.0;
		}
		for (int l138 = 0; l138 < 2; l138 = l138 + 1) {
			fRec108[l138] = 0.0;
		}
		for (int l139 = 0; l139 < 2; l139 = l139 + 1) {
			fVec32[l139] = 0.0;
		}
		for (int l140 = 0; l140 < 131072; l140 = l140 + 1) {
			fVec33[l140] = 0.0;
		}
		for (int l141 = 0; l141 < 2; l141 = l141 + 1) {
			fRec109[l141] = 0.0;
		}
		for (int l142 = 0; l142 < 131072; l142 = l142 + 1) {
			fVec34[l142] = 0.0;
		}
		for (int l143 = 0; l143 < 2; l143 = l143 + 1) {
			fRec110[l143] = 0.0;
		}
		for (int l144 = 0; l144 < 2097152; l144 = l144 + 1) {
			fRec89[l144] = 0.0;
		}
		for (int l145 = 0; l145 < 3; l145 = l145 + 1) {
			fRec88[l145] = 0.0;
		}
		for (int l146 = 0; l146 < 3; l146 = l146 + 1) {
			fRec115[l146] = 0.0;
		}
		for (int l147 = 0; l147 < 2; l147 = l147 + 1) {
			fVec35[l147] = 0.0;
		}
		for (int l148 = 0; l148 < 2; l148 = l148 + 1) {
			fRec114[l148] = 0.0;
		}
		for (int l149 = 0; l149 < 2097152; l149 = l149 + 1) {
			fVec36[l149] = 0.0;
		}
		for (int l150 = 0; l150 < 2; l150 = l150 + 1) {
			iRec124[l150] = 0;
		}
		for (int l151 = 0; l151 < 2; l151 = l151 + 1) {
			iRec123[l151] = 0;
		}
		for (int l152 = 0; l152 < 2; l152 = l152 + 1) {
			fRec122[l152] = 0.0;
		}
		for (int l153 = 0; l153 < 2; l153 = l153 + 1) {
			iRec121[l153] = 0;
		}
		for (int l154 = 0; l154 < 2; l154 = l154 + 1) {
			iVec37[l154] = 0;
		}
		for (int l155 = 0; l155 < 2097152; l155 = l155 + 1) {
			iVec38[l155] = 0;
		}
		for (int l156 = 0; l156 < 2; l156 = l156 + 1) {
			iRec125[l156] = 0;
		}
		for (int l157 = 0; l157 < 2; l157 = l157 + 1) {
			iRec120[l157] = 0;
		}
		for (int l158 = 0; l158 < 2; l158 = l158 + 1) {
			fRec116[l158] = 0.0;
		}
		for (int l159 = 0; l159 < 2; l159 = l159 + 1) {
			fRec117[l159] = 0.0;
		}
		for (int l160 = 0; l160 < 2; l160 = l160 + 1) {
			fRec118[l160] = 0.0;
		}
		for (int l161 = 0; l161 < 2; l161 = l161 + 1) {
			fRec119[l161] = 0.0;
		}
		for (int l162 = 0; l162 < 2; l162 = l162 + 1) {
			fRec113[l162] = 0.0;
		}
		for (int l163 = 0; l163 < 2; l163 = l163 + 1) {
			fRec126[l163] = 0.0;
		}
		for (int l164 = 0; l164 < 2; l164 = l164 + 1) {
			iVec39[l164] = 0;
		}
		for (int l165 = 0; l165 < 2; l165 = l165 + 1) {
			iRec129[l165] = 0;
		}
		for (int l166 = 0; l166 < 2; l166 = l166 + 1) {
			iRec127[l166] = 0;
		}
		for (int l167 = 0; l167 < 2; l167 = l167 + 1) {
			fRec128[l167] = 0.0;
		}
		for (int l168 = 0; l168 < 2; l168 = l168 + 1) {
			fRec130[l168] = 0.0;
		}
		for (int l169 = 0; l169 < 2; l169 = l169 + 1) {
			fRec131[l169] = 0.0;
		}
		for (int l170 = 0; l170 < 2; l170 = l170 + 1) {
			fVec40[l170] = 0.0;
		}
		for (int l171 = 0; l171 < 131072; l171 = l171 + 1) {
			fVec41[l171] = 0.0;
		}
		for (int l172 = 0; l172 < 2; l172 = l172 + 1) {
			fRec132[l172] = 0.0;
		}
		for (int l173 = 0; l173 < 131072; l173 = l173 + 1) {
			fVec42[l173] = 0.0;
		}
		for (int l174 = 0; l174 < 2; l174 = l174 + 1) {
			fRec133[l174] = 0.0;
		}
		for (int l175 = 0; l175 < 2097152; l175 = l175 + 1) {
			fRec112[l175] = 0.0;
		}
		for (int l176 = 0; l176 < 3; l176 = l176 + 1) {
			fRec111[l176] = 0.0;
		}
		for (int l177 = 0; l177 < 2; l177 = l177 + 1) {
			iRec144[l177] = 0;
		}
		for (int l178 = 0; l178 < 2; l178 = l178 + 1) {
			iRec143[l178] = 0;
		}
		for (int l179 = 0; l179 < 2; l179 = l179 + 1) {
			fRec142[l179] = 0.0;
		}
		for (int l180 = 0; l180 < 2; l180 = l180 + 1) {
			iRec141[l180] = 0;
		}
		for (int l181 = 0; l181 < 2; l181 = l181 + 1) {
			iVec43[l181] = 0;
		}
		for (int l182 = 0; l182 < 2097152; l182 = l182 + 1) {
			iVec44[l182] = 0;
		}
		for (int l183 = 0; l183 < 2; l183 = l183 + 1) {
			iRec145[l183] = 0;
		}
		for (int l184 = 0; l184 < 2; l184 = l184 + 1) {
			iRec140[l184] = 0;
		}
		for (int l185 = 0; l185 < 2; l185 = l185 + 1) {
			fRec136[l185] = 0.0;
		}
		for (int l186 = 0; l186 < 2; l186 = l186 + 1) {
			fRec137[l186] = 0.0;
		}
		for (int l187 = 0; l187 < 2; l187 = l187 + 1) {
			fRec138[l187] = 0.0;
		}
		for (int l188 = 0; l188 < 2; l188 = l188 + 1) {
			fRec139[l188] = 0.0;
		}
		for (int l189 = 0; l189 < 131072; l189 = l189 + 1) {
			fVec45[l189] = 0.0;
		}
		for (int l190 = 0; l190 < 2; l190 = l190 + 1) {
			fRec146[l190] = 0.0;
		}
		for (int l191 = 0; l191 < 3; l191 = l191 + 1) {
			fRec149[l191] = 0.0;
		}
		for (int l192 = 0; l192 < 2; l192 = l192 + 1) {
			fVec46[l192] = 0.0;
		}
		for (int l193 = 0; l193 < 2; l193 = l193 + 1) {
			fRec148[l193] = 0.0;
		}
		for (int l194 = 0; l194 < 2097152; l194 = l194 + 1) {
			fVec47[l194] = 0.0;
		}
		for (int l195 = 0; l195 < 2; l195 = l195 + 1) {
			fRec147[l195] = 0.0;
		}
		for (int l196 = 0; l196 < 2; l196 = l196 + 1) {
			fRec150[l196] = 0.0;
		}
		for (int l197 = 0; l197 < 2; l197 = l197 + 1) {
			iVec48[l197] = 0;
		}
		for (int l198 = 0; l198 < 2; l198 = l198 + 1) {
			iRec153[l198] = 0;
		}
		for (int l199 = 0; l199 < 2; l199 = l199 + 1) {
			iRec151[l199] = 0;
		}
		for (int l200 = 0; l200 < 2; l200 = l200 + 1) {
			fRec152[l200] = 0.0;
		}
		for (int l201 = 0; l201 < 2; l201 = l201 + 1) {
			fRec154[l201] = 0.0;
		}
		for (int l202 = 0; l202 < 2; l202 = l202 + 1) {
			fRec155[l202] = 0.0;
		}
		for (int l203 = 0; l203 < 2; l203 = l203 + 1) {
			fVec49[l203] = 0.0;
		}
		for (int l204 = 0; l204 < 131072; l204 = l204 + 1) {
			fVec50[l204] = 0.0;
		}
		for (int l205 = 0; l205 < 2; l205 = l205 + 1) {
			fRec156[l205] = 0.0;
		}
		for (int l206 = 0; l206 < 2097152; l206 = l206 + 1) {
			fRec135[l206] = 0.0;
		}
		for (int l207 = 0; l207 < 3; l207 = l207 + 1) {
			fRec134[l207] = 0.0;
		}
		for (int l208 = 0; l208 < 2; l208 = l208 + 1) {
			iRec167[l208] = 0;
		}
		for (int l209 = 0; l209 < 2; l209 = l209 + 1) {
			iRec166[l209] = 0;
		}
		for (int l210 = 0; l210 < 2; l210 = l210 + 1) {
			fRec165[l210] = 0.0;
		}
		for (int l211 = 0; l211 < 2; l211 = l211 + 1) {
			iRec164[l211] = 0;
		}
		for (int l212 = 0; l212 < 2; l212 = l212 + 1) {
			iVec51[l212] = 0;
		}
		for (int l213 = 0; l213 < 2097152; l213 = l213 + 1) {
			iVec52[l213] = 0;
		}
		for (int l214 = 0; l214 < 2; l214 = l214 + 1) {
			iRec168[l214] = 0;
		}
		for (int l215 = 0; l215 < 2; l215 = l215 + 1) {
			iRec163[l215] = 0;
		}
		for (int l216 = 0; l216 < 2; l216 = l216 + 1) {
			fRec159[l216] = 0.0;
		}
		for (int l217 = 0; l217 < 2; l217 = l217 + 1) {
			fRec160[l217] = 0.0;
		}
		for (int l218 = 0; l218 < 2; l218 = l218 + 1) {
			fRec161[l218] = 0.0;
		}
		for (int l219 = 0; l219 < 2; l219 = l219 + 1) {
			fRec162[l219] = 0.0;
		}
		for (int l220 = 0; l220 < 131072; l220 = l220 + 1) {
			fVec53[l220] = 0.0;
		}
		for (int l221 = 0; l221 < 2; l221 = l221 + 1) {
			fRec169[l221] = 0.0;
		}
		for (int l222 = 0; l222 < 3; l222 = l222 + 1) {
			fRec172[l222] = 0.0;
		}
		for (int l223 = 0; l223 < 2; l223 = l223 + 1) {
			fVec54[l223] = 0.0;
		}
		for (int l224 = 0; l224 < 2; l224 = l224 + 1) {
			fRec171[l224] = 0.0;
		}
		for (int l225 = 0; l225 < 2097152; l225 = l225 + 1) {
			fVec55[l225] = 0.0;
		}
		for (int l226 = 0; l226 < 2; l226 = l226 + 1) {
			fRec170[l226] = 0.0;
		}
		for (int l227 = 0; l227 < 2; l227 = l227 + 1) {
			fRec173[l227] = 0.0;
		}
		for (int l228 = 0; l228 < 2; l228 = l228 + 1) {
			iVec56[l228] = 0;
		}
		for (int l229 = 0; l229 < 2; l229 = l229 + 1) {
			iRec176[l229] = 0;
		}
		for (int l230 = 0; l230 < 2; l230 = l230 + 1) {
			iRec174[l230] = 0;
		}
		for (int l231 = 0; l231 < 2; l231 = l231 + 1) {
			fRec175[l231] = 0.0;
		}
		for (int l232 = 0; l232 < 2; l232 = l232 + 1) {
			fRec177[l232] = 0.0;
		}
		for (int l233 = 0; l233 < 2; l233 = l233 + 1) {
			fRec178[l233] = 0.0;
		}
		for (int l234 = 0; l234 < 2; l234 = l234 + 1) {
			fVec57[l234] = 0.0;
		}
		for (int l235 = 0; l235 < 131072; l235 = l235 + 1) {
			fVec58[l235] = 0.0;
		}
		for (int l236 = 0; l236 < 2; l236 = l236 + 1) {
			fRec179[l236] = 0.0;
		}
		for (int l237 = 0; l237 < 2097152; l237 = l237 + 1) {
			fRec158[l237] = 0.0;
		}
		for (int l238 = 0; l238 < 3; l238 = l238 + 1) {
			fRec157[l238] = 0.0;
		}
		for (int l239 = 0; l239 < 3; l239 = l239 + 1) {
			fRec184[l239] = 0.0;
		}
		for (int l240 = 0; l240 < 2; l240 = l240 + 1) {
			fVec59[l240] = 0.0;
		}
		for (int l241 = 0; l241 < 2; l241 = l241 + 1) {
			fRec183[l241] = 0.0;
		}
		for (int l242 = 0; l242 < 2097152; l242 = l242 + 1) {
			fVec60[l242] = 0.0;
		}
		for (int l243 = 0; l243 < 2; l243 = l243 + 1) {
			iRec193[l243] = 0;
		}
		for (int l244 = 0; l244 < 2; l244 = l244 + 1) {
			iRec192[l244] = 0;
		}
		for (int l245 = 0; l245 < 2; l245 = l245 + 1) {
			fRec191[l245] = 0.0;
		}
		for (int l246 = 0; l246 < 2; l246 = l246 + 1) {
			iRec190[l246] = 0;
		}
		for (int l247 = 0; l247 < 2; l247 = l247 + 1) {
			iVec61[l247] = 0;
		}
		for (int l248 = 0; l248 < 2097152; l248 = l248 + 1) {
			iVec62[l248] = 0;
		}
		for (int l249 = 0; l249 < 2; l249 = l249 + 1) {
			iRec194[l249] = 0;
		}
		for (int l250 = 0; l250 < 2; l250 = l250 + 1) {
			iRec189[l250] = 0;
		}
		for (int l251 = 0; l251 < 2; l251 = l251 + 1) {
			fRec185[l251] = 0.0;
		}
		for (int l252 = 0; l252 < 2; l252 = l252 + 1) {
			fRec186[l252] = 0.0;
		}
		for (int l253 = 0; l253 < 2; l253 = l253 + 1) {
			fRec187[l253] = 0.0;
		}
		for (int l254 = 0; l254 < 2; l254 = l254 + 1) {
			fRec188[l254] = 0.0;
		}
		for (int l255 = 0; l255 < 2; l255 = l255 + 1) {
			fRec182[l255] = 0.0;
		}
		for (int l256 = 0; l256 < 2; l256 = l256 + 1) {
			fRec195[l256] = 0.0;
		}
		for (int l257 = 0; l257 < 2; l257 = l257 + 1) {
			iVec63[l257] = 0;
		}
		for (int l258 = 0; l258 < 2; l258 = l258 + 1) {
			iRec198[l258] = 0;
		}
		for (int l259 = 0; l259 < 2; l259 = l259 + 1) {
			iRec196[l259] = 0;
		}
		for (int l260 = 0; l260 < 2; l260 = l260 + 1) {
			fRec197[l260] = 0.0;
		}
		for (int l261 = 0; l261 < 2; l261 = l261 + 1) {
			fRec199[l261] = 0.0;
		}
		for (int l262 = 0; l262 < 2; l262 = l262 + 1) {
			fRec200[l262] = 0.0;
		}
		for (int l263 = 0; l263 < 2; l263 = l263 + 1) {
			fVec64[l263] = 0.0;
		}
		for (int l264 = 0; l264 < 131072; l264 = l264 + 1) {
			fVec65[l264] = 0.0;
		}
		for (int l265 = 0; l265 < 2; l265 = l265 + 1) {
			fRec201[l265] = 0.0;
		}
		for (int l266 = 0; l266 < 131072; l266 = l266 + 1) {
			fVec66[l266] = 0.0;
		}
		for (int l267 = 0; l267 < 2; l267 = l267 + 1) {
			fRec202[l267] = 0.0;
		}
		for (int l268 = 0; l268 < 2097152; l268 = l268 + 1) {
			fRec181[l268] = 0.0;
		}
		for (int l269 = 0; l269 < 3; l269 = l269 + 1) {
			fRec180[l269] = 0.0;
		}
		for (int l270 = 0; l270 < 3; l270 = l270 + 1) {
			fRec207[l270] = 0.0;
		}
		for (int l271 = 0; l271 < 2; l271 = l271 + 1) {
			fVec67[l271] = 0.0;
		}
		for (int l272 = 0; l272 < 2; l272 = l272 + 1) {
			fRec206[l272] = 0.0;
		}
		for (int l273 = 0; l273 < 2097152; l273 = l273 + 1) {
			fVec68[l273] = 0.0;
		}
		for (int l274 = 0; l274 < 2; l274 = l274 + 1) {
			iRec216[l274] = 0;
		}
		for (int l275 = 0; l275 < 2; l275 = l275 + 1) {
			iRec215[l275] = 0;
		}
		for (int l276 = 0; l276 < 2; l276 = l276 + 1) {
			fRec214[l276] = 0.0;
		}
		for (int l277 = 0; l277 < 2; l277 = l277 + 1) {
			iRec213[l277] = 0;
		}
		for (int l278 = 0; l278 < 2; l278 = l278 + 1) {
			iVec69[l278] = 0;
		}
		for (int l279 = 0; l279 < 2097152; l279 = l279 + 1) {
			iVec70[l279] = 0;
		}
		for (int l280 = 0; l280 < 2; l280 = l280 + 1) {
			iRec217[l280] = 0;
		}
		for (int l281 = 0; l281 < 2; l281 = l281 + 1) {
			iRec212[l281] = 0;
		}
		for (int l282 = 0; l282 < 2; l282 = l282 + 1) {
			fRec208[l282] = 0.0;
		}
		for (int l283 = 0; l283 < 2; l283 = l283 + 1) {
			fRec209[l283] = 0.0;
		}
		for (int l284 = 0; l284 < 2; l284 = l284 + 1) {
			fRec210[l284] = 0.0;
		}
		for (int l285 = 0; l285 < 2; l285 = l285 + 1) {
			fRec211[l285] = 0.0;
		}
		for (int l286 = 0; l286 < 2; l286 = l286 + 1) {
			fRec205[l286] = 0.0;
		}
		for (int l287 = 0; l287 < 2; l287 = l287 + 1) {
			fRec218[l287] = 0.0;
		}
		for (int l288 = 0; l288 < 2; l288 = l288 + 1) {
			iVec71[l288] = 0;
		}
		for (int l289 = 0; l289 < 2; l289 = l289 + 1) {
			iRec221[l289] = 0;
		}
		for (int l290 = 0; l290 < 2; l290 = l290 + 1) {
			iRec219[l290] = 0;
		}
		for (int l291 = 0; l291 < 2; l291 = l291 + 1) {
			fRec220[l291] = 0.0;
		}
		for (int l292 = 0; l292 < 2; l292 = l292 + 1) {
			fRec222[l292] = 0.0;
		}
		for (int l293 = 0; l293 < 2; l293 = l293 + 1) {
			fRec223[l293] = 0.0;
		}
		for (int l294 = 0; l294 < 2; l294 = l294 + 1) {
			fVec72[l294] = 0.0;
		}
		for (int l295 = 0; l295 < 131072; l295 = l295 + 1) {
			fVec73[l295] = 0.0;
		}
		for (int l296 = 0; l296 < 2; l296 = l296 + 1) {
			fRec224[l296] = 0.0;
		}
		for (int l297 = 0; l297 < 131072; l297 = l297 + 1) {
			fVec74[l297] = 0.0;
		}
		for (int l298 = 0; l298 < 2; l298 = l298 + 1) {
			fRec225[l298] = 0.0;
		}
		for (int l299 = 0; l299 < 2097152; l299 = l299 + 1) {
			fRec204[l299] = 0.0;
		}
		for (int l300 = 0; l300 < 3; l300 = l300 + 1) {
			fRec203[l300] = 0.0;
		}
		for (int l301 = 0; l301 < 2; l301 = l301 + 1) {
			iRec236[l301] = 0;
		}
		for (int l302 = 0; l302 < 2; l302 = l302 + 1) {
			iRec235[l302] = 0;
		}
		for (int l303 = 0; l303 < 2; l303 = l303 + 1) {
			fRec234[l303] = 0.0;
		}
		for (int l304 = 0; l304 < 2; l304 = l304 + 1) {
			iRec233[l304] = 0;
		}
		for (int l305 = 0; l305 < 2; l305 = l305 + 1) {
			iVec75[l305] = 0;
		}
		for (int l306 = 0; l306 < 2097152; l306 = l306 + 1) {
			iVec76[l306] = 0;
		}
		for (int l307 = 0; l307 < 2; l307 = l307 + 1) {
			iRec237[l307] = 0;
		}
		for (int l308 = 0; l308 < 2; l308 = l308 + 1) {
			iRec232[l308] = 0;
		}
		for (int l309 = 0; l309 < 2; l309 = l309 + 1) {
			fRec228[l309] = 0.0;
		}
		for (int l310 = 0; l310 < 2; l310 = l310 + 1) {
			fRec229[l310] = 0.0;
		}
		for (int l311 = 0; l311 < 2; l311 = l311 + 1) {
			fRec230[l311] = 0.0;
		}
		for (int l312 = 0; l312 < 2; l312 = l312 + 1) {
			fRec231[l312] = 0.0;
		}
		for (int l313 = 0; l313 < 131072; l313 = l313 + 1) {
			fVec77[l313] = 0.0;
		}
		for (int l314 = 0; l314 < 2; l314 = l314 + 1) {
			fRec238[l314] = 0.0;
		}
		for (int l315 = 0; l315 < 3; l315 = l315 + 1) {
			fRec241[l315] = 0.0;
		}
		for (int l316 = 0; l316 < 2; l316 = l316 + 1) {
			fVec78[l316] = 0.0;
		}
		for (int l317 = 0; l317 < 2; l317 = l317 + 1) {
			fRec240[l317] = 0.0;
		}
		for (int l318 = 0; l318 < 2097152; l318 = l318 + 1) {
			fVec79[l318] = 0.0;
		}
		for (int l319 = 0; l319 < 2; l319 = l319 + 1) {
			fRec239[l319] = 0.0;
		}
		for (int l320 = 0; l320 < 2; l320 = l320 + 1) {
			fRec242[l320] = 0.0;
		}
		for (int l321 = 0; l321 < 2; l321 = l321 + 1) {
			iVec80[l321] = 0;
		}
		for (int l322 = 0; l322 < 2; l322 = l322 + 1) {
			iRec245[l322] = 0;
		}
		for (int l323 = 0; l323 < 2; l323 = l323 + 1) {
			iRec243[l323] = 0;
		}
		for (int l324 = 0; l324 < 2; l324 = l324 + 1) {
			fRec244[l324] = 0.0;
		}
		for (int l325 = 0; l325 < 2; l325 = l325 + 1) {
			fRec246[l325] = 0.0;
		}
		for (int l326 = 0; l326 < 2; l326 = l326 + 1) {
			fRec247[l326] = 0.0;
		}
		for (int l327 = 0; l327 < 2; l327 = l327 + 1) {
			fVec81[l327] = 0.0;
		}
		for (int l328 = 0; l328 < 131072; l328 = l328 + 1) {
			fVec82[l328] = 0.0;
		}
		for (int l329 = 0; l329 < 2; l329 = l329 + 1) {
			fRec248[l329] = 0.0;
		}
		for (int l330 = 0; l330 < 2097152; l330 = l330 + 1) {
			fRec227[l330] = 0.0;
		}
		for (int l331 = 0; l331 < 3; l331 = l331 + 1) {
			fRec226[l331] = 0.0;
		}
		for (int l332 = 0; l332 < 2; l332 = l332 + 1) {
			iRec259[l332] = 0;
		}
		for (int l333 = 0; l333 < 2; l333 = l333 + 1) {
			iRec258[l333] = 0;
		}
		for (int l334 = 0; l334 < 2; l334 = l334 + 1) {
			fRec257[l334] = 0.0;
		}
		for (int l335 = 0; l335 < 2; l335 = l335 + 1) {
			iRec256[l335] = 0;
		}
		for (int l336 = 0; l336 < 2; l336 = l336 + 1) {
			iVec83[l336] = 0;
		}
		for (int l337 = 0; l337 < 2097152; l337 = l337 + 1) {
			iVec84[l337] = 0;
		}
		for (int l338 = 0; l338 < 2; l338 = l338 + 1) {
			iRec260[l338] = 0;
		}
		for (int l339 = 0; l339 < 2; l339 = l339 + 1) {
			iRec255[l339] = 0;
		}
		for (int l340 = 0; l340 < 2; l340 = l340 + 1) {
			fRec251[l340] = 0.0;
		}
		for (int l341 = 0; l341 < 2; l341 = l341 + 1) {
			fRec252[l341] = 0.0;
		}
		for (int l342 = 0; l342 < 2; l342 = l342 + 1) {
			fRec253[l342] = 0.0;
		}
		for (int l343 = 0; l343 < 2; l343 = l343 + 1) {
			fRec254[l343] = 0.0;
		}
		for (int l344 = 0; l344 < 131072; l344 = l344 + 1) {
			fVec85[l344] = 0.0;
		}
		for (int l345 = 0; l345 < 2; l345 = l345 + 1) {
			fRec261[l345] = 0.0;
		}
		for (int l346 = 0; l346 < 3; l346 = l346 + 1) {
			fRec264[l346] = 0.0;
		}
		for (int l347 = 0; l347 < 2; l347 = l347 + 1) {
			fVec86[l347] = 0.0;
		}
		for (int l348 = 0; l348 < 2; l348 = l348 + 1) {
			fRec263[l348] = 0.0;
		}
		for (int l349 = 0; l349 < 2097152; l349 = l349 + 1) {
			fVec87[l349] = 0.0;
		}
		for (int l350 = 0; l350 < 2; l350 = l350 + 1) {
			fRec262[l350] = 0.0;
		}
		for (int l351 = 0; l351 < 2; l351 = l351 + 1) {
			fRec265[l351] = 0.0;
		}
		for (int l352 = 0; l352 < 2; l352 = l352 + 1) {
			iVec88[l352] = 0;
		}
		for (int l353 = 0; l353 < 2; l353 = l353 + 1) {
			iRec268[l353] = 0;
		}
		for (int l354 = 0; l354 < 2; l354 = l354 + 1) {
			iRec266[l354] = 0;
		}
		for (int l355 = 0; l355 < 2; l355 = l355 + 1) {
			fRec267[l355] = 0.0;
		}
		for (int l356 = 0; l356 < 2; l356 = l356 + 1) {
			fRec269[l356] = 0.0;
		}
		for (int l357 = 0; l357 < 2; l357 = l357 + 1) {
			fRec270[l357] = 0.0;
		}
		for (int l358 = 0; l358 < 2; l358 = l358 + 1) {
			fVec89[l358] = 0.0;
		}
		for (int l359 = 0; l359 < 131072; l359 = l359 + 1) {
			fVec90[l359] = 0.0;
		}
		for (int l360 = 0; l360 < 2; l360 = l360 + 1) {
			fRec271[l360] = 0.0;
		}
		for (int l361 = 0; l361 < 2097152; l361 = l361 + 1) {
			fRec250[l361] = 0.0;
		}
		for (int l362 = 0; l362 < 3; l362 = l362 + 1) {
			fRec249[l362] = 0.0;
		}
		for (int l363 = 0; l363 < 3; l363 = l363 + 1) {
			fRec276[l363] = 0.0;
		}
		for (int l364 = 0; l364 < 2; l364 = l364 + 1) {
			fVec91[l364] = 0.0;
		}
		for (int l365 = 0; l365 < 2; l365 = l365 + 1) {
			fRec275[l365] = 0.0;
		}
		for (int l366 = 0; l366 < 2097152; l366 = l366 + 1) {
			fVec92[l366] = 0.0;
		}
		for (int l367 = 0; l367 < 2; l367 = l367 + 1) {
			iRec285[l367] = 0;
		}
		for (int l368 = 0; l368 < 2; l368 = l368 + 1) {
			iRec284[l368] = 0;
		}
		for (int l369 = 0; l369 < 2; l369 = l369 + 1) {
			fRec283[l369] = 0.0;
		}
		for (int l370 = 0; l370 < 2; l370 = l370 + 1) {
			iRec282[l370] = 0;
		}
		for (int l371 = 0; l371 < 2; l371 = l371 + 1) {
			iVec93[l371] = 0;
		}
		for (int l372 = 0; l372 < 2097152; l372 = l372 + 1) {
			iVec94[l372] = 0;
		}
		for (int l373 = 0; l373 < 2; l373 = l373 + 1) {
			iRec286[l373] = 0;
		}
		for (int l374 = 0; l374 < 2; l374 = l374 + 1) {
			iRec281[l374] = 0;
		}
		for (int l375 = 0; l375 < 2; l375 = l375 + 1) {
			fRec277[l375] = 0.0;
		}
		for (int l376 = 0; l376 < 2; l376 = l376 + 1) {
			fRec278[l376] = 0.0;
		}
		for (int l377 = 0; l377 < 2; l377 = l377 + 1) {
			fRec279[l377] = 0.0;
		}
		for (int l378 = 0; l378 < 2; l378 = l378 + 1) {
			fRec280[l378] = 0.0;
		}
		for (int l379 = 0; l379 < 2; l379 = l379 + 1) {
			fRec274[l379] = 0.0;
		}
		for (int l380 = 0; l380 < 2; l380 = l380 + 1) {
			fRec287[l380] = 0.0;
		}
		for (int l381 = 0; l381 < 2; l381 = l381 + 1) {
			iVec95[l381] = 0;
		}
		for (int l382 = 0; l382 < 2; l382 = l382 + 1) {
			iRec290[l382] = 0;
		}
		for (int l383 = 0; l383 < 2; l383 = l383 + 1) {
			iRec288[l383] = 0;
		}
		for (int l384 = 0; l384 < 2; l384 = l384 + 1) {
			fRec289[l384] = 0.0;
		}
		for (int l385 = 0; l385 < 2; l385 = l385 + 1) {
			fRec291[l385] = 0.0;
		}
		for (int l386 = 0; l386 < 2; l386 = l386 + 1) {
			fRec292[l386] = 0.0;
		}
		for (int l387 = 0; l387 < 2; l387 = l387 + 1) {
			fVec96[l387] = 0.0;
		}
		for (int l388 = 0; l388 < 131072; l388 = l388 + 1) {
			fVec97[l388] = 0.0;
		}
		for (int l389 = 0; l389 < 2; l389 = l389 + 1) {
			fRec293[l389] = 0.0;
		}
		for (int l390 = 0; l390 < 131072; l390 = l390 + 1) {
			fVec98[l390] = 0.0;
		}
		for (int l391 = 0; l391 < 2; l391 = l391 + 1) {
			fRec294[l391] = 0.0;
		}
		for (int l392 = 0; l392 < 2097152; l392 = l392 + 1) {
			fRec273[l392] = 0.0;
		}
		for (int l393 = 0; l393 < 3; l393 = l393 + 1) {
			fRec272[l393] = 0.0;
		}
		for (int l394 = 0; l394 < 3; l394 = l394 + 1) {
			fRec299[l394] = 0.0;
		}
		for (int l395 = 0; l395 < 2; l395 = l395 + 1) {
			fVec99[l395] = 0.0;
		}
		for (int l396 = 0; l396 < 2; l396 = l396 + 1) {
			fRec298[l396] = 0.0;
		}
		for (int l397 = 0; l397 < 2097152; l397 = l397 + 1) {
			fVec100[l397] = 0.0;
		}
		for (int l398 = 0; l398 < 2; l398 = l398 + 1) {
			iRec308[l398] = 0;
		}
		for (int l399 = 0; l399 < 2; l399 = l399 + 1) {
			iRec307[l399] = 0;
		}
		for (int l400 = 0; l400 < 2; l400 = l400 + 1) {
			fRec306[l400] = 0.0;
		}
		for (int l401 = 0; l401 < 2; l401 = l401 + 1) {
			iRec305[l401] = 0;
		}
		for (int l402 = 0; l402 < 2; l402 = l402 + 1) {
			iVec101[l402] = 0;
		}
		for (int l403 = 0; l403 < 2097152; l403 = l403 + 1) {
			iVec102[l403] = 0;
		}
		for (int l404 = 0; l404 < 2; l404 = l404 + 1) {
			iRec309[l404] = 0;
		}
		for (int l405 = 0; l405 < 2; l405 = l405 + 1) {
			iRec304[l405] = 0;
		}
		for (int l406 = 0; l406 < 2; l406 = l406 + 1) {
			fRec300[l406] = 0.0;
		}
		for (int l407 = 0; l407 < 2; l407 = l407 + 1) {
			fRec301[l407] = 0.0;
		}
		for (int l408 = 0; l408 < 2; l408 = l408 + 1) {
			fRec302[l408] = 0.0;
		}
		for (int l409 = 0; l409 < 2; l409 = l409 + 1) {
			fRec303[l409] = 0.0;
		}
		for (int l410 = 0; l410 < 2; l410 = l410 + 1) {
			fRec297[l410] = 0.0;
		}
		for (int l411 = 0; l411 < 2; l411 = l411 + 1) {
			fRec310[l411] = 0.0;
		}
		for (int l412 = 0; l412 < 2; l412 = l412 + 1) {
			iVec103[l412] = 0;
		}
		for (int l413 = 0; l413 < 2; l413 = l413 + 1) {
			iRec313[l413] = 0;
		}
		for (int l414 = 0; l414 < 2; l414 = l414 + 1) {
			iRec311[l414] = 0;
		}
		for (int l415 = 0; l415 < 2; l415 = l415 + 1) {
			fRec312[l415] = 0.0;
		}
		for (int l416 = 0; l416 < 2; l416 = l416 + 1) {
			fRec314[l416] = 0.0;
		}
		for (int l417 = 0; l417 < 2; l417 = l417 + 1) {
			fRec315[l417] = 0.0;
		}
		for (int l418 = 0; l418 < 2; l418 = l418 + 1) {
			fVec104[l418] = 0.0;
		}
		for (int l419 = 0; l419 < 131072; l419 = l419 + 1) {
			fVec105[l419] = 0.0;
		}
		for (int l420 = 0; l420 < 2; l420 = l420 + 1) {
			fRec316[l420] = 0.0;
		}
		for (int l421 = 0; l421 < 131072; l421 = l421 + 1) {
			fVec106[l421] = 0.0;
		}
		for (int l422 = 0; l422 < 2; l422 = l422 + 1) {
			fRec317[l422] = 0.0;
		}
		for (int l423 = 0; l423 < 2097152; l423 = l423 + 1) {
			fRec296[l423] = 0.0;
		}
		for (int l424 = 0; l424 < 3; l424 = l424 + 1) {
			fRec295[l424] = 0.0;
		}
		for (int l425 = 0; l425 < 3; l425 = l425 + 1) {
			fRec322[l425] = 0.0;
		}
		for (int l426 = 0; l426 < 2; l426 = l426 + 1) {
			fVec107[l426] = 0.0;
		}
		for (int l427 = 0; l427 < 2; l427 = l427 + 1) {
			fRec321[l427] = 0.0;
		}
		for (int l428 = 0; l428 < 2097152; l428 = l428 + 1) {
			fVec108[l428] = 0.0;
		}
		for (int l429 = 0; l429 < 2; l429 = l429 + 1) {
			iRec331[l429] = 0;
		}
		for (int l430 = 0; l430 < 2; l430 = l430 + 1) {
			iRec330[l430] = 0;
		}
		for (int l431 = 0; l431 < 2; l431 = l431 + 1) {
			fRec329[l431] = 0.0;
		}
		for (int l432 = 0; l432 < 2; l432 = l432 + 1) {
			iRec328[l432] = 0;
		}
		for (int l433 = 0; l433 < 2; l433 = l433 + 1) {
			iVec109[l433] = 0;
		}
		for (int l434 = 0; l434 < 2097152; l434 = l434 + 1) {
			iVec110[l434] = 0;
		}
		for (int l435 = 0; l435 < 2; l435 = l435 + 1) {
			iRec332[l435] = 0;
		}
		for (int l436 = 0; l436 < 2; l436 = l436 + 1) {
			iRec327[l436] = 0;
		}
		for (int l437 = 0; l437 < 2; l437 = l437 + 1) {
			fRec323[l437] = 0.0;
		}
		for (int l438 = 0; l438 < 2; l438 = l438 + 1) {
			fRec324[l438] = 0.0;
		}
		for (int l439 = 0; l439 < 2; l439 = l439 + 1) {
			fRec325[l439] = 0.0;
		}
		for (int l440 = 0; l440 < 2; l440 = l440 + 1) {
			fRec326[l440] = 0.0;
		}
		for (int l441 = 0; l441 < 2; l441 = l441 + 1) {
			fRec320[l441] = 0.0;
		}
		for (int l442 = 0; l442 < 2; l442 = l442 + 1) {
			fRec333[l442] = 0.0;
		}
		for (int l443 = 0; l443 < 2; l443 = l443 + 1) {
			iVec111[l443] = 0;
		}
		for (int l444 = 0; l444 < 2; l444 = l444 + 1) {
			iRec336[l444] = 0;
		}
		for (int l445 = 0; l445 < 2; l445 = l445 + 1) {
			iRec334[l445] = 0;
		}
		for (int l446 = 0; l446 < 2; l446 = l446 + 1) {
			fRec335[l446] = 0.0;
		}
		for (int l447 = 0; l447 < 2; l447 = l447 + 1) {
			fRec337[l447] = 0.0;
		}
		for (int l448 = 0; l448 < 2; l448 = l448 + 1) {
			fRec338[l448] = 0.0;
		}
		for (int l449 = 0; l449 < 2; l449 = l449 + 1) {
			fVec112[l449] = 0.0;
		}
		for (int l450 = 0; l450 < 131072; l450 = l450 + 1) {
			fVec113[l450] = 0.0;
		}
		for (int l451 = 0; l451 < 2; l451 = l451 + 1) {
			fRec339[l451] = 0.0;
		}
		for (int l452 = 0; l452 < 131072; l452 = l452 + 1) {
			fVec114[l452] = 0.0;
		}
		for (int l453 = 0; l453 < 2; l453 = l453 + 1) {
			fRec340[l453] = 0.0;
		}
		for (int l454 = 0; l454 < 2097152; l454 = l454 + 1) {
			fRec319[l454] = 0.0;
		}
		for (int l455 = 0; l455 < 3; l455 = l455 + 1) {
			fRec318[l455] = 0.0;
		}
	}
	
	virtual void init(int sample_rate) {
		classInit(sample_rate);
		instanceInit(sample_rate);
	}
	
	virtual void instanceInit(int sample_rate) {
		instanceConstants(sample_rate);
		instanceResetUserInterface();
		instanceClear();
	}
	
	virtual bbdmi_multi_granulator14* clone() {
		return new bbdmi_multi_granulator14();
	}
	
	virtual int getSampleRate() {
		return fSampleRate;
	}
	
	virtual void buildUserInterface(UI* ui_interface) {
		ui_interface->openHorizontalBox("Multi Granulator");
		ui_interface->openHorizontalBox("granulator");
		ui_interface->openVerticalBox("Grain");
		ui_interface->openHorizontalBox("GrainEnvelope");
		ui_interface->declare(&fVslider1, "01", "");
		ui_interface->addVerticalSlider("indexdistr", &fVslider1, FAUSTFLOAT(0.0), FAUSTFLOAT(0.0), FAUSTFLOAT(21.0), FAUSTFLOAT(1.0));
		ui_interface->declare(&fVslider10, "02", "");
		ui_interface->addVerticalSlider("grainenvmorph", &fVslider10, FAUSTFLOAT(0.5), FAUSTFLOAT(0.0), FAUSTFLOAT(1.0), FAUSTFLOAT(0.01));
		ui_interface->declare(&fVslider2, "03", "");
		ui_interface->declare(&fVslider2, "unit", "msec");
		ui_interface->addVerticalSlider("grainsize", &fVslider2, FAUSTFLOAT(22.0), FAUSTFLOAT(1.0), FAUSTFLOAT(2e+03), FAUSTFLOAT(1.0));
		ui_interface->declare(&fVslider4, "04", "");
		ui_interface->declare(&fVslider4, "unit", "msec");
		ui_interface->addVerticalSlider("grainoffset", &fVslider4, FAUSTFLOAT(0.0), FAUSTFLOAT(0.0), FAUSTFLOAT(1e+03), FAUSTFLOAT(1.0));
		ui_interface->declare(&fVslider3, "05", "");
		ui_interface->declare(&fVslider3, "unit", "msec");
		ui_interface->addVerticalSlider("spacing", &fVslider3, FAUSTFLOAT(25.0), FAUSTFLOAT(1.0), FAUSTFLOAT(1e+03), FAUSTFLOAT(1.0));
		ui_interface->declare(&fVslider11, "06", "");
		ui_interface->addVerticalSlider("modfactor", &fVslider11, FAUSTFLOAT(0.0), FAUSTFLOAT(0.0), FAUSTFLOAT(1.0), FAUSTFLOAT(0.001));
		ui_interface->declare(&fVslider14, "07", "");
		ui_interface->declare(&fVslider14, "unit", "hz");
		ui_interface->addVerticalSlider("modfreq", &fVslider14, FAUSTFLOAT(4e+03), FAUSTFLOAT(1.0), FAUSTFLOAT(1.5e+04), FAUSTFLOAT(1.0));
		ui_interface->declare(&fVslider13, "08", "");
		ui_interface->addVerticalSlider("modmorph", &fVslider13, FAUSTFLOAT(0.0), FAUSTFLOAT(0.0), FAUSTFLOAT(3.0), FAUSTFLOAT(0.001));
		ui_interface->declare(&fVslider12, "09", "");
		ui_interface->addVerticalSlider("modfreqmod", &fVslider12, FAUSTFLOAT(0.5), FAUSTFLOAT(0.0), FAUSTFLOAT(1.0), FAUSTFLOAT(0.001));
		ui_interface->declare(&fVslider15, "10", "");
		ui_interface->declare(&fVslider15, "unit", "");
		ui_interface->addVerticalSlider("transpgrain", &fVslider15, FAUSTFLOAT(0.0), FAUSTFLOAT(-48.0), FAUSTFLOAT(48.0), FAUSTFLOAT(0.01));
		ui_interface->declare(&fVslider5, "11", "");
		ui_interface->declare(&fVslider5, "unit", "%");
		ui_interface->addVerticalSlider("variability", &fVslider5, FAUSTFLOAT(0.7), FAUSTFLOAT(0.0), FAUSTFLOAT(1.0), FAUSTFLOAT(0.01));
		ui_interface->closeBox();
		ui_interface->declare(&fHslider0, "12", "");
		ui_interface->declare(&fHslider0, "unit", "msec");
		ui_interface->addHorizontalSlider("maxdelay", &fHslider0, FAUSTFLOAT(2e+03), FAUSTFLOAT(1.0), FAUSTFLOAT(1e+04), FAUSTFLOAT(1.0));
		ui_interface->closeBox();
		ui_interface->openHorizontalBox("loop");
		ui_interface->declare(&fVslider9, "1", "");
		ui_interface->addVerticalSlider("gain", &fVslider9, FAUSTFLOAT(1.0), FAUSTFLOAT(0.0), FAUSTFLOAT(1.0), FAUSTFLOAT(0.001));
		ui_interface->declare(&fVslider0, "2", "");
		ui_interface->declare(&fVslider0, "unit", "%");
		ui_interface->addVerticalSlider("feedback", &fVslider0, FAUSTFLOAT(0.5), FAUSTFLOAT(0.0), FAUSTFLOAT(1.0), FAUSTFLOAT(0.01));
		ui_interface->declare(&fVslider7, "3", "");
		ui_interface->declare(&fVslider7, "unit", "");
		ui_interface->addVerticalSlider("hpffreq", &fVslider7, FAUSTFLOAT(2e+01), FAUSTFLOAT(2e+01), FAUSTFLOAT(2e+04), FAUSTFLOAT(1.0));
		ui_interface->declare(&fVslider8, "3", "");
		ui_interface->declare(&fVslider8, "unit", "");
		ui_interface->addVerticalSlider("lpffreq", &fVslider8, FAUSTFLOAT(2e+04), FAUSTFLOAT(2e+01), FAUSTFLOAT(2e+04), FAUSTFLOAT(1.0));
		ui_interface->declare(&fVslider6, "3", "");
		ui_interface->declare(&fVslider6, "unit", "");
		ui_interface->addVerticalSlider("transpout", &fVslider6, FAUSTFLOAT(0.0), FAUSTFLOAT(-48.0), FAUSTFLOAT(48.0), FAUSTFLOAT(0.01));
		ui_interface->closeBox();
		ui_interface->closeBox();
		ui_interface->closeBox();
	}
	
	virtual void compute(int count, FAUSTFLOAT** RESTRICT inputs, FAUSTFLOAT** RESTRICT outputs) {
		FAUSTFLOAT* input0 = inputs[0];
		FAUSTFLOAT* input1 = inputs[1];
		FAUSTFLOAT* input2 = inputs[2];
		FAUSTFLOAT* input3 = inputs[3];
		FAUSTFLOAT* input4 = inputs[4];
		FAUSTFLOAT* input5 = inputs[5];
		FAUSTFLOAT* input6 = inputs[6];
		FAUSTFLOAT* input7 = inputs[7];
		FAUSTFLOAT* input8 = inputs[8];
		FAUSTFLOAT* input9 = inputs[9];
		FAUSTFLOAT* input10 = inputs[10];
		FAUSTFLOAT* input11 = inputs[11];
		FAUSTFLOAT* input12 = inputs[12];
		FAUSTFLOAT* input13 = inputs[13];
		FAUSTFLOAT* output0 = outputs[0];
		FAUSTFLOAT* output1 = outputs[1];
		FAUSTFLOAT* output2 = outputs[2];
		FAUSTFLOAT* output3 = outputs[3];
		FAUSTFLOAT* output4 = outputs[4];
		FAUSTFLOAT* output5 = outputs[5];
		FAUSTFLOAT* output6 = outputs[6];
		FAUSTFLOAT* output7 = outputs[7];
		FAUSTFLOAT* output8 = outputs[8];
		FAUSTFLOAT* output9 = outputs[9];
		FAUSTFLOAT* output10 = outputs[10];
		FAUSTFLOAT* output11 = outputs[11];
		FAUSTFLOAT* output12 = outputs[12];
		FAUSTFLOAT* output13 = outputs[13];
		double fSlow0 = fConst1 * double(fVslider0);
		double fSlow1 = double(fVslider1);
		int iSlow2 = fSlow1 == 0.0;
		double fSlow3 = double(iSlow2);
		int iSlow4 = fSlow1 == 1.0;
		double fSlow5 = double(iSlow4);
		double fSlow6 = double(fSlow1 == 2.0);
		double fSlow7 = double(fSlow1 == 3.0);
		double fSlow8 = double(fSlow1 == 4.0);
		double fSlow9 = double(fSlow1 == 5.0);
		double fSlow10 = double(fSlow1 == 6.0);
		double fSlow11 = double(fSlow1 == 7.0);
		double fSlow12 = double(fSlow1 == 8.0);
		double fSlow13 = double(fSlow1 == 9.0);
		double fSlow14 = double(fSlow1 == 1e+01);
		double fSlow15 = double(fSlow1 == 11.0);
		double fSlow16 = double(fSlow1 == 12.0);
		double fSlow17 = double(fSlow1 == 13.0);
		double fSlow18 = double(fSlow1 == 14.0);
		double fSlow19 = double(fSlow1 == 15.0);
		double fSlow20 = double(fSlow1 == 16.0);
		double fSlow21 = double(fSlow1 == 17.0);
		double fSlow22 = double(fSlow1 == 18.0);
		double fSlow23 = double(fSlow1 == 19.0);
		double fSlow24 = double(fSlow1 == 2e+01);
		double fSlow25 = double(fSlow1 == 21.0);
		double fSlow26 = 0.07142857142857142 * fSlow3 + 0.00510204081632653 * fSlow5 + 0.11196447610330786 * fSlow6 + 0.09953567355091437 * fSlow7 + 0.2672612419124244 * fSlow8 + 0.006287790106757396 * fSlow9 + 0.01253604390908819 * fSlow10 + 0.1377551020408163 * fSlow11 + 0.01020408163265306 * fSlow12 + 0.0003644314868804664 * fSlow13 + 0.19934402332361512 * fSlow14 + 0.0014577259475218656 * fSlow15 + 2.6030820491461885e-05 * fSlow16 + 0.2565337359433568 * fSlow17 + 0.00020824656393169508 * fSlow18 + 1.859344320818706e-06 * fSlow19 + 0.30963846909025994 * fSlow20 + 2.9749509133099296e-05 * fSlow21 + 0.001602217492202417 * fSlow22 + 0.3904931728977623 * fSlow23 + 0.002554282587932777 * fSlow24 + 0.3711537444790451 * fSlow25;
		double fSlow27 = fSlow26 + 1.0;
		double fSlow28 = fConst1 * double(fVslider2);
		double fSlow29 = fConst1 * double(fVslider3);
		double fSlow30 = fConst1 * double(fVslider4);
		double fSlow31 = fConst1 * double(fVslider5);
		double fSlow32 = fConst1 * double(fHslider0);
		double fSlow33 = fConst1 * double(fVslider6);
		double fSlow34 = 0.3 * fSlow26 + 0.7;
		double fSlow35 = fConst1 * double(fVslider7);
		double fSlow36 = fConst1 * double(fVslider8);
		double fSlow37 = fConst1 * double(fVslider9);
		double fSlow38 = fConst1 * double(fVslider10);
		double fSlow39 = fConst1 * double(fVslider11);
		double fSlow40 = fConst1 * double(fVslider12);
		double fSlow41 = fConst1 * double(fVslider13);
		double fSlow42 = fConst1 * double(fVslider14);
		double fSlow43 = 0.01 * fSlow34;
		double fSlow44 = fConst1 * double(fVslider15);
		double fSlow45 = 0.14285714285714285 * fSlow3 + 0.02040816326530612 * fSlow5 + 0.2225209339563144 * fSlow6 + 0.19264507794239583 * fSlow7 + 0.3779644730092272 * fSlow8 + 0.02507208781817638 * fSlow9 + 0.04951556604879043 * fSlow10 + 0.26530612244897944 * fSlow11 + 0.04081632653061224 * fSlow12 + 0.0029154518950437313 * fSlow13 + 0.3702623906705538 * fSlow14 + 0.011661807580174925 * fSlow15 + 0.00041649312786339016 * fSlow16 + 0.4602249062890461 * fSlow17 + 0.0033319450229071213 * fSlow18 + 5.949901826619859e-05 * fSlow19 + 0.5373356339620394 * fSlow20 + 0.0009519842922591775 * fSlow21 + 0.002628711313735071 * fSlow22 + 0.6285014277157629 * fSlow23 + 0.010256681389212985 * fSlow24 + 0.5150787536377126 * fSlow25;
		double fSlow46 = fSlow45 + 1.0;
		double fSlow47 = 0.3 * fSlow45 + 0.7;
		double fSlow48 = 0.01 * fSlow47;
		double fSlow49 = 0.21428571428571427 * fSlow3 + 0.04591836734693877 * fSlow5 + 0.33027906195516704 * fSlow6 + 0.2801079191927352 * fSlow7 + 0.4629100498862757 * fSlow8 + 0.056116669691632426 * fSlow9 + 0.1090842587659851 * fSlow10 + 0.38265306122448983 * fSlow11 + 0.09183673469387754 * fSlow12 + 0.009839650145772594 * fSlow13 + 0.5149416909620992 * fSlow14 + 0.039358600583090375 * fSlow15 + 0.0021084964598084128 * fSlow16 + 0.6188827571845065 * fSlow17 + 0.016867971678467302 * fSlow18 + 0.00045182066995894555 * fSlow19 + 0.7005507377878265 * fSlow20 + 0.007229130719343129 * fSlow21 + 0.004312849662788328 * fSlow22 + 0.7735690839340233 * fSlow23 + 0.023228976344475516 * fSlow24 + 0.6185895741317419 * fSlow25;
		double fSlow50 = fSlow49 + 1.0;
		double fSlow51 = 0.3 * fSlow49 + 0.7;
		double fSlow52 = 0.01 * fSlow51;
		double fSlow53 = 0.2857142857142857 * fSlow3 + 0.08163265306122448 * fSlow5 + 0.4338837391175581 * fSlow6 + 0.3625700793847081 * fSlow7 + 0.5345224838248488 * fSlow8 + 0.09903113209758085 * fSlow9 + 0.1882550990706332 * fSlow10 + 0.4897959183673469 * fSlow11 + 0.16326530612244897 * fSlow12 + 0.02332361516034985 * fSlow13 + 0.6355685131195334 * fSlow14 + 0.0932944606413994 * fSlow15 + 0.006663890045814243 * fSlow16 + 0.7396917950853811 * fSlow17 + 0.05331112036651394 * fSlow18 + 0.001903968584518355 * fSlow19 + 0.8140655679181293 * fSlow20 + 0.03046349735229368 * fSlow21 + 0.007075966127061773 * fSlow22 + 0.8619888107907735 * fSlow23 + 0.041685152500090084 * fSlow24 + 0.6998542122237652 * fSlow25;
		double fSlow54 = 0.3 * fSlow53 + 0.7;
		double fSlow55 = fSlow53 + 1.0;
		double fSlow56 = 0.01 * fSlow54;
		double fSlow57 = 0.35714285714285715 * fSlow3 + 0.12755102040816327 * fSlow5 + 0.5320320765153366 * fSlow6 + 0.4405725913859815 * fSlow7 + 0.5976143046671968 * fSlow8 + 0.15327580077171576 * fSlow9 + 0.2830581304412209 * fSlow10 + 0.5867346938775511 * fSlow11 + 0.25510204081632654 * fSlow12 + 0.04555393586005831 * fSlow13 + 0.7343294460641401 * fSlow14 + 0.18221574344023325 * fSlow15 + 0.016269262807163683 * fSlow16 + 0.8292117867555187 * fSlow17 + 0.13015410245730946 * fSlow18 + 0.005810451002558458 * fSlow19 + 0.8902075771999762 * fSlow20 + 0.09296721604093533 * fSlow21 + 0.011609330383882408 * fSlow22 + 0.9158812379604777 * fSlow23 + 0.06595022638414139 * fSlow24 + 0.7659860924831149 * fSlow25;
		double fSlow58 = 0.3 * fSlow57 + 0.7;
		double fSlow59 = fSlow57 + 1.0;
		double fSlow60 = 0.01 * fSlow58;
		double fSlow61 = 0.42857142857142855 * fSlow3 + 0.18367346938775508 * fSlow5 + 0.6234898018587335 * fSlow6 + 0.5145731728297583 * fSlow7 + 0.6546536707079771 * fSlow8 + 0.2181685175319702 * fSlow9 + 0.3887395330218428 * fSlow10 + 0.6734693877551021 * fSlow11 + 0.36734693877551017 * fSlow12 + 0.07871720116618075 * fSlow13 + 0.8134110787172012 * fSlow14 + 0.314868804664723 * fSlow15 + 0.033735943356934604 * fSlow16 + 0.8933777592669722 * fSlow17 + 0.26988754685547683 * fSlow18 + 0.014458261438686257 * fSlow19 + 0.9390730052954126 * fSlow20 + 0.23133218301898012 * fSlow21 + 0.019047088346944928 * fSlow22 + 0.9487290402495226 * fSlow23 + 0.09649209709474871 * fSlow24 + 0.8206518066482898 * fSlow25;
		double fSlow62 = fSlow61 + 1.0;
		double fSlow63 = 0.3 * fSlow61 + 0.7;
		double fSlow64 = 0.01 * fSlow63;
		double fSlow65 = 0.96875 * (fSlow20 + fSlow23) + 0.03125 * (fSlow19 + fSlow22) + 0.5 * (fSlow21 + fSlow18 + fSlow15 + fSlow12 + fSlow3 + fSlow10) + 0.2928932188134524 * fSlow9 + 0.7071067811865476 * fSlow8 + 0.5849625007211562 * fSlow7 + 0.25 * fSlow5 + 0.7071067811865475 * fSlow6 + 0.75 * fSlow11 + 0.125 * fSlow13 + 0.875 * fSlow14 + 0.0625 * fSlow16 + 0.9375 * fSlow17 + 0.1339745962155614 * fSlow24 + 0.8660254037844386 * fSlow25;
		double fSlow66 = fSlow65 + 1.0;
		double fSlow67 = 0.3 * fSlow65 + 0.7;
		double fSlow68 = 0.01 * fSlow67;
		double fSlow69 = 0.5714285714285714 * fSlow3 + 0.32653061224489793 * fSlow5 + 0.7818314824680298 * fSlow6 + 0.652076696579693 * fSlow7 + 0.7559289460184544 * fSlow8 + 0.3765101981412664 * fSlow9 + 0.6112604669781572 * fSlow10 + 0.8163265306122449 * fSlow11 + 0.6326530612244897 * fSlow12 + 0.1865889212827988 * fSlow13 + 0.9212827988338192 * fSlow14 + 0.6851311953352769 * fSlow15 + 0.10662224073302788 * fSlow16 + 0.9662640566430654 * fSlow17 + 0.7301124531445231 * fSlow18 + 0.06092699470458736 * fSlow19 + 0.9855417385613138 * fSlow20 + 0.7686678169810197 * fSlow21 + 0.05127095975047737 * fSlow22 + 0.9809529116530551 * fSlow23 + 0.17934819335171015 * fSlow24 + 0.9035079029052513 * fSlow25;
		double fSlow70 = 0.3 * fSlow69 + 0.7;
		double fSlow71 = fSlow69 + 1.0;
		double fSlow72 = 0.01 * fSlow70;
		double fSlow73 = 0.6428571428571429 * fSlow3 + 0.41326530612244905 * fSlow5 + 0.8467241992282841 * fSlow6 + 0.7162070339994087 * fSlow7 + 0.8017837257372732 * fSlow8 + 0.46796792348466343 * fSlow9 + 0.716941869558779 * fSlow10 + 0.8724489795918368 * fSlow11 + 0.7448979591836735 * fSlow12 + 0.26567055393586014 * fSlow13 + 0.9544460641399417 * fSlow14 + 0.8177842565597668 * fSlow15 + 0.1707882132444815 * fSlow16 + 0.9837307371928363 * fSlow17 + 0.8698458975426906 * fSlow18 + 0.10979242280002384 * fSlow19 + 0.9941895489974415 * fSlow20 + 0.9070327839590647 * fSlow21 + 0.08411876203952227 * fSlow22 + 0.9883906696161175 * fSlow23 + 0.23401390751688522 * fSlow24 + 0.9340497736158586 * fSlow25;
		double fSlow74 = 0.3 * fSlow73 + 0.7;
		double fSlow75 = fSlow73 + 1.0;
		double fSlow76 = 0.01 * fSlow74;
		double fSlow77 = 0.7142857142857143 * fSlow3 + 0.5102040816326531 * fSlow5 + 0.9009688679024191 * fSlow6 + 0.7776075786635521 * fSlow7 + 0.8451542547285166 * fSlow8 + 0.5661162608824418 * fSlow9 + 0.8117449009293667 * fSlow10 + 0.9183673469387755 * fSlow11 + 0.8367346938775511 * fSlow12 + 0.3644314868804665 * fSlow13 + 0.9766763848396501 * fSlow14 + 0.9067055393586005 * fSlow15 + 0.2603082049146189 * fSlow16 + 0.9933361099541858 * fSlow17 + 0.946688879633486 * fSlow18 + 0.18593443208187066 * fSlow19 + 0.9980960314154816 * fSlow20 + 0.9695365026477063 * fSlow21 + 0.1380111892092266 * fSlow22 + 0.9929240338729383 * fSlow23 + 0.3001457877762348 * fSlow24 + 0.9583148474999099 * fSlow25;
		double fSlow78 = fSlow77 + 1.0;
		double fSlow79 = 0.3 * fSlow77 + 0.7;
		double fSlow80 = 0.01 * fSlow79;
		double fSlow81 = 0.7857142857142857 * fSlow3 + 0.6173469387755102 * fSlow5 + 0.9438833303083675 * fSlow6 + 0.8365012677171204 * fSlow7 + 0.8864052604279183 * fSlow8 + 0.6697209380448327 * fSlow9 + 0.8909157412340147 * fSlow10 + 0.9540816326530612 * fSlow11 + 0.9081632653061225 * fSlow12 + 0.48505830903790087 * fSlow13 + 0.9901603498542274 * fSlow14 + 0.9606413994169096 * fSlow15 + 0.38111724281549353 * fSlow16 + 0.9978915035401916 * fSlow17 + 0.9831320283215327 * fSlow18 + 0.2994492622121735 * fSlow19 + 0.999548179330041 * fSlow20 + 0.9927708692806568 * fSlow21 + 0.2264309160659766 * fSlow22 + 0.9956871503372117 * fSlow23 + 0.38141042586825813 * fSlow24 + 0.9767710236555245 * fSlow25;
		double fSlow82 = fSlow81 + 1.0;
		double fSlow83 = 0.3 * fSlow81 + 0.7;
		double fSlow84 = 0.01 * fSlow83;
		double fSlow85 = 0.8571428571428571 * fSlow3 + 0.7346938775510203 * fSlow5 + 0.9749279121818236 * fSlow6 + 0.8930847960834881 * fSlow7 + 0.9258200997725514 * fSlow8 + 0.7774790660436856 * fSlow9 + 0.9504844339512095 * fSlow10 + 0.9795918367346939 * fSlow11 + 0.9591836734693877 * fSlow12 + 0.629737609329446 * fSlow13 + 0.9970845481049563 * fSlow14 + 0.988338192419825 * fSlow15 + 0.5397750937109537 * fSlow16 + 0.9995835068721366 * fSlow17 + 0.9966680549770929 * fSlow18 + 0.46266436603796024 * fSlow19 + 0.9999405009817338 * fSlow20 + 0.9990480157077408 * fSlow21 + 0.37149857228423705 * fSlow22 + 0.9973712886862649 * fSlow23 + 0.4849212463622872 * fSlow24 + 0.989743318610787 * fSlow25;
		double fSlow86 = 0.3 * fSlow85 + 0.7;
		double fSlow87 = fSlow85 + 1.0;
		double fSlow88 = 0.01 * fSlow86;
		double fSlow89 = 0.9285714285714286 * fSlow3 + 0.8622448979591837 * fSlow5 + 0.9937122098932426 * fSlow6 + 0.9475325801058645 * fSlow7 + 0.9636241116594315 * fSlow8 + 0.8880355238966923 * fSlow9 + 0.9874639560909119 * fSlow10 + 0.9948979591836735 * fSlow11 + 0.9897959183673469 * fSlow12 + 0.8006559766763849 * fSlow13 + 0.9996355685131195 * fSlow14 + 0.9985422740524781 * fSlow15 + 0.7434662640566432 * fSlow16 + 0.9999739691795085 * fSlow17 + 0.9997917534360683 * fSlow18 + 0.6903615309097401 * fSlow19 + 0.9999981406556792 * fSlow20 + 0.9999702504908669 * fSlow21 + 0.609506827102238 * fSlow22 + 0.9983977825077975 * fSlow23 + 0.6288462555209549 * fSlow24 + 0.9974457174120672 * fSlow25;
		double fSlow90 = 0.3 * fSlow89 + 0.7;
		double fSlow91 = fSlow89 + 1.0;
		double fSlow92 = 0.01 * fSlow90;
		double fSlow93 = double(iSlow2 + iSlow4) + fSlow25 + fSlow24 + fSlow23 + fSlow22 + fSlow21 + fSlow20 + fSlow19 + fSlow18 + fSlow17 + fSlow16 + fSlow15 + fSlow14 + fSlow13 + fSlow12 + fSlow11 + fSlow10 + fSlow9 + fSlow8 + fSlow6 + fSlow7;
		double fSlow94 = 0.3 * fSlow93 + 0.7;
		double fSlow95 = fSlow93 + 1.0;
		double fSlow96 = 0.01 * fSlow94;
		for (int i0 = 0; i0 < count; i0 = i0 + 1) {
			iVec0[0] = 1;
			fRec2[0] = fSlow0 + fConst2 * fRec2[1];
			fRec11[0] = fSlow28 + fConst2 * fRec11[1];
			fRec12[0] = fSlow29 + fConst2 * fRec12[1];
			iRec10[0] = (iVec0[1] + iRec10[1]) % int(fConst4 * (fRec11[0] + fRec12[0]));
			int iTemp0 = iRec10[0] < 1;
			fRec14[0] = fSlow30 + fConst2 * fRec14[1];
			fRec15[0] = fSlow31 + fConst2 * fRec15[1];
			double fTemp1 = fRec14[0] * fRec15[0];
			iRec16[0] = 1410065407 * iRec16[1] + 90000;
			double fTemp2 = 4.656612875245797e-10 * double(iRec16[0]) + 1.0;
			iRec13[0] = ((iTemp0) ? int(fRec11[0] + 0.5 * fTemp1 * fTemp2) : iRec13[1]);
			double fTemp3 = double(2 * iRec13[0]);
			double fTemp4 = 0.5 * fRec15[0] * fTemp2;
			fRec9[0] = ((iTemp0) ? fTemp3 + fRec12[0] * (2.0 - fTemp4) : fRec9[1]);
			iRec8[0] = (iVec0[1] + iRec8[1]) % int(fConst4 * fRec9[0]);
			double fTemp5 = double(iRec13[0]);
			int iTemp6 = iRec8[0] < int(fConst4 * fTemp5);
			iVec1[0] = iTemp6;
			double fTemp7 = double(iTemp6);
			int iTemp8 = (fTemp7 > 0.001) * (double(iVec1[1]) <= 0.001);
			iVec2[IOTA0 & 2097151] = iTemp8;
			fRec17[0] = fSlow32 + fConst2 * fRec17[1];
			iRec7[0] = ((iTemp8) ? int(fConst4 * fRec17[0] * (1.0 - fTemp4)) : iRec7[1]);
			double fTemp9 = fSlow27 * double(iRec7[0]);
			double fTemp10 = ((fRec3[1] != 0.0) ? (((fRec4[1] > 0.0) & (fRec4[1] < 1.0)) ? fRec3[1] : 0.0) : (((fRec4[1] == 0.0) & (fTemp9 != fRec5[1])) ? 0.0009765625 : (((fRec4[1] == 1.0) & (fTemp9 != fRec6[1])) ? -0.0009765625 : 0.0)));
			fRec3[0] = fTemp10;
			fRec4[0] = std::max<double>(0.0, std::min<double>(1.0, fRec4[1] + fTemp10));
			fRec5[0] = (((fRec4[1] >= 1.0) & (fRec6[1] != fTemp9)) ? fTemp9 : fRec5[1]);
			fRec6[0] = (((fRec4[1] <= 0.0) & (fRec5[1] != fTemp9)) ? fTemp9 : fRec6[1]);
			int iTemp11 = int(std::min<double>(fConst3, std::max<double>(0.0, fRec5[0])));
			double fTemp12 = fRec1[(IOTA0 - (iTemp11 + 1)) & 2097151];
			int iTemp13 = int(std::min<double>(fConst3, std::max<double>(0.0, fRec6[0])));
			double fTemp14 = fRec2[0] * (fTemp12 + fRec4[0] * (fRec1[(IOTA0 - (iTemp13 + 1)) & 2097151] - fTemp12));
			fVec3[IOTA0 & 131071] = fTemp14;
			fRec19[0] = fSlow33 + fConst2 * fRec19[1];
			fRec18[0] = std::fmod(fRec18[1] + (10001.0 - std::pow(2.0, 0.08333333333333333 * std::min<double>(fSlow27 * fRec19[0], 48.0))), 1e+04);
			int iTemp15 = int(fRec18[0]);
			double fTemp16 = std::floor(fRec18[0]);
			double fTemp17 = std::min<double>(0.0001 * fRec18[0], 1.0);
			double fTemp18 = fRec18[0] + 1e+04;
			int iTemp19 = int(fTemp18);
			double fTemp20 = std::floor(fTemp18);
			fRec22[0] = fSlow35 + fConst2 * fRec22[1];
			double fTemp21 = std::tan(fConst5 * std::min<double>(fSlow27 * fRec22[0], 2e+04));
			double fTemp22 = 1.0 / fTemp21;
			fRec24[0] = fSlow36 + fConst2 * fRec24[1];
			double fTemp23 = std::tan(fConst5 * std::min<double>(fSlow27 * fRec24[0], 2e+04));
			double fTemp24 = 1.0 / fTemp23;
			double fTemp25 = (fTemp24 + 1.414213562373095) / fTemp23 + 1.0;
			fRec23[0] = double(input0[i0]) - (fRec23[2] * ((fTemp24 + -1.414213562373095) / fTemp23 + 1.0) + 2.0 * fRec23[1] * (1.0 - 1.0 / bbdmi_multi_granulator14_faustpower2_f(fTemp23))) / fTemp25;
			double fTemp26 = (fRec23[2] + fRec23[0] + 2.0 * fRec23[1]) / fTemp25;
			fVec4[0] = fTemp26;
			fRec21[0] = -((fRec21[1] * (1.0 - fTemp22) - (fTemp26 - fVec4[1]) / fTemp21) / (fTemp22 + 1.0));
			double fTemp27 = fRec21[0] + 0.6 * fRec2[0] * fRec20[1];
			fVec5[IOTA0 & 2097151] = fTemp27;
			double fTemp28 = fVec5[(IOTA0 - iTemp11) & 2097151];
			fRec20[0] = fTemp28 + fRec4[0] * (fVec5[(IOTA0 - iTemp13) & 2097151] - fTemp28);
			fRec25[0] = fSlow37 + fConst2 * fRec25[1];
			double fTemp29 = 0.0005 * fTemp5 * (1.0 - 0.5 * double(1 - iTemp6));
			int iTemp30 = std::fabs(fTemp29) < 2.220446049250313e-16;
			double fTemp31 = ((iTemp30) ? 0.0 : std::exp(-(fConst6 / ((iTemp30) ? 1.0 : fTemp29))));
			fRec26[0] = fTemp7 * (1.0 - fTemp31) + fTemp31 * fRec26[1];
			fRec27[0] = fSlow38 + fConst2 * fRec27[1];
			double fTemp32 = 1.0 - fRec27[0];
			int iTemp33 = iVec2[(IOTA0 - int(std::min<double>(fConst3, std::max<double>(0.0, fConst4 * fTemp3 + -1.0)))) & 2097151];
			iVec6[0] = iTemp33;
			iRec30[0] = (iRec30[1] >= 1) * (((iTemp33 - iVec6[1]) == 1) == 0) + ((iTemp8 - iVec2[(IOTA0 - 1) & 2097151]) == 1);
			double fTemp34 = double(iRec13[0] * iRec30[0]);
			int iTemp35 = ((iRec30[0] != iRec30[1]) ? int(fConst7 * fTemp34) : iRec28[1] + -1);
			iRec28[0] = iTemp35;
			double fTemp36 = double(iRec30[0]);
			fRec29[0] = ((iTemp35 > 0) ? fRec29[1] + (fTemp36 - fRec29[1]) / double(iTemp35) : fTemp36);
			fRec31[0] = fSlow39 + fConst2 * fRec31[1];
			double fTemp37 = 1.0 - fRec31[0];
			double fTemp38 = 0.0005 * fTemp34;
			int iTemp39 = std::fabs(fTemp38) < 2.220446049250313e-16;
			double fTemp40 = ((iTemp39) ? 0.0 : std::exp(-(fConst6 / ((iTemp39) ? 1.0 : fTemp38))));
			fRec32[0] = fTemp36 * (1.0 - fTemp40) + fTemp40 * fRec32[1];
			fRec33[0] = fSlow40 + fConst2 * fRec33[1];
			double fTemp41 = 1.0 - fRec33[0];
			double fTemp42 = fRec32[0] * fTemp41 + fRec33[0] * bbdmi_multi_granulator14_faustpower3_f(fRec29[0]);
			fRec34[0] = fSlow41 + fConst2 * fRec34[1];
			double fTemp43 = 1.0 - std::max<double>(fRec34[0], 0.0);
			double fTemp44 = ((1 - iVec0[1]) ? 0.0 : fRec36[1]);
			fRec36[0] = fTemp44 - std::floor(fTemp44);
			int iTemp45 = int(65536.0 * fRec36[0]);
			double fTemp46 = ftbl0bbdmi_multi_granulator14SIG0[iTemp45];
			fRec38[0] = fSlow42 + fConst2 * fRec38[1];
			fRec37[0] = ((iTemp8) ? fRec38[0] : fRec37[1]);
			double fTemp47 = 6.283185307179586 * fTemp42 * std::min<double>(fSlow27 * fRec37[0], 2e+04) * (fSlow43 * fRec31[0] * fTemp42 + 1.0);
			double fTemp48 = ftbl1bbdmi_multi_granulator14SIG1[iTemp45];
			double fTemp49 = fTemp46 * std::cos(fTemp47) + fTemp48 * std::sin(fTemp47);
			double fTemp50 = fTemp47 + 1.5707963267948966;
			double fTemp51 = fTemp46 * std::cos(fTemp50) + fTemp48 * std::sin(fTemp50);
			fVec9[0] = fTemp51;
			double fTemp52 = fTemp51 - fVec9[1];
			double fTemp53 = double((fTemp49 > 0.0) - (fTemp49 < 0.0));
			double fTemp54 = std::min<double>(std::max<double>(fRec34[0] + -1.0, 0.0), 1.0);
			double fTemp55 = 1.0 - fTemp54;
			double fTemp56 = std::min<double>(std::max<double>(fRec34[0] + -2.0, 0.0), 1.0);
			double fTemp57 = 1.0 - fTemp56;
			double fTemp58 = fSlow34 * fRec20[0] * fRec25[0] * (fRec26[0] * fRec27[0] + fTemp32 * std::pow(2.718281828459045, -(5.0 * bbdmi_multi_granulator14_faustpower2_f(2.0 * fRec29[0] + -1.0)))) * (1.0 - 0.5 * fRec31[0] * (fRec31[0] + fTemp37 * fTemp42) * (((fTemp43 * fTemp49 + std::min<double>(0.6366197723675814 * fRec34[0] * ((std::fabs(fTemp52) <= fConst6) ? std::asin(std::max<double>(-1.0, std::min<double>(1.0, 0.5 * (fTemp51 + fVec9[1])))) : (fTemp51 * std::asin(std::max<double>(-1.0, std::min<double>(1.0, fTemp51))) + std::sqrt(std::max<double>(0.0, 1.0 - bbdmi_multi_granulator14_faustpower2_f(fTemp51))) - (fVec9[1] * std::asin(std::max<double>(-1.0, std::min<double>(1.0, fVec9[1]))) + std::sqrt(std::max<double>(0.0, 1.0 - bbdmi_multi_granulator14_faustpower2_f(fVec9[1]))))) / fTemp52) * fTemp53, 1.0)) * fTemp55 + fTemp53 * fTemp54) * fTemp57 + fTemp49 * fTemp56 + 1.0));
			fVec10[IOTA0 & 131071] = fTemp58;
			fRec41[0] = fSlow44 + fConst2 * fRec41[1];
			fRec40[0] = std::fmod(fRec40[1] + (10001.0 - std::pow(2.0, 0.08333333333333333 * std::min<double>(fSlow27 * fRec41[0], 48.0))), 1e+04);
			int iTemp59 = int(fRec40[0]);
			double fTemp60 = std::floor(fRec40[0]);
			double fTemp61 = std::min<double>(0.0001 * fRec40[0], 1.0);
			double fTemp62 = fRec40[0] + 1e+04;
			int iTemp63 = int(fTemp62);
			double fTemp64 = std::floor(fTemp62);
			fRec1[IOTA0 & 2097151] = 0.6 * ((fVec3[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp15))) & 131071] * (fTemp16 + (1.0 - fRec18[0])) + (fRec18[0] - fTemp16) * fVec3[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp15 + 1))) & 131071]) * fTemp17 + (fVec3[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp19))) & 131071] * (fTemp20 + (-9999.0 - fRec18[0])) + (fRec18[0] + (1e+04 - fTemp20)) * fVec3[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp19 + 1))) & 131071]) * (1.0 - fTemp17)) + (fVec10[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp59))) & 131071] * (fTemp60 + (1.0 - fRec40[0])) + (fRec40[0] - fTemp60) * fVec10[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp59 + 1))) & 131071]) * fTemp61 + (fVec10[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp63))) & 131071] * (fTemp64 + (-9999.0 - fRec40[0])) + (fRec40[0] + (1e+04 - fTemp64)) * fVec10[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp63 + 1))) & 131071]) * (1.0 - fTemp61);
			fRec0[0] = fRec1[IOTA0 & 2097151] + 2.6014143622661517e-16 * fRec0[1] - 0.17157287525381 * fRec0[2];
			output0[i0] = FAUSTFLOAT(0.5857864376269049 * fRec0[1] + 0.2928932188134524 * (fRec0[0] + fRec0[2]));
			iRec52[0] = 1410065407 * iRec52[1] + 90001;
			double fTemp65 = 4.656612875245797e-10 * double(iRec52[0]) + 1.0;
			iRec51[0] = ((iTemp0) ? int(fRec11[0] + 0.5 * fTemp1 * fTemp65) : iRec51[1]);
			double fTemp66 = double(2 * iRec51[0]);
			fRec50[0] = ((iTemp0) ? fTemp66 + fRec12[0] * (2.0 - 0.5 * fRec15[0] * fTemp65) : fRec50[1]);
			iRec49[0] = (iVec0[1] + iRec49[1]) % int(fConst4 * fRec50[0]);
			double fTemp67 = double(iRec51[0]);
			int iTemp68 = iRec49[0] < int(fConst4 * fTemp67);
			iVec11[0] = iTemp68;
			double fTemp69 = double(iTemp68);
			int iTemp70 = (fTemp69 > 0.001) * (double(iVec11[1]) <= 0.001);
			iVec12[IOTA0 & 2097151] = iTemp70;
			iRec53[0] = 1410065407 * iRec53[1] + 90020;
			iRec48[0] = ((iTemp70) ? int(fConst4 * fRec17[0] * (1.0 - 0.5 * fRec15[0] * (4.656612875245797e-10 * double(iRec53[0]) + 1.0))) : iRec48[1]);
			double fTemp71 = fSlow46 * double(iRec48[0]);
			double fTemp72 = ((fRec44[1] != 0.0) ? (((fRec45[1] > 0.0) & (fRec45[1] < 1.0)) ? fRec44[1] : 0.0) : (((fRec45[1] == 0.0) & (fTemp71 != fRec46[1])) ? 0.0009765625 : (((fRec45[1] == 1.0) & (fTemp71 != fRec47[1])) ? -0.0009765625 : 0.0)));
			fRec44[0] = fTemp72;
			fRec45[0] = std::max<double>(0.0, std::min<double>(1.0, fRec45[1] + fTemp72));
			fRec46[0] = (((fRec45[1] >= 1.0) & (fRec47[1] != fTemp71)) ? fTemp71 : fRec46[1]);
			fRec47[0] = (((fRec45[1] <= 0.0) & (fRec46[1] != fTemp71)) ? fTemp71 : fRec47[1]);
			int iTemp73 = int(std::min<double>(fConst3, std::max<double>(0.0, fRec46[0])));
			double fTemp74 = fRec43[(IOTA0 - (iTemp73 + 1)) & 2097151];
			int iTemp75 = int(std::min<double>(fConst3, std::max<double>(0.0, fRec47[0])));
			double fTemp76 = fRec2[0] * (fTemp74 + fRec45[0] * (fRec43[(IOTA0 - (iTemp75 + 1)) & 2097151] - fTemp74));
			fVec13[IOTA0 & 131071] = fTemp76;
			fRec54[0] = std::fmod(fRec54[1] + (10001.0 - std::pow(2.0, 0.08333333333333333 * std::min<double>(fSlow46 * fRec19[0], 48.0))), 1e+04);
			int iTemp77 = int(fRec54[0]);
			double fTemp78 = std::floor(fRec54[0]);
			double fTemp79 = std::min<double>(0.0001 * fRec54[0], 1.0);
			double fTemp80 = fRec54[0] + 1e+04;
			int iTemp81 = int(fTemp80);
			double fTemp82 = std::floor(fTemp80);
			double fTemp83 = std::tan(fConst5 * std::min<double>(fSlow46 * fRec22[0], 2e+04));
			double fTemp84 = 1.0 / fTemp83;
			double fTemp85 = std::tan(fConst5 * std::min<double>(fSlow46 * fRec24[0], 2e+04));
			double fTemp86 = 1.0 / fTemp85;
			double fTemp87 = (fTemp86 + 1.414213562373095) / fTemp85 + 1.0;
			fRec57[0] = double(input1[i0]) - (fRec57[2] * ((fTemp86 + -1.414213562373095) / fTemp85 + 1.0) + 2.0 * fRec57[1] * (1.0 - 1.0 / bbdmi_multi_granulator14_faustpower2_f(fTemp85))) / fTemp87;
			double fTemp88 = (fRec57[2] + fRec57[0] + 2.0 * fRec57[1]) / fTemp87;
			fVec14[0] = fTemp88;
			fRec56[0] = -((fRec56[1] * (1.0 - fTemp84) - (fTemp88 - fVec14[1]) / fTemp83) / (fTemp84 + 1.0));
			double fTemp89 = fRec56[0] + 0.6 * fRec2[0] * fRec55[1];
			fVec15[IOTA0 & 2097151] = fTemp89;
			double fTemp90 = fVec15[(IOTA0 - iTemp73) & 2097151];
			fRec55[0] = fTemp90 + fRec45[0] * (fVec15[(IOTA0 - iTemp75) & 2097151] - fTemp90);
			double fTemp91 = 0.0005 * fTemp67 * (1.0 - 0.5 * double(1 - iTemp68));
			int iTemp92 = std::fabs(fTemp91) < 2.220446049250313e-16;
			double fTemp93 = ((iTemp92) ? 0.0 : std::exp(-(fConst6 / ((iTemp92) ? 1.0 : fTemp91))));
			fRec58[0] = fTemp69 * (1.0 - fTemp93) + fTemp93 * fRec58[1];
			int iTemp94 = iVec12[(IOTA0 - int(std::min<double>(fConst3, std::max<double>(0.0, fConst4 * fTemp66 + -1.0)))) & 2097151];
			iVec16[0] = iTemp94;
			iRec61[0] = (iRec61[1] >= 1) * (((iTemp94 - iVec16[1]) == 1) == 0) + ((iTemp70 - iVec12[(IOTA0 - 1) & 2097151]) == 1);
			double fTemp95 = double(iRec51[0] * iRec61[0]);
			int iTemp96 = ((iRec61[0] != iRec61[1]) ? int(fConst7 * fTemp95) : iRec59[1] + -1);
			iRec59[0] = iTemp96;
			double fTemp97 = double(iRec61[0]);
			fRec60[0] = ((iTemp96 > 0) ? fRec60[1] + (fTemp97 - fRec60[1]) / double(iTemp96) : fTemp97);
			double fTemp98 = 0.0005 * fTemp95;
			int iTemp99 = std::fabs(fTemp98) < 2.220446049250313e-16;
			double fTemp100 = ((iTemp99) ? 0.0 : std::exp(-(fConst6 / ((iTemp99) ? 1.0 : fTemp98))));
			fRec62[0] = fTemp97 * (1.0 - fTemp100) + fTemp100 * fRec62[1];
			double fTemp101 = fRec62[0] * fTemp41 + fRec33[0] * bbdmi_multi_granulator14_faustpower3_f(fRec60[0]);
			fRec63[0] = ((iTemp70) ? fRec38[0] : fRec63[1]);
			double fTemp102 = 6.283185307179586 * fTemp101 * std::min<double>(fSlow46 * fRec63[0], 2e+04) * (fSlow48 * fRec31[0] * fTemp101 + 1.0);
			double fTemp103 = fTemp46 * std::cos(fTemp102) + fTemp48 * std::sin(fTemp102);
			double fTemp104 = fTemp102 + 1.5707963267948966;
			double fTemp105 = fTemp46 * std::cos(fTemp104) + fTemp48 * std::sin(fTemp104);
			fVec17[0] = fTemp105;
			double fTemp106 = fTemp105 - fVec17[1];
			double fTemp107 = double((fTemp103 > 0.0) - (fTemp103 < 0.0));
			double fTemp108 = fSlow47 * fRec25[0] * fRec55[0] * (fRec27[0] * fRec58[0] + fTemp32 * std::pow(2.718281828459045, -(5.0 * bbdmi_multi_granulator14_faustpower2_f(2.0 * fRec60[0] + -1.0)))) * (1.0 - 0.5 * fRec31[0] * (fRec31[0] + fTemp37 * fTemp101) * (fTemp57 * (fTemp55 * (fTemp43 * fTemp103 + std::min<double>(0.6366197723675814 * fRec34[0] * ((std::fabs(fTemp106) <= fConst6) ? std::asin(std::max<double>(-1.0, std::min<double>(1.0, 0.5 * (fTemp105 + fVec17[1])))) : (fTemp105 * std::asin(std::max<double>(-1.0, std::min<double>(1.0, fTemp105))) + std::sqrt(std::max<double>(0.0, 1.0 - bbdmi_multi_granulator14_faustpower2_f(fTemp105))) - (fVec17[1] * std::asin(std::max<double>(-1.0, std::min<double>(1.0, fVec17[1]))) + std::sqrt(std::max<double>(0.0, 1.0 - bbdmi_multi_granulator14_faustpower2_f(fVec17[1]))))) / fTemp106) * fTemp107, 1.0)) + fTemp54 * fTemp107) + fTemp56 * fTemp103 + 1.0));
			fVec18[IOTA0 & 131071] = fTemp108;
			fRec64[0] = std::fmod(fRec64[1] + (10001.0 - std::pow(2.0, 0.08333333333333333 * std::min<double>(fSlow46 * fRec41[0], 48.0))), 1e+04);
			int iTemp109 = int(fRec64[0]);
			double fTemp110 = std::floor(fRec64[0]);
			double fTemp111 = std::min<double>(0.0001 * fRec64[0], 1.0);
			double fTemp112 = fRec64[0] + 1e+04;
			int iTemp113 = int(fTemp112);
			double fTemp114 = std::floor(fTemp112);
			fRec43[IOTA0 & 2097151] = 0.6 * ((fVec13[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp77))) & 131071] * (fTemp78 + (1.0 - fRec54[0])) + (fRec54[0] - fTemp78) * fVec13[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp77 + 1))) & 131071]) * fTemp79 + (fVec13[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp81))) & 131071] * (fTemp82 + (-9999.0 - fRec54[0])) + (fRec54[0] + (1e+04 - fTemp82)) * fVec13[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp81 + 1))) & 131071]) * (1.0 - fTemp79)) + (fVec18[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp109))) & 131071] * (fTemp110 + (1.0 - fRec64[0])) + (fRec64[0] - fTemp110) * fVec18[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp109 + 1))) & 131071]) * fTemp111 + (fVec18[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp113))) & 131071] * (fTemp114 + (-9999.0 - fRec64[0])) + (fRec64[0] + (1e+04 - fTemp114)) * fVec18[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp113 + 1))) & 131071]) * (1.0 - fTemp111);
			fRec42[0] = fRec43[IOTA0 & 2097151] + 2.6014143622661517e-16 * fRec42[1] - 0.17157287525381 * fRec42[2];
			output1[i0] = FAUSTFLOAT(0.5857864376269049 * fRec42[1] + 0.2928932188134524 * (fRec42[0] + fRec42[2]));
			iRec75[0] = 1410065407 * iRec75[1] + 90002;
			double fTemp115 = 4.656612875245797e-10 * double(iRec75[0]) + 1.0;
			iRec74[0] = ((iTemp0) ? int(fRec11[0] + 0.5 * fTemp1 * fTemp115) : iRec74[1]);
			double fTemp116 = double(2 * iRec74[0]);
			fRec73[0] = ((iTemp0) ? fTemp116 + fRec12[0] * (2.0 - 0.5 * fRec15[0] * fTemp115) : fRec73[1]);
			iRec72[0] = (iVec0[1] + iRec72[1]) % int(fConst4 * fRec73[0]);
			double fTemp117 = double(iRec74[0]);
			int iTemp118 = iRec72[0] < int(fConst4 * fTemp117);
			iVec19[0] = iTemp118;
			double fTemp119 = double(iTemp118);
			int iTemp120 = (fTemp119 > 0.001) * (double(iVec19[1]) <= 0.001);
			iVec20[IOTA0 & 2097151] = iTemp120;
			iRec76[0] = 1410065407 * iRec76[1] + 90040;
			iRec71[0] = ((iTemp120) ? int(fConst4 * fRec17[0] * (1.0 - 0.5 * fRec15[0] * (4.656612875245797e-10 * double(iRec76[0]) + 1.0))) : iRec71[1]);
			double fTemp121 = fSlow50 * double(iRec71[0]);
			double fTemp122 = ((fRec67[1] != 0.0) ? (((fRec68[1] > 0.0) & (fRec68[1] < 1.0)) ? fRec67[1] : 0.0) : (((fRec68[1] == 0.0) & (fTemp121 != fRec69[1])) ? 0.0009765625 : (((fRec68[1] == 1.0) & (fTemp121 != fRec70[1])) ? -0.0009765625 : 0.0)));
			fRec67[0] = fTemp122;
			fRec68[0] = std::max<double>(0.0, std::min<double>(1.0, fRec68[1] + fTemp122));
			fRec69[0] = (((fRec68[1] >= 1.0) & (fRec70[1] != fTemp121)) ? fTemp121 : fRec69[1]);
			fRec70[0] = (((fRec68[1] <= 0.0) & (fRec69[1] != fTemp121)) ? fTemp121 : fRec70[1]);
			int iTemp123 = int(std::min<double>(fConst3, std::max<double>(0.0, fRec69[0])));
			double fTemp124 = fRec66[(IOTA0 - (iTemp123 + 1)) & 2097151];
			int iTemp125 = int(std::min<double>(fConst3, std::max<double>(0.0, fRec70[0])));
			double fTemp126 = fRec2[0] * (fTemp124 + fRec68[0] * (fRec66[(IOTA0 - (iTemp125 + 1)) & 2097151] - fTemp124));
			fVec21[IOTA0 & 131071] = fTemp126;
			fRec77[0] = std::fmod(fRec77[1] + (10001.0 - std::pow(2.0, 0.08333333333333333 * std::min<double>(fSlow50 * fRec19[0], 48.0))), 1e+04);
			int iTemp127 = int(fRec77[0]);
			double fTemp128 = std::floor(fRec77[0]);
			double fTemp129 = std::min<double>(0.0001 * fRec77[0], 1.0);
			double fTemp130 = fRec77[0] + 1e+04;
			int iTemp131 = int(fTemp130);
			double fTemp132 = std::floor(fTemp130);
			double fTemp133 = std::tan(fConst5 * std::min<double>(fSlow50 * fRec22[0], 2e+04));
			double fTemp134 = 1.0 / fTemp133;
			double fTemp135 = std::tan(fConst5 * std::min<double>(fSlow50 * fRec24[0], 2e+04));
			double fTemp136 = 1.0 / fTemp135;
			double fTemp137 = (fTemp136 + 1.414213562373095) / fTemp135 + 1.0;
			fRec80[0] = double(input2[i0]) - (fRec80[2] * ((fTemp136 + -1.414213562373095) / fTemp135 + 1.0) + 2.0 * fRec80[1] * (1.0 - 1.0 / bbdmi_multi_granulator14_faustpower2_f(fTemp135))) / fTemp137;
			double fTemp138 = (fRec80[2] + fRec80[0] + 2.0 * fRec80[1]) / fTemp137;
			fVec22[0] = fTemp138;
			fRec79[0] = -((fRec79[1] * (1.0 - fTemp134) - (fTemp138 - fVec22[1]) / fTemp133) / (fTemp134 + 1.0));
			double fTemp139 = fRec79[0] + 0.6 * fRec2[0] * fRec78[1];
			fVec23[IOTA0 & 2097151] = fTemp139;
			double fTemp140 = fVec23[(IOTA0 - iTemp123) & 2097151];
			fRec78[0] = fTemp140 + fRec68[0] * (fVec23[(IOTA0 - iTemp125) & 2097151] - fTemp140);
			double fTemp141 = 0.0005 * fTemp117 * (1.0 - 0.5 * double(1 - iTemp118));
			int iTemp142 = std::fabs(fTemp141) < 2.220446049250313e-16;
			double fTemp143 = ((iTemp142) ? 0.0 : std::exp(-(fConst6 / ((iTemp142) ? 1.0 : fTemp141))));
			fRec81[0] = fTemp119 * (1.0 - fTemp143) + fTemp143 * fRec81[1];
			int iTemp144 = iVec20[(IOTA0 - int(std::min<double>(fConst3, std::max<double>(0.0, fConst4 * fTemp116 + -1.0)))) & 2097151];
			iVec24[0] = iTemp144;
			iRec84[0] = (iRec84[1] >= 1) * (((iTemp144 - iVec24[1]) == 1) == 0) + ((iTemp120 - iVec20[(IOTA0 - 1) & 2097151]) == 1);
			double fTemp145 = double(iRec74[0] * iRec84[0]);
			int iTemp146 = ((iRec84[0] != iRec84[1]) ? int(fConst7 * fTemp145) : iRec82[1] + -1);
			iRec82[0] = iTemp146;
			double fTemp147 = double(iRec84[0]);
			fRec83[0] = ((iTemp146 > 0) ? fRec83[1] + (fTemp147 - fRec83[1]) / double(iTemp146) : fTemp147);
			double fTemp148 = 0.0005 * fTemp145;
			int iTemp149 = std::fabs(fTemp148) < 2.220446049250313e-16;
			double fTemp150 = ((iTemp149) ? 0.0 : std::exp(-(fConst6 / ((iTemp149) ? 1.0 : fTemp148))));
			fRec85[0] = fTemp147 * (1.0 - fTemp150) + fTemp150 * fRec85[1];
			double fTemp151 = fRec85[0] * fTemp41 + fRec33[0] * bbdmi_multi_granulator14_faustpower3_f(fRec83[0]);
			fRec86[0] = ((iTemp120) ? fRec38[0] : fRec86[1]);
			double fTemp152 = 6.283185307179586 * fTemp151 * std::min<double>(fSlow50 * fRec86[0], 2e+04) * (fSlow52 * fRec31[0] * fTemp151 + 1.0);
			double fTemp153 = fTemp46 * std::cos(fTemp152) + fTemp48 * std::sin(fTemp152);
			double fTemp154 = fTemp152 + 1.5707963267948966;
			double fTemp155 = fTemp46 * std::cos(fTemp154) + fTemp48 * std::sin(fTemp154);
			fVec25[0] = fTemp155;
			double fTemp156 = fTemp155 - fVec25[1];
			double fTemp157 = double((fTemp153 > 0.0) - (fTemp153 < 0.0));
			double fTemp158 = fSlow51 * fRec25[0] * fRec78[0] * (fRec27[0] * fRec81[0] + fTemp32 * std::pow(2.718281828459045, -(5.0 * bbdmi_multi_granulator14_faustpower2_f(2.0 * fRec83[0] + -1.0)))) * (1.0 - 0.5 * fRec31[0] * (fRec31[0] + fTemp37 * fTemp151) * (fTemp57 * (fTemp55 * (fTemp43 * fTemp153 + std::min<double>(0.6366197723675814 * fRec34[0] * ((std::fabs(fTemp156) <= fConst6) ? std::asin(std::max<double>(-1.0, std::min<double>(1.0, 0.5 * (fTemp155 + fVec25[1])))) : (fTemp155 * std::asin(std::max<double>(-1.0, std::min<double>(1.0, fTemp155))) + std::sqrt(std::max<double>(0.0, 1.0 - bbdmi_multi_granulator14_faustpower2_f(fTemp155))) - (fVec25[1] * std::asin(std::max<double>(-1.0, std::min<double>(1.0, fVec25[1]))) + std::sqrt(std::max<double>(0.0, 1.0 - bbdmi_multi_granulator14_faustpower2_f(fVec25[1]))))) / fTemp156) * fTemp157, 1.0)) + fTemp54 * fTemp157) + fTemp56 * fTemp153 + 1.0));
			fVec26[IOTA0 & 131071] = fTemp158;
			fRec87[0] = std::fmod(fRec87[1] + (10001.0 - std::pow(2.0, 0.08333333333333333 * std::min<double>(fSlow50 * fRec41[0], 48.0))), 1e+04);
			int iTemp159 = int(fRec87[0]);
			double fTemp160 = std::floor(fRec87[0]);
			double fTemp161 = std::min<double>(0.0001 * fRec87[0], 1.0);
			double fTemp162 = fRec87[0] + 1e+04;
			int iTemp163 = int(fTemp162);
			double fTemp164 = std::floor(fTemp162);
			fRec66[IOTA0 & 2097151] = 0.6 * ((fVec21[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp127))) & 131071] * (fTemp128 + (1.0 - fRec77[0])) + (fRec77[0] - fTemp128) * fVec21[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp127 + 1))) & 131071]) * fTemp129 + (fVec21[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp131))) & 131071] * (fTemp132 + (-9999.0 - fRec77[0])) + (fRec77[0] + (1e+04 - fTemp132)) * fVec21[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp131 + 1))) & 131071]) * (1.0 - fTemp129)) + (fVec26[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp159))) & 131071] * (fTemp160 + (1.0 - fRec87[0])) + (fRec87[0] - fTemp160) * fVec26[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp159 + 1))) & 131071]) * fTemp161 + (fVec26[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp163))) & 131071] * (fTemp164 + (-9999.0 - fRec87[0])) + (fRec87[0] + (1e+04 - fTemp164)) * fVec26[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp163 + 1))) & 131071]) * (1.0 - fTemp161);
			fRec65[0] = fRec66[IOTA0 & 2097151] + 2.6014143622661517e-16 * fRec65[1] - 0.17157287525381 * fRec65[2];
			output2[i0] = FAUSTFLOAT(0.5857864376269049 * fRec65[1] + 0.2928932188134524 * (fRec65[0] + fRec65[2]));
			double fTemp165 = std::tan(fConst5 * std::min<double>(fSlow55 * fRec22[0], 2e+04));
			double fTemp166 = 1.0 / fTemp165;
			double fTemp167 = std::tan(fConst5 * std::min<double>(fSlow55 * fRec24[0], 2e+04));
			double fTemp168 = 1.0 / fTemp167;
			double fTemp169 = (fTemp168 + 1.414213562373095) / fTemp167 + 1.0;
			fRec92[0] = double(input3[i0]) - (fRec92[2] * ((fTemp168 + -1.414213562373095) / fTemp167 + 1.0) + 2.0 * fRec92[1] * (1.0 - 1.0 / bbdmi_multi_granulator14_faustpower2_f(fTemp167))) / fTemp169;
			double fTemp170 = (fRec92[2] + fRec92[0] + 2.0 * fRec92[1]) / fTemp169;
			fVec27[0] = fTemp170;
			fRec91[0] = -((fRec91[1] * (1.0 - fTemp166) - (fTemp170 - fVec27[1]) / fTemp165) / (fTemp166 + 1.0));
			double fTemp171 = fRec91[0] + 0.6 * fRec2[0] * fRec90[1];
			fVec28[IOTA0 & 2097151] = fTemp171;
			iRec101[0] = 1410065407 * iRec101[1] + 90003;
			double fTemp172 = 4.656612875245797e-10 * double(iRec101[0]) + 1.0;
			iRec100[0] = ((iTemp0) ? int(fRec11[0] + 0.5 * fTemp1 * fTemp172) : iRec100[1]);
			double fTemp173 = double(2 * iRec100[0]);
			fRec99[0] = ((iTemp0) ? fTemp173 + fRec12[0] * (2.0 - 0.5 * fRec15[0] * fTemp172) : fRec99[1]);
			iRec98[0] = (iVec0[1] + iRec98[1]) % int(fConst4 * fRec99[0]);
			double fTemp174 = double(iRec100[0]);
			int iTemp175 = iRec98[0] < int(fConst4 * fTemp174);
			iVec29[0] = iTemp175;
			double fTemp176 = double(iTemp175);
			int iTemp177 = (fTemp176 > 0.001) * (double(iVec29[1]) <= 0.001);
			iVec30[IOTA0 & 2097151] = iTemp177;
			iRec102[0] = 1410065407 * iRec102[1] + 90060;
			iRec97[0] = ((iTemp177) ? int(fConst4 * fRec17[0] * (1.0 - 0.5 * fRec15[0] * (4.656612875245797e-10 * double(iRec102[0]) + 1.0))) : iRec97[1]);
			double fTemp178 = fSlow55 * double(iRec97[0]);
			double fTemp179 = ((fRec93[1] != 0.0) ? (((fRec94[1] > 0.0) & (fRec94[1] < 1.0)) ? fRec93[1] : 0.0) : (((fRec94[1] == 0.0) & (fTemp178 != fRec95[1])) ? 0.0009765625 : (((fRec94[1] == 1.0) & (fTemp178 != fRec96[1])) ? -0.0009765625 : 0.0)));
			fRec93[0] = fTemp179;
			fRec94[0] = std::max<double>(0.0, std::min<double>(1.0, fRec94[1] + fTemp179));
			fRec95[0] = (((fRec94[1] >= 1.0) & (fRec96[1] != fTemp178)) ? fTemp178 : fRec95[1]);
			fRec96[0] = (((fRec94[1] <= 0.0) & (fRec95[1] != fTemp178)) ? fTemp178 : fRec96[1]);
			int iTemp180 = int(std::min<double>(fConst3, std::max<double>(0.0, fRec95[0])));
			double fTemp181 = fVec28[(IOTA0 - iTemp180) & 2097151];
			int iTemp182 = int(std::min<double>(fConst3, std::max<double>(0.0, fRec96[0])));
			fRec90[0] = fTemp181 + fRec94[0] * (fVec28[(IOTA0 - iTemp182) & 2097151] - fTemp181);
			double fTemp183 = 0.0005 * fTemp174 * (1.0 - 0.5 * double(1 - iTemp175));
			int iTemp184 = std::fabs(fTemp183) < 2.220446049250313e-16;
			double fTemp185 = ((iTemp184) ? 0.0 : std::exp(-(fConst6 / ((iTemp184) ? 1.0 : fTemp183))));
			fRec103[0] = fTemp176 * (1.0 - fTemp185) + fTemp185 * fRec103[1];
			int iTemp186 = iVec30[(IOTA0 - int(std::min<double>(fConst3, std::max<double>(0.0, fConst4 * fTemp173 + -1.0)))) & 2097151];
			iVec31[0] = iTemp186;
			iRec106[0] = (iRec106[1] >= 1) * (((iTemp186 - iVec31[1]) == 1) == 0) + ((iTemp177 - iVec30[(IOTA0 - 1) & 2097151]) == 1);
			double fTemp187 = double(iRec100[0] * iRec106[0]);
			int iTemp188 = ((iRec106[0] != iRec106[1]) ? int(fConst7 * fTemp187) : iRec104[1] + -1);
			iRec104[0] = iTemp188;
			double fTemp189 = double(iRec106[0]);
			fRec105[0] = ((iTemp188 > 0) ? fRec105[1] + (fTemp189 - fRec105[1]) / double(iTemp188) : fTemp189);
			double fTemp190 = 0.0005 * fTemp187;
			int iTemp191 = std::fabs(fTemp190) < 2.220446049250313e-16;
			double fTemp192 = ((iTemp191) ? 0.0 : std::exp(-(fConst6 / ((iTemp191) ? 1.0 : fTemp190))));
			fRec107[0] = fTemp189 * (1.0 - fTemp192) + fTemp192 * fRec107[1];
			double fTemp193 = fRec107[0] * fTemp41 + fRec33[0] * bbdmi_multi_granulator14_faustpower3_f(fRec105[0]);
			fRec108[0] = ((iTemp177) ? fRec38[0] : fRec108[1]);
			double fTemp194 = 6.283185307179586 * fTemp193 * std::min<double>(fSlow55 * fRec108[0], 2e+04) * (fSlow56 * fRec31[0] * fTemp193 + 1.0);
			double fTemp195 = fTemp46 * std::cos(fTemp194) + fTemp48 * std::sin(fTemp194);
			double fTemp196 = fTemp194 + 1.5707963267948966;
			double fTemp197 = fTemp46 * std::cos(fTemp196) + fTemp48 * std::sin(fTemp196);
			fVec32[0] = fTemp197;
			double fTemp198 = fTemp197 - fVec32[1];
			double fTemp199 = double((fTemp195 > 0.0) - (fTemp195 < 0.0));
			double fTemp200 = fSlow54 * fRec25[0] * fRec90[0] * (fRec27[0] * fRec103[0] + fTemp32 * std::pow(2.718281828459045, -(5.0 * bbdmi_multi_granulator14_faustpower2_f(2.0 * fRec105[0] + -1.0)))) * (1.0 - 0.5 * fRec31[0] * (fRec31[0] + fTemp37 * fTemp193) * (fTemp57 * (fTemp55 * (fTemp43 * fTemp195 + std::min<double>(0.6366197723675814 * fRec34[0] * ((std::fabs(fTemp198) <= fConst6) ? std::asin(std::max<double>(-1.0, std::min<double>(1.0, 0.5 * (fTemp197 + fVec32[1])))) : (fTemp197 * std::asin(std::max<double>(-1.0, std::min<double>(1.0, fTemp197))) + std::sqrt(std::max<double>(0.0, 1.0 - bbdmi_multi_granulator14_faustpower2_f(fTemp197))) - (fVec32[1] * std::asin(std::max<double>(-1.0, std::min<double>(1.0, fVec32[1]))) + std::sqrt(std::max<double>(0.0, 1.0 - bbdmi_multi_granulator14_faustpower2_f(fVec32[1]))))) / fTemp198) * fTemp199, 1.0)) + fTemp54 * fTemp199) + fTemp56 * fTemp195 + 1.0));
			fVec33[IOTA0 & 131071] = fTemp200;
			fRec109[0] = std::fmod(fRec109[1] + (10001.0 - std::pow(2.0, 0.08333333333333333 * std::min<double>(fSlow55 * fRec41[0], 48.0))), 1e+04);
			double fTemp201 = fRec109[0] + 1e+04;
			int iTemp202 = int(fTemp201);
			double fTemp203 = std::floor(fTemp201);
			double fTemp204 = std::min<double>(0.0001 * fRec109[0], 1.0);
			double fTemp205 = fRec89[(IOTA0 - (iTemp180 + 1)) & 2097151];
			double fTemp206 = fRec2[0] * (fTemp205 + fRec94[0] * (fRec89[(IOTA0 - (iTemp182 + 1)) & 2097151] - fTemp205));
			fVec34[IOTA0 & 131071] = fTemp206;
			fRec110[0] = std::fmod(fRec110[1] + (10001.0 - std::pow(2.0, 0.08333333333333333 * std::min<double>(fSlow55 * fRec19[0], 48.0))), 1e+04);
			int iTemp207 = int(fRec110[0]);
			double fTemp208 = std::floor(fRec110[0]);
			double fTemp209 = std::min<double>(0.0001 * fRec110[0], 1.0);
			double fTemp210 = fRec110[0] + 1e+04;
			int iTemp211 = int(fTemp210);
			double fTemp212 = std::floor(fTemp210);
			int iTemp213 = int(fRec109[0]);
			double fTemp214 = std::floor(fRec109[0]);
			fRec89[IOTA0 & 2097151] = (fVec33[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp202))) & 131071] * (fTemp203 + (-9999.0 - fRec109[0])) + (fRec109[0] + (1e+04 - fTemp203)) * fVec33[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp202 + 1))) & 131071]) * (1.0 - fTemp204) + 0.6 * ((fVec34[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp207))) & 131071] * (fTemp208 + (1.0 - fRec110[0])) + (fRec110[0] - fTemp208) * fVec34[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp207 + 1))) & 131071]) * fTemp209 + (fVec34[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp211))) & 131071] * (fTemp212 + (-9999.0 - fRec110[0])) + (fRec110[0] + (1e+04 - fTemp212)) * fVec34[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp211 + 1))) & 131071]) * (1.0 - fTemp209)) + (fVec33[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp213))) & 131071] * (fTemp214 + (1.0 - fRec109[0])) + (fRec109[0] - fTemp214) * fVec33[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp213 + 1))) & 131071]) * fTemp204;
			fRec88[0] = fRec89[IOTA0 & 2097151] + 2.6014143622661517e-16 * fRec88[1] - 0.17157287525381 * fRec88[2];
			output3[i0] = FAUSTFLOAT(0.5857864376269049 * fRec88[1] + 0.2928932188134524 * (fRec88[0] + fRec88[2]));
			double fTemp215 = std::tan(fConst5 * std::min<double>(fSlow59 * fRec22[0], 2e+04));
			double fTemp216 = 1.0 / fTemp215;
			double fTemp217 = std::tan(fConst5 * std::min<double>(fSlow59 * fRec24[0], 2e+04));
			double fTemp218 = 1.0 / fTemp217;
			double fTemp219 = (fTemp218 + 1.414213562373095) / fTemp217 + 1.0;
			fRec115[0] = double(input4[i0]) - (fRec115[2] * ((fTemp218 + -1.414213562373095) / fTemp217 + 1.0) + 2.0 * fRec115[1] * (1.0 - 1.0 / bbdmi_multi_granulator14_faustpower2_f(fTemp217))) / fTemp219;
			double fTemp220 = (fRec115[2] + fRec115[0] + 2.0 * fRec115[1]) / fTemp219;
			fVec35[0] = fTemp220;
			fRec114[0] = -((fRec114[1] * (1.0 - fTemp216) - (fTemp220 - fVec35[1]) / fTemp215) / (fTemp216 + 1.0));
			double fTemp221 = fRec114[0] + 0.6 * fRec2[0] * fRec113[1];
			fVec36[IOTA0 & 2097151] = fTemp221;
			iRec124[0] = 1410065407 * iRec124[1] + 90004;
			double fTemp222 = 4.656612875245797e-10 * double(iRec124[0]) + 1.0;
			iRec123[0] = ((iTemp0) ? int(fRec11[0] + 0.5 * fTemp1 * fTemp222) : iRec123[1]);
			double fTemp223 = double(2 * iRec123[0]);
			fRec122[0] = ((iTemp0) ? fTemp223 + fRec12[0] * (2.0 - 0.5 * fRec15[0] * fTemp222) : fRec122[1]);
			iRec121[0] = (iVec0[1] + iRec121[1]) % int(fConst4 * fRec122[0]);
			double fTemp224 = double(iRec123[0]);
			int iTemp225 = iRec121[0] < int(fConst4 * fTemp224);
			iVec37[0] = iTemp225;
			double fTemp226 = double(iTemp225);
			int iTemp227 = (fTemp226 > 0.001) * (double(iVec37[1]) <= 0.001);
			iVec38[IOTA0 & 2097151] = iTemp227;
			iRec125[0] = 1410065407 * iRec125[1] + 90080;
			iRec120[0] = ((iTemp227) ? int(fConst4 * fRec17[0] * (1.0 - 0.5 * fRec15[0] * (4.656612875245797e-10 * double(iRec125[0]) + 1.0))) : iRec120[1]);
			double fTemp228 = fSlow59 * double(iRec120[0]);
			double fTemp229 = ((fRec116[1] != 0.0) ? (((fRec117[1] > 0.0) & (fRec117[1] < 1.0)) ? fRec116[1] : 0.0) : (((fRec117[1] == 0.0) & (fTemp228 != fRec118[1])) ? 0.0009765625 : (((fRec117[1] == 1.0) & (fTemp228 != fRec119[1])) ? -0.0009765625 : 0.0)));
			fRec116[0] = fTemp229;
			fRec117[0] = std::max<double>(0.0, std::min<double>(1.0, fRec117[1] + fTemp229));
			fRec118[0] = (((fRec117[1] >= 1.0) & (fRec119[1] != fTemp228)) ? fTemp228 : fRec118[1]);
			fRec119[0] = (((fRec117[1] <= 0.0) & (fRec118[1] != fTemp228)) ? fTemp228 : fRec119[1]);
			int iTemp230 = int(std::min<double>(fConst3, std::max<double>(0.0, fRec118[0])));
			double fTemp231 = fVec36[(IOTA0 - iTemp230) & 2097151];
			int iTemp232 = int(std::min<double>(fConst3, std::max<double>(0.0, fRec119[0])));
			fRec113[0] = fTemp231 + fRec117[0] * (fVec36[(IOTA0 - iTemp232) & 2097151] - fTemp231);
			double fTemp233 = 0.0005 * fTemp224 * (1.0 - 0.5 * double(1 - iTemp225));
			int iTemp234 = std::fabs(fTemp233) < 2.220446049250313e-16;
			double fTemp235 = ((iTemp234) ? 0.0 : std::exp(-(fConst6 / ((iTemp234) ? 1.0 : fTemp233))));
			fRec126[0] = fTemp226 * (1.0 - fTemp235) + fTemp235 * fRec126[1];
			int iTemp236 = iVec38[(IOTA0 - int(std::min<double>(fConst3, std::max<double>(0.0, fConst4 * fTemp223 + -1.0)))) & 2097151];
			iVec39[0] = iTemp236;
			iRec129[0] = (iRec129[1] >= 1) * (((iTemp236 - iVec39[1]) == 1) == 0) + ((iTemp227 - iVec38[(IOTA0 - 1) & 2097151]) == 1);
			double fTemp237 = double(iRec123[0] * iRec129[0]);
			int iTemp238 = ((iRec129[0] != iRec129[1]) ? int(fConst7 * fTemp237) : iRec127[1] + -1);
			iRec127[0] = iTemp238;
			double fTemp239 = double(iRec129[0]);
			fRec128[0] = ((iTemp238 > 0) ? fRec128[1] + (fTemp239 - fRec128[1]) / double(iTemp238) : fTemp239);
			double fTemp240 = 0.0005 * fTemp237;
			int iTemp241 = std::fabs(fTemp240) < 2.220446049250313e-16;
			double fTemp242 = ((iTemp241) ? 0.0 : std::exp(-(fConst6 / ((iTemp241) ? 1.0 : fTemp240))));
			fRec130[0] = fTemp239 * (1.0 - fTemp242) + fTemp242 * fRec130[1];
			double fTemp243 = fRec130[0] * fTemp41 + fRec33[0] * bbdmi_multi_granulator14_faustpower3_f(fRec128[0]);
			fRec131[0] = ((iTemp227) ? fRec38[0] : fRec131[1]);
			double fTemp244 = 6.283185307179586 * fTemp243 * std::min<double>(fSlow59 * fRec131[0], 2e+04) * (fSlow60 * fRec31[0] * fTemp243 + 1.0);
			double fTemp245 = fTemp46 * std::cos(fTemp244) + fTemp48 * std::sin(fTemp244);
			double fTemp246 = fTemp244 + 1.5707963267948966;
			double fTemp247 = fTemp46 * std::cos(fTemp246) + fTemp48 * std::sin(fTemp246);
			fVec40[0] = fTemp247;
			double fTemp248 = fTemp247 - fVec40[1];
			double fTemp249 = double((fTemp245 > 0.0) - (fTemp245 < 0.0));
			double fTemp250 = fSlow58 * fRec25[0] * fRec113[0] * (fRec27[0] * fRec126[0] + fTemp32 * std::pow(2.718281828459045, -(5.0 * bbdmi_multi_granulator14_faustpower2_f(2.0 * fRec128[0] + -1.0)))) * (1.0 - 0.5 * fRec31[0] * (fRec31[0] + fTemp37 * fTemp243) * (fTemp57 * (fTemp55 * (fTemp43 * fTemp245 + std::min<double>(0.6366197723675814 * fRec34[0] * ((std::fabs(fTemp248) <= fConst6) ? std::asin(std::max<double>(-1.0, std::min<double>(1.0, 0.5 * (fTemp247 + fVec40[1])))) : (fTemp247 * std::asin(std::max<double>(-1.0, std::min<double>(1.0, fTemp247))) + std::sqrt(std::max<double>(0.0, 1.0 - bbdmi_multi_granulator14_faustpower2_f(fTemp247))) - (fVec40[1] * std::asin(std::max<double>(-1.0, std::min<double>(1.0, fVec40[1]))) + std::sqrt(std::max<double>(0.0, 1.0 - bbdmi_multi_granulator14_faustpower2_f(fVec40[1]))))) / fTemp248) * fTemp249, 1.0)) + fTemp54 * fTemp249) + fTemp56 * fTemp245 + 1.0));
			fVec41[IOTA0 & 131071] = fTemp250;
			fRec132[0] = std::fmod(fRec132[1] + (10001.0 - std::pow(2.0, 0.08333333333333333 * std::min<double>(fSlow59 * fRec41[0], 48.0))), 1e+04);
			double fTemp251 = fRec132[0] + 1e+04;
			int iTemp252 = int(fTemp251);
			double fTemp253 = std::floor(fTemp251);
			double fTemp254 = std::min<double>(0.0001 * fRec132[0], 1.0);
			double fTemp255 = fRec112[(IOTA0 - (iTemp230 + 1)) & 2097151];
			double fTemp256 = fRec2[0] * (fTemp255 + fRec117[0] * (fRec112[(IOTA0 - (iTemp232 + 1)) & 2097151] - fTemp255));
			fVec42[IOTA0 & 131071] = fTemp256;
			fRec133[0] = std::fmod(fRec133[1] + (10001.0 - std::pow(2.0, 0.08333333333333333 * std::min<double>(fSlow59 * fRec19[0], 48.0))), 1e+04);
			int iTemp257 = int(fRec133[0]);
			double fTemp258 = std::floor(fRec133[0]);
			double fTemp259 = std::min<double>(0.0001 * fRec133[0], 1.0);
			double fTemp260 = fRec133[0] + 1e+04;
			int iTemp261 = int(fTemp260);
			double fTemp262 = std::floor(fTemp260);
			int iTemp263 = int(fRec132[0]);
			double fTemp264 = std::floor(fRec132[0]);
			fRec112[IOTA0 & 2097151] = (fVec41[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp252))) & 131071] * (fTemp253 + (-9999.0 - fRec132[0])) + (fRec132[0] + (1e+04 - fTemp253)) * fVec41[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp252 + 1))) & 131071]) * (1.0 - fTemp254) + 0.6 * ((fVec42[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp257))) & 131071] * (fTemp258 + (1.0 - fRec133[0])) + (fRec133[0] - fTemp258) * fVec42[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp257 + 1))) & 131071]) * fTemp259 + (fVec42[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp261))) & 131071] * (fTemp262 + (-9999.0 - fRec133[0])) + (fRec133[0] + (1e+04 - fTemp262)) * fVec42[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp261 + 1))) & 131071]) * (1.0 - fTemp259)) + (fVec41[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp263))) & 131071] * (fTemp264 + (1.0 - fRec132[0])) + (fRec132[0] - fTemp264) * fVec41[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp263 + 1))) & 131071]) * fTemp254;
			fRec111[0] = fRec112[IOTA0 & 2097151] + 2.6014143622661517e-16 * fRec111[1] - 0.17157287525381 * fRec111[2];
			output4[i0] = FAUSTFLOAT(0.5857864376269049 * fRec111[1] + 0.2928932188134524 * (fRec111[0] + fRec111[2]));
			iRec144[0] = 1410065407 * iRec144[1] + 90005;
			double fTemp265 = 4.656612875245797e-10 * double(iRec144[0]) + 1.0;
			iRec143[0] = ((iTemp0) ? int(fRec11[0] + 0.5 * fTemp1 * fTemp265) : iRec143[1]);
			double fTemp266 = double(2 * iRec143[0]);
			fRec142[0] = ((iTemp0) ? fTemp266 + fRec12[0] * (2.0 - 0.5 * fRec15[0] * fTemp265) : fRec142[1]);
			iRec141[0] = (iVec0[1] + iRec141[1]) % int(fConst4 * fRec142[0]);
			double fTemp267 = double(iRec143[0]);
			int iTemp268 = iRec141[0] < int(fConst4 * fTemp267);
			iVec43[0] = iTemp268;
			double fTemp269 = double(iTemp268);
			int iTemp270 = (fTemp269 > 0.001) * (double(iVec43[1]) <= 0.001);
			iVec44[IOTA0 & 2097151] = iTemp270;
			iRec145[0] = 1410065407 * iRec145[1] + 90100;
			iRec140[0] = ((iTemp270) ? int(fConst4 * fRec17[0] * (1.0 - 0.5 * fRec15[0] * (4.656612875245797e-10 * double(iRec145[0]) + 1.0))) : iRec140[1]);
			double fTemp271 = fSlow62 * double(iRec140[0]);
			double fTemp272 = ((fRec136[1] != 0.0) ? (((fRec137[1] > 0.0) & (fRec137[1] < 1.0)) ? fRec136[1] : 0.0) : (((fRec137[1] == 0.0) & (fTemp271 != fRec138[1])) ? 0.0009765625 : (((fRec137[1] == 1.0) & (fTemp271 != fRec139[1])) ? -0.0009765625 : 0.0)));
			fRec136[0] = fTemp272;
			fRec137[0] = std::max<double>(0.0, std::min<double>(1.0, fRec137[1] + fTemp272));
			fRec138[0] = (((fRec137[1] >= 1.0) & (fRec139[1] != fTemp271)) ? fTemp271 : fRec138[1]);
			fRec139[0] = (((fRec137[1] <= 0.0) & (fRec138[1] != fTemp271)) ? fTemp271 : fRec139[1]);
			int iTemp273 = int(std::min<double>(fConst3, std::max<double>(0.0, fRec138[0])));
			double fTemp274 = fRec135[(IOTA0 - (iTemp273 + 1)) & 2097151];
			int iTemp275 = int(std::min<double>(fConst3, std::max<double>(0.0, fRec139[0])));
			double fTemp276 = fRec2[0] * (fTemp274 + fRec137[0] * (fRec135[(IOTA0 - (iTemp275 + 1)) & 2097151] - fTemp274));
			fVec45[IOTA0 & 131071] = fTemp276;
			fRec146[0] = std::fmod(fRec146[1] + (10001.0 - std::pow(2.0, 0.08333333333333333 * std::min<double>(fSlow62 * fRec19[0], 48.0))), 1e+04);
			int iTemp277 = int(fRec146[0]);
			double fTemp278 = std::floor(fRec146[0]);
			double fTemp279 = std::min<double>(0.0001 * fRec146[0], 1.0);
			double fTemp280 = fRec146[0] + 1e+04;
			int iTemp281 = int(fTemp280);
			double fTemp282 = std::floor(fTemp280);
			double fTemp283 = std::tan(fConst5 * std::min<double>(fSlow62 * fRec24[0], 2e+04));
			double fTemp284 = 1.0 / fTemp283;
			double fTemp285 = (fTemp284 + 1.414213562373095) / fTemp283 + 1.0;
			fRec149[0] = double(input5[i0]) - (fRec149[2] * ((fTemp284 + -1.414213562373095) / fTemp283 + 1.0) + 2.0 * fRec149[1] * (1.0 - 1.0 / bbdmi_multi_granulator14_faustpower2_f(fTemp283))) / fTemp285;
			double fTemp286 = (fRec149[2] + fRec149[0] + 2.0 * fRec149[1]) / fTemp285;
			fVec46[0] = fTemp286;
			double fTemp287 = std::tan(fConst5 * std::min<double>(fSlow62 * fRec22[0], 2e+04));
			double fTemp288 = 1.0 / fTemp287;
			fRec148[0] = ((fTemp286 - fVec46[1]) / fTemp287 - fRec148[1] * (1.0 - fTemp288)) / (fTemp288 + 1.0);
			double fTemp289 = fRec148[0] + 0.6 * fRec2[0] * fRec147[1];
			fVec47[IOTA0 & 2097151] = fTemp289;
			double fTemp290 = fVec47[(IOTA0 - iTemp273) & 2097151];
			fRec147[0] = fTemp290 + fRec137[0] * (fVec47[(IOTA0 - iTemp275) & 2097151] - fTemp290);
			double fTemp291 = 0.0005 * fTemp267 * (1.0 - 0.5 * double(1 - iTemp268));
			int iTemp292 = std::fabs(fTemp291) < 2.220446049250313e-16;
			double fTemp293 = ((iTemp292) ? 0.0 : std::exp(-(fConst6 / ((iTemp292) ? 1.0 : fTemp291))));
			fRec150[0] = fTemp269 * (1.0 - fTemp293) + fTemp293 * fRec150[1];
			int iTemp294 = iVec44[(IOTA0 - int(std::min<double>(fConst3, std::max<double>(0.0, fConst4 * fTemp266 + -1.0)))) & 2097151];
			iVec48[0] = iTemp294;
			iRec153[0] = (iRec153[1] >= 1) * (((iTemp294 - iVec48[1]) == 1) == 0) + ((iTemp270 - iVec44[(IOTA0 - 1) & 2097151]) == 1);
			double fTemp295 = double(iRec143[0] * iRec153[0]);
			int iTemp296 = ((iRec153[0] != iRec153[1]) ? int(fConst7 * fTemp295) : iRec151[1] + -1);
			iRec151[0] = iTemp296;
			double fTemp297 = double(iRec153[0]);
			fRec152[0] = ((iTemp296 > 0) ? fRec152[1] + (fTemp297 - fRec152[1]) / double(iTemp296) : fTemp297);
			double fTemp298 = 0.0005 * fTemp295;
			int iTemp299 = std::fabs(fTemp298) < 2.220446049250313e-16;
			double fTemp300 = ((iTemp299) ? 0.0 : std::exp(-(fConst6 / ((iTemp299) ? 1.0 : fTemp298))));
			fRec154[0] = fTemp297 * (1.0 - fTemp300) + fTemp300 * fRec154[1];
			double fTemp301 = fRec154[0] * fTemp41 + fRec33[0] * bbdmi_multi_granulator14_faustpower3_f(fRec152[0]);
			fRec155[0] = ((iTemp270) ? fRec38[0] : fRec155[1]);
			double fTemp302 = 6.283185307179586 * fTemp301 * std::min<double>(fSlow62 * fRec155[0], 2e+04) * (fSlow64 * fRec31[0] * fTemp301 + 1.0);
			double fTemp303 = fTemp46 * std::cos(fTemp302) + fTemp48 * std::sin(fTemp302);
			double fTemp304 = fTemp302 + 1.5707963267948966;
			double fTemp305 = fTemp46 * std::cos(fTemp304) + fTemp48 * std::sin(fTemp304);
			fVec49[0] = fTemp305;
			double fTemp306 = fTemp305 - fVec49[1];
			double fTemp307 = double((fTemp303 > 0.0) - (fTemp303 < 0.0));
			double fTemp308 = fSlow63 * fRec25[0] * fRec147[0] * (fRec27[0] * fRec150[0] + fTemp32 * std::pow(2.718281828459045, -(5.0 * bbdmi_multi_granulator14_faustpower2_f(2.0 * fRec152[0] + -1.0)))) * (1.0 - 0.5 * fRec31[0] * (fRec31[0] + fTemp37 * fTemp301) * (fTemp57 * (fTemp55 * (fTemp43 * fTemp303 + std::min<double>(0.6366197723675814 * fRec34[0] * ((std::fabs(fTemp306) <= fConst6) ? std::asin(std::max<double>(-1.0, std::min<double>(1.0, 0.5 * (fTemp305 + fVec49[1])))) : (fTemp305 * std::asin(std::max<double>(-1.0, std::min<double>(1.0, fTemp305))) + std::sqrt(std::max<double>(0.0, 1.0 - bbdmi_multi_granulator14_faustpower2_f(fTemp305))) - (fVec49[1] * std::asin(std::max<double>(-1.0, std::min<double>(1.0, fVec49[1]))) + std::sqrt(std::max<double>(0.0, 1.0 - bbdmi_multi_granulator14_faustpower2_f(fVec49[1]))))) / fTemp306) * fTemp307, 1.0)) + fTemp54 * fTemp307) + fTemp56 * fTemp303 + 1.0));
			fVec50[IOTA0 & 131071] = fTemp308;
			fRec156[0] = std::fmod(fRec156[1] + (10001.0 - std::pow(2.0, 0.08333333333333333 * std::min<double>(fSlow62 * fRec41[0], 48.0))), 1e+04);
			int iTemp309 = int(fRec156[0]);
			double fTemp310 = std::floor(fRec156[0]);
			double fTemp311 = std::min<double>(0.0001 * fRec156[0], 1.0);
			double fTemp312 = fRec156[0] + 1e+04;
			int iTemp313 = int(fTemp312);
			double fTemp314 = std::floor(fTemp312);
			fRec135[IOTA0 & 2097151] = 0.6 * ((fVec45[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp277))) & 131071] * (fTemp278 + (1.0 - fRec146[0])) + (fRec146[0] - fTemp278) * fVec45[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp277 + 1))) & 131071]) * fTemp279 + (fVec45[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp281))) & 131071] * (fTemp282 + (-9999.0 - fRec146[0])) + (fRec146[0] + (1e+04 - fTemp282)) * fVec45[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp281 + 1))) & 131071]) * (1.0 - fTemp279)) + (fVec50[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp309))) & 131071] * (fTemp310 + (1.0 - fRec156[0])) + (fRec156[0] - fTemp310) * fVec50[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp309 + 1))) & 131071]) * fTemp311 + (fVec50[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp313))) & 131071] * (fTemp314 + (-9999.0 - fRec156[0])) + (fRec156[0] + (1e+04 - fTemp314)) * fVec50[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp313 + 1))) & 131071]) * (1.0 - fTemp311);
			fRec134[0] = fRec135[IOTA0 & 2097151] + 2.6014143622661517e-16 * fRec134[1] - 0.17157287525381 * fRec134[2];
			output5[i0] = FAUSTFLOAT(0.5857864376269049 * fRec134[1] + 0.2928932188134524 * (fRec134[0] + fRec134[2]));
			iRec167[0] = 1410065407 * iRec167[1] + 90006;
			double fTemp315 = 4.656612875245797e-10 * double(iRec167[0]) + 1.0;
			iRec166[0] = ((iTemp0) ? int(fRec11[0] + 0.5 * fTemp1 * fTemp315) : iRec166[1]);
			double fTemp316 = double(2 * iRec166[0]);
			fRec165[0] = ((iTemp0) ? fTemp316 + fRec12[0] * (2.0 - 0.5 * fRec15[0] * fTemp315) : fRec165[1]);
			iRec164[0] = (iVec0[1] + iRec164[1]) % int(fConst4 * fRec165[0]);
			double fTemp317 = double(iRec166[0]);
			int iTemp318 = iRec164[0] < int(fConst4 * fTemp317);
			iVec51[0] = iTemp318;
			double fTemp319 = double(iTemp318);
			int iTemp320 = (fTemp319 > 0.001) * (double(iVec51[1]) <= 0.001);
			iVec52[IOTA0 & 2097151] = iTemp320;
			iRec168[0] = 1410065407 * iRec168[1] + 90120;
			iRec163[0] = ((iTemp320) ? int(fConst4 * fRec17[0] * (1.0 - 0.5 * fRec15[0] * (4.656612875245797e-10 * double(iRec168[0]) + 1.0))) : iRec163[1]);
			double fTemp321 = fSlow66 * double(iRec163[0]);
			double fTemp322 = ((fRec159[1] != 0.0) ? (((fRec160[1] > 0.0) & (fRec160[1] < 1.0)) ? fRec159[1] : 0.0) : (((fRec160[1] == 0.0) & (fTemp321 != fRec161[1])) ? 0.0009765625 : (((fRec160[1] == 1.0) & (fTemp321 != fRec162[1])) ? -0.0009765625 : 0.0)));
			fRec159[0] = fTemp322;
			fRec160[0] = std::max<double>(0.0, std::min<double>(1.0, fRec160[1] + fTemp322));
			fRec161[0] = (((fRec160[1] >= 1.0) & (fRec162[1] != fTemp321)) ? fTemp321 : fRec161[1]);
			fRec162[0] = (((fRec160[1] <= 0.0) & (fRec161[1] != fTemp321)) ? fTemp321 : fRec162[1]);
			int iTemp323 = int(std::min<double>(fConst3, std::max<double>(0.0, fRec161[0])));
			double fTemp324 = fRec158[(IOTA0 - (iTemp323 + 1)) & 2097151];
			int iTemp325 = int(std::min<double>(fConst3, std::max<double>(0.0, fRec162[0])));
			double fTemp326 = fRec2[0] * (fTemp324 + fRec160[0] * (fRec158[(IOTA0 - (iTemp325 + 1)) & 2097151] - fTemp324));
			fVec53[IOTA0 & 131071] = fTemp326;
			fRec169[0] = std::fmod(fRec169[1] + (10001.0 - std::pow(2.0, 0.08333333333333333 * std::min<double>(fSlow66 * fRec19[0], 48.0))), 1e+04);
			int iTemp327 = int(fRec169[0]);
			double fTemp328 = std::floor(fRec169[0]);
			double fTemp329 = std::min<double>(0.0001 * fRec169[0], 1.0);
			double fTemp330 = fRec169[0] + 1e+04;
			int iTemp331 = int(fTemp330);
			double fTemp332 = std::floor(fTemp330);
			double fTemp333 = std::tan(fConst5 * std::min<double>(fSlow66 * fRec22[0], 2e+04));
			double fTemp334 = 1.0 / fTemp333;
			double fTemp335 = std::tan(fConst5 * std::min<double>(fSlow66 * fRec24[0], 2e+04));
			double fTemp336 = 1.0 / fTemp335;
			double fTemp337 = (fTemp336 + 1.414213562373095) / fTemp335 + 1.0;
			fRec172[0] = double(input6[i0]) - (fRec172[2] * ((fTemp336 + -1.414213562373095) / fTemp335 + 1.0) + 2.0 * fRec172[1] * (1.0 - 1.0 / bbdmi_multi_granulator14_faustpower2_f(fTemp335))) / fTemp337;
			double fTemp338 = (fRec172[2] + fRec172[0] + 2.0 * fRec172[1]) / fTemp337;
			fVec54[0] = fTemp338;
			fRec171[0] = -((fRec171[1] * (1.0 - fTemp334) - (fTemp338 - fVec54[1]) / fTemp333) / (fTemp334 + 1.0));
			double fTemp339 = fRec171[0] + 0.6 * fRec2[0] * fRec170[1];
			fVec55[IOTA0 & 2097151] = fTemp339;
			double fTemp340 = fVec55[(IOTA0 - iTemp323) & 2097151];
			fRec170[0] = fTemp340 + fRec160[0] * (fVec55[(IOTA0 - iTemp325) & 2097151] - fTemp340);
			double fTemp341 = 0.0005 * fTemp317 * (1.0 - 0.5 * double(1 - iTemp318));
			int iTemp342 = std::fabs(fTemp341) < 2.220446049250313e-16;
			double fTemp343 = ((iTemp342) ? 0.0 : std::exp(-(fConst6 / ((iTemp342) ? 1.0 : fTemp341))));
			fRec173[0] = fTemp319 * (1.0 - fTemp343) + fTemp343 * fRec173[1];
			int iTemp344 = iVec52[(IOTA0 - int(std::min<double>(fConst3, std::max<double>(0.0, fConst4 * fTemp316 + -1.0)))) & 2097151];
			iVec56[0] = iTemp344;
			iRec176[0] = (iRec176[1] >= 1) * (((iTemp344 - iVec56[1]) == 1) == 0) + ((iTemp320 - iVec52[(IOTA0 - 1) & 2097151]) == 1);
			double fTemp345 = double(iRec166[0] * iRec176[0]);
			int iTemp346 = ((iRec176[0] != iRec176[1]) ? int(fConst7 * fTemp345) : iRec174[1] + -1);
			iRec174[0] = iTemp346;
			double fTemp347 = double(iRec176[0]);
			fRec175[0] = ((iTemp346 > 0) ? fRec175[1] + (fTemp347 - fRec175[1]) / double(iTemp346) : fTemp347);
			double fTemp348 = 0.0005 * fTemp345;
			int iTemp349 = std::fabs(fTemp348) < 2.220446049250313e-16;
			double fTemp350 = ((iTemp349) ? 0.0 : std::exp(-(fConst6 / ((iTemp349) ? 1.0 : fTemp348))));
			fRec177[0] = fTemp347 * (1.0 - fTemp350) + fTemp350 * fRec177[1];
			double fTemp351 = fRec177[0] * fTemp41 + fRec33[0] * bbdmi_multi_granulator14_faustpower3_f(fRec175[0]);
			fRec178[0] = ((iTemp320) ? fRec38[0] : fRec178[1]);
			double fTemp352 = 6.283185307179586 * fTemp351 * std::min<double>(fSlow66 * fRec178[0], 2e+04) * (fSlow68 * fRec31[0] * fTemp351 + 1.0);
			double fTemp353 = fTemp46 * std::cos(fTemp352) + fTemp48 * std::sin(fTemp352);
			double fTemp354 = fTemp352 + 1.5707963267948966;
			double fTemp355 = fTemp46 * std::cos(fTemp354) + fTemp48 * std::sin(fTemp354);
			fVec57[0] = fTemp355;
			double fTemp356 = fTemp355 - fVec57[1];
			double fTemp357 = double((fTemp353 > 0.0) - (fTemp353 < 0.0));
			double fTemp358 = fSlow67 * fRec25[0] * fRec170[0] * (fRec27[0] * fRec173[0] + fTemp32 * std::pow(2.718281828459045, -(5.0 * bbdmi_multi_granulator14_faustpower2_f(2.0 * fRec175[0] + -1.0)))) * (1.0 - 0.5 * fRec31[0] * (fRec31[0] + fTemp37 * fTemp351) * (fTemp57 * (fTemp55 * (fTemp43 * fTemp353 + std::min<double>(0.6366197723675814 * fRec34[0] * ((std::fabs(fTemp356) <= fConst6) ? std::asin(std::max<double>(-1.0, std::min<double>(1.0, 0.5 * (fTemp355 + fVec57[1])))) : (fTemp355 * std::asin(std::max<double>(-1.0, std::min<double>(1.0, fTemp355))) + std::sqrt(std::max<double>(0.0, 1.0 - bbdmi_multi_granulator14_faustpower2_f(fTemp355))) - (fVec57[1] * std::asin(std::max<double>(-1.0, std::min<double>(1.0, fVec57[1]))) + std::sqrt(std::max<double>(0.0, 1.0 - bbdmi_multi_granulator14_faustpower2_f(fVec57[1]))))) / fTemp356) * fTemp357, 1.0)) + fTemp54 * fTemp357) + fTemp56 * fTemp353 + 1.0));
			fVec58[IOTA0 & 131071] = fTemp358;
			fRec179[0] = std::fmod(fRec179[1] + (10001.0 - std::pow(2.0, 0.08333333333333333 * std::min<double>(fSlow66 * fRec41[0], 48.0))), 1e+04);
			int iTemp359 = int(fRec179[0]);
			double fTemp360 = std::floor(fRec179[0]);
			double fTemp361 = std::min<double>(0.0001 * fRec179[0], 1.0);
			double fTemp362 = fRec179[0] + 1e+04;
			int iTemp363 = int(fTemp362);
			double fTemp364 = std::floor(fTemp362);
			fRec158[IOTA0 & 2097151] = 0.6 * ((fVec53[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp327))) & 131071] * (fTemp328 + (1.0 - fRec169[0])) + (fRec169[0] - fTemp328) * fVec53[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp327 + 1))) & 131071]) * fTemp329 + (fVec53[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp331))) & 131071] * (fTemp332 + (-9999.0 - fRec169[0])) + (fRec169[0] + (1e+04 - fTemp332)) * fVec53[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp331 + 1))) & 131071]) * (1.0 - fTemp329)) + (fVec58[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp359))) & 131071] * (fTemp360 + (1.0 - fRec179[0])) + (fRec179[0] - fTemp360) * fVec58[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp359 + 1))) & 131071]) * fTemp361 + (fVec58[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp363))) & 131071] * (fTemp364 + (-9999.0 - fRec179[0])) + (fRec179[0] + (1e+04 - fTemp364)) * fVec58[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp363 + 1))) & 131071]) * (1.0 - fTemp361);
			fRec157[0] = fRec158[IOTA0 & 2097151] + 2.6014143622661517e-16 * fRec157[1] - 0.17157287525381 * fRec157[2];
			output6[i0] = FAUSTFLOAT(0.5857864376269049 * fRec157[1] + 0.2928932188134524 * (fRec157[0] + fRec157[2]));
			double fTemp365 = std::tan(fConst5 * std::min<double>(fSlow71 * fRec22[0], 2e+04));
			double fTemp366 = 1.0 / fTemp365;
			double fTemp367 = std::tan(fConst5 * std::min<double>(fSlow71 * fRec24[0], 2e+04));
			double fTemp368 = 1.0 / fTemp367;
			double fTemp369 = (fTemp368 + 1.414213562373095) / fTemp367 + 1.0;
			fRec184[0] = double(input7[i0]) - (fRec184[2] * ((fTemp368 + -1.414213562373095) / fTemp367 + 1.0) + 2.0 * fRec184[1] * (1.0 - 1.0 / bbdmi_multi_granulator14_faustpower2_f(fTemp367))) / fTemp369;
			double fTemp370 = (fRec184[2] + fRec184[0] + 2.0 * fRec184[1]) / fTemp369;
			fVec59[0] = fTemp370;
			fRec183[0] = -((fRec183[1] * (1.0 - fTemp366) - (fTemp370 - fVec59[1]) / fTemp365) / (fTemp366 + 1.0));
			double fTemp371 = fRec183[0] + 0.6 * fRec2[0] * fRec182[1];
			fVec60[IOTA0 & 2097151] = fTemp371;
			iRec193[0] = 1410065407 * iRec193[1] + 90007;
			double fTemp372 = 4.656612875245797e-10 * double(iRec193[0]) + 1.0;
			iRec192[0] = ((iTemp0) ? int(fRec11[0] + 0.5 * fTemp1 * fTemp372) : iRec192[1]);
			double fTemp373 = double(2 * iRec192[0]);
			fRec191[0] = ((iTemp0) ? fTemp373 + fRec12[0] * (2.0 - 0.5 * fRec15[0] * fTemp372) : fRec191[1]);
			iRec190[0] = (iVec0[1] + iRec190[1]) % int(fConst4 * fRec191[0]);
			double fTemp374 = double(iRec192[0]);
			int iTemp375 = iRec190[0] < int(fConst4 * fTemp374);
			iVec61[0] = iTemp375;
			double fTemp376 = double(iTemp375);
			int iTemp377 = (fTemp376 > 0.001) * (double(iVec61[1]) <= 0.001);
			iVec62[IOTA0 & 2097151] = iTemp377;
			iRec194[0] = 1410065407 * iRec194[1] + 90140;
			iRec189[0] = ((iTemp377) ? int(fConst4 * fRec17[0] * (1.0 - 0.5 * fRec15[0] * (4.656612875245797e-10 * double(iRec194[0]) + 1.0))) : iRec189[1]);
			double fTemp378 = fSlow71 * double(iRec189[0]);
			double fTemp379 = ((fRec185[1] != 0.0) ? (((fRec186[1] > 0.0) & (fRec186[1] < 1.0)) ? fRec185[1] : 0.0) : (((fRec186[1] == 0.0) & (fTemp378 != fRec187[1])) ? 0.0009765625 : (((fRec186[1] == 1.0) & (fTemp378 != fRec188[1])) ? -0.0009765625 : 0.0)));
			fRec185[0] = fTemp379;
			fRec186[0] = std::max<double>(0.0, std::min<double>(1.0, fRec186[1] + fTemp379));
			fRec187[0] = (((fRec186[1] >= 1.0) & (fRec188[1] != fTemp378)) ? fTemp378 : fRec187[1]);
			fRec188[0] = (((fRec186[1] <= 0.0) & (fRec187[1] != fTemp378)) ? fTemp378 : fRec188[1]);
			int iTemp380 = int(std::min<double>(fConst3, std::max<double>(0.0, fRec187[0])));
			double fTemp381 = fVec60[(IOTA0 - iTemp380) & 2097151];
			int iTemp382 = int(std::min<double>(fConst3, std::max<double>(0.0, fRec188[0])));
			fRec182[0] = fTemp381 + fRec186[0] * (fVec60[(IOTA0 - iTemp382) & 2097151] - fTemp381);
			double fTemp383 = 0.0005 * fTemp374 * (1.0 - 0.5 * double(1 - iTemp375));
			int iTemp384 = std::fabs(fTemp383) < 2.220446049250313e-16;
			double fTemp385 = ((iTemp384) ? 0.0 : std::exp(-(fConst6 / ((iTemp384) ? 1.0 : fTemp383))));
			fRec195[0] = fTemp376 * (1.0 - fTemp385) + fTemp385 * fRec195[1];
			int iTemp386 = iVec62[(IOTA0 - int(std::min<double>(fConst3, std::max<double>(0.0, fConst4 * fTemp373 + -1.0)))) & 2097151];
			iVec63[0] = iTemp386;
			iRec198[0] = (iRec198[1] >= 1) * (((iTemp386 - iVec63[1]) == 1) == 0) + ((iTemp377 - iVec62[(IOTA0 - 1) & 2097151]) == 1);
			double fTemp387 = double(iRec192[0] * iRec198[0]);
			int iTemp388 = ((iRec198[0] != iRec198[1]) ? int(fConst7 * fTemp387) : iRec196[1] + -1);
			iRec196[0] = iTemp388;
			double fTemp389 = double(iRec198[0]);
			fRec197[0] = ((iTemp388 > 0) ? fRec197[1] + (fTemp389 - fRec197[1]) / double(iTemp388) : fTemp389);
			double fTemp390 = 0.0005 * fTemp387;
			int iTemp391 = std::fabs(fTemp390) < 2.220446049250313e-16;
			double fTemp392 = ((iTemp391) ? 0.0 : std::exp(-(fConst6 / ((iTemp391) ? 1.0 : fTemp390))));
			fRec199[0] = fTemp389 * (1.0 - fTemp392) + fTemp392 * fRec199[1];
			double fTemp393 = fRec199[0] * fTemp41 + fRec33[0] * bbdmi_multi_granulator14_faustpower3_f(fRec197[0]);
			fRec200[0] = ((iTemp377) ? fRec38[0] : fRec200[1]);
			double fTemp394 = 6.283185307179586 * fTemp393 * std::min<double>(fSlow71 * fRec200[0], 2e+04) * (fSlow72 * fRec31[0] * fTemp393 + 1.0);
			double fTemp395 = fTemp46 * std::cos(fTemp394) + fTemp48 * std::sin(fTemp394);
			double fTemp396 = fTemp394 + 1.5707963267948966;
			double fTemp397 = fTemp46 * std::cos(fTemp396) + fTemp48 * std::sin(fTemp396);
			fVec64[0] = fTemp397;
			double fTemp398 = fTemp397 - fVec64[1];
			double fTemp399 = double((fTemp395 > 0.0) - (fTemp395 < 0.0));
			double fTemp400 = fSlow70 * fRec25[0] * fRec182[0] * (fRec27[0] * fRec195[0] + fTemp32 * std::pow(2.718281828459045, -(5.0 * bbdmi_multi_granulator14_faustpower2_f(2.0 * fRec197[0] + -1.0)))) * (1.0 - 0.5 * fRec31[0] * (fRec31[0] + fTemp37 * fTemp393) * (fTemp57 * (fTemp55 * (fTemp43 * fTemp395 + std::min<double>(0.6366197723675814 * fRec34[0] * ((std::fabs(fTemp398) <= fConst6) ? std::asin(std::max<double>(-1.0, std::min<double>(1.0, 0.5 * (fTemp397 + fVec64[1])))) : (fTemp397 * std::asin(std::max<double>(-1.0, std::min<double>(1.0, fTemp397))) + std::sqrt(std::max<double>(0.0, 1.0 - bbdmi_multi_granulator14_faustpower2_f(fTemp397))) - (fVec64[1] * std::asin(std::max<double>(-1.0, std::min<double>(1.0, fVec64[1]))) + std::sqrt(std::max<double>(0.0, 1.0 - bbdmi_multi_granulator14_faustpower2_f(fVec64[1]))))) / fTemp398) * fTemp399, 1.0)) + fTemp54 * fTemp399) + fTemp56 * fTemp395 + 1.0));
			fVec65[IOTA0 & 131071] = fTemp400;
			fRec201[0] = std::fmod(fRec201[1] + (10001.0 - std::pow(2.0, 0.08333333333333333 * std::min<double>(fSlow71 * fRec41[0], 48.0))), 1e+04);
			double fTemp401 = fRec201[0] + 1e+04;
			int iTemp402 = int(fTemp401);
			double fTemp403 = std::floor(fTemp401);
			double fTemp404 = std::min<double>(0.0001 * fRec201[0], 1.0);
			double fTemp405 = fRec181[(IOTA0 - (iTemp380 + 1)) & 2097151];
			double fTemp406 = fRec2[0] * (fTemp405 + fRec186[0] * (fRec181[(IOTA0 - (iTemp382 + 1)) & 2097151] - fTemp405));
			fVec66[IOTA0 & 131071] = fTemp406;
			fRec202[0] = std::fmod(fRec202[1] + (10001.0 - std::pow(2.0, 0.08333333333333333 * std::min<double>(fSlow71 * fRec19[0], 48.0))), 1e+04);
			int iTemp407 = int(fRec202[0]);
			double fTemp408 = std::floor(fRec202[0]);
			double fTemp409 = std::min<double>(0.0001 * fRec202[0], 1.0);
			double fTemp410 = fRec202[0] + 1e+04;
			int iTemp411 = int(fTemp410);
			double fTemp412 = std::floor(fTemp410);
			int iTemp413 = int(fRec201[0]);
			double fTemp414 = std::floor(fRec201[0]);
			fRec181[IOTA0 & 2097151] = (fVec65[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp402))) & 131071] * (fTemp403 + (-9999.0 - fRec201[0])) + (fRec201[0] + (1e+04 - fTemp403)) * fVec65[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp402 + 1))) & 131071]) * (1.0 - fTemp404) + 0.6 * ((fVec66[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp407))) & 131071] * (fTemp408 + (1.0 - fRec202[0])) + (fRec202[0] - fTemp408) * fVec66[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp407 + 1))) & 131071]) * fTemp409 + (fVec66[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp411))) & 131071] * (fTemp412 + (-9999.0 - fRec202[0])) + (fRec202[0] + (1e+04 - fTemp412)) * fVec66[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp411 + 1))) & 131071]) * (1.0 - fTemp409)) + (fVec65[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp413))) & 131071] * (fTemp414 + (1.0 - fRec201[0])) + (fRec201[0] - fTemp414) * fVec65[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp413 + 1))) & 131071]) * fTemp404;
			fRec180[0] = fRec181[IOTA0 & 2097151] + 2.6014143622661517e-16 * fRec180[1] - 0.17157287525381 * fRec180[2];
			output7[i0] = FAUSTFLOAT(0.5857864376269049 * fRec180[1] + 0.2928932188134524 * (fRec180[0] + fRec180[2]));
			double fTemp415 = std::tan(fConst5 * std::min<double>(fSlow75 * fRec22[0], 2e+04));
			double fTemp416 = 1.0 / fTemp415;
			double fTemp417 = std::tan(fConst5 * std::min<double>(fSlow75 * fRec24[0], 2e+04));
			double fTemp418 = 1.0 / fTemp417;
			double fTemp419 = (fTemp418 + 1.414213562373095) / fTemp417 + 1.0;
			fRec207[0] = double(input8[i0]) - (fRec207[2] * ((fTemp418 + -1.414213562373095) / fTemp417 + 1.0) + 2.0 * fRec207[1] * (1.0 - 1.0 / bbdmi_multi_granulator14_faustpower2_f(fTemp417))) / fTemp419;
			double fTemp420 = (fRec207[2] + fRec207[0] + 2.0 * fRec207[1]) / fTemp419;
			fVec67[0] = fTemp420;
			fRec206[0] = -((fRec206[1] * (1.0 - fTemp416) - (fTemp420 - fVec67[1]) / fTemp415) / (fTemp416 + 1.0));
			double fTemp421 = fRec206[0] + 0.6 * fRec2[0] * fRec205[1];
			fVec68[IOTA0 & 2097151] = fTemp421;
			iRec216[0] = 1410065407 * iRec216[1] + 90008;
			double fTemp422 = 4.656612875245797e-10 * double(iRec216[0]) + 1.0;
			iRec215[0] = ((iTemp0) ? int(fRec11[0] + 0.5 * fTemp1 * fTemp422) : iRec215[1]);
			double fTemp423 = double(2 * iRec215[0]);
			fRec214[0] = ((iTemp0) ? fTemp423 + fRec12[0] * (2.0 - 0.5 * fRec15[0] * fTemp422) : fRec214[1]);
			iRec213[0] = (iVec0[1] + iRec213[1]) % int(fConst4 * fRec214[0]);
			double fTemp424 = double(iRec215[0]);
			int iTemp425 = iRec213[0] < int(fConst4 * fTemp424);
			iVec69[0] = iTemp425;
			double fTemp426 = double(iTemp425);
			int iTemp427 = (fTemp426 > 0.001) * (double(iVec69[1]) <= 0.001);
			iVec70[IOTA0 & 2097151] = iTemp427;
			iRec217[0] = 1410065407 * iRec217[1] + 90160;
			iRec212[0] = ((iTemp427) ? int(fConst4 * fRec17[0] * (1.0 - 0.5 * fRec15[0] * (4.656612875245797e-10 * double(iRec217[0]) + 1.0))) : iRec212[1]);
			double fTemp428 = fSlow75 * double(iRec212[0]);
			double fTemp429 = ((fRec208[1] != 0.0) ? (((fRec209[1] > 0.0) & (fRec209[1] < 1.0)) ? fRec208[1] : 0.0) : (((fRec209[1] == 0.0) & (fTemp428 != fRec210[1])) ? 0.0009765625 : (((fRec209[1] == 1.0) & (fTemp428 != fRec211[1])) ? -0.0009765625 : 0.0)));
			fRec208[0] = fTemp429;
			fRec209[0] = std::max<double>(0.0, std::min<double>(1.0, fRec209[1] + fTemp429));
			fRec210[0] = (((fRec209[1] >= 1.0) & (fRec211[1] != fTemp428)) ? fTemp428 : fRec210[1]);
			fRec211[0] = (((fRec209[1] <= 0.0) & (fRec210[1] != fTemp428)) ? fTemp428 : fRec211[1]);
			int iTemp430 = int(std::min<double>(fConst3, std::max<double>(0.0, fRec210[0])));
			double fTemp431 = fVec68[(IOTA0 - iTemp430) & 2097151];
			int iTemp432 = int(std::min<double>(fConst3, std::max<double>(0.0, fRec211[0])));
			fRec205[0] = fTemp431 + fRec209[0] * (fVec68[(IOTA0 - iTemp432) & 2097151] - fTemp431);
			double fTemp433 = 0.0005 * fTemp424 * (1.0 - 0.5 * double(1 - iTemp425));
			int iTemp434 = std::fabs(fTemp433) < 2.220446049250313e-16;
			double fTemp435 = ((iTemp434) ? 0.0 : std::exp(-(fConst6 / ((iTemp434) ? 1.0 : fTemp433))));
			fRec218[0] = fTemp426 * (1.0 - fTemp435) + fTemp435 * fRec218[1];
			int iTemp436 = iVec70[(IOTA0 - int(std::min<double>(fConst3, std::max<double>(0.0, fConst4 * fTemp423 + -1.0)))) & 2097151];
			iVec71[0] = iTemp436;
			iRec221[0] = (iRec221[1] >= 1) * (((iTemp436 - iVec71[1]) == 1) == 0) + ((iTemp427 - iVec70[(IOTA0 - 1) & 2097151]) == 1);
			double fTemp437 = double(iRec215[0] * iRec221[0]);
			int iTemp438 = ((iRec221[0] != iRec221[1]) ? int(fConst7 * fTemp437) : iRec219[1] + -1);
			iRec219[0] = iTemp438;
			double fTemp439 = double(iRec221[0]);
			fRec220[0] = ((iTemp438 > 0) ? fRec220[1] + (fTemp439 - fRec220[1]) / double(iTemp438) : fTemp439);
			double fTemp440 = 0.0005 * fTemp437;
			int iTemp441 = std::fabs(fTemp440) < 2.220446049250313e-16;
			double fTemp442 = ((iTemp441) ? 0.0 : std::exp(-(fConst6 / ((iTemp441) ? 1.0 : fTemp440))));
			fRec222[0] = fTemp439 * (1.0 - fTemp442) + fTemp442 * fRec222[1];
			double fTemp443 = fRec222[0] * fTemp41 + fRec33[0] * bbdmi_multi_granulator14_faustpower3_f(fRec220[0]);
			fRec223[0] = ((iTemp427) ? fRec38[0] : fRec223[1]);
			double fTemp444 = 6.283185307179586 * fTemp443 * std::min<double>(fSlow75 * fRec223[0], 2e+04) * (fSlow76 * fRec31[0] * fTemp443 + 1.0);
			double fTemp445 = fTemp46 * std::cos(fTemp444) + fTemp48 * std::sin(fTemp444);
			double fTemp446 = fTemp444 + 1.5707963267948966;
			double fTemp447 = fTemp46 * std::cos(fTemp446) + fTemp48 * std::sin(fTemp446);
			fVec72[0] = fTemp447;
			double fTemp448 = fTemp447 - fVec72[1];
			double fTemp449 = double((fTemp445 > 0.0) - (fTemp445 < 0.0));
			double fTemp450 = fSlow74 * fRec25[0] * fRec205[0] * (fRec27[0] * fRec218[0] + fTemp32 * std::pow(2.718281828459045, -(5.0 * bbdmi_multi_granulator14_faustpower2_f(2.0 * fRec220[0] + -1.0)))) * (1.0 - 0.5 * fRec31[0] * (fRec31[0] + fTemp37 * fTemp443) * (fTemp57 * (fTemp55 * (fTemp43 * fTemp445 + std::min<double>(0.6366197723675814 * fRec34[0] * ((std::fabs(fTemp448) <= fConst6) ? std::asin(std::max<double>(-1.0, std::min<double>(1.0, 0.5 * (fTemp447 + fVec72[1])))) : (fTemp447 * std::asin(std::max<double>(-1.0, std::min<double>(1.0, fTemp447))) + std::sqrt(std::max<double>(0.0, 1.0 - bbdmi_multi_granulator14_faustpower2_f(fTemp447))) - (fVec72[1] * std::asin(std::max<double>(-1.0, std::min<double>(1.0, fVec72[1]))) + std::sqrt(std::max<double>(0.0, 1.0 - bbdmi_multi_granulator14_faustpower2_f(fVec72[1]))))) / fTemp448) * fTemp449, 1.0)) + fTemp54 * fTemp449) + fTemp56 * fTemp445 + 1.0));
			fVec73[IOTA0 & 131071] = fTemp450;
			fRec224[0] = std::fmod(fRec224[1] + (10001.0 - std::pow(2.0, 0.08333333333333333 * std::min<double>(fSlow75 * fRec41[0], 48.0))), 1e+04);
			double fTemp451 = fRec224[0] + 1e+04;
			int iTemp452 = int(fTemp451);
			double fTemp453 = std::floor(fTemp451);
			double fTemp454 = std::min<double>(0.0001 * fRec224[0], 1.0);
			double fTemp455 = fRec204[(IOTA0 - (iTemp430 + 1)) & 2097151];
			double fTemp456 = fRec2[0] * (fTemp455 + fRec209[0] * (fRec204[(IOTA0 - (iTemp432 + 1)) & 2097151] - fTemp455));
			fVec74[IOTA0 & 131071] = fTemp456;
			fRec225[0] = std::fmod(fRec225[1] + (10001.0 - std::pow(2.0, 0.08333333333333333 * std::min<double>(fSlow75 * fRec19[0], 48.0))), 1e+04);
			int iTemp457 = int(fRec225[0]);
			double fTemp458 = std::floor(fRec225[0]);
			double fTemp459 = std::min<double>(0.0001 * fRec225[0], 1.0);
			double fTemp460 = fRec225[0] + 1e+04;
			int iTemp461 = int(fTemp460);
			double fTemp462 = std::floor(fTemp460);
			int iTemp463 = int(fRec224[0]);
			double fTemp464 = std::floor(fRec224[0]);
			fRec204[IOTA0 & 2097151] = (fVec73[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp452))) & 131071] * (fTemp453 + (-9999.0 - fRec224[0])) + (fRec224[0] + (1e+04 - fTemp453)) * fVec73[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp452 + 1))) & 131071]) * (1.0 - fTemp454) + 0.6 * ((fVec74[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp457))) & 131071] * (fTemp458 + (1.0 - fRec225[0])) + (fRec225[0] - fTemp458) * fVec74[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp457 + 1))) & 131071]) * fTemp459 + (fVec74[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp461))) & 131071] * (fTemp462 + (-9999.0 - fRec225[0])) + (fRec225[0] + (1e+04 - fTemp462)) * fVec74[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp461 + 1))) & 131071]) * (1.0 - fTemp459)) + (fVec73[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp463))) & 131071] * (fTemp464 + (1.0 - fRec224[0])) + (fRec224[0] - fTemp464) * fVec73[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp463 + 1))) & 131071]) * fTemp454;
			fRec203[0] = fRec204[IOTA0 & 2097151] + 2.6014143622661517e-16 * fRec203[1] - 0.17157287525381 * fRec203[2];
			output8[i0] = FAUSTFLOAT(0.5857864376269049 * fRec203[1] + 0.2928932188134524 * (fRec203[0] + fRec203[2]));
			iRec236[0] = 1410065407 * iRec236[1] + 90009;
			double fTemp465 = 4.656612875245797e-10 * double(iRec236[0]) + 1.0;
			iRec235[0] = ((iTemp0) ? int(fRec11[0] + 0.5 * fTemp1 * fTemp465) : iRec235[1]);
			double fTemp466 = double(2 * iRec235[0]);
			fRec234[0] = ((iTemp0) ? fTemp466 + fRec12[0] * (2.0 - 0.5 * fRec15[0] * fTemp465) : fRec234[1]);
			iRec233[0] = (iVec0[1] + iRec233[1]) % int(fConst4 * fRec234[0]);
			double fTemp467 = double(iRec235[0]);
			int iTemp468 = iRec233[0] < int(fConst4 * fTemp467);
			iVec75[0] = iTemp468;
			double fTemp469 = double(iTemp468);
			int iTemp470 = (fTemp469 > 0.001) * (double(iVec75[1]) <= 0.001);
			iVec76[IOTA0 & 2097151] = iTemp470;
			iRec237[0] = 1410065407 * iRec237[1] + 90180;
			iRec232[0] = ((iTemp470) ? int(fConst4 * fRec17[0] * (1.0 - 0.5 * fRec15[0] * (4.656612875245797e-10 * double(iRec237[0]) + 1.0))) : iRec232[1]);
			double fTemp471 = fSlow78 * double(iRec232[0]);
			double fTemp472 = ((fRec228[1] != 0.0) ? (((fRec229[1] > 0.0) & (fRec229[1] < 1.0)) ? fRec228[1] : 0.0) : (((fRec229[1] == 0.0) & (fTemp471 != fRec230[1])) ? 0.0009765625 : (((fRec229[1] == 1.0) & (fTemp471 != fRec231[1])) ? -0.0009765625 : 0.0)));
			fRec228[0] = fTemp472;
			fRec229[0] = std::max<double>(0.0, std::min<double>(1.0, fRec229[1] + fTemp472));
			fRec230[0] = (((fRec229[1] >= 1.0) & (fRec231[1] != fTemp471)) ? fTemp471 : fRec230[1]);
			fRec231[0] = (((fRec229[1] <= 0.0) & (fRec230[1] != fTemp471)) ? fTemp471 : fRec231[1]);
			int iTemp473 = int(std::min<double>(fConst3, std::max<double>(0.0, fRec230[0])));
			double fTemp474 = fRec227[(IOTA0 - (iTemp473 + 1)) & 2097151];
			int iTemp475 = int(std::min<double>(fConst3, std::max<double>(0.0, fRec231[0])));
			double fTemp476 = fRec2[0] * (fTemp474 + fRec229[0] * (fRec227[(IOTA0 - (iTemp475 + 1)) & 2097151] - fTemp474));
			fVec77[IOTA0 & 131071] = fTemp476;
			fRec238[0] = std::fmod(fRec238[1] + (10001.0 - std::pow(2.0, 0.08333333333333333 * std::min<double>(fSlow78 * fRec19[0], 48.0))), 1e+04);
			int iTemp477 = int(fRec238[0]);
			double fTemp478 = std::floor(fRec238[0]);
			double fTemp479 = std::min<double>(0.0001 * fRec238[0], 1.0);
			double fTemp480 = fRec238[0] + 1e+04;
			int iTemp481 = int(fTemp480);
			double fTemp482 = std::floor(fTemp480);
			double fTemp483 = std::tan(fConst5 * std::min<double>(fSlow78 * fRec24[0], 2e+04));
			double fTemp484 = 1.0 / fTemp483;
			double fTemp485 = (fTemp484 + 1.414213562373095) / fTemp483 + 1.0;
			fRec241[0] = double(input9[i0]) - (fRec241[2] * ((fTemp484 + -1.414213562373095) / fTemp483 + 1.0) + 2.0 * fRec241[1] * (1.0 - 1.0 / bbdmi_multi_granulator14_faustpower2_f(fTemp483))) / fTemp485;
			double fTemp486 = (fRec241[2] + fRec241[0] + 2.0 * fRec241[1]) / fTemp485;
			fVec78[0] = fTemp486;
			double fTemp487 = std::tan(fConst5 * std::min<double>(fSlow78 * fRec22[0], 2e+04));
			double fTemp488 = 1.0 / fTemp487;
			fRec240[0] = ((fTemp486 - fVec78[1]) / fTemp487 - fRec240[1] * (1.0 - fTemp488)) / (fTemp488 + 1.0);
			double fTemp489 = fRec240[0] + 0.6 * fRec2[0] * fRec239[1];
			fVec79[IOTA0 & 2097151] = fTemp489;
			double fTemp490 = fVec79[(IOTA0 - iTemp473) & 2097151];
			fRec239[0] = fTemp490 + fRec229[0] * (fVec79[(IOTA0 - iTemp475) & 2097151] - fTemp490);
			double fTemp491 = 0.0005 * fTemp467 * (1.0 - 0.5 * double(1 - iTemp468));
			int iTemp492 = std::fabs(fTemp491) < 2.220446049250313e-16;
			double fTemp493 = ((iTemp492) ? 0.0 : std::exp(-(fConst6 / ((iTemp492) ? 1.0 : fTemp491))));
			fRec242[0] = fTemp469 * (1.0 - fTemp493) + fTemp493 * fRec242[1];
			int iTemp494 = iVec76[(IOTA0 - int(std::min<double>(fConst3, std::max<double>(0.0, fConst4 * fTemp466 + -1.0)))) & 2097151];
			iVec80[0] = iTemp494;
			iRec245[0] = (iRec245[1] >= 1) * (((iTemp494 - iVec80[1]) == 1) == 0) + ((iTemp470 - iVec76[(IOTA0 - 1) & 2097151]) == 1);
			double fTemp495 = double(iRec235[0] * iRec245[0]);
			int iTemp496 = ((iRec245[0] != iRec245[1]) ? int(fConst7 * fTemp495) : iRec243[1] + -1);
			iRec243[0] = iTemp496;
			double fTemp497 = double(iRec245[0]);
			fRec244[0] = ((iTemp496 > 0) ? fRec244[1] + (fTemp497 - fRec244[1]) / double(iTemp496) : fTemp497);
			double fTemp498 = 0.0005 * fTemp495;
			int iTemp499 = std::fabs(fTemp498) < 2.220446049250313e-16;
			double fTemp500 = ((iTemp499) ? 0.0 : std::exp(-(fConst6 / ((iTemp499) ? 1.0 : fTemp498))));
			fRec246[0] = fTemp497 * (1.0 - fTemp500) + fTemp500 * fRec246[1];
			double fTemp501 = fRec246[0] * fTemp41 + fRec33[0] * bbdmi_multi_granulator14_faustpower3_f(fRec244[0]);
			fRec247[0] = ((iTemp470) ? fRec38[0] : fRec247[1]);
			double fTemp502 = 6.283185307179586 * fTemp501 * std::min<double>(fSlow78 * fRec247[0], 2e+04) * (fSlow80 * fRec31[0] * fTemp501 + 1.0);
			double fTemp503 = fTemp46 * std::cos(fTemp502) + fTemp48 * std::sin(fTemp502);
			double fTemp504 = fTemp502 + 1.5707963267948966;
			double fTemp505 = fTemp46 * std::cos(fTemp504) + fTemp48 * std::sin(fTemp504);
			fVec81[0] = fTemp505;
			double fTemp506 = fTemp505 - fVec81[1];
			double fTemp507 = double((fTemp503 > 0.0) - (fTemp503 < 0.0));
			double fTemp508 = fSlow79 * fRec25[0] * fRec239[0] * (fRec27[0] * fRec242[0] + fTemp32 * std::pow(2.718281828459045, -(5.0 * bbdmi_multi_granulator14_faustpower2_f(2.0 * fRec244[0] + -1.0)))) * (1.0 - 0.5 * fRec31[0] * (fRec31[0] + fTemp37 * fTemp501) * (fTemp57 * (fTemp55 * (fTemp43 * fTemp503 + std::min<double>(0.6366197723675814 * fRec34[0] * ((std::fabs(fTemp506) <= fConst6) ? std::asin(std::max<double>(-1.0, std::min<double>(1.0, 0.5 * (fTemp505 + fVec81[1])))) : (fTemp505 * std::asin(std::max<double>(-1.0, std::min<double>(1.0, fTemp505))) + std::sqrt(std::max<double>(0.0, 1.0 - bbdmi_multi_granulator14_faustpower2_f(fTemp505))) - (fVec81[1] * std::asin(std::max<double>(-1.0, std::min<double>(1.0, fVec81[1]))) + std::sqrt(std::max<double>(0.0, 1.0 - bbdmi_multi_granulator14_faustpower2_f(fVec81[1]))))) / fTemp506) * fTemp507, 1.0)) + fTemp54 * fTemp507) + fTemp56 * fTemp503 + 1.0));
			fVec82[IOTA0 & 131071] = fTemp508;
			fRec248[0] = std::fmod(fRec248[1] + (10001.0 - std::pow(2.0, 0.08333333333333333 * std::min<double>(fSlow78 * fRec41[0], 48.0))), 1e+04);
			int iTemp509 = int(fRec248[0]);
			double fTemp510 = std::floor(fRec248[0]);
			double fTemp511 = std::min<double>(0.0001 * fRec248[0], 1.0);
			double fTemp512 = fRec248[0] + 1e+04;
			int iTemp513 = int(fTemp512);
			double fTemp514 = std::floor(fTemp512);
			fRec227[IOTA0 & 2097151] = 0.6 * ((fVec77[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp477))) & 131071] * (fTemp478 + (1.0 - fRec238[0])) + (fRec238[0] - fTemp478) * fVec77[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp477 + 1))) & 131071]) * fTemp479 + (fVec77[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp481))) & 131071] * (fTemp482 + (-9999.0 - fRec238[0])) + (fRec238[0] + (1e+04 - fTemp482)) * fVec77[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp481 + 1))) & 131071]) * (1.0 - fTemp479)) + (fVec82[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp509))) & 131071] * (fTemp510 + (1.0 - fRec248[0])) + (fRec248[0] - fTemp510) * fVec82[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp509 + 1))) & 131071]) * fTemp511 + (fVec82[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp513))) & 131071] * (fTemp514 + (-9999.0 - fRec248[0])) + (fRec248[0] + (1e+04 - fTemp514)) * fVec82[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp513 + 1))) & 131071]) * (1.0 - fTemp511);
			fRec226[0] = fRec227[IOTA0 & 2097151] + 2.6014143622661517e-16 * fRec226[1] - 0.17157287525381 * fRec226[2];
			output9[i0] = FAUSTFLOAT(0.5857864376269049 * fRec226[1] + 0.2928932188134524 * (fRec226[0] + fRec226[2]));
			iRec259[0] = 1410065407 * iRec259[1] + 90010;
			double fTemp515 = 4.656612875245797e-10 * double(iRec259[0]) + 1.0;
			iRec258[0] = ((iTemp0) ? int(fRec11[0] + 0.5 * fTemp1 * fTemp515) : iRec258[1]);
			double fTemp516 = double(2 * iRec258[0]);
			fRec257[0] = ((iTemp0) ? fTemp516 + fRec12[0] * (2.0 - 0.5 * fRec15[0] * fTemp515) : fRec257[1]);
			iRec256[0] = (iVec0[1] + iRec256[1]) % int(fConst4 * fRec257[0]);
			double fTemp517 = double(iRec258[0]);
			int iTemp518 = iRec256[0] < int(fConst4 * fTemp517);
			iVec83[0] = iTemp518;
			double fTemp519 = double(iTemp518);
			int iTemp520 = (fTemp519 > 0.001) * (double(iVec83[1]) <= 0.001);
			iVec84[IOTA0 & 2097151] = iTemp520;
			iRec260[0] = 1410065407 * iRec260[1] + 90200;
			iRec255[0] = ((iTemp520) ? int(fConst4 * fRec17[0] * (1.0 - 0.5 * fRec15[0] * (4.656612875245797e-10 * double(iRec260[0]) + 1.0))) : iRec255[1]);
			double fTemp521 = fSlow82 * double(iRec255[0]);
			double fTemp522 = ((fRec251[1] != 0.0) ? (((fRec252[1] > 0.0) & (fRec252[1] < 1.0)) ? fRec251[1] : 0.0) : (((fRec252[1] == 0.0) & (fTemp521 != fRec253[1])) ? 0.0009765625 : (((fRec252[1] == 1.0) & (fTemp521 != fRec254[1])) ? -0.0009765625 : 0.0)));
			fRec251[0] = fTemp522;
			fRec252[0] = std::max<double>(0.0, std::min<double>(1.0, fRec252[1] + fTemp522));
			fRec253[0] = (((fRec252[1] >= 1.0) & (fRec254[1] != fTemp521)) ? fTemp521 : fRec253[1]);
			fRec254[0] = (((fRec252[1] <= 0.0) & (fRec253[1] != fTemp521)) ? fTemp521 : fRec254[1]);
			int iTemp523 = int(std::min<double>(fConst3, std::max<double>(0.0, fRec253[0])));
			double fTemp524 = fRec250[(IOTA0 - (iTemp523 + 1)) & 2097151];
			int iTemp525 = int(std::min<double>(fConst3, std::max<double>(0.0, fRec254[0])));
			double fTemp526 = fRec2[0] * (fTemp524 + fRec252[0] * (fRec250[(IOTA0 - (iTemp525 + 1)) & 2097151] - fTemp524));
			fVec85[IOTA0 & 131071] = fTemp526;
			fRec261[0] = std::fmod(fRec261[1] + (10001.0 - std::pow(2.0, 0.08333333333333333 * std::min<double>(fSlow82 * fRec19[0], 48.0))), 1e+04);
			int iTemp527 = int(fRec261[0]);
			double fTemp528 = std::floor(fRec261[0]);
			double fTemp529 = std::min<double>(0.0001 * fRec261[0], 1.0);
			double fTemp530 = fRec261[0] + 1e+04;
			int iTemp531 = int(fTemp530);
			double fTemp532 = std::floor(fTemp530);
			double fTemp533 = std::tan(fConst5 * std::min<double>(fSlow82 * fRec22[0], 2e+04));
			double fTemp534 = 1.0 / fTemp533;
			double fTemp535 = std::tan(fConst5 * std::min<double>(fSlow82 * fRec24[0], 2e+04));
			double fTemp536 = 1.0 / fTemp535;
			double fTemp537 = (fTemp536 + 1.414213562373095) / fTemp535 + 1.0;
			fRec264[0] = double(input10[i0]) - (fRec264[2] * ((fTemp536 + -1.414213562373095) / fTemp535 + 1.0) + 2.0 * fRec264[1] * (1.0 - 1.0 / bbdmi_multi_granulator14_faustpower2_f(fTemp535))) / fTemp537;
			double fTemp538 = (fRec264[2] + fRec264[0] + 2.0 * fRec264[1]) / fTemp537;
			fVec86[0] = fTemp538;
			fRec263[0] = -((fRec263[1] * (1.0 - fTemp534) - (fTemp538 - fVec86[1]) / fTemp533) / (fTemp534 + 1.0));
			double fTemp539 = fRec263[0] + 0.6 * fRec2[0] * fRec262[1];
			fVec87[IOTA0 & 2097151] = fTemp539;
			double fTemp540 = fVec87[(IOTA0 - iTemp523) & 2097151];
			fRec262[0] = fTemp540 + fRec252[0] * (fVec87[(IOTA0 - iTemp525) & 2097151] - fTemp540);
			double fTemp541 = 0.0005 * fTemp517 * (1.0 - 0.5 * double(1 - iTemp518));
			int iTemp542 = std::fabs(fTemp541) < 2.220446049250313e-16;
			double fTemp543 = ((iTemp542) ? 0.0 : std::exp(-(fConst6 / ((iTemp542) ? 1.0 : fTemp541))));
			fRec265[0] = fTemp519 * (1.0 - fTemp543) + fTemp543 * fRec265[1];
			int iTemp544 = iVec84[(IOTA0 - int(std::min<double>(fConst3, std::max<double>(0.0, fConst4 * fTemp516 + -1.0)))) & 2097151];
			iVec88[0] = iTemp544;
			iRec268[0] = (iRec268[1] >= 1) * (((iTemp544 - iVec88[1]) == 1) == 0) + ((iTemp520 - iVec84[(IOTA0 - 1) & 2097151]) == 1);
			double fTemp545 = double(iRec258[0] * iRec268[0]);
			int iTemp546 = ((iRec268[0] != iRec268[1]) ? int(fConst7 * fTemp545) : iRec266[1] + -1);
			iRec266[0] = iTemp546;
			double fTemp547 = double(iRec268[0]);
			fRec267[0] = ((iTemp546 > 0) ? fRec267[1] + (fTemp547 - fRec267[1]) / double(iTemp546) : fTemp547);
			double fTemp548 = 0.0005 * fTemp545;
			int iTemp549 = std::fabs(fTemp548) < 2.220446049250313e-16;
			double fTemp550 = ((iTemp549) ? 0.0 : std::exp(-(fConst6 / ((iTemp549) ? 1.0 : fTemp548))));
			fRec269[0] = fTemp547 * (1.0 - fTemp550) + fTemp550 * fRec269[1];
			double fTemp551 = fRec269[0] * fTemp41 + fRec33[0] * bbdmi_multi_granulator14_faustpower3_f(fRec267[0]);
			fRec270[0] = ((iTemp520) ? fRec38[0] : fRec270[1]);
			double fTemp552 = 6.283185307179586 * fTemp551 * std::min<double>(fSlow82 * fRec270[0], 2e+04) * (fSlow84 * fRec31[0] * fTemp551 + 1.0);
			double fTemp553 = fTemp46 * std::cos(fTemp552) + fTemp48 * std::sin(fTemp552);
			double fTemp554 = fTemp552 + 1.5707963267948966;
			double fTemp555 = fTemp46 * std::cos(fTemp554) + fTemp48 * std::sin(fTemp554);
			fVec89[0] = fTemp555;
			double fTemp556 = fTemp555 - fVec89[1];
			double fTemp557 = double((fTemp553 > 0.0) - (fTemp553 < 0.0));
			double fTemp558 = fSlow83 * fRec25[0] * fRec262[0] * (fRec27[0] * fRec265[0] + fTemp32 * std::pow(2.718281828459045, -(5.0 * bbdmi_multi_granulator14_faustpower2_f(2.0 * fRec267[0] + -1.0)))) * (1.0 - 0.5 * fRec31[0] * (fRec31[0] + fTemp37 * fTemp551) * (fTemp57 * (fTemp55 * (fTemp43 * fTemp553 + std::min<double>(0.6366197723675814 * fRec34[0] * ((std::fabs(fTemp556) <= fConst6) ? std::asin(std::max<double>(-1.0, std::min<double>(1.0, 0.5 * (fTemp555 + fVec89[1])))) : (fTemp555 * std::asin(std::max<double>(-1.0, std::min<double>(1.0, fTemp555))) + std::sqrt(std::max<double>(0.0, 1.0 - bbdmi_multi_granulator14_faustpower2_f(fTemp555))) - (fVec89[1] * std::asin(std::max<double>(-1.0, std::min<double>(1.0, fVec89[1]))) + std::sqrt(std::max<double>(0.0, 1.0 - bbdmi_multi_granulator14_faustpower2_f(fVec89[1]))))) / fTemp556) * fTemp557, 1.0)) + fTemp54 * fTemp557) + fTemp56 * fTemp553 + 1.0));
			fVec90[IOTA0 & 131071] = fTemp558;
			fRec271[0] = std::fmod(fRec271[1] + (10001.0 - std::pow(2.0, 0.08333333333333333 * std::min<double>(fSlow82 * fRec41[0], 48.0))), 1e+04);
			int iTemp559 = int(fRec271[0]);
			double fTemp560 = std::floor(fRec271[0]);
			double fTemp561 = std::min<double>(0.0001 * fRec271[0], 1.0);
			double fTemp562 = fRec271[0] + 1e+04;
			int iTemp563 = int(fTemp562);
			double fTemp564 = std::floor(fTemp562);
			fRec250[IOTA0 & 2097151] = 0.6 * ((fVec85[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp527))) & 131071] * (fTemp528 + (1.0 - fRec261[0])) + (fRec261[0] - fTemp528) * fVec85[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp527 + 1))) & 131071]) * fTemp529 + (fVec85[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp531))) & 131071] * (fTemp532 + (-9999.0 - fRec261[0])) + (fRec261[0] + (1e+04 - fTemp532)) * fVec85[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp531 + 1))) & 131071]) * (1.0 - fTemp529)) + (fVec90[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp559))) & 131071] * (fTemp560 + (1.0 - fRec271[0])) + (fRec271[0] - fTemp560) * fVec90[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp559 + 1))) & 131071]) * fTemp561 + (fVec90[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp563))) & 131071] * (fTemp564 + (-9999.0 - fRec271[0])) + (fRec271[0] + (1e+04 - fTemp564)) * fVec90[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp563 + 1))) & 131071]) * (1.0 - fTemp561);
			fRec249[0] = fRec250[IOTA0 & 2097151] + 2.6014143622661517e-16 * fRec249[1] - 0.17157287525381 * fRec249[2];
			output10[i0] = FAUSTFLOAT(0.5857864376269049 * fRec249[1] + 0.2928932188134524 * (fRec249[0] + fRec249[2]));
			double fTemp565 = std::tan(fConst5 * std::min<double>(fSlow87 * fRec22[0], 2e+04));
			double fTemp566 = 1.0 / fTemp565;
			double fTemp567 = std::tan(fConst5 * std::min<double>(fSlow87 * fRec24[0], 2e+04));
			double fTemp568 = 1.0 / fTemp567;
			double fTemp569 = (fTemp568 + 1.414213562373095) / fTemp567 + 1.0;
			fRec276[0] = double(input11[i0]) - (fRec276[2] * ((fTemp568 + -1.414213562373095) / fTemp567 + 1.0) + 2.0 * fRec276[1] * (1.0 - 1.0 / bbdmi_multi_granulator14_faustpower2_f(fTemp567))) / fTemp569;
			double fTemp570 = (fRec276[2] + fRec276[0] + 2.0 * fRec276[1]) / fTemp569;
			fVec91[0] = fTemp570;
			fRec275[0] = -((fRec275[1] * (1.0 - fTemp566) - (fTemp570 - fVec91[1]) / fTemp565) / (fTemp566 + 1.0));
			double fTemp571 = fRec275[0] + 0.6 * fRec2[0] * fRec274[1];
			fVec92[IOTA0 & 2097151] = fTemp571;
			iRec285[0] = 1410065407 * iRec285[1] + 90011;
			double fTemp572 = 4.656612875245797e-10 * double(iRec285[0]) + 1.0;
			iRec284[0] = ((iTemp0) ? int(fRec11[0] + 0.5 * fTemp1 * fTemp572) : iRec284[1]);
			double fTemp573 = double(2 * iRec284[0]);
			fRec283[0] = ((iTemp0) ? fTemp573 + fRec12[0] * (2.0 - 0.5 * fRec15[0] * fTemp572) : fRec283[1]);
			iRec282[0] = (iVec0[1] + iRec282[1]) % int(fConst4 * fRec283[0]);
			double fTemp574 = double(iRec284[0]);
			int iTemp575 = iRec282[0] < int(fConst4 * fTemp574);
			iVec93[0] = iTemp575;
			double fTemp576 = double(iTemp575);
			int iTemp577 = (fTemp576 > 0.001) * (double(iVec93[1]) <= 0.001);
			iVec94[IOTA0 & 2097151] = iTemp577;
			iRec286[0] = 1410065407 * iRec286[1] + 90220;
			iRec281[0] = ((iTemp577) ? int(fConst4 * fRec17[0] * (1.0 - 0.5 * fRec15[0] * (4.656612875245797e-10 * double(iRec286[0]) + 1.0))) : iRec281[1]);
			double fTemp578 = fSlow87 * double(iRec281[0]);
			double fTemp579 = ((fRec277[1] != 0.0) ? (((fRec278[1] > 0.0) & (fRec278[1] < 1.0)) ? fRec277[1] : 0.0) : (((fRec278[1] == 0.0) & (fTemp578 != fRec279[1])) ? 0.0009765625 : (((fRec278[1] == 1.0) & (fTemp578 != fRec280[1])) ? -0.0009765625 : 0.0)));
			fRec277[0] = fTemp579;
			fRec278[0] = std::max<double>(0.0, std::min<double>(1.0, fRec278[1] + fTemp579));
			fRec279[0] = (((fRec278[1] >= 1.0) & (fRec280[1] != fTemp578)) ? fTemp578 : fRec279[1]);
			fRec280[0] = (((fRec278[1] <= 0.0) & (fRec279[1] != fTemp578)) ? fTemp578 : fRec280[1]);
			int iTemp580 = int(std::min<double>(fConst3, std::max<double>(0.0, fRec279[0])));
			double fTemp581 = fVec92[(IOTA0 - iTemp580) & 2097151];
			int iTemp582 = int(std::min<double>(fConst3, std::max<double>(0.0, fRec280[0])));
			fRec274[0] = fTemp581 + fRec278[0] * (fVec92[(IOTA0 - iTemp582) & 2097151] - fTemp581);
			double fTemp583 = 0.0005 * fTemp574 * (1.0 - 0.5 * double(1 - iTemp575));
			int iTemp584 = std::fabs(fTemp583) < 2.220446049250313e-16;
			double fTemp585 = ((iTemp584) ? 0.0 : std::exp(-(fConst6 / ((iTemp584) ? 1.0 : fTemp583))));
			fRec287[0] = fTemp576 * (1.0 - fTemp585) + fTemp585 * fRec287[1];
			int iTemp586 = iVec94[(IOTA0 - int(std::min<double>(fConst3, std::max<double>(0.0, fConst4 * fTemp573 + -1.0)))) & 2097151];
			iVec95[0] = iTemp586;
			iRec290[0] = (iRec290[1] >= 1) * (((iTemp586 - iVec95[1]) == 1) == 0) + ((iTemp577 - iVec94[(IOTA0 - 1) & 2097151]) == 1);
			double fTemp587 = double(iRec284[0] * iRec290[0]);
			int iTemp588 = ((iRec290[0] != iRec290[1]) ? int(fConst7 * fTemp587) : iRec288[1] + -1);
			iRec288[0] = iTemp588;
			double fTemp589 = double(iRec290[0]);
			fRec289[0] = ((iTemp588 > 0) ? fRec289[1] + (fTemp589 - fRec289[1]) / double(iTemp588) : fTemp589);
			double fTemp590 = 0.0005 * fTemp587;
			int iTemp591 = std::fabs(fTemp590) < 2.220446049250313e-16;
			double fTemp592 = ((iTemp591) ? 0.0 : std::exp(-(fConst6 / ((iTemp591) ? 1.0 : fTemp590))));
			fRec291[0] = fTemp589 * (1.0 - fTemp592) + fTemp592 * fRec291[1];
			double fTemp593 = fRec291[0] * fTemp41 + fRec33[0] * bbdmi_multi_granulator14_faustpower3_f(fRec289[0]);
			fRec292[0] = ((iTemp577) ? fRec38[0] : fRec292[1]);
			double fTemp594 = 6.283185307179586 * fTemp593 * std::min<double>(fSlow87 * fRec292[0], 2e+04) * (fSlow88 * fRec31[0] * fTemp593 + 1.0);
			double fTemp595 = fTemp46 * std::cos(fTemp594) + fTemp48 * std::sin(fTemp594);
			double fTemp596 = fTemp594 + 1.5707963267948966;
			double fTemp597 = fTemp46 * std::cos(fTemp596) + fTemp48 * std::sin(fTemp596);
			fVec96[0] = fTemp597;
			double fTemp598 = fTemp597 - fVec96[1];
			double fTemp599 = double((fTemp595 > 0.0) - (fTemp595 < 0.0));
			double fTemp600 = fSlow86 * fRec25[0] * fRec274[0] * (fRec27[0] * fRec287[0] + fTemp32 * std::pow(2.718281828459045, -(5.0 * bbdmi_multi_granulator14_faustpower2_f(2.0 * fRec289[0] + -1.0)))) * (1.0 - 0.5 * fRec31[0] * (fRec31[0] + fTemp37 * fTemp593) * (fTemp57 * (fTemp55 * (fTemp43 * fTemp595 + std::min<double>(0.6366197723675814 * fRec34[0] * ((std::fabs(fTemp598) <= fConst6) ? std::asin(std::max<double>(-1.0, std::min<double>(1.0, 0.5 * (fTemp597 + fVec96[1])))) : (fTemp597 * std::asin(std::max<double>(-1.0, std::min<double>(1.0, fTemp597))) + std::sqrt(std::max<double>(0.0, 1.0 - bbdmi_multi_granulator14_faustpower2_f(fTemp597))) - (fVec96[1] * std::asin(std::max<double>(-1.0, std::min<double>(1.0, fVec96[1]))) + std::sqrt(std::max<double>(0.0, 1.0 - bbdmi_multi_granulator14_faustpower2_f(fVec96[1]))))) / fTemp598) * fTemp599, 1.0)) + fTemp54 * fTemp599) + fTemp56 * fTemp595 + 1.0));
			fVec97[IOTA0 & 131071] = fTemp600;
			fRec293[0] = std::fmod(fRec293[1] + (10001.0 - std::pow(2.0, 0.08333333333333333 * std::min<double>(fSlow87 * fRec41[0], 48.0))), 1e+04);
			double fTemp601 = fRec293[0] + 1e+04;
			int iTemp602 = int(fTemp601);
			double fTemp603 = std::floor(fTemp601);
			double fTemp604 = std::min<double>(0.0001 * fRec293[0], 1.0);
			double fTemp605 = fRec273[(IOTA0 - (iTemp580 + 1)) & 2097151];
			double fTemp606 = fRec2[0] * (fTemp605 + fRec278[0] * (fRec273[(IOTA0 - (iTemp582 + 1)) & 2097151] - fTemp605));
			fVec98[IOTA0 & 131071] = fTemp606;
			fRec294[0] = std::fmod(fRec294[1] + (10001.0 - std::pow(2.0, 0.08333333333333333 * std::min<double>(fSlow87 * fRec19[0], 48.0))), 1e+04);
			int iTemp607 = int(fRec294[0]);
			double fTemp608 = std::floor(fRec294[0]);
			double fTemp609 = std::min<double>(0.0001 * fRec294[0], 1.0);
			double fTemp610 = fRec294[0] + 1e+04;
			int iTemp611 = int(fTemp610);
			double fTemp612 = std::floor(fTemp610);
			int iTemp613 = int(fRec293[0]);
			double fTemp614 = std::floor(fRec293[0]);
			fRec273[IOTA0 & 2097151] = (fVec97[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp602))) & 131071] * (fTemp603 + (-9999.0 - fRec293[0])) + (fRec293[0] + (1e+04 - fTemp603)) * fVec97[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp602 + 1))) & 131071]) * (1.0 - fTemp604) + 0.6 * ((fVec98[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp607))) & 131071] * (fTemp608 + (1.0 - fRec294[0])) + (fRec294[0] - fTemp608) * fVec98[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp607 + 1))) & 131071]) * fTemp609 + (fVec98[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp611))) & 131071] * (fTemp612 + (-9999.0 - fRec294[0])) + (fRec294[0] + (1e+04 - fTemp612)) * fVec98[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp611 + 1))) & 131071]) * (1.0 - fTemp609)) + (fVec97[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp613))) & 131071] * (fTemp614 + (1.0 - fRec293[0])) + (fRec293[0] - fTemp614) * fVec97[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp613 + 1))) & 131071]) * fTemp604;
			fRec272[0] = fRec273[IOTA0 & 2097151] + 2.6014143622661517e-16 * fRec272[1] - 0.17157287525381 * fRec272[2];
			output11[i0] = FAUSTFLOAT(0.5857864376269049 * fRec272[1] + 0.2928932188134524 * (fRec272[0] + fRec272[2]));
			double fTemp615 = std::tan(fConst5 * std::min<double>(fSlow91 * fRec22[0], 2e+04));
			double fTemp616 = 1.0 / fTemp615;
			double fTemp617 = std::tan(fConst5 * std::min<double>(fSlow91 * fRec24[0], 2e+04));
			double fTemp618 = 1.0 / fTemp617;
			double fTemp619 = (fTemp618 + 1.414213562373095) / fTemp617 + 1.0;
			fRec299[0] = double(input12[i0]) - (fRec299[2] * ((fTemp618 + -1.414213562373095) / fTemp617 + 1.0) + 2.0 * fRec299[1] * (1.0 - 1.0 / bbdmi_multi_granulator14_faustpower2_f(fTemp617))) / fTemp619;
			double fTemp620 = (fRec299[2] + fRec299[0] + 2.0 * fRec299[1]) / fTemp619;
			fVec99[0] = fTemp620;
			fRec298[0] = -((fRec298[1] * (1.0 - fTemp616) - (fTemp620 - fVec99[1]) / fTemp615) / (fTemp616 + 1.0));
			double fTemp621 = fRec298[0] + 0.6 * fRec2[0] * fRec297[1];
			fVec100[IOTA0 & 2097151] = fTemp621;
			iRec308[0] = 1410065407 * iRec308[1] + 90012;
			double fTemp622 = 4.656612875245797e-10 * double(iRec308[0]) + 1.0;
			iRec307[0] = ((iTemp0) ? int(fRec11[0] + 0.5 * fTemp1 * fTemp622) : iRec307[1]);
			double fTemp623 = double(2 * iRec307[0]);
			fRec306[0] = ((iTemp0) ? fTemp623 + fRec12[0] * (2.0 - 0.5 * fRec15[0] * fTemp622) : fRec306[1]);
			iRec305[0] = (iVec0[1] + iRec305[1]) % int(fConst4 * fRec306[0]);
			double fTemp624 = double(iRec307[0]);
			int iTemp625 = iRec305[0] < int(fConst4 * fTemp624);
			iVec101[0] = iTemp625;
			double fTemp626 = double(iTemp625);
			int iTemp627 = (fTemp626 > 0.001) * (double(iVec101[1]) <= 0.001);
			iVec102[IOTA0 & 2097151] = iTemp627;
			iRec309[0] = 1410065407 * iRec309[1] + 90240;
			iRec304[0] = ((iTemp627) ? int(fConst4 * fRec17[0] * (1.0 - 0.5 * fRec15[0] * (4.656612875245797e-10 * double(iRec309[0]) + 1.0))) : iRec304[1]);
			double fTemp628 = fSlow91 * double(iRec304[0]);
			double fTemp629 = ((fRec300[1] != 0.0) ? (((fRec301[1] > 0.0) & (fRec301[1] < 1.0)) ? fRec300[1] : 0.0) : (((fRec301[1] == 0.0) & (fTemp628 != fRec302[1])) ? 0.0009765625 : (((fRec301[1] == 1.0) & (fTemp628 != fRec303[1])) ? -0.0009765625 : 0.0)));
			fRec300[0] = fTemp629;
			fRec301[0] = std::max<double>(0.0, std::min<double>(1.0, fRec301[1] + fTemp629));
			fRec302[0] = (((fRec301[1] >= 1.0) & (fRec303[1] != fTemp628)) ? fTemp628 : fRec302[1]);
			fRec303[0] = (((fRec301[1] <= 0.0) & (fRec302[1] != fTemp628)) ? fTemp628 : fRec303[1]);
			int iTemp630 = int(std::min<double>(fConst3, std::max<double>(0.0, fRec302[0])));
			double fTemp631 = fVec100[(IOTA0 - iTemp630) & 2097151];
			int iTemp632 = int(std::min<double>(fConst3, std::max<double>(0.0, fRec303[0])));
			fRec297[0] = fTemp631 + fRec301[0] * (fVec100[(IOTA0 - iTemp632) & 2097151] - fTemp631);
			double fTemp633 = 0.0005 * fTemp624 * (1.0 - 0.5 * double(1 - iTemp625));
			int iTemp634 = std::fabs(fTemp633) < 2.220446049250313e-16;
			double fTemp635 = ((iTemp634) ? 0.0 : std::exp(-(fConst6 / ((iTemp634) ? 1.0 : fTemp633))));
			fRec310[0] = fTemp626 * (1.0 - fTemp635) + fTemp635 * fRec310[1];
			int iTemp636 = iVec102[(IOTA0 - int(std::min<double>(fConst3, std::max<double>(0.0, fConst4 * fTemp623 + -1.0)))) & 2097151];
			iVec103[0] = iTemp636;
			iRec313[0] = (iRec313[1] >= 1) * (((iTemp636 - iVec103[1]) == 1) == 0) + ((iTemp627 - iVec102[(IOTA0 - 1) & 2097151]) == 1);
			double fTemp637 = double(iRec307[0] * iRec313[0]);
			int iTemp638 = ((iRec313[0] != iRec313[1]) ? int(fConst7 * fTemp637) : iRec311[1] + -1);
			iRec311[0] = iTemp638;
			double fTemp639 = double(iRec313[0]);
			fRec312[0] = ((iTemp638 > 0) ? fRec312[1] + (fTemp639 - fRec312[1]) / double(iTemp638) : fTemp639);
			double fTemp640 = 0.0005 * fTemp637;
			int iTemp641 = std::fabs(fTemp640) < 2.220446049250313e-16;
			double fTemp642 = ((iTemp641) ? 0.0 : std::exp(-(fConst6 / ((iTemp641) ? 1.0 : fTemp640))));
			fRec314[0] = fTemp639 * (1.0 - fTemp642) + fTemp642 * fRec314[1];
			double fTemp643 = fRec314[0] * fTemp41 + fRec33[0] * bbdmi_multi_granulator14_faustpower3_f(fRec312[0]);
			fRec315[0] = ((iTemp627) ? fRec38[0] : fRec315[1]);
			double fTemp644 = 6.283185307179586 * fTemp643 * std::min<double>(fSlow91 * fRec315[0], 2e+04) * (fSlow92 * fRec31[0] * fTemp643 + 1.0);
			double fTemp645 = fTemp46 * std::cos(fTemp644) + fTemp48 * std::sin(fTemp644);
			double fTemp646 = fTemp644 + 1.5707963267948966;
			double fTemp647 = fTemp46 * std::cos(fTemp646) + fTemp48 * std::sin(fTemp646);
			fVec104[0] = fTemp647;
			double fTemp648 = fTemp647 - fVec104[1];
			double fTemp649 = double((fTemp645 > 0.0) - (fTemp645 < 0.0));
			double fTemp650 = fSlow90 * fRec25[0] * fRec297[0] * (fRec27[0] * fRec310[0] + fTemp32 * std::pow(2.718281828459045, -(5.0 * bbdmi_multi_granulator14_faustpower2_f(2.0 * fRec312[0] + -1.0)))) * (1.0 - 0.5 * fRec31[0] * (fRec31[0] + fTemp37 * fTemp643) * (fTemp57 * (fTemp55 * (fTemp43 * fTemp645 + std::min<double>(0.6366197723675814 * fRec34[0] * ((std::fabs(fTemp648) <= fConst6) ? std::asin(std::max<double>(-1.0, std::min<double>(1.0, 0.5 * (fTemp647 + fVec104[1])))) : (fTemp647 * std::asin(std::max<double>(-1.0, std::min<double>(1.0, fTemp647))) + std::sqrt(std::max<double>(0.0, 1.0 - bbdmi_multi_granulator14_faustpower2_f(fTemp647))) - (fVec104[1] * std::asin(std::max<double>(-1.0, std::min<double>(1.0, fVec104[1]))) + std::sqrt(std::max<double>(0.0, 1.0 - bbdmi_multi_granulator14_faustpower2_f(fVec104[1]))))) / fTemp648) * fTemp649, 1.0)) + fTemp54 * fTemp649) + fTemp56 * fTemp645 + 1.0));
			fVec105[IOTA0 & 131071] = fTemp650;
			fRec316[0] = std::fmod(fRec316[1] + (10001.0 - std::pow(2.0, 0.08333333333333333 * std::min<double>(fSlow91 * fRec41[0], 48.0))), 1e+04);
			double fTemp651 = fRec316[0] + 1e+04;
			int iTemp652 = int(fTemp651);
			double fTemp653 = std::floor(fTemp651);
			double fTemp654 = std::min<double>(0.0001 * fRec316[0], 1.0);
			double fTemp655 = fRec296[(IOTA0 - (iTemp630 + 1)) & 2097151];
			double fTemp656 = fRec2[0] * (fTemp655 + fRec301[0] * (fRec296[(IOTA0 - (iTemp632 + 1)) & 2097151] - fTemp655));
			fVec106[IOTA0 & 131071] = fTemp656;
			fRec317[0] = std::fmod(fRec317[1] + (10001.0 - std::pow(2.0, 0.08333333333333333 * std::min<double>(fSlow91 * fRec19[0], 48.0))), 1e+04);
			int iTemp657 = int(fRec317[0]);
			double fTemp658 = std::floor(fRec317[0]);
			double fTemp659 = std::min<double>(0.0001 * fRec317[0], 1.0);
			double fTemp660 = fRec317[0] + 1e+04;
			int iTemp661 = int(fTemp660);
			double fTemp662 = std::floor(fTemp660);
			int iTemp663 = int(fRec316[0]);
			double fTemp664 = std::floor(fRec316[0]);
			fRec296[IOTA0 & 2097151] = (fVec105[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp652))) & 131071] * (fTemp653 + (-9999.0 - fRec316[0])) + (fRec316[0] + (1e+04 - fTemp653)) * fVec105[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp652 + 1))) & 131071]) * (1.0 - fTemp654) + 0.6 * ((fVec106[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp657))) & 131071] * (fTemp658 + (1.0 - fRec317[0])) + (fRec317[0] - fTemp658) * fVec106[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp657 + 1))) & 131071]) * fTemp659 + (fVec106[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp661))) & 131071] * (fTemp662 + (-9999.0 - fRec317[0])) + (fRec317[0] + (1e+04 - fTemp662)) * fVec106[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp661 + 1))) & 131071]) * (1.0 - fTemp659)) + (fVec105[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp663))) & 131071] * (fTemp664 + (1.0 - fRec316[0])) + (fRec316[0] - fTemp664) * fVec105[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp663 + 1))) & 131071]) * fTemp654;
			fRec295[0] = fRec296[IOTA0 & 2097151] + 2.6014143622661517e-16 * fRec295[1] - 0.17157287525381 * fRec295[2];
			output12[i0] = FAUSTFLOAT(0.5857864376269049 * fRec295[1] + 0.2928932188134524 * (fRec295[0] + fRec295[2]));
			double fTemp665 = std::tan(fConst5 * std::min<double>(fSlow95 * fRec22[0], 2e+04));
			double fTemp666 = 1.0 / fTemp665;
			double fTemp667 = std::tan(fConst5 * std::min<double>(fSlow95 * fRec24[0], 2e+04));
			double fTemp668 = 1.0 / fTemp667;
			double fTemp669 = (fTemp668 + 1.414213562373095) / fTemp667 + 1.0;
			fRec322[0] = double(input13[i0]) - (fRec322[2] * ((fTemp668 + -1.414213562373095) / fTemp667 + 1.0) + 2.0 * fRec322[1] * (1.0 - 1.0 / bbdmi_multi_granulator14_faustpower2_f(fTemp667))) / fTemp669;
			double fTemp670 = (fRec322[2] + fRec322[0] + 2.0 * fRec322[1]) / fTemp669;
			fVec107[0] = fTemp670;
			fRec321[0] = -((fRec321[1] * (1.0 - fTemp666) - (fTemp670 - fVec107[1]) / fTemp665) / (fTemp666 + 1.0));
			double fTemp671 = fRec321[0] + 0.6 * fRec2[0] * fRec320[1];
			fVec108[IOTA0 & 2097151] = fTemp671;
			iRec331[0] = 1410065407 * iRec331[1] + 90013;
			double fTemp672 = 4.656612875245797e-10 * double(iRec331[0]) + 1.0;
			iRec330[0] = ((iTemp0) ? int(fRec11[0] + 0.5 * fTemp1 * fTemp672) : iRec330[1]);
			double fTemp673 = double(2 * iRec330[0]);
			fRec329[0] = ((iTemp0) ? fTemp673 + fRec12[0] * (2.0 - 0.5 * fRec15[0] * fTemp672) : fRec329[1]);
			iRec328[0] = (iVec0[1] + iRec328[1]) % int(fConst4 * fRec329[0]);
			double fTemp674 = double(iRec330[0]);
			int iTemp675 = iRec328[0] < int(fConst4 * fTemp674);
			iVec109[0] = iTemp675;
			double fTemp676 = double(iTemp675);
			int iTemp677 = (fTemp676 > 0.001) * (double(iVec109[1]) <= 0.001);
			iVec110[IOTA0 & 2097151] = iTemp677;
			iRec332[0] = 1410065407 * iRec332[1] + 90260;
			iRec327[0] = ((iTemp677) ? int(fConst4 * fRec17[0] * (1.0 - 0.5 * fRec15[0] * (4.656612875245797e-10 * double(iRec332[0]) + 1.0))) : iRec327[1]);
			double fTemp678 = fSlow95 * double(iRec327[0]);
			double fTemp679 = ((fRec323[1] != 0.0) ? (((fRec324[1] > 0.0) & (fRec324[1] < 1.0)) ? fRec323[1] : 0.0) : (((fRec324[1] == 0.0) & (fTemp678 != fRec325[1])) ? 0.0009765625 : (((fRec324[1] == 1.0) & (fTemp678 != fRec326[1])) ? -0.0009765625 : 0.0)));
			fRec323[0] = fTemp679;
			fRec324[0] = std::max<double>(0.0, std::min<double>(1.0, fRec324[1] + fTemp679));
			fRec325[0] = (((fRec324[1] >= 1.0) & (fRec326[1] != fTemp678)) ? fTemp678 : fRec325[1]);
			fRec326[0] = (((fRec324[1] <= 0.0) & (fRec325[1] != fTemp678)) ? fTemp678 : fRec326[1]);
			int iTemp680 = int(std::min<double>(fConst3, std::max<double>(0.0, fRec325[0])));
			double fTemp681 = fVec108[(IOTA0 - iTemp680) & 2097151];
			int iTemp682 = int(std::min<double>(fConst3, std::max<double>(0.0, fRec326[0])));
			fRec320[0] = fTemp681 + fRec324[0] * (fVec108[(IOTA0 - iTemp682) & 2097151] - fTemp681);
			double fTemp683 = 0.0005 * fTemp674 * (1.0 - 0.5 * double(1 - iTemp675));
			int iTemp684 = std::fabs(fTemp683) < 2.220446049250313e-16;
			double fTemp685 = ((iTemp684) ? 0.0 : std::exp(-(fConst6 / ((iTemp684) ? 1.0 : fTemp683))));
			fRec333[0] = fTemp676 * (1.0 - fTemp685) + fTemp685 * fRec333[1];
			int iTemp686 = iVec110[(IOTA0 - int(std::min<double>(fConst3, std::max<double>(0.0, fConst4 * fTemp673 + -1.0)))) & 2097151];
			iVec111[0] = iTemp686;
			iRec336[0] = (iRec336[1] >= 1) * (((iTemp686 - iVec111[1]) == 1) == 0) + ((iTemp677 - iVec110[(IOTA0 - 1) & 2097151]) == 1);
			double fTemp687 = double(iRec330[0] * iRec336[0]);
			int iTemp688 = ((iRec336[0] != iRec336[1]) ? int(fConst7 * fTemp687) : iRec334[1] + -1);
			iRec334[0] = iTemp688;
			double fTemp689 = double(iRec336[0]);
			fRec335[0] = ((iTemp688 > 0) ? fRec335[1] + (fTemp689 - fRec335[1]) / double(iTemp688) : fTemp689);
			double fTemp690 = 0.0005 * fTemp687;
			int iTemp691 = std::fabs(fTemp690) < 2.220446049250313e-16;
			double fTemp692 = ((iTemp691) ? 0.0 : std::exp(-(fConst6 / ((iTemp691) ? 1.0 : fTemp690))));
			fRec337[0] = fTemp689 * (1.0 - fTemp692) + fTemp692 * fRec337[1];
			double fTemp693 = fRec337[0] * fTemp41 + fRec33[0] * bbdmi_multi_granulator14_faustpower3_f(fRec335[0]);
			fRec338[0] = ((iTemp677) ? fRec38[0] : fRec338[1]);
			double fTemp694 = 6.283185307179586 * fTemp693 * std::min<double>(fSlow95 * fRec338[0], 2e+04) * (fSlow96 * fRec31[0] * fTemp693 + 1.0);
			double fTemp695 = fTemp46 * std::cos(fTemp694) + fTemp48 * std::sin(fTemp694);
			double fTemp696 = fTemp694 + 1.5707963267948966;
			double fTemp697 = fTemp46 * std::cos(fTemp696) + fTemp48 * std::sin(fTemp696);
			fVec112[0] = fTemp697;
			double fTemp698 = fTemp697 - fVec112[1];
			double fTemp699 = double((fTemp695 > 0.0) - (fTemp695 < 0.0));
			double fTemp700 = fSlow94 * fRec25[0] * fRec320[0] * (fRec27[0] * fRec333[0] + fTemp32 * std::pow(2.718281828459045, -(5.0 * bbdmi_multi_granulator14_faustpower2_f(2.0 * fRec335[0] + -1.0)))) * (1.0 - 0.5 * fRec31[0] * (fRec31[0] + fTemp37 * fTemp693) * (fTemp57 * (fTemp55 * (fTemp43 * fTemp695 + std::min<double>(0.6366197723675814 * fRec34[0] * ((std::fabs(fTemp698) <= fConst6) ? std::asin(std::max<double>(-1.0, std::min<double>(1.0, 0.5 * (fTemp697 + fVec112[1])))) : (fTemp697 * std::asin(std::max<double>(-1.0, std::min<double>(1.0, fTemp697))) + std::sqrt(std::max<double>(0.0, 1.0 - bbdmi_multi_granulator14_faustpower2_f(fTemp697))) - (fVec112[1] * std::asin(std::max<double>(-1.0, std::min<double>(1.0, fVec112[1]))) + std::sqrt(std::max<double>(0.0, 1.0 - bbdmi_multi_granulator14_faustpower2_f(fVec112[1]))))) / fTemp698) * fTemp699, 1.0)) + fTemp54 * fTemp699) + fTemp56 * fTemp695 + 1.0));
			fVec113[IOTA0 & 131071] = fTemp700;
			fRec339[0] = std::fmod(fRec339[1] + (10001.0 - std::pow(2.0, 0.08333333333333333 * std::min<double>(fSlow95 * fRec41[0], 48.0))), 1e+04);
			double fTemp701 = fRec339[0] + 1e+04;
			int iTemp702 = int(fTemp701);
			double fTemp703 = std::floor(fTemp701);
			double fTemp704 = std::min<double>(0.0001 * fRec339[0], 1.0);
			double fTemp705 = fRec319[(IOTA0 - (iTemp680 + 1)) & 2097151];
			double fTemp706 = fRec2[0] * (fTemp705 + fRec324[0] * (fRec319[(IOTA0 - (iTemp682 + 1)) & 2097151] - fTemp705));
			fVec114[IOTA0 & 131071] = fTemp706;
			fRec340[0] = std::fmod(fRec340[1] + (10001.0 - std::pow(2.0, 0.08333333333333333 * std::min<double>(fSlow95 * fRec19[0], 48.0))), 1e+04);
			int iTemp707 = int(fRec340[0]);
			double fTemp708 = std::floor(fRec340[0]);
			double fTemp709 = std::min<double>(0.0001 * fRec340[0], 1.0);
			double fTemp710 = fRec340[0] + 1e+04;
			int iTemp711 = int(fTemp710);
			double fTemp712 = std::floor(fTemp710);
			int iTemp713 = int(fRec339[0]);
			double fTemp714 = std::floor(fRec339[0]);
			fRec319[IOTA0 & 2097151] = (fVec113[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp702))) & 131071] * (fTemp703 + (-9999.0 - fRec339[0])) + (fRec339[0] + (1e+04 - fTemp703)) * fVec113[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp702 + 1))) & 131071]) * (1.0 - fTemp704) + 0.6 * ((fVec114[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp707))) & 131071] * (fTemp708 + (1.0 - fRec340[0])) + (fRec340[0] - fTemp708) * fVec114[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp707 + 1))) & 131071]) * fTemp709 + (fVec114[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp711))) & 131071] * (fTemp712 + (-9999.0 - fRec340[0])) + (fRec340[0] + (1e+04 - fTemp712)) * fVec114[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp711 + 1))) & 131071]) * (1.0 - fTemp709)) + (fVec113[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp713))) & 131071] * (fTemp714 + (1.0 - fRec339[0])) + (fRec339[0] - fTemp714) * fVec113[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp713 + 1))) & 131071]) * fTemp704;
			fRec318[0] = fRec319[IOTA0 & 2097151] + 2.6014143622661517e-16 * fRec318[1] - 0.17157287525381 * fRec318[2];
			output13[i0] = FAUSTFLOAT(0.5857864376269049 * fRec318[1] + 0.2928932188134524 * (fRec318[0] + fRec318[2]));
			iVec0[1] = iVec0[0];
			fRec2[1] = fRec2[0];
			IOTA0 = IOTA0 + 1;
			fRec11[1] = fRec11[0];
			fRec12[1] = fRec12[0];
			iRec10[1] = iRec10[0];
			fRec14[1] = fRec14[0];
			fRec15[1] = fRec15[0];
			iRec16[1] = iRec16[0];
			iRec13[1] = iRec13[0];
			fRec9[1] = fRec9[0];
			iRec8[1] = iRec8[0];
			iVec1[1] = iVec1[0];
			fRec17[1] = fRec17[0];
			iRec7[1] = iRec7[0];
			fRec3[1] = fRec3[0];
			fRec4[1] = fRec4[0];
			fRec5[1] = fRec5[0];
			fRec6[1] = fRec6[0];
			fRec19[1] = fRec19[0];
			fRec18[1] = fRec18[0];
			fRec22[1] = fRec22[0];
			fRec24[1] = fRec24[0];
			fRec23[2] = fRec23[1];
			fRec23[1] = fRec23[0];
			fVec4[1] = fVec4[0];
			fRec21[1] = fRec21[0];
			fRec20[1] = fRec20[0];
			fRec25[1] = fRec25[0];
			fRec26[1] = fRec26[0];
			fRec27[1] = fRec27[0];
			iVec6[1] = iVec6[0];
			iRec30[1] = iRec30[0];
			iRec28[1] = iRec28[0];
			fRec29[1] = fRec29[0];
			fRec31[1] = fRec31[0];
			fRec32[1] = fRec32[0];
			fRec33[1] = fRec33[0];
			fRec34[1] = fRec34[0];
			fRec36[1] = fRec36[0];
			fRec38[1] = fRec38[0];
			fRec37[1] = fRec37[0];
			fVec9[1] = fVec9[0];
			fRec41[1] = fRec41[0];
			fRec40[1] = fRec40[0];
			fRec0[2] = fRec0[1];
			fRec0[1] = fRec0[0];
			iRec52[1] = iRec52[0];
			iRec51[1] = iRec51[0];
			fRec50[1] = fRec50[0];
			iRec49[1] = iRec49[0];
			iVec11[1] = iVec11[0];
			iRec53[1] = iRec53[0];
			iRec48[1] = iRec48[0];
			fRec44[1] = fRec44[0];
			fRec45[1] = fRec45[0];
			fRec46[1] = fRec46[0];
			fRec47[1] = fRec47[0];
			fRec54[1] = fRec54[0];
			fRec57[2] = fRec57[1];
			fRec57[1] = fRec57[0];
			fVec14[1] = fVec14[0];
			fRec56[1] = fRec56[0];
			fRec55[1] = fRec55[0];
			fRec58[1] = fRec58[0];
			iVec16[1] = iVec16[0];
			iRec61[1] = iRec61[0];
			iRec59[1] = iRec59[0];
			fRec60[1] = fRec60[0];
			fRec62[1] = fRec62[0];
			fRec63[1] = fRec63[0];
			fVec17[1] = fVec17[0];
			fRec64[1] = fRec64[0];
			fRec42[2] = fRec42[1];
			fRec42[1] = fRec42[0];
			iRec75[1] = iRec75[0];
			iRec74[1] = iRec74[0];
			fRec73[1] = fRec73[0];
			iRec72[1] = iRec72[0];
			iVec19[1] = iVec19[0];
			iRec76[1] = iRec76[0];
			iRec71[1] = iRec71[0];
			fRec67[1] = fRec67[0];
			fRec68[1] = fRec68[0];
			fRec69[1] = fRec69[0];
			fRec70[1] = fRec70[0];
			fRec77[1] = fRec77[0];
			fRec80[2] = fRec80[1];
			fRec80[1] = fRec80[0];
			fVec22[1] = fVec22[0];
			fRec79[1] = fRec79[0];
			fRec78[1] = fRec78[0];
			fRec81[1] = fRec81[0];
			iVec24[1] = iVec24[0];
			iRec84[1] = iRec84[0];
			iRec82[1] = iRec82[0];
			fRec83[1] = fRec83[0];
			fRec85[1] = fRec85[0];
			fRec86[1] = fRec86[0];
			fVec25[1] = fVec25[0];
			fRec87[1] = fRec87[0];
			fRec65[2] = fRec65[1];
			fRec65[1] = fRec65[0];
			fRec92[2] = fRec92[1];
			fRec92[1] = fRec92[0];
			fVec27[1] = fVec27[0];
			fRec91[1] = fRec91[0];
			iRec101[1] = iRec101[0];
			iRec100[1] = iRec100[0];
			fRec99[1] = fRec99[0];
			iRec98[1] = iRec98[0];
			iVec29[1] = iVec29[0];
			iRec102[1] = iRec102[0];
			iRec97[1] = iRec97[0];
			fRec93[1] = fRec93[0];
			fRec94[1] = fRec94[0];
			fRec95[1] = fRec95[0];
			fRec96[1] = fRec96[0];
			fRec90[1] = fRec90[0];
			fRec103[1] = fRec103[0];
			iVec31[1] = iVec31[0];
			iRec106[1] = iRec106[0];
			iRec104[1] = iRec104[0];
			fRec105[1] = fRec105[0];
			fRec107[1] = fRec107[0];
			fRec108[1] = fRec108[0];
			fVec32[1] = fVec32[0];
			fRec109[1] = fRec109[0];
			fRec110[1] = fRec110[0];
			fRec88[2] = fRec88[1];
			fRec88[1] = fRec88[0];
			fRec115[2] = fRec115[1];
			fRec115[1] = fRec115[0];
			fVec35[1] = fVec35[0];
			fRec114[1] = fRec114[0];
			iRec124[1] = iRec124[0];
			iRec123[1] = iRec123[0];
			fRec122[1] = fRec122[0];
			iRec121[1] = iRec121[0];
			iVec37[1] = iVec37[0];
			iRec125[1] = iRec125[0];
			iRec120[1] = iRec120[0];
			fRec116[1] = fRec116[0];
			fRec117[1] = fRec117[0];
			fRec118[1] = fRec118[0];
			fRec119[1] = fRec119[0];
			fRec113[1] = fRec113[0];
			fRec126[1] = fRec126[0];
			iVec39[1] = iVec39[0];
			iRec129[1] = iRec129[0];
			iRec127[1] = iRec127[0];
			fRec128[1] = fRec128[0];
			fRec130[1] = fRec130[0];
			fRec131[1] = fRec131[0];
			fVec40[1] = fVec40[0];
			fRec132[1] = fRec132[0];
			fRec133[1] = fRec133[0];
			fRec111[2] = fRec111[1];
			fRec111[1] = fRec111[0];
			iRec144[1] = iRec144[0];
			iRec143[1] = iRec143[0];
			fRec142[1] = fRec142[0];
			iRec141[1] = iRec141[0];
			iVec43[1] = iVec43[0];
			iRec145[1] = iRec145[0];
			iRec140[1] = iRec140[0];
			fRec136[1] = fRec136[0];
			fRec137[1] = fRec137[0];
			fRec138[1] = fRec138[0];
			fRec139[1] = fRec139[0];
			fRec146[1] = fRec146[0];
			fRec149[2] = fRec149[1];
			fRec149[1] = fRec149[0];
			fVec46[1] = fVec46[0];
			fRec148[1] = fRec148[0];
			fRec147[1] = fRec147[0];
			fRec150[1] = fRec150[0];
			iVec48[1] = iVec48[0];
			iRec153[1] = iRec153[0];
			iRec151[1] = iRec151[0];
			fRec152[1] = fRec152[0];
			fRec154[1] = fRec154[0];
			fRec155[1] = fRec155[0];
			fVec49[1] = fVec49[0];
			fRec156[1] = fRec156[0];
			fRec134[2] = fRec134[1];
			fRec134[1] = fRec134[0];
			iRec167[1] = iRec167[0];
			iRec166[1] = iRec166[0];
			fRec165[1] = fRec165[0];
			iRec164[1] = iRec164[0];
			iVec51[1] = iVec51[0];
			iRec168[1] = iRec168[0];
			iRec163[1] = iRec163[0];
			fRec159[1] = fRec159[0];
			fRec160[1] = fRec160[0];
			fRec161[1] = fRec161[0];
			fRec162[1] = fRec162[0];
			fRec169[1] = fRec169[0];
			fRec172[2] = fRec172[1];
			fRec172[1] = fRec172[0];
			fVec54[1] = fVec54[0];
			fRec171[1] = fRec171[0];
			fRec170[1] = fRec170[0];
			fRec173[1] = fRec173[0];
			iVec56[1] = iVec56[0];
			iRec176[1] = iRec176[0];
			iRec174[1] = iRec174[0];
			fRec175[1] = fRec175[0];
			fRec177[1] = fRec177[0];
			fRec178[1] = fRec178[0];
			fVec57[1] = fVec57[0];
			fRec179[1] = fRec179[0];
			fRec157[2] = fRec157[1];
			fRec157[1] = fRec157[0];
			fRec184[2] = fRec184[1];
			fRec184[1] = fRec184[0];
			fVec59[1] = fVec59[0];
			fRec183[1] = fRec183[0];
			iRec193[1] = iRec193[0];
			iRec192[1] = iRec192[0];
			fRec191[1] = fRec191[0];
			iRec190[1] = iRec190[0];
			iVec61[1] = iVec61[0];
			iRec194[1] = iRec194[0];
			iRec189[1] = iRec189[0];
			fRec185[1] = fRec185[0];
			fRec186[1] = fRec186[0];
			fRec187[1] = fRec187[0];
			fRec188[1] = fRec188[0];
			fRec182[1] = fRec182[0];
			fRec195[1] = fRec195[0];
			iVec63[1] = iVec63[0];
			iRec198[1] = iRec198[0];
			iRec196[1] = iRec196[0];
			fRec197[1] = fRec197[0];
			fRec199[1] = fRec199[0];
			fRec200[1] = fRec200[0];
			fVec64[1] = fVec64[0];
			fRec201[1] = fRec201[0];
			fRec202[1] = fRec202[0];
			fRec180[2] = fRec180[1];
			fRec180[1] = fRec180[0];
			fRec207[2] = fRec207[1];
			fRec207[1] = fRec207[0];
			fVec67[1] = fVec67[0];
			fRec206[1] = fRec206[0];
			iRec216[1] = iRec216[0];
			iRec215[1] = iRec215[0];
			fRec214[1] = fRec214[0];
			iRec213[1] = iRec213[0];
			iVec69[1] = iVec69[0];
			iRec217[1] = iRec217[0];
			iRec212[1] = iRec212[0];
			fRec208[1] = fRec208[0];
			fRec209[1] = fRec209[0];
			fRec210[1] = fRec210[0];
			fRec211[1] = fRec211[0];
			fRec205[1] = fRec205[0];
			fRec218[1] = fRec218[0];
			iVec71[1] = iVec71[0];
			iRec221[1] = iRec221[0];
			iRec219[1] = iRec219[0];
			fRec220[1] = fRec220[0];
			fRec222[1] = fRec222[0];
			fRec223[1] = fRec223[0];
			fVec72[1] = fVec72[0];
			fRec224[1] = fRec224[0];
			fRec225[1] = fRec225[0];
			fRec203[2] = fRec203[1];
			fRec203[1] = fRec203[0];
			iRec236[1] = iRec236[0];
			iRec235[1] = iRec235[0];
			fRec234[1] = fRec234[0];
			iRec233[1] = iRec233[0];
			iVec75[1] = iVec75[0];
			iRec237[1] = iRec237[0];
			iRec232[1] = iRec232[0];
			fRec228[1] = fRec228[0];
			fRec229[1] = fRec229[0];
			fRec230[1] = fRec230[0];
			fRec231[1] = fRec231[0];
			fRec238[1] = fRec238[0];
			fRec241[2] = fRec241[1];
			fRec241[1] = fRec241[0];
			fVec78[1] = fVec78[0];
			fRec240[1] = fRec240[0];
			fRec239[1] = fRec239[0];
			fRec242[1] = fRec242[0];
			iVec80[1] = iVec80[0];
			iRec245[1] = iRec245[0];
			iRec243[1] = iRec243[0];
			fRec244[1] = fRec244[0];
			fRec246[1] = fRec246[0];
			fRec247[1] = fRec247[0];
			fVec81[1] = fVec81[0];
			fRec248[1] = fRec248[0];
			fRec226[2] = fRec226[1];
			fRec226[1] = fRec226[0];
			iRec259[1] = iRec259[0];
			iRec258[1] = iRec258[0];
			fRec257[1] = fRec257[0];
			iRec256[1] = iRec256[0];
			iVec83[1] = iVec83[0];
			iRec260[1] = iRec260[0];
			iRec255[1] = iRec255[0];
			fRec251[1] = fRec251[0];
			fRec252[1] = fRec252[0];
			fRec253[1] = fRec253[0];
			fRec254[1] = fRec254[0];
			fRec261[1] = fRec261[0];
			fRec264[2] = fRec264[1];
			fRec264[1] = fRec264[0];
			fVec86[1] = fVec86[0];
			fRec263[1] = fRec263[0];
			fRec262[1] = fRec262[0];
			fRec265[1] = fRec265[0];
			iVec88[1] = iVec88[0];
			iRec268[1] = iRec268[0];
			iRec266[1] = iRec266[0];
			fRec267[1] = fRec267[0];
			fRec269[1] = fRec269[0];
			fRec270[1] = fRec270[0];
			fVec89[1] = fVec89[0];
			fRec271[1] = fRec271[0];
			fRec249[2] = fRec249[1];
			fRec249[1] = fRec249[0];
			fRec276[2] = fRec276[1];
			fRec276[1] = fRec276[0];
			fVec91[1] = fVec91[0];
			fRec275[1] = fRec275[0];
			iRec285[1] = iRec285[0];
			iRec284[1] = iRec284[0];
			fRec283[1] = fRec283[0];
			iRec282[1] = iRec282[0];
			iVec93[1] = iVec93[0];
			iRec286[1] = iRec286[0];
			iRec281[1] = iRec281[0];
			fRec277[1] = fRec277[0];
			fRec278[1] = fRec278[0];
			fRec279[1] = fRec279[0];
			fRec280[1] = fRec280[0];
			fRec274[1] = fRec274[0];
			fRec287[1] = fRec287[0];
			iVec95[1] = iVec95[0];
			iRec290[1] = iRec290[0];
			iRec288[1] = iRec288[0];
			fRec289[1] = fRec289[0];
			fRec291[1] = fRec291[0];
			fRec292[1] = fRec292[0];
			fVec96[1] = fVec96[0];
			fRec293[1] = fRec293[0];
			fRec294[1] = fRec294[0];
			fRec272[2] = fRec272[1];
			fRec272[1] = fRec272[0];
			fRec299[2] = fRec299[1];
			fRec299[1] = fRec299[0];
			fVec99[1] = fVec99[0];
			fRec298[1] = fRec298[0];
			iRec308[1] = iRec308[0];
			iRec307[1] = iRec307[0];
			fRec306[1] = fRec306[0];
			iRec305[1] = iRec305[0];
			iVec101[1] = iVec101[0];
			iRec309[1] = iRec309[0];
			iRec304[1] = iRec304[0];
			fRec300[1] = fRec300[0];
			fRec301[1] = fRec301[0];
			fRec302[1] = fRec302[0];
			fRec303[1] = fRec303[0];
			fRec297[1] = fRec297[0];
			fRec310[1] = fRec310[0];
			iVec103[1] = iVec103[0];
			iRec313[1] = iRec313[0];
			iRec311[1] = iRec311[0];
			fRec312[1] = fRec312[0];
			fRec314[1] = fRec314[0];
			fRec315[1] = fRec315[0];
			fVec104[1] = fVec104[0];
			fRec316[1] = fRec316[0];
			fRec317[1] = fRec317[0];
			fRec295[2] = fRec295[1];
			fRec295[1] = fRec295[0];
			fRec322[2] = fRec322[1];
			fRec322[1] = fRec322[0];
			fVec107[1] = fVec107[0];
			fRec321[1] = fRec321[0];
			iRec331[1] = iRec331[0];
			iRec330[1] = iRec330[0];
			fRec329[1] = fRec329[0];
			iRec328[1] = iRec328[0];
			iVec109[1] = iVec109[0];
			iRec332[1] = iRec332[0];
			iRec327[1] = iRec327[0];
			fRec323[1] = fRec323[0];
			fRec324[1] = fRec324[0];
			fRec325[1] = fRec325[0];
			fRec326[1] = fRec326[0];
			fRec320[1] = fRec320[0];
			fRec333[1] = fRec333[0];
			iVec111[1] = iVec111[0];
			iRec336[1] = iRec336[0];
			iRec334[1] = iRec334[0];
			fRec335[1] = fRec335[0];
			fRec337[1] = fRec337[0];
			fRec338[1] = fRec338[0];
			fVec112[1] = fVec112[0];
			fRec339[1] = fRec339[0];
			fRec340[1] = fRec340[0];
			fRec318[2] = fRec318[1];
			fRec318[1] = fRec318[0];
		}
	}

};

#ifdef FAUST_UIMACROS
	
	#define FAUST_FILE_NAME "bbdmi_multi_granulator14.dsp"
	#define FAUST_CLASS_NAME "bbdmi_multi_granulator14"
	#define FAUST_COMPILATION_OPIONS "-a /Applications/Faust-2.72.14/share/faust/max-msp/max-msp64.cpp -lang cpp -i -ct 1 -cn bbdmi_multi_granulator14 -es 1 -mcd 16 -mdd 1024 -mdy 33 -uim -double -ftz 0"
	#define FAUST_INPUTS 14
	#define FAUST_OUTPUTS 14
	#define FAUST_ACTIVES 17
	#define FAUST_PASSIVES 0

	FAUST_ADDVERTICALSLIDER("Multi Granulator/granulator/Grain/GrainEnvelope/indexdistr", fVslider1, 0.0, 0.0, 21.0, 1.0);
	FAUST_ADDVERTICALSLIDER("Multi Granulator/granulator/Grain/GrainEnvelope/grainenvmorph", fVslider10, 0.5, 0.0, 1.0, 0.01);
	FAUST_ADDVERTICALSLIDER("Multi Granulator/granulator/Grain/GrainEnvelope/grainsize", fVslider2, 22.0, 1.0, 2e+03, 1.0);
	FAUST_ADDVERTICALSLIDER("Multi Granulator/granulator/Grain/GrainEnvelope/grainoffset", fVslider4, 0.0, 0.0, 1e+03, 1.0);
	FAUST_ADDVERTICALSLIDER("Multi Granulator/granulator/Grain/GrainEnvelope/spacing", fVslider3, 25.0, 1.0, 1e+03, 1.0);
	FAUST_ADDVERTICALSLIDER("Multi Granulator/granulator/Grain/GrainEnvelope/modfactor", fVslider11, 0.0, 0.0, 1.0, 0.001);
	FAUST_ADDVERTICALSLIDER("Multi Granulator/granulator/Grain/GrainEnvelope/modfreq", fVslider14, 4e+03, 1.0, 1.5e+04, 1.0);
	FAUST_ADDVERTICALSLIDER("Multi Granulator/granulator/Grain/GrainEnvelope/modmorph", fVslider13, 0.0, 0.0, 3.0, 0.001);
	FAUST_ADDVERTICALSLIDER("Multi Granulator/granulator/Grain/GrainEnvelope/modfreqmod", fVslider12, 0.5, 0.0, 1.0, 0.001);
	FAUST_ADDVERTICALSLIDER("Multi Granulator/granulator/Grain/GrainEnvelope/transpgrain", fVslider15, 0.0, -48.0, 48.0, 0.01);
	FAUST_ADDVERTICALSLIDER("Multi Granulator/granulator/Grain/GrainEnvelope/variability", fVslider5, 0.7, 0.0, 1.0, 0.01);
	FAUST_ADDHORIZONTALSLIDER("Multi Granulator/granulator/Grain/maxdelay", fHslider0, 2e+03, 1.0, 1e+04, 1.0);
	FAUST_ADDVERTICALSLIDER("Multi Granulator/granulator/loop/gain", fVslider9, 1.0, 0.0, 1.0, 0.001);
	FAUST_ADDVERTICALSLIDER("Multi Granulator/granulator/loop/feedback", fVslider0, 0.5, 0.0, 1.0, 0.01);
	FAUST_ADDVERTICALSLIDER("Multi Granulator/granulator/loop/hpffreq", fVslider7, 2e+01, 2e+01, 2e+04, 1.0);
	FAUST_ADDVERTICALSLIDER("Multi Granulator/granulator/loop/lpffreq", fVslider8, 2e+04, 2e+01, 2e+04, 1.0);
	FAUST_ADDVERTICALSLIDER("Multi Granulator/granulator/loop/transpout", fVslider6, 0.0, -48.0, 48.0, 0.01);

	#define FAUST_LIST_ACTIVES(p) \
		p(VERTICALSLIDER, indexdistr, "Multi Granulator/granulator/Grain/GrainEnvelope/indexdistr", fVslider1, 0.0, 0.0, 21.0, 1.0) \
		p(VERTICALSLIDER, grainenvmorph, "Multi Granulator/granulator/Grain/GrainEnvelope/grainenvmorph", fVslider10, 0.5, 0.0, 1.0, 0.01) \
		p(VERTICALSLIDER, grainsize, "Multi Granulator/granulator/Grain/GrainEnvelope/grainsize", fVslider2, 22.0, 1.0, 2e+03, 1.0) \
		p(VERTICALSLIDER, grainoffset, "Multi Granulator/granulator/Grain/GrainEnvelope/grainoffset", fVslider4, 0.0, 0.0, 1e+03, 1.0) \
		p(VERTICALSLIDER, spacing, "Multi Granulator/granulator/Grain/GrainEnvelope/spacing", fVslider3, 25.0, 1.0, 1e+03, 1.0) \
		p(VERTICALSLIDER, modfactor, "Multi Granulator/granulator/Grain/GrainEnvelope/modfactor", fVslider11, 0.0, 0.0, 1.0, 0.001) \
		p(VERTICALSLIDER, modfreq, "Multi Granulator/granulator/Grain/GrainEnvelope/modfreq", fVslider14, 4e+03, 1.0, 1.5e+04, 1.0) \
		p(VERTICALSLIDER, modmorph, "Multi Granulator/granulator/Grain/GrainEnvelope/modmorph", fVslider13, 0.0, 0.0, 3.0, 0.001) \
		p(VERTICALSLIDER, modfreqmod, "Multi Granulator/granulator/Grain/GrainEnvelope/modfreqmod", fVslider12, 0.5, 0.0, 1.0, 0.001) \
		p(VERTICALSLIDER, transpgrain, "Multi Granulator/granulator/Grain/GrainEnvelope/transpgrain", fVslider15, 0.0, -48.0, 48.0, 0.01) \
		p(VERTICALSLIDER, variability, "Multi Granulator/granulator/Grain/GrainEnvelope/variability", fVslider5, 0.7, 0.0, 1.0, 0.01) \
		p(HORIZONTALSLIDER, maxdelay, "Multi Granulator/granulator/Grain/maxdelay", fHslider0, 2e+03, 1.0, 1e+04, 1.0) \
		p(VERTICALSLIDER, gain, "Multi Granulator/granulator/loop/gain", fVslider9, 1.0, 0.0, 1.0, 0.001) \
		p(VERTICALSLIDER, feedback, "Multi Granulator/granulator/loop/feedback", fVslider0, 0.5, 0.0, 1.0, 0.01) \
		p(VERTICALSLIDER, hpffreq, "Multi Granulator/granulator/loop/hpffreq", fVslider7, 2e+01, 2e+01, 2e+04, 1.0) \
		p(VERTICALSLIDER, lpffreq, "Multi Granulator/granulator/loop/lpffreq", fVslider8, 2e+04, 2e+01, 2e+04, 1.0) \
		p(VERTICALSLIDER, transpout, "Multi Granulator/granulator/loop/transpout", fVslider6, 0.0, -48.0, 48.0, 0.01) \

	#define FAUST_LIST_PASSIVES(p) \

#endif

/***************************END USER SECTION ***************************/

/*******************BEGIN ARCHITECTURE SECTION (part 2/2)***************/

/* Faust code wrapper ------- */

#include "ext.h"
#include "ext_obex.h"
#include "z_dsp.h"
#include "jpatcher_api.h"
#include <string.h>

#define ASSIST_INLET 	1
#define ASSIST_OUTLET 	2

#define EXTERNAL_VERSION    "0.87"
#define STR_SIZE            512

/************************** BEGIN MidiUI.h ****************************
FAUST Architecture File
Copyright (C) 2003-2022 GRAME, Centre National de Creation Musicale
---------------------------------------------------------------------
This program is free software; you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation; either version 2.1 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

EXCEPTION : As a special exception, you may create a larger work
that contains this FAUST architecture section and distribute
that work under terms of your choice, so long as this FAUST
architecture section is not modified.
************************************************************************/

#ifndef FAUST_MIDIUI_H
#define FAUST_MIDIUI_H

#include <vector>
#include <string>
#include <utility>
#include <cstdlib>
#include <cmath>

/************************** BEGIN MapUI.h ******************************
 FAUST Architecture File
 Copyright (C) 2003-2022 GRAME, Centre National de Creation Musicale
 ---------------------------------------------------------------------
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation; either version 2.1 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 
 EXCEPTION : As a special exception, you may create a larger work
 that contains this FAUST architecture section and distribute
 that work under terms of your choice, so long as this FAUST
 architecture section is not modified.
 ***********************************************************************/

#ifndef FAUST_MAPUI_H
#define FAUST_MAPUI_H

#include <vector>
#include <map>
#include <string>
#include <stdio.h>


/*******************************************************************************
 * MapUI : Faust User Interface.
 *
 * This class creates:
 * - a map of 'labels' and zones for each UI item.
 * - a map of unique 'shortname' (built so that they never collide) and zones for each UI item
 * - a map of complete hierarchical 'paths' and zones for each UI item
 *
 * Simple 'labels', 'shortname' and complete 'paths' (to fully discriminate between possible same
 * 'labels' at different location in the UI hierachy) can be used to access a given parameter.
 ******************************************************************************/

class FAUST_API MapUI : public UI, public PathBuilder
{
    
    protected:
    
        // Label zone map
        std::map<std::string, FAUSTFLOAT*> fLabelZoneMap;
    
        // Shortname zone map
        std::map<std::string, FAUSTFLOAT*> fShortnameZoneMap;
    
        // Full path map
        std::map<std::string, FAUSTFLOAT*> fPathZoneMap;
    
        void addZoneLabel(const std::string& label, FAUSTFLOAT* zone)
        {
            std::string path = buildPath(label);
            fFullPaths.push_back(path);
            fPathZoneMap[path] = zone;
            fLabelZoneMap[label] = zone;
        }
    
    public:
        
        MapUI() {}
        virtual ~MapUI() {}
        
        // -- widget's layouts
        void openTabBox(const char* label)
        {
            pushLabel(label);
        }
        void openHorizontalBox(const char* label)
        {
            pushLabel(label);
        }
        void openVerticalBox(const char* label)
        {
            pushLabel(label);
        }
        void closeBox()
        {
            if (popLabel()) {
                // Shortnames can be computed when all fullnames are known
                computeShortNames();
                // Fill 'shortname' map
                for (const auto& it : fFullPaths) {
                    fShortnameZoneMap[fFull2Short[it]] = fPathZoneMap[it];
                }
            }
        }
        
        // -- active widgets
        void addButton(const char* label, FAUSTFLOAT* zone)
        {
            addZoneLabel(label, zone);
        }
        void addCheckButton(const char* label, FAUSTFLOAT* zone)
        {
            addZoneLabel(label, zone);
        }
        void addVerticalSlider(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT fmin, FAUSTFLOAT fmax, FAUSTFLOAT step)
        {
            addZoneLabel(label, zone);
        }
        void addHorizontalSlider(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT fmin, FAUSTFLOAT fmax, FAUSTFLOAT step)
        {
            addZoneLabel(label, zone);
        }
        void addNumEntry(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT fmin, FAUSTFLOAT fmax, FAUSTFLOAT step)
        {
            addZoneLabel(label, zone);
        }
        
        // -- passive widgets
        void addHorizontalBargraph(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT fmin, FAUSTFLOAT fmax)
        {
            addZoneLabel(label, zone);
        }
        void addVerticalBargraph(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT fmin, FAUSTFLOAT fmax)
        {
            addZoneLabel(label, zone);
        }
    
        // -- soundfiles
        virtual void addSoundfile(const char* label, const char* filename, Soundfile** sf_zone) {}
        
        // -- metadata declarations
        virtual void declare(FAUSTFLOAT* zone, const char* key, const char* val)
        {}
    
        //-------------------------------------------------------------------------------
        // Public API
        //-------------------------------------------------------------------------------
    
        /**
         * Set the param value.
         *
         * @param str - the UI parameter label/shortname/path
         * @param value - the UI parameter value
         *
         */
        void setParamValue(const std::string& str, FAUSTFLOAT value)
        {
            const auto fPathZoneMapIter = fPathZoneMap.find(str);
            if (fPathZoneMapIter != fPathZoneMap.end()) {
                *fPathZoneMapIter->second = value;
                return;
            }
            
            const auto fShortnameZoneMapIter = fShortnameZoneMap.find(str);
            if (fShortnameZoneMapIter != fShortnameZoneMap.end()) {
                *fShortnameZoneMapIter->second = value;
                return;
            }
            
            const auto fLabelZoneMapIter = fLabelZoneMap.find(str);
            if (fLabelZoneMapIter != fLabelZoneMap.end()) {
                *fLabelZoneMapIter->second = value;
                return;
            }
            
            fprintf(stderr, "ERROR : setParamValue '%s' not found\n", str.c_str());
        }
        
        /**
         * Return the param value.
         *
         * @param str - the UI parameter label/shortname/path
         *
         * @return the param value.
         */
        FAUSTFLOAT getParamValue(const std::string& str)
        {
            const auto fPathZoneMapIter = fPathZoneMap.find(str);
            if (fPathZoneMapIter != fPathZoneMap.end()) {
                return *fPathZoneMapIter->second;
            }
            
            const auto fShortnameZoneMapIter = fShortnameZoneMap.find(str);
            if (fShortnameZoneMapIter != fShortnameZoneMap.end()) {
                return *fShortnameZoneMapIter->second;
            }
            
            const auto fLabelZoneMapIter = fLabelZoneMap.find(str);
            if (fLabelZoneMapIter != fLabelZoneMap.end()) {
                return *fLabelZoneMapIter->second;
            }
            
            fprintf(stderr, "ERROR : getParamValue '%s' not found\n", str.c_str());
            return 0;
        }
    
        // map access 
        std::map<std::string, FAUSTFLOAT*>& getFullpathMap() { return fPathZoneMap; }
        std::map<std::string, FAUSTFLOAT*>& getShortnameMap() { return fShortnameZoneMap; }
        std::map<std::string, FAUSTFLOAT*>& getLabelMap() { return fLabelZoneMap; }
            
        /**
         * Return the number of parameters in the UI.
         *
         * @return the number of parameters
         */
        int getParamsCount() { return int(fPathZoneMap.size()); }
        
        /**
         * Return the param path.
         *
         * @param index - the UI parameter index
         *
         * @return the param path
         */
        std::string getParamAddress(int index)
        {
            if (index < 0 || index > int(fPathZoneMap.size())) {
                return "";
            } else {
                auto it = fPathZoneMap.begin();
                while (index-- > 0 && it++ != fPathZoneMap.end()) {}
                return it->first;
            }
        }
        
        const char* getParamAddress1(int index)
        {
            if (index < 0 || index > int(fPathZoneMap.size())) {
                return nullptr;
            } else {
                auto it = fPathZoneMap.begin();
                while (index-- > 0 && it++ != fPathZoneMap.end()) {}
                return it->first.c_str();
            }
        }
    
        /**
         * Return the param shortname.
         *
         * @param index - the UI parameter index
         *
         * @return the param shortname
         */
        std::string getParamShortname(int index)
        {
            if (index < 0 || index > int(fShortnameZoneMap.size())) {
                return "";
            } else {
                auto it = fShortnameZoneMap.begin();
                while (index-- > 0 && it++ != fShortnameZoneMap.end()) {}
                return it->first;
            }
        }
        
        const char* getParamShortname1(int index)
        {
            if (index < 0 || index > int(fShortnameZoneMap.size())) {
                return nullptr;
            } else {
                auto it = fShortnameZoneMap.begin();
                while (index-- > 0 && it++ != fShortnameZoneMap.end()) {}
                return it->first.c_str();
            }
        }
    
        /**
         * Return the param label.
         *
         * @param index - the UI parameter index
         *
         * @return the param label
         */
        std::string getParamLabel(int index)
        {
            if (index < 0 || index > int(fLabelZoneMap.size())) {
                return "";
            } else {
                auto it = fLabelZoneMap.begin();
                while (index-- > 0 && it++ != fLabelZoneMap.end()) {}
                return it->first;
            }
        }
        
        const char* getParamLabel1(int index)
        {
            if (index < 0 || index > int(fLabelZoneMap.size())) {
                return nullptr;
            } else {
                auto it = fLabelZoneMap.begin();
                while (index-- > 0 && it++ != fLabelZoneMap.end()) {}
                return it->first.c_str();
            }
        }
    
        /**
         * Return the param path.
         *
         * @param zone - the UI parameter memory zone
         *
         * @return the param path
         */
        std::string getParamAddress(FAUSTFLOAT* zone)
        {
            for (const auto& it : fPathZoneMap) {
                if (it.second == zone) return it.first;
            }
            return "";
        }
    
        /**
         * Return the param memory zone.
         *
         * @param zone - the UI parameter label/shortname/path
         *
         * @return the param path
         */
        FAUSTFLOAT* getParamZone(const std::string& str)
        {
            const auto fPathZoneMapIter = fPathZoneMap.find(str);
            if (fPathZoneMapIter != fPathZoneMap.end()) {
                return fPathZoneMapIter->second;
            }
            
            const auto fShortnameZoneMapIter = fShortnameZoneMap.find(str);
            if (fShortnameZoneMapIter != fShortnameZoneMap.end()) {
                return fShortnameZoneMapIter->second;
            }
            
            const auto fLabelZoneMapIter = fLabelZoneMap.find(str);
            if (fLabelZoneMapIter != fLabelZoneMap.end()) {
                return fLabelZoneMapIter->second;
            }

            return nullptr;
        }
    
        /**
         * Return the param memory zone.
         *
         * @param zone - the UI parameter index
         *
         * @return the param path
         */
        FAUSTFLOAT* getParamZone(int index)
        {
            if (index < 0 || index > int(fPathZoneMap.size())) {
                return nullptr;
            } else {
                auto it = fPathZoneMap.begin();
                while (index-- > 0 && it++ != fPathZoneMap.end()) {}
                return it->second;
            }
        }
    
        static bool endsWith(const std::string& str, const std::string& end)
        {
            size_t l1 = str.length();
            size_t l2 = end.length();
            return (l1 >= l2) && (0 == str.compare(l1 - l2, l2, end));
        }
    
};

#endif // FAUST_MAPUI_H
/**************************  END  MapUI.h **************************/
/************************** BEGIN midi.h *******************************
FAUST Architecture File
Copyright (C) 2003-2022 GRAME, Centre National de Creation Musicale
---------------------------------------------------------------------
This program is free software; you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation; either version 2.1 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

EXCEPTION : As a special exception, you may create a larger work
that contains this FAUST architecture section and distribute
that work under terms of your choice, so long as this FAUST
architecture section is not modified.
************************************************************************/

#ifndef __midi__
#define __midi__

#include <vector>
#include <string>
#include <string.h>
#include <algorithm>
#include <assert.h>


class FAUST_API MapUI;

/**
 * A timestamped short MIDI message used with SOUL.
 */

// Force contiguous memory layout
#pragma pack (push, 1)
struct MIDIMessage
{
    uint32_t frameIndex;
    uint8_t byte0, byte1, byte2;
};
#pragma pack (pop)

/**
 * For timestamped MIDI messages (in usec).
 */
struct DatedMessage {
    
    double fDate;
    unsigned char fBuffer[3];
    size_t fSize;
    
    DatedMessage(double date, unsigned char* buffer, size_t size)
    :fDate(date), fSize(size)
    {
        assert(size <= 3);
        memcpy(fBuffer, buffer, size);
    }
    
    DatedMessage():fDate(0.0), fSize(0)
    {}
    
};

/**
 * MIDI processor definition.
 *
 * MIDI input or output handling classes will implement this interface,
 * so the same method names (keyOn, keyOff, ctrlChange...) will be used either
 * when decoding MIDI input or encoding MIDI output events.
 * MIDI channel is numbered in [0..15] in this layer.
 */
class midi {

    public:

        midi() {}
        virtual ~midi() {}

        // Additional timestamped API for MIDI input
        virtual MapUI* keyOn(double, int channel, int pitch, int velocity)
        {
            return keyOn(channel, pitch, velocity);
        }
        
        virtual void keyOff(double, int channel, int pitch, int velocity = 0)
        {
            keyOff(channel, pitch, velocity);
        }
    
        virtual void keyPress(double, int channel, int pitch, int press)
        {
            keyPress(channel, pitch, press);
        }
        
        virtual void chanPress(double date, int channel, int press)
        {
            chanPress(channel, press);
        }
    
        virtual void pitchWheel(double, int channel, int wheel)
        {
            pitchWheel(channel, wheel);
        }
           
        virtual void ctrlChange(double, int channel, int ctrl, int value)
        {
            ctrlChange(channel, ctrl, value);
        }
    
        virtual void ctrlChange14bits(double, int channel, int ctrl, int value)
        {
            ctrlChange14bits(channel, ctrl, value);
        }
    
        virtual void rpn(double, int channel, int ctrl, int value)
        {
            rpn(channel, ctrl, value);
        }

        virtual void progChange(double, int channel, int pgm)
        {
            progChange(channel, pgm);
        }
    
        virtual void sysEx(double, std::vector<unsigned char>& message)
        {
            sysEx(message);
        }

        // MIDI sync
        virtual void startSync(double date)  {}
        virtual void stopSync(double date)   {}
        virtual void clock(double date)  {}

        // Standard MIDI API
        virtual MapUI* keyOn(int channel, int pitch, int velocity)      { return nullptr; }
        virtual void keyOff(int channel, int pitch, int velocity)       {}
        virtual void keyPress(int channel, int pitch, int press)        {}
        virtual void chanPress(int channel, int press)                  {}
        virtual void ctrlChange(int channel, int ctrl, int value)       {}
        virtual void ctrlChange14bits(int channel, int ctrl, int value) {}
        virtual void rpn(int channel, int ctrl, int value)              {}
        virtual void pitchWheel(int channel, int wheel)                 {}
        virtual void progChange(int channel, int pgm)                   {}
        virtual void sysEx(std::vector<unsigned char>& message)         {}

        enum MidiStatus {
            // channel voice messages
            MIDI_NOTE_OFF = 0x80,
            MIDI_NOTE_ON = 0x90,
            MIDI_CONTROL_CHANGE = 0xB0,
            MIDI_PROGRAM_CHANGE = 0xC0,
            MIDI_PITCH_BEND = 0xE0,
            MIDI_AFTERTOUCH = 0xD0,         // aka channel pressure
            MIDI_POLY_AFTERTOUCH = 0xA0,    // aka key pressure
            MIDI_CLOCK = 0xF8,
            MIDI_START = 0xFA,
            MIDI_CONT = 0xFB,
            MIDI_STOP = 0xFC,
            MIDI_SYSEX_START = 0xF0,
            MIDI_SYSEX_STOP = 0xF7
        };

        enum MidiCtrl {
            ALL_NOTES_OFF = 123,
            ALL_SOUND_OFF = 120
        };
    
        enum MidiNPN {
            PITCH_BEND_RANGE = 0
        };

};

/**
 * A class to decode NRPN and RPN messages, adapted from JUCE forum message:
 * https://forum.juce.com/t/14bit-midi-controller-support/11517
 */
class MidiNRPN {
    
    private:
    
        bool ctrlnew;
        int ctrlnum;
        int ctrlval;
        
        int nrpn_lsb, nrpn_msb;
        int data_lsb, data_msb;
        
        enum
        {
            midi_nrpn_lsb = 98,
            midi_nrpn_msb = 99,
            midi_rpn_lsb  = 100,
            midi_rpn_msb  = 101,
            midi_data_lsb = 38,
            midi_data_msb = 6
        };
    
    public:
        
        MidiNRPN(): ctrlnew(false), nrpn_lsb(-1), nrpn_msb(-1), data_lsb(-1), data_msb(-1)
        {}
        
        // return true if the message has been filtered
        bool process(int data1, int data2)
        {
            switch (data1)
            {
                case midi_nrpn_lsb: nrpn_lsb = data2; return true;
                case midi_nrpn_msb: nrpn_msb = data2; return true;
                case midi_rpn_lsb: {
                    if (data2 == 127) {
                        nrpn_lsb = data_lsb = -1;
                    } else {
                        nrpn_lsb = 0;
                        data_lsb = -1;
                    }
                    return true;
                }
                case midi_rpn_msb: {
                    if (data2 == 127) {
                        nrpn_msb = data_msb = -1;
                    } else {
                        nrpn_msb = 0;
                        data_msb = -1;
                    }
                    return true;
                }
                case midi_data_lsb:
                case midi_data_msb:
                {
                    if (data1 == midi_data_msb) {
                        if (nrpn_msb < 0) {
                            return false;
                        }
                        data_msb = data2;
                    } else { // midi_data_lsb
                        if (nrpn_lsb < 0) {
                            return false;
                        }
                        data_lsb = data2;
                    }
                    if (data_lsb >= 0 && data_msb >= 0) {
                        ctrlnum = (nrpn_msb << 7) | nrpn_lsb;
                        ctrlval = (data_msb << 7) | data_lsb;
                        data_lsb = data_msb = -1;
                        nrpn_msb = nrpn_lsb = -1;
                        ctrlnew = true;
                    }
                    return true;
                }
                default: return false;
            };
        }
        
        bool hasNewNRPN() { bool res = ctrlnew; ctrlnew = false; return res; }
        
        // results in [0, 16383]
        int getCtrl() const { return ctrlnum; }
        int getVal() const { return ctrlval; }
    
};

/**
 * A pure interface for MIDI handlers that can send/receive MIDI messages to/from 'midi' objects.
 */
struct midi_interface {
    virtual void addMidiIn(midi* midi_dsp)      = 0;
    virtual void removeMidiIn(midi* midi_dsp)   = 0;
    virtual ~midi_interface() {}
};

/****************************************************
 * Base class for MIDI input handling.
 *
 * Shared common code used for input handling:
 * - decoding Real-Time messages: handleSync
 * - decoding one data byte messages: handleData1
 * - decoding two data byte messages: handleData2
 * - getting ready messages in polling mode
 ****************************************************/
class midi_handler : public midi, public midi_interface {

    protected:

        std::vector<midi*> fMidiInputs;
        std::string fName;
        MidiNRPN fNRPN;
    
        int range(int min, int max, int val) { return (val < min) ? min : ((val >= max) ? max : val); }
  
    public:

        midi_handler(const std::string& name = "MIDIHandler"):midi_interface(), fName(name) {}
        virtual ~midi_handler() {}

        void addMidiIn(midi* midi_dsp) { if (midi_dsp) fMidiInputs.push_back(midi_dsp); }
        void removeMidiIn(midi* midi_dsp)
        {
            std::vector<midi*>::iterator it = std::find(fMidiInputs.begin(), fMidiInputs.end(), midi_dsp);
            if (it != fMidiInputs.end()) {
                fMidiInputs.erase(it);
            }
        }

        // Those 2 methods have to be implemented by subclasses
        virtual bool startMidi() { return true; }
        virtual void stopMidi() {}
    
        void setName(const std::string& name) { fName = name; }
        std::string getName() { return fName; }
    
        // To be used in polling mode
        virtual int recvMessages(std::vector<MIDIMessage>* message) { return 0; }
        virtual void sendMessages(std::vector<MIDIMessage>* message, int count) {}
    
        // MIDI Real-Time
        void handleClock(double time)
        {
            for (unsigned int i = 0; i < fMidiInputs.size(); i++) {
                fMidiInputs[i]->clock(time);
            }
        }
        
        void handleStart(double time)
        {
            for (unsigned int i = 0; i < fMidiInputs.size(); i++) {
                fMidiInputs[i]->startSync(time);
            }
        }
        
        void handleStop(double time)
        {
            for (unsigned int i = 0; i < fMidiInputs.size(); i++) {
                fMidiInputs[i]->stopSync(time);
            }
        }
    
        void handleSync(double time, int type)
        {
            if (type == MIDI_CLOCK) {
                handleClock(time);
            // We can consider start and continue as identical messages
            } else if ((type == MIDI_START) || (type == MIDI_CONT)) {
                handleStart(time);
            } else if (type == MIDI_STOP) {
                handleStop(time);
            }
        }
    
        // MIDI 1 data
        void handleProgChange(double time, int channel, int data1)
        {
            for (unsigned int i = 0; i < fMidiInputs.size(); i++) {
                fMidiInputs[i]->progChange(time, channel, data1);
            }
        }
    
        void handleAfterTouch(double time, int channel, int data1)
        {
            for (unsigned int i = 0; i < fMidiInputs.size(); i++) {
                fMidiInputs[i]->chanPress(time, channel, data1);
            }
        }

        void handleData1(double time, int type, int channel, int data1)
        {
            if (type == MIDI_PROGRAM_CHANGE) {
                handleProgChange(time, channel, data1);
            } else if (type == MIDI_AFTERTOUCH) {
                handleAfterTouch(time, channel, data1);
            }
        }
    
        // MIDI 2 datas
        void handleKeyOff(double time, int channel, int data1, int data2)
        {
            for (unsigned int i = 0; i < fMidiInputs.size(); i++) {
                fMidiInputs[i]->keyOff(time, channel, data1, data2);
            }
        }
        
        void handleKeyOn(double time, int channel, int data1, int data2)
        {
            if (data2 == 0) {
                handleKeyOff(time, channel, data1, data2);
            } else {
                for (unsigned int i = 0; i < fMidiInputs.size(); i++) {
                    fMidiInputs[i]->keyOn(time, channel, data1, data2);
                }
            }
        }
    
        void handleCtrlChange(double time, int channel, int data1, int data2)
        {
            // Special processing for NRPN and RPN
            if (fNRPN.process(data1, data2)) {
                if (fNRPN.hasNewNRPN()) {
                    for (unsigned int i = 0; i < fMidiInputs.size(); i++) {
                        fMidiInputs[i]->rpn(time, channel, fNRPN.getCtrl(), fNRPN.getVal());
                    }
                }
            } else {
                for (unsigned int i = 0; i < fMidiInputs.size(); i++) {
                    fMidiInputs[i]->ctrlChange(time, channel, data1, data2);
                }
            }
        }
        
        void handlePitchWheel(double time, int channel, int data1, int data2)
        {
            for (unsigned int i = 0; i < fMidiInputs.size(); i++) {
                fMidiInputs[i]->pitchWheel(time, channel, (data2 << 7) + data1);
            }
        }
    
        void handlePitchWheel(double time, int channel, int bend)
        {
            for (unsigned int i = 0; i < fMidiInputs.size(); i++) {
                fMidiInputs[i]->pitchWheel(time, channel, bend);
            }
        }
        
        void handlePolyAfterTouch(double time, int channel, int data1, int data2)
        {
            for (unsigned int i = 0; i < fMidiInputs.size(); i++) {
                fMidiInputs[i]->keyPress(time, channel, data1, data2);
            }
        }
  
        void handleData2(double time, int type, int channel, int data1, int data2)
        {
            if (type == MIDI_NOTE_OFF) {
                handleKeyOff(time, channel,  data1, data2);
            } else if (type == MIDI_NOTE_ON) {
                handleKeyOn(time, channel, data1, data2);
            } else if (type == MIDI_CONTROL_CHANGE) {
                handleCtrlChange(time, channel, data1, data2);
            } else if (type == MIDI_PITCH_BEND) {
                handlePitchWheel(time, channel, data1, data2);
            } else if (type == MIDI_POLY_AFTERTOUCH) {
                handlePolyAfterTouch(time, channel, data1, data2);
            }
        }
    
        // SysEx
        void handleSysex(double time, std::vector<unsigned char>& message)
        {
            for (unsigned int i = 0; i < fMidiInputs.size(); i++) {
                fMidiInputs[i]->sysEx(time, message);
            }
        }
    
        void handleMessage(double time, int type, std::vector<unsigned char>& message)
        {
            if (type == MIDI_SYSEX_START) {
                handleSysex(time, message);
            }
        }
  
};

#define ucast(v) static_cast<unsigned char>(v)

#endif // __midi__
/**************************  END  midi.h **************************/

#ifdef _MSC_VER
#define gsscanf sscanf_s
#else
#define gsscanf sscanf
#endif

/**
 * Helper code for MIDI meta and polyphonic 'nvoices' parsing.
 */
struct MidiMeta : public Meta, public std::map<std::string, std::string> {
    
    void declare(const char* key, const char* value)
    {
        (*this)[key] = value;
    }
    
    const std::string get(const char* key, const char* def)
    {
        return (this->find(key) != this->end()) ? (*this)[key] : def;
    }
    
    static void analyse(dsp* mono_dsp, bool& midi_sync, int& nvoices)
    {
        JSONUI jsonui;
        mono_dsp->buildUserInterface(&jsonui);
        std::string json = jsonui.JSON();
        midi_sync = ((json.find("midi") != std::string::npos) &&
                     ((json.find("start") != std::string::npos) ||
                      (json.find("stop") != std::string::npos) ||
                      (json.find("clock") != std::string::npos) ||
                      (json.find("timestamp") != std::string::npos)));
    
    #if defined(NVOICES) && NVOICES!=NUM_VOICES
        nvoices = NVOICES;
    #else
        MidiMeta meta;
        mono_dsp->metadata(&meta);
        bool found_voices = false;
        // If "options" metadata is used
        std::string options = meta.get("options", "");
        if (options != "") {
            std::map<std::string, std::string> metadata;
            std::string res;
            MetaDataUI::extractMetadata(options, res, metadata);
            if (metadata.find("nvoices") != metadata.end()) {
                nvoices = std::atoi(metadata["nvoices"].c_str());
                found_voices = true;
            }
        }
        // Otherwise test for "nvoices" metadata
        if (!found_voices) {
            std::string numVoices = meta.get("nvoices", "0");
            nvoices = std::atoi(numVoices.c_str());
        }
        nvoices = std::max<int>(0, nvoices);
    #endif
    }
    
    static bool checkPolyphony(dsp* mono_dsp)
    {
        MapUI map_ui;
        mono_dsp->buildUserInterface(&map_ui);
        bool has_freq = false;
        bool has_gate = false;
        bool has_gain = false;
        for (int i = 0; i < map_ui.getParamsCount(); i++) {
            std::string path = map_ui.getParamAddress(i);
            has_freq |= MapUI::endsWith(path, "/freq");
            has_freq |= MapUI::endsWith(path, "/key");
            has_gate |= MapUI::endsWith(path, "/gate");
            has_gain |= MapUI::endsWith(path, "/gain");
            has_gain |= MapUI::endsWith(path, "/vel");
            has_gain |= MapUI::endsWith(path, "/velocity");
        }
        return (has_freq && has_gate && has_gain);
    }
    
};

/**
 * uiMidi : Faust User Interface
 * This class decodes MIDI meta data and maps incoming MIDI messages to them.
 * Currently ctrlChange, keyOn/keyOff, keyPress, progChange, chanPress, pitchWheel/pitchBend
 * start/stop/clock meta data is handled.
 * MIDI channel is numbered in [1..16] in this layer.
 * Channel 0 means "all channels" when receiving or sending.
 */
class uiMidi {
    
    friend class MidiUI;
    
    protected:
        
        midi* fMidiOut;
        bool fInputCtrl;
        int fChan;
    
        bool inRange(FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT v) { return (min <= v && v <= max); }
    
    public:
        
        uiMidi(midi* midi_out, bool input, int chan = 0):fMidiOut(midi_out), fInputCtrl(input), fChan(chan)
        {}
        virtual ~uiMidi()
        {}

};

/**
 * Base class for MIDI aware UI items.
 */
class uiMidiItem : public uiMidi, public uiItem {
    
    public:
        
        uiMidiItem(midi* midi_out, GUI* ui, FAUSTFLOAT* zone, bool input = true, int chan = 0)
            :uiMidi(midi_out, input, chan), uiItem(ui, zone)
        {}
        virtual ~uiMidiItem()
        {}
    
        virtual void reflectZone() {}
    
};

/**
 * Base class for MIDI aware UI items with timestamp support.
 */
class uiMidiTimedItem : public uiMidi, public uiTimedItem {
    
    public:
        
        uiMidiTimedItem(midi* midi_out, GUI* ui, FAUSTFLOAT* zone, bool input = true, int chan = 0)
            :uiMidi(midi_out, input, chan), uiTimedItem(ui, zone)
        {}
        virtual ~uiMidiTimedItem()
        {}
    
        virtual void reflectZone() {}
    
};

/**
 * MIDI sync.
 */
class uiMidiStart : public uiMidiTimedItem
{
  
    public:
    
        uiMidiStart(midi* midi_out, GUI* ui, FAUSTFLOAT* zone, bool input = true)
            :uiMidiTimedItem(midi_out, ui, zone, input)
        {}
        virtual ~uiMidiStart()
        {}
        
        virtual void reflectZone()
        {
            FAUSTFLOAT v = *fZone;
            fCache = v;
            if (v != FAUSTFLOAT(0)) {
                fMidiOut->startSync(0);
            }
        }
        void modifyZone(double date, FAUSTFLOAT v)
        {
            if (fInputCtrl) {
                uiItem::modifyZone(FAUSTFLOAT(v));
            }
        }
        
};

class uiMidiStop : public uiMidiTimedItem {
  
    public:
    
        uiMidiStop(midi* midi_out, GUI* ui, FAUSTFLOAT* zone, bool input = true)
            :uiMidiTimedItem(midi_out, ui, zone, input)
        {}
        virtual ~uiMidiStop()
        {}
        
        virtual void reflectZone()
        {
            FAUSTFLOAT v = *fZone;
            fCache = v;
            if (v != FAUSTFLOAT(1)) {
                fMidiOut->stopSync(0);
            }
        }
    
        void modifyZone(double date, FAUSTFLOAT v)
        {
            if (fInputCtrl) {
                uiItem::modifyZone(FAUSTFLOAT(v));
            }
        }
};

class uiMidiClock : public uiMidiTimedItem {

    private:
        
        bool fState;
  
    public:
    
        uiMidiClock(midi* midi_out, GUI* ui, FAUSTFLOAT* zone, bool input = true)
            :uiMidiTimedItem(midi_out, ui, zone, input), fState(false)
        {}
        virtual ~uiMidiClock()
        {}
    
        virtual void reflectZone()
        {
            FAUSTFLOAT v = *fZone;
            fCache = v;
            fMidiOut->clock(0);
        }
    
        void modifyZone(double date, FAUSTFLOAT v)
        {
            if (fInputCtrl) {
                fState = !fState;
                uiMidiTimedItem::modifyZone(date, FAUSTFLOAT(fState));
            }
        }

};

/**
 * Standard MIDI events.
 */

/**
 * uiMidiProgChange uses the [min...max] range.
 */
class uiMidiProgChange : public uiMidiTimedItem {
    
    public:
    
        FAUSTFLOAT fMin, fMax;
    
        uiMidiProgChange(midi* midi_out, GUI* ui, FAUSTFLOAT* zone,
                         FAUSTFLOAT min, FAUSTFLOAT max,
                         bool input = true, int chan = 0)
            :uiMidiTimedItem(midi_out, ui, zone, input, chan), fMin(min), fMax(max)
        {}
        virtual ~uiMidiProgChange()
        {}
        
        virtual void reflectZone()
        {
            FAUSTFLOAT v = *fZone;
            fCache = v;
            if (inRange(fMin, fMax, v)) {
                if (fChan == 0) {
                    // Send on [0..15] channels on the MIDI layer
                    for (int chan = 0; chan < 16; chan++) {
                        fMidiOut->progChange(chan, v);
                    }
                } else {
                    fMidiOut->progChange(fChan - 1, v);
                }
            }
        }
    
        void modifyZone(FAUSTFLOAT v)
        {
            if (fInputCtrl && inRange(fMin, fMax, v)) {
                uiItem::modifyZone(v);
            }
        }
    
        void modifyZone(double date, FAUSTFLOAT v)
        {
            if (fInputCtrl && inRange(fMin, fMax, v)) {
                uiMidiTimedItem::modifyZone(date, v);
            }
        }
        
};

/**
 * uiMidiChanPress.
 */
class uiMidiChanPress : public uiMidiTimedItem, public uiConverter {
    
    public:
    
        uiMidiChanPress(midi* midi_out, GUI* ui,
                        FAUSTFLOAT* zone,
                        FAUSTFLOAT min, FAUSTFLOAT max,
                        bool input = true,
                        MetaDataUI::Scale scale = MetaDataUI::kLin,
                        int chan = 0)
            :uiMidiTimedItem(midi_out, ui, zone, input, chan), uiConverter(scale, 0., 127., min, max)
        {}
        virtual ~uiMidiChanPress()
        {}
        
        virtual void reflectZone()
        {
            FAUSTFLOAT v = *fZone;
            fCache = v;
            int conv = std::round(fConverter->faust2ui(v));
            if (fChan == 0) {
                // Send on [0..15] channels on the MIDI layer
                for (int chan = 0; chan < 16; chan++) {
                    fMidiOut->chanPress(chan, conv);
                }
            } else {
                fMidiOut->chanPress(fChan - 1, conv);
            }
        }
    
        void modifyZone(FAUSTFLOAT v)
        {
            if (fInputCtrl) {
                uiItem::modifyZone(FAUSTFLOAT(fConverter->ui2faust(v)));
            }
        }
    
        void modifyZone(double date, FAUSTFLOAT v)
        {
            if (fInputCtrl) {
                uiMidiTimedItem::modifyZone(date, FAUSTFLOAT(fConverter->ui2faust(v)));
            }
        }
        
};

/**
 * uiMidiCtrlChange does scale (kLin/kLog/kExp) mapping.
 */
class uiMidiCtrlChange : public uiMidiTimedItem, public uiConverter {
    
    private:
    
        int fCtrl;
 
    public:

        uiMidiCtrlChange(midi* midi_out, int ctrl, GUI* ui,
                     FAUSTFLOAT* zone,
                     FAUSTFLOAT min, FAUSTFLOAT max,
                     bool input = true,
                     MetaDataUI::Scale scale = MetaDataUI::kLin,
                     int chan = 0)
            :uiMidiTimedItem(midi_out, ui, zone, input, chan), uiConverter(scale, 0., 127., min, max), fCtrl(ctrl)
        {}
        virtual ~uiMidiCtrlChange()
        {}
        
        virtual void reflectZone()
        {
            FAUSTFLOAT v = *fZone;
            fCache = v;
            int conv = std::round(fConverter->faust2ui(v));
            if (fChan == 0) {
                // Send on [0..15] channels on the MIDI layer
                for (int chan = 0; chan < 16; chan++) {
                    fMidiOut->ctrlChange(chan, fCtrl, conv);
                }
            } else {
                fMidiOut->ctrlChange(fChan - 1, fCtrl, conv);
            }
        }
        
        void modifyZone(FAUSTFLOAT v)
        {
            if (fInputCtrl) {
                uiItem::modifyZone(FAUSTFLOAT(fConverter->ui2faust(v)));
            }
        }
    
        void modifyZone(double date, FAUSTFLOAT v)
        {
            if (fInputCtrl) {
                uiMidiTimedItem::modifyZone(date, FAUSTFLOAT(fConverter->ui2faust(v)));
            }
        }
};

// Use a two segments linear converter
class uiMidiPitchWheel : public uiMidiTimedItem {

    private:
    
        LinearValueConverter2 fConverter;
    
    public:
    
        uiMidiPitchWheel(midi* midi_out, GUI* ui, FAUSTFLOAT* zone,
                         FAUSTFLOAT min, FAUSTFLOAT max,
                         bool input = true, int chan = 0)
            :uiMidiTimedItem(midi_out, ui, zone, input, chan)
        {
            if (min <= 0 && max >= 0) {
                fConverter = LinearValueConverter2(0., 8191., 16383., double(min), 0., double(max));
            } else {
                // Degenerated case...
                fConverter = LinearValueConverter2(0., 8191., 16383., double(min),double(min + (max - min)/FAUSTFLOAT(2)), double(max));
            }
        }
    
        virtual ~uiMidiPitchWheel()
        {}
        
        virtual void reflectZone()
        {
            FAUSTFLOAT v = *fZone;
            fCache = v;
            int conv = std::round(fConverter.faust2ui(v));
            if (fChan == 0) {
                // Send on [0..15] channels on the MIDI layer
                for (int chan = 0; chan < 16; chan++) {
                    fMidiOut->pitchWheel(chan, conv);
                }
            } else {
                fMidiOut->pitchWheel(fChan - 1, conv);
            }
        }
        
        void modifyZone(FAUSTFLOAT v)
        { 
            if (fInputCtrl) {
                uiItem::modifyZone(FAUSTFLOAT(fConverter.ui2faust(v)));
            }
        }
    
        void modifyZone(double date, FAUSTFLOAT v)
        {
            if (fInputCtrl) {
                uiMidiTimedItem::modifyZone(FAUSTFLOAT(fConverter.ui2faust(v)));
            }
        }
    
        void setRange(int val)
        {
            double semi = (val / 128) + ((val % 128) / 100.);
            fConverter.setMappingValues(0., 8191., 16383., -semi, 0., semi);
        }
 
};

/**
 * uiMidiKeyOn does scale (kLin/kLog/kExp) mapping for velocity.
 */
class uiMidiKeyOn : public uiMidiTimedItem, public uiConverter {

    private:
        
        int fKeyOn;
  
    public:
    
        uiMidiKeyOn(midi* midi_out, int key, GUI* ui,
                    FAUSTFLOAT* zone,
                    FAUSTFLOAT min, FAUSTFLOAT max,
                    bool input = true,
                    MetaDataUI::Scale scale = MetaDataUI::kLin,
                    int chan = 0)
            :uiMidiTimedItem(midi_out, ui, zone, input, chan), uiConverter(scale, 0., 127., min, max), fKeyOn(key)
        {}
        virtual ~uiMidiKeyOn()
        {}
        
        virtual void reflectZone()
        {
            FAUSTFLOAT v = *fZone;
            fCache = v;
            int conv = std::round(fConverter->faust2ui(v));
            if (fChan == 0) {
                // Send on [0..15] channels on the MIDI layer
                for (int chan = 0; chan < 16; chan++) {
                    fMidiOut->keyOn(chan, fKeyOn, conv);
                }
            } else {
                fMidiOut->keyOn(fChan - 1, fKeyOn, conv);
            }
        }
        
        void modifyZone(FAUSTFLOAT v)
        { 
            if (fInputCtrl) {
                uiItem::modifyZone(FAUSTFLOAT(fConverter->ui2faust(v)));
            }
        }
    
        void modifyZone(double date, FAUSTFLOAT v)
        {
            if (fInputCtrl) {
                uiMidiTimedItem::modifyZone(date, FAUSTFLOAT(fConverter->ui2faust(v)));
            }
        }
    
};

/**
 * uiMidiKeyOff does scale (kLin/kLog/kExp) mapping for velocity.
 */
class uiMidiKeyOff : public uiMidiTimedItem, public uiConverter {

    private:
        
        int fKeyOff;
  
    public:
    
        uiMidiKeyOff(midi* midi_out, int key, GUI* ui,
                     FAUSTFLOAT* zone,
                     FAUSTFLOAT min, FAUSTFLOAT max,
                     bool input = true,
                     MetaDataUI::Scale scale = MetaDataUI::kLin,
                     int chan = 0)
            :uiMidiTimedItem(midi_out, ui, zone, input, chan), uiConverter(scale, 0., 127., min, max), fKeyOff(key)
        {}
        virtual ~uiMidiKeyOff()
        {}
        
        virtual void reflectZone()
        {
            FAUSTFLOAT v = *fZone;
            fCache = v;
            int conv = std::round(fConverter->faust2ui(v));
            if (fChan == 0) {
                // Send on [0..15] channels on the MIDI layer
                for (int chan = 0; chan < 16; chan++) {
                    fMidiOut->keyOff(chan, fKeyOff, conv);
                }
            } else {
                fMidiOut->keyOff(fChan - 1, fKeyOff, conv);
            }
        }
        
        void modifyZone(FAUSTFLOAT v)
        { 
            if (fInputCtrl) {
                uiItem::modifyZone(FAUSTFLOAT(fConverter->ui2faust(v)));
            }
        }
    
        void modifyZone(double date, FAUSTFLOAT v)
        {
            if (fInputCtrl) {
                uiMidiTimedItem::modifyZone(date, FAUSTFLOAT(fConverter->ui2faust(v)));
            }
        }
    
};

/**
 * uiMidiKeyPress does scale (kLin/kLog/kExp) mapping for velocity.
 */
class uiMidiKeyPress : public uiMidiTimedItem, public uiConverter {

    private:
    
        int fKey;
  
    public:
    
        uiMidiKeyPress(midi* midi_out, int key, GUI* ui,
                       FAUSTFLOAT* zone,
                       FAUSTFLOAT min, FAUSTFLOAT max,
                       bool input = true,
                       MetaDataUI::Scale scale = MetaDataUI::kLin,
                       int chan = 0)
            :uiMidiTimedItem(midi_out, ui, zone, input, chan), uiConverter(scale, 0., 127., min, max), fKey(key)
        {}
        virtual ~uiMidiKeyPress()
        {}
        
        virtual void reflectZone()
        {
            FAUSTFLOAT v = *fZone;
            fCache = v;
            int conv = std::round(fConverter->faust2ui(v));
            if (fChan == 0) {
                // Send on [0..15] channels on the MIDI layer
                for (int chan = 0; chan < 16; chan++) {
                    fMidiOut->keyPress(chan, fKey, conv);
                }
            } else {
                fMidiOut->keyPress(fChan - 1, fKey, conv);
            }
        }
        
        void modifyZone(FAUSTFLOAT v)
        { 
            if (fInputCtrl) {
                uiItem::modifyZone(FAUSTFLOAT(fConverter->ui2faust(v)));
            }
        }
    
        void modifyZone(double date, FAUSTFLOAT v)
        {
            if (fInputCtrl) {
                uiMidiTimedItem::modifyZone(date, FAUSTFLOAT(fConverter->ui2faust(v)));
            }
        }
    
};

/******************************************************************************************
 * MidiUI : Faust User Interface
 * This class decodes MIDI metadata and maps incoming MIDI messages to them.
 * Currently ctrlChange, keyOn/keyOff, keyPress, progChange, chanPress, pitchWheel/pitchBend
 * start/stop/clock meta data are handled.
 *
 * Maps associating MIDI event ID (like each ctrl number) with all MIDI aware UI items
 * are defined and progressively filled when decoding MIDI related metadata.
 * MIDI aware UI items are used in both directions:
 *  - modifying their internal state when receving MIDI input events
 *  - sending their internal state as MIDI output events
 *******************************************************************************************/

class MidiUI : public GUI, public midi, public midi_interface, public MetaDataUI {

    // Add uiItem subclasses objects are deallocated by the inherited GUI class
    typedef std::map <int, std::vector<uiMidiCtrlChange*> > TCtrlChangeTable;
    typedef std::vector<uiMidiProgChange*>                  TProgChangeTable;
    typedef std::vector<uiMidiChanPress*>                   TChanPressTable;
    typedef std::map <int, std::vector<uiMidiKeyOn*> >      TKeyOnTable;
    typedef std::map <int, std::vector<uiMidiKeyOff*> >     TKeyOffTable;
    typedef std::map <int, std::vector<uiMidiKeyPress*> >   TKeyPressTable;
    typedef std::vector<uiMidiPitchWheel*>                  TPitchWheelTable;
    
    protected:
    
        TCtrlChangeTable fCtrlChangeTable;
        TProgChangeTable fProgChangeTable;
        TChanPressTable  fChanPressTable;
        TKeyOnTable      fKeyOnTable;
        TKeyOffTable     fKeyOffTable;
        TKeyOnTable      fKeyTable;
        TKeyPressTable   fKeyPressTable;
        TPitchWheelTable fPitchWheelTable;
        
        std::vector<uiMidiStart*> fStartTable;
        std::vector<uiMidiStop*>  fStopTable;
        std::vector<uiMidiClock*> fClockTable;
        
        std::vector<std::pair <std::string, std::string> > fMetaAux;
        
        midi_handler* fMidiHandler;
        bool fDelete;
        bool fTimeStamp;
    
        void addGenericZone(FAUSTFLOAT* zone, FAUSTFLOAT min, FAUSTFLOAT max, bool input = true)
        {
            if (fMetaAux.size() > 0) {
                for (size_t i = 0; i < fMetaAux.size(); i++) {
                    unsigned num;
                    unsigned chan;
                    if (fMetaAux[i].first == "midi") {
                        if (gsscanf(fMetaAux[i].second.c_str(), "ctrl %u %u", &num, &chan) == 2) {
                            fCtrlChangeTable[num].push_back(new uiMidiCtrlChange(fMidiHandler, num, this, zone, min, max, input, getScale(zone), chan));
                        } else if (gsscanf(fMetaAux[i].second.c_str(), "ctrl %u", &num) == 1) {
                            fCtrlChangeTable[num].push_back(new uiMidiCtrlChange(fMidiHandler, num, this, zone, min, max, input, getScale(zone)));
                        } else if (gsscanf(fMetaAux[i].second.c_str(), "keyon %u %u", &num, &chan) == 2) {
                            fKeyOnTable[num].push_back(new uiMidiKeyOn(fMidiHandler, num, this, zone, min, max, input, getScale(zone), chan));
                        } else if (gsscanf(fMetaAux[i].second.c_str(), "keyon %u", &num) == 1) {
                            fKeyOnTable[num].push_back(new uiMidiKeyOn(fMidiHandler, num, this, zone, min, max, input, getScale(zone)));
                        } else if (gsscanf(fMetaAux[i].second.c_str(), "keyoff %u %u", &num, &chan) == 2) {
                            fKeyOffTable[num].push_back(new uiMidiKeyOff(fMidiHandler, num, this, zone, min, max, input, getScale(zone), chan));
                        } else if (gsscanf(fMetaAux[i].second.c_str(), "keyoff %u", &num) == 1) {
                            fKeyOffTable[num].push_back(new uiMidiKeyOff(fMidiHandler, num, this, zone, min, max, input, getScale(zone)));
                        } else if (gsscanf(fMetaAux[i].second.c_str(), "key %u %u", &num, &chan) == 2) {
                            fKeyTable[num].push_back(new uiMidiKeyOn(fMidiHandler, num, this, zone, min, max, input, getScale(zone), chan));
                        } else if (gsscanf(fMetaAux[i].second.c_str(), "key %u", &num) == 1) {
                            fKeyTable[num].push_back(new uiMidiKeyOn(fMidiHandler, num, this, zone, min, max, input, getScale(zone)));
                        } else if (gsscanf(fMetaAux[i].second.c_str(), "keypress %u %u", &num, &chan) == 2) {
                            fKeyPressTable[num].push_back(new uiMidiKeyPress(fMidiHandler, num, this, zone, min, max, input, getScale(zone), chan));
                        } else if (gsscanf(fMetaAux[i].second.c_str(), "keypress %u", &num) == 1) {
                            fKeyPressTable[num].push_back(new uiMidiKeyPress(fMidiHandler, num, this, zone, min, max, input, getScale(zone)));
                        } else if (gsscanf(fMetaAux[i].second.c_str(), "pgm %u", &chan) == 1) {
                            fProgChangeTable.push_back(new uiMidiProgChange(fMidiHandler, this, zone, min, max, input, chan));
                        } else if (strcmp(fMetaAux[i].second.c_str(), "pgm") == 0) {
                            fProgChangeTable.push_back(new uiMidiProgChange(fMidiHandler, this, zone, min, max, input));
                        } else if (gsscanf(fMetaAux[i].second.c_str(), "chanpress %u", &chan) == 1) {
                            fChanPressTable.push_back(new uiMidiChanPress(fMidiHandler, this, zone, min, max, input, getScale(zone), chan));
                        } else if ((fMetaAux[i].second == "chanpress")) {
                            fChanPressTable.push_back(new uiMidiChanPress(fMidiHandler, this, zone, min, max, input, getScale(zone)));
                        } else if ((gsscanf(fMetaAux[i].second.c_str(), "pitchwheel %u", &chan) == 1) || (gsscanf(fMetaAux[i].second.c_str(), "pitchbend %u", &chan) == 1)) {
                            fPitchWheelTable.push_back(new uiMidiPitchWheel(fMidiHandler, this, zone, min, max, input, chan));
                        } else if ((fMetaAux[i].second == "pitchwheel") || (fMetaAux[i].second == "pitchbend")) {
                            fPitchWheelTable.push_back(new uiMidiPitchWheel(fMidiHandler, this, zone, min, max, input));
                        // MIDI sync
                        } else if (fMetaAux[i].second == "start") {
                            fStartTable.push_back(new uiMidiStart(fMidiHandler, this, zone, input));
                        } else if (fMetaAux[i].second == "stop") {
                            fStopTable.push_back(new uiMidiStop(fMidiHandler, this, zone, input));
                        } else if (fMetaAux[i].second == "clock") {
                            fClockTable.push_back(new uiMidiClock(fMidiHandler, this, zone, input));
                        // Explicit metadata to activate 'timestamp' mode
                        } else if (fMetaAux[i].second == "timestamp") {
                            fTimeStamp = true;
                        }
                    }
                }
            }
            fMetaAux.clear();
        }
    
        template <typename TABLE>
        void updateTable1(TABLE& table, double date, int channel, int val1)
        {
            for (size_t i = 0; i < table.size(); i++) {
                int channel_aux = table[i]->fChan;
                // channel_aux == 0 means "all channels"
                if (channel_aux == 0 || channel == channel_aux - 1) {
                    if (fTimeStamp) {
                        table[i]->modifyZone(date, FAUSTFLOAT(val1));
                    } else {
                        table[i]->modifyZone(FAUSTFLOAT(val1));
                    }
                }
            }
        }
        
        template <typename TABLE>
        void updateTable2(TABLE& table, double date, int channel, int val1, int val2)
        {
            if (table.find(val1) != table.end()) {
                for (size_t i = 0; i < table[val1].size(); i++) {
                    int channel_aux = table[val1][i]->fChan;
                    // channel_aux == 0 means "all channels"
                    if (channel_aux == 0 || channel == channel_aux - 1) {
                        if (fTimeStamp) {
                            table[val1][i]->modifyZone(date, FAUSTFLOAT(val2));
                        } else {
                            table[val1][i]->modifyZone(FAUSTFLOAT(val2));
                        }
                    }
                }
            }
        }
    
    public:
    
        MidiUI(midi_handler* midi_handler, bool delete_handler = false)
        {
            fMidiHandler = midi_handler;
            fMidiHandler->addMidiIn(this);
            // TODO: use shared_ptr based implementation
            fDelete = delete_handler;
            fTimeStamp = false;
        }
 
        virtual ~MidiUI() 
        {
            // Remove from fMidiHandler
            fMidiHandler->removeMidiIn(this);
            // TODO: use shared_ptr based implementation
            if (fDelete) delete fMidiHandler;
        }
    
        bool run() { return fMidiHandler->startMidi(); }
        void stop() { fMidiHandler->stopMidi(); }
        
        void addMidiIn(midi* midi_dsp) { fMidiHandler->addMidiIn(midi_dsp); }
        void removeMidiIn(midi* midi_dsp) { fMidiHandler->removeMidiIn(midi_dsp); }
      
        // -- active widgets
        
        virtual void addButton(const char* label, FAUSTFLOAT* zone)
        {
            addGenericZone(zone, FAUSTFLOAT(0), FAUSTFLOAT(1));
        }
        virtual void addCheckButton(const char* label, FAUSTFLOAT* zone)
        {
            addGenericZone(zone, FAUSTFLOAT(0), FAUSTFLOAT(1));
        }
        
        virtual void addVerticalSlider(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step)
        {
            addGenericZone(zone, min, max);
        }
        virtual void addHorizontalSlider(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step)
        {
            addGenericZone(zone, min, max);
        }
        virtual void addNumEntry(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step)
        {
            addGenericZone(zone, min, max);
        }

        // -- passive widgets

        virtual void addHorizontalBargraph(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT min, FAUSTFLOAT max) 
        {
            addGenericZone(zone, min, max, false);
        }
        virtual void addVerticalBargraph(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT min, FAUSTFLOAT max)
        {
            addGenericZone(zone, min, max, false);
        }

        // -- metadata declarations

        virtual void declare(FAUSTFLOAT* zone, const char* key, const char* val)
        {
            MetaDataUI::declare(zone, key, val);
            fMetaAux.push_back(std::make_pair(key, val));
        }
        
        // -- MIDI API
    
        void key(double date, int channel, int note, int velocity)
        {
            updateTable2<TKeyOnTable>(fKeyTable, date, channel, note, velocity);
        }
    
        MapUI* keyOn(double date, int channel, int note, int velocity)
        {
            updateTable2<TKeyOnTable>(fKeyOnTable, date, channel, note, velocity);
            // If note is in fKeyTable, handle it as a keyOn
            key(date, channel, note, velocity);
            return nullptr;
        }
        
        void keyOff(double date, int channel, int note, int velocity)
        {
            updateTable2<TKeyOffTable>(fKeyOffTable, date, channel, note, velocity);
            // If note is in fKeyTable, handle it as a keyOff with a 0 velocity
            key(date, channel, note, 0);
        }
        
        void ctrlChange(double date, int channel, int ctrl, int value)
        {
            updateTable2<TCtrlChangeTable>(fCtrlChangeTable, date, channel, ctrl, value);
        }
    
        void rpn(double date, int channel, int ctrl, int value)
        {
            if (ctrl == midi::PITCH_BEND_RANGE) {
                for (size_t i = 0; i < fPitchWheelTable.size(); i++) {
                    // channel_aux == 0 means "all channels"
                    int channel_aux = fPitchWheelTable[i]->fChan;
                    if (channel_aux == 0 || channel == channel_aux - 1) {
                        fPitchWheelTable[i]->setRange(value);
                    }
                }
            }
        }
    
        void progChange(double date, int channel, int pgm)
        {
            updateTable1<TProgChangeTable>(fProgChangeTable, date, channel, pgm);
        }
        
        void pitchWheel(double date, int channel, int wheel) 
        {
            updateTable1<TPitchWheelTable>(fPitchWheelTable, date, channel, wheel);
        }
        
        void keyPress(double date, int channel, int pitch, int press) 
        {
            updateTable2<TKeyPressTable>(fKeyPressTable, date, channel, pitch, press);
        }
        
        void chanPress(double date, int channel, int press)
        {
            updateTable1<TChanPressTable>(fChanPressTable, date, channel, press);
        }
        
        void ctrlChange14bits(double date, int channel, int ctrl, int value) {}
        
        // MIDI sync
        
        void startSync(double date)
        {
            for (size_t i = 0; i < fStartTable.size(); i++) {
                fStartTable[i]->modifyZone(date, FAUSTFLOAT(1));
            }
        }
        
        void stopSync(double date)
        {
            for (size_t i = 0; i < fStopTable.size(); i++) {
                fStopTable[i]->modifyZone(date, FAUSTFLOAT(0));
            }
        }
        
        void clock(double date)
        {
            for (size_t i = 0; i < fClockTable.size(); i++) {
                fClockTable[i]->modifyZone(date, FAUSTFLOAT(1));
            }
        }
};

#endif // FAUST_MIDIUI_H
/**************************  END  MidiUI.h **************************/
/************************** BEGIN mspUI.h **************************
 FAUST Architecture File
 Copyright (C) 2003-2022 GRAME, Centre National de Creation Musicale
 ---------------------------------------------------------------------
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation; either version 2.1 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 
 EXCEPTION : As a special exception, you may create a larger work
 that contains this FAUST architecture section and distribute
 that work under terms of your choice, so long as this FAUST
 architecture section is not modified.
 
 mspUI.h for static Max/MSP externals and faustgen~
 Created by Martin Di Rollo on 18/04/12.
 ********************************************************************/

#ifndef _mspUI_h
#define _mspUI_h

#include <math.h>
#include <assert.h>
#include <string>
#include <map>


#define STR_SIZE    512
#define MULTI_SIZE  256

#ifdef WIN32
#include <stdio.h>
#define snprintf _snprintf
#ifndef NAN
    static const unsigned long __nan[2] = {0xffffffff, 0x7fffffff};
    #define NAN (*(const float *) __nan)
#endif
#endif

struct Max_Meta1 : Meta
{
    int fCount;
    
    Max_Meta1():fCount(0)
    {}
    
    void declare(const char* key, const char* value)
    {
        if ((strcmp("name", key) == 0) || (strcmp("author", key) == 0)) {
            fCount++;
        }
    }
};

struct Max_Meta2 : Meta
{
    void declare(const char* key, const char* value)
    {
        if ((strcmp("name", key) == 0) || (strcmp("author", key) == 0)) {
            post("%s : %s", key, value);
        }
    }
};

struct Max_Meta3 : Meta
{
    std::string fName;
    
    bool endWith(const std::string& str, const std::string& suffix)
    {
        size_t i = str.rfind(suffix);
        return (i != std::string::npos) && (i == (str.length() - suffix.length()));
    }
    
    void declare(const char* key, const char* value)
    {
        if ((strcmp("filename", key) == 0)) {
            std::string val = value;
            if (endWith(value, ".dsp")) {
                fName = "com.grame." + val.substr(0, val.size() - 4) + "~";
            } else {
                fName = "com.grame." + val + "~";
            }
        }
    }
};

class mspUIObject {
    
    protected:
        
        std::string fLabel;
        FAUSTFLOAT* fZone;
        
        FAUSTFLOAT range(FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT val) {return (val < min) ? min : (val > max) ? max : val;}
        
    public:
        
        mspUIObject(const std::string& label, FAUSTFLOAT* zone):fLabel(label),fZone(zone) {}
        virtual ~mspUIObject() {}
        
        virtual void setValue(FAUSTFLOAT f) { *fZone = range(0.0, 1.0, f); }
        virtual FAUSTFLOAT getValue() { return *fZone; }
    
        virtual FAUSTFLOAT getInitValue() { return FAUSTFLOAT(0); }
        virtual FAUSTFLOAT getMinValue() { return FAUSTFLOAT(0); }
        virtual FAUSTFLOAT getMaxValue() { return FAUSTFLOAT(0); }
    
        virtual void toString(char* buffer) {}
        virtual std::string getName() { return fLabel; }
};

class mspCheckButton : public mspUIObject {
    
    public:
        
        mspCheckButton(const std::string& label, FAUSTFLOAT* zone):mspUIObject(label,zone) {}
        virtual ~mspCheckButton() {}
        
        void toString(char* buffer)
        {
            snprintf(buffer, STR_SIZE, "CheckButton(float): %s", fLabel.c_str());
        }
};

class mspButton : public mspUIObject {
    
    public:
        
        mspButton(const std::string& label, FAUSTFLOAT* zone):mspUIObject(label,zone) {}
        virtual ~mspButton() {}
        
        void toString(char* buffer)
        {
            snprintf(buffer, STR_SIZE, "Button(float): %s", fLabel.c_str());
        }
};

class mspSlider : public mspUIObject {
    
    private:
        
        FAUSTFLOAT fInit;
        FAUSTFLOAT fMin;
        FAUSTFLOAT fMax;
        FAUSTFLOAT fStep;
        
    public:
        
        mspSlider(const std::string& label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step)
        :mspUIObject(label,zone),fInit(init),fMin(min),fMax(max),fStep(step) {}
        virtual ~mspSlider() {}
        
        void toString(char* buffer)
        {
            std::stringstream str;
            str << "Slider(float): " << fLabel << " [init=" << fInit << ":min=" << fMin << ":max=" << fMax << ":step=" << fStep << ":cur=" << *fZone << "]";
            std::string res = str.str();
            snprintf(buffer, STR_SIZE, "%s", res.c_str());
        }
        
        void setValue(FAUSTFLOAT f) { *fZone = range(fMin, fMax, f); }
    
        virtual FAUSTFLOAT getInitValue() { return fInit; }
        virtual FAUSTFLOAT getMinValue() { return fMin; }
        virtual FAUSTFLOAT getMaxValue() { return fMax; }
    
};

class mspBargraph : public mspUIObject {
    
    private:
        
        FAUSTFLOAT fMin;
        FAUSTFLOAT fMax;
        FAUSTFLOAT fCurrent;
        
    public:
        
        mspBargraph(const std::string& label, FAUSTFLOAT* zone, FAUSTFLOAT min, FAUSTFLOAT max)
        :mspUIObject(label,zone), fMin(min), fMax(max), fCurrent(*zone) {}
        virtual ~mspBargraph() {}
        
        void toString(char* buffer)
        {
            std::stringstream str;
            str << "Bargraph(float): " << fLabel << " [min=" << fMin << ":max=" << fMax << ":cur=" << *fZone << "]";
            std::string res = str.str();
            snprintf(buffer, STR_SIZE, "%s", res.c_str());
        }
    
        // special version
        virtual FAUSTFLOAT getValue(bool& new_val)
        {
            if (*fZone != fCurrent) {
                fCurrent = *fZone;
                new_val = true;
            } else {
                new_val = false;
            }
            return fCurrent;
        }
    
        virtual FAUSTFLOAT getMinValue() { return fMin; }
        virtual FAUSTFLOAT getMaxValue() { return fMax; }
    
};

class mspUI : public UI, public PathBuilder
{
    private:
        
        std::map<std::string, mspUIObject*> fInputLabelTable;      // Input table using labels
        std::map<std::string, mspUIObject*> fInputShortnameTable;  // Input table using shortnames
        std::map<std::string, mspUIObject*> fInputPathTable;       // Input table using paths
        std::map<std::string, mspUIObject*> fOutputLabelTable;     // Table containing bargraph with labels
        std::map<std::string, mspUIObject*> fOutputShortnameTable; // Table containing bargraph with shortnames
        std::map<std::string, mspUIObject*> fOutputPathTable;      // Table containing bargraph with paths
        
        std::map<const char*, const char*> fDeclareTable;
        
        FAUSTFLOAT* fMultiTable[MULTI_SIZE];
        int fMultiIndex;
        int fMultiControl;
        
        std::string createLabel(const char* label)
        {
            std::map<const char*, const char*>::reverse_iterator it;
            if (fDeclareTable.size() > 0) {
                std::string res = std::string(label);
                char sep = '[';
                for (it = fDeclareTable.rbegin(); it != fDeclareTable.rend(); it++) {
                    res = res + sep + (*it).first + ":" + (*it).second;
                    sep = ',';
                }
                res += ']';
                fDeclareTable.clear();
                return res;
            } else {
                return std::string(label);
            }
        }
    
        void addSlider(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step)
        {
            mspUIObject* obj = new mspSlider(createLabel(label), zone, init, min, max, step);
            fInputLabelTable[std::string(label)] = obj;
            std::string path = buildPath(label);
            fInputPathTable[path] = obj;
            fFullPaths.push_back(path);
            fDeclareTable.clear();
        }
    
        void addBargraph(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT min, FAUSTFLOAT max)
        {
            mspUIObject* obj = new mspBargraph(createLabel(label), zone, min, max);
            fOutputLabelTable[std::string(label)] = obj;
            std::string path = buildPath(label);
            fOutputPathTable[path] = obj;
            fFullPaths.push_back(path);
            fDeclareTable.clear();
        }
    
    public:
        
        typedef std::map<std::string, mspUIObject*>::iterator iterator;
        
        mspUI()
        {
            for (int i = 0; i < MULTI_SIZE; i++) {
                fMultiTable[i] = 0;
            }
            fMultiIndex = fMultiControl = 0;
        }
    
        virtual ~mspUI()
        {
            clear();
        }
        
        void addButton(const char* label, FAUSTFLOAT* zone)
        {
            mspUIObject* obj = new mspButton(createLabel(label), zone);
            fInputLabelTable[std::string(label)] = obj;
            std::string path = buildPath(label);
            fInputPathTable[path] = obj;
            fFullPaths.push_back(path);
        }
        
        void addCheckButton(const char* label, FAUSTFLOAT* zone)
        {
            mspUIObject* obj = new mspCheckButton(createLabel(label), zone);
            fInputLabelTable[std::string(label)] = obj;
            std::string path = buildPath(label);
            fInputPathTable[path] = obj;
            fFullPaths.push_back(path);
        }
        
        void addVerticalSlider(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step)
        {
            addSlider(label, zone, init, min, max, step);
        }
        
        void addHorizontalSlider(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step)
        {
            addSlider(label, zone, init, min, max, step);
        }
        
        void addNumEntry(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step)
        {
            addSlider(label, zone, init, min, max, step);
        }
    
        void addHorizontalBargraph(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT min, FAUSTFLOAT max)
        {
            addBargraph(label, zone, min, max);
        }
    
        void addVerticalBargraph(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT min, FAUSTFLOAT max)
        {
            addBargraph(label, zone, min, max);
        }
        
        void addSoundfile(const char* label, const char* filename, Soundfile** sf_zone) {}
        
        void openTabBox(const char* label) { pushLabel(label); fDeclareTable.clear(); }
        void openHorizontalBox(const char* label) { pushLabel(label); fDeclareTable.clear(); }
        void openVerticalBox(const char* label) { pushLabel(label); fDeclareTable.clear(); }
        void closeBox()
        {
            fDeclareTable.clear();
            if (popLabel()) {
                // Shortnames can be computed when all fullnames are known
                computeShortNames();
                // Fill 'shortname' map
                for (const auto& path : fFullPaths) {
                    if (fInputPathTable.count(path)) {
                        fInputShortnameTable[fFull2Short[path]] = fInputPathTable[path];
                    } else if (fOutputPathTable.count(path)) {
                        fOutputShortnameTable[fFull2Short[path]] = fOutputPathTable[path];
                    } else {
                        assert(false);
                    }
                }
             }
        }
        
        virtual void declare(FAUSTFLOAT* zone, const char* key, const char* val)
        {
            if (strcmp(key, "multi") == 0) {
                int index = atoi(val);
                if (index >= 0 && index < MULTI_SIZE) {
                    fMultiTable[index] = zone;
                    fMultiControl++;
                } else {
                    post("Invalid multi index = %d", index);
                }
            }
            
            fDeclareTable[key] = val;
        }
        
        void setMultiValues(FAUSTFLOAT* multi, int buffer_size)
        {
            for (int read = 0; read < buffer_size; read++) {
                int write = (fMultiIndex + read) & (MULTI_SIZE - 1);
                if (fMultiTable[write]) {
                    *fMultiTable[write] = multi[read];
                }
            }
            fMultiIndex += buffer_size;
        }
        
        bool isMulti() { return fMultiControl > 0; }
        
        bool isValue(const std::string& name)
        {
            return (isOutputValue(name) || isInputValue(name));
        }
    
        bool isInputValue(const std::string& name)
        {
            return fInputLabelTable.count(name) || fInputShortnameTable.count(name) || fInputPathTable.count(name);
        }
    
        bool isOutputValue(const std::string& name)
        {
            return fOutputLabelTable.count(name) || fOutputShortnameTable.count(name) || fOutputPathTable.count(name);
        }
    
        bool setValue(const std::string& name, FAUSTFLOAT val)
        {
            if (fInputLabelTable.count(name)) {
                fInputLabelTable[name]->setValue(val);
                return true;
            } else if (fInputShortnameTable.count(name)) {
                fInputShortnameTable[name]->setValue(val);
                return true;
            } else if (fInputPathTable.count(name)) {
                fInputPathTable[name]->setValue(val);
                return true;
            } else {
                return false;
            }
        }
    
        FAUSTFLOAT getOutputValue(const std::string& name, bool& new_val)
        {
            return static_cast<mspBargraph*>(fOutputPathTable[name])->getValue(new_val);
        }
        
        iterator begin1() { return fInputLabelTable.begin(); }
        iterator end1() { return fInputLabelTable.end(); }
        
        iterator begin2() { return fInputPathTable.begin(); }
        iterator end2() { return fInputPathTable.end(); }
    
        iterator begin3() { return fOutputLabelTable.begin(); }
        iterator end3() { return fOutputLabelTable.end(); }
    
        iterator begin4() { return fOutputPathTable.begin(); }
        iterator end4() { return fOutputPathTable.end(); }
    
        int inputItemsCount() { return fInputLabelTable.size(); }
        int outputItemsCount() { return fOutputLabelTable.size(); }
    
        void clear()
        {
            for (const auto& it : fInputLabelTable) {
                delete it.second;
            }
            fInputLabelTable.clear();
            fInputShortnameTable.clear();
            fInputPathTable.clear();
            
            for (const auto& it : fOutputLabelTable) {
                delete it.second;
            }
            fOutputLabelTable.clear();
            fOutputShortnameTable.clear();
            fOutputPathTable.clear();
        }
        
        void displayControls()
        {
            post("------- Range, shortname and path ----------");
            for (const auto& it : fInputPathTable) {
                char param[STR_SIZE];
                it.second->toString(param);
                post(param);
                std::string shortname = "Shortname: " + fFull2Short[it.first];
                post(shortname.c_str());
                std::string path = "Complete path: " + it.first;
                post(path.c_str());
            }
            post("---------------------------------------------");
        }
    
        static bool checkDigit(const std::string& name)
        {
            for (int i = name.size() - 1; i >= 0; i--) {
                if (isdigit(name[i])) { return true; }
            }
            return false;
        }
        
        static int countDigit(const std::string& name)
        {
            int count = 0;
            for (int i = name.size() - 1; i >= 0; i--) {
                if (isdigit(name[i])) { count++; }
            }
            return count;
        }

};

//==============
// MIDI handler
//==============

struct max_midi : public midi_handler {
    
    void* m_midi_outlet = NULL;
    
    max_midi(void* midi_outlet = NULL):m_midi_outlet(midi_outlet)
    {}
    
    void sendMessage(std::vector<unsigned char>& message)
    {
        assert(m_midi_outlet);
        for (int i = 0; i < message.size(); i++) {
            outlet_int(m_midi_outlet, message[i]);
        }
    }
    
    // MIDI output API
    MapUI* keyOn(int channel, int pitch, int velocity)
    {
        std::vector<unsigned char> message = {ucast(MIDI_NOTE_ON + channel), ucast(pitch), ucast(velocity)};
        sendMessage(message);
        return NULL;
    }
    
    void keyOff(int channel, int pitch, int velocity)
    {
        std::vector<unsigned char> message = {ucast(MIDI_NOTE_OFF + channel), ucast(pitch), ucast(velocity)};
        sendMessage(message);
    }
    
    void ctrlChange(int channel, int ctrl, int val)
    {
        std::vector<unsigned char> message = {ucast(MIDI_CONTROL_CHANGE + channel), ucast(ctrl), ucast(val)};
        sendMessage(message);
    }
    
    void chanPress(int channel, int press)
    {
        std::vector<unsigned char> message = {ucast(MIDI_AFTERTOUCH + channel), ucast(press)};
        sendMessage(message);
    }
    
    void progChange(int channel, int pgm)
    {
        std::vector<unsigned char> message = {ucast(MIDI_PROGRAM_CHANGE + channel), ucast(pgm)};
        sendMessage(message);
    }
    
    void keyPress(int channel, int pitch, int press)
    {
        std::vector<unsigned char> message = {ucast(MIDI_POLY_AFTERTOUCH + channel), ucast(pitch), ucast(press)};
        sendMessage(message);
    }
    
    void pitchWheel(int channel, int wheel)
    {
        // lsb 7bit, msb 7bit
        std::vector<unsigned char> message = {ucast(MIDI_PITCH_BEND + channel), ucast(wheel & 0x7F), ucast((wheel >> 7) & 0x7F)};
        sendMessage(message);
    }
    
    void ctrlChange14bits(int channel, int ctrl, int value) {}
    
    void startSync(double date)
    {
        std::vector<unsigned char> message = {ucast(MIDI_START)};
        sendMessage(message);
    }
    
    void stopSync(double date)
    {
        std::vector<unsigned char> message = {ucast(MIDI_STOP)};
        sendMessage(message);
    }
    
    void clock(double date)
    {
        std::vector<unsigned char> message = {ucast(MIDI_CLOCK)};
        sendMessage(message);
    }
    
    void sysEx(double, std::vector<unsigned char>& message)
    {
        sendMessage(message);
    }
};

#endif
/**************************  END  mspUI.h **************************/
/************************** BEGIN poly-dsp.h *************************
FAUST Architecture File
Copyright (C) 2003-2022 GRAME, Centre National de Creation Musicale
---------------------------------------------------------------------
This program is free software; you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation; either version 2.1 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

EXCEPTION : As a special exception, you may create a larger work
that contains this FAUST architecture section and distribute
that work under terms of your choice, so long as this FAUST
architecture section is not modified.
*********************************************************************/

#ifndef __poly_dsp__
#define __poly_dsp__

#include <stdio.h>
#include <string>
#include <cmath>
#include <algorithm>
#include <functional>
#include <ostream>
#include <sstream>
#include <vector>
#include <limits.h>
#include <float.h>
#include <assert.h>

/************************** BEGIN proxy-dsp.h ***************************
FAUST Architecture File
Copyright (C) 2003-2022 GRAME, Centre National de Creation Musicale
---------------------------------------------------------------------
This program is free software; you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation; either version 2.1 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

EXCEPTION : As a special exception, you may create a larger work
that contains this FAUST architecture section and distribute
that work under terms of your choice, so long as this FAUST
architecture section is not modified.
***************************************************************************/

#ifndef __proxy_dsp__
#define __proxy_dsp__

#include <vector>
#include <map>

/************************** BEGIN JSONUIDecoder.h **************************
 FAUST Architecture File
 Copyright (C) 2003-2022 GRAME, Centre National de Creation Musicale
 ---------------------------------------------------------------------
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation; either version 2.1 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 
 EXCEPTION : As a special exception, you may create a larger work
 that contains this FAUST architecture section and distribute
 that work under terms of your choice, so long as this FAUST
 architecture section is not modified.
 *************************************************************************/

#ifndef __JSONUIDecoder__
#define __JSONUIDecoder__

#include <vector>
#include <map>
#include <utility>
#include <cstdlib>
#include <sstream>
#include <functional>

/************************** BEGIN CGlue.h *****************************
FAUST Architecture File
Copyright (C) 2003-2022 GRAME, Centre National de Creation Musicale
---------------------------------------------------------------------
This program is free software; you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation; either version 2.1 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

EXCEPTION : As a special exception, you may create a larger work
that contains this FAUST architecture section and distribute
that work under terms of your choice, so long as this FAUST
architecture section is not modified.
*************************************************************************/

#ifndef CGLUE_H
#define CGLUE_H

/************************** BEGIN CInterface.h **************************
 FAUST Architecture File
 Copyright (C) 2003-2022 GRAME, Centre National de Creation Musicale
 ---------------------------------------------------------------------
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation; either version 2.1 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 
 EXCEPTION : As a special exception, you may create a larger work
 that contains this FAUST architecture section and distribute
 that work under terms of your choice, so long as this FAUST
 architecture section is not modified.
 *************************************************************************/

#ifndef CINTERFACE_H
#define CINTERFACE_H

#ifndef FAUSTFLOAT
#define FAUSTFLOAT float
#endif

#include <stdlib.h>

#ifdef __cplusplus
extern "C" {
#endif
    
struct Soundfile;

/*******************************************************************************
 * UI, Meta and MemoryManager structures for C code.
 ******************************************************************************/

// -- widget's layouts

typedef void (* openTabBoxFun) (void* ui_interface, const char* label);
typedef void (* openHorizontalBoxFun) (void* ui_interface, const char* label);
typedef void (* openVerticalBoxFun) (void* ui_interface, const char* label);
typedef void (* closeBoxFun) (void* ui_interface);

// -- active widgets

typedef void (* addButtonFun) (void* ui_interface, const char* label, FAUSTFLOAT* zone);
typedef void (* addCheckButtonFun) (void* ui_interface, const char* label, FAUSTFLOAT* zone);
typedef void (* addVerticalSliderFun) (void* ui_interface, const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step);
typedef void (* addHorizontalSliderFun) (void* ui_interface, const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step);
typedef void (* addNumEntryFun) (void* ui_interface, const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step);

// -- passive widgets

typedef void (* addHorizontalBargraphFun) (void* ui_interface, const char* label, FAUSTFLOAT* zone, FAUSTFLOAT min, FAUSTFLOAT max);
typedef void (* addVerticalBargraphFun) (void* ui_interface, const char* label, FAUSTFLOAT* zone, FAUSTFLOAT min, FAUSTFLOAT max);

// -- soundfiles
    
typedef void (* addSoundfileFun) (void* ui_interface, const char* label, const char* url, struct Soundfile** sf_zone);

typedef void (* declareFun) (void* ui_interface, FAUSTFLOAT* zone, const char* key, const char* value);

typedef struct {

    void* uiInterface;

    openTabBoxFun openTabBox;
    openHorizontalBoxFun openHorizontalBox;
    openVerticalBoxFun openVerticalBox;
    closeBoxFun closeBox;
    addButtonFun addButton;
    addCheckButtonFun addCheckButton;
    addVerticalSliderFun addVerticalSlider;
    addHorizontalSliderFun addHorizontalSlider;
    addNumEntryFun addNumEntry;
    addHorizontalBargraphFun addHorizontalBargraph;
    addVerticalBargraphFun addVerticalBargraph;
    addSoundfileFun addSoundfile;
    declareFun declare;

} UIGlue;

typedef void (* metaDeclareFun) (void* ui_interface, const char* key, const char* value);

typedef struct {

    void* metaInterface;
    
    metaDeclareFun declare;

} MetaGlue;

/***************************************
 *  Interface for the DSP object
 ***************************************/

typedef char dsp_imp;
    
typedef dsp_imp* (* newDspFun) ();
typedef void (* destroyDspFun) (dsp_imp* dsp);
typedef int (* getNumInputsFun) (dsp_imp* dsp);
typedef int (* getNumOutputsFun) (dsp_imp* dsp);
typedef void (* buildUserInterfaceFun) (dsp_imp* dsp, UIGlue* ui);
typedef int (* getSampleRateFun) (dsp_imp* dsp);
typedef void (* initFun) (dsp_imp* dsp, int sample_rate);
typedef void (* classInitFun) (int sample_rate);
typedef void (* instanceInitFun) (dsp_imp* dsp, int sample_rate);
typedef void (* instanceConstantsFun) (dsp_imp* dsp, int sample_rate);
typedef void (* instanceResetUserInterfaceFun) (dsp_imp* dsp);
typedef void (* instanceClearFun) (dsp_imp* dsp);
typedef void (* computeFun) (dsp_imp* dsp, int len, FAUSTFLOAT** inputs, FAUSTFLOAT** outputs);
typedef void (* metadataFun) (MetaGlue* meta);
    
/***************************************
 * DSP memory manager functions
 ***************************************/

typedef void* (* allocateFun) (void* manager_interface, size_t size);
typedef void (* destroyFun) (void* manager_interface, void* ptr);

typedef struct {
    
    void* managerInterface;
    
    allocateFun allocate;
    destroyFun destroy;
    
} MemoryManagerGlue;

#ifdef __cplusplus
}
#endif

#endif
/**************************  END  CInterface.h **************************/

#ifdef __cplusplus
extern "C" {
#endif

/*******************************************************************************
 * UI glue code
 ******************************************************************************/
 
class UIFloat
{

    public:

        UIFloat() {}

        virtual ~UIFloat() {}

        // -- widget's layouts

        virtual void openTabBox(const char* label) = 0;
        virtual void openHorizontalBox(const char* label) = 0;
        virtual void openVerticalBox(const char* label) = 0;
        virtual void closeBox() = 0;

        // -- active widgets

        virtual void addButton(const char* label, float* zone) = 0;
        virtual void addCheckButton(const char* label, float* zone) = 0;
        virtual void addVerticalSlider(const char* label, float* zone, float init, float min, float max, float step) = 0;
        virtual void addHorizontalSlider(const char* label, float* zone, float init, float min, float max, float step) = 0;
        virtual void addNumEntry(const char* label, float* zone, float init, float min, float max, float step) = 0;

        // -- passive widgets

        virtual void addHorizontalBargraph(const char* label, float* zone, float min, float max) = 0;
        virtual void addVerticalBargraph(const char* label, float* zone, float min, float max) = 0;
    
        // -- soundfiles
    
        virtual void addSoundfile(const char* label, const char* filename, Soundfile** sf_zone) = 0;

        // -- metadata declarations

        virtual void declare(float* zone, const char* key, const char* val) {}
};

static void openTabBoxGlueFloat(void* cpp_interface, const char* label)
{
    UIFloat* ui_interface = static_cast<UIFloat*>(cpp_interface);
    ui_interface->openTabBox(label);
}

static void openHorizontalBoxGlueFloat(void* cpp_interface, const char* label)
{
    UIFloat* ui_interface = static_cast<UIFloat*>(cpp_interface);
    ui_interface->openHorizontalBox(label);
}

static void openVerticalBoxGlueFloat(void* cpp_interface, const char* label)
{
    UIFloat* ui_interface = static_cast<UIFloat*>(cpp_interface);
    ui_interface->openVerticalBox(label);
}

static void closeBoxGlueFloat(void* cpp_interface)
{
    UIFloat* ui_interface = static_cast<UIFloat*>(cpp_interface);
    ui_interface->closeBox();
}

static void addButtonGlueFloat(void* cpp_interface, const char* label, float* zone)
{
    UIFloat* ui_interface = static_cast<UIFloat*>(cpp_interface);
    ui_interface->addButton(label, zone);
}

static void addCheckButtonGlueFloat(void* cpp_interface, const char* label, float* zone)
{
    UIFloat* ui_interface = static_cast<UIFloat*>(cpp_interface);
    ui_interface->addCheckButton(label, zone);
}

static void addVerticalSliderGlueFloat(void* cpp_interface, const char* label, float* zone, float init, float min, float max, float step)
{
    UIFloat* ui_interface = static_cast<UIFloat*>(cpp_interface);
    ui_interface->addVerticalSlider(label, zone, init, min, max, step);
}

static void addHorizontalSliderGlueFloat(void* cpp_interface, const char* label, float* zone, float init, float min, float max, float step)
{
    UIFloat* ui_interface = static_cast<UIFloat*>(cpp_interface);
    ui_interface->addHorizontalSlider(label, zone, init, min, max, step);
}

static void addNumEntryGlueFloat(void* cpp_interface, const char* label, float* zone, float init, float min, float max, float step)
{
    UIFloat* ui_interface = static_cast<UIFloat*>(cpp_interface);
    ui_interface->addNumEntry(label, zone, init, min, max, step);
}

static void addHorizontalBargraphGlueFloat(void* cpp_interface, const char* label, float* zone, float min, float max)
{
    UIFloat* ui_interface = static_cast<UIFloat*>(cpp_interface);
    ui_interface->addHorizontalBargraph(label, zone, min, max);
}

static void addVerticalBargraphGlueFloat(void* cpp_interface, const char* label, float* zone, float min, float max)
{
    UIFloat* ui_interface = static_cast<UIFloat*>(cpp_interface);
    ui_interface->addVerticalBargraph(label, zone, min, max);
}
    
static void addSoundfileGlueFloat(void* cpp_interface, const char* label, const char* url, Soundfile** sf_zone)
{
    UIFloat* ui_interface = static_cast<UIFloat*>(cpp_interface);
    ui_interface->addSoundfile(label, url, sf_zone);
}

static void declareGlueFloat(void* cpp_interface, float* zone, const char* key, const char* value)
{
    UIFloat* ui_interface = static_cast<UIFloat*>(cpp_interface);
    ui_interface->declare(zone, key, value);
}

class UIDouble
{

    public:

        UIDouble() {}

        virtual ~UIDouble() {}

        // -- widget's layouts

        virtual void openTabBox(const char* label) = 0;
        virtual void openHorizontalBox(const char* label) = 0;
        virtual void openVerticalBox(const char* label) = 0;
        virtual void closeBox() = 0;

        // -- active widgets

        virtual void addButton(const char* label, double* zone) = 0;
        virtual void addCheckButton(const char* label, double* zone) = 0;
        virtual void addVerticalSlider(const char* label, double* zone, double init, double min, double max, double step) = 0;
        virtual void addHorizontalSlider(const char* label, double* zone, double init, double min, double max, double step) = 0;
        virtual void addNumEntry(const char* label, double* zone, double init, double min, double max, double step) = 0;

        // -- passive widgets

        virtual void addHorizontalBargraph(const char* label, double* zone, double min, double max) = 0;
        virtual void addVerticalBargraph(const char* label, double* zone, double min, double max) = 0;
    
        // -- soundfiles
    
        virtual void addSoundfile(const char* label, const char* filename, Soundfile** sf_zone) = 0;

        // -- metadata declarations

        virtual void declare(double* zone, const char* key, const char* val) {}
};

static void openTabBoxGlueDouble(void* cpp_interface, const char* label)
{
    UIDouble* ui_interface = static_cast<UIDouble*>(cpp_interface);
    ui_interface->openTabBox(label);
}

static void openHorizontalBoxGlueDouble(void* cpp_interface, const char* label)
{
    UIDouble* ui_interface = static_cast<UIDouble*>(cpp_interface);
    ui_interface->openHorizontalBox(label);
}

static void openVerticalBoxGlueDouble(void* cpp_interface, const char* label)
{
    UIDouble* ui_interface = static_cast<UIDouble*>(cpp_interface);
    ui_interface->openVerticalBox(label);
}

static void closeBoxGlueDouble(void* cpp_interface)
{
    UIDouble* ui_interface = static_cast<UIDouble*>(cpp_interface);
    ui_interface->closeBox();
}

static void addButtonGlueDouble(void* cpp_interface, const char* label, double* zone)
{
    UIDouble* ui_interface = static_cast<UIDouble*>(cpp_interface);
    ui_interface->addButton(label, zone);
}

static void addCheckButtonGlueDouble(void* cpp_interface, const char* label, double* zone)
{
    UIDouble* ui_interface = static_cast<UIDouble*>(cpp_interface);
    ui_interface->addCheckButton(label, zone);
}

static void addVerticalSliderGlueDouble(void* cpp_interface, const char* label, double* zone, double init, double min, double max, double step)
{
    UIDouble* ui_interface = static_cast<UIDouble*>(cpp_interface);
    ui_interface->addVerticalSlider(label, zone, init, min, max, step);
}

static void addHorizontalSliderGlueDouble(void* cpp_interface, const char* label, double* zone, double init, double min, double max, double step)
{
    UIDouble* ui_interface = static_cast<UIDouble*>(cpp_interface);
    ui_interface->addHorizontalSlider(label, zone, init, min, max, step);
}

static void addNumEntryGlueDouble(void* cpp_interface, const char* label, double* zone, double init, double min, double max, double step)
{
    UIDouble* ui_interface = static_cast<UIDouble*>(cpp_interface);
    ui_interface->addNumEntry(label, zone, init, min, max, step);
}

static void addHorizontalBargraphGlueDouble(void* cpp_interface, const char* label, double* zone, double min, double max)
{
    UIDouble* ui_interface = static_cast<UIDouble*>(cpp_interface);
    ui_interface->addHorizontalBargraph(label, zone, min, max);
}

static void addVerticalBargraphGlueDouble(void* cpp_interface, const char* label, double* zone, double min, double max)
{
    UIDouble* ui_interface = static_cast<UIDouble*>(cpp_interface);
    ui_interface->addVerticalBargraph(label, zone, min, max);
}
    
static void addSoundfileGlueDouble(void* cpp_interface, const char* label, const char* url, Soundfile** sf_zone)
{
    UIDouble* ui_interface = static_cast<UIDouble*>(cpp_interface);
    ui_interface->addSoundfile(label, url, sf_zone);
}

static void declareGlueDouble(void* cpp_interface, double* zone, const char* key, const char* value)
{
    UIDouble* ui_interface = static_cast<UIDouble*>(cpp_interface);
    ui_interface->declare(zone, key, value);
}

static void buildUIGlue(UIGlue* glue, UI* ui_interface, bool is_double)
{
    glue->uiInterface = ui_interface;
    
    if (is_double) {
        glue->openTabBox = reinterpret_cast<openTabBoxFun>(openTabBoxGlueDouble);
        glue->openHorizontalBox = reinterpret_cast<openHorizontalBoxFun>(openHorizontalBoxGlueDouble);
        glue->openVerticalBox = reinterpret_cast<openVerticalBoxFun>(openVerticalBoxGlueDouble);
        glue->closeBox = reinterpret_cast<closeBoxFun>(closeBoxGlueDouble);
        glue->addButton = reinterpret_cast<addButtonFun>(addButtonGlueDouble);
        glue->addCheckButton = reinterpret_cast<addCheckButtonFun>(addCheckButtonGlueDouble);
        glue->addVerticalSlider = reinterpret_cast<addVerticalSliderFun>(addVerticalSliderGlueDouble);
        glue->addHorizontalSlider = reinterpret_cast<addHorizontalSliderFun>(addHorizontalSliderGlueDouble);
        glue->addNumEntry = reinterpret_cast<addNumEntryFun>(addNumEntryGlueDouble);
        glue->addHorizontalBargraph = reinterpret_cast<addHorizontalBargraphFun>(addHorizontalBargraphGlueDouble);
        glue->addVerticalBargraph = reinterpret_cast<addVerticalBargraphFun>(addVerticalBargraphGlueDouble);
        glue->addSoundfile = reinterpret_cast<addSoundfileFun>(addSoundfileGlueDouble);
        glue->declare = reinterpret_cast<declareFun>(declareGlueDouble);
    } else {
        glue->openTabBox = reinterpret_cast<openTabBoxFun>(openTabBoxGlueFloat);
        glue->openHorizontalBox = reinterpret_cast<openHorizontalBoxFun>(openHorizontalBoxGlueFloat);
        glue->openVerticalBox = reinterpret_cast<openVerticalBoxFun>(openVerticalBoxGlueFloat);
        glue->closeBox = reinterpret_cast<closeBoxFun>(closeBoxGlueFloat);
        glue->addButton = reinterpret_cast<addButtonFun>(addButtonGlueFloat);
        glue->addCheckButton = reinterpret_cast<addCheckButtonFun>(addCheckButtonGlueFloat);
        glue->addVerticalSlider = reinterpret_cast<addVerticalSliderFun>(addVerticalSliderGlueFloat);
        glue->addHorizontalSlider = reinterpret_cast<addHorizontalSliderFun>(addHorizontalSliderGlueFloat);
        glue->addNumEntry = reinterpret_cast<addNumEntryFun>(addNumEntryGlueFloat);
        glue->addHorizontalBargraph = reinterpret_cast<addHorizontalBargraphFun>(addHorizontalBargraphGlueFloat);
        glue->addVerticalBargraph = reinterpret_cast<addVerticalBargraphFun>(addVerticalBargraphGlueFloat);
        glue->addSoundfile = reinterpret_cast<addSoundfileFun>(addSoundfileGlueFloat);
        glue->declare = reinterpret_cast<declareFun>(declareGlueFloat);
    }
}
    
// Base class
    
struct UIInterface
{
    virtual ~UIInterface() {}
    
    virtual int sizeOfFAUSTFLOAT() = 0;
    
    // -- widget's layouts
    
    virtual void openTabBox(const char* label) = 0;
    virtual void openHorizontalBox(const char* label) = 0;
    virtual void openVerticalBox(const char* label) = 0;
    virtual void closeBox() = 0;
    
    // float version
    
    // -- active widgets
    
    virtual void addButton(const char* label, float* zone) = 0;
    virtual void addCheckButton(const char* label, float* zone) = 0;
    
    virtual void addVerticalSlider(const char* label, float* zone, float init, float min, float max, float step) = 0;
    virtual void addHorizontalSlider(const char* label, float* zone, float init, float min, float max, float step) = 0;
    virtual void addNumEntry(const char* label, float* zone, float init, float min, float max, float step) = 0;
    
    // -- passive widgets
    
    virtual void addHorizontalBargraph(const char* label, float* zone, float min, float max) = 0;
    virtual void addVerticalBargraph(const char* label, float* zone, float min, float max) = 0;
    
    // -- metadata declarations
    
    virtual void declare(float* zone, const char* key, const char* val) = 0;
    
    // double version
    
    virtual void addButton(const char* label, double* zone) = 0;
    virtual void addCheckButton(const char* label, double* zone) = 0;
  
    virtual void addVerticalSlider(const char* label, double* zone, double init, double min, double max, double step) = 0;
    virtual void addHorizontalSlider(const char* label, double* zone, double init, double min, double max, double step) = 0;
    
    virtual void addNumEntry(const char* label, double* zone, double init, double min, double max, double step) = 0;
    
    // -- soundfiles
    
    virtual void addSoundfile(const char* label, const char* url, Soundfile** sf_zone) = 0;
    
    // -- passive widgets
    
    virtual void addHorizontalBargraph(const char* label, double* zone, double min, double max) = 0;
    virtual void addVerticalBargraph(const char* label, double* zone, double min, double max) = 0;
     
    // -- metadata declarations
    
    virtual void declare(double* zone, const char* key, const char* val) = 0;
    
};
    
struct UITemplate : public UIInterface
{
 
    void* fCPPInterface;

    UITemplate(void* cpp_interface):fCPPInterface(cpp_interface)
    {}
    virtual ~UITemplate() {}
    
    int sizeOfFAUSTFLOAT()
    {
        return reinterpret_cast<UI*>(fCPPInterface)->sizeOfFAUSTFLOAT();
    }
    
    // -- widget's layouts
    
    void openTabBox(const char* label)
    {
        openTabBoxGlueFloat(fCPPInterface, label);
    }
    void openHorizontalBox(const char* label)
    {
        openHorizontalBoxGlueFloat(fCPPInterface, label);
    }
    void openVerticalBox(const char* label)
    {
        openVerticalBoxGlueFloat(fCPPInterface, label);
    }
    void closeBox()
    {
        closeBoxGlueFloat(fCPPInterface);
    }
    
    // float version
    
    // -- active widgets
    
    void addButton(const char* label, float* zone)
    {
        addButtonGlueFloat(fCPPInterface, label, zone);
    }
    void addCheckButton(const char* label, float* zone)
    {
        addCheckButtonGlueFloat(fCPPInterface, label, zone);
    }
    
    void addVerticalSlider(const char* label, float* zone, float init, float min, float max, float step)
    {
        addVerticalSliderGlueFloat(fCPPInterface, label, zone, init, min, max, step);
    }
    
    void addHorizontalSlider(const char* label, float* zone, float init, float min, float max, float step)
    {
        addHorizontalSliderGlueFloat(fCPPInterface, label, zone, init, min, max, step);
    }
    
    void addNumEntry(const char* label, float* zone, float init, float min, float max, float step)
    {
        addNumEntryGlueFloat(fCPPInterface, label, zone, init, min, max, step);
    }
    
    // -- passive widgets
    
    void addHorizontalBargraph(const char* label, float* zone, float min, float max)
    {
        addHorizontalBargraphGlueFloat(fCPPInterface, label, zone, min, max);
    }
    
    void addVerticalBargraph(const char* label, float* zone, float min, float max)
    {
        addVerticalBargraphGlueFloat(fCPPInterface, label, zone, min, max);
    }

    // -- metadata declarations
    
    void declare(float* zone, const char* key, const char* val)
    {
        declareGlueFloat(fCPPInterface, zone, key, val);
    }
    
    // double version
    
    void addButton(const char* label, double* zone)
    {
        addButtonGlueDouble(fCPPInterface, label, zone);
    }
    void addCheckButton(const char* label, double* zone)
    {
        addCheckButtonGlueDouble(fCPPInterface, label, zone);
    }
    
    void addVerticalSlider(const char* label, double* zone, double init, double min, double max, double step)
    {
        addVerticalSliderGlueDouble(fCPPInterface, label, zone, init, min, max, step);
    }
    
    void addHorizontalSlider(const char* label, double* zone, double init, double min, double max, double step)
    {
        addHorizontalSliderGlueDouble(fCPPInterface, label, zone, init, min, max, step);
    }
    
    void addNumEntry(const char* label, double* zone, double init, double min, double max, double step)
    {
        addNumEntryGlueDouble(fCPPInterface, label, zone, init, min, max, step);
    }

    // -- soundfiles
    
    void addSoundfile(const char* label, const char* url, Soundfile** sf_zone)
    {
        addSoundfileGlueFloat(fCPPInterface, label, url, sf_zone);
    }

    // -- passive widgets
    
    void addHorizontalBargraph(const char* label, double* zone, double min, double max)
    {
        addHorizontalBargraphGlueDouble(fCPPInterface, label, zone, min, max);
    }
    
    void addVerticalBargraph(const char* label, double* zone, double min, double max)
    {
        addVerticalBargraphGlueDouble(fCPPInterface, label, zone, min, max);
    }

    // -- metadata declarations
    
    void declare(double* zone, const char* key, const char* val)
    {
        declareGlueDouble(fCPPInterface, zone, key, val);
    }

};
    
struct UIGlueTemplate : public UIInterface
{
    
    UIGlue* fGlue;
    
    UIGlueTemplate(UIGlue* glue):fGlue(glue)
    {}
    virtual ~UIGlueTemplate() {}
    
    virtual int sizeOfFAUSTFLOAT() { return sizeof(FAUSTFLOAT); }
    
    // -- widget's layouts
    
    void openTabBox(const char* label)
    {
        fGlue->openTabBox(fGlue->uiInterface, label);
    }
    void openHorizontalBox(const char* label)
    {
        fGlue->openHorizontalBox(fGlue->uiInterface, label);
    }
    void openVerticalBox(const char* label)
    {
        fGlue->openVerticalBox(fGlue->uiInterface, label);
    }
    void closeBox()
    {
        fGlue->closeBox(fGlue->uiInterface);
    }

    // float version
    
    // -- active widgets
    
    void addButton(const char* label, float* zone)
    {
        fGlue->addButton(fGlue->uiInterface, label, reinterpret_cast<FAUSTFLOAT*>(zone));
    }
    void addCheckButton(const char* label, float* zone)
    {
        fGlue->addCheckButton(fGlue->uiInterface, label, reinterpret_cast<FAUSTFLOAT*>(zone));
    }
    
    void addVerticalSlider(const char* label, float* zone, float init, float min, float max, float step)
    {
        fGlue->addVerticalSlider(fGlue->uiInterface, label, reinterpret_cast<FAUSTFLOAT*>(zone), init, min, max, step);
    }
    void addHorizontalSlider(const char* label, float* zone, float init, float min, float max, float step)
    {
        fGlue->addHorizontalSlider(fGlue->uiInterface, label, reinterpret_cast<FAUSTFLOAT*>(zone), init, min, max, step);
    }
    void addNumEntry(const char* label, float* zone, float init, float min, float max, float step)
    {
        fGlue->addNumEntry(fGlue->uiInterface, label, reinterpret_cast<FAUSTFLOAT*>(zone), init, min, max, step);
    }
    
    // -- passive widgets
    
    void addHorizontalBargraph(const char* label, float* zone, float min, float max)
    {
        fGlue->addHorizontalBargraph(fGlue->uiInterface, label, reinterpret_cast<FAUSTFLOAT*>(zone), min, max);
    }
    void addVerticalBargraph(const char* label, float* zone, float min, float max)
    {
        fGlue->addVerticalBargraph(fGlue->uiInterface, label, reinterpret_cast<FAUSTFLOAT*>(zone), min, max);
    }
    
    // -- metadata declarations
    
    void declare(float* zone, const char* key, const char* val)
    {
        fGlue->declare(fGlue->uiInterface, reinterpret_cast<FAUSTFLOAT*>(zone), key, val);
    }
    
    // double version
    
    void addButton(const char* label, double* zone)
    {
        fGlue->addButton(fGlue->uiInterface, label, reinterpret_cast<FAUSTFLOAT*>(zone));
    }
    void addCheckButton(const char* label, double* zone)
    {
        fGlue->addCheckButton(fGlue->uiInterface, label, reinterpret_cast<FAUSTFLOAT*>(zone));
    }
    
    void addVerticalSlider(const char* label, double* zone, double init, double min, double max, double step)
    {
        fGlue->addVerticalSlider(fGlue->uiInterface, label, reinterpret_cast<FAUSTFLOAT*>(zone), init, min, max, step);
    }
    void addHorizontalSlider(const char* label, double* zone, double init, double min, double max, double step)
    {
        fGlue->addHorizontalSlider(fGlue->uiInterface, label, reinterpret_cast<FAUSTFLOAT*>(zone), init, min, max, step);
    }
    void addNumEntry(const char* label, double* zone, double init, double min, double max, double step)
    {
        fGlue->addNumEntry(fGlue->uiInterface, label, reinterpret_cast<FAUSTFLOAT*>(zone), init, min, max, step);
    }
    // -- soundfiles
    
    void addSoundfile(const char* label, const char* url, Soundfile** sf_zone) {}
    
    // -- passive widgets
    
    void addHorizontalBargraph(const char* label, double* zone, double min, double max)
    {
        fGlue->addHorizontalBargraph(fGlue->uiInterface, label, reinterpret_cast<FAUSTFLOAT*>(zone), min, max);
    }
    void addVerticalBargraph(const char* label, double* zone, double min, double max)
    {
        fGlue->addVerticalBargraph(fGlue->uiInterface, label, reinterpret_cast<FAUSTFLOAT*>(zone), min, max);
    }
    
    // -- metadata declarations
    
    void declare(double* zone, const char* key, const char* val)
    {
        fGlue->declare(fGlue->uiInterface, reinterpret_cast<FAUSTFLOAT*>(zone), key, val);
    }
    
};

/*******************************************************************************
 * Meta glue code
 ******************************************************************************/

static void declareMetaGlue(void* cpp_interface, const char* key, const char* value)
{
    Meta* meta_interface = static_cast<Meta*>(cpp_interface);
    meta_interface->declare(key, value);
}

static void buildMetaGlue(MetaGlue* glue, Meta* meta)
{
    glue->metaInterface = meta;
    glue->declare = declareMetaGlue;
}
    
/*******************************************************************************
 * Memory manager glue code
 ******************************************************************************/

static void* allocateMemoryManagerGlue(void* cpp_interface, size_t size)
{
    dsp_memory_manager* manager_interface = static_cast<dsp_memory_manager*>(cpp_interface);
    return manager_interface->allocate(size);
}
    
static void destroyMemoryManagerGlue(void* cpp_interface, void* ptr)
{
    dsp_memory_manager* manager_interface = static_cast<dsp_memory_manager*>(cpp_interface);
    manager_interface->destroy(ptr);
}

static void buildManagerGlue(MemoryManagerGlue* glue, dsp_memory_manager* manager)
{
    glue->managerInterface = manager;
    glue->allocate = allocateMemoryManagerGlue;
    glue->destroy = destroyMemoryManagerGlue;
}

#ifdef __cplusplus
}
#endif

#endif
/**************************  END  CGlue.h **************************/

#ifdef _WIN32
#include <windows.h>
#define snprintf _snprintf
#define STRDUP _strdup
#else
#define STRDUP strdup
#endif

//------------------------------------------------------------------------------------------
//  Decode a dsp JSON description and implement 'buildUserInterface' and 'metadata' methods
//------------------------------------------------------------------------------------------

#define REAL_UI(ui_interface) reinterpret_cast<UIReal<REAL>*>(ui_interface)
#define REAL_ADR(index)      reinterpret_cast<REAL*>(&memory_block[index])
#define REAL_EXT_ADR(index)  reinterpret_cast<FAUSTFLOAT*>(&memory_block[index])
#define SOUNDFILE_ADR(index) reinterpret_cast<Soundfile**>(&memory_block[index])

typedef std::function<void(FAUSTFLOAT)> ReflectFunction;
typedef std::function<FAUSTFLOAT()> ModifyFunction;

struct FAUST_API ExtZoneParam {

    virtual void reflectZone() = 0;
    virtual void modifyZone() = 0;
    
    virtual void setReflectZoneFun(ReflectFunction reflect) = 0;
    virtual void setModifyZoneFun(ModifyFunction modify) = 0;
    
    virtual ~ExtZoneParam()
    {}
    
};

// Templated decoder

struct FAUST_API JSONUIDecoderBase
{
    virtual ~JSONUIDecoderBase()
    {}
    
    virtual void metadata(Meta* m) = 0;
    virtual void metadata(MetaGlue* glue) = 0;
    virtual int getDSPSize() = 0;
    virtual std::string getName() = 0;
    virtual std::string getLibVersion() = 0;
    virtual std::string getCompileOptions() = 0;
    virtual std::vector<std::string> getLibraryList() = 0;
    virtual std::vector<std::string> getIncludePathnames() = 0;
    virtual int getNumInputs() = 0;
    virtual int getNumOutputs() = 0;
    virtual int getSampleRate(char* memory_block) = 0;
    virtual void setReflectZoneFun(int index, ReflectFunction fun) = 0;
    virtual void setModifyZoneFun(int index, ModifyFunction fun) = 0;
    virtual void setupDSPProxy(UI* ui_interface, char* memory_block) = 0;
    virtual bool hasDSPProxy() = 0;
    virtual std::vector<ExtZoneParam*>& getInputControls() = 0;
    virtual std::vector<ExtZoneParam*>& getOutputControls() = 0;
    virtual void resetUserInterface() = 0;
    virtual void resetUserInterface(char* memory_block, Soundfile* defaultsound = nullptr) = 0;
    virtual void buildUserInterface(UI* ui_interface) = 0;
    virtual void buildUserInterface(UI* ui_interface, char* memory_block) = 0;
    virtual void buildUserInterface(UIGlue* ui_interface, char* memory_block) = 0;
    virtual bool hasCompileOption(const std::string& option) = 0;
    virtual std::string getCompileOption(const std::string& option) = 0;
};

template <typename REAL>
struct FAUST_API JSONUIDecoderReal : public JSONUIDecoderBase {
    
    struct ZoneParam : public ExtZoneParam {
        
        FAUSTFLOAT fZone;
        ReflectFunction fReflect;
        ModifyFunction fModify;
        
    #if defined(TARGET_OS_IPHONE) || defined(WIN32)
        ZoneParam(ReflectFunction reflect = nullptr, ModifyFunction modify = nullptr)
        :fReflect(reflect), fModify(modify)
        {}
        void reflectZone() { if (fReflect) fReflect(fZone); }
        void modifyZone() { if (fModify) fZone = fModify(); }
    #else
        ZoneParam(ReflectFunction reflect = [](FAUSTFLOAT value) {}, ModifyFunction modify = []() { return FAUSTFLOAT(-1); })
        :fReflect(reflect), fModify(modify)
        {}
        void reflectZone() { fReflect(fZone); }
        void modifyZone() { fZone = fModify(); }
    #endif
        
        void setReflectZoneFun(ReflectFunction reflect) { fReflect = reflect; }
        void setModifyZoneFun(ModifyFunction modify) { fModify = modify; }
        
    };
    
    typedef std::vector<ExtZoneParam*> controlMap;
  
    std::string fName;
    std::string fFileName;
    std::string fJSON;
    std::string fVersion;
    std::string fCompileOptions;
    
    std::map<std::string, std::string> fMetadata;
    std::vector<itemInfo> fUiItems;
    
    std::vector<std::string> fLibraryList;
    std::vector<std::string> fIncludePathnames;
    
    int fNumInputs, fNumOutputs, fSRIndex;
    int fDSPSize;
    bool fDSPProxy;
    
    controlMap fPathInputTable;     // [path, ZoneParam]
    controlMap fPathOutputTable;    // [path, ZoneParam]
    
    bool startWith(const std::string& str, const std::string& prefix)
    {
        return (str.substr(0, prefix.size()) == prefix);
    }

    bool isInput(const std::string& type)
    {
        return (type == "vslider" || type == "hslider" || type == "nentry" || type == "button" || type == "checkbox");
    }
    bool isOutput(const std::string& type) { return (type == "hbargraph" || type == "vbargraph"); }
    bool isSoundfile(const std::string& type) { return (type == "soundfile"); }
    
    std::string getString(std::map<std::string, std::pair<std::string, double> >& map, const std::string& key)
    {
        return (map.find(key) != map.end()) ? map[key].first : "";
    }
    
    int getInt(std::map<std::string, std::pair<std::string, double> >& map, const std::string& key)
    {
        return (map.find(key) != map.end()) ? int(map[key].second) : -1;
    }
    
    void setReflectZoneFun(int index, ReflectFunction fun)
    {
        fPathInputTable[index]->setReflectZoneFun(fun);
    }
    
    void setModifyZoneFun(int index, ModifyFunction fun)
    {
        fPathOutputTable[index]->setModifyZoneFun(fun);
    }

    JSONUIDecoderReal(const std::string& json)
    {
        fJSON = json;
        const char* p = fJSON.c_str();
        std::map<std::string, std::pair<std::string, double> > meta_data1;
        std::map<std::string, std::vector<std::string> > meta_data2;
        parseJson(p, meta_data1, fMetadata, meta_data2, fUiItems);
        
        // meta_data1 contains <name : val>, <inputs : val>, <ouputs : val> pairs etc...
        fName = getString(meta_data1, "name");
        fFileName = getString(meta_data1, "filename");
        fVersion = getString(meta_data1, "version");
        fCompileOptions = getString(meta_data1, "compile_options");
        
        if (meta_data2.find("library_list") != meta_data2.end()) {
            fLibraryList = meta_data2["library_list"];
        } else {
            // 'library_list' is coded as successive 'library_pathN' metadata
            for (const auto& it : fMetadata) {
                if (startWith(it.first, "library_path")) {
                    fLibraryList.push_back(it.second);
                }
            }
        }
        if (meta_data2.find("include_pathnames") != meta_data2.end()) {
            fIncludePathnames = meta_data2["include_pathnames"];
        }
        
        fDSPSize = getInt(meta_data1, "size");
        fNumInputs = getInt(meta_data1, "inputs");
        fNumOutputs = getInt(meta_data1, "outputs");
        fSRIndex = getInt(meta_data1, "sr_index");
        fDSPProxy = false;
        
        // Prepare the fPathTable and init zone
        for (const auto& it : fUiItems) {
            std::string type = it.type;
            // Meta data declaration for input items
            if (isInput(type)) {
                ZoneParam* param = new ZoneParam();
                fPathInputTable.push_back(param);
                param->fZone = it.init;
            }
            // Meta data declaration for output items
            else if (isOutput(type)) {
                ZoneParam* param = new ZoneParam();
                fPathOutputTable.push_back(param);
                param->fZone = REAL(0);
            }
        }
    }
    
    virtual ~JSONUIDecoderReal()
    {
        for (const auto& it : fPathInputTable) {
            delete it;
        }
        for (const auto& it : fPathOutputTable) {
            delete it;
        }
    }
    
    void metadata(Meta* m)
    {
        for (const auto& it : fMetadata) {
            m->declare(it.first.c_str(), it.second.c_str());
        }
    }
    
    void metadata(MetaGlue* m)
    {
        for (const auto& it : fMetadata) {
            m->declare(m->metaInterface, it.first.c_str(), it.second.c_str());
        }
    }
    
    void resetUserInterface()
    {
        int item = 0;
        for (const auto& it : fUiItems) {
            if (isInput(it.type)) {
                static_cast<ZoneParam*>(fPathInputTable[item++])->fZone = it.init;
            }
        }
    }
    
    void resetUserInterface(char* memory_block, Soundfile* defaultsound = nullptr)
    {
        for (const auto& it : fUiItems) {
            int index = it.index;
            if (isInput(it.type)) {
                *REAL_ADR(index) = it.init;
            } else if (isSoundfile(it.type)) {
                if (*SOUNDFILE_ADR(index) == nullptr) {
                    *SOUNDFILE_ADR(index) = defaultsound;
                }
            }
        }
    }
    
    int getSampleRate(char* memory_block)
    {
        return *reinterpret_cast<int*>(&memory_block[fSRIndex]);
    }
    
    void setupDSPProxy(UI* ui_interface, char* memory_block)
    {
        if (!fDSPProxy) {
            fDSPProxy = true;
            int countIn = 0;
            int countOut = 0;
            for (const auto& it : fUiItems) {
                std::string type = it.type;
                int index = it.index;
                if (isInput(type)) {
                    fPathInputTable[countIn++]->setReflectZoneFun([=](FAUSTFLOAT value) { *REAL_ADR(index) = REAL(value); });
                } else if (isOutput(type)) {
                    fPathOutputTable[countOut++]->setModifyZoneFun([=]() { return FAUSTFLOAT(*REAL_ADR(index)); });
                }
            }
        }
        
        // Setup soundfile in any case
        for (const auto& it : fUiItems) {
            if (isSoundfile(it.type)) {
                ui_interface->addSoundfile(it.label.c_str(), it.url.c_str(), SOUNDFILE_ADR(it.index));
            }
        }
    }
    
    bool hasDSPProxy() { return fDSPProxy; }
  
    void buildUserInterface(UI* ui_interface)
    {
        // MANDATORY: to be sure floats or double are correctly parsed
        char* tmp_local = setlocale(LC_ALL, nullptr);
        if (tmp_local != NULL) {
            tmp_local = STRDUP(tmp_local);
        }
        setlocale(LC_ALL, "C");
        
        int countIn = 0;
        int countOut = 0;
        int countSound = 0;
        
        for (const auto& it : fUiItems) {
            
            std::string type = it.type;
            REAL init = REAL(it.init);
            REAL min = REAL(it.fmin);
            REAL max = REAL(it.fmax);
            REAL step = REAL(it.step);
            
            // Meta data declaration for input items
            if (isInput(type)) {
                for (size_t i = 0; i < it.meta.size(); i++) {
                    ui_interface->declare(&static_cast<ZoneParam*>(fPathInputTable[countIn])->fZone, it.meta[i].first.c_str(), it.meta[i].second.c_str());
                }
            }
            // Meta data declaration for output items
            else if (isOutput(type)) {
                for (size_t i = 0; i < it.meta.size(); i++) {
                    ui_interface->declare(&static_cast<ZoneParam*>(fPathOutputTable[countOut])->fZone, it.meta[i].first.c_str(), it.meta[i].second.c_str());
                }
            }
            // Meta data declaration for group opening or closing
            else {
                for (size_t i = 0; i < it.meta.size(); i++) {
                    ui_interface->declare(0, it.meta[i].first.c_str(), it.meta[i].second.c_str());
                }
            }
            
            if (type == "hgroup") {
                ui_interface->openHorizontalBox(it.label.c_str());
            } else if (type == "vgroup") {
                ui_interface->openVerticalBox(it.label.c_str());
            } else if (type == "tgroup") {
                ui_interface->openTabBox(it.label.c_str());
            } else if (type == "vslider") {
                ui_interface->addVerticalSlider(it.label.c_str(), &static_cast<ZoneParam*>(fPathInputTable[countIn])->fZone, init, min, max, step);
            } else if (type == "hslider") {
                ui_interface->addHorizontalSlider(it.label.c_str(), &static_cast<ZoneParam*>(fPathInputTable[countIn])->fZone, init, min, max, step);
            } else if (type == "checkbox") {
                ui_interface->addCheckButton(it.label.c_str(), &static_cast<ZoneParam*>(fPathInputTable[countIn])->fZone);
            } else if (type == "soundfile") {
                // Nothing
            } else if (type == "hbargraph") {
                ui_interface->addHorizontalBargraph(it.label.c_str(), &static_cast<ZoneParam*>(fPathOutputTable[countOut])->fZone, min, max);
            } else if (type == "vbargraph") {
                ui_interface->addVerticalBargraph(it.label.c_str(), &static_cast<ZoneParam*>(fPathOutputTable[countOut])->fZone, min, max);
            } else if (type == "nentry") {
                ui_interface->addNumEntry(it.label.c_str(), &static_cast<ZoneParam*>(fPathInputTable[countIn])->fZone, init, min, max, step);
            } else if (type == "button") {
                ui_interface->addButton(it.label.c_str(), &static_cast<ZoneParam*>(fPathInputTable[countIn])->fZone);
            } else if (type == "close") {
                ui_interface->closeBox();
            }
            
            if (isInput(type)) {
                countIn++;
            } else if (isOutput(type)) {
                countOut++;
            } else if (isSoundfile(type)) {
                countSound++;
            }
        }
        
        if (tmp_local != NULL) {
            setlocale(LC_ALL, tmp_local);
            free(tmp_local);
        }
    }
    
    void buildUserInterface(UI* ui_interface, char* memory_block)
    {
        // MANDATORY: to be sure floats or double are correctly parsed
        char* tmp_local = setlocale(LC_ALL, nullptr);
        if (tmp_local != NULL) {
            tmp_local = STRDUP(tmp_local);
        }
        setlocale(LC_ALL, "C");
        
        for (const auto& it : fUiItems) {
            
            std::string type = it.type;
            int index = it.index;
            REAL init = REAL(it.init);
            REAL min = REAL(it.fmin);
            REAL max = REAL(it.fmax);
            REAL step = REAL(it.step);
            
            // Meta data declaration for input items
            if (isInput(type)) {
                for (size_t i = 0; i < it.meta.size(); i++) {
                    REAL_UI(ui_interface)->declare(REAL_ADR(index), it.meta[i].first.c_str(), it.meta[i].second.c_str());
                }
            }
            // Meta data declaration for output items
            else if (isOutput(type)) {
                for (size_t i = 0; i < it.meta.size(); i++) {
                    REAL_UI(ui_interface)->declare(REAL_ADR(index), it.meta[i].first.c_str(), it.meta[i].second.c_str());
                }
            }
            // Meta data declaration for group opening or closing
            else {
                for (size_t i = 0; i < it.meta.size(); i++) {
                    REAL_UI(ui_interface)->declare(0, it.meta[i].first.c_str(), it.meta[i].second.c_str());
                }
            }
            
            if (type == "hgroup") {
                REAL_UI(ui_interface)->openHorizontalBox(it.label.c_str());
            } else if (type == "vgroup") {
                REAL_UI(ui_interface)->openVerticalBox(it.label.c_str());
            } else if (type == "tgroup") {
                REAL_UI(ui_interface)->openTabBox(it.label.c_str());
            } else if (type == "vslider") {
                REAL_UI(ui_interface)->addVerticalSlider(it.label.c_str(), REAL_ADR(index), init, min, max, step);
            } else if (type == "hslider") {
                REAL_UI(ui_interface)->addHorizontalSlider(it.label.c_str(), REAL_ADR(index), init, min, max, step);
            } else if (type == "checkbox") {
                REAL_UI(ui_interface)->addCheckButton(it.label.c_str(), REAL_ADR(index));
            } else if (type == "soundfile") {
                REAL_UI(ui_interface)->addSoundfile(it.label.c_str(), it.url.c_str(), SOUNDFILE_ADR(index));
            } else if (type == "hbargraph") {
                REAL_UI(ui_interface)->addHorizontalBargraph(it.label.c_str(), REAL_ADR(index), min, max);
            } else if (type == "vbargraph") {
                REAL_UI(ui_interface)->addVerticalBargraph(it.label.c_str(), REAL_ADR(index), min, max);
            } else if (type == "nentry") {
                REAL_UI(ui_interface)->addNumEntry(it.label.c_str(), REAL_ADR(index), init, min, max, step);
            } else if (type == "button") {
                REAL_UI(ui_interface)->addButton(it.label.c_str(), REAL_ADR(index));
            } else if (type == "close") {
                REAL_UI(ui_interface)->closeBox();
            }
        }
        
        if (tmp_local != NULL) {
            setlocale(LC_ALL, tmp_local);
            free(tmp_local);
        }
    }
    
    void buildUserInterface(UIGlue* ui_interface, char* memory_block)
    {
        // MANDATORY: to be sure floats or double are correctly parsed
        char* tmp_local = setlocale(LC_ALL, nullptr);
        if (tmp_local != NULL) {
            tmp_local = STRDUP(tmp_local);
        }
        setlocale(LC_ALL, "C");
        
        for (const auto& it : fUiItems) {
            
            std::string type = it.type;
            int index = it.index;
            REAL init = REAL(it.init);
            REAL min = REAL(it.fmin);
            REAL max = REAL(it.fmax);
            REAL step = REAL(it.step);
            
            // Meta data declaration for input items
            if (isInput(type)) {
                for (size_t i = 0; i < it.meta.size(); i++) {
                    ui_interface->declare(ui_interface->uiInterface, REAL_EXT_ADR(index), it.meta[i].first.c_str(), it.meta[i].second.c_str());
                }
            }
            // Meta data declaration for output items
            else if (isOutput(type)) {
                for (size_t i = 0; i < it.meta.size(); i++) {
                    ui_interface->declare(ui_interface->uiInterface, REAL_EXT_ADR(index), it.meta[i].first.c_str(), it.meta[i].second.c_str());
                }
            }
            // Meta data declaration for group opening or closing
            else {
                for (size_t i = 0; i < it.meta.size(); i++) {
                    ui_interface->declare(ui_interface->uiInterface, 0, it.meta[i].first.c_str(), it.meta[i].second.c_str());
                }
            }
            
            if (type == "hgroup") {
                ui_interface->openHorizontalBox(ui_interface->uiInterface, it.label.c_str());
            } else if (type == "vgroup") {
                ui_interface->openVerticalBox(ui_interface->uiInterface, it.label.c_str());
            } else if (type == "tgroup") {
                ui_interface->openTabBox(ui_interface->uiInterface, it.label.c_str());
            } else if (type == "vslider") {
                ui_interface->addVerticalSlider(ui_interface->uiInterface, it.label.c_str(), REAL_EXT_ADR(index), init, min, max, step);
            } else if (type == "hslider") {
                ui_interface->addHorizontalSlider(ui_interface->uiInterface, it.label.c_str(), REAL_EXT_ADR(index), init, min, max, step);
            } else if (type == "checkbox") {
                ui_interface->addCheckButton(ui_interface->uiInterface, it.label.c_str(), REAL_EXT_ADR(index));
            } else if (type == "soundfile") {
                ui_interface->addSoundfile(ui_interface->uiInterface, it.label.c_str(), it.url.c_str(), SOUNDFILE_ADR(index));
            } else if (type == "hbargraph") {
                ui_interface->addHorizontalBargraph(ui_interface->uiInterface, it.label.c_str(), REAL_EXT_ADR(index), min, max);
            } else if (type == "vbargraph") {
                ui_interface->addVerticalBargraph(ui_interface->uiInterface, it.label.c_str(), REAL_EXT_ADR(index), min, max);
            } else if (type == "nentry") {
                ui_interface->addNumEntry(ui_interface->uiInterface, it.label.c_str(), REAL_EXT_ADR(index), init, min, max, step);
            } else if (type == "button") {
                ui_interface->addButton(ui_interface->uiInterface, it.label.c_str(), REAL_EXT_ADR(index));
            } else if (type == "close") {
                ui_interface->closeBox(ui_interface->uiInterface);
            }
        }
        
        if (tmp_local != NULL) {
            setlocale(LC_ALL, tmp_local);
            free(tmp_local);
        }
    }
    
    bool hasCompileOption(const std::string& option)
    {
        std::istringstream iss(fCompileOptions);
        std::string token;
        while (std::getline(iss, token, ' ')) {
            if (token == option) return true;
        }
        return false;
    }
    
    std::string getCompileOption(const std::string& option)
    {
        std::istringstream iss(fCompileOptions);
        std::string token;
        while (std::getline(iss, token, ' ')) {
            if (token == option) {
                std::string res;
                iss >> res;
                return res;
            }
        }
        return "";
    }
    
    int getDSPSize() { return fDSPSize; }
    std::string getName() { return fName; }
    std::string getLibVersion() { return fVersion; }
    std::string getCompileOptions() { return fCompileOptions; }
    std::vector<std::string> getLibraryList() { return fLibraryList; }
    std::vector<std::string> getIncludePathnames() { return fIncludePathnames; }
    int getNumInputs() { return fNumInputs; }
    int getNumOutputs() { return fNumOutputs; }
    
    std::vector<ExtZoneParam*>& getInputControls()
    {
        return fPathInputTable;
    }
    std::vector<ExtZoneParam*>& getOutputControls()
    {
        return fPathOutputTable;
    }
    
};

// FAUSTFLOAT templated decoder

struct FAUST_API JSONUIDecoder : public JSONUIDecoderReal<FAUSTFLOAT>
{
    JSONUIDecoder(const std::string& json):JSONUIDecoderReal<FAUSTFLOAT>(json)
    {}
};

// Generic factory

static JSONUIDecoderBase* createJSONUIDecoder(const std::string& json)
{
    JSONUIDecoder decoder(json);
    if (decoder.hasCompileOption("-double")) {
        return new JSONUIDecoderReal<double>(json);
    } else {
        return new JSONUIDecoderReal<float>(json);
    }
}

#endif
/**************************  END  JSONUIDecoder.h **************************/

/**
 * Proxy dsp definition created from the DSP JSON description.
 * This class allows a 'proxy' dsp to control a real dsp
 * possibly running somewhere else.
 */
class proxy_dsp : public dsp {

    protected:
    
        JSONUIDecoder* fDecoder;
        int fSampleRate;
    
        void init(const std::string& json)
        {
            fDecoder = new JSONUIDecoder(json);
            fSampleRate = -1;
        }
        
    public:
    
        proxy_dsp():fDecoder(nullptr), fSampleRate(-1)
        {}
    
        proxy_dsp(const std::string& json)
        {
            init(json);
        }
          
        proxy_dsp(dsp* dsp)
        {
            JSONUI builder(dsp->getNumInputs(), dsp->getNumOutputs());
            dsp->metadata(&builder);
            dsp->buildUserInterface(&builder);
            fSampleRate = dsp->getSampleRate();
            fDecoder = new JSONUIDecoder(builder.JSON());
        }
      
        virtual ~proxy_dsp()
        {
            delete fDecoder;
        }
    
        virtual int getNumInputs() { return fDecoder->fNumInputs; }
        virtual int getNumOutputs() { return fDecoder->fNumOutputs; }
        
        virtual void buildUserInterface(UI* ui) { fDecoder->buildUserInterface(ui); }
        
        // To possibly implement in a concrete proxy dsp 
        virtual void init(int sample_rate)
        {
            instanceInit(sample_rate);
        }
        virtual void instanceInit(int sample_rate)
        {
            instanceConstants(sample_rate);
            instanceResetUserInterface();
            instanceClear();
        }
        virtual void instanceConstants(int sample_rate) { fSampleRate = sample_rate; }
        virtual void instanceResetUserInterface() { fDecoder->resetUserInterface(); }
        virtual void instanceClear() {}
    
        virtual int getSampleRate() { return fSampleRate; }
    
        virtual proxy_dsp* clone() { return new proxy_dsp(fDecoder->fJSON); }
        virtual void metadata(Meta* m) { fDecoder->metadata(m); }
    
        virtual void compute(int count, FAUSTFLOAT** inputs, FAUSTFLOAT** outputs) {}
        virtual void compute(double date_usec, int count, FAUSTFLOAT** inputs, FAUSTFLOAT** outputs) {} 
        
};

#endif
/************************** END proxy-dsp.h **************************/

/************************** BEGIN JSONControl.h **************************
 FAUST Architecture File
 Copyright (C) 2003-2022 GRAME, Centre National de Creation Musicale
 ---------------------------------------------------------------------
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation; either version 2.1 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 
 EXCEPTION : As a special exception, you may create a larger work
 that contains this FAUST architecture section and distribute
 that work under terms of your choice, so long as this FAUST
 architecture section is not modified.
 *************************************************************************/

#ifndef __JSON_CONTROL__
#define __JSON_CONTROL__

#include <string>

#ifndef FAUSTFLOAT
#define FAUSTFLOAT float
#endif

struct FAUST_API JSONControl {
    
    virtual std::string getJSON() { return ""; }

    virtual void setParamValue(const std::string& path, FAUSTFLOAT value) {}

    virtual FAUSTFLOAT getParamValue(const std::string& path) { return 0; }
    
    virtual ~JSONControl()
    {}
    
};

#endif
/**************************  END  JSONControl.h **************************/

#define kActiveVoice    0
#define kFreeVoice     -1
#define kReleaseVoice  -2
#define kLegatoVoice   -3
#define kNoVoice       -4

#define VOICE_STOP_LEVEL  0.0005    // -70 db
#define MIX_BUFFER_SIZE   4096

/**
 * Allows to control zones in a grouped manner.
 */
class GroupUI : public GUI, public PathBuilder {

    private:

        // Map to associate labels with UI group items
        std::map<std::string, uiGroupItem*> fLabelZoneMap;

        // Insert a zone into the map based on the label folloing the freq/gain/gate polyphonic convention
        void insertMap(std::string label, FAUSTFLOAT* zone)
        {
            if (!MapUI::endsWith(label, "/gate")
                && !MapUI::endsWith(label, "/freq")
                && !MapUI::endsWith(label, "/key")
                && !MapUI::endsWith(label, "/gain")
                && !MapUI::endsWith(label, "/vel")
                && !MapUI::endsWith(label, "/velocity")) {

                // Groups all controllers except 'freq/key', 'gate', and 'gain/vel|velocity'
                if (fLabelZoneMap.find(label) != fLabelZoneMap.end()) {
                    fLabelZoneMap[label]->addZone(zone);
                } else {
                    fLabelZoneMap[label] = new uiGroupItem(this, zone);
                }
            }
        }

        uiCallbackItem* fPanic;

    public:

        GroupUI(FAUSTFLOAT* zone, uiCallback cb, void* arg)
        {
            fPanic = new uiCallbackItem(this, zone, cb, arg);
        }
    
        virtual ~GroupUI()
        {
            // 'fPanic' is kept and deleted in GUI, so do not delete here
        }

        // -- widget's layouts
        void openTabBox(const char* label)
        {
            pushLabel(label);
        }
        void openHorizontalBox(const char* label)
        {
            pushLabel(label);
        }
        void openVerticalBox(const char* label)
        {
            pushLabel(label);
        }
        void closeBox()
        {
            popLabel();
        }

        // -- active widgets
        void addButton(const char* label, FAUSTFLOAT* zone)
        {
            insertMap(buildPath(label), zone);
        }
        void addCheckButton(const char* label, FAUSTFLOAT* zone)
        {
            insertMap(buildPath(label), zone);
        }
        void addVerticalSlider(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT fmin, FAUSTFLOAT fmax, FAUSTFLOAT step)
        {
            insertMap(buildPath(label), zone);
        }
        void addHorizontalSlider(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT fmin, FAUSTFLOAT fmax, FAUSTFLOAT step)
        {
            insertMap(buildPath(label), zone);
        }
        void addNumEntry(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT fmin, FAUSTFLOAT fmax, FAUSTFLOAT step)
        {
            insertMap(buildPath(label), zone);
        }

        // -- passive widgets
        void addHorizontalBargraph(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT fmin, FAUSTFLOAT fmax)
        {
            insertMap(buildPath(label), zone);
        }
        void addVerticalBargraph(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT fmin, FAUSTFLOAT fmax)
        {
            insertMap(buildPath(label), zone);
        }

};

/**
 * One voice of polyphony.
 */
struct dsp_voice : public MapUI, public decorator_dsp {
    
    typedef std::function<double(int)> TransformFunction;
  
    // Convert MIDI note to frequency
    static double midiToFreq(double note)
    {
        return 440.0 * std::pow(2.0, (note-69.0)/12.0);
    }
    
    // Voice state and properties
    int fCurNote;                       // Current playing note pitch
    int fNextNote;                      // In kLegatoVoice state, next note to play
    int fNextVel;                       // In kLegatoVoice state, next velocity to play
    int fDate;                          // KeyOn date
    int fRelease;                       // Current number of samples used in release mode to detect end of note
    FAUSTFLOAT fLevel;                  // Last audio block level
    double fReleaseLengthSec;           // Maximum release length in seconds (estimated time to silence after note release)
    std::vector<std::string> fGatePath; // Paths of 'gate' control
    std::vector<std::string> fGainPath; // Paths of 'gain/vel|velocity' control
    std::vector<std::string> fFreqPath; // Paths of 'freq/key' control
    TransformFunction        fKeyFun;   // MIDI key to freq conversion function
    TransformFunction        fVelFun;   // MIDI velocity to gain conversion function
    
    FAUSTFLOAT** fInputsSlice;
    FAUSTFLOAT** fOutputsSlice;
 
    dsp_voice(dsp* dsp):decorator_dsp(dsp)
    {
        // Default conversion functions
        fVelFun = [](int velocity) { return double(velocity)/127.0; };
        fKeyFun = [](int pitch) { return midiToFreq(pitch); };
        dsp->buildUserInterface(this);
        fCurNote = kFreeVoice;
        fNextNote = fNextVel = -1;
        fLevel = FAUSTFLOAT(0);
        fDate = fRelease = 0;
        fReleaseLengthSec = 0.5;  // A half second is a reasonable default maximum release length.
        extractPaths(fGatePath, fFreqPath, fGainPath);
    }
    virtual ~dsp_voice()
    {}
    
    // Compute a slice of audio
    void computeSlice(int offset, int slice, FAUSTFLOAT** inputs, FAUSTFLOAT** outputs)
    {
        FAUSTFLOAT** inputsSlice = static_cast<FAUSTFLOAT**>(alloca(sizeof(FAUSTFLOAT*) * getNumInputs()));
        for (int chan = 0; chan < getNumInputs(); chan++) {
            inputsSlice[chan] = &(inputs[chan][offset]);
        }
        FAUSTFLOAT** outputsSlice = static_cast<FAUSTFLOAT**>(alloca(sizeof(FAUSTFLOAT*) * getNumOutputs()));
        for (int chan = 0; chan < getNumOutputs(); chan++) {
            outputsSlice[chan] = &(outputs[chan][offset]);
        }
        compute(slice, inputsSlice, outputsSlice);
    }
    
    // Compute audio in legato mode
    void computeLegato(int count, FAUSTFLOAT** inputs, FAUSTFLOAT** outputs)
    {
        int slice = count/2;
        
        // Reset envelops
        for (size_t i = 0; i < fGatePath.size(); i++) {
            setParamValue(fGatePath[i], FAUSTFLOAT(0));
        }
        
        // Compute current voice on half buffer
        computeSlice(0, slice, inputs, outputs);
         
        // Start next keyOn
        keyOn(fNextNote, fNextVel);
        
        // Compute on second half buffer
        computeSlice(slice, slice, inputs, outputs);
    }

    // Extract control paths from fullpath map
    void extractPaths(std::vector<std::string>& gate, std::vector<std::string>& freq, std::vector<std::string>& gain)
    {
        // Keep gain/vel|velocity, freq/key and gate labels
        for (const auto& it : getFullpathMap()) {
            std::string path = it.first;
            if (endsWith(path, "/gate")) {
                gate.push_back(path);
            } else if (endsWith(path, "/freq")) {
                fKeyFun = [](int pitch) { return midiToFreq(pitch); };
                freq.push_back(path);
            } else if (endsWith(path, "/key")) {
                fKeyFun = [](int pitch) { return pitch; };
                freq.push_back(path);
            } else if (endsWith(path, "/gain")) {
                fVelFun = [](int velocity) { return double(velocity)/127.0; };
                gain.push_back(path);
            } else if (endsWith(path, "/vel") || endsWith(path, "/velocity")) {
                fVelFun = [](int velocity) { return double(velocity); };
                gain.push_back(path);
            }
        }
    }
    
    // Reset voice
    void reset()
    {
        init(getSampleRate());
    }
 
    // Clear instance state
    void instanceClear()
    {
        decorator_dsp::instanceClear();
        fCurNote = kFreeVoice;
        fNextNote = fNextVel = -1;
        fLevel = FAUSTFLOAT(0);
        fDate = fRelease = 0;
    }
    
    // Keep 'pitch' and 'velocity' to fadeOut the current voice and start next one in the next buffer
    void keyOn(int pitch, int velocity, bool legato = false)
    {
        if (legato) {
            fNextNote = pitch;
            fNextVel = velocity;
        } else {
            keyOn(pitch, fVelFun(velocity));
        }
    }

    // KeyOn with normalized MIDI velocity [0..1]
    void keyOn(int pitch, double velocity)
    {
        for (size_t i = 0; i < fFreqPath.size(); i++) {
            setParamValue(fFreqPath[i], fKeyFun(pitch));
        }
        for (size_t i = 0; i < fGatePath.size(); i++) {
            setParamValue(fGatePath[i], FAUSTFLOAT(1));
        }
        for (size_t i = 0; i < fGainPath.size(); i++) {
            setParamValue(fGainPath[i], velocity);
        }
        
        fCurNote = pitch;
    }

    void keyOff(bool hard = false)
    {
        // No use of velocity for now...
        for (size_t i = 0; i < fGatePath.size(); i++) {
            setParamValue(fGatePath[i], FAUSTFLOAT(0));
        }
        
        if (hard) {
            // Immediately stop voice
            fCurNote = kFreeVoice;
        } else {
            // Release voice
            fRelease = fReleaseLengthSec * fDSP->getSampleRate();
            fCurNote = kReleaseVoice;
        }
    }
 
    // Change the voice release
    void setReleaseLength(double sec)
    {
        fReleaseLengthSec = sec;
    }

};

/**
 * A group of voices.
 */
struct dsp_voice_group {

    // GUI group for controlling voice parameters
    GroupUI fGroups;

    std::vector<dsp_voice*> fVoiceTable; // Individual voices
    dsp* fVoiceGroup;                    // Voices group to be used for GUI grouped control

    FAUSTFLOAT fPanic;  // Panic button value

    bool fVoiceControl; // Voice control mode
    bool fGroupControl; // Group control mode

    dsp_voice_group(uiCallback cb, void* arg, bool control, bool group)
        :fGroups(&fPanic, cb, arg),
        fVoiceGroup(0), fPanic(FAUSTFLOAT(0)),
        fVoiceControl(control), fGroupControl(group)
    {}

    virtual ~dsp_voice_group()
    {
        for (size_t i = 0; i < fVoiceTable.size(); i++) {
            delete fVoiceTable[i];
        }
        delete fVoiceGroup;
    }

    // Add a voice to the group
    void addVoice(dsp_voice* voice)
    {
        fVoiceTable.push_back(voice);
    }
        
    // Clear all voices from the group
    void clearVoices()
    {
        fVoiceTable.clear();
    }

    // Initialize the voice group
    void init()
    {
        // Groups all uiItem for a given path
        fVoiceGroup = new proxy_dsp(fVoiceTable[0]);
        fVoiceGroup->buildUserInterface(&fGroups);
        for (size_t i = 0; i < fVoiceTable.size(); i++) {
            fVoiceTable[i]->buildUserInterface(&fGroups);
        }
    }
    
    // Reset the user interface for each voice instance
    void instanceResetUserInterface()
    {
        for (size_t i = 0; i < fVoiceTable.size(); i++) {
            fVoiceTable[i]->instanceResetUserInterface();
        }
    }

    // Build the user interface for the voice group
    void buildUserInterface(UI* ui_interface)
    {
        if (fVoiceTable.size() > 1) {
            ui_interface->openTabBox("Polyphonic");

            // Grouped voices UI
            ui_interface->openVerticalBox("Voices");
            ui_interface->addButton("Panic", &fPanic);
            fVoiceGroup->buildUserInterface(ui_interface);
            ui_interface->closeBox();

            // If not grouped, also add individual voices UI
            if (!fGroupControl || dynamic_cast<SoundUIInterface*>(ui_interface)) {
                for (size_t i = 0; i < fVoiceTable.size(); i++) {
                    char buffer[32];
                    snprintf(buffer, 32, ((fVoiceTable.size() < 8) ? "Voice%ld" : "V%ld"), long(i+1));
                    ui_interface->openHorizontalBox(buffer);
                    fVoiceTable[i]->buildUserInterface(ui_interface);
                    ui_interface->closeBox();
                }
            }

            ui_interface->closeBox();
        } else {
            fVoiceTable[0]->buildUserInterface(ui_interface);
        }
    }

};

/**
 * Base class for MIDI controllable polyphonic DSP.
 */
#ifdef EMCC
#endif

class dsp_poly : public decorator_dsp, public midi, public JSONControl {

    protected:
    
    #ifdef EMCC
        MapUI fMapUI;               // Map for UI control
        std::string fJSON;          // JSON representation of the UI
        midi_handler fMidiHandler;  // MIDI handler for the UI
        MidiUI fMIDIUI;             // MIDI UI for the DSP
    #endif
    
    public:
    
    #ifdef EMCC
        dsp_poly(dsp* dsp):decorator_dsp(dsp), fMIDIUI(&fMidiHandler)
        {
            JSONUI jsonui(getNumInputs(), getNumOutputs());
            buildUserInterface(&jsonui);
            fJSON = jsonui.JSON(true);
            buildUserInterface(&fMapUI);
            buildUserInterface(&fMIDIUI);
        }
    #else
        dsp_poly(dsp* dsp):decorator_dsp(dsp)
        {}
    #endif
    
        virtual ~dsp_poly() {}
    
        // Reimplemented for EMCC
    #ifdef EMCC
        virtual int getNumInputs() { return decorator_dsp::getNumInputs(); }
        virtual int getNumOutputs() { return decorator_dsp::getNumOutputs(); }
        virtual void buildUserInterface(UI* ui_interface) { decorator_dsp::buildUserInterface(ui_interface); }
        virtual int getSampleRate() { return decorator_dsp::getSampleRate(); }
        virtual void init(int sample_rate) { decorator_dsp::init(sample_rate); }
        virtual void instanceInit(int sample_rate) { decorator_dsp::instanceInit(sample_rate); }
        virtual void instanceConstants(int sample_rate) { decorator_dsp::instanceConstants(sample_rate); }
        virtual void instanceResetUserInterface() { decorator_dsp::instanceResetUserInterface(); }
        virtual void instanceClear() { decorator_dsp::instanceClear(); }
        virtual dsp_poly* clone() { return new dsp_poly(fDSP->clone()); }
        virtual void metadata(Meta* m) { decorator_dsp::metadata(m); }
    
        // Additional API
        std::string getJSON()
        {
            return fJSON;
        }
    
        virtual void setParamValue(const std::string& path, FAUSTFLOAT value)
        {
            fMapUI.setParamValue(path, value);
            GUI::updateAllGuis();
        }
        
        virtual FAUSTFLOAT getParamValue(const std::string& path) { return fMapUI.getParamValue(path); }

        virtual void computeJS(int count, uintptr_t inputs, uintptr_t outputs)
        {
            decorator_dsp::compute(count, reinterpret_cast<FAUSTFLOAT**>(inputs),reinterpret_cast<FAUSTFLOAT**>(outputs));
        }
    #endif
    
        virtual MapUI* keyOn(int channel, int pitch, int velocity)
        {
            return midi::keyOn(channel, pitch, velocity);
        }
        virtual void keyOff(int channel, int pitch, int velocity)
        {
            midi::keyOff(channel, pitch, velocity);
        }
        virtual void keyPress(int channel, int pitch, int press)
        {
            midi::keyPress(channel, pitch, press);
        }
        virtual void chanPress(int channel, int press)
        {
            midi::chanPress(channel, press);
        }
        virtual void ctrlChange(int channel, int ctrl, int value)
        {
            midi::ctrlChange(channel, ctrl, value);
        }
        virtual void ctrlChange14bits(int channel, int ctrl, int value)
        {
            midi::ctrlChange14bits(channel, ctrl, value);
        }
        virtual void pitchWheel(int channel, int wheel)
        {
        #ifdef EMCC
            fMIDIUI.pitchWheel(0., channel, wheel);
            GUI::updateAllGuis();
        #else
            midi::pitchWheel(channel, wheel);
        #endif
        }
        virtual void progChange(int channel, int pgm)
        {
            midi::progChange(channel, pgm);
        }
    
        // Change the voice release
        virtual void setReleaseLength(double seconds)
        {}
    
};

/**
 * Polyphonic DSP: groups a set of DSP to be played together or triggered by MIDI.
 *
 * All voices are preallocated by cloning the single DSP voice given at creation time.
 * Dynamic voice allocation is done in 'getFreeVoice'
 */
class bbdmi_multi_granulator14_poly : public dsp_voice_group, public dsp_poly {

    private:

        FAUSTFLOAT** fMixBuffer;        // Intermediate buffer for mixing voices
        FAUSTFLOAT** fOutBuffer;        // Intermediate buffer for output
        midi_interface* fMidiHandler;   // The midi_interface the DSP is connected to
        int fDate;                      // Current date for managing voices
    
        // Fade out the audio in the buffer
        void fadeOut(int count, FAUSTFLOAT** outBuffer)
        {
            // FadeOut on half buffer
            for (int chan = 0; chan < getNumOutputs(); chan++) {
                double factor = 1., step = 1./double(count);
                for (int frame = 0; frame < count; frame++) {
                    outBuffer[chan][frame] *= factor;
                    factor -= step;
                }
            }
        }
    
        // Mix the audio from the mix buffer to the output buffer, and also calculate the maximum level on the buffer
        FAUSTFLOAT mixCheckVoice(int count, FAUSTFLOAT** mixBuffer, FAUSTFLOAT** outBuffer)
        {
            FAUSTFLOAT level = 0;
            for (int chan = 0; chan < getNumOutputs(); chan++) {
                FAUSTFLOAT* mixChannel = mixBuffer[chan];
                FAUSTFLOAT* outChannel = outBuffer[chan];
                for (int frame = 0; frame < count; frame++) {
                    level = std::max<FAUSTFLOAT>(level, (FAUSTFLOAT)fabs(mixChannel[frame]));
                    outChannel[frame] += mixChannel[frame];
                }
            }
            return level;
        }
    
        // Mix the audio from the mix buffer to the output buffer
        void mixVoice(int count, FAUSTFLOAT** mixBuffer, FAUSTFLOAT** outBuffer)
        {
            for (int chan = 0; chan < getNumOutputs(); chan++) {
                FAUSTFLOAT* mixChannel = mixBuffer[chan];
                FAUSTFLOAT* outChannel = outBuffer[chan];
                for (int frame = 0; frame < count; frame++) {
                    outChannel[frame] += mixChannel[frame];
                }
            }
        }
    
        // Copy the audio from one buffer to another
        void copy(int count, FAUSTFLOAT** mixBuffer, FAUSTFLOAT** outBuffer)
        {
            for (int chan = 0; chan < getNumOutputs(); chan++) {
                memcpy(outBuffer[chan], mixBuffer[chan], count * sizeof(FAUSTFLOAT));
            }
        }

        // Clear the audio buffer
        void clear(int count, FAUSTFLOAT** outBuffer)
        {
            for (int chan = 0; chan < getNumOutputs(); chan++) {
                memset(outBuffer[chan], 0, count * sizeof(FAUSTFLOAT));
            }
        }
    
        // Get the index of a voice currently playing a specific pitch
        int getPlayingVoice(int pitch)
        {
            int voice_playing = kNoVoice;
            int oldest_date_playing = INT_MAX;
            
            for (size_t i = 0; i < fVoiceTable.size(); i++) {
                if (fVoiceTable[i]->fCurNote == pitch) {
                    // Keeps oldest playing voice
                    if (fVoiceTable[i]->fDate < oldest_date_playing) {
                        oldest_date_playing = fVoiceTable[i]->fDate;
                        voice_playing = int(i);
                    }
                }
            }
            
            return voice_playing;
        }
    
        // Allocate a voice with a given type
        int allocVoice(int voice, int type)
        {
            fVoiceTable[voice]->fDate++;
            fVoiceTable[voice]->fCurNote = type;
            return voice;
        }
    
        // Get a free voice for allocation, always returns a voice
        int getFreeVoice()
        {
            // Looks for the first available voice
            for (size_t i = 0; i < fVoiceTable.size(); i++) {
                if (fVoiceTable[i]->fCurNote == kFreeVoice) {
                    return allocVoice(i, kActiveVoice);
                }
            }

            // Otherwise steal one
            int voice_release = kNoVoice;
            int voice_playing = kNoVoice;
            int oldest_date_release = INT_MAX;
            int oldest_date_playing = INT_MAX;

            // Scan all voices
            for (size_t i = 0; i < fVoiceTable.size(); i++) {
                if (fVoiceTable[i]->fCurNote == kReleaseVoice) {
                    // Keeps oldest release voice
                    if (fVoiceTable[i]->fDate < oldest_date_release) {
                        oldest_date_release = fVoiceTable[i]->fDate;
                        voice_release = int(i);
                    }
                } else {
                    // Otherwise keeps oldest playing voice
                    if (fVoiceTable[i]->fDate < oldest_date_playing) {
                        oldest_date_playing = fVoiceTable[i]->fDate;
                        voice_playing = int(i);
                    }
                }
            }
        
            // Then decide which one to steal
            if (oldest_date_release != INT_MAX) {
                fprintf(stderr, "Steal release voice : voice_date = %d cur_date = %d voice = %d \n",
                        fVoiceTable[voice_release]->fDate,
                        fDate,
                        voice_release);
                return allocVoice(voice_release, kLegatoVoice);
            } else if (oldest_date_playing != INT_MAX) {
                fprintf(stderr, "Steal playing voice : voice_date = %d cur_date = %d voice = %d \n",
                        fVoiceTable[voice_playing]->fDate,
                        fDate,
                        voice_release);
                return allocVoice(voice_playing, kLegatoVoice);
            } else {
                assert(false);
                return kNoVoice;
            }
        }

        // Callback for panic button
        static void panic(FAUSTFLOAT val, void* arg)
        {
            if (val == FAUSTFLOAT(1)) {
                static_cast<bbdmi_multi_granulator14_poly*>(arg)->allNotesOff(true);
            }
        }

        // Check if the DSP is polyphonic
        bool checkPolyphony()
        {
            if (fVoiceTable.size() > 0) {
                return true;
            } else {
                fprintf(stderr, "DSP is not polyphonic...\n");
                return false;
            }
        }

    public:
    
        /**
         * Constructor.
         *
         * @param dsp - the dsp to be used for one voice. Beware: bbdmi_multi_granulator14_poly will use and finally delete the pointer.
         * @param nvoices - number of polyphony voices, should be at least 1
         * @param control - whether voices will be dynamically allocated and controlled (typically by a MIDI controler).
         *                If false all voices are always running.
         * @param group - if true, voices are not individually accessible, a global "Voices" tab will automatically dispatch
         *                a given control on all voices, assuming GUI::updateAllGuis() is called.
         *                If false, all voices can be individually controlled.
         *
         */
        bbdmi_multi_granulator14_poly(dsp* dsp,
                   int nvoices,
                   bool control = false,
                   bool group = true)
        : dsp_voice_group(panic, this, control, group), dsp_poly(dsp) // dsp parameter is deallocated by ~dsp_poly
        {
            fDate = 0;
            fMidiHandler = nullptr;

            // Create voices
            assert(nvoices > 0);
            for (int i = 0; i < nvoices; i++) {
                addVoice(new dsp_voice(dsp->clone()));
            }

            // Init audio output buffers
            fMixBuffer = new FAUSTFLOAT*[getNumOutputs()];
            fOutBuffer = new FAUSTFLOAT*[getNumOutputs()];
            for (int chan = 0; chan < getNumOutputs(); chan++) {
                fMixBuffer[chan] = new FAUSTFLOAT[MIX_BUFFER_SIZE];
                fOutBuffer[chan] = new FAUSTFLOAT[MIX_BUFFER_SIZE];
            }

            dsp_voice_group::init();
        }

        virtual ~bbdmi_multi_granulator14_poly()
        {
            // Remove from fMidiHandler
            if (fMidiHandler) fMidiHandler->removeMidiIn(this);
            for (int chan = 0; chan < getNumOutputs(); chan++) {
                delete[] fMixBuffer[chan];
                delete[] fOutBuffer[chan];
            }
            delete[] fMixBuffer;
            delete[] fOutBuffer;
            
        }

        // DSP API
        void buildUserInterface(UI* ui_interface)
        {
            // MidiUI ui_interface contains the midi_handler connected to the MIDI driver
            if (dynamic_cast<midi_interface*>(ui_interface)) {
                fMidiHandler = dynamic_cast<midi_interface*>(ui_interface);
                fMidiHandler->addMidiIn(this);
            }
            dsp_voice_group::buildUserInterface(ui_interface);
        }

        void init(int sample_rate)
        {
            decorator_dsp::init(sample_rate);
            fVoiceGroup->init(sample_rate);
            fPanic = FAUSTFLOAT(0);
            
            // Init voices
            for (size_t i = 0; i < fVoiceTable.size(); i++) {
                fVoiceTable[i]->init(sample_rate);
            }
        }
    
        void instanceInit(int samplingFreq)
        {
            instanceConstants(samplingFreq);
            instanceResetUserInterface();
            instanceClear();
        }

        void instanceConstants(int sample_rate)
        {
            decorator_dsp::instanceConstants(sample_rate);
            fVoiceGroup->instanceConstants(sample_rate);
            
            // Init voices
            for (size_t i = 0; i < fVoiceTable.size(); i++) {
                fVoiceTable[i]->instanceConstants(sample_rate);
            }
        }

        void instanceResetUserInterface()
        {
            decorator_dsp::instanceResetUserInterface();
            fVoiceGroup->instanceResetUserInterface();
            fPanic = FAUSTFLOAT(0);
            
            for (size_t i = 0; i < fVoiceTable.size(); i++) {
                fVoiceTable[i]->instanceResetUserInterface();
            }
        }

        void instanceClear()
        {
            decorator_dsp::instanceClear();
            fVoiceGroup->instanceClear();
            
            for (size_t i = 0; i < fVoiceTable.size(); i++) {
                fVoiceTable[i]->instanceClear();
            }
        }

        virtual bbdmi_multi_granulator14_poly* clone()
        {
            return new bbdmi_multi_granulator14_poly(fDSP->clone(), int(fVoiceTable.size()), fVoiceControl, fGroupControl);
        }

        void compute(int count, FAUSTFLOAT** inputs, FAUSTFLOAT** outputs)
        {
            assert(count <= MIX_BUFFER_SIZE);

            // First clear the intermediate fOutBuffer
            clear(count, fOutBuffer);

            if (fVoiceControl) {
                // Mix all playing voices
                for (size_t i = 0; i < fVoiceTable.size(); i++) {
                    dsp_voice* voice = fVoiceTable[i];
                    if (voice->fCurNote == kLegatoVoice) {
                        // Play from current note and next note
                        voice->computeLegato(count, inputs, fMixBuffer);
                        // FadeOut on first half buffer
                        fadeOut(count/2, fMixBuffer);
                        // Mix it in result
                        voice->fLevel = mixCheckVoice(count, fMixBuffer, fOutBuffer);
                    } else if (voice->fCurNote != kFreeVoice) {
                        // Compute current note
                        voice->compute(count, inputs, fMixBuffer);
                        // Mix it in result
                        voice->fLevel = mixCheckVoice(count, fMixBuffer, fOutBuffer);
                        // Check the level to possibly set the voice in kFreeVoice again
                        voice->fRelease -= count;
                        if ((voice->fCurNote == kReleaseVoice)
                            && (voice->fRelease < 0)
                            && (voice->fLevel < VOICE_STOP_LEVEL)) {
                            voice->fCurNote = kFreeVoice;
                        }
                    }
                }
            } else {
                // Mix all voices
                for (size_t i = 0; i < fVoiceTable.size(); i++) {
                    fVoiceTable[i]->compute(count, inputs, fMixBuffer);
                    mixVoice(count, fMixBuffer, fOutBuffer);
                }
            }
            
            // Finally copy intermediate buffer to outputs
            copy(count, fOutBuffer, outputs);
        }

        void compute(double date_usec, int count, FAUSTFLOAT** inputs, FAUSTFLOAT** outputs)
        {
            compute(count, inputs, outputs);
        }
    
        // Terminate all active voices, gently or immediately (depending of 'hard' value)
        void allNotesOff(bool hard = false)
        {
            for (size_t i = 0; i < fVoiceTable.size(); i++) {
                fVoiceTable[i]->keyOff(hard);
            }
        }
 
        // Additional polyphonic API
        MapUI* newVoice()
        {
            return fVoiceTable[getFreeVoice()];
        }

        void deleteVoice(MapUI* voice)
        {
            auto it = find(fVoiceTable.begin(), fVoiceTable.end(), reinterpret_cast<dsp_voice*>(voice));
            if (it != fVoiceTable.end()) {
                dsp_voice* voice = *it;
                voice->keyOff();
                voice->reset();
            } else {
                fprintf(stderr, "Voice not found\n");
            }
        }

        // MIDI API
        MapUI* keyOn(int channel, int pitch, int velocity)
        {
            if (checkPolyphony()) {
                int voice = getFreeVoice();
                fVoiceTable[voice]->keyOn(pitch, velocity, fVoiceTable[voice]->fCurNote == kLegatoVoice);
                return fVoiceTable[voice];
            } else {
                return 0;
            }
        }

        void keyOff(int channel, int pitch, int velocity = 127)
        {
            if (checkPolyphony()) {
                int voice = getPlayingVoice(pitch);
                if (voice != kNoVoice) {
                    fVoiceTable[voice]->keyOff();
                } else {
                    fprintf(stderr, "Playing pitch = %d not found\n", pitch);
                }
            }
        }

        void ctrlChange(int channel, int ctrl, int value)
        {
            if (ctrl == ALL_NOTES_OFF || ctrl == ALL_SOUND_OFF) {
                allNotesOff();
            }
        }

        // Change the voice release
        void setReleaseLength(double seconds)
        {
            for (size_t i = 0; i < fVoiceTable.size(); i++) {
                fVoiceTable[i]->setReleaseLength(seconds);
            }
        }

};

/**
 * Polyphonic DSP with an integrated effect.
 */
class dsp_poly_effect : public dsp_poly {
    
    private:
    
        // fPolyDSP will respond to MIDI messages.
        dsp_poly* fPolyDSP;
        
    public:
        
        dsp_poly_effect(dsp_poly* voice, dsp* combined)
        :dsp_poly(combined), fPolyDSP(voice)
        {}
        
        virtual ~dsp_poly_effect()
        {
            // dsp_poly_effect is also a decorator_dsp, which will free fPolyDSP
        }
    
        // MIDI API
        MapUI* keyOn(int channel, int pitch, int velocity)
        {
            return fPolyDSP->keyOn(channel, pitch, velocity);
        }
        void keyOff(int channel, int pitch, int velocity)
        {
            fPolyDSP->keyOff(channel, pitch, velocity);
        }
        void keyPress(int channel, int pitch, int press)
        {
            fPolyDSP->keyPress(channel, pitch, press);
        }
        void chanPress(int channel, int press)
        {
            fPolyDSP->chanPress(channel, press);
        }
        void ctrlChange(int channel, int ctrl, int value)
        {
            fPolyDSP->ctrlChange(channel, ctrl, value);
        }
        void ctrlChange14bits(int channel, int ctrl, int value)
        {
            fPolyDSP->ctrlChange14bits(channel, ctrl, value);
        }
        void pitchWheel(int channel, int wheel)
        {
            fPolyDSP->pitchWheel(channel, wheel);
        }
        void progChange(int channel, int pgm)
        {
            fPolyDSP->progChange(channel, pgm);
        }
    
        // Change the voice release
        void setReleaseLength(double sec)
        {
            fPolyDSP->setReleaseLength(sec);
        }
    
};

/**
 * Polyphonic DSP factory class. Helper code to support polyphonic DSP source with an integrated effect.
 */
struct dsp_poly_factory : public dsp_factory {
    
    dsp_factory* fProcessFactory;
    dsp_factory* fEffectFactory;
    
    dsp* adaptDSP(dsp* dsp, bool is_double)
    {
        return (is_double) ? new dsp_sample_adapter<double, float>(dsp) : dsp;
    }

    dsp_poly_factory(dsp_factory* process_factory = nullptr,
                     dsp_factory* effect_factory = nullptr):
    fProcessFactory(process_factory)
    ,fEffectFactory(effect_factory)
    {}

    virtual ~dsp_poly_factory()
    {}

    std::string getName() { return fProcessFactory->getName(); }
    std::string getSHAKey() { return fProcessFactory->getSHAKey(); }
    std::string getDSPCode() { return fProcessFactory->getDSPCode(); }
    std::string getCompileOptions() { return fProcessFactory->getCompileOptions(); }
    std::vector<std::string> getLibraryList() { return fProcessFactory->getLibraryList(); }
    std::vector<std::string> getIncludePathnames() { return fProcessFactory->getIncludePathnames(); }
    std::vector<std::string> getWarningMessages() { return fProcessFactory->getWarningMessages(); }
   
    std::string getEffectCode(const std::string& dsp_content)
    {
        std::stringstream effect_code;
        effect_code << "adapt(1,1) = _; adapt(2,2) = _,_; adapt(1,2) = _ <: _,_; adapt(2,1) = _,_ :> _;";
        effect_code << "adaptor(F,G) = adapt(outputs(F),inputs(G)); dsp_code = environment{ " << dsp_content << " };";
        effect_code << "process = adaptor(dsp_code.process, dsp_code.effect) : dsp_code.effect;";
        return effect_code.str();
    }

    virtual void setMemoryManager(dsp_memory_manager* manager)
    {
        fProcessFactory->setMemoryManager(manager);
        if (fEffectFactory) {
            fEffectFactory->setMemoryManager(manager);
        }
    }
    virtual dsp_memory_manager* getMemoryManager() { return fProcessFactory->getMemoryManager(); }

    /* Create a new polyphonic DSP instance with global effect, to be deleted with C++ 'delete'
     *
     * @param nvoices - number of polyphony voices, should be at least 1.
     * If -1 is used, the voice number found in the 'declare options "[nvoices:N]";' section will be used.
     * @param control - whether voices will be dynamically allocated and controlled (typically by a MIDI controler).
     *                If false all voices are always running.
     * @param group - if true, voices are not individually accessible, a global "Voices" tab will automatically dispatch
     *                a given control on all voices, assuming GUI::updateAllGuis() is called.
     *                If false, all voices can be individually controlled.
     * @param is_double - if true, internally allocated DSPs will be adapted to receive 'double' samples.
     */
    dsp_poly* createPolyDSPInstance(int nvoices, bool control, bool group, bool is_double = false)
    {
        if (nvoices == -1) {
            // Get 'nvoices' from the metadata declaration
            dsp* dsp = fProcessFactory->createDSPInstance();
            bool midi_sync;
            MidiMeta::analyse(dsp, midi_sync, nvoices);
            delete dsp;
        }
        dsp_poly* dsp_poly = new bbdmi_multi_granulator14_poly(adaptDSP(fProcessFactory->createDSPInstance(), is_double), nvoices, control, group);
        if (fEffectFactory) {
            // the 'dsp_poly' object has to be controlled with MIDI, so kept separated from new dsp_sequencer(...) object
            return new dsp_poly_effect(dsp_poly, new dsp_sequencer(dsp_poly, adaptDSP(fEffectFactory->createDSPInstance(), is_double)));
        } else {
            return new dsp_poly_effect(dsp_poly, dsp_poly);
        }
    }

    /* Create a new DSP instance, to be deleted with C++ 'delete' */
    dsp* createDSPInstance()
    {
        return fProcessFactory->createDSPInstance();
    }

};

#endif // __poly_dsp__
/************************** END poly-dsp.h **************************/

list<GUI*> GUI::fGuiList;
ztimedmap GUI::gTimedZoneMap;

static t_class* faust_class;

/*--------------------------------------------------------------------------*/
static const char* getCodeSize()
{
    int tmp;
    return (sizeof(&tmp) == 8) ? "64 bits" : "32 bits";
}

/*--------------------------------------------------------------------------*/
typedef struct faust
{
    t_pxobject m_ob;
    t_atom *m_seen, *m_want;
    map<string, vector<t_object*> > m_output_table;
    short m_where;
    bool m_mute;
    void** m_args;
    mspUI* m_dspUI;
    dsp* m_dsp;
    void* m_control_outlet;
    char* m_json;
    t_systhread_mutex m_mutex;    
    int m_Inputs;
    int m_Outputs;
    SaveUI* m_savedUI;
#ifdef MIDICTRL
    MidiUI* m_midiUI;
    max_midi* m_midiHandler;
    void* m_midi_outlet;
#endif
#ifdef SOUNDFILE
    SoundUI* m_soundInterface;
#endif
#ifdef OSCCTRL
    OSCUI* m_oscInterface;
#endif
} t_faust;

void faust_create_jsui(t_faust* x);
void faust_make_json(t_faust* x);

/*--------------------------------------------------------------------------*/
void faust_allocate(t_faust* x, int nvoices)
{
    // Delete old
    delete x->m_dsp;
    delete x->m_dspUI;
    x->m_dspUI = new mspUI();
    
    if (nvoices > 0) {
    #ifdef POST
        post("polyphonic DSP voices = %d", nvoices);
    #endif
        x->m_dsp = new bbdmi_multi_granulator14_poly(new bbdmi_multi_granulator14(), nvoices, true, true);
    #ifdef POLY2
        x->m_dsp = new dsp_sequencer(x->m_dsp, new effect());
    #endif
    } else {
    #ifdef POST
        post("monophonic DSP");
    #endif
        // Create a DS/US + Filter adapted DSP
        std::string error;
        x->m_dsp = createSRAdapter<FAUSTFLOAT>(new bbdmi_multi_granulator14(), error, DOWN_SAMPLING, UP_SAMPLING, FILTER_TYPE);
    }
    
#ifdef MIDICTRL
    x->m_dsp->buildUserInterface(x->m_midiUI);
#endif
  
    // Possible sample adaptation
    if (sizeof(FAUSTFLOAT) == 4) {
        x->m_dsp = new dsp_sample_adapter<FAUSTFLOAT, double>(x->m_dsp);
    }
    
    x->m_Inputs = x->m_dsp->getNumInputs();
    x->m_Outputs = x->m_dsp->getNumOutputs();
    
    // Initialize at the system's sampling rate
    x->m_dsp->init(long(sys_getsr()));
    
    // Initialize User Interface (here connnection with controls)
    x->m_dsp->buildUserInterface(x->m_dspUI);
    
#ifdef SOUNDFILE
    x->m_dsp->buildUserInterface(x->m_soundInterface);
#endif
    
    // Prepare JSON
    faust_make_json(x);
    
    // Send JSON to JS script
    faust_create_jsui(x);
    
#ifdef OSCCTRL
    x->m_oscInterface = NULL;
#endif
    
    // Load old controller state
    x->m_dsp->buildUserInterface(x->m_savedUI);
}

/*--------------------------------------------------------------------------*/
void faust_anything(t_faust* obj, t_symbol* s, short ac, t_atom* av)
{
    bool res = false;
    string name = string((s)->s_name);
    
    // If no argument is there, consider it as a toggle message for a button
    if (ac == 0 && obj->m_dspUI->isValue(name)) {
        
        FAUSTFLOAT off = FAUSTFLOAT(0.0);
        FAUSTFLOAT on = FAUSTFLOAT(1.0);
        obj->m_dspUI->setValue(name, off);
        obj->m_dspUI->setValue(name, on);
        
        av[0].a_type = A_FLOAT;
        av[0].a_w.w_float = off;
        faust_anything(obj, s, 1, av);
        
    } else if (mspUI::checkDigit(name)) { // List of values
        
        int pos, ndigit = 0;
        for (pos = name.size() - 1; pos >= 0; pos--) {
            if (isdigit(name[pos]) || name[pos] == ' ') {
                ndigit++;
            } else {
                break;
            }
        }
        pos++;
        
        string prefix = name.substr(0, pos);
        string num_base = name.substr(pos);
        int num = atoi(num_base.c_str());
        
        int i;
        t_atom* ap;
       
        // Increment ap each time to get to the next atom
        for (i = 0, ap = av; i < ac; i++, ap++) {
            FAUSTFLOAT value;
            switch (atom_gettype(ap)) {
                case A_LONG:
                    value = FAUSTFLOAT(ap[0].a_w.w_long);
                    break;
                case A_FLOAT:
                    value = FAUSTFLOAT(ap[0].a_w.w_float);
                    break;
                default:
                    post("Invalid argument in parameter setting"); 
                    return;         
            }
            
            string num_val = to_string(num + i);
            stringstream param_name; param_name << prefix;
            for (int i = 0; i < ndigit - mspUI::countDigit(num_val); i++) {
                param_name << ' ';
            }
            param_name << num_val;
              
            // Try special naming scheme for list of parameters
            res = obj->m_dspUI->setValue(param_name.str(), value);
            
            // Otherwise try standard name
            if (!res) {
                res = obj->m_dspUI->setValue(name, value);
            }
            if (!res) {
                post("Unknown parameter : %s", (s)->s_name);
            }
        }
        
    } else {
        // Standard parameter name
        FAUSTFLOAT value = (av[0].a_type == A_LONG) ? FAUSTFLOAT(av[0].a_w.w_long) : FAUSTFLOAT(av[0].a_w.w_float);
        res = obj->m_dspUI->setValue(name, value);
        if (!res) {
            post("Unknown parameter : %s", (s)->s_name);
        }
    }
}

/*--------------------------------------------------------------------------*/
void faust_polyphony(t_faust* x, t_symbol* s, short ac, t_atom* av)
{
    if (systhread_mutex_lock(x->m_mutex) == MAX_ERR_NONE) {
        faust_allocate(x, av[0].a_w.w_long);
        systhread_mutex_unlock(x->m_mutex);
    } else {
        post("Mutex lock cannot be taken...");
    }
}

/*--------------------------------------------------------------------------*/
#ifdef MIDICTRL
void faust_midievent(t_faust* x, t_symbol* s, short ac, t_atom* av) 
{
    if (ac > 0) {
        int type = (int)av[0].a_w.w_long & 0xf0;
        int channel = (int)av[0].a_w.w_long & 0x0f;
                
        if (ac == 1) {
            x->m_midiHandler->handleSync(0.0, av[0].a_w.w_long);
        } else if (ac == 2) {
            x->m_midiHandler->handleData1(0.0, type, channel, av[1].a_w.w_long);
        } else if (ac == 3) {
            x->m_midiHandler->handleData2(0.0, type, channel, av[1].a_w.w_long, av[2].a_w.w_long);
        }
    }
}
#endif

/*--------------------------------------------------------------------------*/
void faust_create_jsui(t_faust* x)
{
    t_object *patcher, *box, *obj;
    object_obex_lookup((t_object*)x, gensym("#P"), &patcher);
    
    for (box = jpatcher_get_firstobject(patcher); box; box = jbox_get_nextobject(box)) {
        obj = jbox_get_object(box);
        // Notify JSON
        if (obj && strcmp(object_classname(obj)->s_name, "js") == 0) {
            t_atom json;
            atom_setsym(&json, gensym(x->m_json));
            object_method_typed(obj, gensym("anything"), 1, &json, 0);
        }
    }
        
    // Keep all outputs to be notified in update_outputs
    x->m_output_table.clear();
    for (box = jpatcher_get_firstobject(patcher); box; box = jbox_get_nextobject(box)) {
        obj = jbox_get_object(box);
        t_symbol* scriptingname = jbox_get_varname(obj); // scripting name
        // Keep control outputs
        if (scriptingname && x->m_dspUI->isOutputValue(scriptingname->s_name)) {
            x->m_output_table[scriptingname->s_name].push_back(obj);
        }
    }
}

/*--------------------------------------------------------------------------*/
void faust_update_outputs(t_faust* x)
{
    for (const auto& it1 : x->m_output_table) {
        bool new_val = false;
        FAUSTFLOAT value = x->m_dspUI->getOutputValue(it1.first, new_val);
        if (new_val) {
            t_atom at_value;
            atom_setfloat(&at_value, value);
            for (const auto& it2 : it1.second) {
                object_method_typed(it2, gensym("float"), 1, &at_value, 0);
            }
        }
    }
}

/*--------------------------------------------------------------------------*/
void faust_make_json(t_faust* x)
{
    // Prepare JSON
    if (x->m_json) free(x->m_json);
    JSONUI builder(x->m_dsp->getNumInputs(), x->m_dsp->getNumOutputs());
    x->m_dsp->metadata(&builder);
    x->m_dsp->buildUserInterface(&builder);
    x->m_json = strdup(builder.JSON().c_str());
}

/*--------------------------------------------------------------------------*/
void* faust_new(t_symbol* s, short ac, t_atom* av)
{
    bool midi_sync = false;
    int nvoices = 0;
    t_faust* x = (t_faust*)object_alloc(faust_class);
    
    bbdmi_multi_granulator14* tmp_dsp = new bbdmi_multi_granulator14();
    MidiMeta::analyse(tmp_dsp, midi_sync, nvoices);
 #ifdef SOUNDFILE
    Max_Meta3 meta3;
    tmp_dsp->metadata(&meta3);
    string bundle_path_str = SoundUI::getBinaryPathFrom(meta3.fName);
    if (bundle_path_str == "") {
        post("Bundle_path '%s' cannot be found!", meta3.fName.c_str());
    }
    x->m_soundInterface = new SoundUI(bundle_path_str, -1, nullptr, sizeof(FAUSTFLOAT) == 8);
 #endif
    delete tmp_dsp;
    
    x->m_savedUI = new SaveLabelUI();
    // allocation is done in faust_allocate
    x->m_dspUI = NULL;
    x->m_dsp = NULL;
    x->m_json = NULL;
    x->m_mute = false;
    x->m_control_outlet = outlet_new((t_pxobject*)x, (char*)"list");
    
#ifdef MIDICTRL
    x->m_midi_outlet = outlet_new((t_pxobject*)x, NULL);
    x->m_midiHandler = new max_midi(x->m_midi_outlet);
    x->m_midiUI = new MidiUI(x->m_midiHandler);
#endif
    
    faust_allocate(x, nvoices);
   
    if (systhread_mutex_new(&x->m_mutex, SYSTHREAD_MUTEX_NORMAL) != MAX_ERR_NONE) {
        post("Cannot allocate mutex...");
    }
    
    int num_input;
    if (x->m_dspUI->isMulti()) {
        num_input = x->m_dsp->getNumInputs() + 1;
    } else {
        num_input = x->m_dsp->getNumInputs();
    }
    
    x->m_args = (void**)calloc((num_input + x->m_dsp->getNumOutputs()) + 2, sizeof(void*));
    
    /* Multi in */
    dsp_setup((t_pxobject*)x, num_input);

    /* Multi out */
    for (int i = 0; i < x->m_dsp->getNumOutputs(); i++) {
        outlet_new((t_pxobject*)x, (char*)"signal");
    }

    ((t_pxobject*)x)->z_misc = Z_NO_INPLACE; // To assure input and output buffers are actually different
 
    // Display controls
#ifdef POST
    x->m_dspUI->displayControls();
#endif
    
    // Get attributes values
    attr_args_process(x, ac, av);
    return x;
}

#ifdef OSCCTRL
// osc 'IP inport outport xmit bundle'
/*--------------------------------------------------------------------------*/
void faust_osc(t_faust* x, t_symbol* s, short ac, t_atom* av)
{
    if (ac == 5) {
        if (systhread_mutex_lock(x->m_mutex) == MAX_ERR_NONE) {
            
            delete x->m_oscInterface;
            
            const char* argv1[32];
            int argc1 = 0;
            
            argv1[argc1++] = "Faust";
            
            argv1[argc1++]  = "-desthost";
            argv1[argc1++]  = atom_getsym(&av[0])->s_name;
            
            char inport[32];
            snprintf(inport, 32, "%ld", long(av[1].a_w.w_long));
            argv1[argc1++] = "-port";
            argv1[argc1++] = inport;
            
            char outport[32];
            snprintf(outport, 32, "%ld", long(av[2].a_w.w_long));
            argv1[argc1++] = "-outport";
            argv1[argc1++] = outport;
            
            char xmit[32];
            snprintf(xmit, 32, "%ld", long(av[3].a_w.w_long));
            argv1[argc1++] = "-xmit";
            argv1[argc1++] = xmit;
            
            char bundle[32];
            snprintf(bundle, 32, "%ld", long(av[4].a_w.w_long));
            argv1[argc1++] = "-bundle";
            argv1[argc1++] = bundle;
            
            x->m_oscInterface = new OSCUI("Faust", argc1, (char**)argv1);
            x->m_dsp->buildUserInterface(x->m_oscInterface);
            x->m_oscInterface->run();

            post(x->m_oscInterface->getInfos().c_str());

            systhread_mutex_unlock(x->m_mutex);
        } else {
            post("Mutex lock cannot be taken...");
        }
    } else {
        post("Should be : osc 'IP inport outport xmit(0|1|2) bundle(0|1)'");
    }
}
#endif

/*--------------------------------------------------------------------------*/
// Reset controllers to init value and send [path, init, min, max]
void faust_init(t_faust* x, t_symbol* s, short ac, t_atom* av)
{
    // Reset internal state
    x->m_savedUI->reset();
    
    // Input controllers
    for (mspUI::iterator it = x->m_dspUI->begin2(); it != x->m_dspUI->end2(); it++) {
        t_atom myList[4];
        atom_setsym(&myList[0], gensym((*it).first.c_str()));
        atom_setfloat(&myList[1], (*it).second->getInitValue());    // init value
        atom_setfloat(&myList[2], (*it).second->getMinValue());
        atom_setfloat(&myList[3], (*it).second->getMaxValue());
        outlet_list(x->m_control_outlet, 0, 4, myList);
    }
    // Output controllers
    for (mspUI::iterator it = x->m_dspUI->begin4(); it != x->m_dspUI->end4(); it++) {
        t_atom myList[4];
        atom_setsym(&myList[0], gensym((*it).first.c_str()));
        atom_setfloat(&myList[1], (*it).second->getInitValue());    // init value
        atom_setfloat(&myList[2], (*it).second->getMinValue());
        atom_setfloat(&myList[3], (*it).second->getMaxValue());
        outlet_list(x->m_control_outlet, 0, 4, myList);
    }
 }

/*--------------------------------------------------------------------------*/
void faust_dump_inputs(t_faust* x)
{
    // Input controllers
    for (mspUI::iterator it = x->m_dspUI->begin2(); it != x->m_dspUI->end2(); it++) {
        t_atom myList[4];
        atom_setsym(&myList[0], gensym((*it).first.c_str()));
        atom_setfloat(&myList[1], (*it).second->getValue());    // cur value
        atom_setfloat(&myList[2], (*it).second->getMinValue());
        atom_setfloat(&myList[3], (*it).second->getMaxValue());
        outlet_list(x->m_control_outlet, 0, 4, myList);
    }
}

/*--------------------------------------------------------------------------*/
void faust_dump_outputs(t_faust* x)
{
    // Output controllers
    for (mspUI::iterator it = x->m_dspUI->begin4(); it != x->m_dspUI->end4(); it++) {
        t_atom myList[4];
        atom_setsym(&myList[0], gensym((*it).first.c_str()));
        atom_setfloat(&myList[1], (*it).second->getValue());    // cur value
        atom_setfloat(&myList[2], (*it).second->getMinValue());
        atom_setfloat(&myList[3], (*it).second->getMaxValue());
        outlet_list(x->m_control_outlet, 0, 4, myList);
    }
}

/*--------------------------------------------------------------------------*/
// Dump controllers as list of [path, cur, min, max]
void faust_dump(t_faust* x, t_symbol* s, short ac, t_atom* av)
{
    faust_dump_inputs(x);
    faust_dump_outputs(x);
}

/*--------------------------------------------------------------------------*/
void faust_dblclick(t_faust* x, long inlet)
{
    x->m_dspUI->displayControls();
}

/*--------------------------------------------------------------------------*/
//11/13/2015 : faust_assist is actually called at each click in the patcher, so we now use 'faust_dblclick' to display the parameters...
void faust_assist(t_faust* x, void* b, long msg, long a, char* dst)
{
    if (msg == ASSIST_INLET) {
        if (a == 0) {
            if (x->m_dsp->getNumInputs() == 0) {
                snprintf(dst, 512, "(messages)");
            } else {
                snprintf(dst, 512, "(messages/signal) : Audio Input %ld", (a+1));
            }
        } else if (a < x->m_dsp->getNumInputs()) {
            snprintf(dst, 512, "(signal) : Audio Input %ld", (a+1));
        }
    } else if (msg == ASSIST_OUTLET) {
        if (a < x->m_dsp->getNumOutputs()) {
            snprintf(dst, 512, "(signal) : Audio Output %ld", (a+1));
        } else if (a == x->m_dsp->getNumOutputs()) {
            snprintf(dst, 512, "(list) : [path, cur|init, min, max]*");
        } else {
            snprintf(dst, 512, "(int) : raw MIDI bytes*");
        }
    }
}

/*--------------------------------------------------------------------------*/
void faust_mute(t_faust* obj, t_symbol* s, short ac, t_atom* at)
{
    if (atom_gettype(at) == A_LONG) {
        obj->m_mute = atom_getlong(at);
    }
}

/*--------------------------------------------------------------------------*/
void faust_free(t_faust* x)
{
    dsp_free((t_pxobject*)x);
    delete x->m_dsp;
    delete x->m_dspUI;
    delete x->m_savedUI;
    if (x->m_args) free(x->m_args);
    if (x->m_json) free(x->m_json);
    systhread_mutex_free(x->m_mutex);
#ifdef MIDICTRL
    // m_midiUI *must* be deleted before m_midiHandler
    delete x->m_midiUI;
    delete x->m_midiHandler;
#endif
#ifdef SOUNDFILE
    delete x->m_soundInterface;
#endif
#ifdef OSCCTRL
    delete x->m_oscInterface;
#endif
}

/*--------------------------------------------------------------------------*/
void faust_perform64(t_faust* x, t_object* dsp64, double** ins, long numins, double** outs, long numouts, long sampleframes, long flags, void* userparam)
{
    AVOIDDENORMALS;
    if (!x->m_mute && systhread_mutex_trylock(x->m_mutex) == MAX_ERR_NONE) {
        if (x->m_dsp) {
            if (x->m_dspUI->isMulti()) {
                x->m_dspUI->setMultiValues(reinterpret_cast<FAUSTFLOAT*>(ins[0]), sampleframes);
                x->m_dsp->compute(sampleframes, reinterpret_cast<FAUSTFLOAT**>(++ins), reinterpret_cast<FAUSTFLOAT**>(outs));
            } else {
                x->m_dsp->compute(sampleframes, reinterpret_cast<FAUSTFLOAT**>(ins), reinterpret_cast<FAUSTFLOAT**>(outs));
            }
        #ifdef OSCCTRL
            if (x->m_oscInterface) x->m_oscInterface->endBundle();
        #endif
            //faust_update_outputs(x);
            // Use the right outlet to output messages
            faust_dump_outputs(x);
        }
    #if defined(MIDICTRL) || defined(OSCCTRL)
        GUI::updateAllGuis();
    #endif
        systhread_mutex_unlock(x->m_mutex);
    } else {
        // Write null buffers to outs
        for (int i = 0; i < numouts; i++) {
             memset(outs[i], 0, sizeof(double) * sampleframes);
        }
    }
}

/*--------------------------------------------------------------------------*/
void faust_dsp64(t_faust* x, t_object* dsp64, short* count, double samplerate, long maxvectorsize, long flags)
{
    object_method(dsp64, gensym("dsp_add64"), x, faust_perform64, 0, NULL);
}

/*--------------------------------------------------------------------------*/
t_max_err faust_attr_set(t_faust* x, t_object* attr, long ac, t_atom* av)
{
    if (ac && av) {
        t_symbol* attrname = (t_symbol*)object_method(attr, gensym("getname"));
        // Redirect on the generic message handling method
        faust_anything(x, attrname, ac, av);
    }
    return MAX_ERR_NONE;
}

/*--------------------------------------------------------------------------*/
#ifdef _WIN32
extern "C" int main(void)
#else
void ext_main(void* r)
#endif
{
    string file_name = string(FAUST_FILE_NAME);
    // Remove ".dsp" ending
    string class_name = file_name.erase(file_name.size()-4) + "~";
    t_class* c = class_new(class_name.c_str(), (method)faust_new, (method)faust_free, sizeof(t_faust), 0L, A_GIMME, 0);
    
    class_addmethod(c, (method)faust_anything, "anything", A_GIMME, 0);
    class_addmethod(c, (method)faust_polyphony, "polyphony", A_GIMME, 0);
#ifdef OSCCTRL
    class_addmethod(c, (method)faust_osc, "osc", A_GIMME, 0);
#endif
    class_addmethod(c, (method)faust_init, "init", A_GIMME, 0);
    class_addmethod(c, (method)faust_dump, "dump", A_GIMME, 0);
#ifdef MIDICTRL
    class_addmethod(c, (method)faust_midievent, "midievent", A_GIMME, 0);
#endif
    class_addmethod(c, (method)faust_dsp64, "dsp64", A_CANT, 0);
    class_addmethod(c, (method)faust_dblclick, "dblclick", A_CANT, 0);
    class_addmethod(c, (method)faust_assist, "assist", A_CANT, 0);
    class_addmethod(c, (method)faust_mute, "mute", A_GIMME, 0);
    
    dsp* tmp_dsp = new bbdmi_multi_granulator14();
    mspUI tmp_UI;
    tmp_dsp->buildUserInterface(&tmp_UI);
    
    // Setup attributes
    if (sizeof(FAUSTFLOAT) == 4) {
        for (mspUI::iterator it = tmp_UI.begin1(); it != tmp_UI.end1(); it++) {
            CLASS_ATTR_FLOAT(c, (*it).first.c_str(), 0, t_faust, m_ob);
            CLASS_ATTR_ACCESSORS(c, (*it).first.c_str(), NULL, (method)faust_attr_set);
        }
    } else {
        for (mspUI::iterator it = tmp_UI.begin1(); it != tmp_UI.end1(); it++) {
            CLASS_ATTR_DOUBLE(c, (*it).first.c_str(), 0, t_faust, m_ob);
            CLASS_ATTR_ACCESSORS(c, (*it).first.c_str(), NULL, (method)faust_attr_set);
        }
    }
    
    class_dspinit(c);
    class_register(CLASS_BOX, c);
    faust_class = c;
#ifdef POST
    post((char*)"Faust DSP object v%s (sample = %s bits code = %s)", EXTERNAL_VERSION, ((sizeof(FAUSTFLOAT) == 4) ? "32" : "64"), getCodeSize());
    post((char*)"Copyright (c) 2012-2024 Grame");
#endif
    Max_Meta1 meta1;
    tmp_dsp->metadata(&meta1);
    if (meta1.fCount > 0) {
    #ifdef POST
        Max_Meta2 meta2;
        post("------------------------------");
        tmp_dsp->metadata(&meta2);
        post("------------------------------");
    #endif
    }
    delete(tmp_dsp);
#ifdef _WIN32
    return 0;
#endif
}

/******************* END max-msp64.cpp ****************/


#endif
