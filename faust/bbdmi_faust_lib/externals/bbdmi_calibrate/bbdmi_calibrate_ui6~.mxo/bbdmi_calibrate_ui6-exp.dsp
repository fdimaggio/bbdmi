declare version "2.72.14";
declare compile_options "-double -scal -e ./bbdmi_calibrate_ui6.dsp -o faust.Qmm/bbdmi_calibrate_ui6/bbdmi_calibrate_ui6-exp.dsp";
declare library_path0 "/Users/davidfierro/Desktop/bbdmiCompilationInProgress/bbdmi.lib";
declare library_path1 "/Applications/Faust-2.72.14/share/faust/basics.lib";
declare library_path2 "/Applications/Faust-2.72.14/share/faust/maths.lib";
declare library_path3 "/Applications/Faust-2.72.14/share/faust/signals.lib";
declare library_path4 "/Applications/Faust-2.72.14/share/faust/stdfaust.lib";
declare library_path5 "/Applications/Faust-2.72.14/share/faust/platform.lib";
declare library_path6 "/Applications/Faust-2.72.14/share/faust/filters.lib";
declare library_path7 "/Applications/Faust-2.72.14/share/faust/analyzers.lib";
declare analyzers_lib_ms_envelope_rect_author "Dario Sanfilippo and Julius O. Smith III";
declare analyzers_lib_ms_envelope_rect_copyright "Copyright (C) 2020 Dario Sanfilippo        <sanfilippo.dario@gmail.com> and         2003-2020 by Julius O. Smith III <jos@ccrma.stanford.edu>";
declare analyzers_lib_ms_envelope_rect_license "MIT-style STK-4.3 license";
declare analyzers_lib_name "Faust Analyzer Library";
declare analyzers_lib_rms_envelope_rect_author "Dario Sanfilippo and Julius O. Smith III";
declare analyzers_lib_rms_envelope_rect_copyright "Copyright (C) 2020 Dario Sanfilippo        <sanfilippo.dario@gmail.com> and         2003-2020 by Julius O. Smith III <jos@ccrma.stanford.edu>";
declare analyzers_lib_rms_envelope_rect_license "MIT-style STK-4.3 license";
declare analyzers_lib_version "1.2.0";
declare author "Alain Bonardi";
declare contributor "David Fierro";
declare contributor "Anne Sedes";
declare contributor "Atau Tanaka";
declare contributor "Stephen Whitmarsch";
declare contributor "Francesco Di Maggio";
declare basics_lib_name "Faust Basic Element Library";
declare basics_lib_sAndH_author "Romain Michon";
declare basics_lib_tabulateNd "Copyright (C) 2023 Bart Brouns <bart@magnetophon.nl>";
declare basics_lib_version "1.15.0";
declare bbdmi_lib_author "Alain Bonardi";
declare bbdmi_lib_copyright "2022-2025 BBDMI TEAM";
declare bbdmi_lib_licence "LGPLv3";
declare bbdmi_lib_name "BBDMI Faust Lib";
declare copyright "2022-2025 BBDMI TEAM";
declare filename "bbdmi_calibrate_ui6.dsp";
declare filters_lib_avg_rect_author "Dario Sanfilippo and Julius O. Smith III";
declare filters_lib_avg_rect_copyright "Copyright (C) 2020 Dario Sanfilippo        <sanfilippo.dario@gmail.com> and         2003-2020 by Julius O. Smith III <jos@ccrma.stanford.edu>";
declare filters_lib_avg_rect_license "MIT-style STK-4.3 license";
declare filters_lib_dcblocker_author "Julius O. Smith III";
declare filters_lib_dcblocker_copyright "Copyright (C) 2003-2019 by Julius O. Smith III <jos@ccrma.stanford.edu>";
declare filters_lib_dcblocker_license "MIT-style STK-4.3 license";
declare filters_lib_integrator_author "Julius O. Smith III";
declare filters_lib_integrator_copyright "Copyright (C) 2003-2019 by Julius O. Smith III <jos@ccrma.stanford.edu>";
declare filters_lib_integrator_license "MIT-style STK-4.3 license";
declare filters_lib_lowpass0_highpass1 "Copyright (C) 2003-2019 by Julius O. Smith III <jos@ccrma.stanford.edu>";
declare filters_lib_name "Faust Filters Library";
declare filters_lib_pole_author "Julius O. Smith III";
declare filters_lib_pole_copyright "Copyright (C) 2003-2019 by Julius O. Smith III <jos@ccrma.stanford.edu>";
declare filters_lib_pole_license "MIT-style STK-4.3 license";
declare filters_lib_version "1.3.0";
declare filters_lib_zero_author "Julius O. Smith III";
declare filters_lib_zero_copyright "Copyright (C) 2003-2019 by Julius O. Smith III <jos@ccrma.stanford.edu>";
declare filters_lib_zero_license "MIT-style STK-4.3 license";
declare maths_lib_author "GRAME";
declare maths_lib_copyright "GRAME";
declare maths_lib_license "LGPL with exception";
declare maths_lib_name "Faust Math Library";
declare maths_lib_version "2.8.0";
declare name "BBDMI Faust Lib";
declare platform_lib_name "Generic Platform Library";
declare platform_lib_version "1.3.0";
declare signals_lib_name "Faust Signal Routing Library";
declare signals_lib_version "1.5.0";
ID_0 = _, mem;
ID_1 = _, 1;
ID_2 = (ID_1 : *);
ID_3 = _, ID_2;
ID_4 = ID_3 : -;
ID_5 = ID_0 : ID_4;
ID_6 = _ <: ID_5;
ID_7 = _, 0.995;
ID_8 = (ID_7 : *);
ID_9 = + ~ ID_8;
ID_10 = ID_6 : ID_9;
ID_11 = ID_10 : \(x5).(x5,x5 : * : \(x6).(x6 : (+~_<:_,(_,(0,(hslider("v: bbdmi_calibrate 0/[4]rmsPeriod [unit:secs]", 0.1, 0.01, 5.0, 0.01),(1.92e+05,(1.0,fconstant(int fSamplingFreq, <math.h>) : max) : min) : * : rint) : max : int) : @):>-),(hslider("v: bbdmi_calibrate 0/[4]rmsPeriod [unit:secs]", 0.1, 0.01, 5.0, 0.01),(1.92e+05,(1.0,fconstant(int fSamplingFreq, <math.h>) : max) : min) : * : rint) : /) : sqrt);
ID_12 = _ : ID_11;
ID_13 = _, _;
ID_14 = (_ <: ID_13);
ID_15 = _, ID_14;
ID_16 = 1, _;
ID_17 = ID_16 : -;
ID_18 = abs : \(x7).(x7,(1.0,(((hslider("v: bbdmi_calibrate 0/[0]bufferTime [unit:secs]", 1e+01, 1.0, 1.2e+03, 1.0) : \(x14).(\(x15).(((1.0,(1,(44.1,(1.92e+05,(1.0,fconstant(int fSamplingFreq, <math.h>) : max) : min) : /) : -) : -),x14 : *),((1,(44.1,(1.92e+05,(1.0,fconstant(int fSamplingFreq, <math.h>) : max) : min) : /) : -),x15 : *) : +)~_) : abs),2.220446049250313e-16 : <),(-1.0,((((hslider("v: bbdmi_calibrate 0/[0]bufferTime [unit:secs]", 1e+01, 1.0, 1.2e+03, 1.0) : \(x16).(\(x17).(((1.0,(1,(44.1,(1.92e+05,(1.0,fconstant(int fSamplingFreq, <math.h>) : max) : min) : /) : -) : -),x16 : *),((1,(44.1,(1.92e+05,(1.0,fconstant(int fSamplingFreq, <math.h>) : max) : min) : /) : -),x17 : *) : +)~_) : abs),2.220446049250313e-16 : <),(hslider("v: bbdmi_calibrate 0/[0]bufferTime [unit:secs]", 1e+01, 1.0, 1.2e+03, 1.0) : \(x16).(\(x17).(((1.0,(1,(44.1,(1.92e+05,(1.0,fconstant(int fSamplingFreq, <math.h>) : max) : min) : /) : -) : -),x16 : *),((1,(44.1,(1.92e+05,(1.0,fconstant(int fSamplingFreq, <math.h>) : max) : min) : /) : -),x17 : *) : +)~_)),1.0 : select2),(1.92e+05,(1.0,fconstant(int fSamplingFreq, <math.h>) : max) : min : float) : *) : / : exp),0.0 : select2) : -) : * : (+ : x7,_ : max)~(_,(((hslider("v: bbdmi_calibrate 0/[0]bufferTime [unit:secs]", 1e+01, 1.0, 1.2e+03, 1.0) : \(x14).(\(x15).(((1.0,(1,(44.1,(1.92e+05,(1.0,fconstant(int fSamplingFreq, <math.h>) : max) : min) : /) : -) : -),x14 : *),((1,(44.1,(1.92e+05,(1.0,fconstant(int fSamplingFreq, <math.h>) : max) : min) : /) : -),x15 : *) : +)~_) : abs),2.220446049250313e-16 : <),(-1.0,((((hslider("v: bbdmi_calibrate 0/[0]bufferTime [unit:secs]", 1e+01, 1.0, 1.2e+03, 1.0) : \(x16).(\(x17).(((1.0,(1,(44.1,(1.92e+05,(1.0,fconstant(int fSamplingFreq, <math.h>) : max) : min) : /) : -) : -),x16 : *),((1,(44.1,(1.92e+05,(1.0,fconstant(int fSamplingFreq, <math.h>) : max) : min) : /) : -),x17 : *) : +)~_) : abs),2.220446049250313e-16 : <),(hslider("v: bbdmi_calibrate 0/[0]bufferTime [unit:secs]", 1e+01, 1.0, 1.2e+03, 1.0) : \(x16).(\(x17).(((1.0,(1,(44.1,(1.92e+05,(1.0,fconstant(int fSamplingFreq, <math.h>) : max) : min) : /) : -) : -),x16 : *),((1,(44.1,(1.92e+05,(1.0,fconstant(int fSamplingFreq, <math.h>) : max) : min) : /) : -),x17 : *) : +)~_)),1.0 : select2),(1.92e+05,(1.0,fconstant(int fSamplingFreq, <math.h>) : max) : min : float) : *) : / : exp),0.0 : select2) : *));
ID_19 = ID_18 : ID_17;
ID_20 = ID_17 : ID_19;
ID_21 = hslider("v: bbdmi_calibrate 0/[1]OnOff", 1.0, 0.0, 1.0, 1.0);
ID_22 = ID_21, ID_13;
ID_23 = (ID_22 : select2);
ID_24 = ID_23 ~ _;
ID_25 = (ID_20 : ID_24);
ID_26 = (ID_18 : ID_24);
ID_27 = ID_25, ID_26;
ID_28 = _, ID_27;
ID_29 = ID_14, _;
ID_30 = _, ID_29;
ID_31 = _, -;
ID_32 = _, ID_31;
ID_33 = _, -1;
ID_34 = (ID_33 : *);
ID_35 = _, ID_34;
ID_36 = _, ID_35;
ID_37 = hslider("v: bbdmi_calibrate 0/[2]minoffset", 0.1, 0.0, 0.5, 0.01);
ID_38 = _, ID_37;
ID_39 = ID_38 : *;
ID_40 = (_ : ID_39);
ID_41 = ID_40, _;
ID_42 = (_ <: ID_41);
ID_43 = _, ID_42;
ID_44 = _, ID_43;
ID_45 = +, _;
ID_46 = _, ID_45;
ID_47 = -, _;
ID_48 = hslider("v: bbdmi_calibrate 0/[3]maxoffset", 0.1, 0.0, 0.5, 0.01);
ID_49 = 1.001, ID_48;
ID_50 = (ID_49 : -);
ID_51 = ID_50, ID_37;
ID_52 = (ID_51 : -);
ID_53 = _, ID_52;
ID_54 = (ID_53 : *);
ID_55 = _, ID_54;
ID_56 = (ID_16 : /);
ID_57 = _, ID_56;
ID_58 = _, 0;
ID_59 = ID_1 : min;
ID_60 = max : ID_59;
ID_61 = ID_58 : ID_60;
ID_62 = * : ID_61;
ID_63 = ID_57 : ID_62;
ID_64 = ID_55 : ID_63;
ID_65 = ID_47 : ID_64;
ID_66 = ID_46 : ID_65;
ID_67 = ID_44 : ID_66;
ID_68 = ID_36 : ID_67;
ID_69 = ID_32 : ID_68;
ID_70 = ID_30 : ID_69;
ID_71 = ID_28 : ID_70;
ID_72 = ID_15 : ID_71;
ID_73 = (ID_12 <: ID_72);
ID_74 = ID_10 : \(x18).(x18,x18 : * : \(x19).(x19 : (+~_<:_,(_,(0,(hslider("v: bbdmi_calibrate 1/[4]rmsPeriod [unit:secs]", 0.1, 0.01, 5.0, 0.01),(1.92e+05,(1.0,fconstant(int fSamplingFreq, <math.h>) : max) : min) : * : rint) : max : int) : @):>-),(hslider("v: bbdmi_calibrate 1/[4]rmsPeriod [unit:secs]", 0.1, 0.01, 5.0, 0.01),(1.92e+05,(1.0,fconstant(int fSamplingFreq, <math.h>) : max) : min) : * : rint) : /) : sqrt);
ID_75 = _ : ID_74;
ID_76 = abs : \(x20).(x20,(1.0,(((hslider("v: bbdmi_calibrate 1/[0]bufferTime [unit:secs]", 1e+01, 1.0, 1.2e+03, 1.0) : \(x14).(\(x15).(((1.0,(1,(44.1,(1.92e+05,(1.0,fconstant(int fSamplingFreq, <math.h>) : max) : min) : /) : -) : -),x14 : *),((1,(44.1,(1.92e+05,(1.0,fconstant(int fSamplingFreq, <math.h>) : max) : min) : /) : -),x15 : *) : +)~_) : abs),2.220446049250313e-16 : <),(-1.0,((((hslider("v: bbdmi_calibrate 1/[0]bufferTime [unit:secs]", 1e+01, 1.0, 1.2e+03, 1.0) : \(x16).(\(x17).(((1.0,(1,(44.1,(1.92e+05,(1.0,fconstant(int fSamplingFreq, <math.h>) : max) : min) : /) : -) : -),x16 : *),((1,(44.1,(1.92e+05,(1.0,fconstant(int fSamplingFreq, <math.h>) : max) : min) : /) : -),x17 : *) : +)~_) : abs),2.220446049250313e-16 : <),(hslider("v: bbdmi_calibrate 1/[0]bufferTime [unit:secs]", 1e+01, 1.0, 1.2e+03, 1.0) : \(x16).(\(x17).(((1.0,(1,(44.1,(1.92e+05,(1.0,fconstant(int fSamplingFreq, <math.h>) : max) : min) : /) : -) : -),x16 : *),((1,(44.1,(1.92e+05,(1.0,fconstant(int fSamplingFreq, <math.h>) : max) : min) : /) : -),x17 : *) : +)~_)),1.0 : select2),(1.92e+05,(1.0,fconstant(int fSamplingFreq, <math.h>) : max) : min : float) : *) : / : exp),0.0 : select2) : -) : * : (+ : x20,_ : max)~(_,(((hslider("v: bbdmi_calibrate 1/[0]bufferTime [unit:secs]", 1e+01, 1.0, 1.2e+03, 1.0) : \(x14).(\(x15).(((1.0,(1,(44.1,(1.92e+05,(1.0,fconstant(int fSamplingFreq, <math.h>) : max) : min) : /) : -) : -),x14 : *),((1,(44.1,(1.92e+05,(1.0,fconstant(int fSamplingFreq, <math.h>) : max) : min) : /) : -),x15 : *) : +)~_) : abs),2.220446049250313e-16 : <),(-1.0,((((hslider("v: bbdmi_calibrate 1/[0]bufferTime [unit:secs]", 1e+01, 1.0, 1.2e+03, 1.0) : \(x16).(\(x17).(((1.0,(1,(44.1,(1.92e+05,(1.0,fconstant(int fSamplingFreq, <math.h>) : max) : min) : /) : -) : -),x16 : *),((1,(44.1,(1.92e+05,(1.0,fconstant(int fSamplingFreq, <math.h>) : max) : min) : /) : -),x17 : *) : +)~_) : abs),2.220446049250313e-16 : <),(hslider("v: bbdmi_calibrate 1/[0]bufferTime [unit:secs]", 1e+01, 1.0, 1.2e+03, 1.0) : \(x16).(\(x17).(((1.0,(1,(44.1,(1.92e+05,(1.0,fconstant(int fSamplingFreq, <math.h>) : max) : min) : /) : -) : -),x16 : *),((1,(44.1,(1.92e+05,(1.0,fconstant(int fSamplingFreq, <math.h>) : max) : min) : /) : -),x17 : *) : +)~_)),1.0 : select2),(1.92e+05,(1.0,fconstant(int fSamplingFreq, <math.h>) : max) : min : float) : *) : / : exp),0.0 : select2) : *));
ID_77 = ID_76 : ID_17;
ID_78 = ID_17 : ID_77;
ID_79 = hslider("v: bbdmi_calibrate 1/[1]OnOff", 1.0, 0.0, 1.0, 1.0);
ID_80 = ID_79, ID_13;
ID_81 = (ID_80 : select2);
ID_82 = ID_81 ~ _;
ID_83 = (ID_78 : ID_82);
ID_84 = (ID_76 : ID_82);
ID_85 = ID_83, ID_84;
ID_86 = _, ID_85;
ID_87 = hslider("v: bbdmi_calibrate 1/[2]minoffset", 0.1, 0.0, 0.5, 0.01);
ID_88 = _, ID_87;
ID_89 = ID_88 : *;
ID_90 = (_ : ID_89);
ID_91 = ID_90, _;
ID_92 = (_ <: ID_91);
ID_93 = _, ID_92;
ID_94 = _, ID_93;
ID_95 = hslider("v: bbdmi_calibrate 1/[3]maxoffset", 0.1, 0.0, 0.5, 0.01);
ID_96 = 1.001, ID_95;
ID_97 = (ID_96 : -);
ID_98 = ID_97, ID_87;
ID_99 = (ID_98 : -);
ID_100 = _, ID_99;
ID_101 = (ID_100 : *);
ID_102 = _, ID_101;
ID_103 = ID_102 : ID_63;
ID_104 = ID_47 : ID_103;
ID_105 = ID_46 : ID_104;
ID_106 = ID_94 : ID_105;
ID_107 = ID_36 : ID_106;
ID_108 = ID_32 : ID_107;
ID_109 = ID_30 : ID_108;
ID_110 = ID_86 : ID_109;
ID_111 = ID_15 : ID_110;
ID_112 = (ID_75 <: ID_111);
ID_113 = ID_10 : \(x21).(x21,x21 : * : \(x22).(x22 : (+~_<:_,(_,(0,(hslider("v: bbdmi_calibrate 2/[4]rmsPeriod [unit:secs]", 0.1, 0.01, 5.0, 0.01),(1.92e+05,(1.0,fconstant(int fSamplingFreq, <math.h>) : max) : min) : * : rint) : max : int) : @):>-),(hslider("v: bbdmi_calibrate 2/[4]rmsPeriod [unit:secs]", 0.1, 0.01, 5.0, 0.01),(1.92e+05,(1.0,fconstant(int fSamplingFreq, <math.h>) : max) : min) : * : rint) : /) : sqrt);
ID_114 = _ : ID_113;
ID_115 = abs : \(x23).(x23,(1.0,(((hslider("v: bbdmi_calibrate 2/[0]bufferTime [unit:secs]", 1e+01, 1.0, 1.2e+03, 1.0) : \(x14).(\(x15).(((1.0,(1,(44.1,(1.92e+05,(1.0,fconstant(int fSamplingFreq, <math.h>) : max) : min) : /) : -) : -),x14 : *),((1,(44.1,(1.92e+05,(1.0,fconstant(int fSamplingFreq, <math.h>) : max) : min) : /) : -),x15 : *) : +)~_) : abs),2.220446049250313e-16 : <),(-1.0,((((hslider("v: bbdmi_calibrate 2/[0]bufferTime [unit:secs]", 1e+01, 1.0, 1.2e+03, 1.0) : \(x16).(\(x17).(((1.0,(1,(44.1,(1.92e+05,(1.0,fconstant(int fSamplingFreq, <math.h>) : max) : min) : /) : -) : -),x16 : *),((1,(44.1,(1.92e+05,(1.0,fconstant(int fSamplingFreq, <math.h>) : max) : min) : /) : -),x17 : *) : +)~_) : abs),2.220446049250313e-16 : <),(hslider("v: bbdmi_calibrate 2/[0]bufferTime [unit:secs]", 1e+01, 1.0, 1.2e+03, 1.0) : \(x16).(\(x17).(((1.0,(1,(44.1,(1.92e+05,(1.0,fconstant(int fSamplingFreq, <math.h>) : max) : min) : /) : -) : -),x16 : *),((1,(44.1,(1.92e+05,(1.0,fconstant(int fSamplingFreq, <math.h>) : max) : min) : /) : -),x17 : *) : +)~_)),1.0 : select2),(1.92e+05,(1.0,fconstant(int fSamplingFreq, <math.h>) : max) : min : float) : *) : / : exp),0.0 : select2) : -) : * : (+ : x23,_ : max)~(_,(((hslider("v: bbdmi_calibrate 2/[0]bufferTime [unit:secs]", 1e+01, 1.0, 1.2e+03, 1.0) : \(x14).(\(x15).(((1.0,(1,(44.1,(1.92e+05,(1.0,fconstant(int fSamplingFreq, <math.h>) : max) : min) : /) : -) : -),x14 : *),((1,(44.1,(1.92e+05,(1.0,fconstant(int fSamplingFreq, <math.h>) : max) : min) : /) : -),x15 : *) : +)~_) : abs),2.220446049250313e-16 : <),(-1.0,((((hslider("v: bbdmi_calibrate 2/[0]bufferTime [unit:secs]", 1e+01, 1.0, 1.2e+03, 1.0) : \(x16).(\(x17).(((1.0,(1,(44.1,(1.92e+05,(1.0,fconstant(int fSamplingFreq, <math.h>) : max) : min) : /) : -) : -),x16 : *),((1,(44.1,(1.92e+05,(1.0,fconstant(int fSamplingFreq, <math.h>) : max) : min) : /) : -),x17 : *) : +)~_) : abs),2.220446049250313e-16 : <),(hslider("v: bbdmi_calibrate 2/[0]bufferTime [unit:secs]", 1e+01, 1.0, 1.2e+03, 1.0) : \(x16).(\(x17).(((1.0,(1,(44.1,(1.92e+05,(1.0,fconstant(int fSamplingFreq, <math.h>) : max) : min) : /) : -) : -),x16 : *),((1,(44.1,(1.92e+05,(1.0,fconstant(int fSamplingFreq, <math.h>) : max) : min) : /) : -),x17 : *) : +)~_)),1.0 : select2),(1.92e+05,(1.0,fconstant(int fSamplingFreq, <math.h>) : max) : min : float) : *) : / : exp),0.0 : select2) : *));
ID_116 = ID_115 : ID_17;
ID_117 = ID_17 : ID_116;
ID_118 = hslider("v: bbdmi_calibrate 2/[1]OnOff", 1.0, 0.0, 1.0, 1.0);
ID_119 = ID_118, ID_13;
ID_120 = (ID_119 : select2);
ID_121 = ID_120 ~ _;
ID_122 = (ID_117 : ID_121);
ID_123 = (ID_115 : ID_121);
ID_124 = ID_122, ID_123;
ID_125 = _, ID_124;
ID_126 = hslider("v: bbdmi_calibrate 2/[2]minoffset", 0.1, 0.0, 0.5, 0.01);
ID_127 = _, ID_126;
ID_128 = ID_127 : *;
ID_129 = (_ : ID_128);
ID_130 = ID_129, _;
ID_131 = (_ <: ID_130);
ID_132 = _, ID_131;
ID_133 = _, ID_132;
ID_134 = hslider("v: bbdmi_calibrate 2/[3]maxoffset", 0.1, 0.0, 0.5, 0.01);
ID_135 = 1.001, ID_134;
ID_136 = (ID_135 : -);
ID_137 = ID_136, ID_126;
ID_138 = (ID_137 : -);
ID_139 = _, ID_138;
ID_140 = (ID_139 : *);
ID_141 = _, ID_140;
ID_142 = ID_141 : ID_63;
ID_143 = ID_47 : ID_142;
ID_144 = ID_46 : ID_143;
ID_145 = ID_133 : ID_144;
ID_146 = ID_36 : ID_145;
ID_147 = ID_32 : ID_146;
ID_148 = ID_30 : ID_147;
ID_149 = ID_125 : ID_148;
ID_150 = ID_15 : ID_149;
ID_151 = (ID_114 <: ID_150);
ID_152 = ID_10 : \(x24).(x24,x24 : * : \(x25).(x25 : (+~_<:_,(_,(0,(hslider("v: bbdmi_calibrate 3/[4]rmsPeriod [unit:secs]", 0.1, 0.01, 5.0, 0.01),(1.92e+05,(1.0,fconstant(int fSamplingFreq, <math.h>) : max) : min) : * : rint) : max : int) : @):>-),(hslider("v: bbdmi_calibrate 3/[4]rmsPeriod [unit:secs]", 0.1, 0.01, 5.0, 0.01),(1.92e+05,(1.0,fconstant(int fSamplingFreq, <math.h>) : max) : min) : * : rint) : /) : sqrt);
ID_153 = _ : ID_152;
ID_154 = abs : \(x26).(x26,(1.0,(((hslider("v: bbdmi_calibrate 3/[0]bufferTime [unit:secs]", 1e+01, 1.0, 1.2e+03, 1.0) : \(x14).(\(x15).(((1.0,(1,(44.1,(1.92e+05,(1.0,fconstant(int fSamplingFreq, <math.h>) : max) : min) : /) : -) : -),x14 : *),((1,(44.1,(1.92e+05,(1.0,fconstant(int fSamplingFreq, <math.h>) : max) : min) : /) : -),x15 : *) : +)~_) : abs),2.220446049250313e-16 : <),(-1.0,((((hslider("v: bbdmi_calibrate 3/[0]bufferTime [unit:secs]", 1e+01, 1.0, 1.2e+03, 1.0) : \(x16).(\(x17).(((1.0,(1,(44.1,(1.92e+05,(1.0,fconstant(int fSamplingFreq, <math.h>) : max) : min) : /) : -) : -),x16 : *),((1,(44.1,(1.92e+05,(1.0,fconstant(int fSamplingFreq, <math.h>) : max) : min) : /) : -),x17 : *) : +)~_) : abs),2.220446049250313e-16 : <),(hslider("v: bbdmi_calibrate 3/[0]bufferTime [unit:secs]", 1e+01, 1.0, 1.2e+03, 1.0) : \(x16).(\(x17).(((1.0,(1,(44.1,(1.92e+05,(1.0,fconstant(int fSamplingFreq, <math.h>) : max) : min) : /) : -) : -),x16 : *),((1,(44.1,(1.92e+05,(1.0,fconstant(int fSamplingFreq, <math.h>) : max) : min) : /) : -),x17 : *) : +)~_)),1.0 : select2),(1.92e+05,(1.0,fconstant(int fSamplingFreq, <math.h>) : max) : min : float) : *) : / : exp),0.0 : select2) : -) : * : (+ : x26,_ : max)~(_,(((hslider("v: bbdmi_calibrate 3/[0]bufferTime [unit:secs]", 1e+01, 1.0, 1.2e+03, 1.0) : \(x14).(\(x15).(((1.0,(1,(44.1,(1.92e+05,(1.0,fconstant(int fSamplingFreq, <math.h>) : max) : min) : /) : -) : -),x14 : *),((1,(44.1,(1.92e+05,(1.0,fconstant(int fSamplingFreq, <math.h>) : max) : min) : /) : -),x15 : *) : +)~_) : abs),2.220446049250313e-16 : <),(-1.0,((((hslider("v: bbdmi_calibrate 3/[0]bufferTime [unit:secs]", 1e+01, 1.0, 1.2e+03, 1.0) : \(x16).(\(x17).(((1.0,(1,(44.1,(1.92e+05,(1.0,fconstant(int fSamplingFreq, <math.h>) : max) : min) : /) : -) : -),x16 : *),((1,(44.1,(1.92e+05,(1.0,fconstant(int fSamplingFreq, <math.h>) : max) : min) : /) : -),x17 : *) : +)~_) : abs),2.220446049250313e-16 : <),(hslider("v: bbdmi_calibrate 3/[0]bufferTime [unit:secs]", 1e+01, 1.0, 1.2e+03, 1.0) : \(x16).(\(x17).(((1.0,(1,(44.1,(1.92e+05,(1.0,fconstant(int fSamplingFreq, <math.h>) : max) : min) : /) : -) : -),x16 : *),((1,(44.1,(1.92e+05,(1.0,fconstant(int fSamplingFreq, <math.h>) : max) : min) : /) : -),x17 : *) : +)~_)),1.0 : select2),(1.92e+05,(1.0,fconstant(int fSamplingFreq, <math.h>) : max) : min : float) : *) : / : exp),0.0 : select2) : *));
ID_155 = ID_154 : ID_17;
ID_156 = ID_17 : ID_155;
ID_157 = hslider("v: bbdmi_calibrate 3/[1]OnOff", 1.0, 0.0, 1.0, 1.0);
ID_158 = ID_157, ID_13;
ID_159 = (ID_158 : select2);
ID_160 = ID_159 ~ _;
ID_161 = (ID_156 : ID_160);
ID_162 = (ID_154 : ID_160);
ID_163 = ID_161, ID_162;
ID_164 = _, ID_163;
ID_165 = hslider("v: bbdmi_calibrate 3/[2]minoffset", 0.1, 0.0, 0.5, 0.01);
ID_166 = _, ID_165;
ID_167 = ID_166 : *;
ID_168 = (_ : ID_167);
ID_169 = ID_168, _;
ID_170 = (_ <: ID_169);
ID_171 = _, ID_170;
ID_172 = _, ID_171;
ID_173 = hslider("v: bbdmi_calibrate 3/[3]maxoffset", 0.1, 0.0, 0.5, 0.01);
ID_174 = 1.001, ID_173;
ID_175 = (ID_174 : -);
ID_176 = ID_175, ID_165;
ID_177 = (ID_176 : -);
ID_178 = _, ID_177;
ID_179 = (ID_178 : *);
ID_180 = _, ID_179;
ID_181 = ID_180 : ID_63;
ID_182 = ID_47 : ID_181;
ID_183 = ID_46 : ID_182;
ID_184 = ID_172 : ID_183;
ID_185 = ID_36 : ID_184;
ID_186 = ID_32 : ID_185;
ID_187 = ID_30 : ID_186;
ID_188 = ID_164 : ID_187;
ID_189 = ID_15 : ID_188;
ID_190 = (ID_153 <: ID_189);
ID_191 = ID_10 : \(x27).(x27,x27 : * : \(x28).(x28 : (+~_<:_,(_,(0,(hslider("v: bbdmi_calibrate 4/[4]rmsPeriod [unit:secs]", 0.1, 0.01, 5.0, 0.01),(1.92e+05,(1.0,fconstant(int fSamplingFreq, <math.h>) : max) : min) : * : rint) : max : int) : @):>-),(hslider("v: bbdmi_calibrate 4/[4]rmsPeriod [unit:secs]", 0.1, 0.01, 5.0, 0.01),(1.92e+05,(1.0,fconstant(int fSamplingFreq, <math.h>) : max) : min) : * : rint) : /) : sqrt);
ID_192 = _ : ID_191;
ID_193 = abs : \(x29).(x29,(1.0,(((hslider("v: bbdmi_calibrate 4/[0]bufferTime [unit:secs]", 1e+01, 1.0, 1.2e+03, 1.0) : \(x14).(\(x15).(((1.0,(1,(44.1,(1.92e+05,(1.0,fconstant(int fSamplingFreq, <math.h>) : max) : min) : /) : -) : -),x14 : *),((1,(44.1,(1.92e+05,(1.0,fconstant(int fSamplingFreq, <math.h>) : max) : min) : /) : -),x15 : *) : +)~_) : abs),2.220446049250313e-16 : <),(-1.0,((((hslider("v: bbdmi_calibrate 4/[0]bufferTime [unit:secs]", 1e+01, 1.0, 1.2e+03, 1.0) : \(x16).(\(x17).(((1.0,(1,(44.1,(1.92e+05,(1.0,fconstant(int fSamplingFreq, <math.h>) : max) : min) : /) : -) : -),x16 : *),((1,(44.1,(1.92e+05,(1.0,fconstant(int fSamplingFreq, <math.h>) : max) : min) : /) : -),x17 : *) : +)~_) : abs),2.220446049250313e-16 : <),(hslider("v: bbdmi_calibrate 4/[0]bufferTime [unit:secs]", 1e+01, 1.0, 1.2e+03, 1.0) : \(x16).(\(x17).(((1.0,(1,(44.1,(1.92e+05,(1.0,fconstant(int fSamplingFreq, <math.h>) : max) : min) : /) : -) : -),x16 : *),((1,(44.1,(1.92e+05,(1.0,fconstant(int fSamplingFreq, <math.h>) : max) : min) : /) : -),x17 : *) : +)~_)),1.0 : select2),(1.92e+05,(1.0,fconstant(int fSamplingFreq, <math.h>) : max) : min : float) : *) : / : exp),0.0 : select2) : -) : * : (+ : x29,_ : max)~(_,(((hslider("v: bbdmi_calibrate 4/[0]bufferTime [unit:secs]", 1e+01, 1.0, 1.2e+03, 1.0) : \(x14).(\(x15).(((1.0,(1,(44.1,(1.92e+05,(1.0,fconstant(int fSamplingFreq, <math.h>) : max) : min) : /) : -) : -),x14 : *),((1,(44.1,(1.92e+05,(1.0,fconstant(int fSamplingFreq, <math.h>) : max) : min) : /) : -),x15 : *) : +)~_) : abs),2.220446049250313e-16 : <),(-1.0,((((hslider("v: bbdmi_calibrate 4/[0]bufferTime [unit:secs]", 1e+01, 1.0, 1.2e+03, 1.0) : \(x16).(\(x17).(((1.0,(1,(44.1,(1.92e+05,(1.0,fconstant(int fSamplingFreq, <math.h>) : max) : min) : /) : -) : -),x16 : *),((1,(44.1,(1.92e+05,(1.0,fconstant(int fSamplingFreq, <math.h>) : max) : min) : /) : -),x17 : *) : +)~_) : abs),2.220446049250313e-16 : <),(hslider("v: bbdmi_calibrate 4/[0]bufferTime [unit:secs]", 1e+01, 1.0, 1.2e+03, 1.0) : \(x16).(\(x17).(((1.0,(1,(44.1,(1.92e+05,(1.0,fconstant(int fSamplingFreq, <math.h>) : max) : min) : /) : -) : -),x16 : *),((1,(44.1,(1.92e+05,(1.0,fconstant(int fSamplingFreq, <math.h>) : max) : min) : /) : -),x17 : *) : +)~_)),1.0 : select2),(1.92e+05,(1.0,fconstant(int fSamplingFreq, <math.h>) : max) : min : float) : *) : / : exp),0.0 : select2) : *));
ID_194 = ID_193 : ID_17;
ID_195 = ID_17 : ID_194;
ID_196 = hslider("v: bbdmi_calibrate 4/[1]OnOff", 1.0, 0.0, 1.0, 1.0);
ID_197 = ID_196, ID_13;
ID_198 = (ID_197 : select2);
ID_199 = ID_198 ~ _;
ID_200 = (ID_195 : ID_199);
ID_201 = (ID_193 : ID_199);
ID_202 = ID_200, ID_201;
ID_203 = _, ID_202;
ID_204 = hslider("v: bbdmi_calibrate 4/[2]minoffset", 0.1, 0.0, 0.5, 0.01);
ID_205 = _, ID_204;
ID_206 = ID_205 : *;
ID_207 = (_ : ID_206);
ID_208 = ID_207, _;
ID_209 = (_ <: ID_208);
ID_210 = _, ID_209;
ID_211 = _, ID_210;
ID_212 = hslider("v: bbdmi_calibrate 4/[3]maxoffset", 0.1, 0.0, 0.5, 0.01);
ID_213 = 1.001, ID_212;
ID_214 = (ID_213 : -);
ID_215 = ID_214, ID_204;
ID_216 = (ID_215 : -);
ID_217 = _, ID_216;
ID_218 = (ID_217 : *);
ID_219 = _, ID_218;
ID_220 = ID_219 : ID_63;
ID_221 = ID_47 : ID_220;
ID_222 = ID_46 : ID_221;
ID_223 = ID_211 : ID_222;
ID_224 = ID_36 : ID_223;
ID_225 = ID_32 : ID_224;
ID_226 = ID_30 : ID_225;
ID_227 = ID_203 : ID_226;
ID_228 = ID_15 : ID_227;
ID_229 = (ID_192 <: ID_228);
ID_230 = ID_10 : \(x30).(x30,x30 : * : \(x31).(x31 : (+~_<:_,(_,(0,(hslider("v: bbdmi_calibrate 5/[4]rmsPeriod [unit:secs]", 0.1, 0.01, 5.0, 0.01),(1.92e+05,(1.0,fconstant(int fSamplingFreq, <math.h>) : max) : min) : * : rint) : max : int) : @):>-),(hslider("v: bbdmi_calibrate 5/[4]rmsPeriod [unit:secs]", 0.1, 0.01, 5.0, 0.01),(1.92e+05,(1.0,fconstant(int fSamplingFreq, <math.h>) : max) : min) : * : rint) : /) : sqrt);
ID_231 = _ : ID_230;
ID_232 = abs : \(x32).(x32,(1.0,(((hslider("v: bbdmi_calibrate 5/[0]bufferTime [unit:secs]", 1e+01, 1.0, 1.2e+03, 1.0) : \(x14).(\(x15).(((1.0,(1,(44.1,(1.92e+05,(1.0,fconstant(int fSamplingFreq, <math.h>) : max) : min) : /) : -) : -),x14 : *),((1,(44.1,(1.92e+05,(1.0,fconstant(int fSamplingFreq, <math.h>) : max) : min) : /) : -),x15 : *) : +)~_) : abs),2.220446049250313e-16 : <),(-1.0,((((hslider("v: bbdmi_calibrate 5/[0]bufferTime [unit:secs]", 1e+01, 1.0, 1.2e+03, 1.0) : \(x16).(\(x17).(((1.0,(1,(44.1,(1.92e+05,(1.0,fconstant(int fSamplingFreq, <math.h>) : max) : min) : /) : -) : -),x16 : *),((1,(44.1,(1.92e+05,(1.0,fconstant(int fSamplingFreq, <math.h>) : max) : min) : /) : -),x17 : *) : +)~_) : abs),2.220446049250313e-16 : <),(hslider("v: bbdmi_calibrate 5/[0]bufferTime [unit:secs]", 1e+01, 1.0, 1.2e+03, 1.0) : \(x16).(\(x17).(((1.0,(1,(44.1,(1.92e+05,(1.0,fconstant(int fSamplingFreq, <math.h>) : max) : min) : /) : -) : -),x16 : *),((1,(44.1,(1.92e+05,(1.0,fconstant(int fSamplingFreq, <math.h>) : max) : min) : /) : -),x17 : *) : +)~_)),1.0 : select2),(1.92e+05,(1.0,fconstant(int fSamplingFreq, <math.h>) : max) : min : float) : *) : / : exp),0.0 : select2) : -) : * : (+ : x32,_ : max)~(_,(((hslider("v: bbdmi_calibrate 5/[0]bufferTime [unit:secs]", 1e+01, 1.0, 1.2e+03, 1.0) : \(x14).(\(x15).(((1.0,(1,(44.1,(1.92e+05,(1.0,fconstant(int fSamplingFreq, <math.h>) : max) : min) : /) : -) : -),x14 : *),((1,(44.1,(1.92e+05,(1.0,fconstant(int fSamplingFreq, <math.h>) : max) : min) : /) : -),x15 : *) : +)~_) : abs),2.220446049250313e-16 : <),(-1.0,((((hslider("v: bbdmi_calibrate 5/[0]bufferTime [unit:secs]", 1e+01, 1.0, 1.2e+03, 1.0) : \(x16).(\(x17).(((1.0,(1,(44.1,(1.92e+05,(1.0,fconstant(int fSamplingFreq, <math.h>) : max) : min) : /) : -) : -),x16 : *),((1,(44.1,(1.92e+05,(1.0,fconstant(int fSamplingFreq, <math.h>) : max) : min) : /) : -),x17 : *) : +)~_) : abs),2.220446049250313e-16 : <),(hslider("v: bbdmi_calibrate 5/[0]bufferTime [unit:secs]", 1e+01, 1.0, 1.2e+03, 1.0) : \(x16).(\(x17).(((1.0,(1,(44.1,(1.92e+05,(1.0,fconstant(int fSamplingFreq, <math.h>) : max) : min) : /) : -) : -),x16 : *),((1,(44.1,(1.92e+05,(1.0,fconstant(int fSamplingFreq, <math.h>) : max) : min) : /) : -),x17 : *) : +)~_)),1.0 : select2),(1.92e+05,(1.0,fconstant(int fSamplingFreq, <math.h>) : max) : min : float) : *) : / : exp),0.0 : select2) : *));
ID_233 = ID_232 : ID_17;
ID_234 = ID_17 : ID_233;
ID_235 = hslider("v: bbdmi_calibrate 5/[1]OnOff", 1.0, 0.0, 1.0, 1.0);
ID_236 = ID_235, ID_13;
ID_237 = (ID_236 : select2);
ID_238 = ID_237 ~ _;
ID_239 = (ID_234 : ID_238);
ID_240 = (ID_232 : ID_238);
ID_241 = ID_239, ID_240;
ID_242 = _, ID_241;
ID_243 = hslider("v: bbdmi_calibrate 5/[2]minoffset", 0.1, 0.0, 0.5, 0.01);
ID_244 = _, ID_243;
ID_245 = ID_244 : *;
ID_246 = (_ : ID_245);
ID_247 = ID_246, _;
ID_248 = (_ <: ID_247);
ID_249 = _, ID_248;
ID_250 = _, ID_249;
ID_251 = hslider("v: bbdmi_calibrate 5/[3]maxoffset", 0.1, 0.0, 0.5, 0.01);
ID_252 = 1.001, ID_251;
ID_253 = (ID_252 : -);
ID_254 = ID_253, ID_243;
ID_255 = (ID_254 : -);
ID_256 = _, ID_255;
ID_257 = (ID_256 : *);
ID_258 = _, ID_257;
ID_259 = ID_258 : ID_63;
ID_260 = ID_47 : ID_259;
ID_261 = ID_46 : ID_260;
ID_262 = ID_250 : ID_261;
ID_263 = ID_36 : ID_262;
ID_264 = ID_32 : ID_263;
ID_265 = ID_30 : ID_264;
ID_266 = ID_242 : ID_265;
ID_267 = ID_15 : ID_266;
ID_268 = (ID_231 <: ID_267);
ID_269 = ID_229, ID_268;
ID_270 = ID_190, ID_269;
ID_271 = ID_151, ID_270;
ID_272 = ID_112, ID_271;
ID_273 = ID_73, ID_272;
ID_274 = hgroup("Calibrate", ID_273);
process = ID_274;
