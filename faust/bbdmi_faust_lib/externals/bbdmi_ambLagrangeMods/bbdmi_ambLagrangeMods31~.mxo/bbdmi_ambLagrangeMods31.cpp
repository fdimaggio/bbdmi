/* ------------------------------------------------------------
author: "Alain Bonardi", "David Fierro", "Anne Sedes", "Atau Tanaka", "Stephen Whitmarsch", "Francesco Di Maggio"
copyright: "2022-2025 BBDMI TEAM"
name: "BBDMI Faust Lib", "bbdmi_ambLagrangeMods31"
Code generated with Faust 2.72.14 (https://faust.grame.fr)
Compilation options: -a /Applications/Faust-2.72.14/share/faust/max-msp/max-msp64.cpp -lang cpp -i -ct 1 -cn bbdmi_ambLagrangeMods31 -es 1 -mcd 16 -mdd 1024 -mdy 33 -uim -double -ftz 0
------------------------------------------------------------ */

#ifndef  __bbdmi_ambLagrangeMods31_H__
#define  __bbdmi_ambLagrangeMods31_H__

/************************************************************************
 
 IMPORTANT NOTE : this file contains two clearly delimited sections :
 the ARCHITECTURE section (in two parts) and the USER section. Each section
 is governed by its own copyright and license. Please check individually
 each section for license and copyright information.
 *************************************************************************/

/******************* BEGIN max-msp64.cpp ****************/
/************************************************************************
 FAUST Architecture File
 Copyright (C) 2004-2020 GRAME, Centre National de Creation Musicale
 ---------------------------------------------------------------------
 This Architecture section is free software; you can redistribute it
 and/or modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either version 3
 of the License, or (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public License
 along with this program; If not, see <http://www.gnu.org/licenses/>.
 
 EXCEPTION : As a special exception, you may create a larger work
 that contains this FAUST architecture section and distribute
 that work under terms of your choice, so long as this FAUST
 architecture section is not modified.
 
 MAX MSP SDK : in order to compile a MaxMSP external with this
 architecture file you will need the official MaxMSP SDK from
 cycling'74. Please check the corresponding license.
 
 ************************************************************************
 ************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <limits.h>
#include <errno.h>
#include <time.h>
#include <unistd.h>
#include <fcntl.h>
#include <assert.h>
#include <string>
#include <vector>
#include <map>
#include <iostream>
#include <fstream>
#include <sstream>

#ifdef __APPLE__
#include <Carbon/Carbon.h>
#include <unistd.h>
#endif

#ifdef WIN32
#ifndef NAN
    static const unsigned long __nan[2] = {0xffffffff, 0x7fffffff};
    #define NAN (*(const float *) __nan)
#endif
#endif

// FAUSTFLOAT is setup by faust2max6

#ifndef DOWN_SAMPLING
#define DOWN_SAMPLING 0
#endif
#ifndef UP_SAMPLING
#define UP_SAMPLING 0
#endif
#ifndef FILTER_TYPE
#define FILTER_TYPE 0
#endif

/************************** BEGIN UI.h *****************************
 FAUST Architecture File
 Copyright (C) 2003-2022 GRAME, Centre National de Creation Musicale
 ---------------------------------------------------------------------
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation; either version 2.1 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 
 EXCEPTION : As a special exception, you may create a larger work
 that contains this FAUST architecture section and distribute
 that work under terms of your choice, so long as this FAUST
 architecture section is not modified.
 ********************************************************************/

#ifndef __UI_H__
#define __UI_H__

/************************************************************************
 FAUST Architecture File
 Copyright (C) 2003-2022 GRAME, Centre National de Creation Musicale
 ---------------------------------------------------------------------
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation; either version 2.1 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 
 EXCEPTION : As a special exception, you may create a larger work
 that contains this FAUST architecture section and distribute
 that work under terms of your choice, so long as this FAUST
 architecture section is not modified.
 ***************************************************************************/

#ifndef __export__
#define __export__

// Version as a global string
#define FAUSTVERSION "2.72.14"

// Version as separated [major,minor,patch] values
#define FAUSTMAJORVERSION 2
#define FAUSTMINORVERSION 72
#define FAUSTPATCHVERSION 14

// Use FAUST_API for code that is part of the external API but is also compiled in faust and libfaust
// Use LIBFAUST_API for code that is compiled in faust and libfaust

#ifdef _WIN32
    #pragma warning (disable: 4251)
    #ifdef FAUST_EXE
        #define FAUST_API
        #define LIBFAUST_API
    #elif FAUST_LIB
        #define FAUST_API __declspec(dllexport)
        #define LIBFAUST_API __declspec(dllexport)
    #else
        #define FAUST_API
        #define LIBFAUST_API 
    #endif
#else
    #ifdef FAUST_EXE
        #define FAUST_API
        #define LIBFAUST_API
    #else
        #define FAUST_API __attribute__((visibility("default")))
        #define LIBFAUST_API __attribute__((visibility("default")))
    #endif
#endif

#endif

#ifndef FAUSTFLOAT
#define FAUSTFLOAT float
#endif

/*******************************************************************************
 * UI : Faust DSP User Interface
 * User Interface as expected by the buildUserInterface() method of a DSP.
 * This abstract class contains only the method that the Faust compiler can
 * generate to describe a DSP user interface.
 ******************************************************************************/

struct Soundfile;

template <typename REAL>
struct FAUST_API UIReal {
    
    UIReal() {}
    virtual ~UIReal() {}
    
    // -- widget's layouts
    
    virtual void openTabBox(const char* label) = 0;
    virtual void openHorizontalBox(const char* label) = 0;
    virtual void openVerticalBox(const char* label) = 0;
    virtual void closeBox() = 0;
    
    // -- active widgets
    
    virtual void addButton(const char* label, REAL* zone) = 0;
    virtual void addCheckButton(const char* label, REAL* zone) = 0;
    virtual void addVerticalSlider(const char* label, REAL* zone, REAL init, REAL min, REAL max, REAL step) = 0;
    virtual void addHorizontalSlider(const char* label, REAL* zone, REAL init, REAL min, REAL max, REAL step) = 0;
    virtual void addNumEntry(const char* label, REAL* zone, REAL init, REAL min, REAL max, REAL step) = 0;
    
    // -- passive widgets
    
    virtual void addHorizontalBargraph(const char* label, REAL* zone, REAL min, REAL max) = 0;
    virtual void addVerticalBargraph(const char* label, REAL* zone, REAL min, REAL max) = 0;
    
    // -- soundfiles
    
    virtual void addSoundfile(const char* label, const char* filename, Soundfile** sf_zone) = 0;
    
    // -- metadata declarations
    
    virtual void declare(REAL* /*zone*/, const char* /*key*/, const char* /*val*/) {}

    // To be used by LLVM client
    virtual int sizeOfFAUSTFLOAT() { return sizeof(FAUSTFLOAT); }
};

struct FAUST_API UI : public UIReal<FAUSTFLOAT> {
    UI() {}
    virtual ~UI() {}
};

#endif
/**************************  END  UI.h **************************/
/************************** BEGIN SimpleParser.h *********************
 FAUST Architecture File
 Copyright (C) 2003-2022 GRAME, Centre National de Creation Musicale
 ---------------------------------------------------------------------
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation; either version 2.1 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 
 EXCEPTION : As a special exception, you may create a larger work
 that contains this FAUST architecture section and distribute
 that work under terms of your choice, so long as this FAUST
 architecture section is not modified.
 ********************************************************************/

#ifndef SIMPLEPARSER_H
#define SIMPLEPARSER_H

// ---------------------------------------------------------------------
//                          Simple Parser
// A parser returns true if it was able to parse what it is
// supposed to parse and advance the pointer. Otherwise it returns false
// and the pointer is not advanced so that another parser can be tried.
// ---------------------------------------------------------------------

#include <vector>
#include <map>
#include <string>
#include <cmath>
#include <fstream>
#include <sstream>
#include <stdio.h> // We use the lighter fprintf code
#include <ctype.h>
#include <assert.h>

#ifndef _WIN32
# pragma GCC diagnostic ignored "-Wunused-function"
#endif

struct itemInfo {
    std::string type;
    std::string label;
    std::string shortname;
    std::string address;
    std::string url;
    int index;
    double init;
    double fmin;
    double fmax;
    double step;
    std::vector<std::pair<std::string, std::string> > meta;
    
    itemInfo():index(0), init(0.), fmin(0.), fmax(0.), step(0.)
    {}
};

// ---------------------------------------------------------------------
//                          Elementary parsers
// ---------------------------------------------------------------------

// Report a parsing error
static bool parseError(const char*& p, const char* errmsg)
{
    fprintf(stderr, "Parse error : %s here : %s\n", errmsg, p);
    return true;
}

/**
 * @brief skipBlank : advance pointer p to the first non blank character
 * @param p the string to parse, then the remaining string
 */
static void skipBlank(const char*& p)
{
    while (isspace(*p)) { p++; }
}

// Parse character x, but don't report error if fails
static bool tryChar(const char*& p, char x)
{
    skipBlank(p);
    if (x == *p) {
        p++;
        return true;
    } else {
        return false;
    }
}

/**
 * @brief parseChar : parse a specific character x
 * @param p the string to parse, then the remaining string
 * @param x the character to recognize
 * @return true if x was found at the begin of p
 */
static bool parseChar(const char*& p, char x)
{
    skipBlank(p);
    if (x == *p) {
        p++;
        return true;
    } else {
        return false;
    }
}

/**
 * @brief parseWord : parse a specific string w
 * @param p the string to parse, then the remaining string
 * @param w the string to recognize
 * @return true if string w was found at the begin of p
 */
static bool parseWord(const char*& p, const char* w)
{
    skipBlank(p);
    const char* saved = p;  // to restore position if we fail
    while ((*w == *p) && (*w)) {++w; ++p;}
    if (*w) {
        p = saved;
        return false;
    } else {
        return true;
    }
}

/**
 * @brief parseDouble : parse number [s]dddd[.dddd] or [s]d[.dddd][E|e][s][dddd] and store the result in x
 * @param p the string to parse, then the remaining string
 * @param x the float number found if any
 * @return true if a float number was found at the begin of p
 */
static bool parseDouble(const char*& p, double& x)
{
    double sign = 1.0;     // sign of the number
    double ipart = 0;      // integral part of the number
    double dpart = 0;      // decimal part of the number before division
    double dcoef = 1.0;    // division factor for the decimal part
    double expsign = 1.0;  // sign of the E|e part
    double expcoef = 0.0;  // multiplication factor of E|e part
    
    bool valid = false;    // true if the number contains at least one digit
    
    skipBlank(p);
    const char* saved = p;  // to restore position if we fail
    
    // Sign
    if (parseChar(p, '+')) {
        sign = 1.0;
    } else if (parseChar(p, '-')) {
        sign = -1.0;
    }
    
    // Integral part
    while (isdigit(*p)) {
        valid = true;
        ipart = ipart*10 + (*p - '0');
        p++;
    }
    
    // Possible decimal part
    if (parseChar(p, '.')) {
        while (isdigit(*p)) {
            valid = true;
            dpart = dpart*10 + (*p - '0');
            dcoef *= 10.0;
            p++;
        }
    }
    
    // Possible E|e part
    if (parseChar(p, 'E') || parseChar(p, 'e')) {
        if (parseChar(p, '+')) {
            expsign = 1.0;
        } else if (parseChar(p, '-')) {
            expsign = -1.0;
        }
        while (isdigit(*p)) {
            expcoef = expcoef*10 + (*p - '0');
            p++;
        }
    }
    
    if (valid)  {
        x = (sign*(ipart + dpart/dcoef)) * std::pow(10.0, expcoef*expsign);
    } else {
        p = saved;
    }
    return valid;
}

/**
 * @brief parseString, parse an arbitrary quoted string q...q and store the result in s
 * @param p the string to parse, then the remaining string
 * @param quote the character used to quote the string
 * @param s the (unquoted) string found if any
 * @return true if a string was found at the begin of p
 */
static bool parseString(const char*& p, char quote, std::string& s)
{
    std::string str;
    skipBlank(p);
    
    const char* saved = p;  // to restore position if we fail
    if (*p++ == quote) {
        while ((*p != 0) && (*p != quote)) {
            str += *p++;
        }
        if (*p++ == quote) {
            s = str;
            return true;
        }
    }
    p = saved;
    return false;
}

/**
 * @brief parseSQString, parse a single quoted string '...' and store the result in s
 * @param p the string to parse, then the remaining string
 * @param s the (unquoted) string found if any
 * @return true if a string was found at the begin of p
 */
static bool parseSQString(const char*& p, std::string& s)
{
    return parseString(p, '\'', s);
}

/**
 * @brief parseDQString, parse a double quoted string "..." and store the result in s
 * @param p the string to parse, then the remaining string
 * @param s the (unquoted) string found if any
 * @return true if a string was found at the begin of p
 */
static bool parseDQString(const char*& p, std::string& s)
{
    return parseString(p, '"', s);
}

// ---------------------------------------------------------------------
//
//                          IMPLEMENTATION
// 
// ---------------------------------------------------------------------

/**
 * @brief parseMenuItem, parse a menu item ...'low':440.0...
 * @param p the string to parse, then the remaining string
 * @param name the name found
 * @param value the value found
 * @return true if a nemu item was found
 */
static bool parseMenuItem(const char*& p, std::string& name, double& value)
{
    const char* saved = p;  // to restore position if we fail
    if (parseSQString(p, name) && parseChar(p, ':') && parseDouble(p, value)) {
        return true;
    } else {
        p = saved;
        return false;
    }
}

static bool parseMenuItem2(const char*& p, std::string& name)
{
    const char* saved = p;  // to restore position if we fail
    // single quoted
    if (parseSQString(p, name)) {
        return true;
    } else {
        p = saved;
        return false;
    }
}

/**
 * @brief parseMenuList, parse a menu list {'low' : 440.0; 'mid' : 880.0; 'hi' : 1760.0}...
 * @param p the string to parse, then the remaining string
 * @param names the vector of names found
 * @param values the vector of values found
 * @return true if a menu list was found
 */
static bool parseMenuList(const char*& p, std::vector<std::string>& names, std::vector<double>& values)
{
    std::vector<std::string> tmpnames;
    std::vector<double> tmpvalues;
    const char* saved = p; // to restore position if we fail

    if (parseChar(p, '{')) {
        do {
            std::string n;
            double v;
            if (parseMenuItem(p, n, v)) {
                tmpnames.push_back(n);
                tmpvalues.push_back(v);
            } else {
                p = saved;
                return false;
            }
        } while (parseChar(p, ';'));
        if (parseChar(p, '}')) {
            // we suceeded
            names = tmpnames;
            values = tmpvalues;
            return true;
        }
    }
    p = saved;
    return false;
}

static bool parseMenuList2(const char*& p, std::vector<std::string>& names, bool debug)
{
    std::vector<std::string> tmpnames;
    const char* saved = p;  // to restore position if we fail
    
    if (parseChar(p, '{')) {
        do {
            std::string n;
            if (parseMenuItem2(p, n)) {
                tmpnames.push_back(n);
            } else {
                goto error;
            }
        } while (parseChar(p, ';'));
        if (parseChar(p, '}')) {
            // we suceeded
            names = tmpnames;
            return true;
        }
    }
    
error:
    if (debug) { fprintf(stderr, "parseMenuList2 : (%s) is not a valid list !\n", p); }
    p = saved;
    return false;
}

/// ---------------------------------------------------------------------
// Parse list of strings
/// ---------------------------------------------------------------------
static bool parseList(const char*& p, std::vector<std::string>& items)
{
    const char* saved = p;  // to restore position if we fail
    if (parseChar(p, '[')) {
        do {
            std::string item;
            if (!parseDQString(p, item)) {
                p = saved;
                return false;
            }
            items.push_back(item);
        } while (tryChar(p, ','));
        return parseChar(p, ']');
    } else {
        p = saved;
        return false;
    }
}

static bool parseMetaData(const char*& p, std::map<std::string, std::string>& metadatas)
{
    const char* saved = p; // to restore position if we fail
    std::string metaKey, metaValue;
    if (parseChar(p, ':') && parseChar(p, '[')) {
        do { 
            if (parseChar(p, '{') && parseDQString(p, metaKey) && parseChar(p, ':') && parseDQString(p, metaValue) && parseChar(p, '}')) {
                metadatas[metaKey] = metaValue;
            }
        } while (tryChar(p, ','));
        return parseChar(p, ']');
    } else {
        p = saved;
        return false;
    }
}

static bool parseItemMetaData(const char*& p, std::vector<std::pair<std::string, std::string> >& metadatas)
{
    const char* saved = p; // to restore position if we fail
    std::string metaKey, metaValue;
    if (parseChar(p, ':') && parseChar(p, '[')) {
        do { 
            if (parseChar(p, '{') && parseDQString(p, metaKey) && parseChar(p, ':') && parseDQString(p, metaValue) && parseChar(p, '}')) {
                metadatas.push_back(std::make_pair(metaKey, metaValue));
            }
        } while (tryChar(p, ','));
        return parseChar(p, ']');
    } else {
        p = saved;
        return false;
    }
}

// ---------------------------------------------------------------------
// Parse metadatas of the interface:
// "name" : "...", "inputs" : "...", "outputs" : "...", ...
// and store the result as key/value
/// ---------------------------------------------------------------------
static bool parseGlobalMetaData(const char*& p, std::string& key, std::string& value, double& dbl, std::map<std::string, std::string>& metadatas, std::vector<std::string>& items)
{
    const char* saved = p; // to restore position if we fail
    if (parseDQString(p, key)) {
        if (key == "meta") {
            return parseMetaData(p, metadatas);
        } else {
            return parseChar(p, ':') && (parseDQString(p, value) || parseList(p, items) || parseDouble(p, dbl));
        }
    } else {
        p = saved;
        return false;
    }
}

// ---------------------------------------------------------------------
// Parse gui:
// "type" : "...", "label" : "...", "address" : "...", ...
// and store the result in uiItems Vector
/// ---------------------------------------------------------------------
static bool parseUI(const char*& p, std::vector<itemInfo>& uiItems, int& numItems)
{
    const char* saved = p; // to restore position if we fail
    if (parseChar(p, '{')) {
   
        std::string label;
        std::string value;
        double dbl = 0;
        
        do {
            if (parseDQString(p, label)) {
                if (label == "type") {
                    if (uiItems.size() != 0) {
                        numItems++;
                    }
                    if (parseChar(p, ':') && parseDQString(p, value)) {   
                        itemInfo item;
                        item.type = value;
                        uiItems.push_back(item);
                    }
                }
                
                else if (label == "label") {
                    if (parseChar(p, ':') && parseDQString(p, value)) {
                        uiItems[numItems].label = value;
                    }
                }
                
                else if (label == "shortname") {
                    if (parseChar(p, ':') && parseDQString(p, value)) {
                        uiItems[numItems].shortname = value;
                    }
                }
                
                else if (label == "address") {
                    if (parseChar(p, ':') && parseDQString(p, value)) {
                        uiItems[numItems].address = value;
                    }
                }
                
                else if (label == "url") {
                    if (parseChar(p, ':') && parseDQString(p, value)) {
                        uiItems[numItems].url = value;
                    }
                }
                
                else if (label == "index") {
                    if (parseChar(p, ':') && parseDouble(p, dbl)) {
                        uiItems[numItems].index = int(dbl);
                    }
                }
                
                else if (label == "meta") {
                    if (!parseItemMetaData(p, uiItems[numItems].meta)) {
                        return false;
                    }
                }
                
                else if (label == "init") {
                    if (parseChar(p, ':') && parseDouble(p, dbl)) {
                        uiItems[numItems].init = dbl;
                    }
                }
                
                else if (label == "min") {
                    if (parseChar(p, ':') && parseDouble(p, dbl)) {
                        uiItems[numItems].fmin = dbl;
                    }
                }
                
                else if (label == "max") {
                    if (parseChar(p, ':') && parseDouble(p, dbl)) {
                        uiItems[numItems].fmax = dbl;
                    }
                }
                
                else if (label == "step") {
                    if (parseChar(p, ':') && parseDouble(p, dbl)) {
                        uiItems[numItems].step = dbl;
                    }
                }
                
                else if (label == "items") {
                    if (parseChar(p, ':') && parseChar(p, '[')) {
                        do {
                            if (!parseUI(p, uiItems, numItems)) {
                                p = saved;
                                return false;
                            }
                        } while (tryChar(p, ','));
                        if (parseChar(p, ']')) {
                            itemInfo item;
                            item.type = "close";
                            uiItems.push_back(item);
                            numItems++;
                        }
                    }
            
                } else {
                    fprintf(stderr, "Parse error unknown : %s \n", label.c_str());
                    assert(false);
                }
            } else {
                p = saved;
                return false;
            }
            
        } while (tryChar(p, ','));
    
        return parseChar(p, '}');
    } else {
        return true; // "items": [] is valid
    }
}

// ---------------------------------------------------------------------
// Parse full JSON record describing a JSON/Faust interface :
// {"metadatas": "...", "ui": [{ "type": "...", "label": "...", "items": [...], "address": "...","init": "...", "min": "...", "max": "...","step": "..."}]}
//
// and store the result in map Metadatas and vector containing the items of the interface. Returns true if parsing was successfull.
/// ---------------------------------------------------------------------
static bool parseJson(const char*& p,
                      std::map<std::string, std::pair<std::string, double> >& metaDatas0,
                      std::map<std::string, std::string>& metaDatas1,
                      std::map<std::string, std::vector<std::string> >& metaDatas2,
                      std::vector<itemInfo>& uiItems)
{
    parseChar(p, '{');
    
    do {
        std::string key;
        std::string value;
        double dbl = 0;
        std::vector<std::string> items;
        if (parseGlobalMetaData(p, key, value, dbl, metaDatas1, items)) {
            if (key != "meta") {
                // keep "name", "inputs", "outputs" key/value pairs
                if (items.size() > 0) {
                    metaDatas2[key] = items;
                    items.clear();
                } else if (value != "") {
                    metaDatas0[key].first = value;
                } else {
                    metaDatas0[key].second = dbl;
                }
            }
        } else if (key == "ui") {
            int numItems = 0;
            parseChar(p, '[') && parseUI(p, uiItems, numItems);
        }
    } while (tryChar(p, ','));
    
    return parseChar(p, '}');
}

#endif // SIMPLEPARSER_H
/**************************  END  SimpleParser.h **************************/
/************************** BEGIN PathBuilder.h **************************
 FAUST Architecture File
 Copyright (C) 2003-2022 GRAME, Centre National de Creation Musicale
 ---------------------------------------------------------------------
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation; either version 2.1 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 
 EXCEPTION : As a special exception, you may create a larger work
 that contains this FAUST architecture section and distribute
 that work under terms of your choice, so long as this FAUST
 architecture section is not modified.
 ************************************************************************/

#ifndef __PathBuilder__
#define __PathBuilder__

#include <vector>
#include <set>
#include <map>
#include <string>
#include <algorithm>


/*******************************************************************************
 * PathBuilder : Faust User Interface
 * Helper class to build complete hierarchical path for UI items.
 ******************************************************************************/

class FAUST_API PathBuilder {

    protected:
    
        std::vector<std::string> fControlsLevel;
        std::vector<std::string> fFullPaths;
        std::map<std::string, std::string> fFull2Short;  // filled by computeShortNames()
    
        /**
         * @brief check if a character is acceptable for an ID
         *
         * @param c
         * @return true is the character is acceptable for an ID
         */
        bool isIDChar(char c) const
        {
            return ((c >= 'a') && (c <= 'z')) || ((c >= 'A') && (c <= 'Z')) || ((c >= '0') && (c <= '9'));
        }
    
        /**
         * @brief remove all "/0x00" parts
         *
         * @param src
         * @return modified string
         */
        std::string remove0x00(const std::string& src_aux) const
        {
            std::string src = src_aux;
            std::string from = "/0x00";
            std::string to = "";
            size_t pos = std::string::npos;
            while ((pos = src.find(from)) && (pos != std::string::npos)) {
                src = src.replace(pos, from.length(), to);
            }
            return src;
        }
    
        /**
         * @brief replace all non ID char with '_' (one '_' may replace several non ID char)
         *
         * @param src
         * @return modified string
         */
        std::string str2ID(const std::string& src) const
        {
            std::string dst;
            bool need_underscore = false;
            for (char c : src) {
                if (isIDChar(c) || (c == '/')) {
                    if (need_underscore) {
                        dst.push_back('_');
                        need_underscore = false;
                    }
                    dst.push_back(c);
                } else {
                    need_underscore = true;
                }
            }
            return dst;
        }
    
        /**
         * @brief Keep only the last n slash-parts
         *
         * @param src
         * @param n : 1 indicates the last slash-part
         * @return modified string
         */
        std::string cut(const std::string& src, int n) const
        {
            std::string rdst;
            for (int i = int(src.length())-1; i >= 0; i--) {
                char c = src[i];
                if (c != '/') {
                    rdst.push_back(c);
                } else if (n == 1) {
                    std::string dst;
                    for (int j = int(rdst.length())-1; j >= 0; j--) {
                        dst.push_back(rdst[j]);
                    }
                    return dst;
                } else {
                    n--;
                    rdst.push_back(c);
                }
            }
            return src;
        }
    
        void addFullPath(const std::string& label) { fFullPaths.push_back(buildPath(label)); }
    
        /**
         * @brief Compute the mapping between full path and short names
         */
        void computeShortNames()
        {
            std::vector<std::string>           uniquePaths;  // all full paths transformed but made unique with a prefix
            std::map<std::string, std::string> unique2full;  // all full paths transformed but made unique with a prefix
            char num_buffer[16];
            int pnum = 0;
            
            for (const auto& s : fFullPaths) {
                // Using snprintf since Teensy does not have the std::to_string function
                snprintf(num_buffer, 16, "%d", pnum++);
                std::string u = "/P" + std::string(num_buffer) + str2ID(remove0x00(s));
                uniquePaths.push_back(u);
                unique2full[u] = s;  // remember the full path associated to a unique path
            }
        
            std::map<std::string, int> uniquePath2level;                // map path to level
            for (const auto& s : uniquePaths) uniquePath2level[s] = 1;   // we init all levels to 1
            bool have_collisions = true;
        
            while (have_collisions) {
                // compute collision list
                std::set<std::string>              collisionSet;
                std::map<std::string, std::string> short2full;
                have_collisions = false;
                for (const auto& it : uniquePath2level) {
                    std::string u = it.first;
                    int n = it.second;
                    std::string shortName = cut(u, n);
                    auto p = short2full.find(shortName);
                    if (p == short2full.end()) {
                        // no collision
                        short2full[shortName] = u;
                    } else {
                        // we have a collision, add the two paths to the collision set
                        have_collisions = true;
                        collisionSet.insert(u);
                        collisionSet.insert(p->second);
                    }
                }
                for (const auto& s : collisionSet) uniquePath2level[s]++;  // increase level of colliding path
            }
        
            for (const auto& it : uniquePath2level) {
                std::string u = it.first;
                int n = it.second;
                std::string shortName = replaceCharList(cut(u, n), {'/'}, '_');
                fFull2Short[unique2full[u]] = shortName;
            }
        }
    
        std::string replaceCharList(const std::string& str, const std::vector<char>& ch1, char ch2)
        {
            auto beg = ch1.begin();
            auto end = ch1.end();
            std::string res = str;
            for (size_t i = 0; i < str.length(); ++i) {
                if (std::find(beg, end, str[i]) != end) res[i] = ch2;
            }
            return res;
        }
     
    public:
    
        PathBuilder() {}
        virtual ~PathBuilder() {}
    
        // Return true for the first level of groups
        bool pushLabel(const std::string& label) { fControlsLevel.push_back(label); return fControlsLevel.size() == 1; }
    
        // Return true for the last level of groups
        bool popLabel() { fControlsLevel.pop_back(); return fControlsLevel.size() == 0; }
    
        // Return a complete path built from a label
        std::string buildPath(const std::string& label)
        {
            std::string res = "/";
            for (size_t i = 0; i < fControlsLevel.size(); i++) {
                res = res + fControlsLevel[i] + "/";
            }
            res += label;
            return replaceCharList(res, {' ', '#', '*', ',', '?', '[', ']', '{', '}', '(', ')'}, '_');
        }
    
        // Assuming shortnames have been built, return the shortname from a label
        std::string buildShortname(const std::string& label)
        {
            return (hasShortname()) ? fFull2Short[buildPath(label)] : "";
        }
    
        bool hasShortname() { return fFull2Short.size() > 0; }
    
};

#endif  // __PathBuilder__
/**************************  END  PathBuilder.h **************************/
/************************** BEGIN dsp-combiner.h **************************
FAUST Architecture File
Copyright (C) 2003-2022 GRAME, Centre National de Creation Musicale
---------------------------------------------------------------------
This program is free software; you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation; either version 2.1 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

EXCEPTION : As a special exception, you may create a larger work
that contains this FAUST architecture section and distribute
that work under terms of your choice, so long as this FAUST
architecture section is not modified.
************************************************************************/

#ifndef __dsp_combiner__
#define __dsp_combiner__

#include <string.h>
#include <string>
#include <assert.h>
#include <sstream>

/************************** BEGIN dsp.h ********************************
 FAUST Architecture File
 Copyright (C) 2003-2022 GRAME, Centre National de Creation Musicale
 ---------------------------------------------------------------------
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation; either version 2.1 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 
 EXCEPTION : As a special exception, you may create a larger work
 that contains this FAUST architecture section and distribute
 that work under terms of your choice, so long as this FAUST
 architecture section is not modified.
 ************************************************************************/

#ifndef __dsp__
#define __dsp__

#include <string>
#include <vector>
#include <cstdint>


#ifndef FAUSTFLOAT
#define FAUSTFLOAT float
#endif

struct FAUST_API UI;
struct FAUST_API Meta;

/**
 * DSP memory manager.
 */

struct FAUST_API dsp_memory_manager {
    
    virtual ~dsp_memory_manager() {}
    
    /**
     * Inform the Memory Manager with the number of expected memory zones.
     * @param count - the number of expected memory zones
     */
    virtual void begin(size_t /*count*/) {}
    
    /**
     * Give the Memory Manager information on a given memory zone.
     * @param size - the size in bytes of the memory zone
     * @param reads - the number of Read access to the zone used to compute one frame
     * @param writes - the number of Write access to the zone used to compute one frame
     */
    virtual void info(size_t /*size*/, size_t /*reads*/, size_t /*writes*/) {}

    /**
     * Inform the Memory Manager that all memory zones have been described,
     * to possibly start a 'compute the best allocation strategy' step.
     */
    virtual void end() {}
    
    /**
     * Allocate a memory zone.
     * @param size - the memory zone size in bytes
     */
    virtual void* allocate(size_t size) = 0;
    
    /**
     * Destroy a memory zone.
     * @param ptr - the memory zone pointer to be deallocated
     */
    virtual void destroy(void* ptr) = 0;
    
};

/**
* Signal processor definition.
*/

class FAUST_API dsp {

    public:

        dsp() {}
        virtual ~dsp() {}

        /* Return instance number of audio inputs */
        virtual int getNumInputs() = 0;
    
        /* Return instance number of audio outputs */
        virtual int getNumOutputs() = 0;
    
        /**
         * Trigger the ui_interface parameter with instance specific calls
         * to 'openTabBox', 'addButton', 'addVerticalSlider'... in order to build the UI.
         *
         * @param ui_interface - the user interface builder
         */
        virtual void buildUserInterface(UI* ui_interface) = 0;
    
        /* Return the sample rate currently used by the instance */
        virtual int getSampleRate() = 0;
    
        /**
         * Global init, calls the following methods:
         * - static class 'classInit': static tables initialization
         * - 'instanceInit': constants and instance state initialization
         *
         * @param sample_rate - the sampling rate in Hz
         */
        virtual void init(int sample_rate) = 0;

        /**
         * Init instance state
         *
         * @param sample_rate - the sampling rate in Hz
         */
        virtual void instanceInit(int sample_rate) = 0;
    
        /**
         * Init instance constant state
         *
         * @param sample_rate - the sampling rate in Hz
         */
        virtual void instanceConstants(int sample_rate) = 0;
    
        /* Init default control parameters values */
        virtual void instanceResetUserInterface() = 0;
    
        /* Init instance state (like delay lines...) but keep the control parameter values */
        virtual void instanceClear() = 0;
 
        /**
         * Return a clone of the instance.
         *
         * @return a copy of the instance on success, otherwise a null pointer.
         */
        virtual dsp* clone() = 0;
    
        /**
         * Trigger the Meta* parameter with instance specific calls to 'declare' (key, value) metadata.
         *
         * @param m - the Meta* meta user
         */
        virtual void metadata(Meta* m) = 0;
    
        /**
         * DSP instance computation, to be called with successive in/out audio buffers.
         *
         * Note that by default inputs and outputs buffers are supposed to be distinct memory zones,
         * so one cannot safely write compute(count, inputs, inputs).
         * The -inpl compilation option can be used for that, but only in scalar mode for now.
         *
         * @param count - the number of frames to compute
         * @param inputs - the input audio buffers as an array of non-interleaved FAUSTFLOAT samples (eiher float, double or quad)
         * @param outputs - the output audio buffers as an array of non-interleaved FAUSTFLOAT samples (eiher float, double or quad)
         *
         */
        virtual void compute(int count, FAUSTFLOAT** inputs, FAUSTFLOAT** outputs) = 0;
    
        /**
         * Alternative DSP instance computation method for use by subclasses, incorporating an additional `date_usec` parameter,
         * which specifies the timestamp of the first sample in the audio buffers.
         *
         * @param date_usec - the timestamp in microsec given by audio driver. By convention timestamp of -1 means 'no timestamp conversion',
         * events already have a timestamp expressed in frames.
         * @param count - the number of frames to compute
         * @param inputs - the input audio buffers as an array of non-interleaved FAUSTFLOAT samples (either float, double or quad)
         * @param outputs - the output audio buffers as an array of non-interleaved FAUSTFLOAT samples (either float, double or quad)
         *
         */
        virtual void compute(double /*date_usec*/, int count, FAUSTFLOAT** inputs, FAUSTFLOAT** outputs) { compute(count, inputs, outputs); }
       
};

/**
 * Generic DSP decorator.
 */

class FAUST_API decorator_dsp : public dsp {

    protected:

        dsp* fDSP;

    public:

        decorator_dsp(dsp* dsp = nullptr):fDSP(dsp) {}
        virtual ~decorator_dsp() { delete fDSP; }

        virtual int getNumInputs() { return fDSP->getNumInputs(); }
        virtual int getNumOutputs() { return fDSP->getNumOutputs(); }
        virtual void buildUserInterface(UI* ui_interface) { fDSP->buildUserInterface(ui_interface); }
        virtual int getSampleRate() { return fDSP->getSampleRate(); }
        virtual void init(int sample_rate) { fDSP->init(sample_rate); }
        virtual void instanceInit(int sample_rate) { fDSP->instanceInit(sample_rate); }
        virtual void instanceConstants(int sample_rate) { fDSP->instanceConstants(sample_rate); }
        virtual void instanceResetUserInterface() { fDSP->instanceResetUserInterface(); }
        virtual void instanceClear() { fDSP->instanceClear(); }
        virtual decorator_dsp* clone() { return new decorator_dsp(fDSP->clone()); }
        virtual void metadata(Meta* m) { fDSP->metadata(m); }
        // Beware: subclasses usually have to overload the two 'compute' methods
        virtual void compute(int count, FAUSTFLOAT** inputs, FAUSTFLOAT** outputs) { fDSP->compute(count, inputs, outputs); }
        virtual void compute(double date_usec, int count, FAUSTFLOAT** inputs, FAUSTFLOAT** outputs) { fDSP->compute(date_usec, count, inputs, outputs); }
    
};

/**
 * DSP factory class, used with LLVM and Interpreter backends
 * to create DSP instances from a compiled DSP program.
 */

class FAUST_API dsp_factory {
    
    protected:
    
        // So that to force sub-classes to use deleteDSPFactory(dsp_factory* factory);
        virtual ~dsp_factory() {}
    
    public:
    
        /* Return factory name */
        virtual std::string getName() = 0;
    
        /* Return factory SHA key */
        virtual std::string getSHAKey() = 0;
    
        /* Return factory expanded DSP code */
        virtual std::string getDSPCode() = 0;
    
        /* Return factory compile options */
        virtual std::string getCompileOptions() = 0;
    
        /* Get the Faust DSP factory list of library dependancies */
        virtual std::vector<std::string> getLibraryList() = 0;
    
        /* Get the list of all used includes */
        virtual std::vector<std::string> getIncludePathnames() = 0;
    
        /* Get warning messages list for a given compilation */
        virtual std::vector<std::string> getWarningMessages() = 0;
    
        /* Create a new DSP instance, to be deleted with C++ 'delete' */
        virtual dsp* createDSPInstance() = 0;
    
        /* Static tables initialization, possibly implemened in sub-classes*/
        virtual void classInit(int sample_rate) {};
    
        /* Set a custom memory manager to be used when creating instances */
        virtual void setMemoryManager(dsp_memory_manager* manager) = 0;
    
        /* Return the currently set custom memory manager */
        virtual dsp_memory_manager* getMemoryManager() = 0;
    
};

// Denormal handling

#if defined (__SSE__)
#include <xmmintrin.h>
#endif

class FAUST_API ScopedNoDenormals {
    
    private:
    
        intptr_t fpsr = 0;
        
        void setFpStatusRegister(intptr_t fpsr_aux) noexcept
        {
        #if defined (__arm64__) || defined (__aarch64__)
            asm volatile("msr fpcr, %0" : : "ri" (fpsr_aux));
        #elif defined (__SSE__)
            // The volatile keyword here is needed to workaround a bug in AppleClang 13.0
            // which aggressively optimises away the variable otherwise
            volatile uint32_t fpsr_w = static_cast<uint32_t>(fpsr_aux);
            _mm_setcsr(fpsr_w);
        #endif
        }
        
        void getFpStatusRegister() noexcept
        {
        #if defined (__arm64__) || defined (__aarch64__)
            asm volatile("mrs %0, fpcr" : "=r" (fpsr));
        #elif defined (__SSE__)
            fpsr = static_cast<intptr_t>(_mm_getcsr());
        #endif
        }
    
    public:
    
        ScopedNoDenormals() noexcept
        {
        #if defined (__arm64__) || defined (__aarch64__)
            intptr_t mask = (1 << 24 /* FZ */);
        #elif defined (__SSE__)
        #if defined (__SSE2__)
            intptr_t mask = 0x8040;
        #else
            intptr_t mask = 0x8000;
        #endif
        #else
            intptr_t mask = 0x0000;
        #endif
            getFpStatusRegister();
            setFpStatusRegister(fpsr | mask);
        }
        
        ~ScopedNoDenormals() noexcept
        {
            setFpStatusRegister(fpsr);
        }

};

#define AVOIDDENORMALS ScopedNoDenormals ftz_scope;

#endif

/************************** END dsp.h **************************/

/**
 * @file dsp-combiner.h
 * @brief DSP Combiner Library
 *
 * This library provides classes for combining DSP modules.
 * It includes classes for sequencing, parallelizing, splitting, merging, recursing, and crossfading DSP modules.
 *
 */

enum Layout { kVerticalGroup, kHorizontalGroup, kTabGroup };

/**
 * @class dsp_binary_combiner
 * @brief Base class and common code for binary combiners
 *
 * This class serves as the base class for various DSP combiners that work with two DSP modules.
 * It provides common methods for building user interfaces, allocating and deleting channels, and more.
 */
class dsp_binary_combiner : public dsp {

    protected:

        dsp* fDSP1;
        dsp* fDSP2;
        int fBufferSize;
        Layout fLayout;
        std::string fLabel;

        void buildUserInterfaceAux(UI* ui_interface)
        {
            switch (fLayout) {
                case kHorizontalGroup:
                    ui_interface->openHorizontalBox(fLabel.c_str());
                    fDSP1->buildUserInterface(ui_interface);
                    fDSP2->buildUserInterface(ui_interface);
                    ui_interface->closeBox();
                    break;
                case kVerticalGroup:
                    ui_interface->openVerticalBox(fLabel.c_str());
                    fDSP1->buildUserInterface(ui_interface);
                    fDSP2->buildUserInterface(ui_interface);
                    ui_interface->closeBox();
                    break;
                case kTabGroup:
                    ui_interface->openTabBox(fLabel.c_str());
                    ui_interface->openVerticalBox("DSP1");
                    fDSP1->buildUserInterface(ui_interface);
                    ui_interface->closeBox();
                    ui_interface->openVerticalBox("DSP2");
                    fDSP2->buildUserInterface(ui_interface);
                    ui_interface->closeBox();
                    ui_interface->closeBox();
                    break;
            }
        }

        FAUSTFLOAT** allocateChannels(int num)
        {
            FAUSTFLOAT** channels = new FAUSTFLOAT*[num];
            for (int chan = 0; chan < num; chan++) {
                channels[chan] = new FAUSTFLOAT[fBufferSize];
                memset(channels[chan], 0, sizeof(FAUSTFLOAT) * fBufferSize);
            }
            return channels;
        }

        void deleteChannels(FAUSTFLOAT** channels, int num)
        {
            for (int chan = 0; chan < num; chan++) {
                delete [] channels[chan];
            }
            delete [] channels;
        }

     public:

        dsp_binary_combiner(dsp* dsp1, dsp* dsp2, int buffer_size, Layout layout, const std::string& label)
        :fDSP1(dsp1), fDSP2(dsp2), fBufferSize(buffer_size), fLayout(layout), fLabel(label)
        {}

        virtual ~dsp_binary_combiner()
        {
            delete fDSP1;
            delete fDSP2;
        }

        virtual int getSampleRate()
        {
            return fDSP1->getSampleRate();
        }
        virtual void init(int sample_rate)
        {
            fDSP1->init(sample_rate);
            fDSP2->init(sample_rate);
        }
        virtual void instanceInit(int sample_rate)
        {
            fDSP1->instanceInit(sample_rate);
            fDSP2->instanceInit(sample_rate);
        }
        virtual void instanceConstants(int sample_rate)
        {
            fDSP1->instanceConstants(sample_rate);
            fDSP2->instanceConstants(sample_rate);
        }

        virtual void instanceResetUserInterface()
        {
            fDSP1->instanceResetUserInterface();
            fDSP2->instanceResetUserInterface();
        }

        virtual void instanceClear()
        {
            fDSP1->instanceClear();
            fDSP2->instanceClear();
        }

        virtual void metadata(Meta* m)
        {
            fDSP1->metadata(m);
            fDSP2->metadata(m);
        }

};

/**
 * @class dsp_sequencer
 * @brief Combine two 'compatible' DSP modules in sequence
 *
 * This class allows you to combine two DSP modules in sequence.
 * It computes the first DSP module's outputs and uses them as inputs for the second DSP module.
 */
class dsp_sequencer : public dsp_binary_combiner {

    private:

        FAUSTFLOAT** fDSP1Outputs;

    public:

        dsp_sequencer(dsp* dsp1, dsp* dsp2,
                      int buffer_size = 4096,
                      Layout layout = Layout::kTabGroup,
                      const std::string& label = "Sequencer")
        :dsp_binary_combiner(dsp1, dsp2, buffer_size, layout, label)
        {
            fDSP1Outputs = allocateChannels(fDSP1->getNumOutputs());
        }

        virtual ~dsp_sequencer()
        {
            deleteChannels(fDSP1Outputs, fDSP1->getNumOutputs());
        }

        virtual int getNumInputs() { return fDSP1->getNumInputs(); }
        virtual int getNumOutputs() { return fDSP2->getNumOutputs(); }

        virtual void buildUserInterface(UI* ui_interface)
        {
            buildUserInterfaceAux(ui_interface);
        }

        virtual dsp* clone()
        {
            return new dsp_sequencer(fDSP1->clone(), fDSP2->clone(), fBufferSize, fLayout, fLabel);
        }

        virtual void compute(int count, FAUSTFLOAT** inputs, FAUSTFLOAT** outputs)
        {
            fDSP1->compute(count, inputs, fDSP1Outputs);
            fDSP2->compute(count, fDSP1Outputs, outputs);
        }

        virtual void compute(double date_usec, int count, FAUSTFLOAT** inputs, FAUSTFLOAT** outputs) { compute(count, inputs, outputs); }

};

/**
 * @class dsp_parallelizer
 * @brief Combine two DSP modules in parallel
 *
 * This class combines two DSP modules in parallel.
 * It computes both DSP modules separately and combines their outputs.
 */
class dsp_parallelizer : public dsp_binary_combiner {

    private:

        FAUSTFLOAT** fDSP2Inputs;
        FAUSTFLOAT** fDSP2Outputs;

    public:

        dsp_parallelizer(dsp* dsp1, dsp* dsp2,
                     int buffer_size = 4096,
                     Layout layout = Layout::kTabGroup,
                     const std::string& label = "Parallelizer")
        :dsp_binary_combiner(dsp1, dsp2, buffer_size, layout, label)
        {
            fDSP2Inputs = new FAUSTFLOAT*[fDSP2->getNumInputs()];
            fDSP2Outputs = new FAUSTFLOAT*[fDSP2->getNumOutputs()];
        }

        virtual ~dsp_parallelizer()
        {
            delete [] fDSP2Inputs;
            delete [] fDSP2Outputs;
        }

        virtual int getNumInputs() { return fDSP1->getNumInputs() + fDSP2->getNumInputs(); }
        virtual int getNumOutputs() { return fDSP1->getNumOutputs() + fDSP2->getNumOutputs(); }

        virtual void buildUserInterface(UI* ui_interface)
        {
            buildUserInterfaceAux(ui_interface);
        }

        virtual dsp* clone()
        {
            return new dsp_parallelizer(fDSP1->clone(), fDSP2->clone(), fBufferSize, fLayout, fLabel);
        }

        virtual void compute(int count, FAUSTFLOAT** inputs, FAUSTFLOAT** outputs)
        {
            fDSP1->compute(count, inputs, outputs);

            // Shift inputs/outputs channels for fDSP2
            for (int chan = 0; chan < fDSP2->getNumInputs(); chan++) {
                fDSP2Inputs[chan] = inputs[fDSP1->getNumInputs() + chan];
            }
            for (int chan = 0; chan < fDSP2->getNumOutputs(); chan++) {
                fDSP2Outputs[chan] = outputs[fDSP1->getNumOutputs() + chan];
            }

            fDSP2->compute(count, fDSP2Inputs, fDSP2Outputs);
        }

        virtual void compute(double date_usec, int count, FAUSTFLOAT** inputs, FAUSTFLOAT** outputs) { compute(count, inputs, outputs); }

};

/**
 * @class dsp_splitter
 * @brief Combine two 'compatible' DSP modules in a splitter
 *
 * This class combines two DSP modules in a splitter configuration.
 * The outputs of the first DSP module are connected to the inputs of the second DSP module.
 */
class dsp_splitter : public dsp_binary_combiner {

    private:

        FAUSTFLOAT** fDSP1Outputs;
        FAUSTFLOAT** fDSP2Inputs;

    public:

        dsp_splitter(dsp* dsp1, dsp* dsp2,
                     int buffer_size = 4096,
                     Layout layout = Layout::kTabGroup,
                     const std::string& label = "Splitter")
        :dsp_binary_combiner(dsp1, dsp2, buffer_size, layout, label)
        {
            fDSP1Outputs = allocateChannels(fDSP1->getNumOutputs());
            fDSP2Inputs = new FAUSTFLOAT*[fDSP2->getNumInputs()];
        }

        virtual ~dsp_splitter()
        {
            deleteChannels(fDSP1Outputs, fDSP1->getNumOutputs());
            delete [] fDSP2Inputs;
        }

        virtual int getNumInputs() { return fDSP1->getNumInputs(); }
        virtual int getNumOutputs() { return fDSP2->getNumOutputs(); }

        virtual void buildUserInterface(UI* ui_interface)
        {
            buildUserInterfaceAux(ui_interface);
        }

        virtual dsp* clone()
        {
            return new dsp_splitter(fDSP1->clone(), fDSP2->clone(), fBufferSize, fLayout, fLabel);
        }

        virtual void compute(int count, FAUSTFLOAT** inputs, FAUSTFLOAT** outputs)
        {
            fDSP1->compute(count, inputs, fDSP1Outputs);

            for (int chan = 0; chan < fDSP2->getNumInputs(); chan++) {
                 fDSP2Inputs[chan] = fDSP1Outputs[chan % fDSP1->getNumOutputs()];
            }

            fDSP2->compute(count, fDSP2Inputs, outputs);
        }
};

/**
 * @class dsp_merger
 * @brief Combine two 'compatible' DSP modules in a merger
 *
 * This class combines two DSP modules in a merger configuration.
 * The outputs of the first DSP module are combined with the inputs of the second DSP module.
 */
class dsp_merger : public dsp_binary_combiner {

    private:

        FAUSTFLOAT** fDSP1Inputs;
        FAUSTFLOAT** fDSP1Outputs;
        FAUSTFLOAT** fDSP2Inputs;

        void mix(int count, FAUSTFLOAT* dst, FAUSTFLOAT* src)
        {
            for (int frame = 0; frame < count; frame++) {
                dst[frame] += src[frame];
            }
        }

    public:

        dsp_merger(dsp* dsp1, dsp* dsp2,
                   int buffer_size = 4096,
                   Layout layout = Layout::kTabGroup,
                   const std::string& label = "Merger")
        :dsp_binary_combiner(dsp1, dsp2, buffer_size, layout, label)
        {
            fDSP1Inputs = allocateChannels(fDSP1->getNumInputs());
            fDSP1Outputs = allocateChannels(fDSP1->getNumOutputs());
            fDSP2Inputs = new FAUSTFLOAT*[fDSP2->getNumInputs()];
        }

        virtual ~dsp_merger()
        {
            deleteChannels(fDSP1Inputs, fDSP1->getNumInputs());
            deleteChannels(fDSP1Outputs, fDSP1->getNumOutputs());
            delete [] fDSP2Inputs;
        }

        virtual int getNumInputs() { return fDSP1->getNumInputs(); }
        virtual int getNumOutputs() { return fDSP2->getNumOutputs(); }

        virtual void buildUserInterface(UI* ui_interface)
        {
            buildUserInterfaceAux(ui_interface);
        }

        virtual dsp* clone()
        {
            return new dsp_merger(fDSP1->clone(), fDSP2->clone(), fBufferSize, fLayout, fLabel);
        }

        virtual void compute(int count, FAUSTFLOAT** inputs, FAUSTFLOAT** outputs)
        {
            fDSP1->compute(count, fDSP1Inputs, fDSP1Outputs);

            memset(fDSP2Inputs, 0, sizeof(FAUSTFLOAT*) * fDSP2->getNumInputs());

            for (int chan = 0; chan < fDSP1->getNumOutputs(); chan++) {
                int mchan = chan % fDSP2->getNumInputs();
                if (fDSP2Inputs[mchan]) {
                    mix(count, fDSP2Inputs[mchan], fDSP1Outputs[chan]);
                } else {
                    fDSP2Inputs[mchan] = fDSP1Outputs[chan];
                }
            }

            fDSP2->compute(count, fDSP2Inputs, outputs);
        }
};

/**
 * @class dsp_recursiver
 * @brief Combine two 'compatible' DSP modules in a recursive way
 *
 * This class recursively combines two DSP modules.
 * The outputs of each module are fed as inputs to the other module in a recursive manner.
 */
class dsp_recursiver : public dsp_binary_combiner {

    private:

        FAUSTFLOAT** fDSP1Inputs;
        FAUSTFLOAT** fDSP1Outputs;

        FAUSTFLOAT** fDSP2Inputs;
        FAUSTFLOAT** fDSP2Outputs;

    public:

        dsp_recursiver(dsp* dsp1, dsp* dsp2,
                       Layout layout = Layout::kTabGroup,
                       const std::string& label = "Recursiver")
        :dsp_binary_combiner(dsp1, dsp2, 1, layout, label)
        {
            fDSP1Inputs = allocateChannels(fDSP1->getNumInputs());
            fDSP1Outputs = allocateChannels(fDSP1->getNumOutputs());
            fDSP2Inputs = allocateChannels(fDSP2->getNumInputs());
            fDSP2Outputs = allocateChannels(fDSP2->getNumOutputs());
        }

        virtual ~dsp_recursiver()
        {
            deleteChannels(fDSP1Inputs, fDSP1->getNumInputs());
            deleteChannels(fDSP1Outputs, fDSP1->getNumOutputs());
            deleteChannels(fDSP2Inputs, fDSP2->getNumInputs());
            deleteChannels(fDSP2Outputs, fDSP2->getNumOutputs());
        }

        virtual int getNumInputs() { return fDSP1->getNumInputs() - fDSP2->getNumOutputs(); }
        virtual int getNumOutputs() { return fDSP1->getNumOutputs(); }

        virtual void buildUserInterface(UI* ui_interface)
        {
            buildUserInterfaceAux(ui_interface);
        }

        virtual dsp* clone()
        {
            return new dsp_recursiver(fDSP1->clone(), fDSP2->clone(), fLayout, fLabel);
        }

        virtual void compute(int count, FAUSTFLOAT** inputs, FAUSTFLOAT** outputs)
        {
            for (int frame = 0; (frame < count); frame++) {

                for (int chan = 0; chan < fDSP2->getNumOutputs(); chan++) {
                    fDSP1Inputs[chan][0] = fDSP2Outputs[chan][0];
                }

                for (int chan = 0; chan < fDSP1->getNumInputs() - fDSP2->getNumOutputs(); chan++) {
                    fDSP1Inputs[chan + fDSP2->getNumOutputs()][0] = inputs[chan][frame];
                }

                fDSP1->compute(1, fDSP1Inputs, fDSP1Outputs);

                for (int chan = 0; chan < fDSP1->getNumOutputs(); chan++) {
                    outputs[chan][frame] = fDSP1Outputs[chan][0];
                }

                for (int chan = 0; chan < fDSP2->getNumInputs(); chan++) {
                    fDSP2Inputs[chan][0] = fDSP1Outputs[chan][0];
                }

                fDSP2->compute(1, fDSP2Inputs, fDSP2Outputs);
            }
        }

        virtual void compute(double date_usec, int count, FAUSTFLOAT** inputs, FAUSTFLOAT** outputs) { compute(count, inputs, outputs); }

};

/**
 * @class dsp_crossfader
 * @brief Crossfade between two DSP modules
 *
 * This class allows you to crossfade between two DSP modules.
 * The crossfade parameter (as a slider) controls the mix between the two modules' outputs.
 * When Crossfade = 1, the first DSP only is computed, when Crossfade = 0,
 * the second DSP only is computed, otherwise both DSPs are computed and mixed.
 */
class dsp_crossfader: public dsp_binary_combiner {

    private:
    
        FAUSTFLOAT fCrossfade;
        FAUSTFLOAT** fDSPOutputs1;
        FAUSTFLOAT** fDSPOutputs2;
    
    public:
    
        dsp_crossfader(dsp* dsp1, dsp* dsp2,
                       Layout layout = Layout::kTabGroup,
                       const std::string& label = "Crossfade")
        :dsp_binary_combiner(dsp1, dsp2, 4096, layout, label),fCrossfade(FAUSTFLOAT(0.5))
        {
            fDSPOutputs1 = allocateChannels(fDSP1->getNumOutputs());
            fDSPOutputs2 = allocateChannels(fDSP1->getNumOutputs());
        }
    
        virtual ~dsp_crossfader()
        {
            deleteChannels(fDSPOutputs1, fDSP1->getNumInputs());
            deleteChannels(fDSPOutputs2, fDSP1->getNumOutputs());
        }
    
        virtual int getNumInputs() { return fDSP1->getNumInputs(); }
        virtual int getNumOutputs() { return fDSP1->getNumOutputs(); }

        void buildUserInterface(UI* ui_interface)
        {
            switch (fLayout) {
                case kHorizontalGroup:
                    ui_interface->openHorizontalBox(fLabel.c_str());
                    ui_interface->addHorizontalSlider("Crossfade", &fCrossfade, FAUSTFLOAT(0.5), FAUSTFLOAT(0), FAUSTFLOAT(1), FAUSTFLOAT(0.01));
                    fDSP1->buildUserInterface(ui_interface);
                    fDSP2->buildUserInterface(ui_interface);
                    ui_interface->closeBox();
                    break;
                case kVerticalGroup:
                    ui_interface->openVerticalBox(fLabel.c_str());
                    ui_interface->addHorizontalSlider("Crossfade", &fCrossfade, FAUSTFLOAT(0.5), FAUSTFLOAT(0), FAUSTFLOAT(1), FAUSTFLOAT(0.01));
                    fDSP1->buildUserInterface(ui_interface);
                    fDSP2->buildUserInterface(ui_interface);
                    ui_interface->closeBox();
                    break;
                case kTabGroup:
                    ui_interface->openTabBox(fLabel.c_str());
                    ui_interface->openVerticalBox("Crossfade");
                    ui_interface->addHorizontalSlider("Crossfade", &fCrossfade, FAUSTFLOAT(0.5), FAUSTFLOAT(0), FAUSTFLOAT(1), FAUSTFLOAT(0.01));
                    ui_interface->closeBox();
                    ui_interface->openVerticalBox("DSP1");
                    fDSP1->buildUserInterface(ui_interface);
                    ui_interface->closeBox();
                    ui_interface->openVerticalBox("DSP2");
                    fDSP2->buildUserInterface(ui_interface);
                    ui_interface->closeBox();
                    ui_interface->closeBox();
                    break;
            }
        }
    
        virtual dsp* clone()
        {
            return new dsp_crossfader(fDSP1->clone(), fDSP2->clone(), fLayout, fLabel);
        }
    
        virtual void compute(int count, FAUSTFLOAT** inputs, FAUSTFLOAT** outputs)
        {
            if (fCrossfade == FAUSTFLOAT(1)) {
                fDSP1->compute(count, inputs, outputs);
            } else if (fCrossfade == FAUSTFLOAT(0)) {
                fDSP2->compute(count, inputs, outputs);
            } else {
                // Compute each effect
                fDSP1->compute(count, inputs, fDSPOutputs1);
                fDSP2->compute(count, inputs, fDSPOutputs2);
                // Mix between the two effects
                FAUSTFLOAT gain1 = fCrossfade;
                FAUSTFLOAT gain2 = FAUSTFLOAT(1) - gain1;
                for (int frame = 0; (frame < count); frame++) {
                    for (int chan = 0; chan < fDSP1->getNumOutputs(); chan++) {
                        outputs[chan][frame] = fDSPOutputs1[chan][frame] * gain1 + fDSPOutputs2[chan][frame] * gain2;
                    }
                }
            }
        }
    
        virtual void compute(double date_usec, int count, FAUSTFLOAT** inputs, FAUSTFLOAT** outputs) { compute(count, inputs, outputs); }
};

#ifndef __dsp_algebra_api__
#define __dsp_algebra_api__

/**
 * DSP algebra API allowing to combine DSPs using the 5 operators Faust block algebra and an additional crossfader combiner.
 * The two arguments GUI are composed in a group, either kVerticalGroup, kHorizontalGroup or kTabGroup with a label.
 *
 * Each operation takes two DSP and a optional layout and label parameters, returns the combined DSPs,
 * or null if failure with an error message.
 * 
 * It includes methods to create sequencers, parallelizers, splitters, mergers, recursivers, and crossfaders.
 */

/**
 * Create a DSP Sequencer
 *
 * This method creates a DSP Sequencer, which combines two DSP modules in a sequencer configuration.
 * The outputs of the first DSP module are connected to the inputs of the second DSP module.
 *
 * @param dsp1 The first DSP module to combine
 * @param dsp2 The second DSP module to combine
 * @param error A reference to a string to store error messages (if any)
 * @param layout The layout for the combined user interface (default: kTabGroup)
 * @param label The label for the combiner (default: "Sequencer")
 * @return A pointer to the created DSP Sequencer, or nullptr if an error occurs
 */
static dsp* createDSPSequencer(dsp* dsp1, dsp* dsp2,
                               std::string& error,
                               Layout layout = Layout::kTabGroup,
                               const std::string& label = "Sequencer")
{
    if (dsp1->getNumOutputs() != dsp2->getNumInputs()) {
        std::stringstream error_aux;
        error_aux << "Connection error in dsp_sequencer : the number of outputs ("
                  << dsp1->getNumOutputs() << ") of A "
                  << "must be equal to the number of inputs (" << dsp2->getNumInputs() << ") of B" << std::endl;
        error = error_aux.str();
        return nullptr;
    } else {
        return new dsp_sequencer(dsp1, dsp2, 4096, layout, label);
    }
}

/**
 * Create a DSP Parallelizer
 *
 * This method creates a DSP Parallelizer, which combines two DSP modules in parallel.
 * The resulting DSP module computes both input modules separately and combines their outputs.
 *
 * @param dsp1 The first DSP module to combine
 * @param dsp2 The second DSP module to combine
 * @param error A reference to a string to store error messages (if any)
 * @param layout The layout for the combined user interface (default: kTabGroup)
 * @param label The label for the combiner (default: "Parallelizer")
 * @return A pointer to the created DSP Parallelizer, or nullptr if an error occurs
 */
static dsp* createDSPParallelizer(dsp* dsp1, dsp* dsp2,
                                  std::string& error,
                                  Layout layout = Layout::kTabGroup,
                                  const std::string& label = "Parallelizer")
{
    return new dsp_parallelizer(dsp1, dsp2, 4096, layout, label);
}

/**
 * Create a DSP Splitter
 *
 * This method creates a DSP Splitter, which combines two 'compatible' DSP modules in a splitter configuration.
 * The outputs of the first DSP module are connected to the inputs of the second DSP module.
 *
 * @param dsp1 The first DSP module to combine
 * @param dsp2 The second DSP module to combine
 * @param error A reference to a string to store error messages (if any)
 * @param layout The layout for the combined user interface (default: kTabGroup)
 * @param label The label for the combiner (default: "Splitter")
 * @return A pointer to the created DSP Splitter, or nullptr if an error occurs
 */
static dsp* createDSPSplitter(dsp* dsp1, dsp* dsp2, std::string& error, Layout layout = Layout::kTabGroup, const std::string& label = "Splitter")
{
    if (dsp1->getNumOutputs() == 0) {
        error = "Connection error in dsp_splitter : the first expression has no outputs\n";
        return nullptr;
    } else if (dsp2->getNumInputs() == 0) {
        error = "Connection error in dsp_splitter : the second expression has no inputs\n";
        return nullptr;
    } else if (dsp2->getNumInputs() % dsp1->getNumOutputs() != 0) {
        std::stringstream error_aux;
        error_aux << "Connection error in dsp_splitter : the number of outputs (" << dsp1->getNumOutputs()
                  << ") of the first expression should be a divisor of the number of inputs ("
                  << dsp2->getNumInputs()
                  << ") of the second expression" << std::endl;
        error = error_aux.str();
        return nullptr;
    } else if (dsp2->getNumInputs() == dsp1->getNumOutputs()) {
        return new dsp_sequencer(dsp1, dsp2, 4096, layout, label);
    } else {
        return new dsp_splitter(dsp1, dsp2, 4096, layout, label);
    }
}

/**
 * Create a DSP Merger
 *
 * This method creates a DSP Merger, which combines two 'compatible' DSP modules in a merger configuration.
 * The outputs of the first DSP module are combined with the inputs of the second DSP module.
 *
 * @param dsp1 The first DSP module to combine
 * @param dsp2 The second DSP module to combine
 * @param error A reference to a string to store error messages (if any)
 * @param layout The layout for the combined user interface (default: kTabGroup)
 * @param label The label for the combiner (default: "Merger")
 * @return A pointer to the created DSP Merger, or nullptr if an error occurs
 */
static dsp* createDSPMerger(dsp* dsp1, dsp* dsp2,
                            std::string& error,
                            Layout layout = Layout::kTabGroup,
                            const std::string& label = "Merger")
{
    if (dsp1->getNumOutputs() == 0) {
        error = "Connection error in dsp_merger : the first expression has no outputs\n";
        return nullptr;
    } else if (dsp2->getNumInputs() == 0) {
        error = "Connection error in dsp_merger : the second expression has no inputs\n";
        return nullptr;
    } else if (dsp1->getNumOutputs() % dsp2->getNumInputs() != 0) {
        std::stringstream error_aux;
        error_aux << "Connection error in dsp_merger : the number of outputs (" << dsp1->getNumOutputs()
                  << ") of the first expression should be a multiple of the number of inputs ("
                  << dsp2->getNumInputs()
                  << ") of the second expression" << std::endl;
        error = error_aux.str();
        return nullptr;
    } else if (dsp2->getNumInputs() == dsp1->getNumOutputs()) {
        return new dsp_sequencer(dsp1, dsp2, 4096, layout, label);
    } else {
        return new dsp_merger(dsp1, dsp2, 4096, layout, label);
    }
}

/**
 * Create a DSP Recursiver
 *
 * This method creates a DSP Recursiver, which combines two 'compatible' DSP modules in a recursive way.
 * The outputs of each module are fed as inputs to the other module in a recursive manner.
 *
 * @param dsp1 The first DSP module to combine
 * @param dsp2 The second DSP module to combine
 * @param error A reference to a string to store error messages (if any)
 * @param layout The layout for the combined user interface (default: kTabGroup)
 * @param label The label for the combiner (default: "Recursiver")
 * @return A pointer to the created DSP Recursiver, or nullptr if an error occurs
 */
static dsp* createDSPRecursiver(dsp* dsp1, dsp* dsp2,
                                std::string& error,
                                Layout layout = Layout::kTabGroup,
                                const std::string& label = "Recursiver")
{
    if ((dsp2->getNumInputs() > dsp1->getNumOutputs()) || (dsp2->getNumOutputs() > dsp1->getNumInputs())) {
        std::stringstream error_aux;
        error_aux << "Connection error in : dsp_recursiver" << std::endl;
        if (dsp2->getNumInputs() > dsp1->getNumOutputs()) {
            error_aux << "The number of outputs " << dsp1->getNumOutputs()
                      << " of the first expression should be greater or equal to the number of inputs ("
                      << dsp2->getNumInputs()
                      << ") of the second expression" << std::endl;
        }
        if (dsp2->getNumOutputs() > dsp1->getNumInputs()) {
            error_aux << "The number of inputs " << dsp1->getNumInputs()
                      << " of the first expression should be greater or equal to the number of outputs ("
                      << dsp2->getNumOutputs()
                      << ") of the second expression" << std::endl;
        }
        error = error_aux.str();
        return nullptr;
    } else {
        return new dsp_recursiver(dsp1, dsp2, layout, label);
    }
}

/**
 * Create a DSP Crossfader
 *
 * This method creates a DSP Crossfader, which allows you to crossfade between two DSP modules.
 * The crossfade parameter (as a slider) controls the mix between the two modules' outputs.
 * When Crossfade = 1, the first DSP only is computed, when Crossfade = 0,
 * the second DSP only is computed, otherwise both DSPs are computed and mixed.
 *
 * @param dsp1 The first DSP module to combine
 * @param dsp2 The second DSP module to combine
 * @param error A reference to a string to store error messages (if any)
 * @param layout The layout for the combined user interface (default: kTabGroup)
 * @param label The label for the crossfade slider (default: "Crossfade")
 * @return A pointer to the created DSP Crossfader, or nullptr if an error occurs
 */
static dsp* createDSPCrossfader(dsp* dsp1, dsp* dsp2,
                                std::string& error,
                                Layout layout = Layout::kTabGroup,
                                const std::string& label = "Crossfade")
{
    if (dsp1->getNumInputs() != dsp2->getNumInputs()) {
        std::stringstream error_aux;
        error_aux << "Error in dsp_crossfader : the number of inputs ("
        << dsp1->getNumInputs() << ") of A "
        << "must be equal to the number of inputs (" << dsp2->getNumInputs() << ") of B" << std::endl;
        error = error_aux.str();
        return nullptr;
    } else if (dsp1->getNumOutputs() != dsp2->getNumOutputs()) {
        std::stringstream error_aux;
        error_aux << "Error in dsp_crossfader : the number of outputs ("
        << dsp1->getNumOutputs() << ") of A "
        << "must be equal to the number of outputs (" << dsp2->getNumOutputs() << ") of B" << std::endl;
        error = error_aux.str();
        return nullptr;
    } else {
        return new dsp_crossfader(dsp1, dsp2, layout, label);
    }
}

#endif

#endif
/************************** END dsp-combiner.h **************************/
/************************** BEGIN dsp-adapter.h *************************
 FAUST Architecture File
 Copyright (C) 2003-2022 GRAME, Centre National de Creation Musicale
 ---------------------------------------------------------------------
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation; either version 2.1 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 
 EXCEPTION : As a special exception, you may create a larger work
 that contains this FAUST architecture section and distribute
 that work under terms of your choice, so long as this FAUST
 architecture section is not modified.
 ************************************************************************/

#ifndef __dsp_adapter__
#define __dsp_adapter__

#ifndef _WIN32
#include <alloca.h>
#endif
#include <string.h>
#include <cmath>
#include <assert.h>
#include <stdio.h>


// Adapts a DSP for a different number of inputs/outputs
class dsp_adapter : public decorator_dsp {
    
    private:
    
        FAUSTFLOAT** fAdaptedInputs;
        FAUSTFLOAT** fAdaptedOutputs;
        int fHWInputs;
        int fHWOutputs;
        int fBufferSize;
    
        void adaptBuffers(FAUSTFLOAT** inputs, FAUSTFLOAT** outputs)
        {
            for (int i = 0; i < fHWInputs; i++) {
                fAdaptedInputs[i] = inputs[i];
            }
            for (int i = 0; i < fHWOutputs; i++) {
                fAdaptedOutputs[i] = outputs[i];
            }
        }
    
    public:
    
        dsp_adapter(dsp* dsp, int hw_inputs, int hw_outputs, int buffer_size):decorator_dsp(dsp)
        {
            fHWInputs = hw_inputs;
            fHWOutputs = hw_outputs;
            fBufferSize = buffer_size;
            
            fAdaptedInputs = new FAUSTFLOAT*[dsp->getNumInputs()];
            for (int i = 0; i < dsp->getNumInputs() - fHWInputs; i++) {
                fAdaptedInputs[i + fHWInputs] = new FAUSTFLOAT[buffer_size];
                memset(fAdaptedInputs[i + fHWInputs], 0, sizeof(FAUSTFLOAT) * buffer_size);
            }
            
            fAdaptedOutputs = new FAUSTFLOAT*[dsp->getNumOutputs()];
            for (int i = 0; i < dsp->getNumOutputs() - fHWOutputs; i++) {
                fAdaptedOutputs[i + fHWOutputs] = new FAUSTFLOAT[buffer_size];
                memset(fAdaptedOutputs[i + fHWOutputs], 0, sizeof(FAUSTFLOAT) * buffer_size);
            }
        }
    
        virtual ~dsp_adapter()
        {
            for (int i = 0; i < fDSP->getNumInputs() - fHWInputs; i++) {
                delete [] fAdaptedInputs[i + fHWInputs];
            }
            delete [] fAdaptedInputs;
            
            for (int i = 0; i < fDSP->getNumOutputs() - fHWOutputs; i++) {
                delete [] fAdaptedOutputs[i + fHWOutputs];
            }
            delete [] fAdaptedOutputs;
        }
    
        virtual int getNumInputs() { return fHWInputs; }
        virtual int getNumOutputs() { return fHWOutputs; }
    
        virtual dsp_adapter* clone() { return new dsp_adapter(fDSP->clone(), fHWInputs, fHWOutputs, fBufferSize); }
    
        virtual void compute(double date_usec, int count, FAUSTFLOAT** inputs, FAUSTFLOAT** outputs)
        {
            adaptBuffers(inputs, outputs);
            fDSP->compute(date_usec, count, fAdaptedInputs, fAdaptedOutputs);
        }
    
        virtual void compute(int count, FAUSTFLOAT** inputs, FAUSTFLOAT** outputs)
        {
            adaptBuffers(inputs, outputs);
            fDSP->compute(count, fAdaptedInputs, fAdaptedOutputs);
        }
};

// Adapts a DSP for a different sample size
template <typename REAL_INT, typename REAL_EXT>
class dsp_sample_adapter : public decorator_dsp {
    
    private:
    
        REAL_INT** fAdaptedInputs;
        REAL_INT** fAdaptedOutputs;
    
        void adaptInputBuffers(int count, FAUSTFLOAT** inputs)
        {
            for (int chan = 0; chan < fDSP->getNumInputs(); chan++) {
                for (int frame = 0; frame < count; frame++) {
                    fAdaptedInputs[chan][frame] = REAL_INT(reinterpret_cast<REAL_EXT**>(inputs)[chan][frame]);
                }
            }
        }
    
        void adaptOutputsBuffers(int count, FAUSTFLOAT** outputs)
        {
            for (int chan = 0; chan < fDSP->getNumOutputs(); chan++) {
                for (int frame = 0; frame < count; frame++) {
                    reinterpret_cast<REAL_EXT**>(outputs)[chan][frame] = REAL_EXT(fAdaptedOutputs[chan][frame]);
                }
            }
        }
    
    public:
    
        dsp_sample_adapter(dsp* dsp):decorator_dsp(dsp)
        {
            fAdaptedInputs = new REAL_INT*[dsp->getNumInputs()];
            for (int i = 0; i < dsp->getNumInputs(); i++) {
                fAdaptedInputs[i] = new REAL_INT[4096];
            }
            
            fAdaptedOutputs = new REAL_INT*[dsp->getNumOutputs()];
            for (int i = 0; i < dsp->getNumOutputs(); i++) {
                fAdaptedOutputs[i] = new REAL_INT[4096];
            }
        }
    
        virtual ~dsp_sample_adapter()
        {
            for (int i = 0; i < fDSP->getNumInputs(); i++) {
                delete [] fAdaptedInputs[i];
            }
            delete [] fAdaptedInputs;
            
            for (int i = 0; i < fDSP->getNumOutputs(); i++) {
                delete [] fAdaptedOutputs[i];
            }
            delete [] fAdaptedOutputs;
        }
    
        virtual dsp_sample_adapter* clone() { return new dsp_sample_adapter(fDSP->clone()); }
    
        virtual void compute(int count, FAUSTFLOAT** inputs, FAUSTFLOAT** outputs)
        {
            assert(count <= 4096);
            adaptInputBuffers(count, inputs);
            // DSP base class uses FAUSTFLOAT** type, so reinterpret_cast has to be used even if the real DSP uses REAL_INT
            fDSP->compute(count, reinterpret_cast<FAUSTFLOAT**>(fAdaptedInputs), reinterpret_cast<FAUSTFLOAT**>(fAdaptedOutputs));
            adaptOutputsBuffers(count, outputs);
        }
    
        virtual void compute(double date_usec, int count, FAUSTFLOAT** inputs, FAUSTFLOAT** outputs)
        {
            assert(count <= 4096);
            adaptInputBuffers(count, inputs);
            // DSP base class uses FAUSTFLOAT** type, so reinterpret_cast has to be used even if the real DSP uses REAL_INT
            fDSP->compute(date_usec, count, reinterpret_cast<FAUSTFLOAT**>(fAdaptedInputs), reinterpret_cast<FAUSTFLOAT**>(fAdaptedOutputs));
            adaptOutputsBuffers(count, outputs);
        }
};

// Template used to specialize double parameters expressed as NUM/DENOM
template <int NUM, int DENOM>
struct Double {
    static constexpr double value() { return double(NUM)/double(DENOM); }
};

// Base class for filters
template <class fVslider0, int fVslider1>
struct Filter {
    inline int getFactor() { return fVslider1; }
};

// Identity filter: copy input to output
template <class fVslider0, int fVslider1>
struct Identity : public Filter<fVslider0, fVslider1> {
    inline int getFactor() { return fVslider1; }
    
    inline void compute(int count, FAUSTFLOAT* input0, FAUSTFLOAT* output0)
    {
        memcpy(output0, input0, count * sizeof(FAUSTFLOAT));
    }
};

// Generated with process = fi.lowpass(3, ma.SR*hslider("FCFactor", 0.4, 0.4, 0.5, 0.01)/hslider("Factor", 2, 2, 8, 1));
template <class fVslider0, int fVslider1, typename REAL>
struct LowPass3 : public Filter<fVslider0, fVslider1> {
    
    REAL fVec0[2];
    REAL fRec1[2];
    REAL fRec0[3];
    
    inline REAL LowPass3_faustpower2_f(REAL value)
    {
        return (value * value);
    }
    
    LowPass3()
    {
        for (int l0 = 0; (l0 < 2); l0 = (l0 + 1)) {
            fVec0[l0] = 0.0;
        }
        for (int l1 = 0; (l1 < 2); l1 = (l1 + 1)) {
            fRec1[l1] = 0.0;
        }
        for (int l2 = 0; (l2 < 3); l2 = (l2 + 1)) {
            fRec0[l2] = 0.0;
        }
    }
    
    inline void compute(int count, FAUSTFLOAT* input0, FAUSTFLOAT* output0)
    {
        // Computed at template specialization time
        REAL fSlow0 = std::tan((3.1415926535897931 * (REAL(fVslider0::value()) / REAL(fVslider1))));
        REAL fSlow1 = (1.0 / fSlow0);
        REAL fSlow2 = (1.0 / (((fSlow1 + 1.0000000000000002) / fSlow0) + 1.0));
        REAL fSlow3 = (1.0 / (fSlow1 + 1.0));
        REAL fSlow4 = (1.0 - fSlow1);
        REAL fSlow5 = (((fSlow1 + -1.0000000000000002) / fSlow0) + 1.0);
        REAL fSlow6 = (2.0 * (1.0 - (1.0 / LowPass3_faustpower2_f(fSlow0))));
        // Computed at runtime
        for (int i = 0; (i < count); i = (i + 1)) {
            REAL fTemp0 = REAL(input0[i]);
            fVec0[0] = fTemp0;
            fRec1[0] = (0.0 - (fSlow3 * ((fSlow4 * fRec1[1]) - (fTemp0 + fVec0[1]))));
            fRec0[0] = (fRec1[0] - (fSlow2 * ((fSlow5 * fRec0[2]) + (fSlow6 * fRec0[1]))));
            output0[i] = FAUSTFLOAT((fSlow2 * (fRec0[2] + (fRec0[0] + (2.0 * fRec0[1])))));
            fVec0[1] = fVec0[0];
            fRec1[1] = fRec1[0];
            fRec0[2] = fRec0[1];
            fRec0[1] = fRec0[0];
        }
    }
};

// Generated with process = fi.lowpass(4, ma.SR*hslider("FCFactor", 0.4, 0.4, 0.5, 0.01)/hslider("Factor", 2, 2, 8, 1));
template <class fVslider0, int fVslider1, typename REAL>
struct LowPass4 : public Filter<fVslider0, fVslider1> {
    
    REAL fRec1[3];
    REAL fRec0[3];
    
    inline REAL LowPass4_faustpower2_f(REAL value)
    {
        return (value * value);
    }
    
    LowPass4()
    {
        for (int l0 = 0; (l0 < 3); l0 = (l0 + 1)) {
            fRec1[l0] = 0.0f;
        }
        for (int l1 = 0; (l1 < 3); l1 = (l1 + 1)) {
            fRec0[l1] = 0.0f;
        }
    }
    
    inline void compute(int count, FAUSTFLOAT* input0, FAUSTFLOAT* output0)
    {
        // Computed at template specialization time
        REAL fSlow0 = std::tan((3.1415926535897931 * (REAL(fVslider0::value()) / REAL(fVslider1))));
        REAL fSlow1 = (1.0 / fSlow0);
        REAL fSlow2 = (1.0 / (((fSlow1 + 0.76536686473017945) / fSlow0) + 1.0));
        REAL fSlow3 = (1.0 / (((fSlow1 + 1.8477590650225735) / fSlow0) + 1.0));
        REAL fSlow4 = (((fSlow1 + -1.8477590650225735) / fSlow0) + 1.0);
        REAL fSlow5 = (2.0 * (1.0 - (1.0 / LowPass4_faustpower2_f(fSlow0))));
        REAL fSlow6 = (((fSlow1 + -0.76536686473017945) / fSlow0) + 1.0);
        // Computed at runtime
        for (int i = 0; (i < count); i = (i + 1)) {
            fRec1[0] = (REAL(input0[i]) - (fSlow3 * ((fSlow4 * fRec1[2]) + (fSlow5 * fRec1[1]))));
            fRec0[0] = ((fSlow3 * (fRec1[2] + (fRec1[0] + (2.0 * fRec1[1])))) - (fSlow2 * ((fSlow6 * fRec0[2]) + (fSlow5 * fRec0[1]))));
            output0[i] = FAUSTFLOAT((fSlow2 * (fRec0[2] + (fRec0[0] + (2.0 * fRec0[1])))));
            fRec1[2] = fRec1[1];
            fRec1[1] = fRec1[0];
            fRec0[2] = fRec0[1];
            fRec0[1] = fRec0[0];
        }
    }
};

// Generated with process = fi.lowpass3e(ma.SR*hslider("FCFactor", 0.4, 0.4, 0.5, 0.01)/hslider("Factor", 2, 2, 8, 1));
template <class fVslider0, int fVslider1, typename REAL>
struct LowPass3e : public Filter<fVslider0, fVslider1> {
    
    REAL fRec1[3];
    REAL fVec0[2];
    REAL fRec0[2];
    
    inline REAL LowPass3e_faustpower2_f(REAL value)
    {
        return (value * value);
    }
    
    LowPass3e()
    {
        for (int l0 = 0; (l0 < 3); l0 = (l0 + 1)) {
            fRec1[l0] = 0.0;
        }
        for (int l1 = 0; (l1 < 2); l1 = (l1 + 1)) {
            fVec0[l1] = 0.0;
        }
        for (int l2 = 0; (l2 < 2); l2 = (l2 + 1)) {
            fRec0[l2] = 0.0;
        }
    }
    
    inline void compute(int count, FAUSTFLOAT* input0, FAUSTFLOAT* output0)
    {
        // Computed at template specialization time
        REAL fSlow0 = std::tan((3.1415926535897931 * (REAL(fVslider0::value()) / REAL(fVslider1))));
        REAL fSlow1 = (1.0 / fSlow0);
        REAL fSlow2 = (1.0 / (fSlow1 + 0.82244590899881598));
        REAL fSlow3 = (0.82244590899881598 - fSlow1);
        REAL fSlow4 = (1.0 / (((fSlow1 + 0.80263676416103003) / fSlow0) + 1.4122708937742039));
        REAL fSlow5 = LowPass3e_faustpower2_f(fSlow0);
        REAL fSlow6 = (0.019809144837788999 / fSlow5);
        REAL fSlow7 = (fSlow6 + 1.1615164189826961);
        REAL fSlow8 = (((fSlow1 + -0.80263676416103003) / fSlow0) + 1.4122708937742039);
        REAL fSlow9 = (2.0 * (1.4122708937742039 - (1.0 / fSlow5)));
        REAL fSlow10 = (2.0 * (1.1615164189826961 - fSlow6));
        // Computed at runtime
        for (int i = 0; (i < count); i = (i + 1)) {
            fRec1[0] = (REAL(input0[i]) - (fSlow4 * ((fSlow8 * fRec1[2]) + (fSlow9 * fRec1[1]))));
            REAL fTemp0 = (fSlow4 * (((fSlow7 * fRec1[0]) + (fSlow10 * fRec1[1])) + (fSlow7 * fRec1[2])));
            fVec0[0] = fTemp0;
            fRec0[0] = (0.0 - (fSlow2 * ((fSlow3 * fRec0[1]) - (fTemp0 + fVec0[1]))));
            output0[i] = FAUSTFLOAT(fRec0[0]);
            fRec1[2] = fRec1[1];
            fRec1[1] = fRec1[0];
            fVec0[1] = fVec0[0];
            fRec0[1] = fRec0[0];
        }
    }
};

// Generated with process = fi.lowpass6e(ma.SR*hslider("FCFactor", 0.4, 0.4, 0.5, 0.01)/hslider("Factor", 2, 2, 8, 1));
template <class fVslider0, int fVslider1, typename REAL>
struct LowPass6e : public Filter<fVslider0, fVslider1> {
    
    REAL fRec2[3];
    REAL fRec1[3];
    REAL fRec0[3];
    
    inline REAL LowPass6e_faustpower2_f(REAL value)
    {
        return (value * value);
    }
    
    LowPass6e()
    {
        for (int l0 = 0; (l0 < 3); l0 = (l0 + 1)) {
            fRec2[l0] = 0.0;
        }
        for (int l1 = 0; (l1 < 3); l1 = (l1 + 1)) {
            fRec1[l1] = 0.0;
        }
        for (int l2 = 0; (l2 < 3); l2 = (l2 + 1)) {
            fRec0[l2] = 0.0;
        }
    }
    
    inline void compute(int count, FAUSTFLOAT* input0, FAUSTFLOAT* output0)
    {
        // Computed at template specialization time
        REAL fSlow0 = std::tan((3.1415926535897931 * (REAL(fVslider0::value()) / REAL(fVslider1))));
        REAL fSlow1 = (1.0 / fSlow0);
        REAL fSlow2 = (1.0 / (((fSlow1 + 0.16840487111358901) / fSlow0) + 1.0693584077073119));
        REAL fSlow3 = LowPass6e_faustpower2_f(fSlow0);
        REAL fSlow4 = (1.0 / fSlow3);
        REAL fSlow5 = (fSlow4 + 53.536152954556727);
        REAL fSlow6 = (1.0 / (((fSlow1 + 0.51247864188914105) / fSlow0) + 0.68962136448467504));
        REAL fSlow7 = (fSlow4 + 7.6217312988706034);
        REAL fSlow8 = (1.0 / (((fSlow1 + 0.78241304682164503) / fSlow0) + 0.24529150870616001));
        REAL fSlow9 = (9.9999997054999994e-05 / fSlow3);
        REAL fSlow10 = (fSlow9 + 0.00043322720055500002);
        REAL fSlow11 = (((fSlow1 + -0.78241304682164503) / fSlow0) + 0.24529150870616001);
        REAL fSlow12 = (2.0 * (0.24529150870616001 - fSlow4));
        REAL fSlow13 = (2.0 * (0.00043322720055500002 - fSlow9));
        REAL fSlow14 = (((fSlow1 + -0.51247864188914105) / fSlow0) + 0.68962136448467504);
        REAL fSlow15 = (2.0 * (0.68962136448467504 - fSlow4));
        REAL fSlow16 = (2.0 * (7.6217312988706034 - fSlow4));
        REAL fSlow17 = (((fSlow1 + -0.16840487111358901) / fSlow0) + 1.0693584077073119);
        REAL fSlow18 = (2.0 * (1.0693584077073119 - fSlow4));
        REAL fSlow19 = (2.0 * (53.536152954556727 - fSlow4));
        // Computed at runtime
        for (int i = 0; (i < count); i = (i + 1)) {
            fRec2[0] = (REAL(input0[i]) - (fSlow8 * ((fSlow11 * fRec2[2]) + (fSlow12 * fRec2[1]))));
            fRec1[0] = ((fSlow8 * (((fSlow10 * fRec2[0]) + (fSlow13 * fRec2[1])) + (fSlow10 * fRec2[2]))) - (fSlow6 * ((fSlow14 * fRec1[2]) + (fSlow15 * fRec1[1]))));
            fRec0[0] = ((fSlow6 * (((fSlow7 * fRec1[0]) + (fSlow16 * fRec1[1])) + (fSlow7 * fRec1[2]))) - (fSlow2 * ((fSlow17 * fRec0[2]) + (fSlow18 * fRec0[1]))));
            output0[i] = FAUSTFLOAT((fSlow2 * (((fSlow5 * fRec0[0]) + (fSlow19 * fRec0[1])) + (fSlow5 * fRec0[2]))));
            fRec2[2] = fRec2[1];
            fRec2[1] = fRec2[0];
            fRec1[2] = fRec1[1];
            fRec1[1] = fRec1[0];
            fRec0[2] = fRec0[1];
            fRec0[1] = fRec0[0];
        }
    }
};

// A "si.bus(N)" like hard-coded class
struct dsp_bus : public dsp {
    
    int fChannels;
    int fSampleRate;
    
    dsp_bus(int channels):fChannels(channels), fSampleRate(-1)
    {}
    
    virtual int getNumInputs() { return fChannels; }
    virtual int getNumOutputs() { return fChannels; }
    
    virtual int getSampleRate() { return fSampleRate; }
    
    virtual void buildUserInterface(UI* ui_interface) {}
    virtual void init(int sample_rate)
    {
        //classInit(sample_rate);
        instanceInit(sample_rate);
    }
    
    virtual void instanceInit(int sample_rate)
    {
        fSampleRate = sample_rate;
        instanceConstants(sample_rate);
        instanceResetUserInterface();
        instanceClear();
    }
    
    virtual void instanceConstants(int sample_rate) {}
    virtual void instanceResetUserInterface() {}
    virtual void instanceClear() {}
    
    virtual dsp* clone() { return new dsp_bus(fChannels); }
    
    virtual void metadata(Meta* m) {}
    
    virtual void compute(int count, FAUSTFLOAT** inputs, FAUSTFLOAT** outputs)
    {
        for (int chan = 0; chan < fChannels; chan++) {
            memcpy(outputs[chan], inputs[chan], sizeof(FAUSTFLOAT) * count);
        }
    }
    
    virtual void compute(double /*date_usec*/, int count, FAUSTFLOAT** inputs, FAUSTFLOAT** outputs)
    {
        compute(count, inputs, outputs);
    }
    
};

// Base class for sample-rate adapter
template <typename FILTER>
class sr_sampler : public decorator_dsp {
    
    protected:
    
        std::vector<FILTER> fInputLowPass;
        std::vector<FILTER> fOutputLowPass;
    
        inline int getFactor() { return this->fOutputLowPass[0].getFactor(); }
    
    public:
    
        sr_sampler(dsp* dsp):decorator_dsp(dsp)
        {
            for (int chan = 0; chan < fDSP->getNumInputs(); chan++) {
                fInputLowPass.push_back(FILTER());
            }
            for (int chan = 0; chan < fDSP->getNumOutputs(); chan++) {
                fOutputLowPass.push_back(FILTER());
            }
        }
};

// Down sample-rate adapter
template <typename FILTER>
class dsp_down_sampler : public sr_sampler<FILTER> {
    
    public:
    
        dsp_down_sampler(dsp* dsp):sr_sampler<FILTER>(dsp)
        {}
    
        virtual void init(int sample_rate)
        {
            this->fDSP->init(sample_rate / this->getFactor());
        }
    
        virtual void instanceInit(int sample_rate)
        {
            this->fDSP->instanceInit(sample_rate / this->getFactor());
        }
    
        virtual void instanceConstants(int sample_rate)
        {
            this->fDSP->instanceConstants(sample_rate / this->getFactor());
        }
    
        virtual dsp_down_sampler* clone() { return new dsp_down_sampler(decorator_dsp::clone()); }
    
        virtual void compute(int count, FAUSTFLOAT** inputs, FAUSTFLOAT** outputs)
        {
            int real_count = count / this->getFactor();
            
            // Adapt inputs
            FAUSTFLOAT** fInputs = (FAUSTFLOAT**)alloca(this->fDSP->getNumInputs() * sizeof(FAUSTFLOAT*));
            for (int chan = 0; chan < this->fDSP->getNumInputs(); chan++) {
                // Lowpass filtering in place on 'inputs'
                this->fInputLowPass[chan].compute(count, inputs[chan], inputs[chan]);
                // Allocate fInputs with 'real_count' frames
                fInputs[chan] = (FAUSTFLOAT*)alloca(sizeof(FAUSTFLOAT) * real_count);
                // Decimate
                for (int frame = 0; frame < real_count; frame++) {
                    fInputs[chan][frame] = inputs[chan][frame * this->getFactor()];
                }
            }
            
            // Allocate fOutputs with 'real_count' frames
            FAUSTFLOAT** fOutputs = (FAUSTFLOAT**)alloca(this->fDSP->getNumOutputs() * sizeof(FAUSTFLOAT*));
            for (int chan = 0; chan < this->fDSP->getNumOutputs(); chan++) {
                fOutputs[chan] = (FAUSTFLOAT*)alloca(sizeof(FAUSTFLOAT) * real_count);
            }
            
            // Compute at lower rate
            this->fDSP->compute(real_count, fInputs, fOutputs);
            
            // Adapt outputs
            for (int chan = 0; chan < this->fDSP->getNumOutputs(); chan++) {
                // Puts zeros
                memset(outputs[chan], 0, sizeof(FAUSTFLOAT) * count);
                for (int frame = 0; frame < real_count; frame++) {
                    // Copy one sample every 'DownFactor'
                    // Apply volume
                    //outputs[chan][frame * this->getFactor()] = fOutputs[chan][frame] * this->getFactor();
                    outputs[chan][frame * this->getFactor()] = fOutputs[chan][frame];
                }
                // Lowpass filtering in place on 'outputs'
                this->fOutputLowPass[chan].compute(count, outputs[chan], outputs[chan]);
            }
        }
    
        virtual void compute(double /*date_usec*/, int count, FAUSTFLOAT** inputs, FAUSTFLOAT** outputs) { compute(count, inputs, outputs); }
};

// Up sample-rate adapter
template <typename FILTER>
class dsp_up_sampler : public sr_sampler<FILTER> {
    
    public:
    
        dsp_up_sampler(dsp* dsp):sr_sampler<FILTER>(dsp)
        {}
    
        virtual void init(int sample_rate)
        {
            this->fDSP->init(sample_rate * this->getFactor());
        }
    
        virtual void instanceInit(int sample_rate)
        {
            this->fDSP->instanceInit(sample_rate * this->getFactor());
        }
    
        virtual void instanceConstants(int sample_rate)
        {
            this->fDSP->instanceConstants(sample_rate * this->getFactor());
        }
    
        virtual dsp_up_sampler* clone() { return new dsp_up_sampler(decorator_dsp::clone()); }
    
        virtual void compute(int count, FAUSTFLOAT** inputs, FAUSTFLOAT** outputs)
        {
            int real_count = count * this->getFactor();
            
            // Adapt inputs
            FAUSTFLOAT** fInputs = (FAUSTFLOAT**)alloca(this->fDSP->getNumInputs() * sizeof(FAUSTFLOAT*));
            
            for (int chan = 0; chan < this->fDSP->getNumInputs(); chan++) {
                // Allocate fInputs with 'real_count' frames
                fInputs[chan] = (FAUSTFLOAT*)alloca(sizeof(FAUSTFLOAT) * real_count);
                // Puts zeros
                memset(fInputs[chan], 0, sizeof(FAUSTFLOAT) * real_count);
                for (int frame = 0; frame < count; frame++) {
                    // Copy one sample every 'UpFactor'
                    fInputs[chan][frame * this->getFactor()] = inputs[chan][frame];
                }
                // Lowpass filtering in place on 'fInputs'
                this->fInputLowPass[chan].compute(real_count, fInputs[chan], fInputs[chan]);
            }
            
            // Allocate fOutputs with 'real_count' frames
            FAUSTFLOAT** fOutputs = (FAUSTFLOAT**)alloca(this->fDSP->getNumOutputs() * sizeof(FAUSTFLOAT*));
            
            for (int chan = 0; chan < this->fDSP->getNumOutputs(); chan++) {
                fOutputs[chan] = (FAUSTFLOAT*)alloca(sizeof(FAUSTFLOAT) * real_count);
            }
            
            // Compute at upper rate
            this->fDSP->compute(real_count, fInputs, fOutputs);
            
            // Adapt outputs
            for (int chan = 0; chan < this->fDSP->getNumOutputs(); chan++) {
                // Lowpass filtering in place on 'fOutputs'
                this->fOutputLowPass[chan].compute(real_count, fOutputs[chan], fOutputs[chan]);
                // Decimate
                for (int frame = 0; frame < count; frame++) {
                    // Apply volume
                    //outputs[chan][frame] = fOutputs[chan][frame * this->getFactor()] * this->getFactor();
                    outputs[chan][frame] = fOutputs[chan][frame * this->getFactor()];
                }
            }
        }
    
        virtual void compute(double /*date_usec*/, int count, FAUSTFLOAT** inputs, FAUSTFLOAT** outputs) { compute(count, inputs, outputs); }
};

// Create a UP/DS + Filter adapted DSP
template <typename REAL>
dsp* createSRAdapter(dsp* DSP, std::string& error, int ds = 0, int us = 0, int filter = 0)
{
    if (ds >= 2) {
        switch (filter) {
            case 0:
                if (ds == 2) {
                    return new dsp_down_sampler<Identity<Double<1,1>, 2>>(DSP);
                } else if (ds == 3) {
                    return new dsp_down_sampler<Identity<Double<1,1>, 3>>(DSP);
                } else if (ds == 4) {
                    return new dsp_down_sampler<Identity<Double<1,1>, 4>>(DSP);
                } else if (ds == 8) {
                    return new dsp_down_sampler<Identity<Double<1,1>, 8>>(DSP);
                } else if (ds == 16) {
                    return new dsp_down_sampler<Identity<Double<1,1>, 16>>(DSP);
                } else if (ds == 32) {
                    return new dsp_down_sampler<Identity<Double<1,1>, 32>>(DSP);
                } else {
                    error = "ERROR : ds factor type must be in [2..32] range\n";
                    return nullptr;
                }
            case 1:
                if (ds == 2) {
                    return new dsp_down_sampler<LowPass3<Double<45,100>, 2, REAL>>(DSP);
                } else if (ds == 3) {
                    return new dsp_down_sampler<LowPass3<Double<45,100>, 3, REAL>>(DSP);
                } else if (ds == 4) {
                    return new dsp_down_sampler<LowPass3<Double<45,100>, 4, REAL>>(DSP);
                } else if (ds == 8) {
                    return new dsp_down_sampler<LowPass3<Double<45,100>, 8, REAL>>(DSP);
                } else if (ds == 16) {
                    return new dsp_down_sampler<LowPass3<Double<45,100>, 16, REAL>>(DSP);
                } else if (ds == 32) {
                    return new dsp_down_sampler<LowPass3<Double<45,100>, 32, REAL>>(DSP);
                } else {
                    error = "ERROR : ds factor type must be in [2..32] range\n";
                    return nullptr;
                }
            case 2:
                if (ds == 2) {
                    return new dsp_down_sampler<LowPass4<Double<45,100>, 2, REAL>>(DSP);
                } else if (ds == 3) {
                    return new dsp_down_sampler<LowPass4<Double<45,100>, 3, REAL>>(DSP);
                } else if (ds == 4) {
                    return new dsp_down_sampler<LowPass4<Double<45,100>, 4, REAL>>(DSP);
                } else if (ds == 8) {
                    return new dsp_down_sampler<LowPass4<Double<45,100>, 8, REAL>>(DSP);
                } else if (ds == 16) {
                    return new dsp_down_sampler<LowPass4<Double<45,100>, 16, REAL>>(DSP);
                } else if (ds == 32) {
                    return new dsp_down_sampler<LowPass4<Double<45,100>, 32, REAL>>(DSP);
                } else {
                    error = "ERROR : ds factor type must be in [2..32] range\n";
                    return nullptr;
                }
            case 3:
                if (ds == 2) {
                    return new dsp_down_sampler<LowPass3e<Double<45,100>, 2, REAL>>(DSP);
                } else if (ds == 3) {
                    return new dsp_down_sampler<LowPass3e<Double<45,100>, 3, REAL>>(DSP);
                } else if (ds == 4) {
                    return new dsp_down_sampler<LowPass3e<Double<45,100>, 4, REAL>>(DSP);
                } else if (ds == 8) {
                    return new dsp_down_sampler<LowPass3e<Double<45,100>, 8, REAL>>(DSP);
                } else if (ds == 16) {
                    return new dsp_down_sampler<LowPass3e<Double<45,100>, 16, REAL>>(DSP);
                } else if (ds == 32) {
                    return new dsp_down_sampler<LowPass3e<Double<45,100>, 32, REAL>>(DSP);
                } else {
                    error = "ERROR : ds factor type must be in [2..32] range\n";
                    return nullptr;
                }
            case 4:
                if (ds == 2) {
                    return new dsp_down_sampler<LowPass6e<Double<45,100>, 2, REAL>>(DSP);
                } else if (ds == 3) {
                    return new dsp_down_sampler<LowPass6e<Double<45,100>, 3, REAL>>(DSP);
                } else if (ds == 4) {
                    return new dsp_down_sampler<LowPass6e<Double<45,100>, 4, REAL>>(DSP);
                } else if (ds == 8) {
                    return new dsp_down_sampler<LowPass6e<Double<45,100>, 8, REAL>>(DSP);
                } else if (ds == 16) {
                    return new dsp_down_sampler<LowPass6e<Double<45,100>, 16, REAL>>(DSP);
                } else if (ds == 32) {
                    return new dsp_down_sampler<LowPass6e<Double<45,100>, 32, REAL>>(DSP);
                } else {
                    error = "ERROR : ds factor type must be in [2..32] range\n";
                    return nullptr;
                }
            default:
                error = "ERROR : filter type must be in [0..4] range\n";
                return nullptr;
        }
    } else if (us >= 2) {
        
        switch (filter) {
            case 0:
                if (us == 2) {
                    return new dsp_up_sampler<Identity<Double<1,1>, 2>>(DSP);
                } else if (us == 3) {
                    return new dsp_up_sampler<Identity<Double<1,1>, 3>>(DSP);
                } else if (us == 4) {
                    return new dsp_up_sampler<Identity<Double<1,1>, 4>>(DSP);
                } else if (us == 8) {
                    return new dsp_up_sampler<Identity<Double<1,1>, 8>>(DSP);
                } else if (us == 16) {
                    return new dsp_up_sampler<Identity<Double<1,1>, 16>>(DSP);
                } else if (us == 32) {
                    return new dsp_up_sampler<Identity<Double<1,1>, 32>>(DSP);
                } else {
                    error = "ERROR : us factor type must be in [2..32] range\n";
                    return nullptr;
                }
            case 1:
                if (us == 2) {
                    return new dsp_up_sampler<LowPass3<Double<45,100>, 2, REAL>>(DSP);
                } else if (us == 3) {
                    return new dsp_up_sampler<LowPass3<Double<45,100>, 3, REAL>>(DSP);
                } else if (us == 4) {
                    return new dsp_up_sampler<LowPass3<Double<45,100>, 4, REAL>>(DSP);
                } else if (us == 8) {
                    return new dsp_up_sampler<LowPass3<Double<45,100>, 8, REAL>>(DSP);
                } else if (us == 16) {
                    return new dsp_up_sampler<LowPass3<Double<45,100>, 16, REAL>>(DSP);
                } else if (us == 32) {
                    return new dsp_up_sampler<LowPass3<Double<45,100>, 32, REAL>>(DSP);
                } else {
                    error = "ERROR : us factor type must be in [2..32] range\n";
                    return nullptr;
                }
            case 2:
                if (us == 2) {
                    return new dsp_up_sampler<LowPass4<Double<45,100>, 2, REAL>>(DSP);
                } else if (us == 3) {
                    return new dsp_up_sampler<LowPass4<Double<45,100>, 3, REAL>>(DSP);
                } else if (us == 4) {
                    return new dsp_up_sampler<LowPass4<Double<45,100>, 4, REAL>>(DSP);
                } else if (us == 8) {
                    return new dsp_up_sampler<LowPass4<Double<45,100>, 8, REAL>>(DSP);
                } else if (us == 16) {
                    return new dsp_up_sampler<LowPass4<Double<45,100>, 16, REAL>>(DSP);
                } else if (us == 32) {
                    return new dsp_up_sampler<LowPass4<Double<45,100>, 32, REAL>>(DSP);
                } else {
                    error = "ERROR : us factor type must be in [2..32] range\n";
                    return nullptr;
                }
            case 3:
                if (us == 2) {
                    return new dsp_up_sampler<LowPass3e<Double<45,100>, 2, REAL>>(DSP);
                } else if (us == 3) {
                    return new dsp_up_sampler<LowPass3e<Double<45,100>, 3, REAL>>(DSP);
                } else if (us == 4) {
                    return new dsp_up_sampler<LowPass3e<Double<45,100>, 4, REAL>>(DSP);
                } else if (us == 8) {
                    return new dsp_up_sampler<LowPass3e<Double<45,100>, 8, REAL>>(DSP);
                } else if (us == 16) {
                    return new dsp_up_sampler<LowPass3e<Double<45,100>, 16, REAL>>(DSP);
                } else if (us == 32) {
                    return new dsp_up_sampler<LowPass3e<Double<45,100>, 32, REAL>>(DSP);
                } else {
                    error = "ERROR : us factor type must be in [2..32] range\n";
                    return nullptr;
                }
            case 4:
                if (us == 2) {
                    return new dsp_up_sampler<LowPass6e<Double<45,100>, 2, REAL>>(DSP);
                } else if (us == 3) {
                    return new dsp_up_sampler<LowPass6e<Double<45,100>, 3, REAL>>(DSP);
                } else if (us == 4) {
                    return new dsp_up_sampler<LowPass6e<Double<45,100>, 4, REAL>>(DSP);
                } else if (us == 8) {
                    return new dsp_up_sampler<LowPass6e<Double<45,100>, 8, REAL>>(DSP);
                } else if (us == 16) {
                    return new dsp_up_sampler<LowPass6e<Double<45,100>, 16, REAL>>(DSP);
                } else if (us == 32) {
                    return new dsp_up_sampler<LowPass6e<Double<45,100>, 32, REAL>>(DSP);
                } else {
                    error = "ERROR : us factor type must be in [2..32] range\n";
                    return nullptr;
                }
            default:
                error = "ERROR : filter type must be in [0..4] range\n";
                return nullptr;
        }
    } else {
        return DSP;
    }
}
    
#endif
/************************** END dsp-adapter.h **************************/
/************************** BEGIN misc.h *******************************
FAUST Architecture File
Copyright (C) 2003-2022 GRAME, Centre National de Creation Musicale
---------------------------------------------------------------------
This program is free software; you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation; either version 2.1 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

EXCEPTION : As a special exception, you may create a larger work
that contains this FAUST architecture section and distribute
that work under terms of your choice, so long as this FAUST
architecture section is not modified.
***************************************************************************/

#ifndef __misc__
#define __misc__

#include <algorithm>
#include <map>
#include <cstdlib>
#include <string.h>
#include <fstream>
#include <string>

/************************** BEGIN meta.h *******************************
 FAUST Architecture File
 Copyright (C) 2003-2022 GRAME, Centre National de Creation Musicale
 ---------------------------------------------------------------------
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation; either version 2.1 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 
 EXCEPTION : As a special exception, you may create a larger work
 that contains this FAUST architecture section and distribute
 that work under terms of your choice, so long as this FAUST
 architecture section is not modified.
 ************************************************************************/

#ifndef __meta__
#define __meta__


/**
 The base class of Meta handler to be used in dsp::metadata(Meta* m) method to retrieve (key, value) metadata.
 */
struct FAUST_API Meta {
    virtual ~Meta() {}
    virtual void declare(const char* key, const char* value) = 0;
};

#endif
/**************************  END  meta.h **************************/

struct MY_Meta : Meta, std::map<const char*, const char*>
{
    void declare(const char* key, const char* value) { (*this)[key] = value; }
};

static int lsr(int x, int n) { return int(((unsigned int)x) >> n); }

static int int2pow2(int x) { int r = 0; while ((1<<r) < x) r++; return r; }

static long lopt(char* argv[], const char* name, long def)
{
    for (int i = 0; argv[i]; i++) if (!strcmp(argv[i], name)) return std::atoi(argv[i+1]);
    return def;
}

static long lopt1(int argc, char* argv[], const char* longname, const char* shortname, long def)
{
    for (int i = 2; i < argc; i++) {
        if (strcmp(argv[i-1], shortname) == 0 || strcmp(argv[i-1], longname) == 0) {
            return atoi(argv[i]);
        }
    }
    return def;
}

static const char* lopts(char* argv[], const char* name, const char* def)
{
    for (int i = 0; argv[i]; i++) if (!strcmp(argv[i], name)) return argv[i+1];
    return def;
}

static const char* lopts1(int argc, char* argv[], const char* longname, const char* shortname, const char* def)
{
    for (int i = 2; i < argc; i++) {
        if (strcmp(argv[i-1], shortname) == 0 || strcmp(argv[i-1], longname) == 0) {
            return argv[i];
        }
    }
    return def;
}

static bool isopt(char* argv[], const char* name)
{
    for (int i = 0; argv[i]; i++) if (!strcmp(argv[i], name)) return true;
    return false;
}

static std::string pathToContent(const std::string& path)
{
    std::ifstream file(path.c_str(), std::ifstream::binary);
    
    file.seekg(0, file.end);
    int size = int(file.tellg());
    file.seekg(0, file.beg);
    
    // And allocate buffer to that a single line can be read...
    char* buffer = new char[size + 1];
    file.read(buffer, size);
    
    // Terminate the string
    buffer[size] = 0;
    std::string result = buffer;
    file.close();
    delete [] buffer;
    return result;
}

#endif

/**************************  END  misc.h **************************/
/************************** BEGIN SaveUI.h **************************
 FAUST Architecture File
 Copyright (C) 2003-2022 GRAME, Centre National de Creation Musicale
 ---------------------------------------------------------------------
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation; either version 2.1 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 
 EXCEPTION : As a special exception, you may create a larger work
 that contains this FAUST architecture section and distribute
 that work under terms of your choice, so long as this FAUST
 architecture section is not modified.
 ********************************************************************/

#ifndef FAUST_SAVEUI_H
#define FAUST_SAVEUI_H

/************************** BEGIN DecoratorUI.h **************************
 FAUST Architecture File
Copyright (C) 2003-2022 GRAME, Centre National de Creation Musicale
---------------------------------------------------------------------
This program is free software; you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation; either version 2.1 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

EXCEPTION : As a special exception, you may create a larger work
that contains this FAUST architecture section and distribute
that work under terms of your choice, so long as this FAUST
architecture section is not modified.
*************************************************************************/

#ifndef Decorator_UI_H
#define Decorator_UI_H


//----------------------------------------------------------------
//  Generic UI empty implementation
//----------------------------------------------------------------

class FAUST_API GenericUI : public UI
{
    
    public:
        
        GenericUI() {}
        virtual ~GenericUI() {}
        
        // -- widget's layouts
        virtual void openTabBox(const char* label) {}
        virtual void openHorizontalBox(const char* label) {}
        virtual void openVerticalBox(const char* label) {}
        virtual void closeBox() {}
        
        // -- active widgets
        virtual void addButton(const char* label, FAUSTFLOAT* zone) {}
        virtual void addCheckButton(const char* label, FAUSTFLOAT* zone) {}
        virtual void addVerticalSlider(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step) {}
        virtual void addHorizontalSlider(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step) {}
        virtual void addNumEntry(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step) {}
    
        // -- passive widgets
        virtual void addHorizontalBargraph(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT min, FAUSTFLOAT max) {}
        virtual void addVerticalBargraph(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT min, FAUSTFLOAT max) {}
    
        // -- soundfiles
        virtual void addSoundfile(const char* label, const char* soundpath, Soundfile** sf_zone) {}
    
        virtual void declare(FAUSTFLOAT* zone, const char* key, const char* val) {}
    
};

//----------------------------------------------------------------
//  Generic UI decorator
//----------------------------------------------------------------

class FAUST_API DecoratorUI : public UI
{
    
    protected:
        
        UI* fUI;
        
    public:
        
        DecoratorUI(UI* ui = 0):fUI(ui) {}
        virtual ~DecoratorUI() { delete fUI; }
        
        // -- widget's layouts
        virtual void openTabBox(const char* label)          { fUI->openTabBox(label); }
        virtual void openHorizontalBox(const char* label)   { fUI->openHorizontalBox(label); }
        virtual void openVerticalBox(const char* label)     { fUI->openVerticalBox(label); }
        virtual void closeBox()                             { fUI->closeBox(); }
        
        // -- active widgets
        virtual void addButton(const char* label, FAUSTFLOAT* zone)         { fUI->addButton(label, zone); }
        virtual void addCheckButton(const char* label, FAUSTFLOAT* zone)    { fUI->addCheckButton(label, zone); }
        virtual void addVerticalSlider(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step)
        { fUI->addVerticalSlider(label, zone, init, min, max, step); }
        virtual void addHorizontalSlider(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step)
        { fUI->addHorizontalSlider(label, zone, init, min, max, step); }
        virtual void addNumEntry(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step)
        { fUI->addNumEntry(label, zone, init, min, max, step); }
        
        // -- passive widgets
        virtual void addHorizontalBargraph(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT min, FAUSTFLOAT max)
        { fUI->addHorizontalBargraph(label, zone, min, max); }
        virtual void addVerticalBargraph(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT min, FAUSTFLOAT max)
        { fUI->addVerticalBargraph(label, zone, min, max); }
    
        // -- soundfiles
        virtual void addSoundfile(const char* label, const char* filename, Soundfile** sf_zone) { fUI->addSoundfile(label, filename, sf_zone); }
    
        virtual void declare(FAUSTFLOAT* zone, const char* key, const char* val) { fUI->declare(zone, key, val); }
    
};

// Defined here to simplify header #include inclusion 
class FAUST_API SoundUIInterface : public GenericUI {};

#endif
/**************************  END  DecoratorUI.h **************************/

// Base class to handle controllers state save/load

class SaveUI : public GenericUI {

    protected:
    
        struct SavedZone {
            FAUSTFLOAT* fZone;
            FAUSTFLOAT fCurrent;
            FAUSTFLOAT fInit;
            
            SavedZone():fZone(nullptr), fCurrent(FAUSTFLOAT(0)), fInit(FAUSTFLOAT(0))
            {}
            SavedZone(FAUSTFLOAT* zone, FAUSTFLOAT current, FAUSTFLOAT init)
            :fZone(zone), fCurrent(current), fInit(init)
            {
                *fZone = current;
            }
            ~SavedZone()
            {}
        };
        
        std::map<std::string, SavedZone> fName2Zone;
    
        virtual void addItem(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init) = 0;
    
    public:
    
        SaveUI() {}
        virtual ~SaveUI() {}
    
        void addButton(const char* label, FAUSTFLOAT* zone)
        {
            addItem(label, zone, FAUSTFLOAT(0));
        }
        void addCheckButton(const char* label, FAUSTFLOAT* zone)
        {
            addItem(label, zone, FAUSTFLOAT(0));
        }
        void addVerticalSlider(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step)
        {
            addItem(label, zone, init);
        }
        void addHorizontalSlider(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step)
        {
            addItem(label, zone, init);
        }
        void addNumEntry(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step)
        {
            addItem(label, zone, init);
        }

        void reset()
        {
            for (const auto& it : fName2Zone) {
                *it.second.fZone = it.second.fInit;
            }
        }
        
        void display()
        {
            for (const auto& it : fName2Zone) {
                std::cout << "SaveUI::display path = " << it.first << " value = " << *it.second.fZone << std::endl;
            }
        }
        
        void save()
        {
            for (auto& it : fName2Zone) {
                it.second.fCurrent = *it.second.fZone;
            }
        }
};

/*
 Save/load current value using the label, reset to init value
 */

class SaveLabelUI : public SaveUI {
    
    protected:
    
        void addItem(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init)
        {
            if (fName2Zone.find(label) != fName2Zone.end()) {
                FAUSTFLOAT current = fName2Zone[label].fCurrent;
                fName2Zone[label] = SavedZone(zone, current, init);
            } else {
                fName2Zone[label] = SavedZone(zone, init, init);
            }
        }
        
    public:
        
        SaveLabelUI() : SaveUI() {}
        virtual ~SaveLabelUI() {}        
   
};

/*
 Save/load current value using the complete path, reset to init value
*/

class SavePathUI : public SaveUI, public PathBuilder {
    
    protected:
    
        void openTabBox(const char* label) { pushLabel(label); }
        void openHorizontalBox(const char* label) { pushLabel(label);; }
        void openVerticalBox(const char* label) { pushLabel(label); }
        void closeBox() { popLabel(); };
    
        void addItem(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init)
        {
            std::string path = buildPath(label);
            if (fName2Zone.find(path) != fName2Zone.end()) {
                FAUSTFLOAT current = fName2Zone[path].fCurrent;
                fName2Zone[path] = SavedZone(zone, current, init);
            } else {
                fName2Zone[path] = SavedZone(zone, init, init);
            }
        }
   
    public:

        SavePathUI(): SaveUI() {}
        virtual ~SavePathUI() {}

};

#endif

/**************************  END  SaveUI.h **************************/

// Always included
/************************** BEGIN OSCUI.h **************************
 FAUST Architecture File
 Copyright (C) 2003-2022 GRAME, Centre National de Creation Musicale
 ---------------------------------------------------------------------
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation; either version 2.1 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 
 EXCEPTION : As a special exception, you may create a larger work
 that contains this FAUST architecture section and distribute
 that work under terms of your choice, so long as this FAUST
 architecture section is not modified.
 *********************************************************************/

#ifndef __OSCUI__
#define __OSCUI__

#include <vector>
#include <string>

/*

  Faust Project

  Copyright (C) 2011 Grame

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

  Grame Research Laboratory, 9 rue du Garet, 69001 Lyon - France
  research@grame.fr

*/

#ifndef __OSCControler__
#define __OSCControler__

#include <string>
/*

  Copyright (C) 2011 Grame

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

  Grame Research Laboratory, 9 rue du Garet, 69001 Lyon - France
  research@grame.fr

*/

#ifndef __FaustFactory__
#define __FaustFactory__

#include <stack>
#include <string>
#include <sstream>

/*

  Copyright (C) 2011 Grame

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

  Grame Research Laboratory, 9 rue du Garet, 69001 Lyon - France
  research@grame.fr

*/

#ifndef __FaustNode__
#define __FaustNode__

#include <string>
#include <vector>

/*

  Copyright (C) 2011 Grame

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

  Grame Research Laboratory, 9 rue du Garet, 69001 Lyon - France
  research@grame.fr

*/

#ifndef __MessageDriven__
#define __MessageDriven__

#include <string>
#include <vector>

/*

  Copyright (C) 2010  Grame

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

  Grame Research Laboratory, 9 rue du Garet, 69001 Lyon - France
  research@grame.fr

*/

#ifndef __MessageProcessor__
#define __MessageProcessor__

namespace oscfaust
{

class Message;
//--------------------------------------------------------------------------
/*!
	\brief an abstract class for objects able to process OSC messages	
*/
class MessageProcessor
{
	public:
		virtual		~MessageProcessor() {}
		virtual void processMessage( const Message* msg ) = 0;
};

} // end namespoace

#endif
/*

  Copyright (C) 2011 Grame

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

  Grame Research Laboratory, 9 rue du Garet, 69001 Lyon - France
  research@grame.fr

*/

#ifndef __smartpointer__
#define __smartpointer__

#include <cassert>

namespace oscfaust
{

/*!
\brief the base class for smart pointers implementation

	Any object that want to support smart pointers should
	inherit from the smartable class which provides reference counting
	and automatic delete when the reference count drops to zero.
*/
class smartable {
	private:
		unsigned 	refCount;		
	public:
		//! gives the reference count of the object
		unsigned refs() const         { return refCount; }
		//! addReference increments the ref count and checks for refCount overflow
		void addReference()           { refCount++; assert(refCount != 0); }
		//! removeReference delete the object when refCount is zero		
		void removeReference()		  { if (--refCount == 0) delete this; }
		
	protected:
		smartable() : refCount(0) {}
		smartable(const smartable&): refCount(0) {}
		//! destructor checks for non-zero refCount
		virtual ~smartable()    
        { 
            /* 
                See "Static SFaustNode create (const char* name, C* zone, C init, C min, C max, const char* prefix, GUI* ui)" comment.
                assert (refCount == 0); 
            */
        }
		smartable& operator=(const smartable&) { return *this; }
};

/*!
\brief the smart pointer implementation

	A smart pointer is in charge of maintaining the objects reference count 
	by the way of pointers operators overloading. It supports class 
	inheritance and conversion whenever possible.
\n	Instances of the SMARTP class are supposed to use \e smartable types (or at least
	objects that implements the \e addReference and \e removeReference
	methods in a consistent way).
*/
template<class T> class SMARTP {
	private:
		//! the actual pointer to the class
		T* fSmartPtr;

	public:
		//! an empty constructor - points to null
		SMARTP()	: fSmartPtr(0) {}
		//! build a smart pointer from a class pointer
		SMARTP(T* rawptr) : fSmartPtr(rawptr)              { if (fSmartPtr) fSmartPtr->addReference(); }
		//! build a smart pointer from an convertible class reference
		template<class T2> 
		SMARTP(const SMARTP<T2>& ptr) : fSmartPtr((T*)ptr) { if (fSmartPtr) fSmartPtr->addReference(); }
		//! build a smart pointer from another smart pointer reference
		SMARTP(const SMARTP& ptr) : fSmartPtr((T*)ptr)     { if (fSmartPtr) fSmartPtr->addReference(); }

		//! the smart pointer destructor: simply removes one reference count
		~SMARTP()  { if (fSmartPtr) fSmartPtr->removeReference(); }
		
		//! cast operator to retrieve the actual class pointer
		operator T*() const  { return fSmartPtr;	}

		//! '*' operator to access the actual class pointer
		T& operator*() const {
			// checks for null dereference
			assert (fSmartPtr != 0);
			return *fSmartPtr;
		}

		//! operator -> overloading to access the actual class pointer
		T* operator->() const	{ 
			// checks for null dereference
			assert (fSmartPtr != 0);
			return fSmartPtr;
		}

		//! operator = that moves the actual class pointer
		template <class T2>
		SMARTP& operator=(T2 p1_)	{ *this=(T*)p1_; return *this; }

		//! operator = that moves the actual class pointer
		SMARTP& operator=(T* p_)	{
			// check first that pointers differ
			if (fSmartPtr != p_) {
				// increments the ref count of the new pointer if not null
				if (p_ != 0) p_->addReference();
				// decrements the ref count of the old pointer if not null
				if (fSmartPtr != 0) fSmartPtr->removeReference();
				// and finally stores the new actual pointer
				fSmartPtr = p_;
			}
			return *this;
		}
		//! operator < to support SMARTP map with Visual C++
		bool operator<(const SMARTP<T>& p_)	const			  { return fSmartPtr < ((T *) p_); }
		//! operator = to support inherited class reference
		SMARTP& operator=(const SMARTP<T>& p_)                { return operator=((T *) p_); }
		//! dynamic cast support
		template<class T2> SMARTP& cast(T2* p_)               { return operator=(dynamic_cast<T*>(p_)); }
		//! dynamic cast support
		template<class T2> SMARTP& cast(const SMARTP<T2>& p_) { return operator=(dynamic_cast<T*>(p_)); }
};

}

#endif

namespace oscfaust
{

class Message;
class OSCRegexp;
class MessageDriven;
typedef class SMARTP<MessageDriven>	SMessageDriven;

//--------------------------------------------------------------------------
/*!
	\brief a base class for objects accepting OSC messages
	
	Message driven objects are hierarchically organized in a tree.
	They provides the necessary to dispatch an OSC message to its destination
	node, according to the message OSC address. 
	
	The principle of the dispatch is the following:
	- first the processMessage() method should be called on the top level node
	- next processMessage call propose 
*/
class MessageDriven : public MessageProcessor, public smartable
{
	std::string						fName;			///< the node name
	std::string						fOSCPrefix;		///< the node OSC address prefix (OSCAddress = fOSCPrefix + '/' + fName)
	std::vector<SMessageDriven>		fSubNodes;		///< the subnodes of the current node

	protected:
				 MessageDriven(const char *name, const char *oscprefix) : fName (name), fOSCPrefix(oscprefix) {}
		virtual ~MessageDriven() {}

	public:
		static SMessageDriven create(const char* name, const char *oscprefix)	{ return new MessageDriven(name, oscprefix); }

		/*!
			\brief OSC message processing method.
			\param msg the osc message to be processed
			The method should be called on the top level node.
		*/
		virtual void	processMessage(const Message* msg);

		/*!
			\brief propose an OSc message at a given hierarchy level.
			\param msg the osc message currently processed
			\param regexp a regular expression based on the osc address head
			\param addrTail the osc address tail
			
			The method first tries to match the regular expression with the object name. 
			When it matches:
			- it calls \c accept when \c addrTail is empty 
			- or it \c propose the message to its subnodes when \c addrTail is not empty. 
			  In this case a new \c regexp is computed with the head of \c addrTail and a new \c addrTail as well.
		*/
		virtual void	propose(const Message* msg, const OSCRegexp* regexp, const std::string& addrTail);

		/*!
			\brief accept an OSC message. 
			\param msg the osc message currently processed
			\return true when the message is processed by the node
			
			The method is called only for the destination nodes. The real message acceptance is the node 
			responsability and may depend on the message content.
		*/
		virtual bool	accept(const Message* msg);

		/*!
			\brief handler for the \c 'get' message
			\param ipdest the output message destination IP
			
			The \c 'get' message is supported by every node:
			- it is propagated to the subnodes until it reaches terminal nodes
			- a terminal node send its state on \c 'get' request to the IP address given as parameter.
			The \c get method is basically called by the accept method.
		*/
		virtual void	get(unsigned long ipdest) const;

		/*!
			\brief handler for the \c 'get' 'attribute' message
			\param ipdest the output message destination IP
			\param what the requested attribute
			
			The \c 'get' message is supported by every node:
			- it is propagated to the subnodes until it reaches terminal nodes
			- a terminal node send its state on \c 'get' request to the IP address given as parameter.
			The \c get method is basically called by the accept method.
		*/
		virtual void	get(unsigned long ipdest, const std::string& what) const {}

		void			add(SMessageDriven node)	{ fSubNodes.push_back (node); }
		const char*		getName() const				{ return fName.c_str(); }
		std::string		getOSCAddress() const;
		int				size() const				{ return (int)fSubNodes.size (); }
		
		const std::string&	name() const			{ return fName; }
		SMessageDriven	subnode(int i)              { return fSubNodes[i]; }
};

} // end namespoace

#endif
/*

  Copyright (C) 2011  Grame

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

  Grame Research Laboratory, 9 rue du Garet, 69001 Lyon - France
  research@grame.fr

*/


#ifndef __Message__
#define __Message__

#include <string>
#include <vector>

namespace oscfaust
{

class OSCStream;
template <typename T> class MsgParam;
class baseparam;
typedef SMARTP<baseparam>	Sbaseparam;

//--------------------------------------------------------------------------
/*!
	\brief base class of a message parameters
*/
class baseparam : public smartable
{
	public:
		virtual ~baseparam() {}

		/*!
		 \brief utility for parameter type checking
		*/
		template<typename X> bool isType() const { return dynamic_cast<const MsgParam<X>*> (this) != 0; }
		/*!
		 \brief utility for parameter convertion
		 \param errvalue the returned value when no conversion applies
		 \return the parameter value when the type matches
		*/
		template<typename X> X	value(X errvalue) const 
			{ const MsgParam<X>* o = dynamic_cast<const MsgParam<X>*> (this); return o ? o->getValue() : errvalue; }
		/*!
		 \brief utility for parameter comparison
		*/
		template<typename X> bool	equal(const baseparam& p) const 
			{ 
				const MsgParam<X>* a = dynamic_cast<const MsgParam<X>*> (this); 
				const MsgParam<X>* b = dynamic_cast<const MsgParam<X>*> (&p);
				return a && b && (a->getValue() == b->getValue());
			}
		/*!
		 \brief utility for parameter comparison
		*/
		bool operator==(const baseparam& p) const 
			{ 
				return equal<float>(p) || equal<int>(p) || equal<std::string>(p);
			}
		bool operator!=(const baseparam& p) const
			{ 
				return !equal<float>(p) && !equal<int>(p) && !equal<std::string>(p);
			}
			
		virtual SMARTP<baseparam> copy() const = 0;
};

//--------------------------------------------------------------------------
/*!
	\brief template for a message parameter
*/
template <typename T> class MsgParam : public baseparam
{
	T fParam;
	public:
				 MsgParam(T val) : fParam(val)	{}
		virtual ~MsgParam() {}
		
		T getValue() const { return fParam; }
		
		virtual Sbaseparam copy() const { return new MsgParam<T>(fParam); }
};

//--------------------------------------------------------------------------
/*!
	\brief a message description
	
	A message is composed of an address (actually an OSC address),
	a message string that may be viewed as a method name
	and a list of message parameters.
*/
class Message
{
    public:
        typedef SMARTP<baseparam>		argPtr;		///< a message argument ptr type
        typedef std::vector<argPtr>		argslist;	///< args list type

    private:
        unsigned long	fSrcIP;			///< the message source IP number
        std::string	fAddress;			///< the message osc destination address
        std::string	fAlias;             ///< the message alias osc destination address
        argslist	fArguments;			///< the message arguments

    public:
            /*!
                \brief an empty message constructor
            */
             Message() {}
            /*!
                \brief a message constructor
                \param address the message destination address
            */
            Message(const std::string& address) : fAddress(address), fAlias("") {}
             
            Message(const std::string& address, const std::string& alias) : fAddress(address), fAlias(alias) {}
            /*!
                \brief a message constructor
                \param address the message destination address
                \param args the message parameters
            */
            Message(const std::string& address, const argslist& args) 
                : fAddress(address), fArguments(args) {}
            /*!
                \brief a message constructor
                \param msg a message
            */
             Message(const Message& msg);
    virtual ~Message() {} //{ freed++; std::cout << "running messages: " << (allocated - freed) << std::endl; }

    /*!
        \brief adds a parameter to the message
        \param val the parameter
    */
    template <typename T> void add(T val)	{ fArguments.push_back(new MsgParam<T>(val)); }
    /*!
        \brief adds a float parameter to the message
        \param val the parameter value
    */
    void	add(float val)					{ add<float>(val); }
    
    /*!
     \brief adds a double parameter to the message
     \param val the parameter value
     */
    void	add(double val)					{ add<double>(val); }
    
    /*!
        \brief adds an int parameter to the message
        \param val the parameter value
    */
    void	add(int val)					{ add<int>(val); }
    
    /*!
        \brief adds a string parameter to the message
        \param val the parameter value
    */
    void	add(const std::string& val)		{ add<std::string>(val); }

    /*!
        \brief adds a parameter to the message
        \param val the parameter
    */
    void	add(argPtr val)                 { fArguments.push_back( val ); }

    /*!
        \brief sets the message address
        \param addr the address
    */
    void				setSrcIP(unsigned long addr)		{ fSrcIP = addr; }

    /*!
        \brief sets the message address
        \param addr the address
    */
    void				setAddress(const std::string& addr)		{ fAddress = addr; }
    /*!
        \brief print the message
        \param out the output stream
    */
    void				print(std::ostream& out) const;
    /*!
        \brief send the message to OSC
        \param out the OSC output stream
    */
    void				print(OSCStream& out) const;
    /*!
        \brief print message arguments
        \param out the OSC output stream
    */
    void				printArgs(OSCStream& out) const;

    /// \brief gives the message address
    const std::string&	address() const		{ return fAddress; }
    /// \brief gives the message alias
    const std::string&	alias() const		{ return fAlias; }
    /// \brief gives the message parameters list
    const argslist&		params() const		{ return fArguments; }
    /// \brief gives the message parameters list
    argslist&			params()			{ return fArguments; }
    /// \brief gives the message source IP 
    unsigned long		src() const			{ return fSrcIP; }
    /// \brief gives the message parameters count
    int					size() const		{ return (int)fArguments.size(); }

    bool operator == (const Message& other) const;	

    /*!
        \brief gives a message float parameter
        \param i the parameter index (0 <= i < size())
        \param val on output: the parameter value when the parameter type matches
        \return false when types don't match
    */
    bool	param(int i, float& val) const		{ val = params()[i]->value<float>(val); return params()[i]->isType<float>(); }
    
    /*!
     \brief gives a message double parameter
     \param i the parameter index (0 <= i < size())
     \param val on output: the parameter value when the parameter type matches
     \return false when types don't match
     */
    bool	param(int i, double& val) const		{ val = params()[i]->value<double>(val); return params()[i]->isType<double>(); }
    
    /*!
        \brief gives a message int parameter
        \param i the parameter index (0 <= i < size())
        \param val on output: the parameter value when the parameter type matches
        \return false when types don't match
    */
    bool	param(int i, int& val) const		{ val = params()[i]->value<int>(val); return params()[i]->isType<int>(); }
    /*!
        \brief gives a message int parameter
        \param i the parameter index (0 <= i < size())
        \param val on output: the parameter value when the parameter type matches
        \return false when types don't match
    */
    bool	param(int i, unsigned int& val) const		{ val = params()[i]->value<int>(val); return params()[i]->isType<int>(); }
    /*!
        \brief gives a message int parameter
        \param i the parameter index (0 <= i < size())
        \param val on output: the parameter value when the parameter type matches
        \return false when types don't match
        \note a boolean value is handled as integer
    */
    bool	param(int i, bool& val) const		{ int ival = 0; ival = params()[i]->value<int>(ival); val = ival!=0; return params()[i]->isType<int>(); }
    /*!
        \brief gives a message int parameter
        \param i the parameter index (0 <= i < size())
        \param val on output: the parameter value when the parameter type matches
        \return false when types don't match
    */
    bool	param(int i, long int& val) const	{ val = long(params()[i]->value<int>(val)); return params()[i]->isType<int>(); }
    /*!
        \brief gives a message string parameter
        \param i the parameter index (0 <= i < size())
        \param val on output: the parameter value when the parameter type matches
        \return false when types don't match
    */
    bool	param(int i, std::string& val) const { val = params()[i]->value<std::string>(val); return params()[i]->isType<std::string>(); }
};


} // end namespoace

#endif
/************************** BEGIN GUI.h **********************************
 FAUST Architecture File
 Copyright (C) 2003-2022 GRAME, Centre National de Creation Musicale
 ---------------------------------------------------------------------
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation; either version 2.1 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 
 EXCEPTION : As a special exception, you may create a larger work
 that contains this FAUST architecture section and distribute
 that work under terms of your choice, so long as this FAUST
 architecture section is not modified.
 *************************************************************************/

#ifndef __GUI_H__
#define __GUI_H__

#include <list>
#include <map>
#include <vector>
#include <assert.h>

#ifdef _WIN32
# pragma warning (disable: 4100)
#else
# pragma GCC diagnostic ignored "-Wunused-parameter"
#endif

/************************** BEGIN ValueConverter.h ********************
 FAUST Architecture File
 Copyright (C) 2003-2022 GRAME, Centre National de Creation Musicale
 ---------------------------------------------------------------------
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation; either version 2.1 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 
 EXCEPTION : As a special exception, you may create a larger work
 that contains this FAUST architecture section and distribute
 that work under terms of your choice, so long as this FAUST
 architecture section is not modified.
 ********************************************************************/

#ifndef __ValueConverter__
#define __ValueConverter__

/***************************************************************************************
 ValueConverter.h
 (GRAME, Copyright 2015-2019)
 
 Set of conversion objects used to map user interface values (for example a gui slider
 delivering values between 0 and 1) to faust values (for example a vslider between
 20 and 20000) using a log scale.
 
 -- Utilities
 
 Range(lo,hi) : clip a value x between lo and hi
 Interpolator(lo,hi,v1,v2) : Maps a value x between lo and hi to a value y between v1 and v2
 Interpolator3pt(lo,mi,hi,v1,vm,v2) : Map values between lo mid hi to values between v1 vm v2
 
 -- Value Converters
 
 ValueConverter::ui2faust(x)
 ValueConverter::faust2ui(x)
 
 -- ValueConverters used for sliders depending of the scale
 
 LinearValueConverter(umin, umax, fmin, fmax)
 LinearValueConverter2(lo, mi, hi, v1, vm, v2) using 2 segments
 LogValueConverter(umin, umax, fmin, fmax)
 ExpValueConverter(umin, umax, fmin, fmax)
 
 -- ValueConverters used for accelerometers based on 3 points
 
 AccUpConverter(amin, amid, amax, fmin, fmid, fmax)        -- curve 0
 AccDownConverter(amin, amid, amax, fmin, fmid, fmax)      -- curve 1
 AccUpDownConverter(amin, amid, amax, fmin, fmid, fmax)    -- curve 2
 AccDownUpConverter(amin, amid, amax, fmin, fmid, fmax)    -- curve 3
 
 -- lists of ZoneControl are used to implement accelerometers metadata for each axes
 
 ZoneControl(zone, valueConverter) : a zone with an accelerometer data converter
 
 -- ZoneReader are used to implement screencolor metadata
 
 ZoneReader(zone, valueConverter) : a zone with a data converter

****************************************************************************************/

#include <float.h>
#include <algorithm>    // std::max
#include <cmath>
#include <vector>
#include <assert.h>


//--------------------------------------------------------------------------------------
// Interpolator(lo,hi,v1,v2)
// Maps a value x between lo and hi to a value y between v1 and v2
// y = v1 + (x-lo)/(hi-lo)*(v2-v1)
// y = v1 + (x-lo) * coef           with coef = (v2-v1)/(hi-lo)
// y = v1 + x*coef - lo*coef
// y = v1 - lo*coef + x*coef
// y = offset + x*coef              with offset = v1 - lo*coef
//--------------------------------------------------------------------------------------
class FAUST_API Interpolator {
    
    private:

        //--------------------------------------------------------------------------------------
        // Range(lo,hi) clip a value between lo and hi
        //--------------------------------------------------------------------------------------
        struct Range
        {
            double fLo;
            double fHi;

            Range(double x, double y) : fLo(std::min<double>(x,y)), fHi(std::max<double>(x,y)) {}
            double operator()(double x) { return (x<fLo) ? fLo : (x>fHi) ? fHi : x; }
        };

        Range fRange;
        double fCoef;
        double fOffset;

    public:

        Interpolator(double lo, double hi, double v1, double v2) : fRange(lo,hi)
        {
            if (hi != lo) {
                // regular case
                fCoef = (v2-v1)/(hi-lo);
                fOffset = v1 - lo*fCoef;
            } else {
                // degenerate case, avoids division by zero
                fCoef = 0;
                fOffset = (v1+v2)/2;
            }
        }
        double operator()(double v)
        {
            double x = fRange(v);
            return  fOffset + x*fCoef;
        }

        void getLowHigh(double& amin, double& amax)
        {
            amin = fRange.fLo;
            amax = fRange.fHi;
        }
};

//--------------------------------------------------------------------------------------
// Interpolator3pt(lo,mi,hi,v1,vm,v2)
// Map values between lo mid hi to values between v1 vm v2
//--------------------------------------------------------------------------------------
class FAUST_API Interpolator3pt {

    private:

        Interpolator fSegment1;
        Interpolator fSegment2;
        double fMid;

    public:

        Interpolator3pt(double lo, double mi, double hi, double v1, double vm, double v2) :
            fSegment1(lo, mi, v1, vm),
            fSegment2(mi, hi, vm, v2),
            fMid(mi) {}
        double operator()(double x) { return  (x < fMid) ? fSegment1(x) : fSegment2(x); }

        void getMappingValues(double& amin, double& amid, double& amax)
        {
            fSegment1.getLowHigh(amin, amid);
            fSegment2.getLowHigh(amid, amax);
        }
};

//--------------------------------------------------------------------------------------
// Abstract ValueConverter class. Converts values between UI and Faust representations
//--------------------------------------------------------------------------------------
class FAUST_API ValueConverter {

    public:

        virtual ~ValueConverter() {}
        virtual double ui2faust(double x) { return x; };
        virtual double faust2ui(double x) { return x; };
};

//--------------------------------------------------------------------------------------
// A converter than can be updated
//--------------------------------------------------------------------------------------

class FAUST_API UpdatableValueConverter : public ValueConverter {
    
    protected:
        
        bool fActive;
        
    public:
        
        UpdatableValueConverter():fActive(true)
        {}
        virtual ~UpdatableValueConverter()
        {}
        
        virtual void setMappingValues(double amin, double amid, double amax, double min, double init, double max) = 0;
        virtual void getMappingValues(double& amin, double& amid, double& amax) = 0;
        
        void setActive(bool on_off) { fActive = on_off; }
        bool getActive() { return fActive; }
    
};

//--------------------------------------------------------------------------------------
// Linear conversion between ui and Faust values
//--------------------------------------------------------------------------------------
class FAUST_API LinearValueConverter : public ValueConverter {
    
    private:
        
        Interpolator fUI2F;
        Interpolator fF2UI;
        
    public:
        
        LinearValueConverter(double umin, double umax, double fmin, double fmax) :
            fUI2F(umin,umax,fmin,fmax), fF2UI(fmin,fmax,umin,umax)
        {}
        
        LinearValueConverter() : fUI2F(0.,0.,0.,0.), fF2UI(0.,0.,0.,0.)
        {}
        virtual double ui2faust(double x) { return fUI2F(x); }
        virtual double faust2ui(double x) { return fF2UI(x); }
    
};

//--------------------------------------------------------------------------------------
// Two segments linear conversion between ui and Faust values
//--------------------------------------------------------------------------------------
class FAUST_API LinearValueConverter2 : public UpdatableValueConverter {
    
    private:
    
        Interpolator3pt fUI2F;
        Interpolator3pt fF2UI;
        
    public:
    
        LinearValueConverter2(double amin, double amid, double amax, double min, double init, double max) :
            fUI2F(amin, amid, amax, min, init, max), fF2UI(min, init, max, amin, amid, amax)
        {}
        
        LinearValueConverter2() : fUI2F(0.,0.,0.,0.,0.,0.), fF2UI(0.,0.,0.,0.,0.,0.)
        {}
    
        virtual double ui2faust(double x) { return fUI2F(x); }
        virtual double faust2ui(double x) { return fF2UI(x); }
    
        virtual void setMappingValues(double amin, double amid, double amax, double min, double init, double max)
        {
            fUI2F = Interpolator3pt(amin, amid, amax, min, init, max);
            fF2UI = Interpolator3pt(min, init, max, amin, amid, amax);
        }

        virtual void getMappingValues(double& amin, double& amid, double& amax)
        {
            fUI2F.getMappingValues(amin, amid, amax);
        }
    
};

//--------------------------------------------------------------------------------------
// Logarithmic conversion between ui and Faust values
//--------------------------------------------------------------------------------------
class FAUST_API LogValueConverter : public LinearValueConverter {

    public:
    
        // We use DBL_EPSILON which is bigger than DBL_MIN (safer)
        LogValueConverter(double umin, double umax, double fmin, double fmax) :
            LinearValueConverter(umin, umax, std::log(std::max<double>(DBL_EPSILON, fmin)), std::log(std::max<double>(DBL_EPSILON, fmax)))
        {}

        virtual double ui2faust(double x) { return std::exp(LinearValueConverter::ui2faust(x)); }
        virtual double faust2ui(double x) { return LinearValueConverter::faust2ui(std::log(std::max<double>(DBL_EPSILON, x))); }

};

//--------------------------------------------------------------------------------------
// Exponential conversion between ui and Faust values
//--------------------------------------------------------------------------------------
class FAUST_API ExpValueConverter : public LinearValueConverter {

    public:

        ExpValueConverter(double umin, double umax, double fmin, double fmax) :
            LinearValueConverter(umin, umax, std::min<double>(DBL_MAX, std::exp(fmin)), std::min<double>(DBL_MAX, std::exp(fmax)))
        {}

        virtual double ui2faust(double x) { return std::log(LinearValueConverter::ui2faust(x)); }
        virtual double faust2ui(double x) { return LinearValueConverter::faust2ui(std::min<double>(DBL_MAX, std::exp(x))); }

};

//--------------------------------------------------------------------------------------
// Convert accelerometer or gyroscope values to Faust values
// Using an Up curve (curve 0)
//--------------------------------------------------------------------------------------
class FAUST_API AccUpConverter : public UpdatableValueConverter {

    private:

        Interpolator3pt fA2F;
        Interpolator3pt fF2A;

    public:

        AccUpConverter(double amin, double amid, double amax, double fmin, double fmid, double fmax) :
            fA2F(amin,amid,amax,fmin,fmid,fmax),
            fF2A(fmin,fmid,fmax,amin,amid,amax)
        {}

        virtual double ui2faust(double x) { return fA2F(x); }
        virtual double faust2ui(double x) { return fF2A(x); }

        virtual void setMappingValues(double amin, double amid, double amax, double fmin, double fmid, double fmax)
        {
            //__android_log_print(ANDROID_LOG_ERROR, "Faust", "AccUpConverter update %f %f %f %f %f %f", amin,amid,amax,fmin,fmid,fmax);
            fA2F = Interpolator3pt(amin, amid, amax, fmin, fmid, fmax);
            fF2A = Interpolator3pt(fmin, fmid, fmax, amin, amid, amax);
        }

        virtual void getMappingValues(double& amin, double& amid, double& amax)
        {
            fA2F.getMappingValues(amin, amid, amax);
        }

};

//--------------------------------------------------------------------------------------
// Convert accelerometer or gyroscope values to Faust values
// Using a Down curve (curve 1)
//--------------------------------------------------------------------------------------
class FAUST_API AccDownConverter : public UpdatableValueConverter {

    private:

        Interpolator3pt	fA2F;
        Interpolator3pt	fF2A;

    public:

        AccDownConverter(double amin, double amid, double amax, double fmin, double fmid, double fmax) :
            fA2F(amin,amid,amax,fmax,fmid,fmin),
            fF2A(fmin,fmid,fmax,amax,amid,amin)
        {}

        virtual double ui2faust(double x) { return fA2F(x); }
        virtual double faust2ui(double x) { return fF2A(x); }

        virtual void setMappingValues(double amin, double amid, double amax, double fmin, double fmid, double fmax)
        {
             //__android_log_print(ANDROID_LOG_ERROR, "Faust", "AccDownConverter update %f %f %f %f %f %f", amin,amid,amax,fmin,fmid,fmax);
            fA2F = Interpolator3pt(amin, amid, amax, fmax, fmid, fmin);
            fF2A = Interpolator3pt(fmin, fmid, fmax, amax, amid, amin);
        }

        virtual void getMappingValues(double& amin, double& amid, double& amax)
        {
            fA2F.getMappingValues(amin, amid, amax);
        }
};

//--------------------------------------------------------------------------------------
// Convert accelerometer or gyroscope values to Faust values
// Using an Up-Down curve (curve 2)
//--------------------------------------------------------------------------------------
class FAUST_API AccUpDownConverter : public UpdatableValueConverter {

    private:

        Interpolator3pt	fA2F;
        Interpolator fF2A;

    public:

        AccUpDownConverter(double amin, double amid, double amax, double fmin, double fmid, double fmax) :
            fA2F(amin,amid,amax,fmin,fmax,fmin),
            fF2A(fmin,fmax,amin,amax)				// Special, pseudo inverse of a non monotonic function
        {}

        virtual double ui2faust(double x) { return fA2F(x); }
        virtual double faust2ui(double x) { return fF2A(x); }

        virtual void setMappingValues(double amin, double amid, double amax, double fmin, double fmid, double fmax)
        {
            //__android_log_print(ANDROID_LOG_ERROR, "Faust", "AccUpDownConverter update %f %f %f %f %f %f", amin,amid,amax,fmin,fmid,fmax);
            fA2F = Interpolator3pt(amin, amid, amax, fmin, fmax, fmin);
            fF2A = Interpolator(fmin, fmax, amin, amax);
        }

        virtual void getMappingValues(double& amin, double& amid, double& amax)
        {
            fA2F.getMappingValues(amin, amid, amax);
        }
};

//--------------------------------------------------------------------------------------
// Convert accelerometer or gyroscope values to Faust values
// Using a Down-Up curve (curve 3)
//--------------------------------------------------------------------------------------
class FAUST_API AccDownUpConverter : public UpdatableValueConverter {

    private:

        Interpolator3pt	fA2F;
        Interpolator fF2A;

    public:

        AccDownUpConverter(double amin, double amid, double amax, double fmin, double fmid, double fmax) :
            fA2F(amin,amid,amax,fmax,fmin,fmax),
            fF2A(fmin,fmax,amin,amax)				// Special, pseudo inverse of a non monotonic function
        {}

        virtual double ui2faust(double x) { return fA2F(x); }
        virtual double faust2ui(double x) { return fF2A(x); }

        virtual void setMappingValues(double amin, double amid, double amax, double fmin, double fmid, double fmax)
        {
            //__android_log_print(ANDROID_LOG_ERROR, "Faust", "AccDownUpConverter update %f %f %f %f %f %f", amin,amid,amax,fmin,fmid,fmax);
            fA2F = Interpolator3pt(amin, amid, amax, fmax, fmin, fmax);
            fF2A = Interpolator(fmin, fmax, amin, amax);
        }

        virtual void getMappingValues(double& amin, double& amid, double& amax)
        {
            fA2F.getMappingValues(amin, amid, amax);
        }
};

//--------------------------------------------------------------------------------------
// Base class for ZoneControl
//--------------------------------------------------------------------------------------
class FAUST_API ZoneControl {

    protected:

        FAUSTFLOAT*	fZone;

    public:

        ZoneControl(FAUSTFLOAT* zone) : fZone(zone) {}
        virtual ~ZoneControl() {}

        virtual void update(double v) const {}

        virtual void setMappingValues(int curve, double amin, double amid, double amax, double min, double init, double max) {}
        virtual void getMappingValues(double& amin, double& amid, double& amax) {}

        FAUSTFLOAT* getZone() { return fZone; }

        virtual void setActive(bool on_off) {}
        virtual bool getActive() { return false; }

        virtual int getCurve() { return -1; }

};

//--------------------------------------------------------------------------------------
//  Useful to implement accelerometers metadata as a list of ZoneControl for each axes
//--------------------------------------------------------------------------------------
class FAUST_API ConverterZoneControl : public ZoneControl {

    protected:

        ValueConverter* fValueConverter;

    public:

        ConverterZoneControl(FAUSTFLOAT* zone, ValueConverter* converter) : ZoneControl(zone), fValueConverter(converter) {}
        virtual ~ConverterZoneControl() { delete fValueConverter; } // Assuming fValueConverter is not kept elsewhere...

        virtual void update(double v) const { *fZone = FAUSTFLOAT(fValueConverter->ui2faust(v)); }

        ValueConverter* getConverter() { return fValueConverter; }

};

//--------------------------------------------------------------------------------------
// Association of a zone and a four value converter, each one for each possible curve.
// Useful to implement accelerometers metadata as a list of ZoneControl for each axes
//--------------------------------------------------------------------------------------
class FAUST_API CurveZoneControl : public ZoneControl {

    private:

        std::vector<UpdatableValueConverter*> fValueConverters;
        int fCurve;

    public:

        CurveZoneControl(FAUSTFLOAT* zone, int curve, double amin, double amid, double amax, double min, double init, double max) : ZoneControl(zone), fCurve(0)
        {
            assert(curve >= 0 && curve <= 3);
            fValueConverters.push_back(new AccUpConverter(amin, amid, amax, min, init, max));
            fValueConverters.push_back(new AccDownConverter(amin, amid, amax, min, init, max));
            fValueConverters.push_back(new AccUpDownConverter(amin, amid, amax, min, init, max));
            fValueConverters.push_back(new AccDownUpConverter(amin, amid, amax, min, init, max));
            fCurve = curve;
        }
        virtual ~CurveZoneControl()
        {
            for (const auto& it : fValueConverters) { delete it; }
        }
        void update(double v) const { if (fValueConverters[fCurve]->getActive()) *fZone = FAUSTFLOAT(fValueConverters[fCurve]->ui2faust(v)); }

        void setMappingValues(int curve, double amin, double amid, double amax, double min, double init, double max)
        {
            fValueConverters[curve]->setMappingValues(amin, amid, amax, min, init, max);
            fCurve = curve;
        }

        void getMappingValues(double& amin, double& amid, double& amax)
        {
            fValueConverters[fCurve]->getMappingValues(amin, amid, amax);
        }

        void setActive(bool on_off)
        {
            for (const auto& it : fValueConverters) { it->setActive(on_off); }
        }

        int getCurve() { return fCurve; }
};

class FAUST_API ZoneReader {

    private:

        FAUSTFLOAT* fZone;
        Interpolator fInterpolator;

    public:

        ZoneReader(FAUSTFLOAT* zone, double lo, double hi) : fZone(zone), fInterpolator(lo, hi, 0, 255) {}

        virtual ~ZoneReader() {}

        int getValue()
        {
            return (fZone != nullptr) ? int(fInterpolator(*fZone)) : 127;
        }

};

#endif
/**************************  END  ValueConverter.h **************************/
/************************** BEGIN MetaDataUI.h **************************
 FAUST Architecture File
 Copyright (C) 2003-2022 GRAME, Centre National de Creation Musicale
 ---------------------------------------------------------------------
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation; either version 2.1 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 
 EXCEPTION : As a special exception, you may create a larger work
 that contains this FAUST architecture section and distribute
 that work under terms of your choice, so long as this FAUST
 architecture section is not modified.
 ************************************************************************/

#ifndef MetaData_UI_H
#define MetaData_UI_H

#ifndef FAUSTFLOAT
#define FAUSTFLOAT float
#endif

#include <map>
#include <set>
#include <string>
#include <string.h>
#include <assert.h>
#include <stdio.h> // We use the lighter fprintf code


static bool startWith(const std::string& str, const std::string& prefix)
{
    return (str.substr(0, prefix.size()) == prefix);
}

/**
 * Convert a dB value into a scale between 0 and 1 (following IEC standard ?)
 */
static FAUSTFLOAT dB2Scale(FAUSTFLOAT dB)
{
    FAUSTFLOAT scale = FAUSTFLOAT(1.0);
    
    /*if (dB < -70.0f)
     scale = 0.0f;
     else*/
    if (dB < FAUSTFLOAT(-60.0))
        scale = (dB + FAUSTFLOAT(70.0)) * FAUSTFLOAT(0.0025);
    else if (dB < FAUSTFLOAT(-50.0))
        scale = (dB + FAUSTFLOAT(60.0)) * FAUSTFLOAT(0.005) + FAUSTFLOAT(0.025);
    else if (dB < FAUSTFLOAT(-40.0))
        scale = (dB + FAUSTFLOAT(50.0)) * FAUSTFLOAT(0.0075) + FAUSTFLOAT(0.075);
    else if (dB < FAUSTFLOAT(-30.0))
        scale = (dB + FAUSTFLOAT(40.0)) * FAUSTFLOAT(0.015) + FAUSTFLOAT(0.15);
    else if (dB < FAUSTFLOAT(-20.0))
        scale = (dB + FAUSTFLOAT(30.0)) * FAUSTFLOAT(0.02) + FAUSTFLOAT(0.3);
    else if (dB < FAUSTFLOAT(-0.001) || dB > FAUSTFLOAT(0.001))  /* if (dB < 0.0) */
        scale = (dB + FAUSTFLOAT(20.0)) * FAUSTFLOAT(0.025) + FAUSTFLOAT(0.5);
    
    return scale;
}

/*******************************************************************************
 * MetaDataUI : Common class for MetaData handling
 ******************************************************************************/

//============================= BEGIN GROUP LABEL METADATA===========================
// Unlike widget's label, metadata inside group's label are not extracted directly by
// the Faust compiler. Therefore they must be extracted within the architecture file
//-----------------------------------------------------------------------------------

class MetaDataUI {
    
    protected:
        
        std::string                         fGroupTooltip;
        std::map<FAUSTFLOAT*, FAUSTFLOAT>   fGuiSize;            // map widget zone with widget size coef
        std::map<FAUSTFLOAT*, std::string>  fTooltip;            // map widget zone with tooltip strings
        std::map<FAUSTFLOAT*, std::string>  fUnit;               // map widget zone to unit string (i.e. "dB")
        std::map<FAUSTFLOAT*, std::string>  fRadioDescription;   // map zone to {'low':440; ...; 'hi':1000.0}
        std::map<FAUSTFLOAT*, std::string>  fMenuDescription;    // map zone to {'low':440; ...; 'hi':1000.0}
        std::set<FAUSTFLOAT*>               fKnobSet;            // set of widget zone to be knobs
        std::set<FAUSTFLOAT*>               fLedSet;             // set of widget zone to be LEDs
        std::set<FAUSTFLOAT*>               fNumSet;             // set of widget zone to be numerical bargraphs
        std::set<FAUSTFLOAT*>               fLogSet;             // set of widget zone having a log UI scale
        std::set<FAUSTFLOAT*>               fExpSet;             // set of widget zone having an exp UI scale
        std::set<FAUSTFLOAT*>               fHiddenSet;          // set of hidden widget zone
        
        void clearMetadata()
        {
            fGuiSize.clear();
            fTooltip.clear();
            fUnit.clear();
            fRadioDescription.clear();
            fMenuDescription.clear();
            fKnobSet.clear();
            fLedSet.clear();
            fNumSet.clear();
            fLogSet.clear();
            fExpSet.clear();
            fHiddenSet.clear();
            fGroupTooltip = "";
        }
        
        /**
         * rmWhiteSpaces(): Remove the leading and trailing white spaces of a string
         * (but not those in the middle of the string)
         */
        static std::string rmWhiteSpaces(const std::string& s)
        {
            size_t i = s.find_first_not_of(" \t");
            size_t j = s.find_last_not_of(" \t");
            if ((i != std::string::npos) && (j != std::string::npos)) {
                return s.substr(i, 1+j-i);
            } else {
                return "";
            }
        }
        
        /**
         * Format tooltip string by replacing some white spaces by
         * return characters so that line width doesn't exceed n.
         * Limitation : long words exceeding n are not cut.
         */
        std::string formatTooltip(int n, const std::string& tt)
        {
            std::string ss = tt;  // ss string we are going to format
            int lws = 0;          // last white space encountered
            int lri = 0;          // last return inserted
            for (int i = 0; i < (int)tt.size(); i++) {
                if (tt[i] == ' ') lws = i;
                if (((i-lri) >= n) && (lws > lri)) {
                    // insert return here
                    ss[lws] = '\n';
                    lri = lws;
                }
            }
            return ss;
        }
        
    public:
        
        virtual ~MetaDataUI()
        {}
        
        enum Scale {
            kLin,
            kLog,
            kExp
        };
        
        Scale getScale(FAUSTFLOAT* zone)
        {
            if (fLogSet.count(zone) > 0) return kLog;
            if (fExpSet.count(zone) > 0) return kExp;
            return kLin;
        }
        
        bool isKnob(FAUSTFLOAT* zone)
        {
            return fKnobSet.count(zone) > 0;
        }
        
        bool isRadio(FAUSTFLOAT* zone)
        {
            return fRadioDescription.count(zone) > 0;
        }
        
        bool isMenu(FAUSTFLOAT* zone)
        {
            return fMenuDescription.count(zone) > 0;
        }
        
        bool isLed(FAUSTFLOAT* zone)
        {
            return fLedSet.count(zone) > 0;
        }
        
        bool isNumerical(FAUSTFLOAT* zone)
        {
            return fNumSet.count(zone) > 0;
        }
        
        bool isHidden(FAUSTFLOAT* zone)
        {
            return fHiddenSet.count(zone) > 0;
        }
        
        /**
         * Extracts metadata from a label : 'vol [unit: dB]' -> 'vol' + metadata(unit=dB)
         */
        static void extractMetadata(const std::string& fulllabel, std::string& label, std::map<std::string, std::string>& metadata)
        {
            enum {kLabel, kEscape1, kEscape2, kEscape3, kKey, kValue};
            int state = kLabel; int deep = 0;
            std::string key, value;
            
            for (unsigned int i = 0; i < fulllabel.size(); i++) {
                char c = fulllabel[i];
                switch (state) {
                    case kLabel :
                        assert(deep == 0);
                        switch (c) {
                            case '\\' : state = kEscape1; break;
                            case '[' : state = kKey; deep++; break;
                            default : label += c;
                        }
                        break;
                        
                    case kEscape1:
                        label += c;
                        state = kLabel;
                        break;
                        
                    case kEscape2:
                        key += c;
                        state = kKey;
                        break;
                        
                    case kEscape3:
                        value += c;
                        state = kValue;
                        break;
                        
                    case kKey:
                        assert(deep > 0);
                        switch (c) {
                            case '\\':
                                state = kEscape2;
                                break;
                                
                            case '[':
                                deep++;
                                key += c;
                                break;
                                
                            case ':':
                                if (deep == 1) {
                                    state = kValue;
                                } else {
                                    key += c;
                                }
                                break;
                            case ']':
                                deep--;
                                if (deep < 1) {
                                    metadata[rmWhiteSpaces(key)] = "";
                                    state = kLabel;
                                    key = "";
                                    value = "";
                                } else {
                                    key += c;
                                }
                                break;
                            default : key += c;
                        }
                        break;
                        
                    case kValue:
                        assert(deep > 0);
                        switch (c) {
                            case '\\':
                                state = kEscape3;
                                break;
                                
                            case '[':
                                deep++;
                                value += c;
                                break;
                                
                            case ']':
                                deep--;
                                if (deep < 1) {
                                    metadata[rmWhiteSpaces(key)] = rmWhiteSpaces(value);
                                    state = kLabel;
                                    key = "";
                                    value = "";
                                } else {
                                    value += c;
                                }
                                break;
                            default : value += c;
                        }
                        break;
                        
                    default:
                        fprintf(stderr, "ERROR unrecognized state %d\n", state);
                }
            }
            label = rmWhiteSpaces(label);
        }
        
        /**
         * Analyses the widget zone metadata declarations and takes appropriate actions.
         */
        void declare(FAUSTFLOAT* zone, const char* key, const char* value)
        {
            if (zone == 0) {
                // special zone 0 means group metadata
                if (strcmp(key, "tooltip") == 0) {
                    // only group tooltip are currently implemented
                    fGroupTooltip = formatTooltip(30, value);
                } else if ((strcmp(key, "hidden") == 0) && (strcmp(value, "1") == 0)) {
                    fHiddenSet.insert(zone);
                }
            } else {
                if (strcmp(key, "size") == 0) {
                    fGuiSize[zone] = atof(value);
                }
                else if (strcmp(key, "tooltip") == 0) {
                    fTooltip[zone] = formatTooltip(30, value);
                }
                else if (strcmp(key, "unit") == 0) {
                    fUnit[zone] = value;
                }
                else if ((strcmp(key, "hidden") == 0) && (strcmp(value, "1") == 0)) {
                    fHiddenSet.insert(zone);
                }
                else if (strcmp(key, "scale") == 0) {
                    if (strcmp(value, "log") == 0) {
                        fLogSet.insert(zone);
                    } else if (strcmp(value, "exp") == 0) {
                        fExpSet.insert(zone);
                    }
                }
                else if (strcmp(key, "style") == 0) {
                    if (strcmp(value, "knob") == 0) {
                        fKnobSet.insert(zone);
                    } else if (strcmp(value, "led") == 0) {
                        fLedSet.insert(zone);
                    } else if (strcmp(value, "numerical") == 0) {
                        fNumSet.insert(zone);
                    } else {
                        const char* p = value;
                        if (parseWord(p, "radio")) {
                            fRadioDescription[zone] = std::string(p);
                        } else if (parseWord(p, "menu")) {
                            fMenuDescription[zone] = std::string(p);
                        }
                    }
                }
            }
        }
    
};

#endif
/**************************  END  MetaDataUI.h **************************/
/************************** BEGIN ring-buffer.h **************************/
/*
  Copyright (C) 2000 Paul Davis
  Copyright (C) 2003 Rohan Drape
  Copyright (C) 2016 GRAME (renaming for internal use)

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation; either version 2.1 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

  ISO/POSIX C version of Paul Davis's lock free ringbuffer C++ code.
  This is safe for the case of one read thread and one write thread.
*/

#ifndef __ring_buffer__
#define __ring_buffer__

#include <stdlib.h>
#include <string.h>

#ifdef WIN32
# pragma warning (disable: 4334)
#else
# pragma GCC diagnostic ignored "-Wunused-function"
#endif

typedef struct {
    char *buf;
    size_t len;
}
ringbuffer_data_t;

typedef struct {
    char *buf;
    volatile size_t write_ptr;
    volatile size_t read_ptr;
    size_t	size;
    size_t	size_mask;
    int	mlocked;
}
ringbuffer_t;

static ringbuffer_t *ringbuffer_create(size_t sz);
static void ringbuffer_free(ringbuffer_t *rb);
static void ringbuffer_get_read_vector(const ringbuffer_t *rb,
                                         ringbuffer_data_t *vec);
static void ringbuffer_get_write_vector(const ringbuffer_t *rb,
                                          ringbuffer_data_t *vec);
static size_t ringbuffer_read(ringbuffer_t *rb, char *dest, size_t cnt);
static size_t ringbuffer_peek(ringbuffer_t *rb, char *dest, size_t cnt);
static void ringbuffer_read_advance(ringbuffer_t *rb, size_t cnt);
static size_t ringbuffer_read_space(const ringbuffer_t *rb);
static int ringbuffer_mlock(ringbuffer_t *rb);
static void ringbuffer_reset(ringbuffer_t *rb);
static void ringbuffer_reset_size (ringbuffer_t * rb, size_t sz);
static size_t ringbuffer_write(ringbuffer_t *rb, const char *src,
                                 size_t cnt);
static void ringbuffer_write_advance(ringbuffer_t *rb, size_t cnt);
static size_t ringbuffer_write_space(const ringbuffer_t *rb);

/* Create a new ringbuffer to hold at least `sz' bytes of data. The
   actual buffer size is rounded up to the next power of two. */

static ringbuffer_t *
ringbuffer_create (size_t sz)
{
	size_t power_of_two;
	ringbuffer_t *rb;

	if ((rb = (ringbuffer_t *) malloc (sizeof (ringbuffer_t))) == NULL) {
		return NULL;
	}

	for (power_of_two = 1u; 1u << power_of_two < sz; power_of_two++);

	rb->size = 1u << power_of_two;
	rb->size_mask = rb->size;
	rb->size_mask -= 1;
	rb->write_ptr = 0;
	rb->read_ptr = 0;
	if ((rb->buf = (char *) malloc (rb->size)) == NULL) {
		free (rb);
		return NULL;
	}
	rb->mlocked = 0;

	return rb;
}

/* Free all data associated with the ringbuffer `rb'. */

static void
ringbuffer_free (ringbuffer_t * rb)
{
#ifdef USE_MLOCK
	if (rb->mlocked) {
		munlock (rb->buf, rb->size);
	}
#endif /* USE_MLOCK */
	free (rb->buf);
	free (rb);
}

/* Lock the data block of `rb' using the system call 'mlock'.  */

static int
ringbuffer_mlock (ringbuffer_t * rb)
{
#ifdef USE_MLOCK
	if (mlock (rb->buf, rb->size)) {
		return -1;
	}
#endif /* USE_MLOCK */
	rb->mlocked = 1;
	return 0;
}

/* Reset the read and write pointers to zero. This is not thread
   safe. */

static void
ringbuffer_reset (ringbuffer_t * rb)
{
	rb->read_ptr = 0;
	rb->write_ptr = 0;
    memset(rb->buf, 0, rb->size);
}

/* Reset the read and write pointers to zero. This is not thread
   safe. */

static void
ringbuffer_reset_size (ringbuffer_t * rb, size_t sz)
{
    rb->size = sz;
    rb->size_mask = rb->size;
    rb->size_mask -= 1;
    rb->read_ptr = 0;
    rb->write_ptr = 0;
}

/* Return the number of bytes available for reading. This is the
   number of bytes in front of the read pointer and behind the write
   pointer.  */

static size_t
ringbuffer_read_space (const ringbuffer_t * rb)
{
	size_t w, r;

	w = rb->write_ptr;
	r = rb->read_ptr;

	if (w > r) {
		return w - r;
	} else {
		return (w - r + rb->size) & rb->size_mask;
	}
}

/* Return the number of bytes available for writing. This is the
   number of bytes in front of the write pointer and behind the read
   pointer.  */

static size_t
ringbuffer_write_space (const ringbuffer_t * rb)
{
	size_t w, r;

	w = rb->write_ptr;
	r = rb->read_ptr;

	if (w > r) {
		return ((r - w + rb->size) & rb->size_mask) - 1;
	} else if (w < r) {
		return (r - w) - 1;
	} else {
		return rb->size - 1;
	}
}

/* The copying data reader. Copy at most `cnt' bytes from `rb' to
   `dest'.  Returns the actual number of bytes copied. */

static size_t
ringbuffer_read (ringbuffer_t * rb, char *dest, size_t cnt)
{
	size_t free_cnt;
	size_t cnt2;
	size_t to_read;
	size_t n1, n2;

	if ((free_cnt = ringbuffer_read_space (rb)) == 0) {
		return 0;
	}

	to_read = cnt > free_cnt ? free_cnt : cnt;

	cnt2 = rb->read_ptr + to_read;

	if (cnt2 > rb->size) {
		n1 = rb->size - rb->read_ptr;
		n2 = cnt2 & rb->size_mask;
	} else {
		n1 = to_read;
		n2 = 0;
	}

	memcpy (dest, &(rb->buf[rb->read_ptr]), n1);
	rb->read_ptr = (rb->read_ptr + n1) & rb->size_mask;

	if (n2) {
		memcpy (dest + n1, &(rb->buf[rb->read_ptr]), n2);
		rb->read_ptr = (rb->read_ptr + n2) & rb->size_mask;
	}

	return to_read;
}

/* The copying data reader w/o read pointer advance. Copy at most
   `cnt' bytes from `rb' to `dest'.  Returns the actual number of bytes
   copied. */

static size_t
ringbuffer_peek (ringbuffer_t * rb, char *dest, size_t cnt)
{
	size_t free_cnt;
	size_t cnt2;
	size_t to_read;
	size_t n1, n2;
	size_t tmp_read_ptr;

	tmp_read_ptr = rb->read_ptr;

	if ((free_cnt = ringbuffer_read_space (rb)) == 0) {
		return 0;
	}

	to_read = cnt > free_cnt ? free_cnt : cnt;

	cnt2 = tmp_read_ptr + to_read;

	if (cnt2 > rb->size) {
		n1 = rb->size - tmp_read_ptr;
		n2 = cnt2 & rb->size_mask;
	} else {
		n1 = to_read;
		n2 = 0;
	}

	memcpy (dest, &(rb->buf[tmp_read_ptr]), n1);
	tmp_read_ptr = (tmp_read_ptr + n1) & rb->size_mask;

	if (n2) {
		memcpy (dest + n1, &(rb->buf[tmp_read_ptr]), n2);
	}

	return to_read;
}

/* The copying data writer. Copy at most `cnt' bytes to `rb' from
   `src'.  Returns the actual number of bytes copied. */

static size_t
ringbuffer_write (ringbuffer_t * rb, const char *src, size_t cnt)
{
	size_t free_cnt;
	size_t cnt2;
	size_t to_write;
	size_t n1, n2;

	if ((free_cnt = ringbuffer_write_space (rb)) == 0) {
		return 0;
	}

	to_write = cnt > free_cnt ? free_cnt : cnt;

	cnt2 = rb->write_ptr + to_write;

	if (cnt2 > rb->size) {
		n1 = rb->size - rb->write_ptr;
		n2 = cnt2 & rb->size_mask;
	} else {
		n1 = to_write;
		n2 = 0;
	}

	memcpy (&(rb->buf[rb->write_ptr]), src, n1);
	rb->write_ptr = (rb->write_ptr + n1) & rb->size_mask;

	if (n2) {
		memcpy (&(rb->buf[rb->write_ptr]), src + n1, n2);
		rb->write_ptr = (rb->write_ptr + n2) & rb->size_mask;
	}

	return to_write;
}

/* Advance the read pointer `cnt' places. */

static void
ringbuffer_read_advance (ringbuffer_t * rb, size_t cnt)
{
	size_t tmp = (rb->read_ptr + cnt) & rb->size_mask;
	rb->read_ptr = tmp;
}

/* Advance the write pointer `cnt' places. */

static void
ringbuffer_write_advance (ringbuffer_t * rb, size_t cnt)
{
	size_t tmp = (rb->write_ptr + cnt) & rb->size_mask;
	rb->write_ptr = tmp;
}

/* The non-copying data reader. `vec' is an array of two places. Set
   the values at `vec' to hold the current readable data at `rb'. If
   the readable data is in one segment the second segment has zero
   length. */

static void
ringbuffer_get_read_vector (const ringbuffer_t * rb,
				 ringbuffer_data_t * vec)
{
	size_t free_cnt;
	size_t cnt2;
	size_t w, r;

	w = rb->write_ptr;
	r = rb->read_ptr;

	if (w > r) {
		free_cnt = w - r;
	} else {
		free_cnt = (w - r + rb->size) & rb->size_mask;
	}

	cnt2 = r + free_cnt;

	if (cnt2 > rb->size) {

		/* Two part vector: the rest of the buffer after the current write
		   ptr, plus some from the start of the buffer. */

		vec[0].buf = &(rb->buf[r]);
		vec[0].len = rb->size - r;
		vec[1].buf = rb->buf;
		vec[1].len = cnt2 & rb->size_mask;

	} else {

		/* Single part vector: just the rest of the buffer */

		vec[0].buf = &(rb->buf[r]);
		vec[0].len = free_cnt;
		vec[1].len = 0;
	}
}

/* The non-copying data writer. `vec' is an array of two places. Set
   the values at `vec' to hold the current writeable data at `rb'. If
   the writeable data is in one segment the second segment has zero
   length. */

static void
ringbuffer_get_write_vector (const ringbuffer_t * rb,
				  ringbuffer_data_t * vec)
{
	size_t free_cnt;
	size_t cnt2;
	size_t w, r;

	w = rb->write_ptr;
	r = rb->read_ptr;

	if (w > r) {
		free_cnt = ((r - w + rb->size) & rb->size_mask) - 1;
	} else if (w < r) {
		free_cnt = (r - w) - 1;
	} else {
		free_cnt = rb->size - 1;
	}

	cnt2 = w + free_cnt;

	if (cnt2 > rb->size) {

		/* Two part vector: the rest of the buffer after the current write
		   ptr, plus some from the start of the buffer. */

		vec[0].buf = &(rb->buf[w]);
		vec[0].len = rb->size - w;
		vec[1].buf = rb->buf;
		vec[1].len = cnt2 & rb->size_mask;
	} else {
		vec[0].buf = &(rb->buf[w]);
		vec[0].len = free_cnt;
		vec[1].len = 0;
	}
}

#endif // __ring_buffer__
/**************************  END  ring-buffer.h **************************/

/*******************************************************************************
 * GUI : Abstract Graphic User Interface
 * Provides additional mechanisms to synchronize widgets and zones. Widgets
 * should both reflect the value of a zone and allow to change this value.
 ******************************************************************************/

class uiItem;
class GUI;
struct clist;

typedef void (*uiCallback)(FAUSTFLOAT val, void* data);

/**
 * Base class for uiTypedItem: memory zones that can be grouped and synchronized, using an internal cache.
 */
struct uiItemBase
{
    
    uiItemBase(GUI* ui, FAUSTFLOAT* zone)
    {
        assert(ui);
        assert(zone);
    }
    
    virtual ~uiItemBase()
    {}
    
    /**
     * This method will be called when the value changes externally,
     * and will signal the new value to all linked uItem
     * when the value is different from the cached one.
     *
     * @param v - the new value
     */
    virtual void modifyZone(FAUSTFLOAT v) = 0;
    
    /**
     * This method will be called when the value changes externally,
     * and will signal the new value to all linked uItem
     * when the value is different from the cached one.
     *
     * @param date - the timestamp of the received value in usec
     * @param v - the new value
     */
    virtual void modifyZone(double date, FAUSTFLOAT v) {}
    
    /**
     * This method is called by the synchronisation mecanism and is expected
     * to 'reflect' the new value, by changing the Widget layout for instance,
     * or sending a message (OSC, MIDI...)
     */
    virtual void reflectZone() = 0;
    
    /**
     * Return the cached value.
     *
     * @return - the cached value
     */
    virtual double cache() = 0;
    
};

// Declared as 'static' to avoid code duplication at link time
static void deleteClist(clist* cl);

/**
 * A list containing all groupe uiItemBase objects.
 */
struct clist : public std::list<uiItemBase*>
{
    
    virtual ~clist()
    {
        deleteClist(this);
    }
        
};

static void createUiCallbackItem(GUI* ui, FAUSTFLOAT* zone, uiCallback foo, void* data);

typedef std::map<FAUSTFLOAT*, clist*> zmap;

typedef std::map<FAUSTFLOAT*, ringbuffer_t*> ztimedmap;

class GUI : public UI
{
		
    private:
     
        static std::list<GUI*> fGuiList;
        zmap fZoneMap;
        bool fStopped;
    
     public:
            
        GUI():fStopped(false)
        {	
            fGuiList.push_back(this);
        }
        
        virtual ~GUI() 
        {   
            // delete all items
            for (const auto& it : fZoneMap) {
                delete it.second;
            }
            // suppress 'this' in static fGuiList
            fGuiList.remove(this);
        }

        // -- registerZone(z,c) : zone management
        
        void registerZone(FAUSTFLOAT* z, uiItemBase* c)
        {
            if (fZoneMap.find(z) == fZoneMap.end()) fZoneMap[z] = new clist();
            fZoneMap[z]->push_back(c);
        }
    
        void updateZone(FAUSTFLOAT* z)
        {
            FAUSTFLOAT v = *z;
            clist* cl = fZoneMap[z];
            for (const auto& c : *cl) {
                if (c->cache() != v) c->reflectZone();
            }
        }
    
        void updateAllZones()
        {
            for (const auto& m : fZoneMap) {
                updateZone(m.first);
            }
        }
    
        static void updateAllGuis()
        {
            for (const auto& g : fGuiList) {
                g->updateAllZones();
            }
        }
    
        void addCallback(FAUSTFLOAT* zone, uiCallback foo, void* data)
        {
            createUiCallbackItem(this, zone, foo, data);
        }

        // Start event or message processing
        virtual bool run() { return false; };
        // Stop event or message processing
        virtual void stop() { fStopped = true; }
        bool stopped() { return fStopped; }
    
        // -- widget's layouts
        
        virtual void openTabBox(const char* label) {}
        virtual void openHorizontalBox(const char* label) {}
        virtual void openVerticalBox(const char* label) {}
        virtual void closeBox() {}
        
        // -- active widgets
        
        virtual void addButton(const char* label, FAUSTFLOAT* zone) {}
        virtual void addCheckButton(const char* label, FAUSTFLOAT* zone) {}
        virtual void addVerticalSlider(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step) {}
        virtual void addHorizontalSlider(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step) {}
        virtual void addNumEntry(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step) {}
    
        // -- passive widgets
        
        virtual void addHorizontalBargraph(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT min, FAUSTFLOAT max) {}
        virtual void addVerticalBargraph(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT min, FAUSTFLOAT max) {}
    
        // -- soundfiles
    
        virtual void addSoundfile(const char* label, const char* filename, Soundfile** sf_zone) {}
    
        // -- metadata declarations

        virtual void declare(FAUSTFLOAT*, const char*, const char*) {}
    
        // Static global for timed zones, shared between all UI that will set timed values
        static ztimedmap gTimedZoneMap;

};

/**
 * User Interface Item: abstract definition.
 */
template <typename REAL>
class uiTypedItemReal : public uiItemBase
{
    protected:
        
        GUI* fGUI;
        REAL* fZone;
        REAL fCache;
        
        uiTypedItemReal(GUI* ui, REAL* zone):uiItemBase(ui, static_cast<FAUSTFLOAT*>(zone)),
        fGUI(ui), fZone(zone), fCache(REAL(-123456.654321))
        {
            ui->registerZone(zone, this);
        }
        
    public:
        
        virtual ~uiTypedItemReal()
        {}
    
        void modifyZone(REAL v)
        {
            fCache = v;
            if (*fZone != v) {
                *fZone = v;
                fGUI->updateZone(fZone);
            }
        }
    
        double cache() { return fCache; }
    
};

class uiItem : public uiTypedItemReal<FAUSTFLOAT> {
    
    protected:
    
        uiItem(GUI* ui, FAUSTFLOAT* zone):uiTypedItemReal<FAUSTFLOAT>(ui, zone)
        {}

    public:

        virtual ~uiItem() 
        {}

		void modifyZone(FAUSTFLOAT v)
		{
			fCache = v;
			if (*fZone != v) {
				*fZone = v;
				fGUI->updateZone(fZone);
			}
		}

};

/**
 * Base class for items with a value converter.
 */
struct uiConverter {
    
    ValueConverter* fConverter;
    
    uiConverter(MetaDataUI::Scale scale, FAUSTFLOAT umin, FAUSTFLOAT umax, FAUSTFLOAT fmin, FAUSTFLOAT fmax)
    {
        // Select appropriate converter according to scale mode
        if (scale == MetaDataUI::kLog) {
            fConverter = new LogValueConverter(umin, umax, fmin, fmax);
        } else if (scale == MetaDataUI::kExp) {
            fConverter = new ExpValueConverter(umin, umax, fmin, fmax);
        } else {
            fConverter = new LinearValueConverter(umin, umax, fmin, fmax);
        }
    }
    
    virtual ~uiConverter()
    {
        delete fConverter;
    }
};

/**
 * User Interface item owned (and so deleted) by external code.
 */
class uiOwnedItem : public uiItem {
    
    protected:
    
        uiOwnedItem(GUI* ui, FAUSTFLOAT* zone):uiItem(ui, zone)
        {}
    
     public:
    
        virtual ~uiOwnedItem()
        {}
    
        virtual void reflectZone() {}
};

/**
 * Callback Item.
 */
class uiCallbackItem : public uiItem {
    
    protected:
    
        uiCallback fCallback;
        void* fData;
    
    public:
    
        uiCallbackItem(GUI* ui, FAUSTFLOAT* zone, uiCallback foo, void* data)
        : uiItem(ui, zone), fCallback(foo), fData(data) {}
        
        virtual void reflectZone() 
        {		
            FAUSTFLOAT v = *fZone;
            fCache = v; 
            fCallback(v, fData);	
        }
};

/**
 *  For timestamped control.
 */
struct DatedControl {
    
    double fDate;
    FAUSTFLOAT fValue;
    
    DatedControl(double d = 0., FAUSTFLOAT v = FAUSTFLOAT(0)):fDate(d), fValue(v) {}
    
};

/**
 * Base class for timed items.
 */
class uiTimedItem : public uiItem
{
    
    protected:
        
        bool fDelete;
        
    public:
    
        using uiItem::modifyZone;
        
        uiTimedItem(GUI* ui, FAUSTFLOAT* zone):uiItem(ui, zone)
        {
            if (GUI::gTimedZoneMap.find(fZone) == GUI::gTimedZoneMap.end()) {
                GUI::gTimedZoneMap[fZone] = ringbuffer_create(8192);
                fDelete = true;
            } else {
                fDelete = false;
            }
        }
        
        virtual ~uiTimedItem()
        {
            ztimedmap::iterator it;
            if (fDelete && ((it = GUI::gTimedZoneMap.find(fZone)) != GUI::gTimedZoneMap.end())) {
                ringbuffer_free((*it).second);
                GUI::gTimedZoneMap.erase(it);
            }
        }
        
        virtual void modifyZone(double date, FAUSTFLOAT v)
        {
            size_t res;
            DatedControl dated_val(date, v);
            if ((res = ringbuffer_write(GUI::gTimedZoneMap[fZone], (const char*)&dated_val, sizeof(DatedControl))) != sizeof(DatedControl)) {
                fprintf(stderr, "ringbuffer_write error DatedControl\n");
            }
        }
    
};

/**
 * Allows to group a set of zones.
 */
class uiGroupItem : public uiItem
{
    protected:
    
        std::vector<FAUSTFLOAT*> fZoneMap;

    public:
    
        uiGroupItem(GUI* ui, FAUSTFLOAT* zone):uiItem(ui, zone)
        {}
        virtual ~uiGroupItem() 
        {}
        
        virtual void reflectZone() 
        {
            FAUSTFLOAT v = *fZone;
            fCache = v;
            
            // Update all zones of the same group
            for (const auto& it : fZoneMap) {
                *it = v;
            }
        }
        
        void addZone(FAUSTFLOAT* zone) { fZoneMap.push_back(zone); }

};

// Cannot be defined as method in the classes.

static void createUiCallbackItem(GUI* ui, FAUSTFLOAT* zone, uiCallback foo, void* data)
{
    new uiCallbackItem(ui, zone, foo, data);
}

static void deleteClist(clist* cl)
{
    for (const auto& it : *cl) {
        // This specific code is only used in JUCE context. TODO: use proper 'shared_ptr' based memory management.
    #if defined(JUCE_32BIT) || defined(JUCE_64BIT)
        uiOwnedItem* owned = dynamic_cast<uiOwnedItem*>(it);
        // owned items are deleted by external code
        if (!owned) {
            delete it;
        }
    #else
        delete it;
    #endif
    }
}

#endif
/**************************  END  GUI.h **************************/
/*

  Copyright (C) 2011 Grame

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

  Grame Research Laboratory, 9 rue du Garet, 69001 Lyon - France
  research@grame.fr

*/

#ifndef __RootNode__
#define __RootNode__

#include <map>
#include <string>
#include <vector>

/************************** BEGIN JSONUI.h *****************************
 FAUST Architecture File
 Copyright (C) 2003-2022 GRAME, Centre National de Creation Musicale
 ---------------------------------------------------------------------
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation; either version 2.1 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 
 EXCEPTION : As a special exception, you may create a larger work
 that contains this FAUST architecture section and distribute
 that work under terms of your choice, so long as this FAUST
 architecture section is not modified.
 ************************************************************************/

#ifndef FAUST_JSONUI_H
#define FAUST_JSONUI_H

#include <vector>
#include <map>
#include <string>
#include <iomanip>
#include <sstream>
#include <algorithm>
#include <limits>


/*******************************************************************************
 * JSONUI : Faust User Interface
 * This class produce a complete JSON decription of the DSP instance.
 *
 * Since 'shortname' can only be computed when all paths have been created,
 * the fAllUI vector is progressively filled with partially built UI items,
 * which are finally created in the JSON(...) method.
 ******************************************************************************/

// Instruction complexity statistics
struct InstComplexity {
    
    int fLoad = 0;
    int fStore = 0;
    int fBinop = 0;
    int fMathop = 0;
    int fNumbers = 0;
    int fDeclare = 0;
    int fCast = 0;
    int fSelect = 0;
    int fLoop = 0;
    
    std::map<std::string, int> fFunctionSymbolTable;
    std::map<std::string, int> fBinopSymbolTable;
   
    InstComplexity operator+(const InstComplexity& icomp)
    {
        fLoad += icomp.fLoad;
        fStore += icomp.fStore;
        fBinop += icomp.fBinop;
        fMathop += icomp.fMathop;
        fNumbers += icomp.fNumbers;
        fDeclare += icomp.fDeclare;
        fCast += icomp.fCast;
        fSelect += icomp.fSelect;
        fLoop += icomp.fLoop;
        return *this;
    }
};

// DSP or field name, type, size, size-in-bytes, reads, writes
typedef std::tuple<std::string, std::string, int, int, int, int> MemoryLayoutItem;
typedef std::vector<MemoryLayoutItem> MemoryLayoutType;
typedef std::map<std::string, int> PathTableType;

/*
    Build a JSON description of the DSP.
 */
template <typename REAL>
class FAUST_API JSONUIReal : public PathBuilder, public Meta, public UIReal<REAL> {

    protected:
    
        std::stringstream fUI;
        std::vector<std::string> fAllUI;
        std::stringstream fMeta;
        std::vector<std::pair <std::string, std::string> > fMetaAux;
        std::string fVersion;           // Compiler version
        std::string fCompileOptions;    // Compilation options
        std::vector<std::string> fLibraryList;
        std::vector<std::string> fIncludePathnames;
        std::string fName;
        std::string fFileName;
        std::string fExpandedCode;
        std::string fSHAKey;
        std::string fJSON;
        int fDSPSize;                   // In bytes
        PathTableType fPathTable;
        MemoryLayoutType fMemoryLayout;
        InstComplexity fIComp;
        bool fExtended;
    
        char fCloseUIPar;
        char fCloseMetaPar;
        int fTab;
    
        int fInputs, fOutputs, fSRIndex;
         
        void tab(int n, std::ostream& fout)
        {
            fout << '\n';
            while (n-- > 0) {
                fout << '\t';
            }
        }
    
        std::string flatten(const std::string& src)
        {
            std::string dst;
            for (size_t i = 0; i < src.size(); i++) {
                switch (src[i]) {
                    case '\n':
                    case '\t':
                        break;
                    default:
                        dst += src[i];
                        break;
                }
            }
            return dst;
        }
    
        void addMeta(int tab_val, bool quote = true)
        {
            if (fMetaAux.size() > 0) {
                tab(tab_val, fUI); fUI << "\"meta\": [";
                std::string sep = "";
                for (size_t i = 0; i < fMetaAux.size(); i++) {
                    fUI << sep;
                    tab(tab_val + 1, fUI); fUI << "{ \"" << fMetaAux[i].first << "\": \"" << fMetaAux[i].second << "\" }";
                    sep = ",";
                }
                tab(tab_val, fUI); fUI << ((quote) ? "],": "]");
                fMetaAux.clear();
            }
        }
    
        int getAddressIndex(const std::string& path)
        {
            return (fPathTable.find(path) != fPathTable.end()) ? fPathTable[path] : -1;
        }
      
     public:
     
        JSONUIReal(const std::string& name,
                  const std::string& filename,
                  int inputs,
                  int outputs,
                  int sr_index,
                  const std::string& sha_key,
                  const std::string& dsp_code,
                  const std::string& version,
                  const std::string& compile_options,
                  const std::vector<std::string>& library_list,
                  const std::vector<std::string>& include_pathnames,
                  int size,
                  const PathTableType& path_table,
                  MemoryLayoutType memory_layout,
                  InstComplexity inst_comp)
        {
            init(name, filename, inputs, outputs, sr_index, sha_key, dsp_code, version, compile_options, library_list, include_pathnames, size, path_table, memory_layout, inst_comp);
        }

        JSONUIReal(const std::string& name, const std::string& filename, int inputs, int outputs)
        {
            init(name, filename, inputs, outputs, -1, "", "", "", "", std::vector<std::string>(), std::vector<std::string>(), -1, PathTableType(), MemoryLayoutType(), InstComplexity());
        }

        JSONUIReal(int inputs, int outputs)
        {
            init("", "", inputs, outputs, -1, "", "","", "", std::vector<std::string>(), std::vector<std::string>(), -1, PathTableType(), MemoryLayoutType(), InstComplexity());
        }
        
        JSONUIReal()
        {
            init("", "", -1, -1, -1, "", "", "", "", std::vector<std::string>(), std::vector<std::string>(), -1, PathTableType(), MemoryLayoutType(), InstComplexity());
        }
 
        virtual ~JSONUIReal() {}
        
        void setInputs(int inputs) { fInputs = inputs; }
        void setOutputs(int outputs) { fOutputs = outputs; }
    
        void setSRIndex(int sr_index) { fSRIndex = sr_index; }
    
        // Init may be called multiple times so fMeta and fUI are reinitialized
        void init(const std::string& name,
                  const std::string& filename,
                  int inputs,
                  int outputs,
                  int sr_index,
                  const std::string& sha_key,
                  const std::string& dsp_code,
                  const std::string& version,
                  const std::string& compile_options,
                  const std::vector<std::string>& library_list,
                  const std::vector<std::string>& include_pathnames,
                  int size,
                  const PathTableType& path_table,
                  MemoryLayoutType memory_layout,
                  InstComplexity inst_comp,
                  bool extended = false)
        {
            fTab = 1;
            fExtended = extended;
            if (fExtended) {
                fUI << std::setprecision(std::numeric_limits<REAL>::max_digits10);
                fMeta << std::setprecision(std::numeric_limits<REAL>::max_digits10);
            }
        
            fIComp = inst_comp;
            
            // Start Meta generation
            fMeta.str("");
            tab(fTab, fMeta); fMeta << "\"meta\": [";
            fCloseMetaPar = ' ';
            
            // Start UI generation
            fUI.str("");
            tab(fTab, fUI); fUI << "\"ui\": [";
            fCloseUIPar = ' ';
            fTab += 1;
            
            fName = name;
            fFileName = filename;
            fInputs = inputs;
            fOutputs = outputs;
            fSRIndex = sr_index;
            fExpandedCode = dsp_code;
            fSHAKey = sha_key;
            fDSPSize = size;
            fPathTable = path_table;
            fVersion = version;
            fCompileOptions = compile_options;
            fLibraryList = library_list;
            fIncludePathnames = include_pathnames;
            fMemoryLayout = memory_layout;
        }
   
        // -- widget's layouts
    
        virtual void openGenericBox(const char* label, const char* name)
        {
            pushLabel(label);
            fUI << fCloseUIPar;
            tab(fTab, fUI); fUI << "{";
            fTab += 1;
            tab(fTab, fUI); fUI << "\"type\": \"" << name << "\",";
            tab(fTab, fUI); fUI << "\"label\": \"" << label << "\",";
            addMeta(fTab);
            tab(fTab, fUI); fUI << "\"items\": [";
            fCloseUIPar = ' ';
            fTab += 1;
        }

        virtual void openTabBox(const char* label)
        {
            openGenericBox(label, "tgroup");
        }
    
        virtual void openHorizontalBox(const char* label)
        {
            openGenericBox(label, "hgroup");
        }
    
        virtual void openVerticalBox(const char* label)
        {
            openGenericBox(label, "vgroup");
        }
    
        virtual void closeBox()
        {
            if (popLabel()) {
                // Shortnames can be computed when all fullnames are known
                computeShortNames();
            }
            fTab -= 1;
            tab(fTab, fUI); fUI << "]";
            fTab -= 1;
            tab(fTab, fUI); fUI << "}";
            fCloseUIPar = ',';
        }
    
        // -- active widgets
  
        virtual void addGenericButton(const char* label, const char* name)
        {
            std::string path = buildPath(label);
            fFullPaths.push_back(path);
            
            fUI << fCloseUIPar;
            tab(fTab, fUI); fUI << "{";
            fTab += 1;
            tab(fTab, fUI); fUI << "\"type\": \"" << name << "\",";
            tab(fTab, fUI); fUI << "\"label\": \"" << label << "\",";
        
            // Generate 'shortname' entry
            tab(fTab, fUI); fUI << "\"shortname\": \"";
        
            // Add fUI section
            fAllUI.push_back(fUI.str());
            fUI.str("");
        
            if (fPathTable.size() > 0) {
                tab(fTab, fUI); fUI << "\"address\": \"" << path << "\",";
                tab(fTab, fUI); fUI << "\"index\": " << getAddressIndex(path) << ((fMetaAux.size() > 0) ? "," : "");
            } else {
                tab(fTab, fUI); fUI << "\"address\": \"" << path << "\"" << ((fMetaAux.size() > 0) ? "," : "");
            }
            addMeta(fTab, false);
            fTab -= 1;
            tab(fTab, fUI); fUI << "}";
            fCloseUIPar = ',';
        }

        virtual void addButton(const char* label, REAL* zone)
        {
            addGenericButton(label, "button");
        }
    
        virtual void addCheckButton(const char* label, REAL* zone)
        {
            addGenericButton(label, "checkbox");
        }

        virtual void addGenericRange(const char* label, const char* name, REAL init, REAL min, REAL max, REAL step)
        {
            std::string path = buildPath(label);
            fFullPaths.push_back(path);
            
            fUI << fCloseUIPar;
            tab(fTab, fUI); fUI << "{";
            fTab += 1;
            tab(fTab, fUI); fUI << "\"type\": \"" << name << "\",";
            tab(fTab, fUI); fUI << "\"label\": \"" << label << "\",";
         
            // Generate 'shortname' entry
            tab(fTab, fUI); fUI << "\"shortname\": \"";
        
            // Add fUI section
            fAllUI.push_back(fUI.str());
            fUI.str("");
        
            tab(fTab, fUI); fUI << "\"address\": \"" << path << "\",";
            if (fPathTable.size() > 0) {
                tab(fTab, fUI); fUI << "\"index\": " << getAddressIndex(path) << ",";
            }
            addMeta(fTab);
            tab(fTab, fUI); fUI << "\"init\": " << init << ",";
            tab(fTab, fUI); fUI << "\"min\": " << min << ",";
            tab(fTab, fUI); fUI << "\"max\": " << max << ",";
            tab(fTab, fUI); fUI << "\"step\": " << step;
            fTab -= 1;
            tab(fTab, fUI); fUI << "}";
            fCloseUIPar = ',';
        }
    
        virtual void addVerticalSlider(const char* label, REAL* zone, REAL init, REAL min, REAL max, REAL step)
        {
            addGenericRange(label, "vslider", init, min, max, step);
        }
    
        virtual void addHorizontalSlider(const char* label, REAL* zone, REAL init, REAL min, REAL max, REAL step)
        {
            addGenericRange(label, "hslider", init, min, max, step);
        }
    
        virtual void addNumEntry(const char* label, REAL* zone, REAL init, REAL min, REAL max, REAL step)
        {
            addGenericRange(label, "nentry", init, min, max, step);
        }

        // -- passive widgets
    
        virtual void addGenericBargraph(const char* label, const char* name, REAL min, REAL max) 
        {
            std::string path = buildPath(label);
            fFullPaths.push_back(path);
            
            fUI << fCloseUIPar;
            tab(fTab, fUI); fUI << "{";
            fTab += 1;
            tab(fTab, fUI); fUI << "\"type\": \"" << name << "\",";
            tab(fTab, fUI); fUI << "\"label\": \"" << label << "\",";
         
            // Generate 'shortname' entry
            tab(fTab, fUI); fUI << "\"shortname\": \"";
        
            // Add fUI section
            fAllUI.push_back(fUI.str());
            fUI.str("");
            
            tab(fTab, fUI); fUI << "\"address\": \"" << path << "\",";
            if (fPathTable.size() > 0) {
                tab(fTab, fUI); fUI << "\"index\": " << getAddressIndex(path) << ",";
            }
            addMeta(fTab);
            tab(fTab, fUI); fUI << "\"min\": " << min << ",";
            tab(fTab, fUI); fUI << "\"max\": " << max;
            fTab -= 1;
            tab(fTab, fUI); fUI << "}";
            fCloseUIPar = ',';
        }

        virtual void addHorizontalBargraph(const char* label, REAL* zone, REAL min, REAL max) 
        {
            addGenericBargraph(label, "hbargraph", min, max);
        }
    
        virtual void addVerticalBargraph(const char* label, REAL* zone, REAL min, REAL max)
        {
            addGenericBargraph(label, "vbargraph", min, max);
        }
    
        virtual void addSoundfile(const char* label, const char* url, Soundfile** zone)
        {
            std::string path = buildPath(label);
            
            fUI << fCloseUIPar;
            tab(fTab, fUI); fUI << "{";
            fTab += 1;
            tab(fTab, fUI); fUI << "\"type\": \"" << "soundfile" << "\",";
            tab(fTab, fUI); fUI << "\"label\": \"" << label << "\"" << ",";
            tab(fTab, fUI); fUI << "\"url\": \"" << url << "\"" << ",";
            tab(fTab, fUI); fUI << "\"address\": \"" << path << "\"" << ((fPathTable.size() > 0) ? "," : "");
            if (fPathTable.size() > 0) {
                tab(fTab, fUI); fUI << "\"index\": " << getAddressIndex(path);
            }
            fTab -= 1;
            tab(fTab, fUI); fUI << "}";
            fCloseUIPar = ',';
        }

        // -- metadata declarations

        virtual void declare(REAL* zone, const char* key, const char* val)
        {
            fMetaAux.push_back(std::make_pair(key, val));
        }
    
        // Meta interface
        virtual void declare(const char* key, const char* value)
        {
            fMeta << fCloseMetaPar;
            // fName found in metadata
            if ((strcmp(key, "name") == 0) && (fName == "")) fName = value;
            // fFileName found in metadata
            if ((strcmp(key, "filename") == 0) && (fFileName == "")) fFileName = value;
            tab(fTab, fMeta); fMeta << "{ " << "\"" << key << "\"" << ": " << "\"" << value << "\" }";
            fCloseMetaPar = ',';
        }
    
        std::string JSON(bool flat = false)
        {
            if (fJSON.empty()) {
                fTab = 0;
                std::stringstream JSON;
                if (fExtended) {
                    JSON << std::setprecision(std::numeric_limits<REAL>::max_digits10);
                }
                JSON << "{";
                fTab += 1;
                tab(fTab, JSON); JSON << "\"name\": \"" << fName << "\",";
                tab(fTab, JSON); JSON << "\"filename\": \"" << fFileName << "\",";
                if (fVersion != "") { tab(fTab, JSON); JSON << "\"version\": \"" << fVersion << "\","; }
                if (fCompileOptions != "") { tab(fTab, JSON); JSON << "\"compile_options\": \"" <<  fCompileOptions << "\","; }
                if (fLibraryList.size() > 0) {
                    tab(fTab, JSON);
                    JSON << "\"library_list\": [";
                    for (size_t i = 0; i < fLibraryList.size(); i++) {
                        JSON << "\"" << fLibraryList[i] << "\"";
                        if (i < (fLibraryList.size() - 1)) JSON << ",";
                    }
                    JSON << "],";
                }
                if (fIncludePathnames.size() > 0) {
                    tab(fTab, JSON);
                    JSON << "\"include_pathnames\": [";
                    for (size_t i = 0; i < fIncludePathnames.size(); i++) {
                        JSON << "\"" << fIncludePathnames[i] << "\"";
                        if (i < (fIncludePathnames.size() - 1)) JSON << ",";
                    }
                    JSON << "],";
                }
                if (fDSPSize != -1) { tab(fTab, JSON); JSON << "\"size\": " << fDSPSize << ","; }
                if (fMemoryLayout.size() > 0) {
                    tab(fTab, JSON);
                    JSON << "\"memory_layout\": [";
                    for (size_t i = 0; i < fMemoryLayout.size(); i++) {
                        // DSP or field name, type, size, size-in-bytes, reads, writes
                        MemoryLayoutItem item = fMemoryLayout[i];
                        tab(fTab + 1, JSON);
                        JSON << "{ \"name\": \"" << std::get<0>(item) << "\", ";
                        JSON << "\"type\": \"" << std::get<1>(item) << "\", ";
                        JSON << "\"size\": " << std::get<2>(item) << ", ";
                        JSON << "\"size_bytes\": " << std::get<3>(item) << ", ";
                        JSON << "\"read\": " << std::get<4>(item) << ", ";
                        JSON << "\"write\": " << std::get<5>(item) << " }";
                        if (i < (fMemoryLayout.size() - 1)) JSON << ",";
                    }
                    tab(fTab, JSON);
                    JSON << "],";
                    
                    // Compute statistics
                    tab(fTab, JSON);
                    JSON << "\"compute_cost\": [{";
                    tab(fTab + 1, JSON);
                    JSON << "\"load\": " << fIComp.fLoad << ", ";
                    tab(fTab + 1, JSON);
                    JSON << "\"store\": " << fIComp.fStore << ", ";
                    tab(fTab + 1, JSON);
                    JSON << "\"declare\": " << fIComp.fDeclare << ", ";
                    tab(fTab + 1, JSON);
                    JSON << "\"number\": " << fIComp.fNumbers << ", ";
                    tab(fTab + 1, JSON);
                    JSON << "\"cast\": " << fIComp.fCast << ", ";
                    tab(fTab + 1, JSON);
                    JSON << "\"select\": " << fIComp.fSelect << ", ";
                    tab(fTab + 1, JSON);
                    JSON << "\"loop\": " << fIComp.fLoop << ", ";
                    tab(fTab + 1, JSON);
                    JSON << "\"binop\": [{ ";
                    JSON << "\"total\": " << fIComp.fBinop;
                    int size1 = (int)fIComp.fBinopSymbolTable.size();
                    if (size1 > 0) {
                        JSON << ", ";
                        for (const auto& it : fIComp.fBinopSymbolTable) {
                            JSON << "\"" << it.first << "\": " << it.second;
                            JSON << ((--size1 == 0) ? " }" : ", ");
                        }
                    } else {
                        JSON << " }";
                    }
                    JSON << "], ";
                    tab(fTab + 1, JSON);
                    JSON << "\"mathop\": [{ ";
                    JSON << "\"total\": " << fIComp.fMathop;
                    int size2 = (int)fIComp.fFunctionSymbolTable.size();
                    if (size2 > 0) {
                        JSON << ", ";
                        for (const auto& it : fIComp.fFunctionSymbolTable) {
                            JSON << "\"" << it.first << "\": " << it.second;
                            JSON << ((--size2 == 0) ? " }" : ", ");
                        }
                    } else {
                        JSON << " }";
                    }
                    JSON << "]";
                    tab(fTab, JSON);
                    JSON << "}],";
                }
                if (fSHAKey != "") { tab(fTab, JSON); JSON << "\"sha_key\": \"" << fSHAKey << "\","; }
                if (fExpandedCode != "") { tab(fTab, JSON); JSON << "\"code\": \"" << fExpandedCode << "\","; }
                tab(fTab, JSON); JSON << "\"inputs\": " << fInputs << ",";
                tab(fTab, JSON); JSON << "\"outputs\": " << fOutputs << ",";
                if (fSRIndex != -1) { tab(fTab, JSON); JSON << "\"sr_index\": " << fSRIndex << ","; }
                tab(fTab, fMeta); fMeta << "],";
              
                // Add last UI section
                fAllUI.push_back(fUI.str());
                // Finalize UI generation
                fUI.str("");
                // Add N-1 sections
                for (size_t i = 0; i < fAllUI.size()-1; i++) {
                    fUI << fAllUI[i] << fFull2Short[fFullPaths[i]] << "\",";
                }
                // And the last one
                fUI << fAllUI[fAllUI.size()-1];
                // Terminates the UI section
                tab(fTab, fUI); fUI << "]";
            
                fTab -= 1;
                if (fCloseMetaPar == ',') { // If "declare" has been called, fCloseMetaPar state is now ','
                    JSON << fMeta.str() << fUI.str();
                } else {
                    JSON << fUI.str();
                }
                
                tab(fTab, JSON); JSON << "}";
                
                // Keep result in fJSON
                fJSON = JSON.str();
            }
            return (flat) ? flatten(fJSON) : fJSON;
        }
    
};

// Externally available class using FAUSTFLOAT

struct FAUST_API JSONUI : public JSONUIReal<FAUSTFLOAT>, public UI {
    
    JSONUI(const std::string& name,
           const std::string& filename,
           int inputs,
           int outputs,
           int sr_index,
           const std::string& sha_key,
           const std::string& dsp_code,
           const std::string& version,
           const std::string& compile_options,
           const std::vector<std::string>& library_list,
           const std::vector<std::string>& include_pathnames,
           int size,
           const PathTableType& path_table,
           MemoryLayoutType memory_layout,
           InstComplexity inst_comp):
    JSONUIReal<FAUSTFLOAT>(name, filename,
                          inputs, outputs,
                          sr_index,
                          sha_key, dsp_code,
                          version, compile_options,
                          library_list, include_pathnames,
                          size, path_table,
                          memory_layout, inst_comp)
    {}
    
    JSONUI(const std::string& name, const std::string& filename, int inputs, int outputs):
    JSONUIReal<FAUSTFLOAT>(name, filename, inputs, outputs)
    {}
    
    JSONUI(int inputs, int outputs):JSONUIReal<FAUSTFLOAT>(inputs, outputs)
    {}
    
    JSONUI():JSONUIReal<FAUSTFLOAT>()
    {}

    virtual void openTabBox(const char* label)
    {
        JSONUIReal<FAUSTFLOAT>::openTabBox(label);
    }
    virtual void openHorizontalBox(const char* label)
    {
        JSONUIReal<FAUSTFLOAT>::openHorizontalBox(label);
    }
    virtual void openVerticalBox(const char* label)
    {
        JSONUIReal<FAUSTFLOAT>::openVerticalBox(label);
    }
    virtual void closeBox()
    {
        JSONUIReal<FAUSTFLOAT>::closeBox();
    }
    
    // -- active widgets
    
    virtual void addButton(const char* label, FAUSTFLOAT* zone)
    {
        JSONUIReal<FAUSTFLOAT>::addButton(label, zone);
    }
    virtual void addCheckButton(const char* label, FAUSTFLOAT* zone)
    {
        JSONUIReal<FAUSTFLOAT>::addCheckButton(label, zone);
    }
    virtual void addVerticalSlider(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step)
    {
        JSONUIReal<FAUSTFLOAT>::addVerticalSlider(label, zone, init, min, max, step);
    }
    virtual void addHorizontalSlider(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step)
    {
        JSONUIReal<FAUSTFLOAT>::addHorizontalSlider(label, zone, init, min, max, step);
    }
    virtual void addNumEntry(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step)
    {
        JSONUIReal<FAUSTFLOAT>::addNumEntry(label, zone, init, min, max, step);
    }
    
    // -- passive widgets
    
    virtual void addHorizontalBargraph(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT min, FAUSTFLOAT max)
    {
        JSONUIReal<FAUSTFLOAT>::addHorizontalBargraph(label, zone, min, max);
    }
    virtual void addVerticalBargraph(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT min, FAUSTFLOAT max)
    {
        JSONUIReal<FAUSTFLOAT>::addVerticalBargraph(label, zone, min, max);
    }
    
    // -- soundfiles
    
    virtual void addSoundfile(const char* label, const char* filename, Soundfile** sf_zone)
    {
        JSONUIReal<FAUSTFLOAT>::addSoundfile(label, filename, sf_zone);
    }
    
    // -- metadata declarations
    
    virtual void declare(FAUSTFLOAT* zone, const char* key, const char* val)
    {
        JSONUIReal<FAUSTFLOAT>::declare(zone, key, val);
    }

    virtual void declare(const char* key, const char* val)
    {
        JSONUIReal<FAUSTFLOAT>::declare(key, val);
    }

    virtual ~JSONUI() {}
    
};

#endif // FAUST_JSONUI_H
/**************************  END  JSONUI.h **************************/

namespace oscfaust
{

class OSCIO;
class RootNode;
typedef class SMARTP<RootNode> SRootNode;

/**
 * an alias target includes a map to rescale input values to output values
 * and a target osc address. The input values can be given in reversed order
 * to reverse the control
 */
struct aliastarget
{
	double      fMinIn;
	double      fMaxIn;
	double      fMinOut;
	double      fMaxOut;
	std::string fTarget;	// the real osc address

	aliastarget(const char* address, double imin, double imax, double omin, double omax)
		: fMinIn(imin), fMaxIn(imax), fMinOut(omin), fMaxOut(omax), fTarget(address) {}

    aliastarget(const aliastarget& t)
        : fMinIn(t.fMinIn), fMaxIn(t.fMaxIn), fMinOut(t.fMinOut), fMaxOut(t.fMaxOut), fTarget(t.fTarget) {}
    
    // explicit copy assignment operator
    aliastarget& operator=(const aliastarget& other) { return *this; }
    
	double scale(double x) const 
    {
        if (fMinIn < fMaxIn) {
            // increasing control
            double z = (x < fMinIn) ? fMinIn : (x > fMaxIn) ? fMaxIn : x;
            return fMinOut + (z-fMinIn)*(fMaxOut-fMinOut)/(fMaxIn-fMinIn);
            
        } else if (fMinIn > fMaxIn) {
            // reversed control
            double z = (x < fMaxIn) ? fMaxIn : (x > fMinIn) ? fMinIn : x;
            return fMinOut + (fMinIn-z)*(fMaxOut-fMinOut)/(fMinIn-fMaxIn);
            
        } else {
            // no control !
            return (fMinOut+fMaxOut)/2.0f;
        }
    }
    
    double invscale(double x) const
    {
        if (fMinOut < fMaxOut) {
            // increasing control
            double z = (x < fMinOut) ? fMinOut : (x > fMaxOut) ? fMaxOut : x;
            return fMinIn + (z-fMinOut)*(fMaxIn-fMinIn)/(fMaxOut-fMinOut);
            
        } else if (fMinOut > fMaxOut) {
            // reversed control
            double z = (x < fMaxOut) ? fMaxOut : (x > fMinOut) ? fMinOut : x;
            return fMinIn + (fMinOut-z)*(fMaxIn-fMinIn)/(fMinOut-fMaxOut);
            
        } else {
            // no control !
            return (fMinIn+fMaxIn)/2.0f;
        }
    }
};

//--------------------------------------------------------------------------
/*!
	\brief a faust root node

	A Faust root node handles the \c 'hello' message and provides support
	for incoming osc signal data. 
*/
class RootNode : public MessageDriven
{

    private:
        int *fUPDIn, *fUDPOut, *fUDPErr;    // the osc port numbers (required by the hello method)
        OSCIO* fIO;                         // an OSC IO controler
        JSONUI* fJSON;

        typedef std::map<std::string, std::vector<aliastarget> > TAliasMap;
        TAliasMap fAliases;

        template <typename T>
        void processAliasAux(const std::string& address, T val);
        void processAlias(const std::string& address, float val);
        void processAlias(const std::string& address, double val);
    
        void eraseAliases(const std::string& target);
        void eraseAlias(const std::string& target, const std::string& alias);
        bool aliasError(const Message* msg);

    protected:
        RootNode(const char *name, JSONUI* json, OSCIO* io = NULL) : MessageDriven(name, ""), fUPDIn(0), fUDPOut(0), fUDPErr(0), fIO(io), fJSON(json) {}
        virtual ~RootNode() {}

    public:
        static SRootNode create(const char* name, JSONUI* json, OSCIO* io = NULL) { return new RootNode(name, json, io); }

        virtual void processMessage(const Message* msg);
        virtual bool accept(const Message* msg);
        virtual void get(unsigned long ipdest) const;
        virtual void get(unsigned long ipdest, const std::string& what) const;

        template <typename T>
        bool aliasMsgAux(const Message* msg, T omin, T omax);
        bool aliasMsg(const Message* msg, double omin, double omax);
        bool aliasMsg(const Message* msg, float omin, float omax);
    
        template <typename T>
        void addAliasAux(const char* alias, const char* address, T imin, T imax, T omin, T omax);
        void addAlias(const char* alias, const char* address, float imin, float imax, float omin, float omax);
        void addAlias(const char* alias, const char* address, double imin, double imax, double omin, double omax);
    
        bool acceptSignal(const Message* msg);      ///< handler for signal data
        void hello(unsigned long ipdest) const;     ///< handler for the 'hello' message
        void setPorts(int* in, int* out, int* err);

        std::vector<std::pair<std::string, double> > getAliases(const std::string& address, double value);
};

} // end namespoace

#endif

namespace oscfaust
{

/**
 * map (rescale) input values to output values
 */
template <typename C> struct mapping
{
	const C fMinOut;
	const C fMaxOut;
	mapping(C omin, C omax) : fMinOut(omin), fMaxOut(omax) {}
	C clip (C x) { return (x < fMinOut) ? fMinOut : (x > fMaxOut) ? fMaxOut : x; }
};

//--------------------------------------------------------------------------
/*!
	\brief a faust node is a terminal node and represents a faust parameter controler
*/
template <typename C> class FaustNode : public MessageDriven, public uiTypedItemReal<C>
{
	mapping<C>	fMapping;
    RootNode*   fRoot;
    bool        fInput;  // true for input nodes (slider, button...)
	
	//---------------------------------------------------------------------
	// Warning !!!
	// The cast (C*)fZone is necessary because the real size allocated is
	// only known at execution time. When the library is compiled, fZone is
	// uniquely defined by FAUSTFLOAT.
	//---------------------------------------------------------------------
	bool store(C val) { *(C*)this->fZone = fMapping.clip(val); return true; }
	void sendOSC() const;

	protected:
		FaustNode(RootNode* root, const char *name, C* zone, C init, C min, C max, const char* prefix, GUI* ui, bool initZone, bool input) 
			: MessageDriven(name, prefix), uiTypedItemReal<C>(ui, zone), fMapping(min, max), fRoot(root), fInput(input)
			{
                if (initZone) {
                    *zone = init; 
                }
            }
			
		virtual ~FaustNode() {}

	public:
		typedef SMARTP<FaustNode<C> > SFaustNode;
		static SFaustNode create(RootNode* root, const char* name, C* zone, C init, C min, C max, const char* prefix, GUI* ui, bool initZone, bool input)	
        { 
            SFaustNode node = new FaustNode(root, name, zone, init, min, max, prefix, ui, initZone, input); 
            /*
                Since FaustNode is a subclass of uiItem, the pointer will also be kept in the GUI class, and it's desallocation will be done there.
                So we don't want to have smartpointer logic desallocate it and we increment the refcount.
            */
            node->addReference();
            return node; 
        }
    
		bool accept(const Message* msg);
		void get(unsigned long ipdest) const;		///< handler for the 'get' message
		virtual void reflectZone() { sendOSC(); this->fCache = *this->fZone; }
};

} // end namespace

#endif

class GUI;
namespace oscfaust
{

class OSCIO;
class RootNode;
typedef class SMARTP<RootNode> SRootNode;
class MessageDriven;
typedef class SMARTP<MessageDriven>	SMessageDriven;

//--------------------------------------------------------------------------
/*!
	\brief a factory to build a OSC UI hierarchy
	
	Actually, makes use of a stack to build the UI hierarchy.
	It includes a pointer to a OSCIO controler, but just to give it to the root node.
*/
class FaustFactory
{
    std::stack<SMessageDriven>  fNodes;		///< maintains the current hierarchy level
    SRootNode  fRoot;   ///< keep track of the root node
    OSCIO*     fIO;     ///< hack to support audio IO via OSC, actually the field is given to the root node
    GUI*       fGUI;    ///< a GUI pointer to support updateAllGuis(), required for bi-directionnal OSC
    JSONUI*    fJSON;
    
    private:
        std::string addressFirst(const std::string& address) const;
        std::string addressTail(const std::string& address) const;
        
    public:
        FaustFactory(GUI* ui, JSONUI* json, OSCIO * io = NULL);
        virtual ~FaustFactory();
        
        template <typename C> void addnode(const char* label, C* zone, C init, C min, C max, bool initZone, bool input);
        template <typename C> void addAlias(const std::string& fullpath, C* zone, C imin, C imax, C init, C min, C max, const char* label);
        
        void addAlias(const char* alias, const char* address, float imin, float imax, float omin, float omax);
        void addAlias(const char* alias, const char* address, double imin, double imax, double omin, double omax);
        void opengroup(const char* label);
        void closegroup();
        
        SRootNode root() const; 
};

/**
 * Add a node to the OSC UI tree in the current group at the top of the stack 
 */
template <typename C> void FaustFactory::addnode(const char* label, C* zone, C init, C min, C max, bool initZone, bool input) 
{
	SMessageDriven top;
	if (fNodes.size()) top = fNodes.top();
	if (top) {
		std::string prefix = top->getOSCAddress();
		top->add(FaustNode<C>::create(root(), label, zone, init, min, max, prefix.c_str(), fGUI, initZone, input));
	}
}

/**
 * Add an alias (actually stored and handled at root node level
 */
template <typename C> void FaustFactory::addAlias(const std::string& fullpath, C* zone, C imin, C imax, C init, C min, C max, const char* label)
{
	std::istringstream 	ss(fullpath);
	std::string 		realpath; 
 
	ss >> realpath >> imin >> imax;
	SMessageDriven top = fNodes.top();
	if (top) {
		std::string target = top->getOSCAddress() + "/" + label;
		addAlias(realpath.c_str(), target.c_str(), C(imin), C(imax), C(min), C(max));
	}
}

} // end namespoace

#endif

class GUI;

typedef void (*ErrorCallback)(void*);  

namespace oscfaust
{

class OSCIO;
class OSCSetup;
class OSCRegexp;
    
//--------------------------------------------------------------------------
/*!
	\brief the main Faust OSC Lib API
	
	The OSCControler is essentially a glue between the memory representation (in charge of the FaustFactory),
	and the network services (in charge of OSCSetup).
*/
class OSCControler
{
	int fUDPPort,   fUDPOut, fUPDErr;	// the udp ports numbers
	std::string     fDestAddress;		// the osc messages destination address, used at initialization only
										// to collect the address from the command line
	std::string     fBindAddress;		// when non empty, the address used to bind the socket for listening
	OSCSetup*		fOsc;				// the network manager (handles the udp sockets)
	OSCIO*			fIO;				// hack for OSC IO support (actually only relayed to the factory)
	FaustFactory*	fFactory;			// a factory to build the memory representation

    bool            fInit;
    
	public:
		/*
			base udp port is chosen in an unassigned range from IANA PORT NUMBERS (last updated 2011-01-24)
			see at http://www.iana.org/assignments/port-numbers
			5507-5552  Unassigned
		*/
		enum { kUDPBasePort = 5510 };
            
        OSCControler(int argc, char* argv[], GUI* ui, JSONUI* json, OSCIO* io = NULL, ErrorCallback errCallback = NULL, void* arg = NULL, bool init = true);

        virtual ~OSCControler();
	
		//--------------------------------------------------------------------------
		// addnode, opengroup and closegroup are simply relayed to the factory
		//--------------------------------------------------------------------------
		// Add a node in the current group (top of the group stack)
		template <typename T> void addnode(const char* label, T* zone, T init, T min, T max, bool input = true)
							{ fFactory->addnode(label, zone, init, min, max, fInit, input); }
		
		//--------------------------------------------------------------------------
		// This method is used for alias messages. The arguments imin and imax allow
		// to map incomming values from the alias input range to the actual range 
		template <typename T> void addAlias(const std::string& fullpath, T* zone, T imin, T imax, T init, T min, T max, const char* label)
							{ fFactory->addAlias(fullpath, zone, imin, imax, init, min, max, label); }

		void opengroup(const char* label)		{ fFactory->opengroup(label); }
		void closegroup()						{ fFactory->closegroup(); }
	   
		//--------------------------------------------------------------------------
		void run();				// starts the network services
		void endBundle();		// when bundle mode is on, close and send the current bundle (if any)
		void stop();			// stop the network services
		std::string getInfos() const; // gives information about the current environment (version, port numbers,...)

		int	getUDPPort() const			{ return fUDPPort; }
		int	getUDPOut()	const			{ return fUDPOut; }
		int	getUDPErr()	const			{ return fUPDErr; }
		const char*	getDestAddress() const { return fDestAddress.c_str(); }
		const char*	getRootName() const;	// probably useless, introduced for UI extension experiments
    
        void setUDPPort(int port) { fUDPPort = port; }
        void setUDPOut(int port) { fUDPOut = port; }
        void setUDPErr(int port) { fUPDErr = port; }
        void setDestAddress(const char* address) { fDestAddress = address; }

        // By default, an osc interface emits all parameters. You can filter specific params dynamically.
        static std::vector<OSCRegexp*>     fFilteredPaths; // filtered paths will not be emitted
        static void addFilteredPath(std::string path);
        static bool isPathFiltered(std::string path);
        static void resetFilteredPaths();
    
		static float version();				// the Faust OSC library version number
		static const char* versionstr();	// the Faust OSC library version number as a string
		static int gXmit;                   // a static variable to control the transmission of values
                                            // i.e. the use of the interface as a controler
		static int gBundle;                 // a static variable to control the osc bundle mode
};

#define kNoXmit     0
#define kAll        1
#define kAlias      2

}

#endif

#ifdef _WIN32
#define strcasecmp _stricmp
#endif

/******************************************************************************
 *******************************************************************************
 
 OSC (Open Sound Control) USER INTERFACE
 
 *******************************************************************************
 *******************************************************************************/
/*
 
 Note about the OSC addresses and the Faust UI names:
 ----------------------------------------------------
 There are potential conflicts between the Faust UI objects naming scheme and
 the OSC address space. An OSC symbolic names is an ASCII string consisting of
 printable characters other than the following:
	space
 #	number sign
 *	asterisk
 ,	comma
 /	forward
 ?	question mark
 [	open bracket
 ]	close bracket
 {	open curly brace
 }	close curly brace
 
 a simple solution to address the problem consists in replacing
 space or tabulation with '_' (underscore)
 all the other osc excluded characters with '-' (hyphen)
 
 This solution is implemented in the proposed OSC UI;
 */

class OSCUI : public GUI
{
    
    private:
        
        oscfaust::OSCControler*	fCtrl;
        std::vector<const char*> fAlias;
        JSONUI fJSON;
        
        const char* tr(const char* label) const
        {
            static char buffer[1024];
            char* ptr = buffer; int n = 1;
            while (*label && (n++ < 1024)) {
                switch (*label) {
                    case ' ': case '	':
                        *ptr++ = '_';
                        break;
                    case '#': case '*': case ',': case '/': case '?':
                    case '[': case ']': case '{': case '}': case '(': case ')':
                        *ptr++ = '_';
                        break;
                    default:
                        *ptr++ = *label;
                }
                label++;
            }
            *ptr = 0;
            return buffer;
        }
        
        // add all accumulated alias
        void addalias(FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, const char* label)
        {
            for (unsigned int i = 0; i < fAlias.size(); i++) {
                fCtrl->addAlias(fAlias[i], zone, FAUSTFLOAT(0), FAUSTFLOAT(1), init, min, max, label);
            }
            fAlias.clear();
        }
        
    public:
        
        OSCUI(const char* /*applicationname*/, int argc, char* argv[],
              oscfaust::OSCIO* io = NULL,
              ErrorCallback errCallback = NULL,
              void* arg = NULL,
              bool init = true) : GUI()
        {
            fCtrl = new oscfaust::OSCControler(argc, argv, this, &fJSON, io, errCallback, arg, init);
            // fCtrl->opengroup(applicationname);
        }
        
        virtual ~OSCUI() { delete fCtrl; }
        
        // -- widget's layouts
        
        virtual void openTabBox(const char* label)          { fCtrl->opengroup(tr(label)); fJSON.openTabBox(label); }
        virtual void openHorizontalBox(const char* label)   { fCtrl->opengroup(tr(label)); fJSON.openHorizontalBox(label); }
        virtual void openVerticalBox(const char* label)     { fCtrl->opengroup(tr(label)); fJSON.openVerticalBox(label); }
        virtual void closeBox()                             { fCtrl->closegroup(); fJSON.closeBox(); }
        
        // -- active widgets
        virtual void addButton(const char* label, FAUSTFLOAT* zone)
        {
            const char* l = tr(label);
            addalias(zone, 0, 0, 1, l);
            fCtrl->addnode(l, zone, FAUSTFLOAT(0), FAUSTFLOAT(0), FAUSTFLOAT(1));
            fJSON.addButton(label, zone);
        }
        virtual void addCheckButton(const char* label, FAUSTFLOAT* zone)
        {
            const char* l = tr(label);
            addalias(zone, 0, 0, 1, l);
            fCtrl->addnode(l, zone, FAUSTFLOAT(0), FAUSTFLOAT(0), FAUSTFLOAT(1));
            fJSON.addCheckButton(label, zone);
        }
        virtual void addVerticalSlider(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step)
        {
            const char* l = tr(label);
            addalias(zone, init, min, max, l);
            fCtrl->addnode(l, zone, init, min, max);
            fJSON.addVerticalSlider(label, zone, init, min, max, step);
        }
        virtual void addHorizontalSlider(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step)
        {
            const char* l = tr(label);
            addalias(zone, init, min, max, l);
            fCtrl->addnode(l, zone, init, min, max);
            fJSON.addHorizontalSlider(label, zone, init, min, max, step);
        }
        virtual void addNumEntry(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step)
        {
            const char* l = tr(label);
            addalias(zone, init, min, max, l);
            fCtrl->addnode(l, zone, init, min, max);
            fJSON.addNumEntry(label, zone, init, min, max, step);
        }
        
        // -- passive widgets
        
        virtual void addHorizontalBargraph(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT min, FAUSTFLOAT max)
        {
            const char* l = tr(label);
            addalias(zone, 0, min, max, l);
            fCtrl->addnode(l, zone, FAUSTFLOAT(0), min, max, false);
            fJSON.addHorizontalBargraph(label, zone, min, max);
        }
        virtual void addVerticalBargraph(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT min, FAUSTFLOAT max)
        {
            const char* l = tr(label);
            addalias(zone, 0, min, max, l);
            fCtrl->addnode(l, zone, FAUSTFLOAT(0), min, max, false);
            fJSON.addVerticalBargraph(label, zone, min, max);
        }
            
        // -- metadata declarations
        
        virtual void declare(FAUSTFLOAT* zone, const char* key, const char* alias)
        {
            if (strcasecmp(key, "OSC") == 0) fAlias.push_back(alias);
            fJSON.declare(zone, key, alias);
        }
        
        bool run()
        {
            fCtrl->run();
            return true;
        }
        
        void stop()			{ fCtrl->stop(); }
        void endBundle() 	{ fCtrl->endBundle(); }
        
        std::string getInfos()          { return fCtrl->getInfos(); }
        
        const char* getRootName()		{ return fCtrl->getRootName(); }
        int getUDPPort()                { return fCtrl->getUDPPort(); }
        int getUDPOut()                 { return fCtrl->getUDPOut(); }
        int getUDPErr()                 { return fCtrl->getUDPErr(); }
        const char* getDestAddress()    { return fCtrl->getDestAddress(); }
        
        void setUDPPort(int port)       { fCtrl->setUDPPort(port); }
        void setUDPOut(int port)        { fCtrl->setUDPOut(port); }
        void setUDPErr(int port)        { fCtrl->setUDPErr(port); }
        void setDestAddress(const char* address)    { return fCtrl->setDestAddress(address); }
    
};

#endif // __OSCUI__
/**************************  END  OSCUI.h **************************/

#ifdef POLY2
#include "effect.h"
#endif

#if SOUNDFILE
/************************** BEGIN SoundUI.h **************************
 FAUST Architecture File
 Copyright (C) 2003-2022 GRAME, Centre National de Creation Musicale
 ---------------------------------------------------------------------
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation; either version 2.1 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 
 EXCEPTION : As a special exception, you may create a larger work
 that contains this FAUST architecture section and distribute
 that work under terms of your choice, so long as this FAUST
 architecture section is not modified.
 ********************************************************************/
 
#ifndef __SoundUI_H__
#define __SoundUI_H__

#include <map>
#include <vector>
#include <string>
#include <iostream>
#include <memory>


#if defined(__APPLE__) && !defined(__VCVRACK__) && !defined(JUCE_32BIT) && !defined(JUCE_64BIT)
#include <CoreFoundation/CFBundle.h>
#endif

// Always included otherwise -i mode later on will not always include it (with the conditional includes)
/************************** BEGIN Soundfile.h **************************
 FAUST Architecture File
 Copyright (C) 2003-2022 GRAME, Centre National de Creation Musicale
 ---------------------------------------------------------------------
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation; either version 2.1 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 
 EXCEPTION : As a special exception, you may create a larger work
 that contains this FAUST architecture section and distribute
 that work under terms of your choice, so long as this FAUST
 architecture section is not modified.
 ********************************************************************/

#ifndef __Soundfile__
#define __Soundfile__

#include <string.h>
#include <string>
#include <vector>

#ifndef FAUSTFLOAT
#define FAUSTFLOAT float
#endif

#define BUFFER_SIZE 1024
#define SAMPLE_RATE 44100
#define MAX_CHAN 64
#define MAX_SOUNDFILE_PARTS 256

#ifdef _MSC_VER
#define PRE_PACKED_STRUCTURE __pragma(pack(push, 1))
#define POST_PACKED_STRUCTURE \
    ;                         \
    __pragma(pack(pop))
#else
#define PRE_PACKED_STRUCTURE
#define POST_PACKED_STRUCTURE __attribute__((__packed__))
#endif

/*
 The soundfile structure to be used by the DSP code. Soundfile has a MAX_SOUNDFILE_PARTS parts 
 (even a single soundfile or an empty soundfile). 
 The fLength, fOffset and fSR fields are filled accordingly by repeating the actual parts if needed.
 The fBuffers contains MAX_CHAN non-interleaved arrays of samples.
 
 It has to be 'packed' to that the LLVM backend can correctly access it.

 Index computation:
    - p is the current part number [0..MAX_SOUNDFILE_PARTS-1] (must be proved by the type system)
    - i is the current position in the part. It will be constrained between [0..length]
    - idx(p,i) = fOffset[p] + max(0, min(i, fLength[p]));
*/

PRE_PACKED_STRUCTURE
struct Soundfile {
    void* fBuffers; // will correspond to a double** or float** pointer chosen at runtime
    int* fLength;   // length of each part (so fLength[P] contains the length in frames of part P)
    int* fSR;       // sample rate of each part (so fSR[P] contains the SR of part P)
    int* fOffset;   // offset of each part in the global buffer (so fOffset[P] contains the offset in frames of part P)
    int fChannels;  // max number of channels of all concatenated files
    int fParts;     // the total number of loaded parts
    bool fIsDouble; // keep the sample format (float or double)

    Soundfile(int cur_chan, int length, int max_chan, int total_parts, bool is_double)
    {
        fLength   = new int[MAX_SOUNDFILE_PARTS];
        fSR       = new int[MAX_SOUNDFILE_PARTS];
        fOffset   = new int[MAX_SOUNDFILE_PARTS];
        fIsDouble = is_double;
        fChannels = cur_chan;
        fParts    = total_parts;
        if (fIsDouble) {
            fBuffers = allocBufferReal<double>(cur_chan, length, max_chan);
        } else {
            fBuffers = allocBufferReal<float>(cur_chan, length, max_chan);
        }
    }
    
    template <typename REAL>
    void* allocBufferReal(int cur_chan, int length, int max_chan)
    {
        REAL** buffers = new REAL*[max_chan];
        for (int chan = 0; chan < cur_chan; chan++) {
            buffers[chan] = new REAL[length];
            memset(buffers[chan], 0, sizeof(REAL) * length);
        }
        return buffers;
    }
    
    void copyToOut(int size, int channels, int max_channels, int offset, void* buffer)
    {
        if (fIsDouble) {
            copyToOutReal<double>(size, channels, max_channels, offset, buffer);
       } else {
            copyToOutReal<float>(size, channels, max_channels, offset, buffer);
        }
    }
    
    void shareBuffers(int cur_chan, int max_chan)
    {
        // Share the same buffers for all other channels so that we have max_chan channels available
        if (fIsDouble) {
            for (int chan = cur_chan; chan < max_chan; chan++) {
                static_cast<double**>(fBuffers)[chan] = static_cast<double**>(fBuffers)[chan % cur_chan];
            }
        } else {
            for (int chan = cur_chan; chan < max_chan; chan++) {
                static_cast<float**>(fBuffers)[chan] = static_cast<float**>(fBuffers)[chan % cur_chan];
            }
        }
    }
    
    template <typename REAL>
    void copyToOutReal(int size, int channels, int max_channels, int offset, void* buffer)
    {
        for (int sample = 0; sample < size; sample++) {
            for (int chan = 0; chan < channels; chan++) {
                static_cast<REAL**>(fBuffers)[chan][offset + sample] = static_cast<REAL*>(buffer)[sample * max_channels + chan];
            }
        }
    }
    
    template <typename REAL>
    void getBuffersOffsetReal(void* buffers, int offset)
    {
        for (int chan = 0; chan < fChannels; chan++) {
            static_cast<REAL**>(buffers)[chan] = &(static_cast<REAL**>(fBuffers))[chan][offset];
        }
    }
    
    void emptyFile(int part, int& offset)
    {
        fLength[part] = BUFFER_SIZE;
        fSR[part] = SAMPLE_RATE;
        fOffset[part] = offset;
        // Update offset
        offset += fLength[part];
    }
 
    ~Soundfile()
    {
        // Free the real channels only
        if (fIsDouble) {
            for (int chan = 0; chan < fChannels; chan++) {
                delete[] static_cast<double**>(fBuffers)[chan];
            }
            delete[] static_cast<double**>(fBuffers);
        } else {
            for (int chan = 0; chan < fChannels; chan++) {
                delete[] static_cast<float**>(fBuffers)[chan];
            }
            delete[] static_cast<float**>(fBuffers);
        }
        delete[] fLength;
        delete[] fSR;
        delete[] fOffset;
    }

    typedef std::vector<std::string> Directories;
    
} POST_PACKED_STRUCTURE;

/*
 The generic soundfile reader.
 */

class SoundfileReader {
    
   protected:
    
    int fDriverSR;
   
    // Check if a soundfile exists and return its real path_name
    std::string checkFile(const Soundfile::Directories& sound_directories, const std::string& file_name)
    {
        if (checkFile(file_name)) {
            return file_name;
        } else {
            for (size_t i = 0; i < sound_directories.size(); i++) {
                std::string path_name = sound_directories[i] + "/" + file_name;
                if (checkFile(path_name)) { return path_name; }
            }
            return "";
        }
    }
    
    bool isResampling(int sample_rate) { return (fDriverSR > 0 && fDriverSR != sample_rate); }
 
    // To be implemented by subclasses

    /**
     * Check the availability of a sound resource.
     *
     * @param path_name - the name of the file, or sound resource identified this way
     *
     * @return true if the sound resource is available, false otherwise.
     */
    virtual bool checkFile(const std::string& path_name) = 0;
    
    /**
     * Check the availability of a sound resource.
     *
     * @param buffer - the sound buffer
     * @param size - the sound buffer length
     *
     * @return true if the sound resource is available, false otherwise.
     */

    virtual bool checkFile(unsigned char* buffer, size_t size) { return true; }

    /**
     * Get the channels and length values of the given sound resource.
     *
     * @param path_name - the name of the file, or sound resource identified this way
     * @param channels - the channels value to be filled with the sound resource number of channels
     * @param length - the length value to be filled with the sound resource length in frames
     *
     */
    virtual void getParamsFile(const std::string& path_name, int& channels, int& length) = 0;
    
    /**
     * Get the channels and length values of the given sound resource.
     *
     * @param buffer - the sound buffer
     * @param size - the sound buffer length
     * @param channels - the channels value to be filled with the sound resource number of channels
     * @param length - the length value to be filled with the sound resource length in frames
     *
     */
    virtual void getParamsFile(unsigned char* buffer, size_t size, int& channels, int& length) {}

    /**
     * Read one sound resource and fill the 'soundfile' structure accordingly
     *
     * @param soundfile - the soundfile to be filled
     * @param path_name - the name of the file, or sound resource identified this way
     * @param part - the part number to be filled in the soundfile
     * @param offset - the offset value to be incremented with the actual sound resource length in frames
     * @param max_chan - the maximum number of mono channels to fill
     *
     */
    virtual void readFile(Soundfile* soundfile, const std::string& path_name, int part, int& offset, int max_chan) = 0;
    
    /**
     * Read one sound resource and fill the 'soundfile' structure accordingly
     *
     * @param soundfile - the soundfile to be filled
     * @param buffer - the sound buffer
     * @param size - the sound buffer length
     * @param part - the part number to be filled in the soundfile
     * @param offset - the offset value to be incremented with the actual sound resource length in frames
     * @param max_chan - the maximum number of mono channels to fill
     *
     */
    virtual void readFile(Soundfile* soundfile, unsigned char* buffer, size_t size, int part, int& offset, int max_chan) {}

  public:
    
    SoundfileReader() {}
    virtual ~SoundfileReader() {}
    
    void setSampleRate(int sample_rate) { fDriverSR = sample_rate; }
   
    Soundfile* createSoundfile(const std::vector<std::string>& path_name_list, int max_chan, bool is_double)
    {
        try {
            int cur_chan = 1; // At least one channel
            int total_length = 0;
            
            // Compute total length and channels max of all files
            for (size_t i = 0; i < path_name_list.size(); i++) {
                int chan, length;
                if (path_name_list[i] == "__empty_sound__") {
                    length = BUFFER_SIZE;
                    chan = 1;
                } else {
                    getParamsFile(path_name_list[i], chan, length);
                }
                cur_chan = std::max<int>(cur_chan, chan);
                total_length += length;
            }
           
            // Complete with empty parts
            total_length += (MAX_SOUNDFILE_PARTS - path_name_list.size()) * BUFFER_SIZE;
            
            // Create the soundfile
            Soundfile* soundfile = new Soundfile(cur_chan, total_length, max_chan, path_name_list.size(), is_double);
            
            // Init offset
            int offset = 0;
            
            // Read all files
            for (size_t i = 0; i < path_name_list.size(); i++) {
                if (path_name_list[i] == "__empty_sound__") {
                    soundfile->emptyFile(i, offset);
                } else {
                    readFile(soundfile, path_name_list[i], i, offset, max_chan);
                }
            }
            
            // Complete with empty parts
            for (size_t i = path_name_list.size(); i < MAX_SOUNDFILE_PARTS; i++) {
                soundfile->emptyFile(i, offset);
            }
            
            // Share the same buffers for all other channels so that we have max_chan channels available
            soundfile->shareBuffers(cur_chan, max_chan);
            return soundfile;
            
        } catch (...) {
            return nullptr;
        }
    }

    // Check if all soundfiles exist and return their real path_name
    std::vector<std::string> checkFiles(const Soundfile::Directories& sound_directories,
                                        const std::vector<std::string>& file_name_list)
    {
        std::vector<std::string> path_name_list;
        for (size_t i = 0; i < file_name_list.size(); i++) {
            std::string path_name = checkFile(sound_directories, file_name_list[i]);
            // If 'path_name' is not found, it is replaced by an empty sound (= silence)
            path_name_list.push_back((path_name == "") ? "__empty_sound__" : path_name);
        }
        return path_name_list;
    }

};

#endif
/**************************  END  Soundfile.h **************************/

#if defined(JUCE_32BIT) || defined(JUCE_64BIT)
/************************** BEGIN JuceReader.h **************************
 FAUST Architecture File
 Copyright (C) 2003-2022 GRAME, Centre National de Creation Musicale
 ---------------------------------------------------------------------
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation; either version 2.1 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 
 EXCEPTION : As a special exception, you may create a larger work
 that contains this FAUST architecture section and distribute
 that work under terms of your choice, so long as this FAUST
 architecture section is not modified.
 ************************************************************************/

#ifndef __JuceReader__
#define __JuceReader__

#include <assert.h>

#include "../JuceLibraryCode/JuceHeader.h"


struct JuceReader : public SoundfileReader {
    
    juce::AudioFormatManager fFormatManager;
    
    JuceReader() { fFormatManager.registerBasicFormats(); }
    virtual ~JuceReader()
    {}
    
    bool checkFile(const std::string& path_name) override
    {
        juce::File file = juce::File::getCurrentWorkingDirectory().getChildFile(path_name);
        if (file.existsAsFile()) {
            return true;
        } else {
            //std::cerr << "ERROR : cannot open '" << path_name << "'" << std::endl;
            return false;
        }
    }
    
    void getParamsFile(const std::string& path_name, int& channels, int& length) override
    {
        std::unique_ptr<juce::AudioFormatReader> formatReader (fFormatManager.createReaderFor (juce::File::getCurrentWorkingDirectory().getChildFile(path_name)));
        channels = int(formatReader->numChannels);
        length = int(formatReader->lengthInSamples);
    }
    
    void readFile(Soundfile* soundfile, const std::string& path_name, int part, int& offset, int max_chan) override
    {
        std::unique_ptr<juce::AudioFormatReader> formatReader (fFormatManager.createReaderFor (juce::File::getCurrentWorkingDirectory().getChildFile(path_name)));
        
        soundfile->fLength[part] = int(formatReader->lengthInSamples);
        soundfile->fSR[part] = int(formatReader->sampleRate);
        soundfile->fOffset[part] = offset;
        
        void* buffers;
        if (soundfile->fIsDouble) {
            buffers = alloca(soundfile->fChannels * sizeof(double*));
            soundfile->getBuffersOffsetReal<double>(buffers, offset);
        } else {
            buffers = alloca(soundfile->fChannels * sizeof(float*));
            soundfile->getBuffersOffsetReal<float>(buffers, offset);
        }
        
        if (formatReader->read(reinterpret_cast<int *const *>(buffers), int(formatReader->numChannels), 0, int(formatReader->lengthInSamples), false)) {
            
            // Possibly convert samples
            if (!formatReader->usesFloatingPointData) {
                for (int chan = 0; chan < int(formatReader->numChannels); ++chan) {
                    if (soundfile->fIsDouble) {
                        // TODO
                    } else {
                        float* buffer = &(static_cast<float**>(soundfile->fBuffers))[chan][soundfile->fOffset[part]];
                        juce::FloatVectorOperations::convertFixedToFloat(buffer, reinterpret_cast<const int*>(buffer),
                                                                         1.0f/0x7fffffff, int(formatReader->lengthInSamples));
                    }
                }
            }
            
        } else {
            std::cerr << "Error reading the file : " << path_name << std::endl;
        }
            
        // Update offset
        offset += soundfile->fLength[part];
    }
    
};

#endif
/**************************  END  JuceReader.h **************************/
static JuceReader gReader;
#elif defined(DAISY) || defined(SUPERCOLLIDER)
/************************** BEGIN WaveReader.h **************************
 FAUST Architecture File
 Copyright (C) 2003-2022 GRAME, Centre National de Creation Musicale
 ---------------------------------------------------------------------
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation; either version 2.1 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 
 EXCEPTION : As a special exception, you may create a larger work
 that contains this FAUST architecture section and distribute
 that work under terms of your choice, so long as this FAUST
 architecture section is not modified.
 ********************************************************************/

#ifndef __WaveReader__
#define __WaveReader__

#include <string.h>
#include <assert.h>
#include <stdio.h>


// WAVE file description
typedef struct {
    
    // The canonical WAVE format starts with the RIFF header
    
    /**
     Variable: chunk_id
     Contains the letters "RIFF" in ASCII form (0x52494646 big-endian form).
     **/
    int chunk_id;
    
    /**
     Variable: chunk_size
     36 + SubChunk2Size, or more precisely: 4 + (8 + SubChunk1Size) + (8 + SubChunk2Size)
     This is the size of the rest of the chunk following this number.
     This is the size of the entire file in bytes minus 8 bytes for the
     two fields not included in this count: ChunkID and ChunkSize.
     **/
    int chunk_size;
    
    /**
     Variable: format
     Contains the letters "WAVE" (0x57415645 big-endian form).
     **/
    int format;
    
    // The "WAVE" format consists of two subchunks: "fmt " and "data":
    // The "fmt " subchunk describes the sound data's format:
    
    /**
     Variable: subchunk_1_id
     Contains the letters "fmt " (0x666d7420 big-endian form).
     **/
    int subchunk_1_id;
    
    /**
     Variable: subchunk_1_size
     16 for PCM. This is the size of the rest of the Subchunk which follows this number.
     **/
    int subchunk_1_size;
    
    /**
     Variable: audio_format
     PCM = 1 (i.e. Linear quantization) Values other than 1 indicate some form of compression.
     **/
    short audio_format;
    
    /**
     Variable: num_channels
     Mono = 1, Stereo = 2, etc.
     **/
    short num_channels;
    
    /**
     Variable: sample_rate
     8000, 44100, etc.
     **/
    int sample_rate;
    
    /**
     Variable: byte_rate
     == SampleRate * NumChannels * BitsPerSample/8
     **/
    int byte_rate;
    
    /**
     Variable: block_align
     == NumChannels * BitsPerSample/8
     The number of bytes for one sample including all channels. I wonder what happens
     when this number isn't an integer?
     **/
    short block_align;
    
    /**
     Variable: bits_per_sample
     8 bits = 8, 16 bits = 16, etc.
     **/
    short bits_per_sample;
    
    /**
     Here should come some extra parameters which i will avoid.
     **/
    
    // The "data" subchunk contains the size of the data and the actual sound:
    
    /**
     Variable: subchunk_2_id
     Contains the letters "data" (0x64617461 big-endian form).
     **/
    int subchunk_2_id;
    
    /**
     Variable: subchunk_2_size
     == NumSamples * NumChannels * BitsPerSample/8
     This is the number of bytes in the data. You can also think of this as the size
     of the read of the subchunk following this number.
     **/
    int subchunk_2_size;
    
    /**
     Variable: data
     The actual sound data.
     **/
    char* data;
    
} wave_t;

// Base reader
struct Reader {
    
    wave_t* fWave;

    inline int is_big_endian()
    {
        int a = 1;
        return !((char*)&a)[0];
    }
    
    inline int convert_to_int(char* buffer, int len)
    {
        int a = 0;
        if (!is_big_endian()) {
            for(int i = 0; i < len; i++) {
                ((char*)&a)[i] = buffer[i];
            }
        } else {
            for(int i = 0; i < len; i++) {
                ((char*)&a)[3-i] = buffer[i];
            }
        }
        return a;
    }
    
    Reader()
    {
        fWave = (wave_t*)calloc(1, sizeof(wave_t));
    }

    virtual ~Reader()
    {
        free(fWave->data);
        free(fWave);
    }

    bool load_wave_header()
    {
        char buffer[4];
        
        read(buffer, 4);
        if (strncmp(buffer, "RIFF", 4) != 0) {
            fprintf(stderr, "This is not valid WAV file!\n");
            return false;
        }
        fWave->chunk_id = convert_to_int(buffer, 4);
        
        read(buffer, 4);
        fWave->chunk_size = convert_to_int(buffer, 4);
        
        read(buffer, 4);
        fWave->format = convert_to_int(buffer, 4);
        
        read(buffer, 4);
        fWave->subchunk_1_id = convert_to_int(buffer, 4);
        
        read(buffer, 4);
        fWave->subchunk_1_size = convert_to_int(buffer, 4);
        
        read(buffer, 2);
        fWave->audio_format = convert_to_int(buffer, 2);
        
        read(buffer, 2);
        fWave->num_channels = convert_to_int(buffer, 2);
        
        read(buffer, 4);
        fWave->sample_rate = convert_to_int(buffer, 4);
        
        read(buffer, 4);
        fWave->byte_rate = convert_to_int(buffer, 4);
        
        read(buffer, 2);
        fWave->block_align = convert_to_int(buffer, 2);
        
        read(buffer, 2);
        fWave->bits_per_sample = convert_to_int(buffer, 2);
        
        read(buffer, 4);
        if (strncmp(buffer, "data", 4) != 0) {
            read(buffer, 4);
            int _extra_size = convert_to_int(buffer, 4);
            char _extra_data[_extra_size];
            read(_extra_data, _extra_size);
            read(buffer, 4);
            fWave->subchunk_2_id = convert_to_int(buffer, 4);
        } else {
            fWave->subchunk_2_id = convert_to_int(buffer, 4);
        }
        
        read(buffer, 4);
        fWave->subchunk_2_size = convert_to_int(buffer, 4);
        return true;
    }
    
    void load_wave()
    {
        // Read sound data
        fWave->data = (char*)malloc(fWave->subchunk_2_size);
        read(fWave->data, fWave->subchunk_2_size);
    }

    virtual void read(char* buffer, unsigned int size) = 0;
   
};

struct FileReader : public Reader {
    
    FILE* fFile;
    
    FileReader(const std::string& file_path)
    {
        fFile = fopen(file_path.c_str(), "rb");
        if (!fFile) {
            fprintf(stderr, "FileReader : cannot open file!\n");
            throw -1;
        }
        if (!load_wave_header()) {
            fprintf(stderr, "FileReader : not a WAV file!\n");
            throw -1;
        }
    }
    
    virtual ~FileReader()
    {
        fclose(fFile);
    }
    
    void read(char* buffer, unsigned int size)
    {
        fread(buffer, 1, size, fFile);
    }
    
};

extern const uint8_t file_start[] asm("_binary_FILE_start");
extern const uint8_t file_end[]   asm("_binary_FILE_end");

struct MemoryReader : public Reader {
    
    int fPos;
    const uint8_t* fStart;
    const uint8_t* fEnd;
    
    MemoryReader(const uint8_t* start, const uint8_t* end):fPos(0)
    {
        fStart = start;
        fEnd = end;
        if (!load_wave_header()) {
            fprintf(stderr, "MemoryReader : not a WAV file!\n");
            throw -1;
        }
    }
    
    virtual ~MemoryReader()
    {}
    
    void read(char* buffer, unsigned int size)
    {
        memcpy(buffer, fStart + fPos, size);
        fPos += size;
    }
    
};

// Using a FileReader to implement SoundfileReader

struct WaveReader : public SoundfileReader {
    
    WaveReader() {}
    virtual ~WaveReader() {}
    
    bool checkFile(const std::string& path_name) override
    {
        try {
            FileReader reader(path_name);
            return true;
        } catch (...)  {
            return false;
        }
    }
    
    void getParamsFile(const std::string& path_name, int& channels, int& length) override
    {
        FileReader reader(path_name);
        channels = reader.fWave->num_channels;
        length = (reader.fWave->subchunk_2_size * 8) / (reader.fWave->num_channels * reader.fWave->bits_per_sample);
    }
    
    void readFile(Soundfile* soundfile, const std::string& path_name, int part, int& offset, int max_chan) override
    {
        FileReader reader(path_name);
        reader.load_wave();
        
        soundfile->fLength[part] = (reader.fWave->subchunk_2_size * 8) / (reader.fWave->num_channels * reader.fWave->bits_per_sample);
        soundfile->fSR[part] = reader.fWave->sample_rate;
        soundfile->fOffset[part] = offset;
        
        // Audio frames have to be written for each chan
        if (reader.fWave->bits_per_sample == 16) {
            float factor = 1.f/32767.f;
            for (int sample = 0; sample < soundfile->fLength[part]; sample++) {
                short* frame = (short*)&reader.fWave->data[reader.fWave->block_align * sample];
                if (soundfile->fIsDouble) {
                    for (int chan = 0; chan < reader.fWave->num_channels; chan++) {
                        static_cast<double**>(soundfile->fBuffers)[chan][offset + sample] = frame[chan] * factor;
                    }
                } else {
                    for (int chan = 0; chan < reader.fWave->num_channels; chan++) {
                        static_cast<float**>(soundfile->fBuffers)[chan][offset + sample] = frame[chan] * factor;
                    }
                }
            }
        } else if (reader.fWave->bits_per_sample == 32) {
            fprintf(stderr, "readFile : not implemented\n");
        }
        
        // Update offset
        offset += soundfile->fLength[part];
    }
};

#endif
/**************************  END  WaveReader.h **************************/
static WaveReader gReader;
#elif defined(ESP32)
/************************** BEGIN Esp32Reader.h **************************
 FAUST Architecture File
 Copyright (C) 2003-2022 GRAME, Centre National de Creation Musicale
 ---------------------------------------------------------------------
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation; either version 2.1 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 
 EXCEPTION : As a special exception, you may create a larger work
 that contains this FAUST architecture section and distribute
 that work under terms of your choice, so long as this FAUST
 architecture section is not modified.
 *************************************************************************/

#ifndef FAUST_ESP32READER_H
#define FAUST_ESP32READER_H

#include <stdio.h>
#include "esp_err.h"
#include "esp_log.h"
#include "esp_spi_flash.h"
#include "esp_vfs_fat.h"
#include "driver/sdspi_host.h"
#include "sdmmc_cmd.h"


#define TAG "Esp32Reader"

#define SD_PIN_NUM_MISO GPIO_NUM_2
#define SD_PIN_NUM_MOSI GPIO_NUM_15
#define SD_PIN_NUM_CLK  GPIO_NUM_14
#define SD_PIN_NUM_CS   GPIO_NUM_13

struct Esp32Reader : public WaveReader {
    
    void sdcard_init()
    {
        ESP_LOGI(TAG, "Initializing SD card");
        ESP_LOGI(TAG, "Using SPI peripheral");
        
        sdmmc_host_t host = SDSPI_HOST_DEFAULT();
        sdspi_slot_config_t slot_config = SDSPI_SLOT_CONFIG_DEFAULT();
        slot_config.gpio_miso = SD_PIN_NUM_MISO;
        slot_config.gpio_mosi = SD_PIN_NUM_MOSI;
        slot_config.gpio_sck  = SD_PIN_NUM_CLK;
        slot_config.gpio_cs   = SD_PIN_NUM_CS;
        // This initializes the slot without card detect (CD) and write protect (WP) signals.
        // Modify slot_config.gpio_cd and slot_config.gpio_wp if your board has these signals.
        
        // Options for mounting the filesystem.
        // If format_if_mount_failed is set to true, SD card will be partitioned and
        // formatted in case when mounting fails.
        esp_vfs_fat_sdmmc_mount_config_t mount_config = {
            .format_if_mount_failed = false,
            .max_files = 5,
            .allocation_unit_size = 16 * 1024
        };
        
        // Use settings defined above to initialize SD card and mount FAT filesystem.
        // Note: esp_vfs_fat_sdmmc_mount is an all-in-one convenience function.
        // Please check its source code and implement error recovery when developing
        // production applications.
        sdmmc_card_t* card;
        esp_err_t ret = esp_vfs_fat_sdmmc_mount("/sdcard", &host, &slot_config, &mount_config, &card);
        
        if (ret != ESP_OK) {
            if (ret == ESP_FAIL) {
                ESP_LOGE(TAG, "Failed to mount filesystem. "
                         "If you want the card to be formatted, set format_if_mount_failed = true.");
            } else {
                ESP_LOGE(TAG, "Failed to initialize the card (%s). "
                         "Make sure SD card lines have pull-up resistors in place.", esp_err_to_name(ret));
            }
            return;
        }
        
        // Card has been initialized, print its properties
        sdmmc_card_print_info(stdout, card);
        ESP_LOGI(TAG, "SD card initialized");
    }
    
    Esp32Reader()
    {
        sdcard_init();
    }
   
    // Access methods inherited from WaveReader
};

#endif // FAUST_ESP32READER_H
/**************************  END  Esp32Reader.h **************************/
static Esp32Reader gReader;
#elif defined(MEMORY_READER)
/************************** BEGIN MemoryReader.h ************************
 FAUST Architecture File
 Copyright (C) 2003-2022 GRAME, Centre National de Creation Musicale
 ---------------------------------------------------------------------
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation; either version 2.1 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 
 EXCEPTION : As a special exception, you may create a larger work
 that contains this FAUST architecture section and distribute
 that work under terms of your choice, so long as this FAUST
 architecture section is not modified.
 ************************************************************************/

#ifndef __MemoryReader__
#define __MemoryReader__


/*
 A 'MemoryReader' object can be used to prepare a set of sound resources in memory, to be used by SoundUI::addSoundfile.
 
 A Soundfile* object will have to be filled with a list of sound resources: the fLength, fOffset, fSampleRate and fBuffers fields 
 have to be completed with the appropriate values, and will be accessed in the DSP object while running.
 *
 */

// To adapt for a real case use

#define SOUND_CHAN      2
#define SOUND_LENGTH    4096
#define SOUND_SR        44100

struct MemoryReader : public SoundfileReader {
    
    MemoryReader()
    {}
    virtual ~MemoryReader()
    {}
    
    /**
     * Check the availability of a sound resource.
     *
     * @param path_name - the name of the file, or sound resource identified this way
     *
     * @return true if the sound resource is available, false otherwise.
     */
    virtual bool checkFile(const std::string& path_name) override { return true; }
    
    /**
     * Get the channels and length values of the given sound resource.
     *
     * @param path_name - the name of the file, or sound resource identified this way
     * @param channels - the channels value to be filled with the sound resource number of channels
     * @param length - the length value to be filled with the sound resource length in frames
     *
     */
    virtual void getParamsFile(const std::string& path_name, int& channels, int& length) override
    {
        channels = SOUND_CHAN;
        length = SOUND_LENGTH;
    }
    
    /**
     * Read one sound resource and fill the 'soundfile' structure accordingly
     *
     * @param path_name - the name of the file, or sound resource identified this way
     * @param part - the part number to be filled in the soundfile
     * @param offset - the offset value to be incremented with the actual sound resource length in frames
     * @param max_chan - the maximum number of mono channels to fill
     *
     */
    virtual void readFile(Soundfile* soundfile, const std::string& path_name, int part, int& offset, int max_chan) override
    {
        soundfile->fLength[part] = SOUND_LENGTH;
        soundfile->fSR[part] = SOUND_SR;
        soundfile->fOffset[part] = offset;
        
        // Audio frames have to be written for each chan
        if (soundfile->fIsDouble) {
            for (int sample = 0; sample < SOUND_LENGTH; sample++) {
                for (int chan = 0; chan < SOUND_CHAN; chan++) {
                    static_cast<double**>(soundfile->fBuffers)[chan][offset + sample] = 0.f;
                }
            }
        } else {
            for (int sample = 0; sample < SOUND_LENGTH; sample++) {
                for (int chan = 0; chan < SOUND_CHAN; chan++) {
                    static_cast<float**>(soundfile->fBuffers)[chan][offset + sample] = 0.f;
                }
            }
        }
        
        // Update offset
        offset += SOUND_LENGTH;
    }
    
};

#endif
/**************************  END  MemoryReader.h **************************/
static MemoryReader gReader;
#else
/************************** BEGIN LibsndfileReader.h *********************
 FAUST Architecture File
 Copyright (C) 2003-2022 GRAME, Centre National de Creation Musicale
 ---------------------------------------------------------------------
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation; either version 2.1 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 
 EXCEPTION : As a special exception, you may create a larger work
 that contains this FAUST architecture section and distribute
 that work under terms of your choice, so long as this FAUST
 architecture section is not modified.
 ************************************************************************/

#ifndef __LibsndfileReader__
#define __LibsndfileReader__

#ifdef _SAMPLERATE
#include <samplerate.h>
#endif
#include <sndfile.h>
#include <string.h>
#include <assert.h>
#include <iostream>
#include <fstream>


/*
// Deactivated for now, since the macOS remote cross-compiler fails with this code.
#if __has_include(<filesystem>) && __cplusplus >= 201703L
    #define HAS_FILESYSTEM
    #include <filesystem>
    namespace fs = std::filesystem;
#elif __has_include(<experimental/filesystem>) && __cplusplus >= 201103L
    #define HAS_FILESYSTEM
    #include <experimental/filesystem>
    namespace fs = std::experimental::filesystem;
#endif
*/

struct VFLibsndfile {
    
    #define SIGNED_SIZEOF(x) ((int)sizeof(x))
    
    unsigned char* fBuffer;
    size_t fLength;
    size_t fOffset;
    SF_VIRTUAL_IO fVIO;
    
    VFLibsndfile(unsigned char* buffer, size_t length):fBuffer(buffer), fLength(length), fOffset(0)
    {
        fVIO.get_filelen = vfget_filelen;
        fVIO.seek = vfseek;
        fVIO.read = vfread;
        fVIO.write = vfwrite;
        fVIO.tell = vftell;
    }
    
    static sf_count_t vfget_filelen(void* user_data)
    {
        VFLibsndfile* vf = static_cast<VFLibsndfile*>(user_data);
        return vf->fLength;
    }
  
    static sf_count_t vfseek(sf_count_t offset, int whence, void* user_data)
    {
        VFLibsndfile* vf = static_cast<VFLibsndfile*>(user_data);
        switch (whence) {
            case SEEK_SET:
                vf->fOffset = offset;
                break;
                
            case SEEK_CUR:
                vf->fOffset = vf->fOffset + offset;
                break;
                
            case SEEK_END:
                vf->fOffset = vf->fLength + offset;
                break;
                
            default:
                break;
        };
        
        return vf->fOffset;
    }
    
    static sf_count_t vfread(void* ptr, sf_count_t count, void* user_data)
    {
        VFLibsndfile* vf = static_cast<VFLibsndfile*>(user_data);
        
        /*
         **	This will break badly for files over 2Gig in length, but
         **	is sufficient for testing.
         */
        if (vf->fOffset + count > vf->fLength) {
            count = vf->fLength - vf->fOffset;
        }
        
        memcpy(ptr, vf->fBuffer + vf->fOffset, count);
        vf->fOffset += count;
        
        return count;
    }
    
    static sf_count_t vfwrite(const void* ptr, sf_count_t count, void* user_data)
    {
        VFLibsndfile* vf = static_cast<VFLibsndfile*>(user_data);
        
        /*
         **	This will break badly for files over 2Gig in length, but
         **	is sufficient for testing.
         */
        if (vf->fOffset >= SIGNED_SIZEOF(vf->fBuffer)) {
            return 0;
        }
        
        if (vf->fOffset + count > SIGNED_SIZEOF(vf->fBuffer)) {
            count = sizeof (vf->fBuffer) - vf->fOffset;
        }
        
        memcpy(vf->fBuffer + vf->fOffset, ptr, (size_t)count);
        vf->fOffset += count;
        
        if (vf->fOffset > vf->fLength) {
            vf->fLength = vf->fOffset;
        }
        
        return count;
    }
    
    static sf_count_t vftell(void* user_data)
    {
        VFLibsndfile* vf = static_cast<VFLibsndfile*>(user_data);
        return vf->fOffset;
    }
 
};

struct LibsndfileReader : public SoundfileReader {
	
    LibsndfileReader() {}
	
    typedef sf_count_t (* sample_read)(SNDFILE* sndfile, void* buffer, sf_count_t frames);
	
    // Check file
    bool checkFile(const std::string& path_name) override
    {
        /*
         // Better since it supports Unicode characters.
         #ifdef HAS_FILESYSTEM
         if (!fs::exists(path_name)) {
            std::cerr << "FILE NOT FOUND\n";
            return false;
         }
         #endif
        */
    
        std::ifstream ifs(path_name);
        if (!ifs.is_open()) {
            return false;
        } else {
            SF_INFO snd_info;
            snd_info.format = 0;
            SNDFILE* snd_file = sf_open(path_name.c_str(), SFM_READ, &snd_info);
            return checkFileAux(snd_file, path_name);
        }
    }
    
    bool checkFile(unsigned char* buffer, size_t length) override
    {
        SF_INFO snd_info;
        snd_info.format = 0;
        VFLibsndfile vio(buffer, length);
        SNDFILE* snd_file = sf_open_virtual(&vio.fVIO, SFM_READ, &snd_info, &vio);
        return checkFileAux(snd_file, "virtual file");
    }
    
    bool checkFileAux(SNDFILE* snd_file, const std::string& path_name)
    {
        if (snd_file) {
            sf_close(snd_file);
            return true;
        } else {
            std::cerr << "ERROR : cannot open '" << path_name << "' (" << sf_strerror(NULL) << ")" << std::endl;
            return false;
        }
    }

    // Open the file and returns its length and channels
    void getParamsFile(const std::string& path_name, int& channels, int& length) override
    {
        SF_INFO	snd_info;
        snd_info.format = 0;
        SNDFILE* snd_file = sf_open(path_name.c_str(), SFM_READ, &snd_info);
        getParamsFileAux(snd_file, snd_info, channels, length);
    }
    
    void getParamsFile(unsigned char* buffer, size_t size, int& channels, int& length) override
    {
        SF_INFO	snd_info;
        snd_info.format = 0;
        VFLibsndfile vio(buffer, size);
        SNDFILE* snd_file = sf_open_virtual(&vio.fVIO, SFM_READ, &snd_info, &vio);
        getParamsFileAux(snd_file, snd_info, channels, length);
    }
    
    void getParamsFileAux(SNDFILE* snd_file, const SF_INFO& snd_info, int& channels, int& length)
    {
        assert(snd_file);
        channels = int(snd_info.channels);
    #ifdef _SAMPLERATE
        length = (isResampling(snd_info.samplerate)) ? ((double(snd_info.frames) * double(fDriverSR) / double(snd_info.samplerate)) + BUFFER_SIZE) : int(snd_info.frames);
    #else
        length = int(snd_info.frames);
    #endif
        sf_close(snd_file);
    }
    
    // Read the file
    void readFile(Soundfile* soundfile, const std::string& path_name, int part, int& offset, int max_chan) override
    {
        SF_INFO	snd_info;
        snd_info.format = 0;
        SNDFILE* snd_file = sf_open(path_name.c_str(), SFM_READ, &snd_info);
        readFileAux(soundfile, snd_file, snd_info, part, offset, max_chan);
    }
    
    void readFile(Soundfile* soundfile, unsigned char* buffer, size_t length, int part, int& offset, int max_chan) override
    {
        SF_INFO	snd_info;
        snd_info.format = 0;
        VFLibsndfile vio(buffer, length);
        SNDFILE* snd_file = sf_open_virtual(&vio.fVIO, SFM_READ, &snd_info, &vio);
        readFileAux(soundfile, snd_file, snd_info, part, offset, max_chan);
    }
	
    // Will be called to fill all parts from 0 to MAX_SOUNDFILE_PARTS-1
    void readFileAux(Soundfile* soundfile, SNDFILE* snd_file, const SF_INFO& snd_info, int part, int& offset, int max_chan)
    {
        assert(snd_file);
        int channels = std::min<int>(max_chan, snd_info.channels);
    #ifdef _SAMPLERATE
        if (isResampling(snd_info.samplerate)) {
            soundfile->fLength[part] = int(double(snd_info.frames) * double(fDriverSR) / double(snd_info.samplerate));
            soundfile->fSR[part] = fDriverSR;
        } else {
            soundfile->fLength[part] = int(snd_info.frames);
            soundfile->fSR[part] = snd_info.samplerate;
        }
    #else
        soundfile->fLength[part] = int(snd_info.frames);
        soundfile->fSR[part] = snd_info.samplerate;
    #endif
        soundfile->fOffset[part] = offset;
		
        // Read and fill snd_info.channels number of channels
        sf_count_t nbf;
        
        sample_read reader;
        void* buffer_in = nullptr;
        if (soundfile->fIsDouble) {
            buffer_in = static_cast<double*>(alloca(BUFFER_SIZE * sizeof(double) * snd_info.channels));
            reader = reinterpret_cast<sample_read>(sf_readf_double);
        } else {
            buffer_in = static_cast<float*>(alloca(BUFFER_SIZE * sizeof(float) * snd_info.channels));
            reader = reinterpret_cast<sample_read>(sf_readf_float);
        }
        
    #ifdef _SAMPLERATE
        // Resampling
        SRC_STATE* resampler = nullptr;
        float* src_buffer_out = nullptr;
        float* src_buffer_in = nullptr;
        void* buffer_out = nullptr;
        if  (isResampling(snd_info.samplerate)) {
            int error;
            resampler = src_new(SRC_SINC_FASTEST, channels, &error);
            if (error != 0) {
                std::cerr << "ERROR : src_new " << src_strerror(error) << std::endl;
                throw -1;
            }
            if (soundfile->fIsDouble) {
                // Additional buffers for SRC resampling
                src_buffer_in = static_cast<float*>(alloca(BUFFER_SIZE * sizeof(float) * snd_info.channels));
                src_buffer_out = static_cast<float*>(alloca(BUFFER_SIZE * sizeof(float) * snd_info.channels));
                buffer_out = static_cast<double*>(alloca(BUFFER_SIZE * sizeof(double) * snd_info.channels));
            } else {
                buffer_out = static_cast<float*>(alloca(BUFFER_SIZE * sizeof(float) * snd_info.channels));
            }
        }
    #endif
        
        do {
            nbf = reader(snd_file, buffer_in, BUFFER_SIZE);
        #ifdef _SAMPLERATE
            // Resampling
            if  (isResampling(snd_info.samplerate)) {
                int in_offset = 0;
                SRC_DATA src_data;
                src_data.src_ratio = double(fDriverSR)/double(snd_info.samplerate);
                if (soundfile->fIsDouble) {
                    for (int frame = 0; frame < (BUFFER_SIZE * snd_info.channels); frame++) {
                        src_buffer_in[frame] = float(static_cast<float*>(buffer_in)[frame]);
                    }
                }
                do {
                    if (soundfile->fIsDouble) {
                        src_data.data_in = src_buffer_in;
                        src_data.data_out = src_buffer_out;
                    } else {
                        src_data.data_in = static_cast<const float*>(buffer_in);
                        src_data.data_out = static_cast<float*>(buffer_out);
                    }
                    src_data.input_frames = nbf - in_offset;
                    src_data.output_frames = BUFFER_SIZE;
                    src_data.end_of_input = (nbf < BUFFER_SIZE);
                    int res = src_process(resampler, &src_data);
                    if (res != 0) {
                        std::cerr << "ERROR : src_process " << src_strerror(res) << std::endl;
                        throw -1;
                    }
                    if (soundfile->fIsDouble) {
                        for (int frame = 0; frame < (BUFFER_SIZE * snd_info.channels); frame++) {
                            static_cast<double*>(buffer_out)[frame] = double(src_buffer_out[frame]);
                        }
                    }
                    soundfile->copyToOut(src_data.output_frames_gen, channels, snd_info.channels, offset, buffer_out);
                    in_offset += src_data.input_frames_used;
                    // Update offset
                    offset += src_data.output_frames_gen;
                } while (in_offset < nbf);
            } else {
                soundfile->copyToOut(nbf, channels, snd_info.channels, offset, buffer_in);
                // Update offset
                offset += nbf;
            }
        #else
            soundfile->copyToOut(nbf, channels, snd_info.channels, offset, buffer_in);
            // Update offset
            offset += nbf;
        #endif
        } while (nbf == BUFFER_SIZE);
		
        sf_close(snd_file);
    #ifdef _SAMPLERATE
        if (resampler) src_delete(resampler);
    #endif
    }

};

#endif
/**************************  END  LibsndfileReader.h **************************/
static LibsndfileReader gReader;
#endif

// To be used by DSP code if no SoundUI is used
static std::vector<std::string> gPathNameList;
static Soundfile* defaultsound = nullptr;

class SoundUI : public SoundUIInterface
{
		
    protected:
    
        // The soundfile directories
        Soundfile::Directories fSoundfileDir;
        // Map to share loaded soundfiles
        std::map<std::string, std::shared_ptr<Soundfile>> fSoundfileMap;
        // The soundfile reader
        std::shared_ptr<SoundfileReader> fSoundReader;
        bool fIsDouble;

     public:
    
        /**
         * Create a soundfile loader which will typically use a concrete SoundfileReader like LibsndfileReader or JuceReader to load soundfiles.
         *
         * @param sound_directory - the base directory to look for files, which paths will be relative to this one
         * @param sample_rate - the audio driver SR which may be different from the file SR, to possibly resample files
         * @param reader - an alternative soundfile reader
         * @param is_double - whether Faust code has been compiled in -double mode and soundfile buffers have to be in double
         *
         * @return the soundfile loader.
         */
        SoundUI(const std::string& sound_directory = "", int sample_rate = -1, SoundfileReader* reader = nullptr, bool is_double = false)
        {
            fSoundfileDir.push_back(sound_directory);
            fSoundReader = (reader)
                ? std::shared_ptr<SoundfileReader>(reader)
                // the static gReader should not be deleted, so use an empty destructor
                : std::shared_ptr<SoundfileReader>(std::shared_ptr<SoundfileReader>{}, &gReader);
            fSoundReader->setSampleRate(sample_rate);
            fIsDouble = is_double;
            if (!defaultsound) defaultsound = gReader.createSoundfile(gPathNameList, MAX_CHAN, is_double);
        }
    
        /**
         * Create a soundfile loader which will typically use a concrete SoundfileReader like LibsndfileReader or JuceReader to load soundfiles.
         *
         * @param sound_directories - a vector of base directories to look for files, which paths will be relative to these ones
         * @param sample_rate - the audio driver SR which may be different from the file SR, to possibly resample files
         * @param reader - an alternative soundfile reader
         * @param is_double - whether Faust code has been compiled in -double mode and soundfile buffers have to be in double
         *
         * @return the soundfile loader.
         */
        SoundUI(const Soundfile::Directories& sound_directories, int sample_rate = -1, SoundfileReader* reader = nullptr, bool is_double = false)
        :fSoundfileDir(sound_directories)
        {
            fSoundReader = (reader)
                ? std::shared_ptr<SoundfileReader>(reader)
                // the static gReader should not be deleted, so use an empty destructor
                : std::shared_ptr<SoundfileReader>(std::shared_ptr<SoundfileReader>{}, &gReader);
            fSoundReader->setSampleRate(sample_rate);
            fIsDouble = is_double;
            if (!defaultsound) defaultsound = gReader.createSoundfile(gPathNameList, MAX_CHAN, is_double);
        }
    
        virtual ~SoundUI()
        {}

        // -- soundfiles
        virtual void addSoundfile(const char* label, const char* url, Soundfile** sf_zone)
        {
            const char* saved_url = url; // 'url' is consumed by parseMenuList2
            std::vector<std::string> file_name_list;
            
            bool menu = parseMenuList2(url, file_name_list, true);
            // If not a list, we have as single file
            if (!menu) { file_name_list.push_back(saved_url); }
            
            // Parse the possible list
            std::string saved_url_real = std::string(saved_url) + "_" + std::to_string(fIsDouble); // fIsDouble is used in the key
            if (fSoundfileMap.find(saved_url_real) == fSoundfileMap.end()) {
                // Check all files and get their complete path
                std::vector<std::string> path_name_list = fSoundReader->checkFiles(fSoundfileDir, file_name_list);
                // Read them and create the Soundfile
                Soundfile* sound_file = fSoundReader->createSoundfile(path_name_list, MAX_CHAN, fIsDouble);
                if (sound_file) {
                    fSoundfileMap[saved_url_real] = std::shared_ptr<Soundfile>(sound_file);
                } else {
                    // If failure, use 'defaultsound'
                    std::cerr << "addSoundfile : soundfile for " << saved_url << " cannot be created !" << std::endl;
                    *sf_zone = defaultsound;
                    return;
                }
            }
            
            // Get the soundfile pointer
            *sf_zone = fSoundfileMap[saved_url_real].get();
        }
    
        /**
         * An OS dependant function to get the path of the running executable or plugin.
         * This will typically be used when creating a SoundUI soundfile loader, like new SoundUI(SoundUI::getBinaryPath());
         *
         * @return the running executable or plugin path.
         */
        static std::string getBinaryPath()
        {
            std::string bundle_path_str;
        #if defined(__APPLE__) && !defined(__VCVRACK__) && !defined(JUCE_32BIT) && !defined(JUCE_64BIT)
            CFURLRef bundle_ref = CFBundleCopyBundleURL(CFBundleGetMainBundle());
            if (!bundle_ref) { std::cerr << "getBinaryPath CFBundleCopyBundleURL error\n"; return ""; }
      
            UInt8 bundle_path[1024];
            if (CFURLGetFileSystemRepresentation(bundle_ref, true, bundle_path, 1024)) {
                bundle_path_str = std::string((char*)bundle_path);
            } else {
                std::cerr << "getBinaryPath CFURLGetFileSystemRepresentation error\n";
            }
        #endif
        #ifdef ANDROID_DRIVER
            bundle_path_str = "/data/data/__CURRENT_ANDROID_PACKAGE__/files";
        #endif
            return bundle_path_str;
        }
    
        /**
         * An OS dependant function to get the path of the running executable or plugin.
         * This will typically be used when creating a SoundUI soundfile loader, like new SoundUI(SoundUI::getBinaryPathFrom());
         *
         * @param path - entry point to start getting the path of the running executable or plugin.
         *
         * @return the running executable or plugin path.
         */
        static std::string getBinaryPathFrom(const std::string& path)
        {
            std::string bundle_path_str;
        #if defined(__APPLE__) && !defined(__VCVRACK__) && !defined(JUCE_32BIT) && !defined(JUCE_64BIT)
            CFBundleRef bundle = CFBundleGetBundleWithIdentifier(CFStringCreateWithCString(kCFAllocatorDefault, path.c_str(), CFStringGetSystemEncoding()));
            if (!bundle) { std::cerr << "getBinaryPathFrom CFBundleGetBundleWithIdentifier error '" << path << "'" << std::endl; return ""; }
         
            CFURLRef bundle_ref = CFBundleCopyBundleURL(bundle);
            if (!bundle_ref) { std::cerr << "getBinaryPathFrom CFBundleCopyBundleURL error\n"; return ""; }
            
            UInt8 bundle_path[1024];
            if (CFURLGetFileSystemRepresentation(bundle_ref, true, bundle_path, 1024)) {
                bundle_path_str = std::string((char*)bundle_path);
            } else {
                std::cerr << "getBinaryPathFrom CFURLGetFileSystemRepresentation error\n";
            }
        #endif
        #ifdef ANDROID_DRIVER
            bundle_path_str = "/data/data/__CURRENT_ANDROID_PACKAGE__/files";
        #endif
            return bundle_path_str;
        }
};

#endif
/**************************  END  SoundUI.h **************************/
#endif

// For FAUST_CLASS_NAME to be defined
#define FAUST_UIMACROS

// but we will ignore most of them
#define FAUST_ADDBUTTON(l,f)
#define FAUST_ADDCHECKBOX(l,f)
#define FAUST_ADDVERTICALSLIDER(l,f,i,a,b,s)
#define FAUST_ADDHORIZONTALSLIDER(l,f,i,a,b,s)
#define FAUST_ADDNUMENTRY(l,f,i,a,b,s)
#define FAUST_ADDVERTICALBARGRAPH(l,f,a,b)
#define FAUST_ADDHORIZONTALBARGRAPH(l,f,a,b)
#define FAUST_ADDSOUNDFILE(s,f)

using namespace std;

/******************************************************************************
*******************************************************************************

							       VECTOR INTRINSICS

*******************************************************************************
*******************************************************************************/


/********************END ARCHITECTURE SECTION (part 1/2)****************/

/**************************BEGIN USER SECTION **************************/

#ifndef FAUSTFLOAT
#define FAUSTFLOAT float
#endif 

#include <algorithm>
#include <cmath>
#include <cstdint>
#include <math.h>

#ifndef FAUSTCLASS 
#define FAUSTCLASS bbdmi_ambLagrangeMods31
#endif

#ifdef __APPLE__ 
#define exp10f __exp10f
#define exp10 __exp10
#endif

#if defined(_WIN32)
#define RESTRICT __restrict
#else
#define RESTRICT __restrict__
#endif

class bbdmi_ambLagrangeMods31SIG0 {
	
  public:
	
	int iVec1[2];
	int iRec23[2];
	
  public:
	
	int getNumInputsbbdmi_ambLagrangeMods31SIG0() {
		return 0;
	}
	int getNumOutputsbbdmi_ambLagrangeMods31SIG0() {
		return 1;
	}
	
	void instanceInitbbdmi_ambLagrangeMods31SIG0(int sample_rate) {
		for (int l22 = 0; l22 < 2; l22 = l22 + 1) {
			iVec1[l22] = 0;
		}
		for (int l23 = 0; l23 < 2; l23 = l23 + 1) {
			iRec23[l23] = 0;
		}
	}
	
	void fillbbdmi_ambLagrangeMods31SIG0(int count, double* table) {
		for (int i1 = 0; i1 < count; i1 = i1 + 1) {
			iVec1[0] = 1;
			iRec23[0] = (iVec1[1] + iRec23[1]) % 65536;
			table[i1] = std::sin(9.587379924285257e-05 * double(iRec23[0]));
			iVec1[1] = iVec1[0];
			iRec23[1] = iRec23[0];
		}
	}

};

static bbdmi_ambLagrangeMods31SIG0* newbbdmi_ambLagrangeMods31SIG0() { return (bbdmi_ambLagrangeMods31SIG0*)new bbdmi_ambLagrangeMods31SIG0(); }
static void deletebbdmi_ambLagrangeMods31SIG0(bbdmi_ambLagrangeMods31SIG0* dsp) { delete dsp; }

static double ftbl0bbdmi_ambLagrangeMods31SIG0[65536];
static double bbdmi_ambLagrangeMods31_faustpower2_f(double value) {
	return value * value;
}

class bbdmi_ambLagrangeMods31 : public dsp {
	
 public:
	
	int iVec0[2];
	int fSampleRate;
	double fConst0;
	double fConst1;
	FAUSTFLOAT fVslider0;
	double fConst2;
	double fRec8[2];
	double fConst3;
	FAUSTFLOAT fVslider1;
	double fRec9[2];
	double fConst4;
	double fConst5;
	FAUSTFLOAT fVslider2;
	double fRec11[2];
	double fRec10[2];
	FAUSTFLOAT fButton0;
	int iRec14[2];
	double fRec13[2];
	double fRec12[2];
	double fRec7[2];
	FAUSTFLOAT fVslider3;
	double fRec15[2];
	double fRec16[2];
	double fRec6[2];
	double fRec2[2];
	double fRec3[2];
	int iRec4[2];
	int iRec5[2];
	FAUSTFLOAT fVslider4;
	double fRec18[2];
	double fRec19[2];
	FAUSTFLOAT fCheckbox0;
	double fRec20[2];
	double fRec17[2];
	FAUSTFLOAT fVslider5;
	double fRec21[2];
	double fRec22[2];
	FAUSTFLOAT fVslider6;
	double fRec25[2];
	double fRec26[2];
	double fRec24[2];
	FAUSTFLOAT fVslider7;
	double fRec27[2];
	double fRec28[2];
	FAUSTFLOAT fVslider8;
	double fRec29[2];
	double fRec30[2];
	FAUSTFLOAT fVslider9;
	double fRec31[2];
	double fRec32[2];
	int IOTA0;
	double fVec2[262144];
	double fVec3[131072];
	FAUSTFLOAT fVslider10;
	double fRec34[2];
	double fRec35[2];
	FAUSTFLOAT fVslider11;
	double fRec36[2];
	double fRec37[2];
	double fRec33[2];
	double fVec4[2];
	double fRec1[2];
	double fRec44[2];
	double fRec43[2];
	double fRec39[2];
	double fRec40[2];
	int iRec41[2];
	int iRec42[2];
	double fVec5[262144];
	double fVec6[131072];
	double fRec45[2];
	double fVec7[2];
	double fRec38[2];
	double fVec8[2];
	double fRec0[2];
	double fRec53[2];
	double fRec52[2];
	double fRec48[2];
	double fRec49[2];
	int iRec50[2];
	int iRec51[2];
	double fVec9[262144];
	double fVec10[131072];
	double fRec54[2];
	double fVec11[2];
	double fRec47[2];
	double fRec61[2];
	double fRec60[2];
	double fRec56[2];
	double fRec57[2];
	int iRec58[2];
	int iRec59[2];
	double fVec12[262144];
	double fVec13[131072];
	double fRec62[2];
	double fVec14[2];
	double fRec55[2];
	double fVec15[2];
	double fRec46[2];
	double fRec70[2];
	double fRec69[2];
	double fRec65[2];
	double fRec66[2];
	int iRec67[2];
	int iRec68[2];
	double fVec16[262144];
	double fVec17[131072];
	double fRec71[2];
	double fVec18[2];
	double fRec64[2];
	double fRec78[2];
	double fRec77[2];
	double fRec73[2];
	double fRec74[2];
	int iRec75[2];
	int iRec76[2];
	double fVec19[262144];
	double fVec20[131072];
	double fRec79[2];
	double fVec21[2];
	double fRec72[2];
	double fVec22[2];
	double fRec63[2];
	double fRec87[2];
	double fRec86[2];
	double fRec82[2];
	double fRec83[2];
	int iRec84[2];
	int iRec85[2];
	double fVec23[262144];
	double fVec24[131072];
	double fRec88[2];
	double fVec25[2];
	double fRec81[2];
	double fRec95[2];
	double fRec94[2];
	double fRec90[2];
	double fRec91[2];
	int iRec92[2];
	int iRec93[2];
	double fVec26[262144];
	double fVec27[131072];
	double fRec96[2];
	double fVec28[2];
	double fRec89[2];
	double fVec29[2];
	double fRec80[2];
	double fRec104[2];
	double fRec103[2];
	double fRec99[2];
	double fRec100[2];
	int iRec101[2];
	int iRec102[2];
	double fVec30[262144];
	double fVec31[131072];
	double fRec105[2];
	double fVec32[2];
	double fRec98[2];
	double fRec112[2];
	double fRec111[2];
	double fRec107[2];
	double fRec108[2];
	int iRec109[2];
	int iRec110[2];
	double fVec33[262144];
	double fVec34[131072];
	double fRec113[2];
	double fVec35[2];
	double fRec106[2];
	double fVec36[2];
	double fRec97[2];
	double fRec121[2];
	double fRec120[2];
	double fRec116[2];
	double fRec117[2];
	int iRec118[2];
	int iRec119[2];
	double fVec37[262144];
	double fVec38[131072];
	double fRec122[2];
	double fVec39[2];
	double fRec115[2];
	double fRec129[2];
	double fRec128[2];
	double fRec124[2];
	double fRec125[2];
	int iRec126[2];
	int iRec127[2];
	double fVec40[262144];
	double fVec41[131072];
	double fRec130[2];
	double fVec42[2];
	double fRec123[2];
	double fVec43[2];
	double fRec114[2];
	double fRec138[2];
	double fRec137[2];
	double fRec133[2];
	double fRec134[2];
	int iRec135[2];
	int iRec136[2];
	double fVec44[262144];
	double fVec45[131072];
	double fRec139[2];
	double fVec46[2];
	double fRec132[2];
	double fRec146[2];
	double fRec145[2];
	double fRec141[2];
	double fRec142[2];
	int iRec143[2];
	int iRec144[2];
	double fVec47[262144];
	double fVec48[131072];
	double fRec147[2];
	double fVec49[2];
	double fRec140[2];
	double fVec50[2];
	double fRec131[2];
	double fRec155[2];
	double fRec154[2];
	double fRec150[2];
	double fRec151[2];
	int iRec152[2];
	int iRec153[2];
	double fVec51[262144];
	double fVec52[131072];
	double fRec156[2];
	double fVec53[2];
	double fRec149[2];
	double fRec163[2];
	double fRec162[2];
	double fRec158[2];
	double fRec159[2];
	int iRec160[2];
	int iRec161[2];
	double fVec54[262144];
	double fVec55[131072];
	double fRec164[2];
	double fVec56[2];
	double fRec157[2];
	double fVec57[2];
	double fRec148[2];
	double fRec172[2];
	double fRec171[2];
	double fRec167[2];
	double fRec168[2];
	int iRec169[2];
	int iRec170[2];
	double fVec58[262144];
	double fVec59[131072];
	double fRec173[2];
	double fVec60[2];
	double fRec166[2];
	double fRec180[2];
	double fRec179[2];
	double fRec175[2];
	double fRec176[2];
	int iRec177[2];
	int iRec178[2];
	double fVec61[262144];
	double fVec62[131072];
	double fRec181[2];
	double fVec63[2];
	double fRec174[2];
	double fVec64[2];
	double fRec165[2];
	double fRec189[2];
	double fRec188[2];
	double fRec184[2];
	double fRec185[2];
	int iRec186[2];
	int iRec187[2];
	double fVec65[262144];
	double fVec66[131072];
	double fRec190[2];
	double fVec67[2];
	double fRec183[2];
	double fRec197[2];
	double fRec196[2];
	double fRec192[2];
	double fRec193[2];
	int iRec194[2];
	int iRec195[2];
	double fVec68[262144];
	double fVec69[131072];
	double fRec198[2];
	double fVec70[2];
	double fRec191[2];
	double fVec71[2];
	double fRec182[2];
	double fRec206[2];
	double fRec205[2];
	double fRec201[2];
	double fRec202[2];
	int iRec203[2];
	int iRec204[2];
	double fVec72[262144];
	double fVec73[131072];
	double fRec207[2];
	double fVec74[2];
	double fRec200[2];
	double fRec214[2];
	double fRec213[2];
	double fRec209[2];
	double fRec210[2];
	int iRec211[2];
	int iRec212[2];
	double fVec75[262144];
	double fVec76[131072];
	double fRec215[2];
	double fVec77[2];
	double fRec208[2];
	double fVec78[2];
	double fRec199[2];
	double fRec223[2];
	double fRec222[2];
	double fRec218[2];
	double fRec219[2];
	int iRec220[2];
	int iRec221[2];
	double fVec79[262144];
	double fVec80[131072];
	double fRec224[2];
	double fVec81[2];
	double fRec217[2];
	double fRec231[2];
	double fRec230[2];
	double fRec226[2];
	double fRec227[2];
	int iRec228[2];
	int iRec229[2];
	double fVec82[262144];
	double fVec83[131072];
	double fRec232[2];
	double fVec84[2];
	double fRec225[2];
	double fVec85[2];
	double fRec216[2];
	double fRec240[2];
	double fRec239[2];
	double fRec235[2];
	double fRec236[2];
	int iRec237[2];
	int iRec238[2];
	double fVec86[262144];
	double fVec87[131072];
	double fRec241[2];
	double fVec88[2];
	double fRec234[2];
	double fRec248[2];
	double fRec247[2];
	double fRec243[2];
	double fRec244[2];
	int iRec245[2];
	int iRec246[2];
	double fVec89[262144];
	double fVec90[131072];
	double fRec249[2];
	double fVec91[2];
	double fRec242[2];
	double fVec92[2];
	double fRec233[2];
	double fRec257[2];
	double fRec256[2];
	double fRec252[2];
	double fRec253[2];
	int iRec254[2];
	int iRec255[2];
	double fVec93[262144];
	double fVec94[131072];
	double fRec258[2];
	double fVec95[2];
	double fRec251[2];
	double fRec265[2];
	double fRec264[2];
	double fRec260[2];
	double fRec261[2];
	int iRec262[2];
	int iRec263[2];
	double fVec96[262144];
	double fVec97[131072];
	double fRec266[2];
	double fVec98[2];
	double fRec259[2];
	double fVec99[2];
	double fRec250[2];
	double fRec274[2];
	double fRec273[2];
	double fRec269[2];
	double fRec270[2];
	int iRec271[2];
	int iRec272[2];
	double fVec100[262144];
	double fVec101[131072];
	double fRec275[2];
	double fVec102[2];
	double fRec268[2];
	double fRec282[2];
	double fRec281[2];
	double fRec277[2];
	double fRec278[2];
	int iRec279[2];
	int iRec280[2];
	double fVec103[262144];
	double fVec104[131072];
	double fRec283[2];
	double fVec105[2];
	double fRec276[2];
	double fVec106[2];
	double fRec267[2];
	double fRec291[2];
	double fRec290[2];
	double fRec286[2];
	double fRec287[2];
	int iRec288[2];
	int iRec289[2];
	double fVec107[262144];
	double fVec108[131072];
	double fRec292[2];
	double fVec109[2];
	double fRec285[2];
	double fRec299[2];
	double fRec298[2];
	double fRec294[2];
	double fRec295[2];
	int iRec296[2];
	int iRec297[2];
	double fVec110[262144];
	double fVec111[131072];
	double fRec300[2];
	double fVec112[2];
	double fRec293[2];
	double fVec113[2];
	double fRec284[2];
	double fRec308[2];
	double fRec307[2];
	double fRec303[2];
	double fRec304[2];
	int iRec305[2];
	int iRec306[2];
	double fVec114[262144];
	double fVec115[131072];
	double fRec309[2];
	double fVec116[2];
	double fRec302[2];
	double fRec316[2];
	double fRec315[2];
	double fRec311[2];
	double fRec312[2];
	int iRec313[2];
	int iRec314[2];
	double fVec117[262144];
	double fVec118[131072];
	double fRec317[2];
	double fVec119[2];
	double fRec310[2];
	double fVec120[2];
	double fRec301[2];
	double fRec325[2];
	double fRec324[2];
	double fRec320[2];
	double fRec321[2];
	int iRec322[2];
	int iRec323[2];
	double fVec121[262144];
	double fVec122[131072];
	double fRec326[2];
	double fVec123[2];
	double fRec319[2];
	double fRec333[2];
	double fRec332[2];
	double fRec328[2];
	double fRec329[2];
	int iRec330[2];
	int iRec331[2];
	double fVec124[262144];
	double fVec125[131072];
	double fRec334[2];
	double fVec126[2];
	double fRec327[2];
	double fVec127[2];
	double fRec318[2];
	double fRec342[2];
	double fRec341[2];
	double fRec337[2];
	double fRec338[2];
	int iRec339[2];
	int iRec340[2];
	double fVec128[262144];
	double fVec129[131072];
	double fRec343[2];
	double fVec130[2];
	double fRec336[2];
	double fRec350[2];
	double fRec349[2];
	double fRec345[2];
	double fRec346[2];
	int iRec347[2];
	int iRec348[2];
	double fVec131[262144];
	double fVec132[131072];
	double fRec351[2];
	double fVec133[2];
	double fRec344[2];
	double fVec134[2];
	double fRec335[2];
	double fRec359[2];
	double fRec358[2];
	double fRec354[2];
	double fRec355[2];
	int iRec356[2];
	int iRec357[2];
	double fVec135[262144];
	double fVec136[131072];
	double fRec360[2];
	double fVec137[2];
	double fRec353[2];
	double fRec367[2];
	double fRec366[2];
	double fRec362[2];
	double fRec363[2];
	int iRec364[2];
	int iRec365[2];
	double fVec138[262144];
	double fVec139[131072];
	double fRec368[2];
	double fVec140[2];
	double fRec361[2];
	double fVec141[2];
	double fRec352[2];
	double fRec376[2];
	double fRec375[2];
	double fRec371[2];
	double fRec372[2];
	int iRec373[2];
	int iRec374[2];
	double fVec142[262144];
	double fVec143[131072];
	double fRec377[2];
	double fVec144[2];
	double fRec370[2];
	double fRec384[2];
	double fRec383[2];
	double fRec379[2];
	double fRec380[2];
	int iRec381[2];
	int iRec382[2];
	double fVec145[262144];
	double fVec146[131072];
	double fRec385[2];
	double fVec147[2];
	double fRec378[2];
	double fVec148[2];
	double fRec369[2];
	double fRec393[2];
	double fRec392[2];
	double fRec388[2];
	double fRec389[2];
	int iRec390[2];
	int iRec391[2];
	double fVec149[262144];
	double fVec150[131072];
	double fRec394[2];
	double fVec151[2];
	double fRec387[2];
	double fRec401[2];
	double fRec400[2];
	double fRec396[2];
	double fRec397[2];
	int iRec398[2];
	int iRec399[2];
	double fVec152[262144];
	double fVec153[131072];
	double fRec402[2];
	double fVec154[2];
	double fRec395[2];
	double fVec155[2];
	double fRec386[2];
	double fRec410[2];
	double fRec409[2];
	double fRec405[2];
	double fRec406[2];
	int iRec407[2];
	int iRec408[2];
	double fVec156[262144];
	double fVec157[131072];
	double fRec411[2];
	double fVec158[2];
	double fRec404[2];
	double fRec418[2];
	double fRec417[2];
	double fRec413[2];
	double fRec414[2];
	int iRec415[2];
	int iRec416[2];
	double fVec159[262144];
	double fVec160[131072];
	double fRec419[2];
	double fVec161[2];
	double fRec412[2];
	double fVec162[2];
	double fRec403[2];
	double fRec427[2];
	double fRec426[2];
	double fRec422[2];
	double fRec423[2];
	int iRec424[2];
	int iRec425[2];
	double fVec163[262144];
	double fVec164[131072];
	double fRec428[2];
	double fVec165[2];
	double fRec421[2];
	double fRec435[2];
	double fRec434[2];
	double fRec430[2];
	double fRec431[2];
	int iRec432[2];
	int iRec433[2];
	double fVec166[262144];
	double fVec167[131072];
	double fRec436[2];
	double fVec168[2];
	double fRec429[2];
	double fVec169[2];
	double fRec420[2];
	double fRec444[2];
	double fRec443[2];
	double fRec439[2];
	double fRec440[2];
	int iRec441[2];
	int iRec442[2];
	double fVec170[262144];
	double fVec171[131072];
	double fRec445[2];
	double fVec172[2];
	double fRec438[2];
	double fRec452[2];
	double fRec451[2];
	double fRec447[2];
	double fRec448[2];
	int iRec449[2];
	int iRec450[2];
	double fVec173[262144];
	double fVec174[131072];
	double fRec453[2];
	double fVec175[2];
	double fRec446[2];
	double fVec176[2];
	double fRec437[2];
	double fRec461[2];
	double fRec460[2];
	double fRec456[2];
	double fRec457[2];
	int iRec458[2];
	int iRec459[2];
	double fVec177[262144];
	double fVec178[131072];
	double fRec462[2];
	double fVec179[2];
	double fRec455[2];
	double fRec469[2];
	double fRec468[2];
	double fRec464[2];
	double fRec465[2];
	int iRec466[2];
	int iRec467[2];
	double fVec180[262144];
	double fVec181[131072];
	double fRec470[2];
	double fVec182[2];
	double fRec463[2];
	double fVec183[2];
	double fRec454[2];
	double fRec478[2];
	double fRec477[2];
	double fRec473[2];
	double fRec474[2];
	int iRec475[2];
	int iRec476[2];
	double fVec184[262144];
	double fVec185[131072];
	double fRec479[2];
	double fVec186[2];
	double fRec472[2];
	double fRec486[2];
	double fRec485[2];
	double fRec481[2];
	double fRec482[2];
	int iRec483[2];
	int iRec484[2];
	double fVec187[262144];
	double fVec188[131072];
	double fRec487[2];
	double fVec189[2];
	double fRec480[2];
	double fVec190[2];
	double fRec471[2];
	double fRec495[2];
	double fRec494[2];
	double fRec490[2];
	double fRec491[2];
	int iRec492[2];
	int iRec493[2];
	double fVec191[262144];
	double fVec192[131072];
	double fRec496[2];
	double fVec193[2];
	double fRec489[2];
	double fRec503[2];
	double fRec502[2];
	double fRec498[2];
	double fRec499[2];
	int iRec500[2];
	int iRec501[2];
	double fVec194[262144];
	double fVec195[131072];
	double fRec504[2];
	double fVec196[2];
	double fRec497[2];
	double fVec197[2];
	double fRec488[2];
	double fRec512[2];
	double fRec511[2];
	double fRec507[2];
	double fRec508[2];
	int iRec509[2];
	int iRec510[2];
	double fVec198[262144];
	double fVec199[131072];
	double fRec513[2];
	double fVec200[2];
	double fRec506[2];
	double fRec520[2];
	double fRec519[2];
	double fRec515[2];
	double fRec516[2];
	int iRec517[2];
	int iRec518[2];
	double fVec201[262144];
	double fVec202[131072];
	double fRec521[2];
	double fVec203[2];
	double fRec514[2];
	double fVec204[2];
	double fRec505[2];
	double fRec529[2];
	double fRec528[2];
	double fRec524[2];
	double fRec525[2];
	int iRec526[2];
	int iRec527[2];
	double fVec205[262144];
	double fVec206[131072];
	double fRec530[2];
	double fVec207[2];
	double fRec523[2];
	double fRec537[2];
	double fRec536[2];
	double fRec532[2];
	double fRec533[2];
	int iRec534[2];
	int iRec535[2];
	double fVec208[262144];
	double fVec209[131072];
	double fRec538[2];
	double fVec210[2];
	double fRec531[2];
	double fVec211[2];
	double fRec522[2];
	double fRec546[2];
	double fRec545[2];
	double fRec541[2];
	double fRec542[2];
	int iRec543[2];
	int iRec544[2];
	double fVec212[262144];
	double fVec213[131072];
	double fRec547[2];
	double fVec214[2];
	double fRec540[2];
	double fRec554[2];
	double fRec553[2];
	double fRec549[2];
	double fRec550[2];
	int iRec551[2];
	int iRec552[2];
	double fVec215[262144];
	double fVec216[131072];
	double fRec555[2];
	double fVec217[2];
	double fRec548[2];
	double fVec218[2];
	double fRec539[2];
	
 public:
	bbdmi_ambLagrangeMods31() {}

	void metadata(Meta* m) { 
		m->declare("author", "Alain Bonardi");
		m->declare("contributor", "David Fierro");
		m->declare("contributor", "Anne Sedes");
		m->declare("contributor", "Atau Tanaka");
		m->declare("contributor", "Stephen Whitmarsch");
		m->declare("contributor", "Francesco Di Maggio");
		m->declare("basics.lib/name", "Faust Basic Element Library");
		m->declare("basics.lib/sAndH:author", "Romain Michon");
		m->declare("basics.lib/tabulateNd", "Copyright (C) 2023 Bart Brouns <bart@magnetophon.nl>");
		m->declare("basics.lib/version", "1.15.0");
		m->declare("bbdmi.lib/author", "Alain Bonardi");
		m->declare("bbdmi.lib/copyright", "2022-2025 BBDMI TEAM");
		m->declare("bbdmi.lib/licence", "LGPLv3");
		m->declare("bbdmi.lib/name", "BBDMI Faust Lib");
		m->declare("compile_options", "-a /Applications/Faust-2.72.14/share/faust/max-msp/max-msp64.cpp -lang cpp -i -ct 1 -cn bbdmi_ambLagrangeMods31 -es 1 -mcd 16 -mdd 1024 -mdy 33 -uim -double -ftz 0");
		m->declare("copyright", "2022-2025 BBDMI TEAM");
		m->declare("delays.lib/name", "Faust Delay Library");
		m->declare("delays.lib/version", "1.1.0");
		m->declare("filename", "bbdmi_ambLagrangeMods31.dsp");
		m->declare("filters.lib/avg_tau:author", "Dario Sanfilippo and Julius O. Smith III");
		m->declare("filters.lib/avg_tau:copyright", "Copyright (C) 2020 Dario Sanfilippo        <sanfilippo.dario@gmail.com> and         2003-2020 by Julius O. Smith III <jos@ccrma.stanford.edu>");
		m->declare("filters.lib/avg_tau:license", "MIT-style STK-4.3 license");
		m->declare("filters.lib/dcblocker:author", "Julius O. Smith III");
		m->declare("filters.lib/dcblocker:copyright", "Copyright (C) 2003-2019 by Julius O. Smith III <jos@ccrma.stanford.edu>");
		m->declare("filters.lib/dcblocker:license", "MIT-style STK-4.3 license");
		m->declare("filters.lib/lowpass0_highpass1", "MIT-style STK-4.3 license");
		m->declare("filters.lib/lptN:author", "Julius O. Smith III");
		m->declare("filters.lib/lptN:copyright", "Copyright (C) 2003-2019 by Julius O. Smith III <jos@ccrma.stanford.edu>");
		m->declare("filters.lib/lptN:license", "MIT-style STK-4.3 license");
		m->declare("filters.lib/name", "Faust Filters Library");
		m->declare("filters.lib/pole:author", "Julius O. Smith III");
		m->declare("filters.lib/pole:copyright", "Copyright (C) 2003-2019 by Julius O. Smith III <jos@ccrma.stanford.edu>");
		m->declare("filters.lib/pole:license", "MIT-style STK-4.3 license");
		m->declare("filters.lib/version", "1.3.0");
		m->declare("filters.lib/zero:author", "Julius O. Smith III");
		m->declare("filters.lib/zero:copyright", "Copyright (C) 2003-2019 by Julius O. Smith III <jos@ccrma.stanford.edu>");
		m->declare("filters.lib/zero:license", "MIT-style STK-4.3 license");
		m->declare("interpolators.lib/interpolate_linear:author", "Stéphane Letz");
		m->declare("interpolators.lib/interpolate_linear:licence", "MIT");
		m->declare("interpolators.lib/lagrangeCoeffs:author", "Dario Sanfilippo");
		m->declare("interpolators.lib/lagrangeCoeffs:copyright", "Copyright (C) 2021 Dario Sanfilippo     <sanfilippo.dario@gmail.com>");
		m->declare("interpolators.lib/lagrangeCoeffs:license", "MIT license");
		m->declare("interpolators.lib/lagrangeInterpolation:author", "Dario Sanfilippo");
		m->declare("interpolators.lib/lagrangeInterpolation:copyright", "Copyright (C) 2021 Dario Sanfilippo     <sanfilippo.dario@gmail.com>");
		m->declare("interpolators.lib/lagrangeInterpolation:license", "MIT license");
		m->declare("interpolators.lib/name", "Faust Interpolator Library");
		m->declare("interpolators.lib/version", "1.3.1");
		m->declare("maths.lib/author", "GRAME");
		m->declare("maths.lib/copyright", "GRAME");
		m->declare("maths.lib/license", "LGPL with exception");
		m->declare("maths.lib/name", "Faust Math Library");
		m->declare("maths.lib/version", "2.8.0");
		m->declare("misceffects.lib/name", "Misc Effects Library");
		m->declare("misceffects.lib/version", "2.4.0");
		m->declare("name", "BBDMI Faust Lib");
		m->declare("noises.lib/name", "Faust Noise Generator Library");
		m->declare("noises.lib/version", "1.4.1");
		m->declare("oscillators.lib/name", "Faust Oscillator Library");
		m->declare("oscillators.lib/version", "1.5.1");
		m->declare("platform.lib/name", "Generic Platform Library");
		m->declare("platform.lib/version", "1.3.0");
		m->declare("routes.lib/name", "Faust Signal Routing Library");
		m->declare("routes.lib/version", "1.2.0");
		m->declare("signals.lib/name", "Faust Signal Routing Library");
		m->declare("signals.lib/version", "1.5.0");
	}

	virtual int getNumInputs() {
		return 0;
	}
	virtual int getNumOutputs() {
		return 31;
	}
	
	static void classInit(int sample_rate) {
		bbdmi_ambLagrangeMods31SIG0* sig0 = newbbdmi_ambLagrangeMods31SIG0();
		sig0->instanceInitbbdmi_ambLagrangeMods31SIG0(sample_rate);
		sig0->fillbbdmi_ambLagrangeMods31SIG0(65536, ftbl0bbdmi_ambLagrangeMods31SIG0);
		deletebbdmi_ambLagrangeMods31SIG0(sig0);
	}
	
	virtual void instanceConstants(int sample_rate) {
		fSampleRate = sample_rate;
		fConst0 = std::min<double>(1.92e+05, std::max<double>(1.0, double(fSampleRate)));
		fConst1 = 44.1 / fConst0;
		fConst2 = 1.0 - fConst1;
		fConst3 = 1.0 / fConst0;
		fConst4 = std::exp(-(5.000000000020127 / fConst0));
		fConst5 = 1.0 - fConst4;
	}
	
	virtual void instanceResetUserInterface() {
		fVslider0 = FAUSTFLOAT(5.0);
		fVslider1 = FAUSTFLOAT(0.0);
		fVslider2 = FAUSTFLOAT(1.0);
		fButton0 = FAUSTFLOAT(0.0);
		fVslider3 = FAUSTFLOAT(0.0);
		fVslider4 = FAUSTFLOAT(1.1e+02);
		fCheckbox0 = FAUSTFLOAT(0.0);
		fVslider5 = FAUSTFLOAT(0.2);
		fVslider6 = FAUSTFLOAT(5e+02);
		fVslider7 = FAUSTFLOAT(0.2);
		fVslider8 = FAUSTFLOAT(0.2);
		fVslider9 = FAUSTFLOAT(0.2);
		fVslider10 = FAUSTFLOAT(0.0);
		fVslider11 = FAUSTFLOAT(0.0);
	}
	
	virtual void instanceClear() {
		for (int l0 = 0; l0 < 2; l0 = l0 + 1) {
			iVec0[l0] = 0;
		}
		for (int l1 = 0; l1 < 2; l1 = l1 + 1) {
			fRec8[l1] = 0.0;
		}
		for (int l2 = 0; l2 < 2; l2 = l2 + 1) {
			fRec9[l2] = 0.0;
		}
		for (int l3 = 0; l3 < 2; l3 = l3 + 1) {
			fRec11[l3] = 0.0;
		}
		for (int l4 = 0; l4 < 2; l4 = l4 + 1) {
			fRec10[l4] = 0.0;
		}
		for (int l5 = 0; l5 < 2; l5 = l5 + 1) {
			iRec14[l5] = 0;
		}
		for (int l6 = 0; l6 < 2; l6 = l6 + 1) {
			fRec13[l6] = 0.0;
		}
		for (int l7 = 0; l7 < 2; l7 = l7 + 1) {
			fRec12[l7] = 0.0;
		}
		for (int l8 = 0; l8 < 2; l8 = l8 + 1) {
			fRec7[l8] = 0.0;
		}
		for (int l9 = 0; l9 < 2; l9 = l9 + 1) {
			fRec15[l9] = 0.0;
		}
		for (int l10 = 0; l10 < 2; l10 = l10 + 1) {
			fRec16[l10] = 0.0;
		}
		for (int l11 = 0; l11 < 2; l11 = l11 + 1) {
			fRec6[l11] = 0.0;
		}
		for (int l12 = 0; l12 < 2; l12 = l12 + 1) {
			fRec2[l12] = 0.0;
		}
		for (int l13 = 0; l13 < 2; l13 = l13 + 1) {
			fRec3[l13] = 0.0;
		}
		for (int l14 = 0; l14 < 2; l14 = l14 + 1) {
			iRec4[l14] = 0;
		}
		for (int l15 = 0; l15 < 2; l15 = l15 + 1) {
			iRec5[l15] = 0;
		}
		for (int l16 = 0; l16 < 2; l16 = l16 + 1) {
			fRec18[l16] = 0.0;
		}
		for (int l17 = 0; l17 < 2; l17 = l17 + 1) {
			fRec19[l17] = 0.0;
		}
		for (int l18 = 0; l18 < 2; l18 = l18 + 1) {
			fRec20[l18] = 0.0;
		}
		for (int l19 = 0; l19 < 2; l19 = l19 + 1) {
			fRec17[l19] = 0.0;
		}
		for (int l20 = 0; l20 < 2; l20 = l20 + 1) {
			fRec21[l20] = 0.0;
		}
		for (int l21 = 0; l21 < 2; l21 = l21 + 1) {
			fRec22[l21] = 0.0;
		}
		for (int l24 = 0; l24 < 2; l24 = l24 + 1) {
			fRec25[l24] = 0.0;
		}
		for (int l25 = 0; l25 < 2; l25 = l25 + 1) {
			fRec26[l25] = 0.0;
		}
		for (int l26 = 0; l26 < 2; l26 = l26 + 1) {
			fRec24[l26] = 0.0;
		}
		for (int l27 = 0; l27 < 2; l27 = l27 + 1) {
			fRec27[l27] = 0.0;
		}
		for (int l28 = 0; l28 < 2; l28 = l28 + 1) {
			fRec28[l28] = 0.0;
		}
		for (int l29 = 0; l29 < 2; l29 = l29 + 1) {
			fRec29[l29] = 0.0;
		}
		for (int l30 = 0; l30 < 2; l30 = l30 + 1) {
			fRec30[l30] = 0.0;
		}
		for (int l31 = 0; l31 < 2; l31 = l31 + 1) {
			fRec31[l31] = 0.0;
		}
		for (int l32 = 0; l32 < 2; l32 = l32 + 1) {
			fRec32[l32] = 0.0;
		}
		IOTA0 = 0;
		for (int l33 = 0; l33 < 262144; l33 = l33 + 1) {
			fVec2[l33] = 0.0;
		}
		for (int l34 = 0; l34 < 131072; l34 = l34 + 1) {
			fVec3[l34] = 0.0;
		}
		for (int l35 = 0; l35 < 2; l35 = l35 + 1) {
			fRec34[l35] = 0.0;
		}
		for (int l36 = 0; l36 < 2; l36 = l36 + 1) {
			fRec35[l36] = 0.0;
		}
		for (int l37 = 0; l37 < 2; l37 = l37 + 1) {
			fRec36[l37] = 0.0;
		}
		for (int l38 = 0; l38 < 2; l38 = l38 + 1) {
			fRec37[l38] = 0.0;
		}
		for (int l39 = 0; l39 < 2; l39 = l39 + 1) {
			fRec33[l39] = 0.0;
		}
		for (int l40 = 0; l40 < 2; l40 = l40 + 1) {
			fVec4[l40] = 0.0;
		}
		for (int l41 = 0; l41 < 2; l41 = l41 + 1) {
			fRec1[l41] = 0.0;
		}
		for (int l42 = 0; l42 < 2; l42 = l42 + 1) {
			fRec44[l42] = 0.0;
		}
		for (int l43 = 0; l43 < 2; l43 = l43 + 1) {
			fRec43[l43] = 0.0;
		}
		for (int l44 = 0; l44 < 2; l44 = l44 + 1) {
			fRec39[l44] = 0.0;
		}
		for (int l45 = 0; l45 < 2; l45 = l45 + 1) {
			fRec40[l45] = 0.0;
		}
		for (int l46 = 0; l46 < 2; l46 = l46 + 1) {
			iRec41[l46] = 0;
		}
		for (int l47 = 0; l47 < 2; l47 = l47 + 1) {
			iRec42[l47] = 0;
		}
		for (int l48 = 0; l48 < 262144; l48 = l48 + 1) {
			fVec5[l48] = 0.0;
		}
		for (int l49 = 0; l49 < 131072; l49 = l49 + 1) {
			fVec6[l49] = 0.0;
		}
		for (int l50 = 0; l50 < 2; l50 = l50 + 1) {
			fRec45[l50] = 0.0;
		}
		for (int l51 = 0; l51 < 2; l51 = l51 + 1) {
			fVec7[l51] = 0.0;
		}
		for (int l52 = 0; l52 < 2; l52 = l52 + 1) {
			fRec38[l52] = 0.0;
		}
		for (int l53 = 0; l53 < 2; l53 = l53 + 1) {
			fVec8[l53] = 0.0;
		}
		for (int l54 = 0; l54 < 2; l54 = l54 + 1) {
			fRec0[l54] = 0.0;
		}
		for (int l55 = 0; l55 < 2; l55 = l55 + 1) {
			fRec53[l55] = 0.0;
		}
		for (int l56 = 0; l56 < 2; l56 = l56 + 1) {
			fRec52[l56] = 0.0;
		}
		for (int l57 = 0; l57 < 2; l57 = l57 + 1) {
			fRec48[l57] = 0.0;
		}
		for (int l58 = 0; l58 < 2; l58 = l58 + 1) {
			fRec49[l58] = 0.0;
		}
		for (int l59 = 0; l59 < 2; l59 = l59 + 1) {
			iRec50[l59] = 0;
		}
		for (int l60 = 0; l60 < 2; l60 = l60 + 1) {
			iRec51[l60] = 0;
		}
		for (int l61 = 0; l61 < 262144; l61 = l61 + 1) {
			fVec9[l61] = 0.0;
		}
		for (int l62 = 0; l62 < 131072; l62 = l62 + 1) {
			fVec10[l62] = 0.0;
		}
		for (int l63 = 0; l63 < 2; l63 = l63 + 1) {
			fRec54[l63] = 0.0;
		}
		for (int l64 = 0; l64 < 2; l64 = l64 + 1) {
			fVec11[l64] = 0.0;
		}
		for (int l65 = 0; l65 < 2; l65 = l65 + 1) {
			fRec47[l65] = 0.0;
		}
		for (int l66 = 0; l66 < 2; l66 = l66 + 1) {
			fRec61[l66] = 0.0;
		}
		for (int l67 = 0; l67 < 2; l67 = l67 + 1) {
			fRec60[l67] = 0.0;
		}
		for (int l68 = 0; l68 < 2; l68 = l68 + 1) {
			fRec56[l68] = 0.0;
		}
		for (int l69 = 0; l69 < 2; l69 = l69 + 1) {
			fRec57[l69] = 0.0;
		}
		for (int l70 = 0; l70 < 2; l70 = l70 + 1) {
			iRec58[l70] = 0;
		}
		for (int l71 = 0; l71 < 2; l71 = l71 + 1) {
			iRec59[l71] = 0;
		}
		for (int l72 = 0; l72 < 262144; l72 = l72 + 1) {
			fVec12[l72] = 0.0;
		}
		for (int l73 = 0; l73 < 131072; l73 = l73 + 1) {
			fVec13[l73] = 0.0;
		}
		for (int l74 = 0; l74 < 2; l74 = l74 + 1) {
			fRec62[l74] = 0.0;
		}
		for (int l75 = 0; l75 < 2; l75 = l75 + 1) {
			fVec14[l75] = 0.0;
		}
		for (int l76 = 0; l76 < 2; l76 = l76 + 1) {
			fRec55[l76] = 0.0;
		}
		for (int l77 = 0; l77 < 2; l77 = l77 + 1) {
			fVec15[l77] = 0.0;
		}
		for (int l78 = 0; l78 < 2; l78 = l78 + 1) {
			fRec46[l78] = 0.0;
		}
		for (int l79 = 0; l79 < 2; l79 = l79 + 1) {
			fRec70[l79] = 0.0;
		}
		for (int l80 = 0; l80 < 2; l80 = l80 + 1) {
			fRec69[l80] = 0.0;
		}
		for (int l81 = 0; l81 < 2; l81 = l81 + 1) {
			fRec65[l81] = 0.0;
		}
		for (int l82 = 0; l82 < 2; l82 = l82 + 1) {
			fRec66[l82] = 0.0;
		}
		for (int l83 = 0; l83 < 2; l83 = l83 + 1) {
			iRec67[l83] = 0;
		}
		for (int l84 = 0; l84 < 2; l84 = l84 + 1) {
			iRec68[l84] = 0;
		}
		for (int l85 = 0; l85 < 262144; l85 = l85 + 1) {
			fVec16[l85] = 0.0;
		}
		for (int l86 = 0; l86 < 131072; l86 = l86 + 1) {
			fVec17[l86] = 0.0;
		}
		for (int l87 = 0; l87 < 2; l87 = l87 + 1) {
			fRec71[l87] = 0.0;
		}
		for (int l88 = 0; l88 < 2; l88 = l88 + 1) {
			fVec18[l88] = 0.0;
		}
		for (int l89 = 0; l89 < 2; l89 = l89 + 1) {
			fRec64[l89] = 0.0;
		}
		for (int l90 = 0; l90 < 2; l90 = l90 + 1) {
			fRec78[l90] = 0.0;
		}
		for (int l91 = 0; l91 < 2; l91 = l91 + 1) {
			fRec77[l91] = 0.0;
		}
		for (int l92 = 0; l92 < 2; l92 = l92 + 1) {
			fRec73[l92] = 0.0;
		}
		for (int l93 = 0; l93 < 2; l93 = l93 + 1) {
			fRec74[l93] = 0.0;
		}
		for (int l94 = 0; l94 < 2; l94 = l94 + 1) {
			iRec75[l94] = 0;
		}
		for (int l95 = 0; l95 < 2; l95 = l95 + 1) {
			iRec76[l95] = 0;
		}
		for (int l96 = 0; l96 < 262144; l96 = l96 + 1) {
			fVec19[l96] = 0.0;
		}
		for (int l97 = 0; l97 < 131072; l97 = l97 + 1) {
			fVec20[l97] = 0.0;
		}
		for (int l98 = 0; l98 < 2; l98 = l98 + 1) {
			fRec79[l98] = 0.0;
		}
		for (int l99 = 0; l99 < 2; l99 = l99 + 1) {
			fVec21[l99] = 0.0;
		}
		for (int l100 = 0; l100 < 2; l100 = l100 + 1) {
			fRec72[l100] = 0.0;
		}
		for (int l101 = 0; l101 < 2; l101 = l101 + 1) {
			fVec22[l101] = 0.0;
		}
		for (int l102 = 0; l102 < 2; l102 = l102 + 1) {
			fRec63[l102] = 0.0;
		}
		for (int l103 = 0; l103 < 2; l103 = l103 + 1) {
			fRec87[l103] = 0.0;
		}
		for (int l104 = 0; l104 < 2; l104 = l104 + 1) {
			fRec86[l104] = 0.0;
		}
		for (int l105 = 0; l105 < 2; l105 = l105 + 1) {
			fRec82[l105] = 0.0;
		}
		for (int l106 = 0; l106 < 2; l106 = l106 + 1) {
			fRec83[l106] = 0.0;
		}
		for (int l107 = 0; l107 < 2; l107 = l107 + 1) {
			iRec84[l107] = 0;
		}
		for (int l108 = 0; l108 < 2; l108 = l108 + 1) {
			iRec85[l108] = 0;
		}
		for (int l109 = 0; l109 < 262144; l109 = l109 + 1) {
			fVec23[l109] = 0.0;
		}
		for (int l110 = 0; l110 < 131072; l110 = l110 + 1) {
			fVec24[l110] = 0.0;
		}
		for (int l111 = 0; l111 < 2; l111 = l111 + 1) {
			fRec88[l111] = 0.0;
		}
		for (int l112 = 0; l112 < 2; l112 = l112 + 1) {
			fVec25[l112] = 0.0;
		}
		for (int l113 = 0; l113 < 2; l113 = l113 + 1) {
			fRec81[l113] = 0.0;
		}
		for (int l114 = 0; l114 < 2; l114 = l114 + 1) {
			fRec95[l114] = 0.0;
		}
		for (int l115 = 0; l115 < 2; l115 = l115 + 1) {
			fRec94[l115] = 0.0;
		}
		for (int l116 = 0; l116 < 2; l116 = l116 + 1) {
			fRec90[l116] = 0.0;
		}
		for (int l117 = 0; l117 < 2; l117 = l117 + 1) {
			fRec91[l117] = 0.0;
		}
		for (int l118 = 0; l118 < 2; l118 = l118 + 1) {
			iRec92[l118] = 0;
		}
		for (int l119 = 0; l119 < 2; l119 = l119 + 1) {
			iRec93[l119] = 0;
		}
		for (int l120 = 0; l120 < 262144; l120 = l120 + 1) {
			fVec26[l120] = 0.0;
		}
		for (int l121 = 0; l121 < 131072; l121 = l121 + 1) {
			fVec27[l121] = 0.0;
		}
		for (int l122 = 0; l122 < 2; l122 = l122 + 1) {
			fRec96[l122] = 0.0;
		}
		for (int l123 = 0; l123 < 2; l123 = l123 + 1) {
			fVec28[l123] = 0.0;
		}
		for (int l124 = 0; l124 < 2; l124 = l124 + 1) {
			fRec89[l124] = 0.0;
		}
		for (int l125 = 0; l125 < 2; l125 = l125 + 1) {
			fVec29[l125] = 0.0;
		}
		for (int l126 = 0; l126 < 2; l126 = l126 + 1) {
			fRec80[l126] = 0.0;
		}
		for (int l127 = 0; l127 < 2; l127 = l127 + 1) {
			fRec104[l127] = 0.0;
		}
		for (int l128 = 0; l128 < 2; l128 = l128 + 1) {
			fRec103[l128] = 0.0;
		}
		for (int l129 = 0; l129 < 2; l129 = l129 + 1) {
			fRec99[l129] = 0.0;
		}
		for (int l130 = 0; l130 < 2; l130 = l130 + 1) {
			fRec100[l130] = 0.0;
		}
		for (int l131 = 0; l131 < 2; l131 = l131 + 1) {
			iRec101[l131] = 0;
		}
		for (int l132 = 0; l132 < 2; l132 = l132 + 1) {
			iRec102[l132] = 0;
		}
		for (int l133 = 0; l133 < 262144; l133 = l133 + 1) {
			fVec30[l133] = 0.0;
		}
		for (int l134 = 0; l134 < 131072; l134 = l134 + 1) {
			fVec31[l134] = 0.0;
		}
		for (int l135 = 0; l135 < 2; l135 = l135 + 1) {
			fRec105[l135] = 0.0;
		}
		for (int l136 = 0; l136 < 2; l136 = l136 + 1) {
			fVec32[l136] = 0.0;
		}
		for (int l137 = 0; l137 < 2; l137 = l137 + 1) {
			fRec98[l137] = 0.0;
		}
		for (int l138 = 0; l138 < 2; l138 = l138 + 1) {
			fRec112[l138] = 0.0;
		}
		for (int l139 = 0; l139 < 2; l139 = l139 + 1) {
			fRec111[l139] = 0.0;
		}
		for (int l140 = 0; l140 < 2; l140 = l140 + 1) {
			fRec107[l140] = 0.0;
		}
		for (int l141 = 0; l141 < 2; l141 = l141 + 1) {
			fRec108[l141] = 0.0;
		}
		for (int l142 = 0; l142 < 2; l142 = l142 + 1) {
			iRec109[l142] = 0;
		}
		for (int l143 = 0; l143 < 2; l143 = l143 + 1) {
			iRec110[l143] = 0;
		}
		for (int l144 = 0; l144 < 262144; l144 = l144 + 1) {
			fVec33[l144] = 0.0;
		}
		for (int l145 = 0; l145 < 131072; l145 = l145 + 1) {
			fVec34[l145] = 0.0;
		}
		for (int l146 = 0; l146 < 2; l146 = l146 + 1) {
			fRec113[l146] = 0.0;
		}
		for (int l147 = 0; l147 < 2; l147 = l147 + 1) {
			fVec35[l147] = 0.0;
		}
		for (int l148 = 0; l148 < 2; l148 = l148 + 1) {
			fRec106[l148] = 0.0;
		}
		for (int l149 = 0; l149 < 2; l149 = l149 + 1) {
			fVec36[l149] = 0.0;
		}
		for (int l150 = 0; l150 < 2; l150 = l150 + 1) {
			fRec97[l150] = 0.0;
		}
		for (int l151 = 0; l151 < 2; l151 = l151 + 1) {
			fRec121[l151] = 0.0;
		}
		for (int l152 = 0; l152 < 2; l152 = l152 + 1) {
			fRec120[l152] = 0.0;
		}
		for (int l153 = 0; l153 < 2; l153 = l153 + 1) {
			fRec116[l153] = 0.0;
		}
		for (int l154 = 0; l154 < 2; l154 = l154 + 1) {
			fRec117[l154] = 0.0;
		}
		for (int l155 = 0; l155 < 2; l155 = l155 + 1) {
			iRec118[l155] = 0;
		}
		for (int l156 = 0; l156 < 2; l156 = l156 + 1) {
			iRec119[l156] = 0;
		}
		for (int l157 = 0; l157 < 262144; l157 = l157 + 1) {
			fVec37[l157] = 0.0;
		}
		for (int l158 = 0; l158 < 131072; l158 = l158 + 1) {
			fVec38[l158] = 0.0;
		}
		for (int l159 = 0; l159 < 2; l159 = l159 + 1) {
			fRec122[l159] = 0.0;
		}
		for (int l160 = 0; l160 < 2; l160 = l160 + 1) {
			fVec39[l160] = 0.0;
		}
		for (int l161 = 0; l161 < 2; l161 = l161 + 1) {
			fRec115[l161] = 0.0;
		}
		for (int l162 = 0; l162 < 2; l162 = l162 + 1) {
			fRec129[l162] = 0.0;
		}
		for (int l163 = 0; l163 < 2; l163 = l163 + 1) {
			fRec128[l163] = 0.0;
		}
		for (int l164 = 0; l164 < 2; l164 = l164 + 1) {
			fRec124[l164] = 0.0;
		}
		for (int l165 = 0; l165 < 2; l165 = l165 + 1) {
			fRec125[l165] = 0.0;
		}
		for (int l166 = 0; l166 < 2; l166 = l166 + 1) {
			iRec126[l166] = 0;
		}
		for (int l167 = 0; l167 < 2; l167 = l167 + 1) {
			iRec127[l167] = 0;
		}
		for (int l168 = 0; l168 < 262144; l168 = l168 + 1) {
			fVec40[l168] = 0.0;
		}
		for (int l169 = 0; l169 < 131072; l169 = l169 + 1) {
			fVec41[l169] = 0.0;
		}
		for (int l170 = 0; l170 < 2; l170 = l170 + 1) {
			fRec130[l170] = 0.0;
		}
		for (int l171 = 0; l171 < 2; l171 = l171 + 1) {
			fVec42[l171] = 0.0;
		}
		for (int l172 = 0; l172 < 2; l172 = l172 + 1) {
			fRec123[l172] = 0.0;
		}
		for (int l173 = 0; l173 < 2; l173 = l173 + 1) {
			fVec43[l173] = 0.0;
		}
		for (int l174 = 0; l174 < 2; l174 = l174 + 1) {
			fRec114[l174] = 0.0;
		}
		for (int l175 = 0; l175 < 2; l175 = l175 + 1) {
			fRec138[l175] = 0.0;
		}
		for (int l176 = 0; l176 < 2; l176 = l176 + 1) {
			fRec137[l176] = 0.0;
		}
		for (int l177 = 0; l177 < 2; l177 = l177 + 1) {
			fRec133[l177] = 0.0;
		}
		for (int l178 = 0; l178 < 2; l178 = l178 + 1) {
			fRec134[l178] = 0.0;
		}
		for (int l179 = 0; l179 < 2; l179 = l179 + 1) {
			iRec135[l179] = 0;
		}
		for (int l180 = 0; l180 < 2; l180 = l180 + 1) {
			iRec136[l180] = 0;
		}
		for (int l181 = 0; l181 < 262144; l181 = l181 + 1) {
			fVec44[l181] = 0.0;
		}
		for (int l182 = 0; l182 < 131072; l182 = l182 + 1) {
			fVec45[l182] = 0.0;
		}
		for (int l183 = 0; l183 < 2; l183 = l183 + 1) {
			fRec139[l183] = 0.0;
		}
		for (int l184 = 0; l184 < 2; l184 = l184 + 1) {
			fVec46[l184] = 0.0;
		}
		for (int l185 = 0; l185 < 2; l185 = l185 + 1) {
			fRec132[l185] = 0.0;
		}
		for (int l186 = 0; l186 < 2; l186 = l186 + 1) {
			fRec146[l186] = 0.0;
		}
		for (int l187 = 0; l187 < 2; l187 = l187 + 1) {
			fRec145[l187] = 0.0;
		}
		for (int l188 = 0; l188 < 2; l188 = l188 + 1) {
			fRec141[l188] = 0.0;
		}
		for (int l189 = 0; l189 < 2; l189 = l189 + 1) {
			fRec142[l189] = 0.0;
		}
		for (int l190 = 0; l190 < 2; l190 = l190 + 1) {
			iRec143[l190] = 0;
		}
		for (int l191 = 0; l191 < 2; l191 = l191 + 1) {
			iRec144[l191] = 0;
		}
		for (int l192 = 0; l192 < 262144; l192 = l192 + 1) {
			fVec47[l192] = 0.0;
		}
		for (int l193 = 0; l193 < 131072; l193 = l193 + 1) {
			fVec48[l193] = 0.0;
		}
		for (int l194 = 0; l194 < 2; l194 = l194 + 1) {
			fRec147[l194] = 0.0;
		}
		for (int l195 = 0; l195 < 2; l195 = l195 + 1) {
			fVec49[l195] = 0.0;
		}
		for (int l196 = 0; l196 < 2; l196 = l196 + 1) {
			fRec140[l196] = 0.0;
		}
		for (int l197 = 0; l197 < 2; l197 = l197 + 1) {
			fVec50[l197] = 0.0;
		}
		for (int l198 = 0; l198 < 2; l198 = l198 + 1) {
			fRec131[l198] = 0.0;
		}
		for (int l199 = 0; l199 < 2; l199 = l199 + 1) {
			fRec155[l199] = 0.0;
		}
		for (int l200 = 0; l200 < 2; l200 = l200 + 1) {
			fRec154[l200] = 0.0;
		}
		for (int l201 = 0; l201 < 2; l201 = l201 + 1) {
			fRec150[l201] = 0.0;
		}
		for (int l202 = 0; l202 < 2; l202 = l202 + 1) {
			fRec151[l202] = 0.0;
		}
		for (int l203 = 0; l203 < 2; l203 = l203 + 1) {
			iRec152[l203] = 0;
		}
		for (int l204 = 0; l204 < 2; l204 = l204 + 1) {
			iRec153[l204] = 0;
		}
		for (int l205 = 0; l205 < 262144; l205 = l205 + 1) {
			fVec51[l205] = 0.0;
		}
		for (int l206 = 0; l206 < 131072; l206 = l206 + 1) {
			fVec52[l206] = 0.0;
		}
		for (int l207 = 0; l207 < 2; l207 = l207 + 1) {
			fRec156[l207] = 0.0;
		}
		for (int l208 = 0; l208 < 2; l208 = l208 + 1) {
			fVec53[l208] = 0.0;
		}
		for (int l209 = 0; l209 < 2; l209 = l209 + 1) {
			fRec149[l209] = 0.0;
		}
		for (int l210 = 0; l210 < 2; l210 = l210 + 1) {
			fRec163[l210] = 0.0;
		}
		for (int l211 = 0; l211 < 2; l211 = l211 + 1) {
			fRec162[l211] = 0.0;
		}
		for (int l212 = 0; l212 < 2; l212 = l212 + 1) {
			fRec158[l212] = 0.0;
		}
		for (int l213 = 0; l213 < 2; l213 = l213 + 1) {
			fRec159[l213] = 0.0;
		}
		for (int l214 = 0; l214 < 2; l214 = l214 + 1) {
			iRec160[l214] = 0;
		}
		for (int l215 = 0; l215 < 2; l215 = l215 + 1) {
			iRec161[l215] = 0;
		}
		for (int l216 = 0; l216 < 262144; l216 = l216 + 1) {
			fVec54[l216] = 0.0;
		}
		for (int l217 = 0; l217 < 131072; l217 = l217 + 1) {
			fVec55[l217] = 0.0;
		}
		for (int l218 = 0; l218 < 2; l218 = l218 + 1) {
			fRec164[l218] = 0.0;
		}
		for (int l219 = 0; l219 < 2; l219 = l219 + 1) {
			fVec56[l219] = 0.0;
		}
		for (int l220 = 0; l220 < 2; l220 = l220 + 1) {
			fRec157[l220] = 0.0;
		}
		for (int l221 = 0; l221 < 2; l221 = l221 + 1) {
			fVec57[l221] = 0.0;
		}
		for (int l222 = 0; l222 < 2; l222 = l222 + 1) {
			fRec148[l222] = 0.0;
		}
		for (int l223 = 0; l223 < 2; l223 = l223 + 1) {
			fRec172[l223] = 0.0;
		}
		for (int l224 = 0; l224 < 2; l224 = l224 + 1) {
			fRec171[l224] = 0.0;
		}
		for (int l225 = 0; l225 < 2; l225 = l225 + 1) {
			fRec167[l225] = 0.0;
		}
		for (int l226 = 0; l226 < 2; l226 = l226 + 1) {
			fRec168[l226] = 0.0;
		}
		for (int l227 = 0; l227 < 2; l227 = l227 + 1) {
			iRec169[l227] = 0;
		}
		for (int l228 = 0; l228 < 2; l228 = l228 + 1) {
			iRec170[l228] = 0;
		}
		for (int l229 = 0; l229 < 262144; l229 = l229 + 1) {
			fVec58[l229] = 0.0;
		}
		for (int l230 = 0; l230 < 131072; l230 = l230 + 1) {
			fVec59[l230] = 0.0;
		}
		for (int l231 = 0; l231 < 2; l231 = l231 + 1) {
			fRec173[l231] = 0.0;
		}
		for (int l232 = 0; l232 < 2; l232 = l232 + 1) {
			fVec60[l232] = 0.0;
		}
		for (int l233 = 0; l233 < 2; l233 = l233 + 1) {
			fRec166[l233] = 0.0;
		}
		for (int l234 = 0; l234 < 2; l234 = l234 + 1) {
			fRec180[l234] = 0.0;
		}
		for (int l235 = 0; l235 < 2; l235 = l235 + 1) {
			fRec179[l235] = 0.0;
		}
		for (int l236 = 0; l236 < 2; l236 = l236 + 1) {
			fRec175[l236] = 0.0;
		}
		for (int l237 = 0; l237 < 2; l237 = l237 + 1) {
			fRec176[l237] = 0.0;
		}
		for (int l238 = 0; l238 < 2; l238 = l238 + 1) {
			iRec177[l238] = 0;
		}
		for (int l239 = 0; l239 < 2; l239 = l239 + 1) {
			iRec178[l239] = 0;
		}
		for (int l240 = 0; l240 < 262144; l240 = l240 + 1) {
			fVec61[l240] = 0.0;
		}
		for (int l241 = 0; l241 < 131072; l241 = l241 + 1) {
			fVec62[l241] = 0.0;
		}
		for (int l242 = 0; l242 < 2; l242 = l242 + 1) {
			fRec181[l242] = 0.0;
		}
		for (int l243 = 0; l243 < 2; l243 = l243 + 1) {
			fVec63[l243] = 0.0;
		}
		for (int l244 = 0; l244 < 2; l244 = l244 + 1) {
			fRec174[l244] = 0.0;
		}
		for (int l245 = 0; l245 < 2; l245 = l245 + 1) {
			fVec64[l245] = 0.0;
		}
		for (int l246 = 0; l246 < 2; l246 = l246 + 1) {
			fRec165[l246] = 0.0;
		}
		for (int l247 = 0; l247 < 2; l247 = l247 + 1) {
			fRec189[l247] = 0.0;
		}
		for (int l248 = 0; l248 < 2; l248 = l248 + 1) {
			fRec188[l248] = 0.0;
		}
		for (int l249 = 0; l249 < 2; l249 = l249 + 1) {
			fRec184[l249] = 0.0;
		}
		for (int l250 = 0; l250 < 2; l250 = l250 + 1) {
			fRec185[l250] = 0.0;
		}
		for (int l251 = 0; l251 < 2; l251 = l251 + 1) {
			iRec186[l251] = 0;
		}
		for (int l252 = 0; l252 < 2; l252 = l252 + 1) {
			iRec187[l252] = 0;
		}
		for (int l253 = 0; l253 < 262144; l253 = l253 + 1) {
			fVec65[l253] = 0.0;
		}
		for (int l254 = 0; l254 < 131072; l254 = l254 + 1) {
			fVec66[l254] = 0.0;
		}
		for (int l255 = 0; l255 < 2; l255 = l255 + 1) {
			fRec190[l255] = 0.0;
		}
		for (int l256 = 0; l256 < 2; l256 = l256 + 1) {
			fVec67[l256] = 0.0;
		}
		for (int l257 = 0; l257 < 2; l257 = l257 + 1) {
			fRec183[l257] = 0.0;
		}
		for (int l258 = 0; l258 < 2; l258 = l258 + 1) {
			fRec197[l258] = 0.0;
		}
		for (int l259 = 0; l259 < 2; l259 = l259 + 1) {
			fRec196[l259] = 0.0;
		}
		for (int l260 = 0; l260 < 2; l260 = l260 + 1) {
			fRec192[l260] = 0.0;
		}
		for (int l261 = 0; l261 < 2; l261 = l261 + 1) {
			fRec193[l261] = 0.0;
		}
		for (int l262 = 0; l262 < 2; l262 = l262 + 1) {
			iRec194[l262] = 0;
		}
		for (int l263 = 0; l263 < 2; l263 = l263 + 1) {
			iRec195[l263] = 0;
		}
		for (int l264 = 0; l264 < 262144; l264 = l264 + 1) {
			fVec68[l264] = 0.0;
		}
		for (int l265 = 0; l265 < 131072; l265 = l265 + 1) {
			fVec69[l265] = 0.0;
		}
		for (int l266 = 0; l266 < 2; l266 = l266 + 1) {
			fRec198[l266] = 0.0;
		}
		for (int l267 = 0; l267 < 2; l267 = l267 + 1) {
			fVec70[l267] = 0.0;
		}
		for (int l268 = 0; l268 < 2; l268 = l268 + 1) {
			fRec191[l268] = 0.0;
		}
		for (int l269 = 0; l269 < 2; l269 = l269 + 1) {
			fVec71[l269] = 0.0;
		}
		for (int l270 = 0; l270 < 2; l270 = l270 + 1) {
			fRec182[l270] = 0.0;
		}
		for (int l271 = 0; l271 < 2; l271 = l271 + 1) {
			fRec206[l271] = 0.0;
		}
		for (int l272 = 0; l272 < 2; l272 = l272 + 1) {
			fRec205[l272] = 0.0;
		}
		for (int l273 = 0; l273 < 2; l273 = l273 + 1) {
			fRec201[l273] = 0.0;
		}
		for (int l274 = 0; l274 < 2; l274 = l274 + 1) {
			fRec202[l274] = 0.0;
		}
		for (int l275 = 0; l275 < 2; l275 = l275 + 1) {
			iRec203[l275] = 0;
		}
		for (int l276 = 0; l276 < 2; l276 = l276 + 1) {
			iRec204[l276] = 0;
		}
		for (int l277 = 0; l277 < 262144; l277 = l277 + 1) {
			fVec72[l277] = 0.0;
		}
		for (int l278 = 0; l278 < 131072; l278 = l278 + 1) {
			fVec73[l278] = 0.0;
		}
		for (int l279 = 0; l279 < 2; l279 = l279 + 1) {
			fRec207[l279] = 0.0;
		}
		for (int l280 = 0; l280 < 2; l280 = l280 + 1) {
			fVec74[l280] = 0.0;
		}
		for (int l281 = 0; l281 < 2; l281 = l281 + 1) {
			fRec200[l281] = 0.0;
		}
		for (int l282 = 0; l282 < 2; l282 = l282 + 1) {
			fRec214[l282] = 0.0;
		}
		for (int l283 = 0; l283 < 2; l283 = l283 + 1) {
			fRec213[l283] = 0.0;
		}
		for (int l284 = 0; l284 < 2; l284 = l284 + 1) {
			fRec209[l284] = 0.0;
		}
		for (int l285 = 0; l285 < 2; l285 = l285 + 1) {
			fRec210[l285] = 0.0;
		}
		for (int l286 = 0; l286 < 2; l286 = l286 + 1) {
			iRec211[l286] = 0;
		}
		for (int l287 = 0; l287 < 2; l287 = l287 + 1) {
			iRec212[l287] = 0;
		}
		for (int l288 = 0; l288 < 262144; l288 = l288 + 1) {
			fVec75[l288] = 0.0;
		}
		for (int l289 = 0; l289 < 131072; l289 = l289 + 1) {
			fVec76[l289] = 0.0;
		}
		for (int l290 = 0; l290 < 2; l290 = l290 + 1) {
			fRec215[l290] = 0.0;
		}
		for (int l291 = 0; l291 < 2; l291 = l291 + 1) {
			fVec77[l291] = 0.0;
		}
		for (int l292 = 0; l292 < 2; l292 = l292 + 1) {
			fRec208[l292] = 0.0;
		}
		for (int l293 = 0; l293 < 2; l293 = l293 + 1) {
			fVec78[l293] = 0.0;
		}
		for (int l294 = 0; l294 < 2; l294 = l294 + 1) {
			fRec199[l294] = 0.0;
		}
		for (int l295 = 0; l295 < 2; l295 = l295 + 1) {
			fRec223[l295] = 0.0;
		}
		for (int l296 = 0; l296 < 2; l296 = l296 + 1) {
			fRec222[l296] = 0.0;
		}
		for (int l297 = 0; l297 < 2; l297 = l297 + 1) {
			fRec218[l297] = 0.0;
		}
		for (int l298 = 0; l298 < 2; l298 = l298 + 1) {
			fRec219[l298] = 0.0;
		}
		for (int l299 = 0; l299 < 2; l299 = l299 + 1) {
			iRec220[l299] = 0;
		}
		for (int l300 = 0; l300 < 2; l300 = l300 + 1) {
			iRec221[l300] = 0;
		}
		for (int l301 = 0; l301 < 262144; l301 = l301 + 1) {
			fVec79[l301] = 0.0;
		}
		for (int l302 = 0; l302 < 131072; l302 = l302 + 1) {
			fVec80[l302] = 0.0;
		}
		for (int l303 = 0; l303 < 2; l303 = l303 + 1) {
			fRec224[l303] = 0.0;
		}
		for (int l304 = 0; l304 < 2; l304 = l304 + 1) {
			fVec81[l304] = 0.0;
		}
		for (int l305 = 0; l305 < 2; l305 = l305 + 1) {
			fRec217[l305] = 0.0;
		}
		for (int l306 = 0; l306 < 2; l306 = l306 + 1) {
			fRec231[l306] = 0.0;
		}
		for (int l307 = 0; l307 < 2; l307 = l307 + 1) {
			fRec230[l307] = 0.0;
		}
		for (int l308 = 0; l308 < 2; l308 = l308 + 1) {
			fRec226[l308] = 0.0;
		}
		for (int l309 = 0; l309 < 2; l309 = l309 + 1) {
			fRec227[l309] = 0.0;
		}
		for (int l310 = 0; l310 < 2; l310 = l310 + 1) {
			iRec228[l310] = 0;
		}
		for (int l311 = 0; l311 < 2; l311 = l311 + 1) {
			iRec229[l311] = 0;
		}
		for (int l312 = 0; l312 < 262144; l312 = l312 + 1) {
			fVec82[l312] = 0.0;
		}
		for (int l313 = 0; l313 < 131072; l313 = l313 + 1) {
			fVec83[l313] = 0.0;
		}
		for (int l314 = 0; l314 < 2; l314 = l314 + 1) {
			fRec232[l314] = 0.0;
		}
		for (int l315 = 0; l315 < 2; l315 = l315 + 1) {
			fVec84[l315] = 0.0;
		}
		for (int l316 = 0; l316 < 2; l316 = l316 + 1) {
			fRec225[l316] = 0.0;
		}
		for (int l317 = 0; l317 < 2; l317 = l317 + 1) {
			fVec85[l317] = 0.0;
		}
		for (int l318 = 0; l318 < 2; l318 = l318 + 1) {
			fRec216[l318] = 0.0;
		}
		for (int l319 = 0; l319 < 2; l319 = l319 + 1) {
			fRec240[l319] = 0.0;
		}
		for (int l320 = 0; l320 < 2; l320 = l320 + 1) {
			fRec239[l320] = 0.0;
		}
		for (int l321 = 0; l321 < 2; l321 = l321 + 1) {
			fRec235[l321] = 0.0;
		}
		for (int l322 = 0; l322 < 2; l322 = l322 + 1) {
			fRec236[l322] = 0.0;
		}
		for (int l323 = 0; l323 < 2; l323 = l323 + 1) {
			iRec237[l323] = 0;
		}
		for (int l324 = 0; l324 < 2; l324 = l324 + 1) {
			iRec238[l324] = 0;
		}
		for (int l325 = 0; l325 < 262144; l325 = l325 + 1) {
			fVec86[l325] = 0.0;
		}
		for (int l326 = 0; l326 < 131072; l326 = l326 + 1) {
			fVec87[l326] = 0.0;
		}
		for (int l327 = 0; l327 < 2; l327 = l327 + 1) {
			fRec241[l327] = 0.0;
		}
		for (int l328 = 0; l328 < 2; l328 = l328 + 1) {
			fVec88[l328] = 0.0;
		}
		for (int l329 = 0; l329 < 2; l329 = l329 + 1) {
			fRec234[l329] = 0.0;
		}
		for (int l330 = 0; l330 < 2; l330 = l330 + 1) {
			fRec248[l330] = 0.0;
		}
		for (int l331 = 0; l331 < 2; l331 = l331 + 1) {
			fRec247[l331] = 0.0;
		}
		for (int l332 = 0; l332 < 2; l332 = l332 + 1) {
			fRec243[l332] = 0.0;
		}
		for (int l333 = 0; l333 < 2; l333 = l333 + 1) {
			fRec244[l333] = 0.0;
		}
		for (int l334 = 0; l334 < 2; l334 = l334 + 1) {
			iRec245[l334] = 0;
		}
		for (int l335 = 0; l335 < 2; l335 = l335 + 1) {
			iRec246[l335] = 0;
		}
		for (int l336 = 0; l336 < 262144; l336 = l336 + 1) {
			fVec89[l336] = 0.0;
		}
		for (int l337 = 0; l337 < 131072; l337 = l337 + 1) {
			fVec90[l337] = 0.0;
		}
		for (int l338 = 0; l338 < 2; l338 = l338 + 1) {
			fRec249[l338] = 0.0;
		}
		for (int l339 = 0; l339 < 2; l339 = l339 + 1) {
			fVec91[l339] = 0.0;
		}
		for (int l340 = 0; l340 < 2; l340 = l340 + 1) {
			fRec242[l340] = 0.0;
		}
		for (int l341 = 0; l341 < 2; l341 = l341 + 1) {
			fVec92[l341] = 0.0;
		}
		for (int l342 = 0; l342 < 2; l342 = l342 + 1) {
			fRec233[l342] = 0.0;
		}
		for (int l343 = 0; l343 < 2; l343 = l343 + 1) {
			fRec257[l343] = 0.0;
		}
		for (int l344 = 0; l344 < 2; l344 = l344 + 1) {
			fRec256[l344] = 0.0;
		}
		for (int l345 = 0; l345 < 2; l345 = l345 + 1) {
			fRec252[l345] = 0.0;
		}
		for (int l346 = 0; l346 < 2; l346 = l346 + 1) {
			fRec253[l346] = 0.0;
		}
		for (int l347 = 0; l347 < 2; l347 = l347 + 1) {
			iRec254[l347] = 0;
		}
		for (int l348 = 0; l348 < 2; l348 = l348 + 1) {
			iRec255[l348] = 0;
		}
		for (int l349 = 0; l349 < 262144; l349 = l349 + 1) {
			fVec93[l349] = 0.0;
		}
		for (int l350 = 0; l350 < 131072; l350 = l350 + 1) {
			fVec94[l350] = 0.0;
		}
		for (int l351 = 0; l351 < 2; l351 = l351 + 1) {
			fRec258[l351] = 0.0;
		}
		for (int l352 = 0; l352 < 2; l352 = l352 + 1) {
			fVec95[l352] = 0.0;
		}
		for (int l353 = 0; l353 < 2; l353 = l353 + 1) {
			fRec251[l353] = 0.0;
		}
		for (int l354 = 0; l354 < 2; l354 = l354 + 1) {
			fRec265[l354] = 0.0;
		}
		for (int l355 = 0; l355 < 2; l355 = l355 + 1) {
			fRec264[l355] = 0.0;
		}
		for (int l356 = 0; l356 < 2; l356 = l356 + 1) {
			fRec260[l356] = 0.0;
		}
		for (int l357 = 0; l357 < 2; l357 = l357 + 1) {
			fRec261[l357] = 0.0;
		}
		for (int l358 = 0; l358 < 2; l358 = l358 + 1) {
			iRec262[l358] = 0;
		}
		for (int l359 = 0; l359 < 2; l359 = l359 + 1) {
			iRec263[l359] = 0;
		}
		for (int l360 = 0; l360 < 262144; l360 = l360 + 1) {
			fVec96[l360] = 0.0;
		}
		for (int l361 = 0; l361 < 131072; l361 = l361 + 1) {
			fVec97[l361] = 0.0;
		}
		for (int l362 = 0; l362 < 2; l362 = l362 + 1) {
			fRec266[l362] = 0.0;
		}
		for (int l363 = 0; l363 < 2; l363 = l363 + 1) {
			fVec98[l363] = 0.0;
		}
		for (int l364 = 0; l364 < 2; l364 = l364 + 1) {
			fRec259[l364] = 0.0;
		}
		for (int l365 = 0; l365 < 2; l365 = l365 + 1) {
			fVec99[l365] = 0.0;
		}
		for (int l366 = 0; l366 < 2; l366 = l366 + 1) {
			fRec250[l366] = 0.0;
		}
		for (int l367 = 0; l367 < 2; l367 = l367 + 1) {
			fRec274[l367] = 0.0;
		}
		for (int l368 = 0; l368 < 2; l368 = l368 + 1) {
			fRec273[l368] = 0.0;
		}
		for (int l369 = 0; l369 < 2; l369 = l369 + 1) {
			fRec269[l369] = 0.0;
		}
		for (int l370 = 0; l370 < 2; l370 = l370 + 1) {
			fRec270[l370] = 0.0;
		}
		for (int l371 = 0; l371 < 2; l371 = l371 + 1) {
			iRec271[l371] = 0;
		}
		for (int l372 = 0; l372 < 2; l372 = l372 + 1) {
			iRec272[l372] = 0;
		}
		for (int l373 = 0; l373 < 262144; l373 = l373 + 1) {
			fVec100[l373] = 0.0;
		}
		for (int l374 = 0; l374 < 131072; l374 = l374 + 1) {
			fVec101[l374] = 0.0;
		}
		for (int l375 = 0; l375 < 2; l375 = l375 + 1) {
			fRec275[l375] = 0.0;
		}
		for (int l376 = 0; l376 < 2; l376 = l376 + 1) {
			fVec102[l376] = 0.0;
		}
		for (int l377 = 0; l377 < 2; l377 = l377 + 1) {
			fRec268[l377] = 0.0;
		}
		for (int l378 = 0; l378 < 2; l378 = l378 + 1) {
			fRec282[l378] = 0.0;
		}
		for (int l379 = 0; l379 < 2; l379 = l379 + 1) {
			fRec281[l379] = 0.0;
		}
		for (int l380 = 0; l380 < 2; l380 = l380 + 1) {
			fRec277[l380] = 0.0;
		}
		for (int l381 = 0; l381 < 2; l381 = l381 + 1) {
			fRec278[l381] = 0.0;
		}
		for (int l382 = 0; l382 < 2; l382 = l382 + 1) {
			iRec279[l382] = 0;
		}
		for (int l383 = 0; l383 < 2; l383 = l383 + 1) {
			iRec280[l383] = 0;
		}
		for (int l384 = 0; l384 < 262144; l384 = l384 + 1) {
			fVec103[l384] = 0.0;
		}
		for (int l385 = 0; l385 < 131072; l385 = l385 + 1) {
			fVec104[l385] = 0.0;
		}
		for (int l386 = 0; l386 < 2; l386 = l386 + 1) {
			fRec283[l386] = 0.0;
		}
		for (int l387 = 0; l387 < 2; l387 = l387 + 1) {
			fVec105[l387] = 0.0;
		}
		for (int l388 = 0; l388 < 2; l388 = l388 + 1) {
			fRec276[l388] = 0.0;
		}
		for (int l389 = 0; l389 < 2; l389 = l389 + 1) {
			fVec106[l389] = 0.0;
		}
		for (int l390 = 0; l390 < 2; l390 = l390 + 1) {
			fRec267[l390] = 0.0;
		}
		for (int l391 = 0; l391 < 2; l391 = l391 + 1) {
			fRec291[l391] = 0.0;
		}
		for (int l392 = 0; l392 < 2; l392 = l392 + 1) {
			fRec290[l392] = 0.0;
		}
		for (int l393 = 0; l393 < 2; l393 = l393 + 1) {
			fRec286[l393] = 0.0;
		}
		for (int l394 = 0; l394 < 2; l394 = l394 + 1) {
			fRec287[l394] = 0.0;
		}
		for (int l395 = 0; l395 < 2; l395 = l395 + 1) {
			iRec288[l395] = 0;
		}
		for (int l396 = 0; l396 < 2; l396 = l396 + 1) {
			iRec289[l396] = 0;
		}
		for (int l397 = 0; l397 < 262144; l397 = l397 + 1) {
			fVec107[l397] = 0.0;
		}
		for (int l398 = 0; l398 < 131072; l398 = l398 + 1) {
			fVec108[l398] = 0.0;
		}
		for (int l399 = 0; l399 < 2; l399 = l399 + 1) {
			fRec292[l399] = 0.0;
		}
		for (int l400 = 0; l400 < 2; l400 = l400 + 1) {
			fVec109[l400] = 0.0;
		}
		for (int l401 = 0; l401 < 2; l401 = l401 + 1) {
			fRec285[l401] = 0.0;
		}
		for (int l402 = 0; l402 < 2; l402 = l402 + 1) {
			fRec299[l402] = 0.0;
		}
		for (int l403 = 0; l403 < 2; l403 = l403 + 1) {
			fRec298[l403] = 0.0;
		}
		for (int l404 = 0; l404 < 2; l404 = l404 + 1) {
			fRec294[l404] = 0.0;
		}
		for (int l405 = 0; l405 < 2; l405 = l405 + 1) {
			fRec295[l405] = 0.0;
		}
		for (int l406 = 0; l406 < 2; l406 = l406 + 1) {
			iRec296[l406] = 0;
		}
		for (int l407 = 0; l407 < 2; l407 = l407 + 1) {
			iRec297[l407] = 0;
		}
		for (int l408 = 0; l408 < 262144; l408 = l408 + 1) {
			fVec110[l408] = 0.0;
		}
		for (int l409 = 0; l409 < 131072; l409 = l409 + 1) {
			fVec111[l409] = 0.0;
		}
		for (int l410 = 0; l410 < 2; l410 = l410 + 1) {
			fRec300[l410] = 0.0;
		}
		for (int l411 = 0; l411 < 2; l411 = l411 + 1) {
			fVec112[l411] = 0.0;
		}
		for (int l412 = 0; l412 < 2; l412 = l412 + 1) {
			fRec293[l412] = 0.0;
		}
		for (int l413 = 0; l413 < 2; l413 = l413 + 1) {
			fVec113[l413] = 0.0;
		}
		for (int l414 = 0; l414 < 2; l414 = l414 + 1) {
			fRec284[l414] = 0.0;
		}
		for (int l415 = 0; l415 < 2; l415 = l415 + 1) {
			fRec308[l415] = 0.0;
		}
		for (int l416 = 0; l416 < 2; l416 = l416 + 1) {
			fRec307[l416] = 0.0;
		}
		for (int l417 = 0; l417 < 2; l417 = l417 + 1) {
			fRec303[l417] = 0.0;
		}
		for (int l418 = 0; l418 < 2; l418 = l418 + 1) {
			fRec304[l418] = 0.0;
		}
		for (int l419 = 0; l419 < 2; l419 = l419 + 1) {
			iRec305[l419] = 0;
		}
		for (int l420 = 0; l420 < 2; l420 = l420 + 1) {
			iRec306[l420] = 0;
		}
		for (int l421 = 0; l421 < 262144; l421 = l421 + 1) {
			fVec114[l421] = 0.0;
		}
		for (int l422 = 0; l422 < 131072; l422 = l422 + 1) {
			fVec115[l422] = 0.0;
		}
		for (int l423 = 0; l423 < 2; l423 = l423 + 1) {
			fRec309[l423] = 0.0;
		}
		for (int l424 = 0; l424 < 2; l424 = l424 + 1) {
			fVec116[l424] = 0.0;
		}
		for (int l425 = 0; l425 < 2; l425 = l425 + 1) {
			fRec302[l425] = 0.0;
		}
		for (int l426 = 0; l426 < 2; l426 = l426 + 1) {
			fRec316[l426] = 0.0;
		}
		for (int l427 = 0; l427 < 2; l427 = l427 + 1) {
			fRec315[l427] = 0.0;
		}
		for (int l428 = 0; l428 < 2; l428 = l428 + 1) {
			fRec311[l428] = 0.0;
		}
		for (int l429 = 0; l429 < 2; l429 = l429 + 1) {
			fRec312[l429] = 0.0;
		}
		for (int l430 = 0; l430 < 2; l430 = l430 + 1) {
			iRec313[l430] = 0;
		}
		for (int l431 = 0; l431 < 2; l431 = l431 + 1) {
			iRec314[l431] = 0;
		}
		for (int l432 = 0; l432 < 262144; l432 = l432 + 1) {
			fVec117[l432] = 0.0;
		}
		for (int l433 = 0; l433 < 131072; l433 = l433 + 1) {
			fVec118[l433] = 0.0;
		}
		for (int l434 = 0; l434 < 2; l434 = l434 + 1) {
			fRec317[l434] = 0.0;
		}
		for (int l435 = 0; l435 < 2; l435 = l435 + 1) {
			fVec119[l435] = 0.0;
		}
		for (int l436 = 0; l436 < 2; l436 = l436 + 1) {
			fRec310[l436] = 0.0;
		}
		for (int l437 = 0; l437 < 2; l437 = l437 + 1) {
			fVec120[l437] = 0.0;
		}
		for (int l438 = 0; l438 < 2; l438 = l438 + 1) {
			fRec301[l438] = 0.0;
		}
		for (int l439 = 0; l439 < 2; l439 = l439 + 1) {
			fRec325[l439] = 0.0;
		}
		for (int l440 = 0; l440 < 2; l440 = l440 + 1) {
			fRec324[l440] = 0.0;
		}
		for (int l441 = 0; l441 < 2; l441 = l441 + 1) {
			fRec320[l441] = 0.0;
		}
		for (int l442 = 0; l442 < 2; l442 = l442 + 1) {
			fRec321[l442] = 0.0;
		}
		for (int l443 = 0; l443 < 2; l443 = l443 + 1) {
			iRec322[l443] = 0;
		}
		for (int l444 = 0; l444 < 2; l444 = l444 + 1) {
			iRec323[l444] = 0;
		}
		for (int l445 = 0; l445 < 262144; l445 = l445 + 1) {
			fVec121[l445] = 0.0;
		}
		for (int l446 = 0; l446 < 131072; l446 = l446 + 1) {
			fVec122[l446] = 0.0;
		}
		for (int l447 = 0; l447 < 2; l447 = l447 + 1) {
			fRec326[l447] = 0.0;
		}
		for (int l448 = 0; l448 < 2; l448 = l448 + 1) {
			fVec123[l448] = 0.0;
		}
		for (int l449 = 0; l449 < 2; l449 = l449 + 1) {
			fRec319[l449] = 0.0;
		}
		for (int l450 = 0; l450 < 2; l450 = l450 + 1) {
			fRec333[l450] = 0.0;
		}
		for (int l451 = 0; l451 < 2; l451 = l451 + 1) {
			fRec332[l451] = 0.0;
		}
		for (int l452 = 0; l452 < 2; l452 = l452 + 1) {
			fRec328[l452] = 0.0;
		}
		for (int l453 = 0; l453 < 2; l453 = l453 + 1) {
			fRec329[l453] = 0.0;
		}
		for (int l454 = 0; l454 < 2; l454 = l454 + 1) {
			iRec330[l454] = 0;
		}
		for (int l455 = 0; l455 < 2; l455 = l455 + 1) {
			iRec331[l455] = 0;
		}
		for (int l456 = 0; l456 < 262144; l456 = l456 + 1) {
			fVec124[l456] = 0.0;
		}
		for (int l457 = 0; l457 < 131072; l457 = l457 + 1) {
			fVec125[l457] = 0.0;
		}
		for (int l458 = 0; l458 < 2; l458 = l458 + 1) {
			fRec334[l458] = 0.0;
		}
		for (int l459 = 0; l459 < 2; l459 = l459 + 1) {
			fVec126[l459] = 0.0;
		}
		for (int l460 = 0; l460 < 2; l460 = l460 + 1) {
			fRec327[l460] = 0.0;
		}
		for (int l461 = 0; l461 < 2; l461 = l461 + 1) {
			fVec127[l461] = 0.0;
		}
		for (int l462 = 0; l462 < 2; l462 = l462 + 1) {
			fRec318[l462] = 0.0;
		}
		for (int l463 = 0; l463 < 2; l463 = l463 + 1) {
			fRec342[l463] = 0.0;
		}
		for (int l464 = 0; l464 < 2; l464 = l464 + 1) {
			fRec341[l464] = 0.0;
		}
		for (int l465 = 0; l465 < 2; l465 = l465 + 1) {
			fRec337[l465] = 0.0;
		}
		for (int l466 = 0; l466 < 2; l466 = l466 + 1) {
			fRec338[l466] = 0.0;
		}
		for (int l467 = 0; l467 < 2; l467 = l467 + 1) {
			iRec339[l467] = 0;
		}
		for (int l468 = 0; l468 < 2; l468 = l468 + 1) {
			iRec340[l468] = 0;
		}
		for (int l469 = 0; l469 < 262144; l469 = l469 + 1) {
			fVec128[l469] = 0.0;
		}
		for (int l470 = 0; l470 < 131072; l470 = l470 + 1) {
			fVec129[l470] = 0.0;
		}
		for (int l471 = 0; l471 < 2; l471 = l471 + 1) {
			fRec343[l471] = 0.0;
		}
		for (int l472 = 0; l472 < 2; l472 = l472 + 1) {
			fVec130[l472] = 0.0;
		}
		for (int l473 = 0; l473 < 2; l473 = l473 + 1) {
			fRec336[l473] = 0.0;
		}
		for (int l474 = 0; l474 < 2; l474 = l474 + 1) {
			fRec350[l474] = 0.0;
		}
		for (int l475 = 0; l475 < 2; l475 = l475 + 1) {
			fRec349[l475] = 0.0;
		}
		for (int l476 = 0; l476 < 2; l476 = l476 + 1) {
			fRec345[l476] = 0.0;
		}
		for (int l477 = 0; l477 < 2; l477 = l477 + 1) {
			fRec346[l477] = 0.0;
		}
		for (int l478 = 0; l478 < 2; l478 = l478 + 1) {
			iRec347[l478] = 0;
		}
		for (int l479 = 0; l479 < 2; l479 = l479 + 1) {
			iRec348[l479] = 0;
		}
		for (int l480 = 0; l480 < 262144; l480 = l480 + 1) {
			fVec131[l480] = 0.0;
		}
		for (int l481 = 0; l481 < 131072; l481 = l481 + 1) {
			fVec132[l481] = 0.0;
		}
		for (int l482 = 0; l482 < 2; l482 = l482 + 1) {
			fRec351[l482] = 0.0;
		}
		for (int l483 = 0; l483 < 2; l483 = l483 + 1) {
			fVec133[l483] = 0.0;
		}
		for (int l484 = 0; l484 < 2; l484 = l484 + 1) {
			fRec344[l484] = 0.0;
		}
		for (int l485 = 0; l485 < 2; l485 = l485 + 1) {
			fVec134[l485] = 0.0;
		}
		for (int l486 = 0; l486 < 2; l486 = l486 + 1) {
			fRec335[l486] = 0.0;
		}
		for (int l487 = 0; l487 < 2; l487 = l487 + 1) {
			fRec359[l487] = 0.0;
		}
		for (int l488 = 0; l488 < 2; l488 = l488 + 1) {
			fRec358[l488] = 0.0;
		}
		for (int l489 = 0; l489 < 2; l489 = l489 + 1) {
			fRec354[l489] = 0.0;
		}
		for (int l490 = 0; l490 < 2; l490 = l490 + 1) {
			fRec355[l490] = 0.0;
		}
		for (int l491 = 0; l491 < 2; l491 = l491 + 1) {
			iRec356[l491] = 0;
		}
		for (int l492 = 0; l492 < 2; l492 = l492 + 1) {
			iRec357[l492] = 0;
		}
		for (int l493 = 0; l493 < 262144; l493 = l493 + 1) {
			fVec135[l493] = 0.0;
		}
		for (int l494 = 0; l494 < 131072; l494 = l494 + 1) {
			fVec136[l494] = 0.0;
		}
		for (int l495 = 0; l495 < 2; l495 = l495 + 1) {
			fRec360[l495] = 0.0;
		}
		for (int l496 = 0; l496 < 2; l496 = l496 + 1) {
			fVec137[l496] = 0.0;
		}
		for (int l497 = 0; l497 < 2; l497 = l497 + 1) {
			fRec353[l497] = 0.0;
		}
		for (int l498 = 0; l498 < 2; l498 = l498 + 1) {
			fRec367[l498] = 0.0;
		}
		for (int l499 = 0; l499 < 2; l499 = l499 + 1) {
			fRec366[l499] = 0.0;
		}
		for (int l500 = 0; l500 < 2; l500 = l500 + 1) {
			fRec362[l500] = 0.0;
		}
		for (int l501 = 0; l501 < 2; l501 = l501 + 1) {
			fRec363[l501] = 0.0;
		}
		for (int l502 = 0; l502 < 2; l502 = l502 + 1) {
			iRec364[l502] = 0;
		}
		for (int l503 = 0; l503 < 2; l503 = l503 + 1) {
			iRec365[l503] = 0;
		}
		for (int l504 = 0; l504 < 262144; l504 = l504 + 1) {
			fVec138[l504] = 0.0;
		}
		for (int l505 = 0; l505 < 131072; l505 = l505 + 1) {
			fVec139[l505] = 0.0;
		}
		for (int l506 = 0; l506 < 2; l506 = l506 + 1) {
			fRec368[l506] = 0.0;
		}
		for (int l507 = 0; l507 < 2; l507 = l507 + 1) {
			fVec140[l507] = 0.0;
		}
		for (int l508 = 0; l508 < 2; l508 = l508 + 1) {
			fRec361[l508] = 0.0;
		}
		for (int l509 = 0; l509 < 2; l509 = l509 + 1) {
			fVec141[l509] = 0.0;
		}
		for (int l510 = 0; l510 < 2; l510 = l510 + 1) {
			fRec352[l510] = 0.0;
		}
		for (int l511 = 0; l511 < 2; l511 = l511 + 1) {
			fRec376[l511] = 0.0;
		}
		for (int l512 = 0; l512 < 2; l512 = l512 + 1) {
			fRec375[l512] = 0.0;
		}
		for (int l513 = 0; l513 < 2; l513 = l513 + 1) {
			fRec371[l513] = 0.0;
		}
		for (int l514 = 0; l514 < 2; l514 = l514 + 1) {
			fRec372[l514] = 0.0;
		}
		for (int l515 = 0; l515 < 2; l515 = l515 + 1) {
			iRec373[l515] = 0;
		}
		for (int l516 = 0; l516 < 2; l516 = l516 + 1) {
			iRec374[l516] = 0;
		}
		for (int l517 = 0; l517 < 262144; l517 = l517 + 1) {
			fVec142[l517] = 0.0;
		}
		for (int l518 = 0; l518 < 131072; l518 = l518 + 1) {
			fVec143[l518] = 0.0;
		}
		for (int l519 = 0; l519 < 2; l519 = l519 + 1) {
			fRec377[l519] = 0.0;
		}
		for (int l520 = 0; l520 < 2; l520 = l520 + 1) {
			fVec144[l520] = 0.0;
		}
		for (int l521 = 0; l521 < 2; l521 = l521 + 1) {
			fRec370[l521] = 0.0;
		}
		for (int l522 = 0; l522 < 2; l522 = l522 + 1) {
			fRec384[l522] = 0.0;
		}
		for (int l523 = 0; l523 < 2; l523 = l523 + 1) {
			fRec383[l523] = 0.0;
		}
		for (int l524 = 0; l524 < 2; l524 = l524 + 1) {
			fRec379[l524] = 0.0;
		}
		for (int l525 = 0; l525 < 2; l525 = l525 + 1) {
			fRec380[l525] = 0.0;
		}
		for (int l526 = 0; l526 < 2; l526 = l526 + 1) {
			iRec381[l526] = 0;
		}
		for (int l527 = 0; l527 < 2; l527 = l527 + 1) {
			iRec382[l527] = 0;
		}
		for (int l528 = 0; l528 < 262144; l528 = l528 + 1) {
			fVec145[l528] = 0.0;
		}
		for (int l529 = 0; l529 < 131072; l529 = l529 + 1) {
			fVec146[l529] = 0.0;
		}
		for (int l530 = 0; l530 < 2; l530 = l530 + 1) {
			fRec385[l530] = 0.0;
		}
		for (int l531 = 0; l531 < 2; l531 = l531 + 1) {
			fVec147[l531] = 0.0;
		}
		for (int l532 = 0; l532 < 2; l532 = l532 + 1) {
			fRec378[l532] = 0.0;
		}
		for (int l533 = 0; l533 < 2; l533 = l533 + 1) {
			fVec148[l533] = 0.0;
		}
		for (int l534 = 0; l534 < 2; l534 = l534 + 1) {
			fRec369[l534] = 0.0;
		}
		for (int l535 = 0; l535 < 2; l535 = l535 + 1) {
			fRec393[l535] = 0.0;
		}
		for (int l536 = 0; l536 < 2; l536 = l536 + 1) {
			fRec392[l536] = 0.0;
		}
		for (int l537 = 0; l537 < 2; l537 = l537 + 1) {
			fRec388[l537] = 0.0;
		}
		for (int l538 = 0; l538 < 2; l538 = l538 + 1) {
			fRec389[l538] = 0.0;
		}
		for (int l539 = 0; l539 < 2; l539 = l539 + 1) {
			iRec390[l539] = 0;
		}
		for (int l540 = 0; l540 < 2; l540 = l540 + 1) {
			iRec391[l540] = 0;
		}
		for (int l541 = 0; l541 < 262144; l541 = l541 + 1) {
			fVec149[l541] = 0.0;
		}
		for (int l542 = 0; l542 < 131072; l542 = l542 + 1) {
			fVec150[l542] = 0.0;
		}
		for (int l543 = 0; l543 < 2; l543 = l543 + 1) {
			fRec394[l543] = 0.0;
		}
		for (int l544 = 0; l544 < 2; l544 = l544 + 1) {
			fVec151[l544] = 0.0;
		}
		for (int l545 = 0; l545 < 2; l545 = l545 + 1) {
			fRec387[l545] = 0.0;
		}
		for (int l546 = 0; l546 < 2; l546 = l546 + 1) {
			fRec401[l546] = 0.0;
		}
		for (int l547 = 0; l547 < 2; l547 = l547 + 1) {
			fRec400[l547] = 0.0;
		}
		for (int l548 = 0; l548 < 2; l548 = l548 + 1) {
			fRec396[l548] = 0.0;
		}
		for (int l549 = 0; l549 < 2; l549 = l549 + 1) {
			fRec397[l549] = 0.0;
		}
		for (int l550 = 0; l550 < 2; l550 = l550 + 1) {
			iRec398[l550] = 0;
		}
		for (int l551 = 0; l551 < 2; l551 = l551 + 1) {
			iRec399[l551] = 0;
		}
		for (int l552 = 0; l552 < 262144; l552 = l552 + 1) {
			fVec152[l552] = 0.0;
		}
		for (int l553 = 0; l553 < 131072; l553 = l553 + 1) {
			fVec153[l553] = 0.0;
		}
		for (int l554 = 0; l554 < 2; l554 = l554 + 1) {
			fRec402[l554] = 0.0;
		}
		for (int l555 = 0; l555 < 2; l555 = l555 + 1) {
			fVec154[l555] = 0.0;
		}
		for (int l556 = 0; l556 < 2; l556 = l556 + 1) {
			fRec395[l556] = 0.0;
		}
		for (int l557 = 0; l557 < 2; l557 = l557 + 1) {
			fVec155[l557] = 0.0;
		}
		for (int l558 = 0; l558 < 2; l558 = l558 + 1) {
			fRec386[l558] = 0.0;
		}
		for (int l559 = 0; l559 < 2; l559 = l559 + 1) {
			fRec410[l559] = 0.0;
		}
		for (int l560 = 0; l560 < 2; l560 = l560 + 1) {
			fRec409[l560] = 0.0;
		}
		for (int l561 = 0; l561 < 2; l561 = l561 + 1) {
			fRec405[l561] = 0.0;
		}
		for (int l562 = 0; l562 < 2; l562 = l562 + 1) {
			fRec406[l562] = 0.0;
		}
		for (int l563 = 0; l563 < 2; l563 = l563 + 1) {
			iRec407[l563] = 0;
		}
		for (int l564 = 0; l564 < 2; l564 = l564 + 1) {
			iRec408[l564] = 0;
		}
		for (int l565 = 0; l565 < 262144; l565 = l565 + 1) {
			fVec156[l565] = 0.0;
		}
		for (int l566 = 0; l566 < 131072; l566 = l566 + 1) {
			fVec157[l566] = 0.0;
		}
		for (int l567 = 0; l567 < 2; l567 = l567 + 1) {
			fRec411[l567] = 0.0;
		}
		for (int l568 = 0; l568 < 2; l568 = l568 + 1) {
			fVec158[l568] = 0.0;
		}
		for (int l569 = 0; l569 < 2; l569 = l569 + 1) {
			fRec404[l569] = 0.0;
		}
		for (int l570 = 0; l570 < 2; l570 = l570 + 1) {
			fRec418[l570] = 0.0;
		}
		for (int l571 = 0; l571 < 2; l571 = l571 + 1) {
			fRec417[l571] = 0.0;
		}
		for (int l572 = 0; l572 < 2; l572 = l572 + 1) {
			fRec413[l572] = 0.0;
		}
		for (int l573 = 0; l573 < 2; l573 = l573 + 1) {
			fRec414[l573] = 0.0;
		}
		for (int l574 = 0; l574 < 2; l574 = l574 + 1) {
			iRec415[l574] = 0;
		}
		for (int l575 = 0; l575 < 2; l575 = l575 + 1) {
			iRec416[l575] = 0;
		}
		for (int l576 = 0; l576 < 262144; l576 = l576 + 1) {
			fVec159[l576] = 0.0;
		}
		for (int l577 = 0; l577 < 131072; l577 = l577 + 1) {
			fVec160[l577] = 0.0;
		}
		for (int l578 = 0; l578 < 2; l578 = l578 + 1) {
			fRec419[l578] = 0.0;
		}
		for (int l579 = 0; l579 < 2; l579 = l579 + 1) {
			fVec161[l579] = 0.0;
		}
		for (int l580 = 0; l580 < 2; l580 = l580 + 1) {
			fRec412[l580] = 0.0;
		}
		for (int l581 = 0; l581 < 2; l581 = l581 + 1) {
			fVec162[l581] = 0.0;
		}
		for (int l582 = 0; l582 < 2; l582 = l582 + 1) {
			fRec403[l582] = 0.0;
		}
		for (int l583 = 0; l583 < 2; l583 = l583 + 1) {
			fRec427[l583] = 0.0;
		}
		for (int l584 = 0; l584 < 2; l584 = l584 + 1) {
			fRec426[l584] = 0.0;
		}
		for (int l585 = 0; l585 < 2; l585 = l585 + 1) {
			fRec422[l585] = 0.0;
		}
		for (int l586 = 0; l586 < 2; l586 = l586 + 1) {
			fRec423[l586] = 0.0;
		}
		for (int l587 = 0; l587 < 2; l587 = l587 + 1) {
			iRec424[l587] = 0;
		}
		for (int l588 = 0; l588 < 2; l588 = l588 + 1) {
			iRec425[l588] = 0;
		}
		for (int l589 = 0; l589 < 262144; l589 = l589 + 1) {
			fVec163[l589] = 0.0;
		}
		for (int l590 = 0; l590 < 131072; l590 = l590 + 1) {
			fVec164[l590] = 0.0;
		}
		for (int l591 = 0; l591 < 2; l591 = l591 + 1) {
			fRec428[l591] = 0.0;
		}
		for (int l592 = 0; l592 < 2; l592 = l592 + 1) {
			fVec165[l592] = 0.0;
		}
		for (int l593 = 0; l593 < 2; l593 = l593 + 1) {
			fRec421[l593] = 0.0;
		}
		for (int l594 = 0; l594 < 2; l594 = l594 + 1) {
			fRec435[l594] = 0.0;
		}
		for (int l595 = 0; l595 < 2; l595 = l595 + 1) {
			fRec434[l595] = 0.0;
		}
		for (int l596 = 0; l596 < 2; l596 = l596 + 1) {
			fRec430[l596] = 0.0;
		}
		for (int l597 = 0; l597 < 2; l597 = l597 + 1) {
			fRec431[l597] = 0.0;
		}
		for (int l598 = 0; l598 < 2; l598 = l598 + 1) {
			iRec432[l598] = 0;
		}
		for (int l599 = 0; l599 < 2; l599 = l599 + 1) {
			iRec433[l599] = 0;
		}
		for (int l600 = 0; l600 < 262144; l600 = l600 + 1) {
			fVec166[l600] = 0.0;
		}
		for (int l601 = 0; l601 < 131072; l601 = l601 + 1) {
			fVec167[l601] = 0.0;
		}
		for (int l602 = 0; l602 < 2; l602 = l602 + 1) {
			fRec436[l602] = 0.0;
		}
		for (int l603 = 0; l603 < 2; l603 = l603 + 1) {
			fVec168[l603] = 0.0;
		}
		for (int l604 = 0; l604 < 2; l604 = l604 + 1) {
			fRec429[l604] = 0.0;
		}
		for (int l605 = 0; l605 < 2; l605 = l605 + 1) {
			fVec169[l605] = 0.0;
		}
		for (int l606 = 0; l606 < 2; l606 = l606 + 1) {
			fRec420[l606] = 0.0;
		}
		for (int l607 = 0; l607 < 2; l607 = l607 + 1) {
			fRec444[l607] = 0.0;
		}
		for (int l608 = 0; l608 < 2; l608 = l608 + 1) {
			fRec443[l608] = 0.0;
		}
		for (int l609 = 0; l609 < 2; l609 = l609 + 1) {
			fRec439[l609] = 0.0;
		}
		for (int l610 = 0; l610 < 2; l610 = l610 + 1) {
			fRec440[l610] = 0.0;
		}
		for (int l611 = 0; l611 < 2; l611 = l611 + 1) {
			iRec441[l611] = 0;
		}
		for (int l612 = 0; l612 < 2; l612 = l612 + 1) {
			iRec442[l612] = 0;
		}
		for (int l613 = 0; l613 < 262144; l613 = l613 + 1) {
			fVec170[l613] = 0.0;
		}
		for (int l614 = 0; l614 < 131072; l614 = l614 + 1) {
			fVec171[l614] = 0.0;
		}
		for (int l615 = 0; l615 < 2; l615 = l615 + 1) {
			fRec445[l615] = 0.0;
		}
		for (int l616 = 0; l616 < 2; l616 = l616 + 1) {
			fVec172[l616] = 0.0;
		}
		for (int l617 = 0; l617 < 2; l617 = l617 + 1) {
			fRec438[l617] = 0.0;
		}
		for (int l618 = 0; l618 < 2; l618 = l618 + 1) {
			fRec452[l618] = 0.0;
		}
		for (int l619 = 0; l619 < 2; l619 = l619 + 1) {
			fRec451[l619] = 0.0;
		}
		for (int l620 = 0; l620 < 2; l620 = l620 + 1) {
			fRec447[l620] = 0.0;
		}
		for (int l621 = 0; l621 < 2; l621 = l621 + 1) {
			fRec448[l621] = 0.0;
		}
		for (int l622 = 0; l622 < 2; l622 = l622 + 1) {
			iRec449[l622] = 0;
		}
		for (int l623 = 0; l623 < 2; l623 = l623 + 1) {
			iRec450[l623] = 0;
		}
		for (int l624 = 0; l624 < 262144; l624 = l624 + 1) {
			fVec173[l624] = 0.0;
		}
		for (int l625 = 0; l625 < 131072; l625 = l625 + 1) {
			fVec174[l625] = 0.0;
		}
		for (int l626 = 0; l626 < 2; l626 = l626 + 1) {
			fRec453[l626] = 0.0;
		}
		for (int l627 = 0; l627 < 2; l627 = l627 + 1) {
			fVec175[l627] = 0.0;
		}
		for (int l628 = 0; l628 < 2; l628 = l628 + 1) {
			fRec446[l628] = 0.0;
		}
		for (int l629 = 0; l629 < 2; l629 = l629 + 1) {
			fVec176[l629] = 0.0;
		}
		for (int l630 = 0; l630 < 2; l630 = l630 + 1) {
			fRec437[l630] = 0.0;
		}
		for (int l631 = 0; l631 < 2; l631 = l631 + 1) {
			fRec461[l631] = 0.0;
		}
		for (int l632 = 0; l632 < 2; l632 = l632 + 1) {
			fRec460[l632] = 0.0;
		}
		for (int l633 = 0; l633 < 2; l633 = l633 + 1) {
			fRec456[l633] = 0.0;
		}
		for (int l634 = 0; l634 < 2; l634 = l634 + 1) {
			fRec457[l634] = 0.0;
		}
		for (int l635 = 0; l635 < 2; l635 = l635 + 1) {
			iRec458[l635] = 0;
		}
		for (int l636 = 0; l636 < 2; l636 = l636 + 1) {
			iRec459[l636] = 0;
		}
		for (int l637 = 0; l637 < 262144; l637 = l637 + 1) {
			fVec177[l637] = 0.0;
		}
		for (int l638 = 0; l638 < 131072; l638 = l638 + 1) {
			fVec178[l638] = 0.0;
		}
		for (int l639 = 0; l639 < 2; l639 = l639 + 1) {
			fRec462[l639] = 0.0;
		}
		for (int l640 = 0; l640 < 2; l640 = l640 + 1) {
			fVec179[l640] = 0.0;
		}
		for (int l641 = 0; l641 < 2; l641 = l641 + 1) {
			fRec455[l641] = 0.0;
		}
		for (int l642 = 0; l642 < 2; l642 = l642 + 1) {
			fRec469[l642] = 0.0;
		}
		for (int l643 = 0; l643 < 2; l643 = l643 + 1) {
			fRec468[l643] = 0.0;
		}
		for (int l644 = 0; l644 < 2; l644 = l644 + 1) {
			fRec464[l644] = 0.0;
		}
		for (int l645 = 0; l645 < 2; l645 = l645 + 1) {
			fRec465[l645] = 0.0;
		}
		for (int l646 = 0; l646 < 2; l646 = l646 + 1) {
			iRec466[l646] = 0;
		}
		for (int l647 = 0; l647 < 2; l647 = l647 + 1) {
			iRec467[l647] = 0;
		}
		for (int l648 = 0; l648 < 262144; l648 = l648 + 1) {
			fVec180[l648] = 0.0;
		}
		for (int l649 = 0; l649 < 131072; l649 = l649 + 1) {
			fVec181[l649] = 0.0;
		}
		for (int l650 = 0; l650 < 2; l650 = l650 + 1) {
			fRec470[l650] = 0.0;
		}
		for (int l651 = 0; l651 < 2; l651 = l651 + 1) {
			fVec182[l651] = 0.0;
		}
		for (int l652 = 0; l652 < 2; l652 = l652 + 1) {
			fRec463[l652] = 0.0;
		}
		for (int l653 = 0; l653 < 2; l653 = l653 + 1) {
			fVec183[l653] = 0.0;
		}
		for (int l654 = 0; l654 < 2; l654 = l654 + 1) {
			fRec454[l654] = 0.0;
		}
		for (int l655 = 0; l655 < 2; l655 = l655 + 1) {
			fRec478[l655] = 0.0;
		}
		for (int l656 = 0; l656 < 2; l656 = l656 + 1) {
			fRec477[l656] = 0.0;
		}
		for (int l657 = 0; l657 < 2; l657 = l657 + 1) {
			fRec473[l657] = 0.0;
		}
		for (int l658 = 0; l658 < 2; l658 = l658 + 1) {
			fRec474[l658] = 0.0;
		}
		for (int l659 = 0; l659 < 2; l659 = l659 + 1) {
			iRec475[l659] = 0;
		}
		for (int l660 = 0; l660 < 2; l660 = l660 + 1) {
			iRec476[l660] = 0;
		}
		for (int l661 = 0; l661 < 262144; l661 = l661 + 1) {
			fVec184[l661] = 0.0;
		}
		for (int l662 = 0; l662 < 131072; l662 = l662 + 1) {
			fVec185[l662] = 0.0;
		}
		for (int l663 = 0; l663 < 2; l663 = l663 + 1) {
			fRec479[l663] = 0.0;
		}
		for (int l664 = 0; l664 < 2; l664 = l664 + 1) {
			fVec186[l664] = 0.0;
		}
		for (int l665 = 0; l665 < 2; l665 = l665 + 1) {
			fRec472[l665] = 0.0;
		}
		for (int l666 = 0; l666 < 2; l666 = l666 + 1) {
			fRec486[l666] = 0.0;
		}
		for (int l667 = 0; l667 < 2; l667 = l667 + 1) {
			fRec485[l667] = 0.0;
		}
		for (int l668 = 0; l668 < 2; l668 = l668 + 1) {
			fRec481[l668] = 0.0;
		}
		for (int l669 = 0; l669 < 2; l669 = l669 + 1) {
			fRec482[l669] = 0.0;
		}
		for (int l670 = 0; l670 < 2; l670 = l670 + 1) {
			iRec483[l670] = 0;
		}
		for (int l671 = 0; l671 < 2; l671 = l671 + 1) {
			iRec484[l671] = 0;
		}
		for (int l672 = 0; l672 < 262144; l672 = l672 + 1) {
			fVec187[l672] = 0.0;
		}
		for (int l673 = 0; l673 < 131072; l673 = l673 + 1) {
			fVec188[l673] = 0.0;
		}
		for (int l674 = 0; l674 < 2; l674 = l674 + 1) {
			fRec487[l674] = 0.0;
		}
		for (int l675 = 0; l675 < 2; l675 = l675 + 1) {
			fVec189[l675] = 0.0;
		}
		for (int l676 = 0; l676 < 2; l676 = l676 + 1) {
			fRec480[l676] = 0.0;
		}
		for (int l677 = 0; l677 < 2; l677 = l677 + 1) {
			fVec190[l677] = 0.0;
		}
		for (int l678 = 0; l678 < 2; l678 = l678 + 1) {
			fRec471[l678] = 0.0;
		}
		for (int l679 = 0; l679 < 2; l679 = l679 + 1) {
			fRec495[l679] = 0.0;
		}
		for (int l680 = 0; l680 < 2; l680 = l680 + 1) {
			fRec494[l680] = 0.0;
		}
		for (int l681 = 0; l681 < 2; l681 = l681 + 1) {
			fRec490[l681] = 0.0;
		}
		for (int l682 = 0; l682 < 2; l682 = l682 + 1) {
			fRec491[l682] = 0.0;
		}
		for (int l683 = 0; l683 < 2; l683 = l683 + 1) {
			iRec492[l683] = 0;
		}
		for (int l684 = 0; l684 < 2; l684 = l684 + 1) {
			iRec493[l684] = 0;
		}
		for (int l685 = 0; l685 < 262144; l685 = l685 + 1) {
			fVec191[l685] = 0.0;
		}
		for (int l686 = 0; l686 < 131072; l686 = l686 + 1) {
			fVec192[l686] = 0.0;
		}
		for (int l687 = 0; l687 < 2; l687 = l687 + 1) {
			fRec496[l687] = 0.0;
		}
		for (int l688 = 0; l688 < 2; l688 = l688 + 1) {
			fVec193[l688] = 0.0;
		}
		for (int l689 = 0; l689 < 2; l689 = l689 + 1) {
			fRec489[l689] = 0.0;
		}
		for (int l690 = 0; l690 < 2; l690 = l690 + 1) {
			fRec503[l690] = 0.0;
		}
		for (int l691 = 0; l691 < 2; l691 = l691 + 1) {
			fRec502[l691] = 0.0;
		}
		for (int l692 = 0; l692 < 2; l692 = l692 + 1) {
			fRec498[l692] = 0.0;
		}
		for (int l693 = 0; l693 < 2; l693 = l693 + 1) {
			fRec499[l693] = 0.0;
		}
		for (int l694 = 0; l694 < 2; l694 = l694 + 1) {
			iRec500[l694] = 0;
		}
		for (int l695 = 0; l695 < 2; l695 = l695 + 1) {
			iRec501[l695] = 0;
		}
		for (int l696 = 0; l696 < 262144; l696 = l696 + 1) {
			fVec194[l696] = 0.0;
		}
		for (int l697 = 0; l697 < 131072; l697 = l697 + 1) {
			fVec195[l697] = 0.0;
		}
		for (int l698 = 0; l698 < 2; l698 = l698 + 1) {
			fRec504[l698] = 0.0;
		}
		for (int l699 = 0; l699 < 2; l699 = l699 + 1) {
			fVec196[l699] = 0.0;
		}
		for (int l700 = 0; l700 < 2; l700 = l700 + 1) {
			fRec497[l700] = 0.0;
		}
		for (int l701 = 0; l701 < 2; l701 = l701 + 1) {
			fVec197[l701] = 0.0;
		}
		for (int l702 = 0; l702 < 2; l702 = l702 + 1) {
			fRec488[l702] = 0.0;
		}
		for (int l703 = 0; l703 < 2; l703 = l703 + 1) {
			fRec512[l703] = 0.0;
		}
		for (int l704 = 0; l704 < 2; l704 = l704 + 1) {
			fRec511[l704] = 0.0;
		}
		for (int l705 = 0; l705 < 2; l705 = l705 + 1) {
			fRec507[l705] = 0.0;
		}
		for (int l706 = 0; l706 < 2; l706 = l706 + 1) {
			fRec508[l706] = 0.0;
		}
		for (int l707 = 0; l707 < 2; l707 = l707 + 1) {
			iRec509[l707] = 0;
		}
		for (int l708 = 0; l708 < 2; l708 = l708 + 1) {
			iRec510[l708] = 0;
		}
		for (int l709 = 0; l709 < 262144; l709 = l709 + 1) {
			fVec198[l709] = 0.0;
		}
		for (int l710 = 0; l710 < 131072; l710 = l710 + 1) {
			fVec199[l710] = 0.0;
		}
		for (int l711 = 0; l711 < 2; l711 = l711 + 1) {
			fRec513[l711] = 0.0;
		}
		for (int l712 = 0; l712 < 2; l712 = l712 + 1) {
			fVec200[l712] = 0.0;
		}
		for (int l713 = 0; l713 < 2; l713 = l713 + 1) {
			fRec506[l713] = 0.0;
		}
		for (int l714 = 0; l714 < 2; l714 = l714 + 1) {
			fRec520[l714] = 0.0;
		}
		for (int l715 = 0; l715 < 2; l715 = l715 + 1) {
			fRec519[l715] = 0.0;
		}
		for (int l716 = 0; l716 < 2; l716 = l716 + 1) {
			fRec515[l716] = 0.0;
		}
		for (int l717 = 0; l717 < 2; l717 = l717 + 1) {
			fRec516[l717] = 0.0;
		}
		for (int l718 = 0; l718 < 2; l718 = l718 + 1) {
			iRec517[l718] = 0;
		}
		for (int l719 = 0; l719 < 2; l719 = l719 + 1) {
			iRec518[l719] = 0;
		}
		for (int l720 = 0; l720 < 262144; l720 = l720 + 1) {
			fVec201[l720] = 0.0;
		}
		for (int l721 = 0; l721 < 131072; l721 = l721 + 1) {
			fVec202[l721] = 0.0;
		}
		for (int l722 = 0; l722 < 2; l722 = l722 + 1) {
			fRec521[l722] = 0.0;
		}
		for (int l723 = 0; l723 < 2; l723 = l723 + 1) {
			fVec203[l723] = 0.0;
		}
		for (int l724 = 0; l724 < 2; l724 = l724 + 1) {
			fRec514[l724] = 0.0;
		}
		for (int l725 = 0; l725 < 2; l725 = l725 + 1) {
			fVec204[l725] = 0.0;
		}
		for (int l726 = 0; l726 < 2; l726 = l726 + 1) {
			fRec505[l726] = 0.0;
		}
		for (int l727 = 0; l727 < 2; l727 = l727 + 1) {
			fRec529[l727] = 0.0;
		}
		for (int l728 = 0; l728 < 2; l728 = l728 + 1) {
			fRec528[l728] = 0.0;
		}
		for (int l729 = 0; l729 < 2; l729 = l729 + 1) {
			fRec524[l729] = 0.0;
		}
		for (int l730 = 0; l730 < 2; l730 = l730 + 1) {
			fRec525[l730] = 0.0;
		}
		for (int l731 = 0; l731 < 2; l731 = l731 + 1) {
			iRec526[l731] = 0;
		}
		for (int l732 = 0; l732 < 2; l732 = l732 + 1) {
			iRec527[l732] = 0;
		}
		for (int l733 = 0; l733 < 262144; l733 = l733 + 1) {
			fVec205[l733] = 0.0;
		}
		for (int l734 = 0; l734 < 131072; l734 = l734 + 1) {
			fVec206[l734] = 0.0;
		}
		for (int l735 = 0; l735 < 2; l735 = l735 + 1) {
			fRec530[l735] = 0.0;
		}
		for (int l736 = 0; l736 < 2; l736 = l736 + 1) {
			fVec207[l736] = 0.0;
		}
		for (int l737 = 0; l737 < 2; l737 = l737 + 1) {
			fRec523[l737] = 0.0;
		}
		for (int l738 = 0; l738 < 2; l738 = l738 + 1) {
			fRec537[l738] = 0.0;
		}
		for (int l739 = 0; l739 < 2; l739 = l739 + 1) {
			fRec536[l739] = 0.0;
		}
		for (int l740 = 0; l740 < 2; l740 = l740 + 1) {
			fRec532[l740] = 0.0;
		}
		for (int l741 = 0; l741 < 2; l741 = l741 + 1) {
			fRec533[l741] = 0.0;
		}
		for (int l742 = 0; l742 < 2; l742 = l742 + 1) {
			iRec534[l742] = 0;
		}
		for (int l743 = 0; l743 < 2; l743 = l743 + 1) {
			iRec535[l743] = 0;
		}
		for (int l744 = 0; l744 < 262144; l744 = l744 + 1) {
			fVec208[l744] = 0.0;
		}
		for (int l745 = 0; l745 < 131072; l745 = l745 + 1) {
			fVec209[l745] = 0.0;
		}
		for (int l746 = 0; l746 < 2; l746 = l746 + 1) {
			fRec538[l746] = 0.0;
		}
		for (int l747 = 0; l747 < 2; l747 = l747 + 1) {
			fVec210[l747] = 0.0;
		}
		for (int l748 = 0; l748 < 2; l748 = l748 + 1) {
			fRec531[l748] = 0.0;
		}
		for (int l749 = 0; l749 < 2; l749 = l749 + 1) {
			fVec211[l749] = 0.0;
		}
		for (int l750 = 0; l750 < 2; l750 = l750 + 1) {
			fRec522[l750] = 0.0;
		}
		for (int l751 = 0; l751 < 2; l751 = l751 + 1) {
			fRec546[l751] = 0.0;
		}
		for (int l752 = 0; l752 < 2; l752 = l752 + 1) {
			fRec545[l752] = 0.0;
		}
		for (int l753 = 0; l753 < 2; l753 = l753 + 1) {
			fRec541[l753] = 0.0;
		}
		for (int l754 = 0; l754 < 2; l754 = l754 + 1) {
			fRec542[l754] = 0.0;
		}
		for (int l755 = 0; l755 < 2; l755 = l755 + 1) {
			iRec543[l755] = 0;
		}
		for (int l756 = 0; l756 < 2; l756 = l756 + 1) {
			iRec544[l756] = 0;
		}
		for (int l757 = 0; l757 < 262144; l757 = l757 + 1) {
			fVec212[l757] = 0.0;
		}
		for (int l758 = 0; l758 < 131072; l758 = l758 + 1) {
			fVec213[l758] = 0.0;
		}
		for (int l759 = 0; l759 < 2; l759 = l759 + 1) {
			fRec547[l759] = 0.0;
		}
		for (int l760 = 0; l760 < 2; l760 = l760 + 1) {
			fVec214[l760] = 0.0;
		}
		for (int l761 = 0; l761 < 2; l761 = l761 + 1) {
			fRec540[l761] = 0.0;
		}
		for (int l762 = 0; l762 < 2; l762 = l762 + 1) {
			fRec554[l762] = 0.0;
		}
		for (int l763 = 0; l763 < 2; l763 = l763 + 1) {
			fRec553[l763] = 0.0;
		}
		for (int l764 = 0; l764 < 2; l764 = l764 + 1) {
			fRec549[l764] = 0.0;
		}
		for (int l765 = 0; l765 < 2; l765 = l765 + 1) {
			fRec550[l765] = 0.0;
		}
		for (int l766 = 0; l766 < 2; l766 = l766 + 1) {
			iRec551[l766] = 0;
		}
		for (int l767 = 0; l767 < 2; l767 = l767 + 1) {
			iRec552[l767] = 0;
		}
		for (int l768 = 0; l768 < 262144; l768 = l768 + 1) {
			fVec215[l768] = 0.0;
		}
		for (int l769 = 0; l769 < 131072; l769 = l769 + 1) {
			fVec216[l769] = 0.0;
		}
		for (int l770 = 0; l770 < 2; l770 = l770 + 1) {
			fRec555[l770] = 0.0;
		}
		for (int l771 = 0; l771 < 2; l771 = l771 + 1) {
			fVec217[l771] = 0.0;
		}
		for (int l772 = 0; l772 < 2; l772 = l772 + 1) {
			fRec548[l772] = 0.0;
		}
		for (int l773 = 0; l773 < 2; l773 = l773 + 1) {
			fVec218[l773] = 0.0;
		}
		for (int l774 = 0; l774 < 2; l774 = l774 + 1) {
			fRec539[l774] = 0.0;
		}
	}
	
	virtual void init(int sample_rate) {
		classInit(sample_rate);
		instanceInit(sample_rate);
	}
	
	virtual void instanceInit(int sample_rate) {
		instanceConstants(sample_rate);
		instanceResetUserInterface();
		instanceClear();
	}
	
	virtual bbdmi_ambLagrangeMods31* clone() {
		return new bbdmi_ambLagrangeMods31();
	}
	
	virtual int getSampleRate() {
		return fSampleRate;
	}
	
	virtual void buildUserInterface(UI* ui_interface) {
		ui_interface->openVerticalBox("LagrangeModSynth");
		ui_interface->openVerticalBox("lagrangeModulations");
		ui_interface->declare(0, "00", "");
		ui_interface->openHorizontalBox("Randomizer");
		ui_interface->declare(&fVslider2, "00", "");
		ui_interface->declare(&fVslider2, "style", "knob");
		ui_interface->addVerticalSlider("dryWetRandom", &fVslider2, FAUSTFLOAT(1.0), FAUSTFLOAT(0.0), FAUSTFLOAT(1.0), FAUSTFLOAT(0.001));
		ui_interface->declare(&fButton0, "01", "");
		ui_interface->addButton("newRandom", &fButton0);
		ui_interface->declare(&fVslider0, "02", "");
		ui_interface->declare(&fVslider0, "style", "knob");
		ui_interface->declare(&fVslider0, "unit", "secs");
		ui_interface->addVerticalSlider("randomGetTime", &fVslider0, FAUSTFLOAT(5.0), FAUSTFLOAT(0.1), FAUSTFLOAT(3e+02), FAUSTFLOAT(0.1));
		ui_interface->declare(&fCheckbox0, "03", "");
		ui_interface->addCheckButton("freqRandom", &fCheckbox0);
		ui_interface->closeBox();
		ui_interface->declare(0, "01", "");
		ui_interface->openHorizontalBox("Parameters");
		ui_interface->declare(&fVslider1, "00", "");
		ui_interface->addVerticalSlider("indexdistr", &fVslider1, FAUSTFLOAT(0.0), FAUSTFLOAT(0.0), FAUSTFLOAT(21.0), FAUSTFLOAT(1.0));
		ui_interface->declare(&fVslider4, "01", "");
		ui_interface->addVerticalSlider("Freq", &fVslider4, FAUSTFLOAT(1.1e+02), FAUSTFLOAT(1e+01), FAUSTFLOAT(1e+04), FAUSTFLOAT(0.01));
		ui_interface->declare(&fVslider3, "02", "");
		ui_interface->declare(&fVslider3, "style", "knob");
		ui_interface->addVerticalSlider("retardFactor", &fVslider3, FAUSTFLOAT(0.0), FAUSTFLOAT(0.0), FAUSTFLOAT(1.0), FAUSTFLOAT(0.001));
		ui_interface->declare(&fVslider10, "03", "");
		ui_interface->addVerticalSlider("maxtransp", &fVslider10, FAUSTFLOAT(0.0), FAUSTFLOAT(0.0), FAUSTFLOAT(2e+01), FAUSTFLOAT(0.001));
		ui_interface->declare(&fVslider11, "04", "");
		ui_interface->addVerticalSlider("transpSide", &fVslider11, FAUSTFLOAT(0.0), FAUSTFLOAT(-1.0), FAUSTFLOAT(1.0), FAUSTFLOAT(0.001));
		ui_interface->declare(&fVslider6, "05", "");
		ui_interface->addVerticalSlider("modGlobalFreq", &fVslider6, FAUSTFLOAT(5e+02), FAUSTFLOAT(0.01), FAUSTFLOAT(1e+04), FAUSTFLOAT(0.01));
		ui_interface->declare(&fVslider5, "06", "");
		ui_interface->addVerticalSlider("y2modFactor", &fVslider5, FAUSTFLOAT(0.2), FAUSTFLOAT(0.0), FAUSTFLOAT(1e+01), FAUSTFLOAT(0.001));
		ui_interface->declare(&fVslider7, "07", "");
		ui_interface->addVerticalSlider("y3modFactor", &fVslider7, FAUSTFLOAT(0.2), FAUSTFLOAT(0.0), FAUSTFLOAT(1e+01), FAUSTFLOAT(0.001));
		ui_interface->declare(&fVslider8, "08", "");
		ui_interface->addVerticalSlider("y4modFactor", &fVslider8, FAUSTFLOAT(0.2), FAUSTFLOAT(0.0), FAUSTFLOAT(1e+01), FAUSTFLOAT(0.001));
		ui_interface->declare(&fVslider9, "09", "");
		ui_interface->addVerticalSlider("y5modFactor", &fVslider9, FAUSTFLOAT(0.2), FAUSTFLOAT(0.0), FAUSTFLOAT(1e+01), FAUSTFLOAT(0.001));
		ui_interface->closeBox();
		ui_interface->closeBox();
		ui_interface->closeBox();
	}
	
	virtual void compute(int count, FAUSTFLOAT** RESTRICT inputs, FAUSTFLOAT** RESTRICT outputs) {
		FAUSTFLOAT* output0 = outputs[0];
		FAUSTFLOAT* output1 = outputs[1];
		FAUSTFLOAT* output2 = outputs[2];
		FAUSTFLOAT* output3 = outputs[3];
		FAUSTFLOAT* output4 = outputs[4];
		FAUSTFLOAT* output5 = outputs[5];
		FAUSTFLOAT* output6 = outputs[6];
		FAUSTFLOAT* output7 = outputs[7];
		FAUSTFLOAT* output8 = outputs[8];
		FAUSTFLOAT* output9 = outputs[9];
		FAUSTFLOAT* output10 = outputs[10];
		FAUSTFLOAT* output11 = outputs[11];
		FAUSTFLOAT* output12 = outputs[12];
		FAUSTFLOAT* output13 = outputs[13];
		FAUSTFLOAT* output14 = outputs[14];
		FAUSTFLOAT* output15 = outputs[15];
		FAUSTFLOAT* output16 = outputs[16];
		FAUSTFLOAT* output17 = outputs[17];
		FAUSTFLOAT* output18 = outputs[18];
		FAUSTFLOAT* output19 = outputs[19];
		FAUSTFLOAT* output20 = outputs[20];
		FAUSTFLOAT* output21 = outputs[21];
		FAUSTFLOAT* output22 = outputs[22];
		FAUSTFLOAT* output23 = outputs[23];
		FAUSTFLOAT* output24 = outputs[24];
		FAUSTFLOAT* output25 = outputs[25];
		FAUSTFLOAT* output26 = outputs[26];
		FAUSTFLOAT* output27 = outputs[27];
		FAUSTFLOAT* output28 = outputs[28];
		FAUSTFLOAT* output29 = outputs[29];
		FAUSTFLOAT* output30 = outputs[30];
		double fSlow0 = fConst1 * double(fVslider0);
		double fSlow1 = fConst1 * double(fVslider1);
		double fSlow2 = fConst1 * double(fVslider2);
		int iSlow3 = int(double(fButton0));
		double fSlow4 = fConst1 * double(fVslider3);
		double fSlow5 = fConst1 * double(fVslider4);
		double fSlow6 = 1.0 - double(fCheckbox0);
		double fSlow7 = fConst1 * double(fVslider5);
		double fSlow8 = fConst1 * double(fVslider6);
		double fSlow9 = fConst1 * double(fVslider7);
		double fSlow10 = fConst1 * double(fVslider8);
		double fSlow11 = fConst1 * double(fVslider9);
		double fSlow12 = fConst1 * double(fVslider10);
		double fSlow13 = fConst1 * double(fVslider11);
		for (int i0 = 0; i0 < count; i0 = i0 + 1) {
			iVec0[0] = 1;
			fRec8[0] = fSlow0 + fConst2 * fRec8[1];
			double fTemp0 = 0.9999999999959746 * fRec8[0];
			int iTemp1 = std::fabs(fTemp0) < 2.220446049250313e-16;
			double fTemp2 = ((iTemp1) ? 0.0 : std::exp(-(fConst3 / ((iTemp1) ? 1.0 : fTemp0))));
			double fTemp3 = 1.0 - fTemp2;
			fRec9[0] = fSlow1 + fConst2 * fRec9[1];
			fRec11[0] = fSlow2 + fConst2 * fRec11[1];
			fRec10[0] = fConst5 * fRec11[0] + fConst4 * fRec10[1];
			iRec14[0] = 1103515245 * iRec14[1] + 12345;
			fRec13[0] = ((iSlow3) ? 4.656612875245797e-10 * double(iRec14[0]) : fRec13[1]);
			double fTemp4 = fRec13[0] + 1.0;
			fRec12[0] = 10.5 * fTemp4 * fTemp3 + fTemp2 * fRec12[1];
			int iTemp5 = int(fRec9[0] + fRec10[0] * (fRec12[0] - fRec9[0]));
			int iTemp6 = iTemp5 == 0;
			double fTemp7 = double(iTemp6);
			int iTemp8 = iTemp5 == 1;
			double fTemp9 = double(iTemp8);
			double fTemp10 = double(iTemp5 == 2);
			double fTemp11 = double(iTemp5 == 3);
			double fTemp12 = double(iTemp5 == 4);
			double fTemp13 = double(iTemp5 == 5);
			double fTemp14 = double(iTemp5 == 6);
			double fTemp15 = double(iTemp5 == 7);
			double fTemp16 = double(iTemp5 == 8);
			double fTemp17 = double(iTemp5 == 9);
			double fTemp18 = double(iTemp5 == 10);
			double fTemp19 = double(iTemp5 == 11);
			double fTemp20 = double(iTemp5 == 12);
			double fTemp21 = double(iTemp5 == 13);
			double fTemp22 = double(iTemp5 == 14);
			double fTemp23 = double(iTemp5 == 15);
			double fTemp24 = double(iTemp5 == 16);
			double fTemp25 = double(iTemp5 == 17);
			double fTemp26 = double(iTemp5 == 18);
			double fTemp27 = double(iTemp5 == 19);
			double fTemp28 = double(iTemp5 == 20);
			double fTemp29 = double(iTemp5 == 21);
			fRec7[0] = fTemp3 * (0.016129032258064516 * fTemp7 + 0.0002601456815816857 * fTemp9 + 0.025332714313187926 * fTemp10 + 0.023083613113041242 * fTemp11 + 0.1270001270001905 * fTemp12 + 0.00032092470356948066 * fTemp13 + 0.0006417464144736207 * fTemp14 + 0.03199791883454728 * fTemp15 + 0.0005202913631633714 * fTemp16 + 4.195898090027189e-06 * fTemp17 + 0.04761085562753853 * fTemp18 + 1.6783592360108757e-05 * fTemp19 + 6.767577564559982e-08 * fTemp20 + 0.06297197085935236 * fTemp21 + 5.414062051647986e-07 * fTemp22 + 1.0915447684774164e-09 * fTemp23 + 0.07808532616807251 * fTemp24 + 1.7464716295638662e-08 * fTemp25 + 0.0010920770197379254 * fTemp26 + 0.10577506682234361 * fTemp27 + 0.0001300813013633828 * fTemp28 + 0.17887962107111943 * fTemp29) + fTemp2 * fRec7[1];
			fRec15[0] = fSlow4 + fConst2 * fRec15[1];
			fRec16[0] = 0.5 * fRec15[0] * fTemp4 * fTemp3 + fTemp2 * fRec16[1];
			double fTemp30 = fRec15[0] + fRec10[0] * (fRec16[0] - fRec15[0]);
			fRec6[0] = fConst0 * fRec7[0] * fTemp30 * fTemp3 + fTemp2 * fRec6[1];
			int iTemp31 = int(fRec6[0]);
			double fTemp32 = ((fRec2[1] != 0.0) ? (((fRec3[1] > 0.0) & (fRec3[1] < 1.0)) ? fRec2[1] : 0.0) : (((fRec3[1] == 0.0) & (iTemp31 != iRec4[1])) ? 0.00048828125 : (((fRec3[1] == 1.0) & (iTemp31 != iRec5[1])) ? -0.00048828125 : 0.0)));
			fRec2[0] = fTemp32;
			fRec3[0] = std::max<double>(0.0, std::min<double>(1.0, fRec3[1] + fTemp32));
			iRec4[0] = (((fRec3[1] >= 1.0) & (iRec5[1] != iTemp31)) ? iTemp31 : iRec4[1]);
			iRec5[0] = (((fRec3[1] <= 0.0) & (iRec4[1] != iTemp31)) ? iTemp31 : iRec5[1]);
			int iTemp33 = 1 - iVec0[1];
			fRec18[0] = fSlow5 + fConst2 * fRec18[1];
			fRec19[0] = fTemp3 * (0.5 * fRec18[0] * fTemp4 + 2e+01) + fTemp2 * fRec19[1];
			fRec20[0] = fSlow6 * fTemp3 + fTemp2 * fRec20[1];
			double fTemp34 = ((iTemp33) ? 0.0 : fRec17[1] + fConst3 * (fRec18[0] + (fRec19[0] - fRec18[0]) * std::max<double>(fRec10[0] - fRec20[0], 0.0)));
			fRec17[0] = fTemp34 - std::floor(fTemp34);
			double fTemp35 = 1.0 - fRec17[0];
			double fTemp36 = 2e+02 * fRec17[0];
			double fTemp37 = fTemp36 + -1.6e+02;
			double fTemp38 = fTemp36 + -1.2e+02;
			double fTemp39 = fTemp36 + -8e+01;
			fRec21[0] = fSlow7 + fConst2 * fRec21[1];
			fRec22[0] = 0.5 * fRec21[0] * fTemp4 * fTemp3 + fTemp2 * fRec22[1];
			fRec25[0] = fSlow8 + fConst2 * fRec25[1];
			fRec26[0] = 0.5 * fRec25[0] * fTemp4 * fTemp3 + fTemp2 * fRec26[1];
			double fTemp40 = ((iTemp33) ? 0.0 : fRec24[1] + fConst3 * (fRec25[0] + fRec10[0] * (fRec26[0] - fRec25[0])));
			fRec24[0] = fTemp40 - std::floor(fTemp40);
			double fTemp41 = ftbl0bbdmi_ambLagrangeMods31SIG0[std::max<int>(0, std::min<int>(int(65536.0 * fRec24[0]), 65535))];
			double fTemp42 = std::sin(2.0 * fRec17[0]) + 1.0;
			double fTemp43 = 3.141592653589793 * (fRec21[0] + fRec10[0] * (fRec22[0] - fRec21[0])) * fTemp41 * fTemp42 + 6.283185307179586;
			double fTemp44 = fTemp36 + -4e+01;
			fRec27[0] = fSlow9 + fConst2 * fRec27[1];
			fRec28[0] = 0.5 * fRec27[0] * fTemp4 * fTemp3 + fTemp2 * fRec28[1];
			double fTemp45 = 3.141592653589793 * (fRec27[0] + fRec10[0] * (fRec28[0] - fRec27[0])) * fTemp41 * fTemp42 + 6.283185307179586;
			double fTemp46 = fTemp44 * fTemp39;
			fRec29[0] = fSlow10 + fConst2 * fRec29[1];
			fRec30[0] = 0.5 * fRec29[0] * fTemp4 * fTemp3 + fTemp2 * fRec30[1];
			double fTemp47 = 3.141592653589793 * (fRec29[0] + fRec10[0] * (fRec30[0] - fRec29[0])) * fTemp41 * fTemp42 + 6.283185307179586;
			double fTemp48 = fTemp46 * fTemp38;
			fRec31[0] = fSlow11 + fConst2 * fRec31[1];
			fRec32[0] = 0.5 * fRec31[0] * fTemp4 * fTemp3 + fTemp2 * fRec32[1];
			double fTemp49 = 3.141592653589793 * (fRec31[0] + fRec10[0] * (fRec32[0] - fRec31[0])) * fTemp41 * fTemp42 + 6.283185307179586;
			double fTemp50 = fRec17[0] * fRec7[0] * fTemp35 * (fTemp37 * (fTemp38 * (4.069010416666667e-06 * fTemp39 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec7[0] * fTemp43) + 1.0) + 8.138020833333335e-06 * fTemp44 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec7[0] * fTemp45) + 1.0)) + 8.138020833333335e-06 * fTemp46 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec7[0] * fTemp47) + 1.0)) + 4.069010416666667e-06 * fTemp48 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec7[0] * fTemp49) + 1.0));
			fVec2[IOTA0 & 262143] = fTemp50;
			double fTemp51 = fVec2[(IOTA0 - int(std::min<double>(fConst0, double(std::max<int>(0, iRec4[0]))))) & 262143];
			double fTemp52 = fRec3[0] * (fTemp51 - fVec2[(IOTA0 - int(std::min<double>(fConst0, double(std::max<int>(0, iRec5[0]))))) & 262143]) - fTemp51;
			fVec3[IOTA0 & 131071] = fTemp52;
			fRec34[0] = fSlow12 + fConst2 * fRec34[1];
			fRec35[0] = 0.5 * fRec34[0] * fTemp4 * fTemp3 + fTemp2 * fRec35[1];
			double fTemp53 = fRec34[0] + fRec10[0] * (fRec35[0] - fRec34[0]);
			fRec36[0] = fSlow13 + fConst2 * fRec36[1];
			fRec37[0] = 0.5 * fRec36[0] * fTemp4 * fTemp3 + fTemp2 * fRec37[1];
			double fTemp54 = fRec36[0] + fRec10[0] * (fRec37[0] - fRec36[0]);
			fRec33[0] = std::fmod(fRec33[1] + (10001.0 - std::pow(2.0, 0.08333333333333333 * fRec7[0] * fTemp53 * fTemp54)), 1e+04);
			int iTemp55 = int(fRec33[0]);
			double fTemp56 = std::floor(fRec33[0]);
			double fTemp57 = std::min<double>(0.0001 * fRec33[0], 1.0);
			double fTemp58 = fRec33[0] + 1e+04;
			int iTemp59 = int(fTemp58);
			double fTemp60 = std::floor(fTemp58);
			double fTemp61 = (fVec3[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp55))) & 131071] * (fTemp56 + (1.0 - fRec33[0])) + (fRec33[0] - fTemp56) * fVec3[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp55 + 1))) & 131071]) * fTemp57 + (fVec3[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp59))) & 131071] * (fTemp60 + (-9999.0 - fRec33[0])) + (fRec33[0] + (1e+04 - fTemp60)) * fVec3[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp59 + 1))) & 131071]) * (1.0 - fTemp57);
			fVec4[0] = fTemp61;
			fRec1[0] = 0.995 * fRec1[1] + fTemp61 - fVec4[1];
			fRec44[0] = fTemp3 * (0.5161290322580645 * fTemp7 + 0.26638917793964617 * fTemp9 + 0.72479278722912 * fTemp10 + 0.6003925412907621 * fTemp11 + 0.7184212081070996 * fTemp12 + 0.31103308092431337 * fTemp13 + 0.5253245844193564 * fTemp14 + 0.7658688865764828 * fTemp15 + 0.5317377731529656 * fTemp16 + 0.13749118861401094 * fTemp17 + 0.8867107515692658 * fTemp18 + 0.5468430062770635 * fTemp19 + 0.07096319412336048 * fTemp20 + 0.9451826217270641 * fTemp21 + 0.561460973816513 * fTemp22 + 0.03662616470883121 * fTemp23 + 0.9734754621259988 * fTemp24 + 0.5756073940159804 * fTemp25 + 0.03494646463161359 * fTemp26 + 0.9720554708381982 * fTemp27 + 0.14348915823537067 * fTemp28 + 0.8751393526613249 * fTemp29) + fTemp2 * fRec44[1];
			fRec43[0] = fConst0 * fTemp30 * fRec44[0] * fTemp3 + fTemp2 * fRec43[1];
			int iTemp62 = int(fRec43[0]);
			double fTemp63 = ((fRec39[1] != 0.0) ? (((fRec40[1] > 0.0) & (fRec40[1] < 1.0)) ? fRec39[1] : 0.0) : (((fRec40[1] == 0.0) & (iTemp62 != iRec41[1])) ? 0.00048828125 : (((fRec40[1] == 1.0) & (iTemp62 != iRec42[1])) ? -0.00048828125 : 0.0)));
			fRec39[0] = fTemp63;
			fRec40[0] = std::max<double>(0.0, std::min<double>(1.0, fRec40[1] + fTemp63));
			iRec41[0] = (((fRec40[1] >= 1.0) & (iRec42[1] != iTemp62)) ? iTemp62 : iRec41[1]);
			iRec42[0] = (((fRec40[1] <= 0.0) & (iRec41[1] != iTemp62)) ? iTemp62 : iRec42[1]);
			double fTemp64 = fRec17[0] * fRec44[0] * fTemp35 * (fTemp37 * (fTemp38 * (4.069010416666667e-06 * fTemp39 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec44[0] * fTemp43) + 1.0) + 8.138020833333335e-06 * fTemp44 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec44[0] * fTemp45) + 1.0)) + 8.138020833333335e-06 * fTemp46 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec44[0] * fTemp47) + 1.0)) + 4.069010416666667e-06 * fTemp48 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec44[0] * fTemp49) + 1.0));
			fVec5[IOTA0 & 262143] = fTemp64;
			double fTemp65 = fVec5[(IOTA0 - int(std::min<double>(fConst0, double(std::max<int>(0, iRec41[0]))))) & 262143];
			double fTemp66 = fRec40[0] * (fTemp65 - fVec5[(IOTA0 - int(std::min<double>(fConst0, double(std::max<int>(0, iRec42[0]))))) & 262143]) - fTemp65;
			fVec6[IOTA0 & 131071] = fTemp66;
			double fTemp67 = fTemp53 * fTemp54;
			fRec45[0] = std::fmod(fRec45[1] + (10001.0 - std::pow(2.0, 0.08333333333333333 * fTemp67 * fRec44[0])), 1e+04);
			int iTemp68 = int(fRec45[0]);
			double fTemp69 = std::floor(fRec45[0]);
			double fTemp70 = std::min<double>(0.0001 * fRec45[0], 1.0);
			double fTemp71 = fRec45[0] + 1e+04;
			int iTemp72 = int(fTemp71);
			double fTemp73 = std::floor(fTemp71);
			double fTemp74 = (fVec6[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp68))) & 131071] * (fTemp69 + (1.0 - fRec45[0])) + (fRec45[0] - fTemp69) * fVec6[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp68 + 1))) & 131071]) * fTemp70 + (fVec6[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp72))) & 131071] * (fTemp73 + (-9999.0 - fRec45[0])) + (fRec45[0] + (1e+04 - fTemp73)) * fVec6[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp72 + 1))) & 131071]) * (1.0 - fTemp70);
			fVec7[0] = fTemp74;
			fRec38[0] = 0.995 * fRec38[1] + fTemp74 - fVec7[1];
			double fTemp75 = std::min<double>(std::max<double>(fRec1[0], -1.0), 1.0) + std::min<double>(std::max<double>(fRec38[0], -1.0), 1.0);
			fVec8[0] = fTemp75;
			fRec0[0] = 0.995 * fRec0[1] - 0.125 * (fVec8[1] - fTemp75);
			output0[i0] = FAUSTFLOAT(std::min<double>(std::max<double>(fRec0[0], -1.0), 1.0));
			fRec53[0] = fTemp3 * (0.03225806451612903 * fTemp7 + 0.0010405827263267429 * fTemp9 + 0.05064916883871271 * fTemp10 + 0.045803689613124754 * fTemp11 + 0.1796053020267749 * fTemp12 + 0.0012834928289472414 * fTemp13 + 0.0025653383040524225 * fTemp14 + 0.06347554630593122 * fTemp15 + 0.0020811654526534857 * fTemp16 + 3.3567184720217515e-05 * fTemp17 + 0.09368601255412701 * fTemp18 + 0.00013426873888087006 * fTemp19 + 1.0828124103295972e-06 * fTemp20 + 0.12292194763302611 * fTemp21 + 8.662499282636778e-06 * fTemp22 + 3.4929432591277324e-08 * fTemp23 + 0.15121478803196076 * fTemp24 + 5.588709214604372e-07 * fTemp25 + 0.0012212553902486197 * fTemp26 + 0.20036176888341595 * fTemp27 + 0.0005204267851827149 * fTemp28 + 0.25194353793247254 * fTemp29) + fTemp2 * fRec53[1];
			fRec52[0] = fConst0 * fTemp30 * fRec53[0] * fTemp3 + fTemp2 * fRec52[1];
			int iTemp76 = int(fRec52[0]);
			double fTemp77 = ((fRec48[1] != 0.0) ? (((fRec49[1] > 0.0) & (fRec49[1] < 1.0)) ? fRec48[1] : 0.0) : (((fRec49[1] == 0.0) & (iTemp76 != iRec50[1])) ? 0.00048828125 : (((fRec49[1] == 1.0) & (iTemp76 != iRec51[1])) ? -0.00048828125 : 0.0)));
			fRec48[0] = fTemp77;
			fRec49[0] = std::max<double>(0.0, std::min<double>(1.0, fRec49[1] + fTemp77));
			iRec50[0] = (((fRec49[1] >= 1.0) & (iRec51[1] != iTemp76)) ? iTemp76 : iRec50[1]);
			iRec51[0] = (((fRec49[1] <= 0.0) & (iRec50[1] != iTemp76)) ? iTemp76 : iRec51[1]);
			double fTemp78 = fRec17[0] * fRec53[0] * fTemp35 * (fTemp37 * (fTemp38 * (4.069010416666667e-06 * fTemp39 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec53[0] * fTemp43) + 1.0) + 8.138020833333335e-06 * fTemp44 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec53[0] * fTemp45) + 1.0)) + 8.138020833333335e-06 * fTemp46 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec53[0] * fTemp47) + 1.0)) + 4.069010416666667e-06 * fTemp48 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec53[0] * fTemp49) + 1.0));
			fVec9[IOTA0 & 262143] = fTemp78;
			double fTemp79 = fVec9[(IOTA0 - int(std::min<double>(fConst0, double(std::max<int>(0, iRec50[0]))))) & 262143];
			double fTemp80 = fRec49[0] * (fTemp79 - fVec9[(IOTA0 - int(std::min<double>(fConst0, double(std::max<int>(0, iRec51[0]))))) & 262143]) - fTemp79;
			fVec10[IOTA0 & 131071] = fTemp80;
			fRec54[0] = std::fmod(fRec54[1] + (10001.0 - std::pow(2.0, 0.08333333333333333 * fTemp67 * fRec53[0])), 1e+04);
			int iTemp81 = int(fRec54[0]);
			double fTemp82 = std::floor(fRec54[0]);
			double fTemp83 = std::min<double>(0.0001 * fRec54[0], 1.0);
			double fTemp84 = fRec54[0] + 1e+04;
			int iTemp85 = int(fTemp84);
			double fTemp86 = std::floor(fTemp84);
			double fTemp87 = (fVec10[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp81))) & 131071] * (fTemp82 + (1.0 - fRec54[0])) + (fRec54[0] - fTemp82) * fVec10[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp81 + 1))) & 131071]) * fTemp83 + (fVec10[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp85))) & 131071] * (fTemp86 + (-9999.0 - fRec54[0])) + (fRec54[0] + (1e+04 - fTemp86)) * fVec10[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp85 + 1))) & 131071]) * (1.0 - fTemp83);
			fVec11[0] = fTemp87;
			fRec47[0] = 0.995 * fRec47[1] + fTemp87 - fVec11[1];
			fRec61[0] = fTemp3 * (0.532258064516129 * fTemp7 + 0.28329864724245574 * fTemp9 + 0.7420135854509108 * fTemp10 + 0.6156592979440726 * fTemp11 + 0.7295601856708801 * fTemp12 + 0.3296151560437215 * fTemp13 + 0.5505841609937161 * fTemp14 + 0.7812174817898023 * fTemp15 + 0.5624349635796045 * fTemp16 + 0.15078798966130708 * fTemp17 + 0.8976662414823269 * fTemp18 + 0.5906649659293075 * fTemp19 + 0.08025812352940538 * fTemp20 + 0.9521342097256045 * fTemp21 + 0.617073677804836 * fTemp22 + 0.042718033491457696 * fTemp23 + 0.9776111626135892 * fTemp24 + 0.6417786018174272 * fTemp25 + 0.03908017248795583 * fTemp26 + 0.9750113052776067 * fTemp27 + 0.15341784051543805 * fTemp28 + 0.8838650812142101 * fTemp29) + fTemp2 * fRec61[1];
			fRec60[0] = fConst0 * fTemp30 * fRec61[0] * fTemp3 + fTemp2 * fRec60[1];
			int iTemp88 = int(fRec60[0]);
			double fTemp89 = ((fRec56[1] != 0.0) ? (((fRec57[1] > 0.0) & (fRec57[1] < 1.0)) ? fRec56[1] : 0.0) : (((fRec57[1] == 0.0) & (iTemp88 != iRec58[1])) ? 0.00048828125 : (((fRec57[1] == 1.0) & (iTemp88 != iRec59[1])) ? -0.00048828125 : 0.0)));
			fRec56[0] = fTemp89;
			fRec57[0] = std::max<double>(0.0, std::min<double>(1.0, fRec57[1] + fTemp89));
			iRec58[0] = (((fRec57[1] >= 1.0) & (iRec59[1] != iTemp88)) ? iTemp88 : iRec58[1]);
			iRec59[0] = (((fRec57[1] <= 0.0) & (iRec58[1] != iTemp88)) ? iTemp88 : iRec59[1]);
			double fTemp90 = fRec17[0] * fRec61[0] * fTemp35 * (fTemp37 * (fTemp38 * (4.069010416666667e-06 * fTemp39 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec61[0] * fTemp43) + 1.0) + 8.138020833333335e-06 * fTemp44 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec61[0] * fTemp45) + 1.0)) + 8.138020833333335e-06 * fTemp46 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec61[0] * fTemp47) + 1.0)) + 4.069010416666667e-06 * fTemp48 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec61[0] * fTemp49) + 1.0));
			fVec12[IOTA0 & 262143] = fTemp90;
			double fTemp91 = fVec12[(IOTA0 - int(std::min<double>(fConst0, double(std::max<int>(0, iRec58[0]))))) & 262143];
			double fTemp92 = fRec57[0] * (fTemp91 - fVec12[(IOTA0 - int(std::min<double>(fConst0, double(std::max<int>(0, iRec59[0]))))) & 262143]) - fTemp91;
			fVec13[IOTA0 & 131071] = fTemp92;
			fRec62[0] = std::fmod(fRec62[1] + (10001.0 - std::pow(2.0, 0.08333333333333333 * fTemp67 * fRec61[0])), 1e+04);
			int iTemp93 = int(fRec62[0]);
			double fTemp94 = std::floor(fRec62[0]);
			double fTemp95 = std::min<double>(0.0001 * fRec62[0], 1.0);
			double fTemp96 = fRec62[0] + 1e+04;
			int iTemp97 = int(fTemp96);
			double fTemp98 = std::floor(fTemp96);
			double fTemp99 = (fVec13[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp93))) & 131071] * (fTemp94 + (1.0 - fRec62[0])) + (fRec62[0] - fTemp94) * fVec13[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp93 + 1))) & 131071]) * fTemp95 + (fVec13[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp97))) & 131071] * (fTemp98 + (-9999.0 - fRec62[0])) + (fRec62[0] + (1e+04 - fTemp98)) * fVec13[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp97 + 1))) & 131071]) * (1.0 - fTemp95);
			fVec14[0] = fTemp99;
			fRec55[0] = 0.995 * fRec55[1] + fTemp99 - fVec14[1];
			double fTemp100 = std::min<double>(std::max<double>(fRec47[0], -1.0), 1.0) + std::min<double>(std::max<double>(fRec55[0], -1.0), 1.0);
			fVec15[0] = fTemp100;
			fRec46[0] = 0.995 * fRec46[1] - 0.125 * (fVec15[1] - fTemp100);
			output1[i0] = FAUSTFLOAT(std::min<double>(std::max<double>(fRec46[0], -1.0), 1.0));
			fRec70[0] = fTemp3 * (0.04838709677419355 * fTemp7 + 0.0023413111342351716 * fTemp9 + 0.07593311422524629 * fTemp10 + 0.06817150264157924 * fTemp11 + 0.21997067253202993 * fTemp12 + 0.0028870865523525913 * fTemp13 + 0.00576583783594431 * fTemp14 + 0.09443288241415182 * fTemp15 + 0.004682622268470343 * fTemp16 + 0.0001132892484307341 * fTemp17 + 0.13825064616830574 * fTemp18 + 0.0004531569937229364 * fTemp19 + 5.4817378272935855e-06 * fTemp20 + 0.1799481955472586 * fTemp21 + 4.3853902618348684e-05 * fTemp22 + 2.6524537874001217e-07 * fTemp23 + 0.21962812156916545 * fTemp24 + 4.243926059840195e-06 * fTemp25 + 0.001365713865647708 * fTemp26 + 0.28494355621347334 * fTemp27 + 0.0011713415876750677 * fTemp28 + 0.3072993368267361 * fTemp29) + fTemp2 * fRec70[1];
			fRec69[0] = fConst0 * fTemp30 * fRec70[0] * fTemp3 + fTemp2 * fRec69[1];
			int iTemp101 = int(fRec69[0]);
			double fTemp102 = ((fRec65[1] != 0.0) ? (((fRec66[1] > 0.0) & (fRec66[1] < 1.0)) ? fRec65[1] : 0.0) : (((fRec66[1] == 0.0) & (iTemp101 != iRec67[1])) ? 0.00048828125 : (((fRec66[1] == 1.0) & (iTemp101 != iRec68[1])) ? -0.00048828125 : 0.0)));
			fRec65[0] = fTemp102;
			fRec66[0] = std::max<double>(0.0, std::min<double>(1.0, fRec66[1] + fTemp102));
			iRec67[0] = (((fRec66[1] >= 1.0) & (iRec68[1] != iTemp101)) ? iTemp101 : iRec67[1]);
			iRec68[0] = (((fRec66[1] <= 0.0) & (iRec67[1] != iTemp101)) ? iTemp101 : iRec68[1]);
			double fTemp103 = fRec17[0] * fRec70[0] * fTemp35 * (fTemp37 * (fTemp38 * (4.069010416666667e-06 * fTemp39 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec70[0] * fTemp43) + 1.0) + 8.138020833333335e-06 * fTemp44 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec70[0] * fTemp45) + 1.0)) + 8.138020833333335e-06 * fTemp46 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec70[0] * fTemp47) + 1.0)) + 4.069010416666667e-06 * fTemp48 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec70[0] * fTemp49) + 1.0));
			fVec16[IOTA0 & 262143] = fTemp103;
			double fTemp104 = fVec16[(IOTA0 - int(std::min<double>(fConst0, double(std::max<int>(0, iRec67[0]))))) & 262143];
			double fTemp105 = fRec66[0] * (fTemp104 - fVec16[(IOTA0 - int(std::min<double>(fConst0, double(std::max<int>(0, iRec68[0]))))) & 262143]) - fTemp104;
			fVec17[IOTA0 & 131071] = fTemp105;
			fRec71[0] = std::fmod(fRec71[1] + (10001.0 - std::pow(2.0, 0.08333333333333333 * fTemp67 * fRec70[0])), 1e+04);
			int iTemp106 = int(fRec71[0]);
			double fTemp107 = std::floor(fRec71[0]);
			double fTemp108 = std::min<double>(0.0001 * fRec71[0], 1.0);
			double fTemp109 = fRec71[0] + 1e+04;
			int iTemp110 = int(fTemp109);
			double fTemp111 = std::floor(fTemp109);
			double fTemp112 = (fVec17[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp106))) & 131071] * (fTemp107 + (1.0 - fRec71[0])) + (fRec71[0] - fTemp107) * fVec17[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp106 + 1))) & 131071]) * fTemp108 + (fVec17[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp110))) & 131071] * (fTemp111 + (-9999.0 - fRec71[0])) + (fRec71[0] + (1e+04 - fTemp111)) * fVec17[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp110 + 1))) & 131071]) * (1.0 - fTemp108);
			fVec18[0] = fTemp112;
			fRec64[0] = 0.995 * fRec64[1] + fTemp112 - fVec18[1];
			fRec78[0] = fTemp3 * (0.5483870967741935 * fTemp7 + 0.3007284079084287 * fTemp9 + 0.758758122692791 * fTemp10 + 0.6307661903342809 * fTemp11 + 0.7405316311773545 * fTemp12 + 0.34862751727777785 * fTemp13 + 0.5757138887522883 * fTemp14 + 0.7960457856399583 * fTemp15 + 0.5920915712799166 * fTemp16 + 0.16491557853042862 * fTemp17 + 0.9078916451277231 * fTemp18 + 0.6315665805108924 * fTemp19 + 0.09043757532313826 * fTemp20 + 0.9584026784447782 * fTemp21 + 0.6672214275582253 * fTemp22 + 0.049594799370753236 * fTemp23 + 0.9812141128460289 * fTemp24 + 0.6994258055364616 * fTemp25 + 0.04370284370072663 * fTemp26 + 0.977654486131671 * fTemp27 + 0.16377539375382444 * fTemp28 + 0.8922139797380213 * fTemp29) + fTemp2 * fRec78[1];
			fRec77[0] = fConst0 * fTemp30 * fRec78[0] * fTemp3 + fTemp2 * fRec77[1];
			int iTemp113 = int(fRec77[0]);
			double fTemp114 = ((fRec73[1] != 0.0) ? (((fRec74[1] > 0.0) & (fRec74[1] < 1.0)) ? fRec73[1] : 0.0) : (((fRec74[1] == 0.0) & (iTemp113 != iRec75[1])) ? 0.00048828125 : (((fRec74[1] == 1.0) & (iTemp113 != iRec76[1])) ? -0.00048828125 : 0.0)));
			fRec73[0] = fTemp114;
			fRec74[0] = std::max<double>(0.0, std::min<double>(1.0, fRec74[1] + fTemp114));
			iRec75[0] = (((fRec74[1] >= 1.0) & (iRec76[1] != iTemp113)) ? iTemp113 : iRec75[1]);
			iRec76[0] = (((fRec74[1] <= 0.0) & (iRec75[1] != iTemp113)) ? iTemp113 : iRec76[1]);
			double fTemp115 = fRec17[0] * fRec78[0] * fTemp35 * (fTemp37 * (fTemp38 * (4.069010416666667e-06 * fTemp39 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec78[0] * fTemp43) + 1.0) + 8.138020833333335e-06 * fTemp44 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec78[0] * fTemp45) + 1.0)) + 8.138020833333335e-06 * fTemp46 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec78[0] * fTemp47) + 1.0)) + 4.069010416666667e-06 * fTemp48 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec78[0] * fTemp49) + 1.0));
			fVec19[IOTA0 & 262143] = fTemp115;
			double fTemp116 = fVec19[(IOTA0 - int(std::min<double>(fConst0, double(std::max<int>(0, iRec75[0]))))) & 262143];
			double fTemp117 = fRec74[0] * (fTemp116 - fVec19[(IOTA0 - int(std::min<double>(fConst0, double(std::max<int>(0, iRec76[0]))))) & 262143]) - fTemp116;
			fVec20[IOTA0 & 131071] = fTemp117;
			fRec79[0] = std::fmod(fRec79[1] + (10001.0 - std::pow(2.0, 0.08333333333333333 * fTemp67 * fRec78[0])), 1e+04);
			int iTemp118 = int(fRec79[0]);
			double fTemp119 = std::floor(fRec79[0]);
			double fTemp120 = std::min<double>(0.0001 * fRec79[0], 1.0);
			double fTemp121 = fRec79[0] + 1e+04;
			int iTemp122 = int(fTemp121);
			double fTemp123 = std::floor(fTemp121);
			double fTemp124 = (fVec20[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp118))) & 131071] * (fTemp119 + (1.0 - fRec79[0])) + (fRec79[0] - fTemp119) * fVec20[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp118 + 1))) & 131071]) * fTemp120 + (fVec20[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp122))) & 131071] * (fTemp123 + (-9999.0 - fRec79[0])) + (fRec79[0] + (1e+04 - fTemp123)) * fVec20[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp122 + 1))) & 131071]) * (1.0 - fTemp120);
			fVec21[0] = fTemp124;
			fRec72[0] = 0.995 * fRec72[1] + fTemp124 - fVec21[1];
			double fTemp125 = std::min<double>(std::max<double>(fRec64[0], -1.0), 1.0) + std::min<double>(std::max<double>(fRec72[0], -1.0), 1.0);
			fVec22[0] = fTemp125;
			fRec63[0] = 0.995 * fRec63[1] - 0.125 * (fVec22[1] - fTemp125);
			output2[i0] = FAUSTFLOAT(std::min<double>(std::max<double>(fRec63[0], -1.0), 1.0));
			fRec87[0] = fTemp3 * (0.06451612903225806 * fTemp7 + 0.004162330905306971 * fTemp9 + 0.10116832198743217 * fTemp10 + 0.09019780897157816 * fTemp11 + 0.254000254000381 * fTemp12 + 0.005130676608104845 * fTemp13 + 0.010235029373752758 * fTemp14 + 0.12486992715920908 * fTemp15 + 0.008324661810613943 * fTemp16 + 0.0002685374777617401 * fTemp17 + 0.1813299318586149 * fTemp18 + 0.0010741499110469605 * fTemp19 + 1.7324998565273555e-05 * fTemp20 + 0.23414735560967193 * fTemp21 + 0.00013859998852218844 * fTemp22 + 1.1177418429208744e-06 * fTemp23 + 0.28355720363485437 * fTemp24 + 1.788386948673399e-05 * fTemp25 + 0.0015272598816883799 * fTemp26 + 0.3605786993367406 * fTemp27 + 0.00208333559625673 * fTemp28 + 0.3533693919388167 * fTemp29) + fTemp2 * fRec87[1];
			fRec86[0] = fConst0 * fTemp30 * fRec87[0] * fTemp3 + fTemp2 * fRec86[1];
			int iTemp126 = int(fRec86[0]);
			double fTemp127 = ((fRec82[1] != 0.0) ? (((fRec83[1] > 0.0) & (fRec83[1] < 1.0)) ? fRec82[1] : 0.0) : (((fRec83[1] == 0.0) & (iTemp126 != iRec84[1])) ? 0.00048828125 : (((fRec83[1] == 1.0) & (iTemp126 != iRec85[1])) ? -0.00048828125 : 0.0)));
			fRec82[0] = fTemp127;
			fRec83[0] = std::max<double>(0.0, std::min<double>(1.0, fRec83[1] + fTemp127));
			iRec84[0] = (((fRec83[1] >= 1.0) & (iRec85[1] != iTemp126)) ? iTemp126 : iRec84[1]);
			iRec85[0] = (((fRec83[1] <= 0.0) & (iRec84[1] != iTemp126)) ? iTemp126 : iRec85[1]);
			double fTemp128 = fRec17[0] * fRec87[0] * fTemp35 * (fTemp37 * (fTemp38 * (4.069010416666667e-06 * fTemp39 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec87[0] * fTemp43) + 1.0) + 8.138020833333335e-06 * fTemp44 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec87[0] * fTemp45) + 1.0)) + 8.138020833333335e-06 * fTemp46 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec87[0] * fTemp47) + 1.0)) + 4.069010416666667e-06 * fTemp48 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec87[0] * fTemp49) + 1.0));
			fVec23[IOTA0 & 262143] = fTemp128;
			double fTemp129 = fVec23[(IOTA0 - int(std::min<double>(fConst0, double(std::max<int>(0, iRec84[0]))))) & 262143];
			double fTemp130 = fRec83[0] * (fTemp129 - fVec23[(IOTA0 - int(std::min<double>(fConst0, double(std::max<int>(0, iRec85[0]))))) & 262143]) - fTemp129;
			fVec24[IOTA0 & 131071] = fTemp130;
			fRec88[0] = std::fmod(fRec88[1] + (10001.0 - std::pow(2.0, 0.08333333333333333 * fTemp67 * fRec87[0])), 1e+04);
			int iTemp131 = int(fRec88[0]);
			double fTemp132 = std::floor(fRec88[0]);
			double fTemp133 = std::min<double>(0.0001 * fRec88[0], 1.0);
			double fTemp134 = fRec88[0] + 1e+04;
			int iTemp135 = int(fTemp134);
			double fTemp136 = std::floor(fTemp134);
			double fTemp137 = (fVec24[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp131))) & 131071] * (fTemp132 + (1.0 - fRec88[0])) + (fRec88[0] - fTemp132) * fVec24[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp131 + 1))) & 131071]) * fTemp133 + (fVec24[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp135))) & 131071] * (fTemp136 + (-9999.0 - fRec88[0])) + (fRec88[0] + (1e+04 - fTemp136)) * fVec24[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp135 + 1))) & 131071]) * (1.0 - fTemp133);
			fVec25[0] = fTemp137;
			fRec81[0] = 0.995 * fRec81[1] + fTemp137 - fVec25[1];
			fRec95[0] = fTemp3 * (0.5645161290322581 * fTemp7 + 0.3186784599375651 * fTemp9 + 0.7750156514834587 * fTemp10 + 0.6457165318002523 * fTemp11 + 0.7513428837969107 * fTemp12 + 0.36805796155369597 * fTemp13 + 0.60064926004433 * fTemp14 + 0.8103537981269511 * fTemp15 + 0.6207075962539023 * fTemp16 + 0.1798991306099158 * fTemp17 + 0.9174121378939949 * fTemp18 + 0.6696485515759795 * fTemp19 + 0.10155596082817828 * fTemp20 + 0.9640343181151267 * fTemp21 + 0.7122745449210144 * fTemp22 + 0.05732997788687484 * fTemp23 + 0.984337525630781 * fTemp24 + 0.7494004100924965 * fTemp25 + 0.04887231621402819 * fTemp26 + 0.9800180843542732 * fTemp27 + 0.17457796245651713 * fTemp28 + 0.9001965330565048 * fTemp29) + fTemp2 * fRec95[1];
			fRec94[0] = fConst0 * fTemp30 * fRec95[0] * fTemp3 + fTemp2 * fRec94[1];
			int iTemp138 = int(fRec94[0]);
			double fTemp139 = ((fRec90[1] != 0.0) ? (((fRec91[1] > 0.0) & (fRec91[1] < 1.0)) ? fRec90[1] : 0.0) : (((fRec91[1] == 0.0) & (iTemp138 != iRec92[1])) ? 0.00048828125 : (((fRec91[1] == 1.0) & (iTemp138 != iRec93[1])) ? -0.00048828125 : 0.0)));
			fRec90[0] = fTemp139;
			fRec91[0] = std::max<double>(0.0, std::min<double>(1.0, fRec91[1] + fTemp139));
			iRec92[0] = (((fRec91[1] >= 1.0) & (iRec93[1] != iTemp138)) ? iTemp138 : iRec92[1]);
			iRec93[0] = (((fRec91[1] <= 0.0) & (iRec92[1] != iTemp138)) ? iTemp138 : iRec93[1]);
			double fTemp140 = fRec17[0] * fRec95[0] * fTemp35 * (fTemp37 * (fTemp38 * (4.069010416666667e-06 * fTemp39 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec95[0] * fTemp43) + 1.0) + 8.138020833333335e-06 * fTemp44 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec95[0] * fTemp45) + 1.0)) + 8.138020833333335e-06 * fTemp46 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec95[0] * fTemp47) + 1.0)) + 4.069010416666667e-06 * fTemp48 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec95[0] * fTemp49) + 1.0));
			fVec26[IOTA0 & 262143] = fTemp140;
			double fTemp141 = fVec26[(IOTA0 - int(std::min<double>(fConst0, double(std::max<int>(0, iRec92[0]))))) & 262143];
			double fTemp142 = fRec91[0] * (fTemp141 - fVec26[(IOTA0 - int(std::min<double>(fConst0, double(std::max<int>(0, iRec93[0]))))) & 262143]) - fTemp141;
			fVec27[IOTA0 & 131071] = fTemp142;
			fRec96[0] = std::fmod(fRec96[1] + (10001.0 - std::pow(2.0, 0.08333333333333333 * fTemp67 * fRec95[0])), 1e+04);
			int iTemp143 = int(fRec96[0]);
			double fTemp144 = std::floor(fRec96[0]);
			double fTemp145 = std::min<double>(0.0001 * fRec96[0], 1.0);
			double fTemp146 = fRec96[0] + 1e+04;
			int iTemp147 = int(fTemp146);
			double fTemp148 = std::floor(fTemp146);
			double fTemp149 = (fVec27[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp143))) & 131071] * (fTemp144 + (1.0 - fRec96[0])) + (fRec96[0] - fTemp144) * fVec27[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp143 + 1))) & 131071]) * fTemp145 + (fVec27[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp147))) & 131071] * (fTemp148 + (-9999.0 - fRec96[0])) + (fRec96[0] + (1e+04 - fTemp148)) * fVec27[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp147 + 1))) & 131071]) * (1.0 - fTemp145);
			fVec28[0] = fTemp149;
			fRec89[0] = 0.995 * fRec89[1] + fTemp149 - fVec28[1];
			double fTemp150 = std::min<double>(std::max<double>(fRec81[0], -1.0), 1.0) + std::min<double>(std::max<double>(fRec89[0], -1.0), 1.0);
			fVec29[0] = fTemp150;
			fRec80[0] = 0.995 * fRec80[1] - 0.125 * (fVec29[1] - fTemp150);
			output3[i0] = FAUSTFLOAT(std::min<double>(std::max<double>(fRec80[0], -1.0), 1.0));
			fRec104[0] = fTemp3 * (0.08064516129032258 * fTemp7 + 0.0065036420395421434 * fTemp9 + 0.1263385949221292 * fTemp10 + 0.11189288007089712 * fTemp11 + 0.2839809171235324 * fTemp12 + 0.008012822949256981 * fTemp13 + 0.015961440566897855 * fTemp14 + 0.15478668054110312 * fTemp15 + 0.013007284079084287 * fTemp16 + 0.0005244872612533986 * fTemp17 + 0.2229490450135948 * fTemp18 + 0.0020979490450135944 * fTemp19 + 4.229735977849989e-05 * fTemp20 + 0.28561444460927266 * fTemp21 + 0.00033837887822799913 * fTemp22 + 3.4110774014919266e-06 * fTemp23 + 0.3432261829472346 * fTemp24 + 5.4577238423870825e-05 * fTemp25 + 0.0017079146700385715 * fTemp26 + 0.4282135301420267 * fTemp27 + 0.0032571254528789284 * fTemp28 + 0.3934293844403378 * fTemp29) + fTemp2 * fRec104[1];
			fRec103[0] = fConst0 * fTemp30 * fRec104[0] * fTemp3 + fTemp2 * fRec103[1];
			int iTemp151 = int(fRec103[0]);
			double fTemp152 = ((fRec99[1] != 0.0) ? (((fRec100[1] > 0.0) & (fRec100[1] < 1.0)) ? fRec99[1] : 0.0) : (((fRec100[1] == 0.0) & (iTemp151 != iRec101[1])) ? 0.00048828125 : (((fRec100[1] == 1.0) & (iTemp151 != iRec102[1])) ? -0.00048828125 : 0.0)));
			fRec99[0] = fTemp152;
			fRec100[0] = std::max<double>(0.0, std::min<double>(1.0, fRec100[1] + fTemp152));
			iRec101[0] = (((fRec100[1] >= 1.0) & (iRec102[1] != iTemp151)) ? iTemp151 : iRec101[1]);
			iRec102[0] = (((fRec100[1] <= 0.0) & (iRec101[1] != iTemp151)) ? iTemp151 : iRec102[1]);
			double fTemp153 = fRec17[0] * fRec104[0] * fTemp35 * (fTemp37 * (fTemp38 * (4.069010416666667e-06 * fTemp39 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec104[0] * fTemp43) + 1.0) + 8.138020833333335e-06 * fTemp44 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec104[0] * fTemp45) + 1.0)) + 8.138020833333335e-06 * fTemp46 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec104[0] * fTemp47) + 1.0)) + 4.069010416666667e-06 * fTemp48 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec104[0] * fTemp49) + 1.0));
			fVec30[IOTA0 & 262143] = fTemp153;
			double fTemp154 = fVec30[(IOTA0 - int(std::min<double>(fConst0, double(std::max<int>(0, iRec101[0]))))) & 262143];
			double fTemp155 = fRec100[0] * (fTemp154 - fVec30[(IOTA0 - int(std::min<double>(fConst0, double(std::max<int>(0, iRec102[0]))))) & 262143]) - fTemp154;
			fVec31[IOTA0 & 131071] = fTemp155;
			fRec105[0] = std::fmod(fRec105[1] + (10001.0 - std::pow(2.0, 0.08333333333333333 * fTemp67 * fRec104[0])), 1e+04);
			int iTemp156 = int(fRec105[0]);
			double fTemp157 = std::floor(fRec105[0]);
			double fTemp158 = std::min<double>(0.0001 * fRec105[0], 1.0);
			double fTemp159 = fRec105[0] + 1e+04;
			int iTemp160 = int(fTemp159);
			double fTemp161 = std::floor(fTemp159);
			double fTemp162 = (fVec31[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp156))) & 131071] * (fTemp157 + (1.0 - fRec105[0])) + (fRec105[0] - fTemp157) * fVec31[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp156 + 1))) & 131071]) * fTemp158 + (fVec31[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp160))) & 131071] * (fTemp161 + (-9999.0 - fRec105[0])) + (fRec105[0] + (1e+04 - fTemp161)) * fVec31[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp160 + 1))) & 131071]) * (1.0 - fTemp158);
			fVec32[0] = fTemp162;
			fRec98[0] = 0.995 * fRec98[1] + fTemp162 - fVec32[1];
			fRec112[0] = fTemp3 * (0.5806451612903226 * fTemp7 + 0.33714880332986474 * fTemp9 + 0.7907757369376985 * fTemp10 + 0.6605135337283329 * fTemp11 + 0.762000762001143 * fTemp12 + 0.3878940174523371 * fTemp13 + 0.6253262661293602 * fTemp14 + 0.8241415192507805 * fTemp15 + 0.648283038501561 * fTemp16 + 0.19576382128830858 * fTemp17 + 0.9262528951696821 * fTemp18 + 0.7050115806787285 * fTemp19 + 0.11366931558675983 * fTemp20 + 0.9690737947485764 * fTemp21 + 0.7525903579886111 * fTemp22 + 0.06600153808263474 * fTemp23 + 0.9870309461848868 * fTemp24 + 0.79249513895819 * fTemp25 + 0.05465326944123429 * fTemp26 + 0.9821316728169384 * fTemp27 + 0.18584326037909915 * fTemp28 + 0.9078224051271154 * fTemp29) + fTemp2 * fRec112[1];
			fRec111[0] = fConst0 * fTemp30 * fRec112[0] * fTemp3 + fTemp2 * fRec111[1];
			int iTemp163 = int(fRec111[0]);
			double fTemp164 = ((fRec107[1] != 0.0) ? (((fRec108[1] > 0.0) & (fRec108[1] < 1.0)) ? fRec107[1] : 0.0) : (((fRec108[1] == 0.0) & (iTemp163 != iRec109[1])) ? 0.00048828125 : (((fRec108[1] == 1.0) & (iTemp163 != iRec110[1])) ? -0.00048828125 : 0.0)));
			fRec107[0] = fTemp164;
			fRec108[0] = std::max<double>(0.0, std::min<double>(1.0, fRec108[1] + fTemp164));
			iRec109[0] = (((fRec108[1] >= 1.0) & (iRec110[1] != iTemp163)) ? iTemp163 : iRec109[1]);
			iRec110[0] = (((fRec108[1] <= 0.0) & (iRec109[1] != iTemp163)) ? iTemp163 : iRec110[1]);
			double fTemp165 = fRec17[0] * fRec112[0] * fTemp35 * (fTemp37 * (fTemp38 * (4.069010416666667e-06 * fTemp39 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec112[0] * fTemp43) + 1.0) + 8.138020833333335e-06 * fTemp44 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec112[0] * fTemp45) + 1.0)) + 8.138020833333335e-06 * fTemp46 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec112[0] * fTemp47) + 1.0)) + 4.069010416666667e-06 * fTemp48 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec112[0] * fTemp49) + 1.0));
			fVec33[IOTA0 & 262143] = fTemp165;
			double fTemp166 = fVec33[(IOTA0 - int(std::min<double>(fConst0, double(std::max<int>(0, iRec109[0]))))) & 262143];
			double fTemp167 = fRec108[0] * (fTemp166 - fVec33[(IOTA0 - int(std::min<double>(fConst0, double(std::max<int>(0, iRec110[0]))))) & 262143]) - fTemp166;
			fVec34[IOTA0 & 131071] = fTemp167;
			fRec113[0] = std::fmod(fRec113[1] + (10001.0 - std::pow(2.0, 0.08333333333333333 * fTemp67 * fRec112[0])), 1e+04);
			int iTemp168 = int(fRec113[0]);
			double fTemp169 = std::floor(fRec113[0]);
			double fTemp170 = std::min<double>(0.0001 * fRec113[0], 1.0);
			double fTemp171 = fRec113[0] + 1e+04;
			int iTemp172 = int(fTemp171);
			double fTemp173 = std::floor(fTemp171);
			double fTemp174 = (fVec34[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp168))) & 131071] * (fTemp169 + (1.0 - fRec113[0])) + (fRec113[0] - fTemp169) * fVec34[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp168 + 1))) & 131071]) * fTemp170 + (fVec34[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp172))) & 131071] * (fTemp173 + (-9999.0 - fRec113[0])) + (fRec113[0] + (1e+04 - fTemp173)) * fVec34[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp172 + 1))) & 131071]) * (1.0 - fTemp170);
			fVec35[0] = fTemp174;
			fRec106[0] = 0.995 * fRec106[1] + fTemp174 - fVec35[1];
			double fTemp175 = std::min<double>(std::max<double>(fRec98[0], -1.0), 1.0) + std::min<double>(std::max<double>(fRec106[0], -1.0), 1.0);
			fVec36[0] = fTemp175;
			fRec97[0] = 0.995 * fRec97[1] - 0.125 * (fVec36[1] - fTemp175);
			output4[i0] = FAUSTFLOAT(std::min<double>(std::max<double>(fRec97[0], -1.0), 1.0));
			fRec121[0] = fTemp3 * (0.0967741935483871 * fTemp7 + 0.009365244536940686 * fTemp9 + 0.15142777750457667 * fTemp10 + 0.13326653086346407 * fTemp11 + 0.3110855084191276 * fTemp12 + 0.01153167567188862 * fTemp13 + 0.02293037179997559 * fTemp14 + 0.1841831425598336 * fTemp15 + 0.018730489073881373 * fTemp16 + 0.0009063139874458729 * fTemp17 + 0.2631331610217852 * fTemp18 + 0.0036252559497834914 * fTemp19 + 8.770780523669737e-05 * fTemp20 + 0.3344428551164511 * fTemp21 + 0.0007016624418935789 * fTemp22 + 8.48785211968039e-06 * fTemp23 + 0.39885161107292366 * fTemp24 + 0.00013580563391488623 * fTemp25 + 0.0019099385475301433 * fTemp26 + 0.48869428219936584 * fTemp27 + 0.004693637384418525 * fTemp28 + 0.4291656353435508 * fTemp29) + fTemp2 * fRec121[1];
			fRec120[0] = fConst0 * fTemp30 * fRec121[0] * fTemp3 + fTemp2 * fRec120[1];
			int iTemp176 = int(fRec120[0]);
			double fTemp177 = ((fRec116[1] != 0.0) ? (((fRec117[1] > 0.0) & (fRec117[1] < 1.0)) ? fRec116[1] : 0.0) : (((fRec117[1] == 0.0) & (iTemp176 != iRec118[1])) ? 0.00048828125 : (((fRec117[1] == 1.0) & (iTemp176 != iRec119[1])) ? -0.00048828125 : 0.0)));
			fRec116[0] = fTemp177;
			fRec117[0] = std::max<double>(0.0, std::min<double>(1.0, fRec117[1] + fTemp177));
			iRec118[0] = (((fRec117[1] >= 1.0) & (iRec119[1] != iTemp176)) ? iTemp176 : iRec118[1]);
			iRec119[0] = (((fRec117[1] <= 0.0) & (iRec118[1] != iTemp176)) ? iTemp176 : iRec119[1]);
			double fTemp178 = fRec17[0] * fRec121[0] * fTemp35 * (fTemp37 * (fTemp38 * (8.138020833333335e-06 * fTemp44 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec121[0] * fTemp45) + 1.0) + 4.069010416666667e-06 * fTemp39 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec121[0] * fTemp43) + 1.0)) + 8.138020833333335e-06 * fTemp46 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec121[0] * fTemp47) + 1.0)) + 4.069010416666667e-06 * fTemp48 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec121[0] * fTemp49) + 1.0));
			fVec37[IOTA0 & 262143] = fTemp178;
			double fTemp179 = fVec37[(IOTA0 - int(std::min<double>(fConst0, double(std::max<int>(0, iRec118[0]))))) & 262143];
			double fTemp180 = fRec117[0] * (fTemp179 - fVec37[(IOTA0 - int(std::min<double>(fConst0, double(std::max<int>(0, iRec119[0]))))) & 262143]) - fTemp179;
			fVec38[IOTA0 & 131071] = fTemp180;
			fRec122[0] = std::fmod(fRec122[1] + (10001.0 - std::pow(2.0, 0.08333333333333333 * fTemp67 * fRec121[0])), 1e+04);
			int iTemp181 = int(fRec122[0]);
			double fTemp182 = std::floor(fRec122[0]);
			double fTemp183 = std::min<double>(0.0001 * fRec122[0], 1.0);
			double fTemp184 = fRec122[0] + 1e+04;
			int iTemp185 = int(fTemp184);
			double fTemp186 = std::floor(fTemp184);
			double fTemp187 = (fVec38[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp181))) & 131071] * (fTemp182 + (1.0 - fRec122[0])) + (fRec122[0] - fTemp182) * fVec38[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp181 + 1))) & 131071]) * fTemp183 + (fVec38[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp185))) & 131071] * (fTemp186 + (-9999.0 - fRec122[0])) + (fRec122[0] + (1e+04 - fTemp186)) * fVec38[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp185 + 1))) & 131071]) * (1.0 - fTemp183);
			fVec39[0] = fTemp187;
			fRec115[0] = 0.995 * fRec115[1] + fTemp187 - fVec39[1];
			fRec129[0] = fTemp3 * (0.5967741935483871 * fTemp7 + 0.35613943808532783 * fTemp9 + 0.806028263454005 * fTemp10 + 0.6751603096927343 * fTemp11 + 0.772511613859874 * fTemp12 + 0.40812295321298275 * fTemp13 + 0.6496815614866789 * fTemp14 + 0.8374089490114465 * fTemp15 + 0.6748178980228929 * fTemp16 + 0.21253482595414724 * fTemp17 + 0.9344390923433252 * fTemp18 + 0.7377563693733007 * fTemp19 + 0.12683529935973303 * fTemp20 + 0.9735641501384376 * fTemp21 + 0.7885132011075006 * fTemp22 + 0.07569203348887295 * fTemp23 + 0.9893403831203378 * fTemp24 + 0.8294461299254037 * fTemp25 + 0.061118033520964545 * fTemp26 + 0.9840216963187302 * fTemp27 + 0.19759077652691936 * fTemp28 + 0.9151005130647925 * fTemp29) + fTemp2 * fRec129[1];
			fRec128[0] = fConst0 * fTemp30 * fRec129[0] * fTemp3 + fTemp2 * fRec128[1];
			int iTemp188 = int(fRec128[0]);
			double fTemp189 = ((fRec124[1] != 0.0) ? (((fRec125[1] > 0.0) & (fRec125[1] < 1.0)) ? fRec124[1] : 0.0) : (((fRec125[1] == 0.0) & (iTemp188 != iRec126[1])) ? 0.00048828125 : (((fRec125[1] == 1.0) & (iTemp188 != iRec127[1])) ? -0.00048828125 : 0.0)));
			fRec124[0] = fTemp189;
			fRec125[0] = std::max<double>(0.0, std::min<double>(1.0, fRec125[1] + fTemp189));
			iRec126[0] = (((fRec125[1] >= 1.0) & (iRec127[1] != iTemp188)) ? iTemp188 : iRec126[1]);
			iRec127[0] = (((fRec125[1] <= 0.0) & (iRec126[1] != iTemp188)) ? iTemp188 : iRec127[1]);
			double fTemp190 = fRec17[0] * fRec129[0] * fTemp35 * (fTemp37 * (fTemp38 * (4.069010416666667e-06 * fTemp39 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec129[0] * fTemp43) + 1.0) + 8.138020833333335e-06 * fTemp44 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec129[0] * fTemp45) + 1.0)) + 8.138020833333335e-06 * fTemp46 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec129[0] * fTemp47) + 1.0)) + 4.069010416666667e-06 * fTemp48 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec129[0] * fTemp49) + 1.0));
			fVec40[IOTA0 & 262143] = fTemp190;
			double fTemp191 = fVec40[(IOTA0 - int(std::min<double>(fConst0, double(std::max<int>(0, iRec126[0]))))) & 262143];
			double fTemp192 = fRec125[0] * (fTemp191 - fVec40[(IOTA0 - int(std::min<double>(fConst0, double(std::max<int>(0, iRec127[0]))))) & 262143]) - fTemp191;
			fVec41[IOTA0 & 131071] = fTemp192;
			fRec130[0] = std::fmod(fRec130[1] + (10001.0 - std::pow(2.0, 0.08333333333333333 * fTemp67 * fRec129[0])), 1e+04);
			double fTemp193 = fRec130[0] + 1e+04;
			int iTemp194 = int(fTemp193);
			double fTemp195 = std::floor(fTemp193);
			double fTemp196 = std::min<double>(0.0001 * fRec130[0], 1.0);
			double fTemp197 = (fVec41[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp194))) & 131071] * (fTemp195 + (-9999.0 - fRec130[0])) + (fRec130[0] + (1e+04 - fTemp195)) * fVec41[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp194 + 1))) & 131071]) * (1.0 - fTemp196);
			int iTemp198 = int(fRec130[0]);
			double fTemp199 = std::floor(fRec130[0]);
			double fTemp200 = (fVec41[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp198))) & 131071] * (fTemp199 + (1.0 - fRec130[0])) + (fRec130[0] - fTemp199) * fVec41[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp198 + 1))) & 131071]) * fTemp196;
			fVec42[0] = fTemp200 + fTemp197;
			fRec123[0] = fTemp197 + 0.995 * fRec123[1] + fTemp200 - fVec42[1];
			double fTemp201 = std::min<double>(std::max<double>(fRec115[0], -1.0), 1.0) + std::min<double>(std::max<double>(fRec123[0], -1.0), 1.0);
			fVec43[0] = fTemp201;
			fRec114[0] = 0.995 * fRec114[1] - 0.125 * (fVec43[1] - fTemp201);
			output5[i0] = FAUSTFLOAT(std::min<double>(std::max<double>(fRec114[0], -1.0), 1.0));
			fRec138[0] = fTemp3 * (0.11290322580645161 * fTemp7 + 0.0127471383975026 * fTemp9 + 0.17641976625780845 * fTemp10 + 0.15432814639129372 * fTemp11 + 0.3360107525161235 * fTemp12 + 0.015684976202465895 * fTemp13 + 0.03112393392645979 * fTemp14 + 0.21305931321540061 * fTemp15 + 0.0254942767950052 * fTemp16 + 0.0014391930448793258 * fTemp17 + 0.30190745527172635 * fTemp18 + 0.005756772179517303 * fTemp19 + 0.00016248953732508518 * fTemp20 + 0.3807243554829831 * fTemp21 + 0.0012999162986006814 * fTemp22 + 1.8345592923799937e-05 * fTemp23 + 0.4506425734123237 * fTemp24 + 0.000293529486780799 * fTemp25 + 0.0021358591967941616 * fTemp26 + 0.5427776786663743 * fTemp27 + 0.006394010886358714 * fTemp28 + 0.4615834845565866 * fTemp29) + fTemp2 * fRec138[1];
			fRec137[0] = fConst0 * fTemp30 * fRec138[0] * fTemp3 + fTemp2 * fRec137[1];
			int iTemp202 = int(fRec137[0]);
			double fTemp203 = ((fRec133[1] != 0.0) ? (((fRec134[1] > 0.0) & (fRec134[1] < 1.0)) ? fRec133[1] : 0.0) : (((fRec134[1] == 0.0) & (iTemp202 != iRec135[1])) ? 0.00048828125 : (((fRec134[1] == 1.0) & (iTemp202 != iRec136[1])) ? -0.00048828125 : 0.0)));
			fRec133[0] = fTemp203;
			fRec134[0] = std::max<double>(0.0, std::min<double>(1.0, fRec134[1] + fTemp203));
			iRec135[0] = (((fRec134[1] >= 1.0) & (iRec136[1] != iTemp202)) ? iTemp202 : iRec135[1]);
			iRec136[0] = (((fRec134[1] <= 0.0) & (iRec135[1] != iTemp202)) ? iTemp202 : iRec136[1]);
			double fTemp204 = fRec17[0] * fRec138[0] * fTemp35 * (fTemp37 * (fTemp38 * (4.069010416666667e-06 * fTemp39 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec138[0] * fTemp43) + 1.0) + 8.138020833333335e-06 * fTemp44 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec138[0] * fTemp45) + 1.0)) + 8.138020833333335e-06 * fTemp46 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec138[0] * fTemp47) + 1.0)) + 4.069010416666667e-06 * fTemp48 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec138[0] * fTemp49) + 1.0));
			fVec44[IOTA0 & 262143] = fTemp204;
			double fTemp205 = fVec44[(IOTA0 - int(std::min<double>(fConst0, double(std::max<int>(0, iRec135[0]))))) & 262143];
			double fTemp206 = fRec134[0] * (fTemp205 - fVec44[(IOTA0 - int(std::min<double>(fConst0, double(std::max<int>(0, iRec136[0]))))) & 262143]) - fTemp205;
			fVec45[IOTA0 & 131071] = fTemp206;
			fRec139[0] = std::fmod(fRec139[1] + (10001.0 - std::pow(2.0, 0.08333333333333333 * fTemp67 * fRec138[0])), 1e+04);
			int iTemp207 = int(fRec139[0]);
			double fTemp208 = std::floor(fRec139[0]);
			double fTemp209 = std::min<double>(0.0001 * fRec139[0], 1.0);
			double fTemp210 = fRec139[0] + 1e+04;
			int iTemp211 = int(fTemp210);
			double fTemp212 = std::floor(fTemp210);
			double fTemp213 = (fVec45[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp207))) & 131071] * (fTemp208 + (1.0 - fRec139[0])) + (fRec139[0] - fTemp208) * fVec45[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp207 + 1))) & 131071]) * fTemp209 + (fVec45[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp211))) & 131071] * (fTemp212 + (-9999.0 - fRec139[0])) + (fRec139[0] + (1e+04 - fTemp212)) * fVec45[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp211 + 1))) & 131071]) * (1.0 - fTemp209);
			fVec46[0] = fTemp213;
			fRec132[0] = 0.995 * fRec132[1] + fTemp213 - fVec46[1];
			fRec146[0] = fTemp3 * (0.6129032258064516 * fTemp7 + 0.3756503642039542 * fTemp9 + 0.8207634412072763 * fTemp10 + 0.6896598793878495 * fTemp11 + 0.7828813612588127 * fTemp12 + 0.4287317849052077 * fTemp13 + 0.6736526264224101 * fTemp14 + 0.850156087408949 * fTemp15 + 0.700312174817898 * fTemp16 + 0.23023731999597194 * fTemp17 + 0.9419959048034642 * fTemp18 + 0.7679836192138565 * fTemp19 + 0.14111319612656345 * fTemp20 + 0.9775468018594055 * fTemp21 + 0.8203744148752438 * fTemp22 + 0.08648873310982921 * fTemp23 + 0.9913084394294472 * fTemp24 + 0.8609350308711565 * fTemp25 + 0.06834749429741317 * fTemp26 + 0.9857118024583242 * fTemp27 + 0.20984201845703931 * fTemp28 + 0.9220390921262227 * fTemp29) + fTemp2 * fRec146[1];
			fRec145[0] = fConst0 * fTemp30 * fRec146[0] * fTemp3 + fTemp2 * fRec145[1];
			int iTemp214 = int(fRec145[0]);
			double fTemp215 = ((fRec141[1] != 0.0) ? (((fRec142[1] > 0.0) & (fRec142[1] < 1.0)) ? fRec141[1] : 0.0) : (((fRec142[1] == 0.0) & (iTemp214 != iRec143[1])) ? 0.00048828125 : (((fRec142[1] == 1.0) & (iTemp214 != iRec144[1])) ? -0.00048828125 : 0.0)));
			fRec141[0] = fTemp215;
			fRec142[0] = std::max<double>(0.0, std::min<double>(1.0, fRec142[1] + fTemp215));
			iRec143[0] = (((fRec142[1] >= 1.0) & (iRec144[1] != iTemp214)) ? iTemp214 : iRec143[1]);
			iRec144[0] = (((fRec142[1] <= 0.0) & (iRec143[1] != iTemp214)) ? iTemp214 : iRec144[1]);
			double fTemp216 = fRec17[0] * fRec146[0] * fTemp35 * (fTemp37 * (fTemp38 * (4.069010416666667e-06 * fTemp39 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec146[0] * fTemp43) + 1.0) + 8.138020833333335e-06 * fTemp44 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec146[0] * fTemp45) + 1.0)) + 8.138020833333335e-06 * fTemp46 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec146[0] * fTemp47) + 1.0)) + 4.069010416666667e-06 * fTemp48 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec146[0] * fTemp49) + 1.0));
			fVec47[IOTA0 & 262143] = fTemp216;
			double fTemp217 = fVec47[(IOTA0 - int(std::min<double>(fConst0, double(std::max<int>(0, iRec143[0]))))) & 262143];
			double fTemp218 = fRec142[0] * (fTemp217 - fVec47[(IOTA0 - int(std::min<double>(fConst0, double(std::max<int>(0, iRec144[0]))))) & 262143]) - fTemp217;
			fVec48[IOTA0 & 131071] = fTemp218;
			fRec147[0] = std::fmod(fRec147[1] + (10001.0 - std::pow(2.0, 0.08333333333333333 * fTemp67 * fRec146[0])), 1e+04);
			int iTemp219 = int(fRec147[0]);
			double fTemp220 = std::floor(fRec147[0]);
			double fTemp221 = std::min<double>(0.0001 * fRec147[0], 1.0);
			double fTemp222 = fRec147[0] + 1e+04;
			int iTemp223 = int(fTemp222);
			double fTemp224 = std::floor(fTemp222);
			double fTemp225 = (fVec48[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp219))) & 131071] * (fTemp220 + (1.0 - fRec147[0])) + (fRec147[0] - fTemp220) * fVec48[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp219 + 1))) & 131071]) * fTemp221 + (fVec48[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp223))) & 131071] * (fTemp224 + (-9999.0 - fRec147[0])) + (fRec147[0] + (1e+04 - fTemp224)) * fVec48[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp223 + 1))) & 131071]) * (1.0 - fTemp221);
			fVec49[0] = fTemp225;
			fRec140[0] = 0.995 * fRec140[1] + fTemp225 - fVec49[1];
			double fTemp226 = std::min<double>(std::max<double>(fRec132[0], -1.0), 1.0) + std::min<double>(std::max<double>(fRec140[0], -1.0), 1.0);
			fVec50[0] = fTemp226;
			fRec131[0] = 0.995 * fRec131[1] - 0.125 * (fVec50[1] - fTemp226);
			output6[i0] = FAUSTFLOAT(std::min<double>(std::max<double>(fRec131[0], -1.0), 1.0));
			fRec155[0] = fTemp3 * (0.12903225806451613 * fTemp7 + 0.016649323621227886 * fTemp9 + 0.20129852008866006 * fTemp10 + 0.1750867065580911 * fTemp11 + 0.3592106040535498 * fTemp12 + 0.020470058747505515 * fTemp13 + 0.0405210941898847 * fTemp14 + 0.2414151925078044 * fTemp15 + 0.03329864724245577 * fTemp16 + 0.002148299822093921 * fTemp17 + 0.3392971031519586 * fTemp18 + 0.008593199288375684 * fTemp19 + 0.0002771999770443769 * fTemp20 + 0.42454908984202844 * fTemp21 + 0.002217599816355015 * fTemp22 + 3.576773897346798e-05 * fTemp23 + 0.4988008201849925 * fTemp24 + 0.0005722838235754877 * fTemp25 + 0.0023885032921239623 * fTemp26 + 0.5911404002581055 * fTemp27 + 0.008359603294232354 * fTemp28 + 0.4913402003783167 * fTemp29) + fTemp2 * fRec155[1];
			fRec154[0] = fConst0 * fTemp30 * fRec155[0] * fTemp3 + fTemp2 * fRec154[1];
			int iTemp227 = int(fRec154[0]);
			double fTemp228 = ((fRec150[1] != 0.0) ? (((fRec151[1] > 0.0) & (fRec151[1] < 1.0)) ? fRec150[1] : 0.0) : (((fRec151[1] == 0.0) & (iTemp227 != iRec152[1])) ? 0.00048828125 : (((fRec151[1] == 1.0) & (iTemp227 != iRec153[1])) ? -0.00048828125 : 0.0)));
			fRec150[0] = fTemp228;
			fRec151[0] = std::max<double>(0.0, std::min<double>(1.0, fRec151[1] + fTemp228));
			iRec152[0] = (((fRec151[1] >= 1.0) & (iRec153[1] != iTemp227)) ? iTemp227 : iRec152[1]);
			iRec153[0] = (((fRec151[1] <= 0.0) & (iRec152[1] != iTemp227)) ? iTemp227 : iRec153[1]);
			double fTemp229 = fRec17[0] * fRec155[0] * fTemp35 * (fTemp37 * (fTemp38 * (8.138020833333335e-06 * fTemp44 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec155[0] * fTemp45) + 1.0) + 4.069010416666667e-06 * fTemp39 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec155[0] * fTemp43) + 1.0)) + 8.138020833333335e-06 * fTemp46 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec155[0] * fTemp47) + 1.0)) + 4.069010416666667e-06 * fTemp48 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec155[0] * fTemp49) + 1.0));
			fVec51[IOTA0 & 262143] = fTemp229;
			double fTemp230 = fVec51[(IOTA0 - int(std::min<double>(fConst0, double(std::max<int>(0, iRec152[0]))))) & 262143];
			double fTemp231 = fRec151[0] * (fTemp230 - fVec51[(IOTA0 - int(std::min<double>(fConst0, double(std::max<int>(0, iRec153[0]))))) & 262143]) - fTemp230;
			fVec52[IOTA0 & 131071] = fTemp231;
			fRec156[0] = std::fmod(fRec156[1] + (10001.0 - std::pow(2.0, 0.08333333333333333 * fTemp67 * fRec155[0])), 1e+04);
			int iTemp232 = int(fRec156[0]);
			double fTemp233 = std::floor(fRec156[0]);
			double fTemp234 = std::min<double>(0.0001 * fRec156[0], 1.0);
			double fTemp235 = fRec156[0] + 1e+04;
			int iTemp236 = int(fTemp235);
			double fTemp237 = std::floor(fTemp235);
			double fTemp238 = (fVec52[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp232))) & 131071] * (fTemp233 + (1.0 - fRec156[0])) + (fRec156[0] - fTemp233) * fVec52[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp232 + 1))) & 131071]) * fTemp234 + (fVec52[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp236))) & 131071] * (fTemp237 + (-9999.0 - fRec156[0])) + (fRec156[0] + (1e+04 - fTemp237)) * fVec52[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp236 + 1))) & 131071]) * (1.0 - fTemp234);
			fVec53[0] = fTemp238;
			fRec149[0] = 0.995 * fRec149[1] + fTemp238 - fVec53[1];
			fRec163[0] = fTemp3 * (0.6290322580645161 * fTemp7 + 0.395681581685744 * fTemp9 + 0.8349718124324074 * fTemp10 + 0.7040151723649194 * fTemp11 + 0.7931155389125321 * fTemp12 + 0.4497072847626086 * fTemp13 + 0.6971779275566592 * fTemp14 + 0.8623829344432883 * fTemp15 + 0.7247658688865765 * fTemp16 + 0.24889647880232285 * fTemp17 + 0.9489485079386392 * fTemp18 + 0.7957940317545568 * fTemp19 + 0.1565639140853321 * fTemp20 + 0.9810615432675597 * fTemp21 + 0.8484923461404776 * fTemp22 + 0.09848375240851535 * fTemp23 + 0.9929744434702238 * fTemp24 + 0.8875910955235802 * fTemp25 + 0.07643210534796675 * fTemp26 + 0.9872231375080658 * fTemp27 + 0.2226208014654264 * fTemp28 + 0.9286457529345021 * fTemp29) + fTemp2 * fRec163[1];
			fRec162[0] = fConst0 * fTemp30 * fRec163[0] * fTemp3 + fTemp2 * fRec162[1];
			int iTemp239 = int(fRec162[0]);
			double fTemp240 = ((fRec158[1] != 0.0) ? (((fRec159[1] > 0.0) & (fRec159[1] < 1.0)) ? fRec158[1] : 0.0) : (((fRec159[1] == 0.0) & (iTemp239 != iRec160[1])) ? 0.00048828125 : (((fRec159[1] == 1.0) & (iTemp239 != iRec161[1])) ? -0.00048828125 : 0.0)));
			fRec158[0] = fTemp240;
			fRec159[0] = std::max<double>(0.0, std::min<double>(1.0, fRec159[1] + fTemp240));
			iRec160[0] = (((fRec159[1] >= 1.0) & (iRec161[1] != iTemp239)) ? iTemp239 : iRec160[1]);
			iRec161[0] = (((fRec159[1] <= 0.0) & (iRec160[1] != iTemp239)) ? iTemp239 : iRec161[1]);
			double fTemp241 = fRec17[0] * fRec163[0] * fTemp35 * (fTemp37 * (fTemp38 * (4.069010416666667e-06 * fTemp39 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec163[0] * fTemp43) + 1.0) + 8.138020833333335e-06 * fTemp44 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec163[0] * fTemp45) + 1.0)) + 8.138020833333335e-06 * fTemp46 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec163[0] * fTemp47) + 1.0)) + 4.069010416666667e-06 * fTemp48 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec163[0] * fTemp49) + 1.0));
			fVec54[IOTA0 & 262143] = fTemp241;
			double fTemp242 = fVec54[(IOTA0 - int(std::min<double>(fConst0, double(std::max<int>(0, iRec160[0]))))) & 262143];
			double fTemp243 = fRec159[0] * (fTemp242 - fVec54[(IOTA0 - int(std::min<double>(fConst0, double(std::max<int>(0, iRec161[0]))))) & 262143]) - fTemp242;
			fVec55[IOTA0 & 131071] = fTemp243;
			fRec164[0] = std::fmod(fRec164[1] + (10001.0 - std::pow(2.0, 0.08333333333333333 * fTemp67 * fRec163[0])), 1e+04);
			int iTemp244 = int(fRec164[0]);
			double fTemp245 = std::floor(fRec164[0]);
			double fTemp246 = std::min<double>(0.0001 * fRec164[0], 1.0);
			double fTemp247 = fRec164[0] + 1e+04;
			int iTemp248 = int(fTemp247);
			double fTemp249 = std::floor(fTemp247);
			double fTemp250 = (fVec55[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp244))) & 131071] * (fTemp245 + (1.0 - fRec164[0])) + (fRec164[0] - fTemp245) * fVec55[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp244 + 1))) & 131071]) * fTemp246 + (fVec55[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp248))) & 131071] * (fTemp249 + (-9999.0 - fRec164[0])) + (fRec164[0] + (1e+04 - fTemp249)) * fVec55[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp248 + 1))) & 131071]) * (1.0 - fTemp246);
			fVec56[0] = fTemp250;
			fRec157[0] = 0.995 * fRec157[1] + fTemp250 - fVec56[1];
			double fTemp251 = std::min<double>(std::max<double>(fRec149[0], -1.0), 1.0) + std::min<double>(std::max<double>(fRec157[0], -1.0), 1.0);
			fVec57[0] = fTemp251;
			fRec148[0] = 0.995 * fRec148[1] - 0.125 * (fVec57[1] - fTemp251);
			output7[i0] = FAUSTFLOAT(std::min<double>(std::max<double>(fRec148[0], -1.0), 1.0));
			fRec172[0] = fTemp3 * (0.14516129032258066 * fTemp7 + 0.021071800208116546 * fTemp9 + 0.22604807058373483 * fTemp10 + 0.19555080911780698 * fTemp11 + 0.3810003810005715 * fTemp12 + 0.02588385200461296 * fTemp13 + 0.051097730214629156 * fTemp14 + 0.26925078043704476 * fTemp15 + 0.04214360041623309 * fTemp16 + 0.0030588097076298215 * fTemp17 + 0.3753272800510221 * fTemp18 + 0.012235238830519286 * fTemp19 + 0.0004440207640107806 * fTemp20 + 0.4660055781081318 * fTemp21 + 0.0035521661120862446 * fTemp22 + 6.445462703382299e-05 * fTemp23 + 0.5435208974150159 * fTemp24 + 0.0010312740325411679 * fTemp25 + 0.002671031866262484 * fTemp26 + 0.6343875517417612 * fTemp27 + 0.010591995286128975 * fTemp28 + 0.5188938045853358 * fTemp29) + fTemp2 * fRec172[1];
			fRec171[0] = fConst0 * fTemp30 * fRec172[0] * fTemp3 + fTemp2 * fRec171[1];
			int iTemp252 = int(fRec171[0]);
			double fTemp253 = ((fRec167[1] != 0.0) ? (((fRec168[1] > 0.0) & (fRec168[1] < 1.0)) ? fRec167[1] : 0.0) : (((fRec168[1] == 0.0) & (iTemp252 != iRec169[1])) ? 0.00048828125 : (((fRec168[1] == 1.0) & (iTemp252 != iRec170[1])) ? -0.00048828125 : 0.0)));
			fRec167[0] = fTemp253;
			fRec168[0] = std::max<double>(0.0, std::min<double>(1.0, fRec168[1] + fTemp253));
			iRec169[0] = (((fRec168[1] >= 1.0) & (iRec170[1] != iTemp252)) ? iTemp252 : iRec169[1]);
			iRec170[0] = (((fRec168[1] <= 0.0) & (iRec169[1] != iTemp252)) ? iTemp252 : iRec170[1]);
			double fTemp254 = fRec17[0] * fRec172[0] * fTemp35 * (fTemp37 * (fTemp38 * (4.069010416666667e-06 * fTemp39 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec172[0] * fTemp43) + 1.0) + 8.138020833333335e-06 * fTemp44 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec172[0] * fTemp45) + 1.0)) + 8.138020833333335e-06 * fTemp46 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec172[0] * fTemp47) + 1.0)) + 4.069010416666667e-06 * fTemp48 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec172[0] * fTemp49) + 1.0));
			fVec58[IOTA0 & 262143] = fTemp254;
			double fTemp255 = fVec58[(IOTA0 - int(std::min<double>(fConst0, double(std::max<int>(0, iRec169[0]))))) & 262143];
			double fTemp256 = fRec168[0] * (fTemp255 - fVec58[(IOTA0 - int(std::min<double>(fConst0, double(std::max<int>(0, iRec170[0]))))) & 262143]) - fTemp255;
			fVec59[IOTA0 & 131071] = fTemp256;
			fRec173[0] = std::fmod(fRec173[1] + (10001.0 - std::pow(2.0, 0.08333333333333333 * fTemp67 * fRec172[0])), 1e+04);
			int iTemp257 = int(fRec173[0]);
			double fTemp258 = std::floor(fRec173[0]);
			double fTemp259 = std::min<double>(0.0001 * fRec173[0], 1.0);
			double fTemp260 = fRec173[0] + 1e+04;
			int iTemp261 = int(fTemp260);
			double fTemp262 = std::floor(fTemp260);
			double fTemp263 = (fVec59[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp257))) & 131071] * (fTemp258 + (1.0 - fRec173[0])) + (fRec173[0] - fTemp258) * fVec59[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp257 + 1))) & 131071]) * fTemp259 + (fVec59[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp261))) & 131071] * (fTemp262 + (-9999.0 - fRec173[0])) + (fRec173[0] + (1e+04 - fTemp262)) * fVec59[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp261 + 1))) & 131071]) * (1.0 - fTemp259);
			fVec60[0] = fTemp263;
			fRec166[0] = 0.995 * fRec166[1] + fTemp263 - fVec60[1];
			fRec180[0] = fTemp3 * (0.6451612903225806 * fTemp7 + 0.4162330905306972 * fTemp9 + 0.8486442574947509 * fTemp10 + 0.7182290315846203 * fTemp11 + 0.8032193289024988 * fTemp12 + 0.4710359896730376 * fTemp13 + 0.7201970757788172 * fTemp14 + 0.874089490114464 * fTemp15 + 0.7481789802289281 * fTemp16 + 0.2685374777617401 * fTemp17 + 0.9553220771373905 * fTemp18 + 0.8212883085495619 * fTemp19 + 0.17324998565273556 * fTemp20 + 0.9841465435003643 * fTemp21 + 0.8731723480029149 * fTemp22 + 0.11177418429208745 * fTemp23 + 0.9943745799517422 * fTemp24 + 0.9099932792278751 * fTemp25 + 0.08547301972039949 * fTemp26 + 0.98857461099193 * fTemp27 + 0.2359535946885799 * fTemp28 + 0.9349275320122218 * fTemp29) + fTemp2 * fRec180[1];
			fRec179[0] = fConst0 * fTemp30 * fRec180[0] * fTemp3 + fTemp2 * fRec179[1];
			int iTemp264 = int(fRec179[0]);
			double fTemp265 = ((fRec175[1] != 0.0) ? (((fRec176[1] > 0.0) & (fRec176[1] < 1.0)) ? fRec175[1] : 0.0) : (((fRec176[1] == 0.0) & (iTemp264 != iRec177[1])) ? 0.00048828125 : (((fRec176[1] == 1.0) & (iTemp264 != iRec178[1])) ? -0.00048828125 : 0.0)));
			fRec175[0] = fTemp265;
			fRec176[0] = std::max<double>(0.0, std::min<double>(1.0, fRec176[1] + fTemp265));
			iRec177[0] = (((fRec176[1] >= 1.0) & (iRec178[1] != iTemp264)) ? iTemp264 : iRec177[1]);
			iRec178[0] = (((fRec176[1] <= 0.0) & (iRec177[1] != iTemp264)) ? iTemp264 : iRec178[1]);
			double fTemp266 = fRec17[0] * fRec180[0] * fTemp35 * (fTemp37 * (fTemp38 * (4.069010416666667e-06 * fTemp39 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec180[0] * fTemp43) + 1.0) + 8.138020833333335e-06 * fTemp44 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec180[0] * fTemp45) + 1.0)) + 8.138020833333335e-06 * fTemp46 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec180[0] * fTemp47) + 1.0)) + 4.069010416666667e-06 * fTemp48 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec180[0] * fTemp49) + 1.0));
			fVec61[IOTA0 & 262143] = fTemp266;
			double fTemp267 = fVec61[(IOTA0 - int(std::min<double>(fConst0, double(std::max<int>(0, iRec177[0]))))) & 262143];
			double fTemp268 = fRec176[0] * (fTemp267 - fVec61[(IOTA0 - int(std::min<double>(fConst0, double(std::max<int>(0, iRec178[0]))))) & 262143]) - fTemp267;
			fVec62[IOTA0 & 131071] = fTemp268;
			fRec181[0] = std::fmod(fRec181[1] + (10001.0 - std::pow(2.0, 0.08333333333333333 * fTemp67 * fRec180[0])), 1e+04);
			int iTemp269 = int(fRec181[0]);
			double fTemp270 = std::floor(fRec181[0]);
			double fTemp271 = std::min<double>(0.0001 * fRec181[0], 1.0);
			double fTemp272 = fRec181[0] + 1e+04;
			int iTemp273 = int(fTemp272);
			double fTemp274 = std::floor(fTemp272);
			double fTemp275 = (fVec62[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp269))) & 131071] * (fTemp270 + (1.0 - fRec181[0])) + (fRec181[0] - fTemp270) * fVec62[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp269 + 1))) & 131071]) * fTemp271 + (fVec62[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp273))) & 131071] * (fTemp274 + (-9999.0 - fRec181[0])) + (fRec181[0] + (1e+04 - fTemp274)) * fVec62[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp273 + 1))) & 131071]) * (1.0 - fTemp271);
			fVec63[0] = fTemp275;
			fRec174[0] = 0.995 * fRec174[1] + fTemp275 - fVec63[1];
			double fTemp276 = std::min<double>(std::max<double>(fRec166[0], -1.0), 1.0) + std::min<double>(std::max<double>(fRec174[0], -1.0), 1.0);
			fVec64[0] = fTemp276;
			fRec165[0] = 0.995 * fRec165[1] - 0.125 * (fVec64[1] - fTemp276);
			output8[i0] = FAUSTFLOAT(std::min<double>(std::max<double>(fRec165[0], -1.0), 1.0));
			fRec189[0] = fTemp3 * (0.16129032258064516 * fTemp7 + 0.026014568158168574 * fTemp9 + 0.25065253225872053 * fTemp10 + 0.21572869105543724 * fTemp11 + 0.4016096644512494 * fTemp12 + 0.03192288113379571 * fTemp13 + 0.06282669192770896 * fTemp14 + 0.29656607700312165 * fTemp15 + 0.05202913631633715 * fTemp16 + 0.004195898090027189 * fTemp17 + 0.41002316135745687 * fTemp18 + 0.016783592360108755 * fTemp19 + 0.0006767577564559983 * fTemp20 + 0.5051807159772219 * fTemp21 + 0.005414062051647986 * fTemp22 + 0.00010915447684774165 * fTemp23 + 0.5849902779163796 * fTemp24 + 0.0017464716295638664 * fTemp25 + 0.0029869798606161537 * fTemp26 + 0.673060232887357 * fTemp27 + 0.013092997369138382 * fTemp28 + 0.5445788069720687 * fTemp29) + fTemp2 * fRec189[1];
			fRec188[0] = fConst0 * fTemp30 * fRec189[0] * fTemp3 + fTemp2 * fRec188[1];
			int iTemp277 = int(fRec188[0]);
			double fTemp278 = ((fRec184[1] != 0.0) ? (((fRec185[1] > 0.0) & (fRec185[1] < 1.0)) ? fRec184[1] : 0.0) : (((fRec185[1] == 0.0) & (iTemp277 != iRec186[1])) ? 0.00048828125 : (((fRec185[1] == 1.0) & (iTemp277 != iRec187[1])) ? -0.00048828125 : 0.0)));
			fRec184[0] = fTemp278;
			fRec185[0] = std::max<double>(0.0, std::min<double>(1.0, fRec185[1] + fTemp278));
			iRec186[0] = (((fRec185[1] >= 1.0) & (iRec187[1] != iTemp277)) ? iTemp277 : iRec186[1]);
			iRec187[0] = (((fRec185[1] <= 0.0) & (iRec186[1] != iTemp277)) ? iTemp277 : iRec187[1]);
			double fTemp279 = fRec17[0] * fRec189[0] * fTemp35 * (fTemp37 * (fTemp38 * (8.138020833333335e-06 * fTemp44 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec189[0] * fTemp45) + 1.0) + 4.069010416666667e-06 * fTemp39 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec189[0] * fTemp43) + 1.0)) + 8.138020833333335e-06 * fTemp46 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec189[0] * fTemp47) + 1.0)) + 4.069010416666667e-06 * fTemp48 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec189[0] * fTemp49) + 1.0));
			fVec65[IOTA0 & 262143] = fTemp279;
			double fTemp280 = fVec65[(IOTA0 - int(std::min<double>(fConst0, double(std::max<int>(0, iRec186[0]))))) & 262143];
			double fTemp281 = fRec185[0] * (fTemp280 - fVec65[(IOTA0 - int(std::min<double>(fConst0, double(std::max<int>(0, iRec187[0]))))) & 262143]) - fTemp280;
			fVec66[IOTA0 & 131071] = fTemp281;
			fRec190[0] = std::fmod(fRec190[1] + (10001.0 - std::pow(2.0, 0.08333333333333333 * fTemp67 * fRec189[0])), 1e+04);
			int iTemp282 = int(fRec190[0]);
			double fTemp283 = std::floor(fRec190[0]);
			double fTemp284 = std::min<double>(0.0001 * fRec190[0], 1.0);
			double fTemp285 = fRec190[0] + 1e+04;
			int iTemp286 = int(fTemp285);
			double fTemp287 = std::floor(fTemp285);
			double fTemp288 = (fVec66[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp282))) & 131071] * (fTemp283 + (1.0 - fRec190[0])) + (fRec190[0] - fTemp283) * fVec66[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp282 + 1))) & 131071]) * fTemp284 + (fVec66[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp286))) & 131071] * (fTemp287 + (-9999.0 - fRec190[0])) + (fRec190[0] + (1e+04 - fTemp287)) * fVec66[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp286 + 1))) & 131071]) * (1.0 - fTemp284);
			fVec67[0] = fTemp288;
			fRec183[0] = 0.995 * fRec183[1] + fTemp288 - fVec67[1];
			fRec197[0] = fTemp3 * (0.6612903225806451 * fTemp7 + 0.4373048907388137 * fTemp9 + 0.8617720007435496 * fTemp10 + 0.732304216796343 * fTemp11 + 0.8131975913519697 * fTemp12 + 0.4927042098198926 * fTemp13 + 0.7426509812655404 * fTemp14 + 0.8852757544224765 * fTemp15 + 0.7705515088449532 * fTemp16 + 0.28918549226276385 * fTemp17 + 0.9611417877882582 * fTemp18 + 0.8445671511530327 * fTemp19 + 0.1912355674640858 * fTemp20 + 0.9868383474766681 * fTemp21 + 0.8947067798133448 * fTemp22 + 0.12646223009721802 * fTemp23 + 0.9955420209195166 * fTemp24 + 0.9286723347122658 * fTemp25 + 0.09558335553971692 * fTemp26 + 0.98978313227773 * fTemp27 + 0.24986993843654937 * fTemp28 + 0.9408909365184025 * fTemp29) + fTemp2 * fRec197[1];
			fRec196[0] = fConst0 * fTemp30 * fRec197[0] * fTemp3 + fTemp2 * fRec196[1];
			int iTemp289 = int(fRec196[0]);
			double fTemp290 = ((fRec192[1] != 0.0) ? (((fRec193[1] > 0.0) & (fRec193[1] < 1.0)) ? fRec192[1] : 0.0) : (((fRec193[1] == 0.0) & (iTemp289 != iRec194[1])) ? 0.00048828125 : (((fRec193[1] == 1.0) & (iTemp289 != iRec195[1])) ? -0.00048828125 : 0.0)));
			fRec192[0] = fTemp290;
			fRec193[0] = std::max<double>(0.0, std::min<double>(1.0, fRec193[1] + fTemp290));
			iRec194[0] = (((fRec193[1] >= 1.0) & (iRec195[1] != iTemp289)) ? iTemp289 : iRec194[1]);
			iRec195[0] = (((fRec193[1] <= 0.0) & (iRec194[1] != iTemp289)) ? iTemp289 : iRec195[1]);
			double fTemp291 = fRec17[0] * fRec197[0] * fTemp35 * (fTemp37 * (fTemp38 * (4.069010416666667e-06 * fTemp39 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec197[0] * fTemp43) + 1.0) + 8.138020833333335e-06 * fTemp44 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec197[0] * fTemp45) + 1.0)) + 8.138020833333335e-06 * fTemp46 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec197[0] * fTemp47) + 1.0)) + 4.069010416666667e-06 * fTemp48 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec197[0] * fTemp49) + 1.0));
			fVec68[IOTA0 & 262143] = fTemp291;
			double fTemp292 = fVec68[(IOTA0 - int(std::min<double>(fConst0, double(std::max<int>(0, iRec194[0]))))) & 262143];
			double fTemp293 = fRec193[0] * (fTemp292 - fVec68[(IOTA0 - int(std::min<double>(fConst0, double(std::max<int>(0, iRec195[0]))))) & 262143]) - fTemp292;
			fVec69[IOTA0 & 131071] = fTemp293;
			fRec198[0] = std::fmod(fRec198[1] + (10001.0 - std::pow(2.0, 0.08333333333333333 * fTemp67 * fRec197[0])), 1e+04);
			int iTemp294 = int(fRec198[0]);
			double fTemp295 = std::floor(fRec198[0]);
			double fTemp296 = std::min<double>(0.0001 * fRec198[0], 1.0);
			double fTemp297 = fRec198[0] + 1e+04;
			int iTemp298 = int(fTemp297);
			double fTemp299 = std::floor(fTemp297);
			double fTemp300 = (fVec69[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp294))) & 131071] * (fTemp295 + (1.0 - fRec198[0])) + (fRec198[0] - fTemp295) * fVec69[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp294 + 1))) & 131071]) * fTemp296 + (fVec69[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp298))) & 131071] * (fTemp299 + (-9999.0 - fRec198[0])) + (fRec198[0] + (1e+04 - fTemp299)) * fVec69[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp298 + 1))) & 131071]) * (1.0 - fTemp296);
			fVec70[0] = fTemp300;
			fRec191[0] = 0.995 * fRec191[1] + fTemp300 - fVec70[1];
			double fTemp301 = std::min<double>(std::max<double>(fRec183[0], -1.0), 1.0) + std::min<double>(std::max<double>(fRec191[0], -1.0), 1.0);
			fVec71[0] = fTemp301;
			fRec182[0] = 0.995 * fRec182[1] - 0.125 * (fVec71[1] - fTemp301);
			output9[i0] = FAUSTFLOAT(std::min<double>(std::max<double>(fRec182[0], -1.0), 1.0));
			fRec206[0] = fTemp3 * (0.1774193548387097 * fTemp7 + 0.03147762747138398 * fTemp9 + 0.27509611275447804 * fTemp10 + 0.23562824849314212 * fTemp11 + 0.421211769587116 * fTemp12 + 0.038583269987787494 * fTemp13 + 0.07567787125262448 * fTemp14 + 0.32336108220603543 * fTemp15 + 0.06295525494276796 * fTemp16 + 0.00558474035782619 * fTemp17 + 0.4434099224598034 * fTemp18 + 0.02233896143130476 * fTemp19 + 0.0009908410312272273 * fTemp20 + 0.5421597749266125 * fTemp21 + 0.007926728249817818 * fTemp22 + 0.00017579437650805645 * fTemp23 + 0.6233894922783426 * fTemp24 + 0.0028127100241289033 * fTemp25 + 0.0033403003537396626 * fTemp26 + 0.7076423086005783 * fTemp27 + 0.015864657413109606 * fTemp28 + 0.5686484698001354 * fTemp29) + fTemp2 * fRec206[1];
			fRec205[0] = fConst0 * fTemp30 * fRec206[0] * fTemp3 + fTemp2 * fRec205[1];
			int iTemp302 = int(fRec205[0]);
			double fTemp303 = ((fRec201[1] != 0.0) ? (((fRec202[1] > 0.0) & (fRec202[1] < 1.0)) ? fRec201[1] : 0.0) : (((fRec202[1] == 0.0) & (iTemp302 != iRec203[1])) ? 0.00048828125 : (((fRec202[1] == 1.0) & (iTemp302 != iRec204[1])) ? -0.00048828125 : 0.0)));
			fRec201[0] = fTemp303;
			fRec202[0] = std::max<double>(0.0, std::min<double>(1.0, fRec202[1] + fTemp303));
			iRec203[0] = (((fRec202[1] >= 1.0) & (iRec204[1] != iTemp302)) ? iTemp302 : iRec203[1]);
			iRec204[0] = (((fRec202[1] <= 0.0) & (iRec203[1] != iTemp302)) ? iTemp302 : iRec204[1]);
			double fTemp304 = fRec17[0] * fRec206[0] * fTemp35 * (fTemp37 * (fTemp38 * (4.069010416666667e-06 * fTemp39 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec206[0] * fTemp43) + 1.0) + 8.138020833333335e-06 * fTemp44 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec206[0] * fTemp45) + 1.0)) + 8.138020833333335e-06 * fTemp46 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec206[0] * fTemp47) + 1.0)) + 4.069010416666667e-06 * fTemp48 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec206[0] * fTemp49) + 1.0));
			fVec72[IOTA0 & 262143] = fTemp304;
			double fTemp305 = fVec72[(IOTA0 - int(std::min<double>(fConst0, double(std::max<int>(0, iRec203[0]))))) & 262143];
			double fTemp306 = fRec202[0] * (fTemp305 - fVec72[(IOTA0 - int(std::min<double>(fConst0, double(std::max<int>(0, iRec204[0]))))) & 262143]) - fTemp305;
			fVec73[IOTA0 & 131071] = fTemp306;
			fRec207[0] = std::fmod(fRec207[1] + (10001.0 - std::pow(2.0, 0.08333333333333333 * fTemp67 * fRec206[0])), 1e+04);
			int iTemp307 = int(fRec207[0]);
			double fTemp308 = std::floor(fRec207[0]);
			double fTemp309 = std::min<double>(0.0001 * fRec207[0], 1.0);
			double fTemp310 = fRec207[0] + 1e+04;
			int iTemp311 = int(fTemp310);
			double fTemp312 = std::floor(fTemp310);
			double fTemp313 = (fVec73[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp307))) & 131071] * (fTemp308 + (1.0 - fRec207[0])) + (fRec207[0] - fTemp308) * fVec73[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp307 + 1))) & 131071]) * fTemp309 + (fVec73[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp311))) & 131071] * (fTemp312 + (-9999.0 - fRec207[0])) + (fRec207[0] + (1e+04 - fTemp312)) * fVec73[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp311 + 1))) & 131071]) * (1.0 - fTemp309);
			fVec74[0] = fTemp313;
			fRec200[0] = 0.995 * fRec200[1] + fTemp313 - fVec74[1];
			fRec214[0] = fTemp3 * (0.6774193548387096 * fTemp7 + 0.45889698231009357 * fTemp9 + 0.8743466161445821 * fTemp10 + 0.7462434077542168 * fTemp11 + 0.8230548917531015 * fTemp12 + 0.514698037468919 * fTemp13 + 0.7644820051634813 * fTemp14 + 0.8959417273673257 * fTemp15 + 0.7918834547346514 * fTemp16 + 0.31086569769393435 * fTemp17 + 0.9664328152797824 * fTemp18 + 0.8657312611191299 * fTemp19 + 0.21058644037331034 * fTemp20 + 0.989171875896704 * fTemp21 + 0.9133750071736322 * fTemp22 + 0.14265533057546828 * fTemp23 + 0.9965070567408723 * fTemp24 + 0.9441129078539562 * fTemp25 + 0.10688961131966927 * fTemp26 + 0.9908638221437681 * fTemp27 + 0.2644029515489432 * fTemp28 + 0.9465419839433038 * fTemp29) + fTemp2 * fRec214[1];
			fRec213[0] = fConst0 * fTemp30 * fRec214[0] * fTemp3 + fTemp2 * fRec213[1];
			int iTemp314 = int(fRec213[0]);
			double fTemp315 = ((fRec209[1] != 0.0) ? (((fRec210[1] > 0.0) & (fRec210[1] < 1.0)) ? fRec209[1] : 0.0) : (((fRec210[1] == 0.0) & (iTemp314 != iRec211[1])) ? 0.00048828125 : (((fRec210[1] == 1.0) & (iTemp314 != iRec212[1])) ? -0.00048828125 : 0.0)));
			fRec209[0] = fTemp315;
			fRec210[0] = std::max<double>(0.0, std::min<double>(1.0, fRec210[1] + fTemp315));
			iRec211[0] = (((fRec210[1] >= 1.0) & (iRec212[1] != iTemp314)) ? iTemp314 : iRec211[1]);
			iRec212[0] = (((fRec210[1] <= 0.0) & (iRec211[1] != iTemp314)) ? iTemp314 : iRec212[1]);
			double fTemp316 = fRec17[0] * fRec214[0] * fTemp35 * (fTemp37 * (fTemp38 * (4.069010416666667e-06 * fTemp39 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec214[0] * fTemp43) + 1.0) + 8.138020833333335e-06 * fTemp44 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec214[0] * fTemp45) + 1.0)) + 8.138020833333335e-06 * fTemp46 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec214[0] * fTemp47) + 1.0)) + 4.069010416666667e-06 * fTemp48 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec214[0] * fTemp49) + 1.0));
			fVec75[IOTA0 & 262143] = fTemp316;
			double fTemp317 = fVec75[(IOTA0 - int(std::min<double>(fConst0, double(std::max<int>(0, iRec211[0]))))) & 262143];
			double fTemp318 = fRec210[0] * (fTemp317 - fVec75[(IOTA0 - int(std::min<double>(fConst0, double(std::max<int>(0, iRec212[0]))))) & 262143]) - fTemp317;
			fVec76[IOTA0 & 131071] = fTemp318;
			fRec215[0] = std::fmod(fRec215[1] + (10001.0 - std::pow(2.0, 0.08333333333333333 * fTemp67 * fRec214[0])), 1e+04);
			int iTemp319 = int(fRec215[0]);
			double fTemp320 = std::floor(fRec215[0]);
			double fTemp321 = std::min<double>(0.0001 * fRec215[0], 1.0);
			double fTemp322 = fRec215[0] + 1e+04;
			int iTemp323 = int(fTemp322);
			double fTemp324 = std::floor(fTemp322);
			double fTemp325 = (fVec76[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp319))) & 131071] * (fTemp320 + (1.0 - fRec215[0])) + (fRec215[0] - fTemp320) * fVec76[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp319 + 1))) & 131071]) * fTemp321 + (fVec76[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp323))) & 131071] * (fTemp324 + (-9999.0 - fRec215[0])) + (fRec215[0] + (1e+04 - fTemp324)) * fVec76[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp323 + 1))) & 131071]) * (1.0 - fTemp321);
			fVec77[0] = fTemp325;
			fRec208[0] = 0.995 * fRec208[1] + fTemp325 - fVec77[1];
			double fTemp326 = std::min<double>(std::max<double>(fRec200[0], -1.0), 1.0) + std::min<double>(std::max<double>(fRec208[0], -1.0), 1.0);
			fVec78[0] = fTemp326;
			fRec199[0] = 0.995 * fRec199[1] - 0.125 * (fVec78[1] - fTemp326);
			output10[i0] = FAUSTFLOAT(std::min<double>(std::max<double>(fRec199[0], -1.0), 1.0));
			fRec223[0] = fTemp3 * (0.1935483870967742 * fTemp7 + 0.037460978147762745 * fTemp9 + 0.29936312297335793 * fTemp10 + 0.2552570552420746 * fTemp11 + 0.43994134506405985 * fTemp12 + 0.04586074359995118 * fTemp13 + 0.08961827939636186 * fTemp14 + 0.34963579604578576 * fTemp15 + 0.07492195629552549 * fTemp16 + 0.007250511899566983 * fTemp17 + 0.4755127387466014 * fTemp18 + 0.02900204759826793 * fTemp19 + 0.0014033248837871579 * fTemp20 + 0.5770264022150011 * fTemp21 + 0.011226599070297263 * fTemp22 + 0.00027161126782977246 * fTemp23 + 0.6588922598508073 * fTemp24 + 0.0043457802852763594 * fTemp25 + 0.003735414021469746 * fTemp26 + 0.7385664629443782 * fTemp27 + 0.018909269306738752 * fTemp28 + 0.5913000896717214 * fTemp29) + fTemp2 * fRec223[1];
			fRec222[0] = fConst0 * fTemp30 * fRec223[0] * fTemp3 + fTemp2 * fRec222[1];
			int iTemp327 = int(fRec222[0]);
			double fTemp328 = ((fRec218[1] != 0.0) ? (((fRec219[1] > 0.0) & (fRec219[1] < 1.0)) ? fRec218[1] : 0.0) : (((fRec219[1] == 0.0) & (iTemp327 != iRec220[1])) ? 0.00048828125 : (((fRec219[1] == 1.0) & (iTemp327 != iRec221[1])) ? -0.00048828125 : 0.0)));
			fRec218[0] = fTemp328;
			fRec219[0] = std::max<double>(0.0, std::min<double>(1.0, fRec219[1] + fTemp328));
			iRec220[0] = (((fRec219[1] >= 1.0) & (iRec221[1] != iTemp327)) ? iTemp327 : iRec220[1]);
			iRec221[0] = (((fRec219[1] <= 0.0) & (iRec220[1] != iTemp327)) ? iTemp327 : iRec221[1]);
			double fTemp329 = fRec17[0] * fRec223[0] * fTemp35 * (fTemp37 * (fTemp38 * (4.069010416666667e-06 * fTemp39 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec223[0] * fTemp43) + 1.0) + 8.138020833333335e-06 * fTemp44 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec223[0] * fTemp45) + 1.0)) + 8.138020833333335e-06 * fTemp46 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec223[0] * fTemp47) + 1.0)) + 4.069010416666667e-06 * fTemp48 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec223[0] * fTemp49) + 1.0));
			fVec79[IOTA0 & 262143] = fTemp329;
			double fTemp330 = fVec79[(IOTA0 - int(std::min<double>(fConst0, double(std::max<int>(0, iRec220[0]))))) & 262143];
			double fTemp331 = fRec219[0] * (fTemp330 - fVec79[(IOTA0 - int(std::min<double>(fConst0, double(std::max<int>(0, iRec221[0]))))) & 262143]) - fTemp330;
			fVec80[IOTA0 & 131071] = fTemp331;
			fRec224[0] = std::fmod(fRec224[1] + (10001.0 - std::pow(2.0, 0.08333333333333333 * fTemp67 * fRec223[0])), 1e+04);
			int iTemp332 = int(fRec224[0]);
			double fTemp333 = std::floor(fRec224[0]);
			double fTemp334 = std::min<double>(0.0001 * fRec224[0], 1.0);
			double fTemp335 = fRec224[0] + 1e+04;
			int iTemp336 = int(fTemp335);
			double fTemp337 = std::floor(fTemp335);
			double fTemp338 = (fVec80[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp332))) & 131071] * (fTemp333 + (1.0 - fRec224[0])) + (fRec224[0] - fTemp333) * fVec80[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp332 + 1))) & 131071]) * fTemp334 + (fVec80[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp336))) & 131071] * (fTemp337 + (-9999.0 - fRec224[0])) + (fRec224[0] + (1e+04 - fTemp337)) * fVec80[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp336 + 1))) & 131071]) * (1.0 - fTemp334);
			fVec81[0] = fTemp338;
			fRec217[0] = 0.995 * fRec217[1] + fTemp338 - fVec81[1];
			fRec231[0] = fTemp3 * (0.6935483870967742 * fTemp7 + 0.481009365244537 * fTemp9 + 0.8863600326884082 * fTemp10 + 0.7600492072792475 * fTemp11 + 0.8327955253822958 * fTemp12 + 0.5370033558948791 * fTemp13 + 0.785634107547396 * fTemp14 + 0.9060874089490115 * fTemp15 + 0.8121748178980229 * fTemp16 + 0.3336032694437918 * fTemp17 + 0.9712203350005035 * fTemp18 + 0.8848813400020141 * fTemp19 + 0.2313700094529524 * fTemp20 + 0.9911804252420898 * fTemp21 + 0.9294434019367184 * fTemp22 + 0.16046629687866054 * fTemp23 + 0.9972972270903179 * fTemp24 + 0.9567556334450854 * fTemp25 + 0.11953324868703187 * fTemp26 + 0.9918302019670118 * fTemp27 + 0.27958995373782936 * fTemp28 + 0.9518862373986775 * fTemp29) + fTemp2 * fRec231[1];
			fRec230[0] = fConst0 * fTemp30 * fRec231[0] * fTemp3 + fTemp2 * fRec230[1];
			int iTemp339 = int(fRec230[0]);
			double fTemp340 = ((fRec226[1] != 0.0) ? (((fRec227[1] > 0.0) & (fRec227[1] < 1.0)) ? fRec226[1] : 0.0) : (((fRec227[1] == 0.0) & (iTemp339 != iRec228[1])) ? 0.00048828125 : (((fRec227[1] == 1.0) & (iTemp339 != iRec229[1])) ? -0.00048828125 : 0.0)));
			fRec226[0] = fTemp340;
			fRec227[0] = std::max<double>(0.0, std::min<double>(1.0, fRec227[1] + fTemp340));
			iRec228[0] = (((fRec227[1] >= 1.0) & (iRec229[1] != iTemp339)) ? iTemp339 : iRec228[1]);
			iRec229[0] = (((fRec227[1] <= 0.0) & (iRec228[1] != iTemp339)) ? iTemp339 : iRec229[1]);
			double fTemp341 = fRec17[0] * fRec231[0] * fTemp35 * (fTemp37 * (fTemp38 * (4.069010416666667e-06 * fTemp39 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec231[0] * fTemp43) + 1.0) + 8.138020833333335e-06 * fTemp44 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec231[0] * fTemp45) + 1.0)) + 8.138020833333335e-06 * fTemp46 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec231[0] * fTemp47) + 1.0)) + 4.069010416666667e-06 * fTemp48 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec231[0] * fTemp49) + 1.0));
			fVec82[IOTA0 & 262143] = fTemp341;
			double fTemp342 = fVec82[(IOTA0 - int(std::min<double>(fConst0, double(std::max<int>(0, iRec228[0]))))) & 262143];
			double fTemp343 = fRec227[0] * (fTemp342 - fVec82[(IOTA0 - int(std::min<double>(fConst0, double(std::max<int>(0, iRec229[0]))))) & 262143]) - fTemp342;
			fVec83[IOTA0 & 131071] = fTemp343;
			fRec232[0] = std::fmod(fRec232[1] + (10001.0 - std::pow(2.0, 0.08333333333333333 * fTemp67 * fRec231[0])), 1e+04);
			int iTemp344 = int(fRec232[0]);
			double fTemp345 = std::floor(fRec232[0]);
			double fTemp346 = std::min<double>(0.0001 * fRec232[0], 1.0);
			double fTemp347 = fRec232[0] + 1e+04;
			int iTemp348 = int(fTemp347);
			double fTemp349 = std::floor(fTemp347);
			double fTemp350 = (fVec83[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp344))) & 131071] * (fTemp345 + (1.0 - fRec232[0])) + (fRec232[0] - fTemp345) * fVec83[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp344 + 1))) & 131071]) * fTemp346 + (fVec83[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp348))) & 131071] * (fTemp349 + (-9999.0 - fRec232[0])) + (fRec232[0] + (1e+04 - fTemp349)) * fVec83[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp348 + 1))) & 131071]) * (1.0 - fTemp346);
			fVec84[0] = fTemp350;
			fRec225[0] = 0.995 * fRec225[1] + fTemp350 - fVec84[1];
			double fTemp351 = std::min<double>(std::max<double>(fRec217[0], -1.0), 1.0) + std::min<double>(std::max<double>(fRec225[0], -1.0), 1.0);
			fVec85[0] = fTemp351;
			fRec216[0] = 0.995 * fRec216[1] - 0.125 * (fVec85[1] - fTemp351);
			output11[i0] = FAUSTFLOAT(std::min<double>(std::max<double>(fRec216[0], -1.0), 1.0));
			fRec240[0] = fTemp3 * (0.20967741935483872 * fTemp7 + 0.0439646201873049 * fTemp9 + 0.3234379871492381 * fTemp10 + 0.2746223801090057 * fTemp11 + 0.4579054698896255 * fTemp12 + 0.05375063092816312 * fTemp13 + 0.10461213153115073 * fTemp14 + 0.37539021852237253 * fTemp15 + 0.0879292403746098 * fTemp16 + 0.009218388103789736 * fTemp17 + 0.5063567856063913 * fTemp18 + 0.036873552415158946 * fTemp19 + 0.001932887828213977 * fTemp20 + 0.6098626208824705 * fTemp21 + 0.015463102625711817 * fTemp22 + 0.0004052829317222855 * fTemp23 + 0.6916656197296944 * fTemp24 + 0.006484526907556568 * fTemp25 + 0.004177264447543231 * fTemp26 + 0.7662196127960382 * fTemp27 + 0.02222938282402087 * fTemp28 + 0.6126909649426638 * fTemp29) + fTemp2 * fRec240[1];
			fRec239[0] = fConst0 * fTemp30 * fRec240[0] * fTemp3 + fTemp2 * fRec239[1];
			int iTemp352 = int(fRec239[0]);
			double fTemp353 = ((fRec235[1] != 0.0) ? (((fRec236[1] > 0.0) & (fRec236[1] < 1.0)) ? fRec235[1] : 0.0) : (((fRec236[1] == 0.0) & (iTemp352 != iRec237[1])) ? 0.00048828125 : (((fRec236[1] == 1.0) & (iTemp352 != iRec238[1])) ? -0.00048828125 : 0.0)));
			fRec235[0] = fTemp353;
			fRec236[0] = std::max<double>(0.0, std::min<double>(1.0, fRec236[1] + fTemp353));
			iRec237[0] = (((fRec236[1] >= 1.0) & (iRec238[1] != iTemp352)) ? iTemp352 : iRec237[1]);
			iRec238[0] = (((fRec236[1] <= 0.0) & (iRec237[1] != iTemp352)) ? iTemp352 : iRec238[1]);
			double fTemp354 = fRec17[0] * fRec240[0] * fTemp35 * (fTemp37 * (fTemp38 * (4.069010416666667e-06 * fTemp39 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec240[0] * fTemp43) + 1.0) + 8.138020833333335e-06 * fTemp44 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec240[0] * fTemp45) + 1.0)) + 8.138020833333335e-06 * fTemp46 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec240[0] * fTemp47) + 1.0)) + 4.069010416666667e-06 * fTemp48 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec240[0] * fTemp49) + 1.0));
			fVec86[IOTA0 & 262143] = fTemp354;
			double fTemp355 = fVec86[(IOTA0 - int(std::min<double>(fConst0, double(std::max<int>(0, iRec237[0]))))) & 262143];
			double fTemp356 = fRec236[0] * (fTemp355 - fVec86[(IOTA0 - int(std::min<double>(fConst0, double(std::max<int>(0, iRec238[0]))))) & 262143]) - fTemp355;
			fVec87[IOTA0 & 131071] = fTemp356;
			fRec241[0] = std::fmod(fRec241[1] + (10001.0 - std::pow(2.0, 0.08333333333333333 * fTemp67 * fRec240[0])), 1e+04);
			double fTemp357 = fRec241[0] + 1e+04;
			int iTemp358 = int(fTemp357);
			double fTemp359 = std::floor(fTemp357);
			double fTemp360 = std::min<double>(0.0001 * fRec241[0], 1.0);
			double fTemp361 = (fVec87[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp358))) & 131071] * (fTemp359 + (-9999.0 - fRec241[0])) + (fRec241[0] + (1e+04 - fTemp359)) * fVec87[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp358 + 1))) & 131071]) * (1.0 - fTemp360);
			int iTemp362 = int(fRec241[0]);
			double fTemp363 = std::floor(fRec241[0]);
			double fTemp364 = (fVec87[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp362))) & 131071] * (fTemp363 + (1.0 - fRec241[0])) + (fRec241[0] - fTemp363) * fVec87[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp362 + 1))) & 131071]) * fTemp360;
			fVec88[0] = fTemp364 + fTemp361;
			fRec234[0] = fTemp361 + 0.995 * fRec234[1] + fTemp364 - fVec88[1];
			fRec248[0] = fTemp3 * (0.7096774193548387 * fTemp7 + 0.5036420395421437 * fTemp9 + 0.8978045395707416 * fTemp10 + 0.773724144176324 * fTemp11 + 0.842423539174232 * fTemp12 + 0.5596058484423655 * fTemp13 + 0.8060529912738312 * fTemp14 + 0.9157127991675338 * fTemp15 + 0.8314255983350677 * fTemp16 + 0.35742338290087616 * fTemp17 + 0.9755295223389614 * fTemp18 + 0.9021180893558458 * fTemp19 + 0.2536553039941702 * fTemp20 + 0.9928956677758275 * fTemp21 + 0.9431653422066202 * fTemp22 + 0.1800134415442498 * fTemp23 + 0.9979374519349177 * fTemp24 + 0.9669992309586827 * fTemp25 + 0.13367246232138338 * fTemp26 + 0.9926943628998762 * fTemp27 + 0.29547323652124025 * fTemp28 + 0.9569288370446016 * fTemp29) + fTemp2 * fRec248[1];
			fRec247[0] = fConst0 * fTemp30 * fRec248[0] * fTemp3 + fTemp2 * fRec247[1];
			int iTemp365 = int(fRec247[0]);
			double fTemp366 = ((fRec243[1] != 0.0) ? (((fRec244[1] > 0.0) & (fRec244[1] < 1.0)) ? fRec243[1] : 0.0) : (((fRec244[1] == 0.0) & (iTemp365 != iRec245[1])) ? 0.00048828125 : (((fRec244[1] == 1.0) & (iTemp365 != iRec246[1])) ? -0.00048828125 : 0.0)));
			fRec243[0] = fTemp366;
			fRec244[0] = std::max<double>(0.0, std::min<double>(1.0, fRec244[1] + fTemp366));
			iRec245[0] = (((fRec244[1] >= 1.0) & (iRec246[1] != iTemp365)) ? iTemp365 : iRec245[1]);
			iRec246[0] = (((fRec244[1] <= 0.0) & (iRec245[1] != iTemp365)) ? iTemp365 : iRec246[1]);
			double fTemp367 = fRec17[0] * fRec248[0] * fTemp35 * (fTemp37 * (fTemp38 * (4.069010416666667e-06 * fTemp39 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec248[0] * fTemp43) + 1.0) + 8.138020833333335e-06 * fTemp44 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec248[0] * fTemp45) + 1.0)) + 8.138020833333335e-06 * fTemp46 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec248[0] * fTemp47) + 1.0)) + 4.069010416666667e-06 * fTemp48 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec248[0] * fTemp49) + 1.0));
			fVec89[IOTA0 & 262143] = fTemp367;
			double fTemp368 = fVec89[(IOTA0 - int(std::min<double>(fConst0, double(std::max<int>(0, iRec245[0]))))) & 262143];
			double fTemp369 = fRec244[0] * (fTemp368 - fVec89[(IOTA0 - int(std::min<double>(fConst0, double(std::max<int>(0, iRec246[0]))))) & 262143]) - fTemp368;
			fVec90[IOTA0 & 131071] = fTemp369;
			fRec249[0] = std::fmod(fRec249[1] + (10001.0 - std::pow(2.0, 0.08333333333333333 * fTemp67 * fRec248[0])), 1e+04);
			int iTemp370 = int(fRec249[0]);
			double fTemp371 = std::floor(fRec249[0]);
			double fTemp372 = std::min<double>(0.0001 * fRec249[0], 1.0);
			double fTemp373 = fRec249[0] + 1e+04;
			int iTemp374 = int(fTemp373);
			double fTemp375 = std::floor(fTemp373);
			double fTemp376 = (fVec90[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp370))) & 131071] * (fTemp371 + (1.0 - fRec249[0])) + (fRec249[0] - fTemp371) * fVec90[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp370 + 1))) & 131071]) * fTemp372 + (fVec90[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp374))) & 131071] * (fTemp375 + (-9999.0 - fRec249[0])) + (fRec249[0] + (1e+04 - fTemp375)) * fVec90[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp374 + 1))) & 131071]) * (1.0 - fTemp372);
			fVec91[0] = fTemp376;
			fRec242[0] = 0.995 * fRec242[1] + fTemp376 - fVec91[1];
			double fTemp377 = std::min<double>(std::max<double>(fRec234[0], -1.0), 1.0) + std::min<double>(std::max<double>(fRec242[0], -1.0), 1.0);
			fVec92[0] = fTemp377;
			fRec233[0] = 0.995 * fRec233[1] + 0.125 * (fTemp377 - fVec92[1]);
			output12[i0] = FAUSTFLOAT(std::min<double>(std::max<double>(fRec233[0], -1.0), 1.0));
			fRec257[0] = fTemp3 * (0.22580645161290322 * fTemp7 + 0.0509885535900104 * fTemp9 + 0.3473052528448203 * fTemp10 + 0.2937312030567103 * fTemp11 + 0.47519096331149147 * fTemp12 + 0.06224786785291958 * fTemp13 + 0.12062093865360451 * fTemp14 + 0.4006243496357961 * fTemp15 + 0.1019771071800208 * fTemp16 + 0.011513544359034606 * fTemp17 + 0.5359672384277131 * fTemp18 + 0.046054177436138426 * fTemp19 + 0.002599832597201363 * fTemp20 + 0.6407488297504875 * fTemp21 + 0.020798660777610903 * fTemp22 + 0.000587058973561598 * fTemp23 + 0.721870061742313 * fTemp24 + 0.009392943576985568 * fTemp25 + 0.004671379976734925 * fTemp26 + 0.7909477488742906 * fTemp27 + 0.025827814803774274 * fTemp28 + 0.6329489313015673 * fTemp29) + fTemp2 * fRec257[1];
			fRec256[0] = fConst0 * fTemp30 * fRec257[0] * fTemp3 + fTemp2 * fRec256[1];
			int iTemp378 = int(fRec256[0]);
			double fTemp379 = ((fRec252[1] != 0.0) ? (((fRec253[1] > 0.0) & (fRec253[1] < 1.0)) ? fRec252[1] : 0.0) : (((fRec253[1] == 0.0) & (iTemp378 != iRec254[1])) ? 0.00048828125 : (((fRec253[1] == 1.0) & (iTemp378 != iRec255[1])) ? -0.00048828125 : 0.0)));
			fRec252[0] = fTemp379;
			fRec253[0] = std::max<double>(0.0, std::min<double>(1.0, fRec253[1] + fTemp379));
			iRec254[0] = (((fRec253[1] >= 1.0) & (iRec255[1] != iTemp378)) ? iTemp378 : iRec254[1]);
			iRec255[0] = (((fRec253[1] <= 0.0) & (iRec254[1] != iTemp378)) ? iTemp378 : iRec255[1]);
			double fTemp380 = fRec17[0] * fRec257[0] * fTemp35 * (fTemp37 * (fTemp38 * (4.069010416666667e-06 * fTemp39 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec257[0] * fTemp43) + 1.0) + 8.138020833333335e-06 * fTemp44 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec257[0] * fTemp45) + 1.0)) + 8.138020833333335e-06 * fTemp46 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec257[0] * fTemp47) + 1.0)) + 4.069010416666667e-06 * fTemp48 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec257[0] * fTemp49) + 1.0));
			fVec93[IOTA0 & 262143] = fTemp380;
			double fTemp381 = fVec93[(IOTA0 - int(std::min<double>(fConst0, double(std::max<int>(0, iRec254[0]))))) & 262143];
			double fTemp382 = fRec253[0] * (fTemp381 - fVec93[(IOTA0 - int(std::min<double>(fConst0, double(std::max<int>(0, iRec255[0]))))) & 262143]) - fTemp381;
			fVec94[IOTA0 & 131071] = fTemp382;
			fRec258[0] = std::fmod(fRec258[1] + (10001.0 - std::pow(2.0, 0.08333333333333333 * fTemp67 * fRec257[0])), 1e+04);
			int iTemp383 = int(fRec258[0]);
			double fTemp384 = std::floor(fRec258[0]);
			double fTemp385 = std::min<double>(0.0001 * fRec258[0], 1.0);
			double fTemp386 = fRec258[0] + 1e+04;
			int iTemp387 = int(fTemp386);
			double fTemp388 = std::floor(fTemp386);
			double fTemp389 = (fVec94[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp383))) & 131071] * (fTemp384 + (1.0 - fRec258[0])) + (fRec258[0] - fTemp384) * fVec94[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp383 + 1))) & 131071]) * fTemp385 + (fVec94[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp387))) & 131071] * (fTemp388 + (-9999.0 - fRec258[0])) + (fRec258[0] + (1e+04 - fTemp388)) * fVec94[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp387 + 1))) & 131071]) * (1.0 - fTemp385);
			fVec95[0] = fTemp389;
			fRec251[0] = 0.995 * fRec251[1] + fTemp389 - fVec95[1];
			fRec265[0] = fTemp3 * (0.7258064516129032 * fTemp7 + 0.5267950052029137 * fTemp9 + 0.9086727911416249 * fTemp10 + 0.7872706760142718 * fTemp11 + 0.8519427513705973 * fTemp12 + 0.5824910077149368 * fTemp13 + 0.825686241361111 * fTemp14 + 0.9248178980228928 * fTemp15 + 0.8496357960457857 * fTemp16 + 0.3823512134537277 * fTemp17 + 0.9793855526836964 * fTemp18 + 0.9175422107347857 * fTemp19 + 0.2775129775067378 * fTemp20 + 0.9943476515423039 * fTemp21 + 0.9547812123384308 * fTemp22 + 0.20142070948069682 * fTemp23 + 0.998450162519664 * fTemp24 + 0.9752026003146234 * fTemp25 + 0.1494841592555176 * fTemp26 + 0.9934671171523216 * fTemp27 + 0.3121010286407703 * fTemp28 + 0.9616745281137963 * fTemp29) + fTemp2 * fRec265[1];
			fRec264[0] = fConst0 * fTemp30 * fRec265[0] * fTemp3 + fTemp2 * fRec264[1];
			int iTemp390 = int(fRec264[0]);
			double fTemp391 = ((fRec260[1] != 0.0) ? (((fRec261[1] > 0.0) & (fRec261[1] < 1.0)) ? fRec260[1] : 0.0) : (((fRec261[1] == 0.0) & (iTemp390 != iRec262[1])) ? 0.00048828125 : (((fRec261[1] == 1.0) & (iTemp390 != iRec263[1])) ? -0.00048828125 : 0.0)));
			fRec260[0] = fTemp391;
			fRec261[0] = std::max<double>(0.0, std::min<double>(1.0, fRec261[1] + fTemp391));
			iRec262[0] = (((fRec261[1] >= 1.0) & (iRec263[1] != iTemp390)) ? iTemp390 : iRec262[1]);
			iRec263[0] = (((fRec261[1] <= 0.0) & (iRec262[1] != iTemp390)) ? iTemp390 : iRec263[1]);
			double fTemp392 = fRec17[0] * fRec265[0] * fTemp35 * (fTemp37 * (fTemp38 * (8.138020833333335e-06 * fTemp44 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec265[0] * fTemp45) + 1.0) + 4.069010416666667e-06 * fTemp39 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec265[0] * fTemp43) + 1.0)) + 8.138020833333335e-06 * fTemp46 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec265[0] * fTemp47) + 1.0)) + 4.069010416666667e-06 * fTemp48 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec265[0] * fTemp49) + 1.0));
			fVec96[IOTA0 & 262143] = fTemp392;
			double fTemp393 = fVec96[(IOTA0 - int(std::min<double>(fConst0, double(std::max<int>(0, iRec262[0]))))) & 262143];
			double fTemp394 = fRec261[0] * (fTemp393 - fVec96[(IOTA0 - int(std::min<double>(fConst0, double(std::max<int>(0, iRec263[0]))))) & 262143]) - fTemp393;
			fVec97[IOTA0 & 131071] = fTemp394;
			fRec266[0] = std::fmod(fRec266[1] + (10001.0 - std::pow(2.0, 0.08333333333333333 * fTemp67 * fRec265[0])), 1e+04);
			int iTemp395 = int(fRec266[0]);
			double fTemp396 = std::floor(fRec266[0]);
			double fTemp397 = std::min<double>(0.0001 * fRec266[0], 1.0);
			double fTemp398 = fRec266[0] + 1e+04;
			int iTemp399 = int(fTemp398);
			double fTemp400 = std::floor(fTemp398);
			double fTemp401 = (fVec97[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp395))) & 131071] * (fTemp396 + (1.0 - fRec266[0])) + (fRec266[0] - fTemp396) * fVec97[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp395 + 1))) & 131071]) * fTemp397 + (fVec97[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp399))) & 131071] * (fTemp400 + (-9999.0 - fRec266[0])) + (fRec266[0] + (1e+04 - fTemp400)) * fVec97[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp399 + 1))) & 131071]) * (1.0 - fTemp397);
			fVec98[0] = fTemp401;
			fRec259[0] = 0.995 * fRec259[1] + fTemp401 - fVec98[1];
			double fTemp402 = std::min<double>(std::max<double>(fRec251[0], -1.0), 1.0) + std::min<double>(std::max<double>(fRec259[0], -1.0), 1.0);
			fVec99[0] = fTemp402;
			fRec250[0] = 0.995 * fRec250[1] + 0.125 * (fTemp402 - fVec99[1]);
			output13[i0] = FAUSTFLOAT(std::min<double>(std::max<double>(fRec250[0], -1.0), 1.0));
			fRec274[0] = fTemp3 * (0.24193548387096775 * fTemp7 + 0.0585327783558793 * fTemp9 + 0.3709496008697677 * fTemp10 + 0.3125902303080262 * fTemp11 + 0.4918693768379647 * fTemp12 + 0.07134700042773778 * fTemp13 + 0.13760360638543995 * fTemp14 + 0.42533818938605616 * fTemp15 + 0.1170655567117586 * fTemp16 + 0.014161156053841766 * fTemp17 + 0.5643692725991072 * fTemp18 + 0.05664462421536706 * fTemp19 + 0.0034260861420584918 * fTemp20 + 0.6697638034219038 * fTemp21 + 0.027408689136467934 * fTemp22 + 0.0008288918085625383 * fTemp23 + 0.7496596574327334 * fTemp24 + 0.013262268937000613 * fTemp25 + 0.005223942884409443 * fTemp26 + 0.813060264706474 * fTemp27 + 0.02970766176161077 * fTemp28 + 0.6521795683598622 * fTemp29) + fTemp2 * fRec274[1];
			fRec273[0] = fConst0 * fTemp30 * fRec274[0] * fTemp3 + fTemp2 * fRec273[1];
			int iTemp403 = int(fRec273[0]);
			double fTemp404 = ((fRec269[1] != 0.0) ? (((fRec270[1] > 0.0) & (fRec270[1] < 1.0)) ? fRec269[1] : 0.0) : (((fRec270[1] == 0.0) & (iTemp403 != iRec271[1])) ? 0.00048828125 : (((fRec270[1] == 1.0) & (iTemp403 != iRec272[1])) ? -0.00048828125 : 0.0)));
			fRec269[0] = fTemp404;
			fRec270[0] = std::max<double>(0.0, std::min<double>(1.0, fRec270[1] + fTemp404));
			iRec271[0] = (((fRec270[1] >= 1.0) & (iRec272[1] != iTemp403)) ? iTemp403 : iRec271[1]);
			iRec272[0] = (((fRec270[1] <= 0.0) & (iRec271[1] != iTemp403)) ? iTemp403 : iRec272[1]);
			double fTemp405 = fRec17[0] * fRec274[0] * fTemp35 * (fTemp37 * (fTemp38 * (4.069010416666667e-06 * fTemp39 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec274[0] * fTemp43) + 1.0) + 8.138020833333335e-06 * fTemp44 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec274[0] * fTemp45) + 1.0)) + 8.138020833333335e-06 * fTemp46 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec274[0] * fTemp47) + 1.0)) + 4.069010416666667e-06 * fTemp48 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec274[0] * fTemp49) + 1.0));
			fVec100[IOTA0 & 262143] = fTemp405;
			double fTemp406 = fVec100[(IOTA0 - int(std::min<double>(fConst0, double(std::max<int>(0, iRec271[0]))))) & 262143];
			double fTemp407 = fRec270[0] * (fTemp406 - fVec100[(IOTA0 - int(std::min<double>(fConst0, double(std::max<int>(0, iRec272[0]))))) & 262143]) - fTemp406;
			fVec101[IOTA0 & 131071] = fTemp407;
			fRec275[0] = std::fmod(fRec275[1] + (10001.0 - std::pow(2.0, 0.08333333333333333 * fTemp67 * fRec274[0])), 1e+04);
			int iTemp408 = int(fRec275[0]);
			double fTemp409 = std::floor(fRec275[0]);
			double fTemp410 = std::min<double>(0.0001 * fRec275[0], 1.0);
			double fTemp411 = fRec275[0] + 1e+04;
			int iTemp412 = int(fTemp411);
			double fTemp413 = std::floor(fTemp411);
			double fTemp414 = (fVec101[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp408))) & 131071] * (fTemp409 + (1.0 - fRec275[0])) + (fRec275[0] - fTemp409) * fVec101[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp408 + 1))) & 131071]) * fTemp410 + (fVec101[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp412))) & 131071] * (fTemp413 + (-9999.0 - fRec275[0])) + (fRec275[0] + (1e+04 - fTemp413)) * fVec101[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp412 + 1))) & 131071]) * (1.0 - fTemp410);
			fVec102[0] = fTemp414;
			fRec268[0] = 0.995 * fRec268[1] + fTemp414 - fVec102[1];
			fRec282[0] = fTemp3 * (0.7419354838709677 * fTemp7 + 0.5504682622268471 * fTemp9 + 0.9189578116202306 * fTemp10 + 0.8006911917765933 * fTemp11 + 0.861356769214109 * fTemp12 + 0.6056441448866812 * fTemp13 + 0.8444834595378432 * fTemp14 + 0.9334027055150884 * fTemp15 + 0.8668054110301769 * fTemp16 + 0.40841193649088653 * fTemp17 + 0.9828136014232486 * fTemp18 + 0.9312544056929946 * fTemp19 + 0.30301530771904484 * fTemp20 + 0.99556480036729 * fTemp21 + 0.9645184029383198 * fTemp22 + 0.22481780895283973 * fTemp23 + 0.998855432352849 * fTemp24 + 0.9816869176455844 * fTemp25 + 0.1671661723011021 * fTemp26 + 0.9941581332720774 * fTemp27 + 0.32952871965075725 * fTemp28 + 0.9661276859272218 * fTemp29) + fTemp2 * fRec282[1];
			fRec281[0] = fConst0 * fTemp30 * fRec282[0] * fTemp3 + fTemp2 * fRec281[1];
			int iTemp415 = int(fRec281[0]);
			double fTemp416 = ((fRec277[1] != 0.0) ? (((fRec278[1] > 0.0) & (fRec278[1] < 1.0)) ? fRec277[1] : 0.0) : (((fRec278[1] == 0.0) & (iTemp415 != iRec279[1])) ? 0.00048828125 : (((fRec278[1] == 1.0) & (iTemp415 != iRec280[1])) ? -0.00048828125 : 0.0)));
			fRec277[0] = fTemp416;
			fRec278[0] = std::max<double>(0.0, std::min<double>(1.0, fRec278[1] + fTemp416));
			iRec279[0] = (((fRec278[1] >= 1.0) & (iRec280[1] != iTemp415)) ? iTemp415 : iRec279[1]);
			iRec280[0] = (((fRec278[1] <= 0.0) & (iRec279[1] != iTemp415)) ? iTemp415 : iRec280[1]);
			double fTemp417 = fRec17[0] * fRec282[0] * fTemp35 * (fTemp37 * (fTemp38 * (4.069010416666667e-06 * fTemp39 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec282[0] * fTemp43) + 1.0) + 8.138020833333335e-06 * fTemp44 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec282[0] * fTemp45) + 1.0)) + 8.138020833333335e-06 * fTemp46 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec282[0] * fTemp47) + 1.0)) + 4.069010416666667e-06 * fTemp48 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec282[0] * fTemp49) + 1.0));
			fVec103[IOTA0 & 262143] = fTemp417;
			double fTemp418 = fVec103[(IOTA0 - int(std::min<double>(fConst0, double(std::max<int>(0, iRec279[0]))))) & 262143];
			double fTemp419 = fRec278[0] * (fTemp418 - fVec103[(IOTA0 - int(std::min<double>(fConst0, double(std::max<int>(0, iRec280[0]))))) & 262143]) - fTemp418;
			fVec104[IOTA0 & 131071] = fTemp419;
			fRec283[0] = std::fmod(fRec283[1] + (10001.0 - std::pow(2.0, 0.08333333333333333 * fTemp67 * fRec282[0])), 1e+04);
			int iTemp420 = int(fRec283[0]);
			double fTemp421 = std::floor(fRec283[0]);
			double fTemp422 = std::min<double>(0.0001 * fRec283[0], 1.0);
			double fTemp423 = fRec283[0] + 1e+04;
			int iTemp424 = int(fTemp423);
			double fTemp425 = std::floor(fTemp423);
			double fTemp426 = (fVec104[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp420))) & 131071] * (fTemp421 + (1.0 - fRec283[0])) + (fRec283[0] - fTemp421) * fVec104[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp420 + 1))) & 131071]) * fTemp422 + (fVec104[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp424))) & 131071] * (fTemp425 + (-9999.0 - fRec283[0])) + (fRec283[0] + (1e+04 - fTemp425)) * fVec104[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp424 + 1))) & 131071]) * (1.0 - fTemp422);
			fVec105[0] = fTemp426;
			fRec276[0] = 0.995 * fRec276[1] + fTemp426 - fVec105[1];
			double fTemp427 = std::min<double>(std::max<double>(fRec268[0], -1.0), 1.0) + std::min<double>(std::max<double>(fRec276[0], -1.0), 1.0);
			fVec106[0] = fTemp427;
			fRec267[0] = 0.995 * fRec267[1] + 0.125 * (fTemp427 - fVec106[1]);
			output14[i0] = FAUSTFLOAT(std::min<double>(std::max<double>(fRec267[0], -1.0), 1.0));
			fRec291[0] = fTemp3 * (0.25806451612903225 * fTemp7 + 0.06659729448491154 * fTemp9 + 0.39435585511331855 * fTemp10 + 0.3312059084753731 * fTemp11 + 0.508000508000762 * fTemp12 + 0.0810421883797694 * fTemp13 + 0.15551654046215668 * fTemp14 + 0.4495317377731529 * fTemp15 + 0.13319458896982309 * fTemp16 + 0.017186398576751367 * fTemp17 + 0.5915880635091135 * fTemp18 + 0.06874559430700547 * fTemp19 + 0.00443519963271003 * fTemp20 + 0.6969846922809552 * fTemp21 + 0.03548159706168024 * fTemp22 + 0.0011445676471509754 * fTemp23 + 0.7751821910471602 * fTemp24 + 0.018313082354415606 * fTemp25 + 0.005841866727922688 * fTemp26 + 0.8328338276988979 * fTemp27 + 0.03387231407277824 * fTemp28 + 0.6704712803492427 * fTemp29) + fTemp2 * fRec291[1];
			fRec290[0] = fConst0 * fTemp30 * fRec291[0] * fTemp3 + fTemp2 * fRec290[1];
			int iTemp428 = int(fRec290[0]);
			double fTemp429 = ((fRec286[1] != 0.0) ? (((fRec287[1] > 0.0) & (fRec287[1] < 1.0)) ? fRec286[1] : 0.0) : (((fRec287[1] == 0.0) & (iTemp428 != iRec288[1])) ? 0.00048828125 : (((fRec287[1] == 1.0) & (iTemp428 != iRec289[1])) ? -0.00048828125 : 0.0)));
			fRec286[0] = fTemp429;
			fRec287[0] = std::max<double>(0.0, std::min<double>(1.0, fRec287[1] + fTemp429));
			iRec288[0] = (((fRec287[1] >= 1.0) & (iRec289[1] != iTemp428)) ? iTemp428 : iRec288[1]);
			iRec289[0] = (((fRec287[1] <= 0.0) & (iRec288[1] != iTemp428)) ? iTemp428 : iRec289[1]);
			double fTemp430 = fRec17[0] * fRec291[0] * fTemp35 * (fTemp37 * (fTemp38 * (4.069010416666667e-06 * fTemp39 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec291[0] * fTemp43) + 1.0) + 8.138020833333335e-06 * fTemp44 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec291[0] * fTemp45) + 1.0)) + 8.138020833333335e-06 * fTemp46 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec291[0] * fTemp47) + 1.0)) + 4.069010416666667e-06 * fTemp48 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec291[0] * fTemp49) + 1.0));
			fVec107[IOTA0 & 262143] = fTemp430;
			double fTemp431 = fVec107[(IOTA0 - int(std::min<double>(fConst0, double(std::max<int>(0, iRec288[0]))))) & 262143];
			double fTemp432 = fRec287[0] * (fTemp431 - fVec107[(IOTA0 - int(std::min<double>(fConst0, double(std::max<int>(0, iRec289[0]))))) & 262143]) - fTemp431;
			fVec108[IOTA0 & 131071] = fTemp432;
			fRec292[0] = std::fmod(fRec292[1] + (10001.0 - std::pow(2.0, 0.08333333333333333 * fTemp67 * fRec291[0])), 1e+04);
			int iTemp433 = int(fRec292[0]);
			double fTemp434 = std::floor(fRec292[0]);
			double fTemp435 = std::min<double>(0.0001 * fRec292[0], 1.0);
			double fTemp436 = fRec292[0] + 1e+04;
			int iTemp437 = int(fTemp436);
			double fTemp438 = std::floor(fTemp436);
			double fTemp439 = (fVec108[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp433))) & 131071] * (fTemp434 + (1.0 - fRec292[0])) + (fRec292[0] - fTemp434) * fVec108[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp433 + 1))) & 131071]) * fTemp435 + (fVec108[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp437))) & 131071] * (fTemp438 + (-9999.0 - fRec292[0])) + (fRec292[0] + (1e+04 - fTemp438)) * fVec108[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp437 + 1))) & 131071]) * (1.0 - fTemp435);
			fVec109[0] = fTemp439;
			fRec285[0] = 0.995 * fRec285[1] + fTemp439 - fVec109[1];
			fRec299[0] = fTemp3 * (0.7580645161290323 * fTemp7 + 0.5746618106139438 * fTemp9 + 0.9286529995722622 * fTemp10 + 0.8139880143900511 * fTemp11 + 0.8706690049203728 * fTemp12 + 0.6290503991302323 * fTemp13 + 0.86239639361456 * fTemp14 + 0.9414672216441207 * fTemp15 + 0.8829344432882414 * fTemp16 + 0.43563072740089287 * fTemp17 + 0.9858388439461583 * fTemp18 + 0.9433553757846329 * fTemp19 + 0.3302361965780962 * fTemp20 + 0.9965739138579415 * fTemp21 + 0.972591310863532 * fTemp22 + 0.2503403425672665 * fTemp23 + 0.9991711081914375 * fTemp24 + 0.9867377310629993 * fTemp25 + 0.18693973529352612 * fTemp26 + 0.9947760571155906 * fTemp27 + 0.34782043164013776 * fTemp28 + 0.9702923382383892 * fTemp29) + fTemp2 * fRec299[1];
			fRec298[0] = fConst0 * fTemp30 * fRec299[0] * fTemp3 + fTemp2 * fRec298[1];
			int iTemp440 = int(fRec298[0]);
			double fTemp441 = ((fRec294[1] != 0.0) ? (((fRec295[1] > 0.0) & (fRec295[1] < 1.0)) ? fRec294[1] : 0.0) : (((fRec295[1] == 0.0) & (iTemp440 != iRec296[1])) ? 0.00048828125 : (((fRec295[1] == 1.0) & (iTemp440 != iRec297[1])) ? -0.00048828125 : 0.0)));
			fRec294[0] = fTemp441;
			fRec295[0] = std::max<double>(0.0, std::min<double>(1.0, fRec295[1] + fTemp441));
			iRec296[0] = (((fRec295[1] >= 1.0) & (iRec297[1] != iTemp440)) ? iTemp440 : iRec296[1]);
			iRec297[0] = (((fRec295[1] <= 0.0) & (iRec296[1] != iTemp440)) ? iTemp440 : iRec297[1]);
			double fTemp442 = fRec17[0] * fRec299[0] * fTemp35 * (fTemp37 * (fTemp38 * (8.138020833333335e-06 * fTemp44 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec299[0] * fTemp45) + 1.0) + 4.069010416666667e-06 * fTemp39 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec299[0] * fTemp43) + 1.0)) + 8.138020833333335e-06 * fTemp46 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec299[0] * fTemp47) + 1.0)) + 4.069010416666667e-06 * fTemp48 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec299[0] * fTemp49) + 1.0));
			fVec110[IOTA0 & 262143] = fTemp442;
			double fTemp443 = fVec110[(IOTA0 - int(std::min<double>(fConst0, double(std::max<int>(0, iRec296[0]))))) & 262143];
			double fTemp444 = fRec295[0] * (fTemp443 - fVec110[(IOTA0 - int(std::min<double>(fConst0, double(std::max<int>(0, iRec297[0]))))) & 262143]) - fTemp443;
			fVec111[IOTA0 & 131071] = fTemp444;
			fRec300[0] = std::fmod(fRec300[1] + (10001.0 - std::pow(2.0, 0.08333333333333333 * fTemp67 * fRec299[0])), 1e+04);
			int iTemp445 = int(fRec300[0]);
			double fTemp446 = std::floor(fRec300[0]);
			double fTemp447 = std::min<double>(0.0001 * fRec300[0], 1.0);
			double fTemp448 = fRec300[0] + 1e+04;
			int iTemp449 = int(fTemp448);
			double fTemp450 = std::floor(fTemp448);
			double fTemp451 = (fVec111[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp445))) & 131071] * (fTemp446 + (1.0 - fRec300[0])) + (fRec300[0] - fTemp446) * fVec111[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp445 + 1))) & 131071]) * fTemp447 + (fVec111[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp449))) & 131071] * (fTemp450 + (-9999.0 - fRec300[0])) + (fRec300[0] + (1e+04 - fTemp450)) * fVec111[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp449 + 1))) & 131071]) * (1.0 - fTemp447);
			fVec112[0] = fTemp451;
			fRec293[0] = 0.995 * fRec293[1] + fTemp451 - fVec112[1];
			double fTemp452 = std::min<double>(std::max<double>(fRec285[0], -1.0), 1.0) + std::min<double>(std::max<double>(fRec293[0], -1.0), 1.0);
			fVec113[0] = fTemp452;
			fRec284[0] = 0.995 * fRec284[1] + 0.125 * (fTemp452 - fVec113[1]);
			output15[i0] = FAUSTFLOAT(std::min<double>(std::max<double>(fRec284[0], -1.0), 1.0));
			fRec308[0] = fTemp3 * (0.27419354838709675 * fTemp7 + 0.07518210197710717 * fTemp9 + 0.41750899228506316 * fTemp10 + 0.3495844377902277 * fTemp11 + 0.5236349380886428 * fTemp12 + 0.09132720885837509 * fTemp13 + 0.17431375863888893 * fTemp14 + 0.47320499479708633 * fTemp15 + 0.15036420395421435 * fTemp16 + 0.020614447316303578 * fTemp17 + 0.6176487865462723 * fTemp18 + 0.08245778926521431 * fTemp19 + 0.005652348457696141 * fTemp20 + 0.7224870224932622 * fTemp21 + 0.04521878766156913 * fTemp22 + 0.0015498374803360386 * fTemp23 + 0.7985792905193032 * fTemp24 + 0.024797399685376618 * fTemp25 + 0.006532882847678417 * fTemp26 + 0.8505158407444824 * fTemp27 + 0.03832547188620372 * fTemp28 + 0.6878989713592297 * fTemp29) + fTemp2 * fRec308[1];
			fRec307[0] = fConst0 * fTemp30 * fRec308[0] * fTemp3 + fTemp2 * fRec307[1];
			int iTemp453 = int(fRec307[0]);
			double fTemp454 = ((fRec303[1] != 0.0) ? (((fRec304[1] > 0.0) & (fRec304[1] < 1.0)) ? fRec303[1] : 0.0) : (((fRec304[1] == 0.0) & (iTemp453 != iRec305[1])) ? 0.00048828125 : (((fRec304[1] == 1.0) & (iTemp453 != iRec306[1])) ? -0.00048828125 : 0.0)));
			fRec303[0] = fTemp454;
			fRec304[0] = std::max<double>(0.0, std::min<double>(1.0, fRec304[1] + fTemp454));
			iRec305[0] = (((fRec304[1] >= 1.0) & (iRec306[1] != iTemp453)) ? iTemp453 : iRec305[1]);
			iRec306[0] = (((fRec304[1] <= 0.0) & (iRec305[1] != iTemp453)) ? iTemp453 : iRec306[1]);
			double fTemp455 = fRec17[0] * fRec308[0] * fTemp35 * (fTemp37 * (fTemp38 * (4.069010416666667e-06 * fTemp39 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec308[0] * fTemp43) + 1.0) + 8.138020833333335e-06 * fTemp44 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec308[0] * fTemp45) + 1.0)) + 8.138020833333335e-06 * fTemp46 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec308[0] * fTemp47) + 1.0)) + 4.069010416666667e-06 * fTemp48 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec308[0] * fTemp49) + 1.0));
			fVec114[IOTA0 & 262143] = fTemp455;
			double fTemp456 = fVec114[(IOTA0 - int(std::min<double>(fConst0, double(std::max<int>(0, iRec305[0]))))) & 262143];
			double fTemp457 = fRec304[0] * (fTemp456 - fVec114[(IOTA0 - int(std::min<double>(fConst0, double(std::max<int>(0, iRec306[0]))))) & 262143]) - fTemp456;
			fVec115[IOTA0 & 131071] = fTemp457;
			fRec309[0] = std::fmod(fRec309[1] + (10001.0 - std::pow(2.0, 0.08333333333333333 * fTemp67 * fRec308[0])), 1e+04);
			int iTemp458 = int(fRec309[0]);
			double fTemp459 = std::floor(fRec309[0]);
			double fTemp460 = std::min<double>(0.0001 * fRec309[0], 1.0);
			double fTemp461 = fRec309[0] + 1e+04;
			int iTemp462 = int(fTemp461);
			double fTemp463 = std::floor(fTemp461);
			double fTemp464 = (fVec115[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp458))) & 131071] * (fTemp459 + (1.0 - fRec309[0])) + (fRec309[0] - fTemp459) * fVec115[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp458 + 1))) & 131071]) * fTemp460 + (fVec115[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp462))) & 131071] * (fTemp463 + (-9999.0 - fRec309[0])) + (fRec309[0] + (1e+04 - fTemp463)) * fVec115[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp462 + 1))) & 131071]) * (1.0 - fTemp460);
			fVec116[0] = fTemp464;
			fRec302[0] = 0.995 * fRec302[1] + fTemp464 - fVec116[1];
			fRec316[0] = fTemp3 * (0.7741935483870968 * fTemp7 + 0.5993756503642039 * fTemp9 + 0.9377521321470804 * fTemp10 + 0.8271634031377844 * fTemp11 + 0.8798826901281197 * fTemp12 + 0.6526947471551797 * fTemp13 + 0.8793790613463954 * fTemp14 + 0.9490114464099896 * fTemp15 + 0.8980228928199792 * fTemp16 + 0.4640327615722869 * fTemp17 + 0.9884864556409654 * fTemp18 + 0.9539458225638615 * fTemp19 + 0.3592511702495124 * fTemp20 + 0.9974001674027987 * fTemp21 + 0.9792013392223891 * fTemp22 + 0.278129938257687 * fTemp23 + 0.9994129410264384 * fTemp24 + 0.9906070564230144 * fTemp25 + 0.20905225112570935 * fTemp26 + 0.995328620023265 * fTemp27 + 0.36705106869843274 * fTemp28 + 0.9741721851962257 * fTemp29) + fTemp2 * fRec316[1];
			fRec315[0] = fConst0 * fTemp30 * fRec316[0] * fTemp3 + fTemp2 * fRec315[1];
			int iTemp465 = int(fRec315[0]);
			double fTemp466 = ((fRec311[1] != 0.0) ? (((fRec312[1] > 0.0) & (fRec312[1] < 1.0)) ? fRec311[1] : 0.0) : (((fRec312[1] == 0.0) & (iTemp465 != iRec313[1])) ? 0.00048828125 : (((fRec312[1] == 1.0) & (iTemp465 != iRec314[1])) ? -0.00048828125 : 0.0)));
			fRec311[0] = fTemp466;
			fRec312[0] = std::max<double>(0.0, std::min<double>(1.0, fRec312[1] + fTemp466));
			iRec313[0] = (((fRec312[1] >= 1.0) & (iRec314[1] != iTemp465)) ? iTemp465 : iRec313[1]);
			iRec314[0] = (((fRec312[1] <= 0.0) & (iRec313[1] != iTemp465)) ? iTemp465 : iRec314[1]);
			double fTemp467 = fRec17[0] * fRec316[0] * fTemp35 * (fTemp37 * (fTemp38 * (4.069010416666667e-06 * fTemp39 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec316[0] * fTemp43) + 1.0) + 8.138020833333335e-06 * fTemp44 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec316[0] * fTemp45) + 1.0)) + 8.138020833333335e-06 * fTemp46 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec316[0] * fTemp47) + 1.0)) + 4.069010416666667e-06 * fTemp48 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec316[0] * fTemp49) + 1.0));
			fVec117[IOTA0 & 262143] = fTemp467;
			double fTemp468 = fVec117[(IOTA0 - int(std::min<double>(fConst0, double(std::max<int>(0, iRec313[0]))))) & 262143];
			double fTemp469 = fRec312[0] * (fTemp468 - fVec117[(IOTA0 - int(std::min<double>(fConst0, double(std::max<int>(0, iRec314[0]))))) & 262143]) - fTemp468;
			fVec118[IOTA0 & 131071] = fTemp469;
			fRec317[0] = std::fmod(fRec317[1] + (10001.0 - std::pow(2.0, 0.08333333333333333 * fTemp67 * fRec316[0])), 1e+04);
			int iTemp470 = int(fRec317[0]);
			double fTemp471 = std::floor(fRec317[0]);
			double fTemp472 = std::min<double>(0.0001 * fRec317[0], 1.0);
			double fTemp473 = fRec317[0] + 1e+04;
			int iTemp474 = int(fTemp473);
			double fTemp475 = std::floor(fTemp473);
			double fTemp476 = (fVec118[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp470))) & 131071] * (fTemp471 + (1.0 - fRec317[0])) + (fRec317[0] - fTemp471) * fVec118[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp470 + 1))) & 131071]) * fTemp472 + (fVec118[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp474))) & 131071] * (fTemp475 + (-9999.0 - fRec317[0])) + (fRec317[0] + (1e+04 - fTemp475)) * fVec118[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp474 + 1))) & 131071]) * (1.0 - fTemp472);
			fVec119[0] = fTemp476;
			fRec310[0] = 0.995 * fRec310[1] + fTemp476 - fVec119[1];
			double fTemp477 = std::min<double>(std::max<double>(fRec302[0], -1.0), 1.0) + std::min<double>(std::max<double>(fRec310[0], -1.0), 1.0);
			fVec120[0] = fTemp477;
			fRec301[0] = 0.995 * fRec301[1] + 0.125 * (fTemp477 - fVec120[1]);
			output16[i0] = FAUSTFLOAT(std::min<double>(std::max<double>(fRec301[0], -1.0), 1.0));
			fRec325[0] = fTemp3 * (0.2903225806451613 * fTemp7 + 0.08428720083246619 * fTemp9 + 0.4403941515576343 * fTemp10 + 0.3677317845004871 * fTemp11 + 0.5388159060803247 * fTemp12 + 0.10219546042925831 * fTemp13 + 0.19394700872616855 * fTemp14 + 0.4963579604578563 * fTemp15 + 0.16857440166493237 * fTemp16 + 0.024470477661038572 * fTemp17 + 0.6425766170991238 * fTemp18 + 0.09788191064415429 * fTemp19 + 0.007104332224172489 * fTemp20 + 0.7463446960058298 * fTemp21 + 0.056834657793379914 * fTemp22 + 0.0020625480650823358 * fTemp23 + 0.8199865584557502 * fTemp24 + 0.03300076904131737 * fTemp25 + 0.007305637100123805 * fTemp26 + 0.8663275376786166 * fTemp27 + 0.04307116295539837 * fTemp28 + 0.7045267634787598 * fTemp29) + fTemp2 * fRec325[1];
			fRec324[0] = fConst0 * fTemp30 * fRec325[0] * fTemp3 + fTemp2 * fRec324[1];
			int iTemp478 = int(fRec324[0]);
			double fTemp479 = ((fRec320[1] != 0.0) ? (((fRec321[1] > 0.0) & (fRec321[1] < 1.0)) ? fRec320[1] : 0.0) : (((fRec321[1] == 0.0) & (iTemp478 != iRec322[1])) ? 0.00048828125 : (((fRec321[1] == 1.0) & (iTemp478 != iRec323[1])) ? -0.00048828125 : 0.0)));
			fRec320[0] = fTemp479;
			fRec321[0] = std::max<double>(0.0, std::min<double>(1.0, fRec321[1] + fTemp479));
			iRec322[0] = (((fRec321[1] >= 1.0) & (iRec323[1] != iTemp478)) ? iTemp478 : iRec322[1]);
			iRec323[0] = (((fRec321[1] <= 0.0) & (iRec322[1] != iTemp478)) ? iTemp478 : iRec323[1]);
			double fTemp480 = fRec17[0] * fRec325[0] * fTemp35 * (fTemp37 * (fTemp38 * (4.069010416666667e-06 * fTemp39 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec325[0] * fTemp43) + 1.0) + 8.138020833333335e-06 * fTemp44 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec325[0] * fTemp45) + 1.0)) + 8.138020833333335e-06 * fTemp46 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec325[0] * fTemp47) + 1.0)) + 4.069010416666667e-06 * fTemp48 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec325[0] * fTemp49) + 1.0));
			fVec121[IOTA0 & 262143] = fTemp480;
			double fTemp481 = fVec121[(IOTA0 - int(std::min<double>(fConst0, double(std::max<int>(0, iRec322[0]))))) & 262143];
			double fTemp482 = fRec321[0] * (fTemp481 - fVec121[(IOTA0 - int(std::min<double>(fConst0, double(std::max<int>(0, iRec323[0]))))) & 262143]) - fTemp481;
			fVec122[IOTA0 & 131071] = fTemp482;
			fRec326[0] = std::fmod(fRec326[1] + (10001.0 - std::pow(2.0, 0.08333333333333333 * fTemp67 * fRec325[0])), 1e+04);
			int iTemp483 = int(fRec326[0]);
			double fTemp484 = std::floor(fRec326[0]);
			double fTemp485 = std::min<double>(0.0001 * fRec326[0], 1.0);
			double fTemp486 = fRec326[0] + 1e+04;
			int iTemp487 = int(fTemp486);
			double fTemp488 = std::floor(fTemp486);
			double fTemp489 = (fVec122[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp483))) & 131071] * (fTemp484 + (1.0 - fRec326[0])) + (fRec326[0] - fTemp484) * fVec122[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp483 + 1))) & 131071]) * fTemp485 + (fVec122[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp487))) & 131071] * (fTemp488 + (-9999.0 - fRec326[0])) + (fRec326[0] + (1e+04 - fTemp488)) * fVec122[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp487 + 1))) & 131071]) * (1.0 - fTemp485);
			fVec123[0] = fTemp489;
			fRec319[0] = 0.995 * fRec319[1] + fTemp489 - fVec123[1];
			fRec333[0] = fTemp3 * (0.7903225806451613 * fTemp7 + 0.6246097814776275 * fTemp9 + 0.9462493690718368 * fTemp10 + 0.8402195559632306 * fTemp11 + 0.8890008890013334 * fTemp12 + 0.6765620128507618 * fTemp13 + 0.8953878684688492 * fTemp14 + 0.956035379812695 * fTemp15 + 0.9120707596253902 * fTemp16 + 0.49364321439360875 * fTemp17 + 0.9907816118962103 * fTemp18 + 0.963126447584841 * fTemp19 + 0.3901373791175295 * fTemp20 + 0.998067112171786 * fTemp21 + 0.9845368973742882 * fTemp22 + 0.3083343802703055 * fTemp23 + 0.9995947170682777 * fTemp24 + 0.9935154730924435 * fTemp25 + 0.23378038720396177 * fTemp26 + 0.9958227355524568 * fTemp27 + 0.38730903505733616 * fTemp28 + 0.9777706171759791 * fTemp29) + fTemp2 * fRec333[1];
			fRec332[0] = fConst0 * fTemp30 * fRec333[0] * fTemp3 + fTemp2 * fRec332[1];
			int iTemp490 = int(fRec332[0]);
			double fTemp491 = ((fRec328[1] != 0.0) ? (((fRec329[1] > 0.0) & (fRec329[1] < 1.0)) ? fRec328[1] : 0.0) : (((fRec329[1] == 0.0) & (iTemp490 != iRec330[1])) ? 0.00048828125 : (((fRec329[1] == 1.0) & (iTemp490 != iRec331[1])) ? -0.00048828125 : 0.0)));
			fRec328[0] = fTemp491;
			fRec329[0] = std::max<double>(0.0, std::min<double>(1.0, fRec329[1] + fTemp491));
			iRec330[0] = (((fRec329[1] >= 1.0) & (iRec331[1] != iTemp490)) ? iTemp490 : iRec330[1]);
			iRec331[0] = (((fRec329[1] <= 0.0) & (iRec330[1] != iTemp490)) ? iTemp490 : iRec331[1]);
			double fTemp492 = fRec17[0] * fRec333[0] * fTemp35 * (fTemp37 * (fTemp38 * (4.069010416666667e-06 * fTemp39 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec333[0] * fTemp43) + 1.0) + 8.138020833333335e-06 * fTemp44 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec333[0] * fTemp45) + 1.0)) + 8.138020833333335e-06 * fTemp46 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec333[0] * fTemp47) + 1.0)) + 4.069010416666667e-06 * fTemp48 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec333[0] * fTemp49) + 1.0));
			fVec124[IOTA0 & 262143] = fTemp492;
			double fTemp493 = fVec124[(IOTA0 - int(std::min<double>(fConst0, double(std::max<int>(0, iRec330[0]))))) & 262143];
			double fTemp494 = fRec329[0] * (fTemp493 - fVec124[(IOTA0 - int(std::min<double>(fConst0, double(std::max<int>(0, iRec331[0]))))) & 262143]) - fTemp493;
			fVec125[IOTA0 & 131071] = fTemp494;
			fRec334[0] = std::fmod(fRec334[1] + (10001.0 - std::pow(2.0, 0.08333333333333333 * fTemp67 * fRec333[0])), 1e+04);
			int iTemp495 = int(fRec334[0]);
			double fTemp496 = std::floor(fRec334[0]);
			double fTemp497 = std::min<double>(0.0001 * fRec334[0], 1.0);
			double fTemp498 = fRec334[0] + 1e+04;
			int iTemp499 = int(fTemp498);
			double fTemp500 = std::floor(fTemp498);
			double fTemp501 = (fVec125[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp495))) & 131071] * (fTemp496 + (1.0 - fRec334[0])) + (fRec334[0] - fTemp496) * fVec125[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp495 + 1))) & 131071]) * fTemp497 + (fVec125[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp499))) & 131071] * (fTemp500 + (-9999.0 - fRec334[0])) + (fRec334[0] + (1e+04 - fTemp500)) * fVec125[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp499 + 1))) & 131071]) * (1.0 - fTemp497);
			fVec126[0] = fTemp501;
			fRec327[0] = 0.995 * fRec327[1] + fTemp501 - fVec126[1];
			double fTemp502 = std::min<double>(std::max<double>(fRec319[0], -1.0), 1.0) + std::min<double>(std::max<double>(fRec327[0], -1.0), 1.0);
			fVec127[0] = fTemp502;
			fRec318[0] = 0.995 * fRec318[1] + 0.125 * (fTemp502 - fVec127[1]);
			output17[i0] = FAUSTFLOAT(std::min<double>(std::max<double>(fRec318[0], -1.0), 1.0));
			fRec342[0] = fTemp3 * (0.3064516129032258 * fTemp7 + 0.09391259105098855 * fTemp9 + 0.46299664410512076 * fTemp10 + 0.3856536924977495 * fTemp11 + 0.5535807194106618 * fTemp12 + 0.11363996731159176 * fTemp13 + 0.21436589245260385 * fTemp14 + 0.5189906347554629 * fTemp15 + 0.1878251821019771 * fTemp16 + 0.028779664999496492 * fTemp17 + 0.6663967305562082 * fTemp18 + 0.11511865999798597 * fTemp19 + 0.008819574757910216 * fTemp20 + 0.7686299905470476 * fTemp21 + 0.07055659806328173 * fTemp22 + 0.002702772909682163 * fTemp23 + 0.8395337031213395 * fTemp24 + 0.04324436655491461 * fTemp25 + 0.00816979803298818 * fTemp26 + 0.8804667513129681 * fTemp27 + 0.048113762601322496 * fTemp28 + 0.7204100462621706 * fTemp29) + fTemp2 * fRec342[1];
			fRec341[0] = fConst0 * fTemp30 * fRec342[0] * fTemp3 + fTemp2 * fRec341[1];
			int iTemp503 = int(fRec341[0]);
			double fTemp504 = ((fRec337[1] != 0.0) ? (((fRec338[1] > 0.0) & (fRec338[1] < 1.0)) ? fRec337[1] : 0.0) : (((fRec338[1] == 0.0) & (iTemp503 != iRec339[1])) ? 0.00048828125 : (((fRec338[1] == 1.0) & (iTemp503 != iRec340[1])) ? -0.00048828125 : 0.0)));
			fRec337[0] = fTemp504;
			fRec338[0] = std::max<double>(0.0, std::min<double>(1.0, fRec338[1] + fTemp504));
			iRec339[0] = (((fRec338[1] >= 1.0) & (iRec340[1] != iTemp503)) ? iTemp503 : iRec339[1]);
			iRec340[0] = (((fRec338[1] <= 0.0) & (iRec339[1] != iTemp503)) ? iTemp503 : iRec340[1]);
			double fTemp505 = fRec17[0] * fRec342[0] * fTemp35 * (fTemp37 * (fTemp38 * (4.069010416666667e-06 * fTemp39 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec342[0] * fTemp43) + 1.0) + 8.138020833333335e-06 * fTemp44 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec342[0] * fTemp45) + 1.0)) + 8.138020833333335e-06 * fTemp46 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec342[0] * fTemp47) + 1.0)) + 4.069010416666667e-06 * fTemp48 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec342[0] * fTemp49) + 1.0));
			fVec128[IOTA0 & 262143] = fTemp505;
			double fTemp506 = fVec128[(IOTA0 - int(std::min<double>(fConst0, double(std::max<int>(0, iRec339[0]))))) & 262143];
			double fTemp507 = fRec338[0] * (fTemp506 - fVec128[(IOTA0 - int(std::min<double>(fConst0, double(std::max<int>(0, iRec340[0]))))) & 262143]) - fTemp506;
			fVec129[IOTA0 & 131071] = fTemp507;
			fRec343[0] = std::fmod(fRec343[1] + (10001.0 - std::pow(2.0, 0.08333333333333333 * fTemp67 * fRec342[0])), 1e+04);
			int iTemp508 = int(fRec343[0]);
			double fTemp509 = std::floor(fRec343[0]);
			double fTemp510 = std::min<double>(0.0001 * fRec343[0], 1.0);
			double fTemp511 = fRec343[0] + 1e+04;
			int iTemp512 = int(fTemp511);
			double fTemp513 = std::floor(fTemp511);
			double fTemp514 = (fVec129[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp508))) & 131071] * (fTemp509 + (1.0 - fRec343[0])) + (fRec343[0] - fTemp509) * fVec129[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp508 + 1))) & 131071]) * fTemp510 + (fVec129[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp512))) & 131071] * (fTemp513 + (-9999.0 - fRec343[0])) + (fRec343[0] + (1e+04 - fTemp513)) * fVec129[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp512 + 1))) & 131071]) * (1.0 - fTemp510);
			fVec130[0] = fTemp514;
			fRec336[0] = 0.995 * fRec336[1] + fTemp514 - fVec130[1];
			fRec350[0] = fTemp3 * (0.8064516129032258 * fTemp7 + 0.6503642039542142 * fTemp9 + 0.9541392564000488 * fTemp10 + 0.8531586116707288 * fTemp11 + 0.8980265101338745 * fTemp12 + 0.700636877026642 * fTemp13 + 0.9103817206036382 * fTemp14 + 0.9625390218522373 * fTemp15 + 0.9250780437044744 * fTemp16 + 0.5244872612533986 * fTemp17 + 0.9927494881004331 * fTemp18 + 0.970997952401732 * fTemp19 + 0.4229735977849988 * fTemp20 + 0.9985966751162129 * fTemp21 + 0.9887734009297027 * fTemp22 + 0.3411077401491926 * fTemp23 + 0.9997283887321702 * fTemp24 + 0.9956542197147237 * fTemp25 + 0.2614335370556218 * fTemp26 + 0.9962645859785303 * fTemp27 + 0.40869991032827857 * fTemp28 + 0.9810907306932612 * fTemp29) + fTemp2 * fRec350[1];
			fRec349[0] = fConst0 * fTemp30 * fRec350[0] * fTemp3 + fTemp2 * fRec349[1];
			int iTemp515 = int(fRec349[0]);
			double fTemp516 = ((fRec345[1] != 0.0) ? (((fRec346[1] > 0.0) & (fRec346[1] < 1.0)) ? fRec345[1] : 0.0) : (((fRec346[1] == 0.0) & (iTemp515 != iRec347[1])) ? 0.00048828125 : (((fRec346[1] == 1.0) & (iTemp515 != iRec348[1])) ? -0.00048828125 : 0.0)));
			fRec345[0] = fTemp516;
			fRec346[0] = std::max<double>(0.0, std::min<double>(1.0, fRec346[1] + fTemp516));
			iRec347[0] = (((fRec346[1] >= 1.0) & (iRec348[1] != iTemp515)) ? iTemp515 : iRec347[1]);
			iRec348[0] = (((fRec346[1] <= 0.0) & (iRec347[1] != iTemp515)) ? iTemp515 : iRec348[1]);
			double fTemp517 = fRec17[0] * fRec350[0] * fTemp35 * (fTemp37 * (fTemp38 * (4.069010416666667e-06 * fTemp39 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec350[0] * fTemp43) + 1.0) + 8.138020833333335e-06 * fTemp44 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec350[0] * fTemp45) + 1.0)) + 8.138020833333335e-06 * fTemp46 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec350[0] * fTemp47) + 1.0)) + 4.069010416666667e-06 * fTemp48 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec350[0] * fTemp49) + 1.0));
			fVec131[IOTA0 & 262143] = fTemp517;
			double fTemp518 = fVec131[(IOTA0 - int(std::min<double>(fConst0, double(std::max<int>(0, iRec347[0]))))) & 262143];
			double fTemp519 = fRec346[0] * (fTemp518 - fVec131[(IOTA0 - int(std::min<double>(fConst0, double(std::max<int>(0, iRec348[0]))))) & 262143]) - fTemp518;
			fVec132[IOTA0 & 131071] = fTemp519;
			fRec351[0] = std::fmod(fRec351[1] + (10001.0 - std::pow(2.0, 0.08333333333333333 * fTemp67 * fRec350[0])), 1e+04);
			double fTemp520 = fRec351[0] + 1e+04;
			int iTemp521 = int(fTemp520);
			double fTemp522 = std::floor(fTemp520);
			double fTemp523 = std::min<double>(0.0001 * fRec351[0], 1.0);
			double fTemp524 = (fVec132[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp521))) & 131071] * (fTemp522 + (-9999.0 - fRec351[0])) + (fRec351[0] + (1e+04 - fTemp522)) * fVec132[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp521 + 1))) & 131071]) * (1.0 - fTemp523);
			int iTemp525 = int(fRec351[0]);
			double fTemp526 = std::floor(fRec351[0]);
			double fTemp527 = (fVec132[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp525))) & 131071] * (fTemp526 + (1.0 - fRec351[0])) + (fRec351[0] - fTemp526) * fVec132[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp525 + 1))) & 131071]) * fTemp523;
			fVec133[0] = fTemp527 + fTemp524;
			fRec344[0] = fTemp524 + 0.995 * fRec344[1] + fTemp527 - fVec133[1];
			double fTemp528 = std::min<double>(std::max<double>(fRec336[0], -1.0), 1.0) + std::min<double>(std::max<double>(fRec344[0], -1.0), 1.0);
			fVec134[0] = fTemp528;
			fRec335[0] = 0.995 * fRec335[1] + 0.125 * (fTemp528 - fVec134[1]);
			output18[i0] = FAUSTFLOAT(std::min<double>(std::max<double>(fRec335[0], -1.0), 1.0));
			fRec359[0] = fTemp3 * (0.3225806451612903 * fTemp7 + 0.1040582726326743 * fTemp9 + 0.48530196253108104 * fTemp10 + 0.40335569423120843 * fTemp11 + 0.5679618342470648 * fTemp12 + 0.1256533838554179 * fTemp13 + 0.2355179948365188 * fTemp14 + 0.5411030176899063 * fTemp15 + 0.2081165452653486 * fTemp16 + 0.03356718472021751 * fTemp17 + 0.6891343023060654 * fTemp18 + 0.13426873888087004 * fTemp19 + 0.010828124103295972 * fTemp20 + 0.7894135596266896 * fTemp21 + 0.08662499282636778 * fTemp22 + 0.003492943259127733 * fTemp23 + 0.8573446694245316 * fTemp24 + 0.055887092146043725 * fTemp25 + 0.009136177856231928 * fTemp26 + 0.8931103886803308 * fTemp27 + 0.0534580160566962 * fTemp28 + 0.7355970484510568 * fTemp29) + fTemp2 * fRec359[1];
			fRec358[0] = fConst0 * fTemp30 * fRec359[0] * fTemp3 + fTemp2 * fRec358[1];
			int iTemp529 = int(fRec358[0]);
			double fTemp530 = ((fRec354[1] != 0.0) ? (((fRec355[1] > 0.0) & (fRec355[1] < 1.0)) ? fRec354[1] : 0.0) : (((fRec355[1] == 0.0) & (iTemp529 != iRec356[1])) ? 0.00048828125 : (((fRec355[1] == 1.0) & (iTemp529 != iRec357[1])) ? -0.00048828125 : 0.0)));
			fRec354[0] = fTemp530;
			fRec355[0] = std::max<double>(0.0, std::min<double>(1.0, fRec355[1] + fTemp530));
			iRec356[0] = (((fRec355[1] >= 1.0) & (iRec357[1] != iTemp529)) ? iTemp529 : iRec356[1]);
			iRec357[0] = (((fRec355[1] <= 0.0) & (iRec356[1] != iTemp529)) ? iTemp529 : iRec357[1]);
			double fTemp531 = fRec17[0] * fRec359[0] * fTemp35 * (fTemp37 * (fTemp38 * (4.069010416666667e-06 * fTemp39 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec359[0] * fTemp43) + 1.0) + 8.138020833333335e-06 * fTemp44 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec359[0] * fTemp45) + 1.0)) + 8.138020833333335e-06 * fTemp46 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec359[0] * fTemp47) + 1.0)) + 4.069010416666667e-06 * fTemp48 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec359[0] * fTemp49) + 1.0));
			fVec135[IOTA0 & 262143] = fTemp531;
			double fTemp532 = fVec135[(IOTA0 - int(std::min<double>(fConst0, double(std::max<int>(0, iRec356[0]))))) & 262143];
			double fTemp533 = fRec355[0] * (fTemp532 - fVec135[(IOTA0 - int(std::min<double>(fConst0, double(std::max<int>(0, iRec357[0]))))) & 262143]) - fTemp532;
			fVec136[IOTA0 & 131071] = fTemp533;
			fRec360[0] = std::fmod(fRec360[1] + (10001.0 - std::pow(2.0, 0.08333333333333333 * fTemp67 * fRec359[0])), 1e+04);
			int iTemp534 = int(fRec360[0]);
			double fTemp535 = std::floor(fRec360[0]);
			double fTemp536 = std::min<double>(0.0001 * fRec360[0], 1.0);
			double fTemp537 = fRec360[0] + 1e+04;
			int iTemp538 = int(fTemp537);
			double fTemp539 = std::floor(fTemp537);
			double fTemp540 = (fVec136[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp534))) & 131071] * (fTemp535 + (1.0 - fRec360[0])) + (fRec360[0] - fTemp535) * fVec136[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp534 + 1))) & 131071]) * fTemp536 + (fVec136[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp538))) & 131071] * (fTemp539 + (-9999.0 - fRec360[0])) + (fRec360[0] + (1e+04 - fTemp539)) * fVec136[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp538 + 1))) & 131071]) * (1.0 - fTemp536);
			fVec137[0] = fTemp540;
			fRec353[0] = 0.995 * fRec353[1] + fTemp540 - fVec137[1];
			fRec367[0] = fTemp3 * (0.8225806451612904 * fTemp7 + 0.6766389177939647 * fTemp9 + 0.9614167300122124 * fTemp10 + 0.8659826520283126 * fTemp11 + 0.9069623173877128 * fTemp12 + 0.7249038872455218 * fTemp13 + 0.9243221287473754 * fTemp14 + 0.9685223725286161 * fTemp15 + 0.9370447450572321 * fTemp16 + 0.5565900775401968 * fTemp17 + 0.9944152596421738 * fTemp18 + 0.9776610385686952 * fTemp19 + 0.45784022507338773 * fTemp20 + 0.9990091589687727 * fTemp21 + 0.9920732717501822 * fTemp22 + 0.37661050772165766 * fTemp23 + 0.999824205623492 * fTemp24 + 0.9971872899758711 * fTemp25 + 0.29235769139942186 * fTemp26 + 0.9966596996462603 * fTemp27 + 0.4313515301998647 * fTemp28 + 0.9841353425868904 * fTemp29) + fTemp2 * fRec367[1];
			fRec366[0] = fConst0 * fTemp30 * fRec367[0] * fTemp3 + fTemp2 * fRec366[1];
			int iTemp541 = int(fRec366[0]);
			double fTemp542 = ((fRec362[1] != 0.0) ? (((fRec363[1] > 0.0) & (fRec363[1] < 1.0)) ? fRec362[1] : 0.0) : (((fRec363[1] == 0.0) & (iTemp541 != iRec364[1])) ? 0.00048828125 : (((fRec363[1] == 1.0) & (iTemp541 != iRec365[1])) ? -0.00048828125 : 0.0)));
			fRec362[0] = fTemp542;
			fRec363[0] = std::max<double>(0.0, std::min<double>(1.0, fRec363[1] + fTemp542));
			iRec364[0] = (((fRec363[1] >= 1.0) & (iRec365[1] != iTemp541)) ? iTemp541 : iRec364[1]);
			iRec365[0] = (((fRec363[1] <= 0.0) & (iRec364[1] != iTemp541)) ? iTemp541 : iRec365[1]);
			double fTemp543 = fRec17[0] * fRec367[0] * fTemp35 * (fTemp37 * (fTemp38 * (4.069010416666667e-06 * fTemp39 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec367[0] * fTemp43) + 1.0) + 8.138020833333335e-06 * fTemp44 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec367[0] * fTemp45) + 1.0)) + 8.138020833333335e-06 * fTemp46 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec367[0] * fTemp47) + 1.0)) + 4.069010416666667e-06 * fTemp48 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec367[0] * fTemp49) + 1.0));
			fVec138[IOTA0 & 262143] = fTemp543;
			double fTemp544 = fVec138[(IOTA0 - int(std::min<double>(fConst0, double(std::max<int>(0, iRec364[0]))))) & 262143];
			double fTemp545 = fRec363[0] * (fTemp544 - fVec138[(IOTA0 - int(std::min<double>(fConst0, double(std::max<int>(0, iRec365[0]))))) & 262143]) - fTemp544;
			fVec139[IOTA0 & 131071] = fTemp545;
			fRec368[0] = std::fmod(fRec368[1] + (10001.0 - std::pow(2.0, 0.08333333333333333 * fTemp67 * fRec367[0])), 1e+04);
			int iTemp546 = int(fRec368[0]);
			double fTemp547 = std::floor(fRec368[0]);
			double fTemp548 = std::min<double>(0.0001 * fRec368[0], 1.0);
			double fTemp549 = fRec368[0] + 1e+04;
			int iTemp550 = int(fTemp549);
			double fTemp551 = std::floor(fTemp549);
			double fTemp552 = (fVec139[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp546))) & 131071] * (fTemp547 + (1.0 - fRec368[0])) + (fRec368[0] - fTemp547) * fVec139[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp546 + 1))) & 131071]) * fTemp548 + (fVec139[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp550))) & 131071] * (fTemp551 + (-9999.0 - fRec368[0])) + (fRec368[0] + (1e+04 - fTemp551)) * fVec139[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp550 + 1))) & 131071]) * (1.0 - fTemp548);
			fVec140[0] = fTemp552;
			fRec361[0] = 0.995 * fRec361[1] + fTemp552 - fVec140[1];
			double fTemp553 = std::min<double>(std::max<double>(fRec353[0], -1.0), 1.0) + std::min<double>(std::max<double>(fRec361[0], -1.0), 1.0);
			fVec141[0] = fTemp553;
			fRec352[0] = 0.995 * fRec352[1] + 0.125 * (fTemp553 - fVec141[1]);
			output19[i0] = FAUSTFLOAT(std::min<double>(std::max<double>(fRec352[0], -1.0), 1.0));
			fRec376[0] = fTemp3 * (0.3387096774193548 * fTemp7 + 0.11472424557752339 * fTemp9 + 0.5072957901801074 * fTemp10 + 0.4208431209600495 * fTemp11 + 0.5819876952473779 * fTemp12 + 0.13822799925645035 * fTemp13 + 0.2573490187344595 * fTemp14 + 0.5626951092611862 * fTemp15 + 0.22944849115504679 * fTemp16 + 0.038858212211741794 * fTemp17 + 0.710814507737236 * fTemp18 + 0.15543284884696718 * fTemp19 + 0.013161652523331896 * fTemp20 + 0.8087644325359141 * fTemp21 + 0.10529322018665517 * fTemp22 + 0.004457979080483384 * fTemp23 + 0.8735377699027819 * fTemp24 + 0.07132766528773414 * fTemp25 + 0.010216867722270092 * fTemp26 + 0.9044166444602831 * fTemp27 + 0.05910906348159739 * fTemp28 + 0.7501300615634505 * fTemp29) + fTemp2 * fRec376[1];
			fRec375[0] = fConst0 * fTemp30 * fRec376[0] * fTemp3 + fTemp2 * fRec375[1];
			int iTemp554 = int(fRec375[0]);
			double fTemp555 = ((fRec371[1] != 0.0) ? (((fRec372[1] > 0.0) & (fRec372[1] < 1.0)) ? fRec371[1] : 0.0) : (((fRec372[1] == 0.0) & (iTemp554 != iRec373[1])) ? 0.00048828125 : (((fRec372[1] == 1.0) & (iTemp554 != iRec374[1])) ? -0.00048828125 : 0.0)));
			fRec371[0] = fTemp555;
			fRec372[0] = std::max<double>(0.0, std::min<double>(1.0, fRec372[1] + fTemp555));
			iRec373[0] = (((fRec372[1] >= 1.0) & (iRec374[1] != iTemp554)) ? iTemp554 : iRec373[1]);
			iRec374[0] = (((fRec372[1] <= 0.0) & (iRec373[1] != iTemp554)) ? iTemp554 : iRec374[1]);
			double fTemp556 = fRec17[0] * fRec376[0] * fTemp35 * (fTemp37 * (fTemp38 * (4.069010416666667e-06 * fTemp39 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec376[0] * fTemp43) + 1.0) + 8.138020833333335e-06 * fTemp44 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec376[0] * fTemp45) + 1.0)) + 8.138020833333335e-06 * fTemp46 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec376[0] * fTemp47) + 1.0)) + 4.069010416666667e-06 * fTemp48 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec376[0] * fTemp49) + 1.0));
			fVec142[IOTA0 & 262143] = fTemp556;
			double fTemp557 = fVec142[(IOTA0 - int(std::min<double>(fConst0, double(std::max<int>(0, iRec373[0]))))) & 262143];
			double fTemp558 = fRec372[0] * (fTemp557 - fVec142[(IOTA0 - int(std::min<double>(fConst0, double(std::max<int>(0, iRec374[0]))))) & 262143]) - fTemp557;
			fVec143[IOTA0 & 131071] = fTemp558;
			fRec377[0] = std::fmod(fRec377[1] + (10001.0 - std::pow(2.0, 0.08333333333333333 * fTemp67 * fRec376[0])), 1e+04);
			int iTemp559 = int(fRec377[0]);
			double fTemp560 = std::floor(fRec377[0]);
			double fTemp561 = std::min<double>(0.0001 * fRec377[0], 1.0);
			double fTemp562 = fRec377[0] + 1e+04;
			int iTemp563 = int(fTemp562);
			double fTemp564 = std::floor(fTemp562);
			double fTemp565 = (fVec143[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp559))) & 131071] * (fTemp560 + (1.0 - fRec377[0])) + (fRec377[0] - fTemp560) * fVec143[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp559 + 1))) & 131071]) * fTemp561 + (fVec143[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp563))) & 131071] * (fTemp564 + (-9999.0 - fRec377[0])) + (fRec377[0] + (1e+04 - fTemp564)) * fVec143[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp563 + 1))) & 131071]) * (1.0 - fTemp561);
			fVec144[0] = fTemp565;
			fRec370[0] = 0.995 * fRec370[1] + fTemp565 - fVec144[1];
			fRec384[0] = fTemp3 * (0.8387096774193549 * fTemp7 + 0.7034339229968783 * fTemp9 + 0.9680771188662043 * fTemp10 + 0.8786937037778666 * fTemp11 + 0.915810939779251 * fTemp12 + 0.7493474677412795 * fTemp13 + 0.937173308072291 * fTemp14 + 0.9739854318418314 * fTemp15 + 0.9479708636836629 * fTemp16 + 0.5899768386425431 * fTemp17 + 0.9958041019099728 * fTemp18 + 0.9832164076398913 * fTemp19 + 0.49481928402277814 * fTemp20 + 0.999323242243544 * fTemp21 + 0.994585937948352 * fTemp22 + 0.41500972208362036 * fTemp23 + 0.9998908455231522 * fTemp24 + 0.9982535283704361 * fTemp25 + 0.32693976711264294 * fTemp26 + 0.9970130201393839 * fTemp27 + 0.45542119302793127 * fTemp28 + 0.9869070026308616 * fTemp29) + fTemp2 * fRec384[1];
			fRec383[0] = fConst0 * fTemp30 * fRec384[0] * fTemp3 + fTemp2 * fRec383[1];
			int iTemp566 = int(fRec383[0]);
			double fTemp567 = ((fRec379[1] != 0.0) ? (((fRec380[1] > 0.0) & (fRec380[1] < 1.0)) ? fRec379[1] : 0.0) : (((fRec380[1] == 0.0) & (iTemp566 != iRec381[1])) ? 0.00048828125 : (((fRec380[1] == 1.0) & (iTemp566 != iRec382[1])) ? -0.00048828125 : 0.0)));
			fRec379[0] = fTemp567;
			fRec380[0] = std::max<double>(0.0, std::min<double>(1.0, fRec380[1] + fTemp567));
			iRec381[0] = (((fRec380[1] >= 1.0) & (iRec382[1] != iTemp566)) ? iTemp566 : iRec381[1]);
			iRec382[0] = (((fRec380[1] <= 0.0) & (iRec381[1] != iTemp566)) ? iTemp566 : iRec382[1]);
			double fTemp568 = fRec17[0] * fRec384[0] * fTemp35 * (fTemp37 * (fTemp38 * (4.069010416666667e-06 * fTemp39 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec384[0] * fTemp43) + 1.0) + 8.138020833333335e-06 * fTemp44 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec384[0] * fTemp45) + 1.0)) + 8.138020833333335e-06 * fTemp46 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec384[0] * fTemp47) + 1.0)) + 4.069010416666667e-06 * fTemp48 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec384[0] * fTemp49) + 1.0));
			fVec145[IOTA0 & 262143] = fTemp568;
			double fTemp569 = fVec145[(IOTA0 - int(std::min<double>(fConst0, double(std::max<int>(0, iRec381[0]))))) & 262143];
			double fTemp570 = fRec380[0] * (fTemp569 - fVec145[(IOTA0 - int(std::min<double>(fConst0, double(std::max<int>(0, iRec382[0]))))) & 262143]) - fTemp569;
			fVec146[IOTA0 & 131071] = fTemp570;
			fRec385[0] = std::fmod(fRec385[1] + (10001.0 - std::pow(2.0, 0.08333333333333333 * fTemp67 * fRec384[0])), 1e+04);
			int iTemp571 = int(fRec385[0]);
			double fTemp572 = std::floor(fRec385[0]);
			double fTemp573 = std::min<double>(0.0001 * fRec385[0], 1.0);
			double fTemp574 = fRec385[0] + 1e+04;
			int iTemp575 = int(fTemp574);
			double fTemp576 = std::floor(fTemp574);
			double fTemp577 = (fVec146[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp571))) & 131071] * (fTemp572 + (1.0 - fRec385[0])) + (fRec385[0] - fTemp572) * fVec146[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp571 + 1))) & 131071]) * fTemp573 + (fVec146[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp575))) & 131071] * (fTemp576 + (-9999.0 - fRec385[0])) + (fRec385[0] + (1e+04 - fTemp576)) * fVec146[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp575 + 1))) & 131071]) * (1.0 - fTemp573);
			fVec147[0] = fTemp577;
			fRec378[0] = 0.995 * fRec378[1] + fTemp577 - fVec147[1];
			double fTemp578 = std::min<double>(std::max<double>(fRec370[0], -1.0), 1.0) + std::min<double>(std::max<double>(fRec378[0], -1.0), 1.0);
			fVec148[0] = fTemp578;
			fRec369[0] = 0.995 * fRec369[1] + 0.125 * (fTemp578 - fVec148[1]);
			output20[i0] = FAUSTFLOAT(std::min<double>(std::max<double>(fRec369[0], -1.0), 1.0));
			fRec393[0] = fTemp3 * (0.3548387096774194 * fTemp7 + 0.12591050988553593 * fTemp9 + 0.5289640103269624 * fTemp10 + 0.4381211123918852 * fTemp11 + 0.5956833971812706 * fTemp12 + 0.15135574250524897 * fTemp13 + 0.27980292422118275 * fTemp14 + 0.5837669094693028 * fTemp15 + 0.25182101977107185 * fTemp16 + 0.04467792286260952 * fTemp17 + 0.7314625222382599 * fTemp18 + 0.17871169145043808 * fTemp19 + 0.015853456499635637 * fTemp20 + 0.8267500143472645 * fTemp21 + 0.1268276519970851 * fTemp22 + 0.0056254200482578065 * fTemp23 + 0.8882258157079126 * fTemp24 + 0.0900067207721249 * fTemp25 + 0.011425389008069968 * fTemp26 + 0.9145269802796004 * fTemp27 + 0.06507246798777822 * fTemp28 + 0.7640464053114201 * fTemp29) + fTemp2 * fRec393[1];
			fRec392[0] = fConst0 * fTemp30 * fRec393[0] * fTemp3 + fTemp2 * fRec392[1];
			int iTemp579 = int(fRec392[0]);
			double fTemp580 = ((fRec388[1] != 0.0) ? (((fRec389[1] > 0.0) & (fRec389[1] < 1.0)) ? fRec388[1] : 0.0) : (((fRec389[1] == 0.0) & (iTemp579 != iRec390[1])) ? 0.00048828125 : (((fRec389[1] == 1.0) & (iTemp579 != iRec391[1])) ? -0.00048828125 : 0.0)));
			fRec388[0] = fTemp580;
			fRec389[0] = std::max<double>(0.0, std::min<double>(1.0, fRec389[1] + fTemp580));
			iRec390[0] = (((fRec389[1] >= 1.0) & (iRec391[1] != iTemp579)) ? iTemp579 : iRec390[1]);
			iRec391[0] = (((fRec389[1] <= 0.0) & (iRec390[1] != iTemp579)) ? iTemp579 : iRec391[1]);
			double fTemp581 = fRec17[0] * fRec393[0] * fTemp35 * (fTemp37 * (fTemp38 * (4.069010416666667e-06 * fTemp39 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec393[0] * fTemp43) + 1.0) + 8.138020833333335e-06 * fTemp44 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec393[0] * fTemp45) + 1.0)) + 8.138020833333335e-06 * fTemp46 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec393[0] * fTemp47) + 1.0)) + 4.069010416666667e-06 * fTemp48 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec393[0] * fTemp49) + 1.0));
			fVec149[IOTA0 & 262143] = fTemp581;
			double fTemp582 = fVec149[(IOTA0 - int(std::min<double>(fConst0, double(std::max<int>(0, iRec390[0]))))) & 262143];
			double fTemp583 = fRec389[0] * (fTemp582 - fVec149[(IOTA0 - int(std::min<double>(fConst0, double(std::max<int>(0, iRec391[0]))))) & 262143]) - fTemp582;
			fVec150[IOTA0 & 131071] = fTemp583;
			fRec394[0] = std::fmod(fRec394[1] + (10001.0 - std::pow(2.0, 0.08333333333333333 * fTemp67 * fRec393[0])), 1e+04);
			int iTemp584 = int(fRec394[0]);
			double fTemp585 = std::floor(fRec394[0]);
			double fTemp586 = std::min<double>(0.0001 * fRec394[0], 1.0);
			double fTemp587 = fRec394[0] + 1e+04;
			int iTemp588 = int(fTemp587);
			double fTemp589 = std::floor(fTemp587);
			double fTemp590 = (fVec150[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp584))) & 131071] * (fTemp585 + (1.0 - fRec394[0])) + (fRec394[0] - fTemp585) * fVec150[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp584 + 1))) & 131071]) * fTemp586 + (fVec150[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp588))) & 131071] * (fTemp589 + (-9999.0 - fRec394[0])) + (fRec394[0] + (1e+04 - fTemp589)) * fVec150[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp588 + 1))) & 131071]) * (1.0 - fTemp586);
			fVec151[0] = fTemp590;
			fRec387[0] = 0.995 * fRec387[1] + fTemp590 - fVec151[1];
			fRec401[0] = fTemp3 * (0.8548387096774194 * fTemp7 + 0.7307492195629552 * fTemp9 + 0.974116147995387 * fTemp10 + 0.8912937405575002 * fTemp11 + 0.9245748805139686 * fTemp12 + 0.773951929416265 * fTemp13 + 0.9489022697853708 * fTemp14 + 0.9789281997918835 * fTemp15 + 0.9578563995837669 * fTemp16 + 0.6246727199489779 * fTemp17 + 0.9969411902923702 * fTemp18 + 0.9877647611694808 * fTemp19 + 0.5339944218918682 * fTemp20 + 0.9995559792359893 * fTemp21 + 0.9964478338879138 * fTemp22 + 0.45647910258498414 * fTemp23 + 0.9999355453729661 * fTemp24 + 0.9989687259674588 * fTemp25 + 0.36561244825823896 * fTemp26 + 0.9973289681337375 * fTemp27 + 0.48110619541466415 * fTemp28 + 0.989408004713871 * fTemp29) + fTemp2 * fRec401[1];
			fRec400[0] = fConst0 * fTemp30 * fRec401[0] * fTemp3 + fTemp2 * fRec400[1];
			int iTemp591 = int(fRec400[0]);
			double fTemp592 = ((fRec396[1] != 0.0) ? (((fRec397[1] > 0.0) & (fRec397[1] < 1.0)) ? fRec396[1] : 0.0) : (((fRec397[1] == 0.0) & (iTemp591 != iRec398[1])) ? 0.00048828125 : (((fRec397[1] == 1.0) & (iTemp591 != iRec399[1])) ? -0.00048828125 : 0.0)));
			fRec396[0] = fTemp592;
			fRec397[0] = std::max<double>(0.0, std::min<double>(1.0, fRec397[1] + fTemp592));
			iRec398[0] = (((fRec397[1] >= 1.0) & (iRec399[1] != iTemp591)) ? iTemp591 : iRec398[1]);
			iRec399[0] = (((fRec397[1] <= 0.0) & (iRec398[1] != iTemp591)) ? iTemp591 : iRec399[1]);
			double fTemp593 = fRec17[0] * fRec401[0] * fTemp35 * (fTemp37 * (fTemp38 * (4.069010416666667e-06 * fTemp39 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec401[0] * fTemp43) + 1.0) + 8.138020833333335e-06 * fTemp44 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec401[0] * fTemp45) + 1.0)) + 8.138020833333335e-06 * fTemp46 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec401[0] * fTemp47) + 1.0)) + 4.069010416666667e-06 * fTemp48 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec401[0] * fTemp49) + 1.0));
			fVec152[IOTA0 & 262143] = fTemp593;
			double fTemp594 = fVec152[(IOTA0 - int(std::min<double>(fConst0, double(std::max<int>(0, iRec398[0]))))) & 262143];
			double fTemp595 = fRec397[0] * (fTemp594 - fVec152[(IOTA0 - int(std::min<double>(fConst0, double(std::max<int>(0, iRec399[0]))))) & 262143]) - fTemp594;
			fVec153[IOTA0 & 131071] = fTemp595;
			fRec402[0] = std::fmod(fRec402[1] + (10001.0 - std::pow(2.0, 0.08333333333333333 * fTemp67 * fRec401[0])), 1e+04);
			int iTemp596 = int(fRec402[0]);
			double fTemp597 = std::floor(fRec402[0]);
			double fTemp598 = std::min<double>(0.0001 * fRec402[0], 1.0);
			double fTemp599 = fRec402[0] + 1e+04;
			int iTemp600 = int(fTemp599);
			double fTemp601 = std::floor(fTemp599);
			double fTemp602 = (fVec153[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp596))) & 131071] * (fTemp597 + (1.0 - fRec402[0])) + (fRec402[0] - fTemp597) * fVec153[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp596 + 1))) & 131071]) * fTemp598 + (fVec153[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp600))) & 131071] * (fTemp601 + (-9999.0 - fRec402[0])) + (fRec402[0] + (1e+04 - fTemp601)) * fVec153[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp600 + 1))) & 131071]) * (1.0 - fTemp598);
			fVec154[0] = fTemp602;
			fRec395[0] = 0.995 * fRec395[1] + fTemp602 - fVec154[1];
			double fTemp603 = std::min<double>(std::max<double>(fRec387[0], -1.0), 1.0) + std::min<double>(std::max<double>(fRec395[0], -1.0), 1.0);
			fVec155[0] = fTemp603;
			fRec386[0] = 0.995 * fRec386[1] + 0.125 * (fTemp603 - fVec155[1]);
			output21[i0] = FAUSTFLOAT(std::min<double>(std::max<double>(fRec386[0], -1.0), 1.0));
			fRec410[0] = fTemp3 * (0.3709677419354839 * fTemp7 + 0.13761706555671177 * fTemp9 + 0.5502927152373913 * fTemp10 + 0.4551946257508267 * fTemp11 + 0.6090712125322324 * fTemp12 + 0.16502818756759263 * fTemp13 + 0.3028220724433406 * fTemp14 + 0.604318418314256 * fTemp15 + 0.27523413111342354 * fTemp16 + 0.051051492061360816 * fTemp17 + 0.7511035211976771 * fTemp18 + 0.20420596824544326 * fTemp19 + 0.018938456732440302 * fTemp20 + 0.8434360859146679 * fTemp21 + 0.15150765385952242 * fTemp22 + 0.007025556529776242 * fTemp23 + 0.9015162475914846 * fTemp24 + 0.11240890447641987 * fTemp25 + 0.012776862491934195 * fTemp26 + 0.9235678946520333 * fTemp27 + 0.07135424706549787 * fTemp28 + 0.7773791985345736 * fTemp29) + fTemp2 * fRec410[1];
			fRec409[0] = fConst0 * fTemp30 * fRec410[0] * fTemp3 + fTemp2 * fRec409[1];
			int iTemp604 = int(fRec409[0]);
			double fTemp605 = ((fRec405[1] != 0.0) ? (((fRec406[1] > 0.0) & (fRec406[1] < 1.0)) ? fRec405[1] : 0.0) : (((fRec406[1] == 0.0) & (iTemp604 != iRec407[1])) ? 0.00048828125 : (((fRec406[1] == 1.0) & (iTemp604 != iRec408[1])) ? -0.00048828125 : 0.0)));
			fRec405[0] = fTemp605;
			fRec406[0] = std::max<double>(0.0, std::min<double>(1.0, fRec406[1] + fTemp605));
			iRec407[0] = (((fRec406[1] >= 1.0) & (iRec408[1] != iTemp604)) ? iTemp604 : iRec407[1]);
			iRec408[0] = (((fRec406[1] <= 0.0) & (iRec407[1] != iTemp604)) ? iTemp604 : iRec408[1]);
			double fTemp606 = fRec17[0] * fRec410[0] * fTemp35 * (fTemp37 * (fTemp38 * (8.138020833333335e-06 * fTemp44 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec410[0] * fTemp45) + 1.0) + 4.069010416666667e-06 * fTemp39 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec410[0] * fTemp43) + 1.0)) + 8.138020833333335e-06 * fTemp46 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec410[0] * fTemp47) + 1.0)) + 4.069010416666667e-06 * fTemp48 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec410[0] * fTemp49) + 1.0));
			fVec156[IOTA0 & 262143] = fTemp606;
			double fTemp607 = fVec156[(IOTA0 - int(std::min<double>(fConst0, double(std::max<int>(0, iRec407[0]))))) & 262143];
			double fTemp608 = fRec406[0] * (fTemp607 - fVec156[(IOTA0 - int(std::min<double>(fConst0, double(std::max<int>(0, iRec408[0]))))) & 262143]) - fTemp607;
			fVec157[IOTA0 & 131071] = fTemp608;
			fRec411[0] = std::fmod(fRec411[1] + (10001.0 - std::pow(2.0, 0.08333333333333333 * fTemp67 * fRec410[0])), 1e+04);
			int iTemp609 = int(fRec411[0]);
			double fTemp610 = std::floor(fRec411[0]);
			double fTemp611 = std::min<double>(0.0001 * fRec411[0], 1.0);
			double fTemp612 = fRec411[0] + 1e+04;
			int iTemp613 = int(fTemp612);
			double fTemp614 = std::floor(fTemp612);
			double fTemp615 = (fVec157[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp609))) & 131071] * (fTemp610 + (1.0 - fRec411[0])) + (fRec411[0] - fTemp610) * fVec157[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp609 + 1))) & 131071]) * fTemp611 + (fVec157[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp613))) & 131071] * (fTemp614 + (-9999.0 - fRec411[0])) + (fRec411[0] + (1e+04 - fTemp614)) * fVec157[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp613 + 1))) & 131071]) * (1.0 - fTemp611);
			fVec158[0] = fTemp615;
			fRec404[0] = 0.995 * fRec404[1] + fTemp615 - fVec158[1];
			fRec418[0] = fTemp3 * (0.8709677419354839 * fTemp7 + 0.7585848074921956 * fTemp9 + 0.9795299412524945 * fTemp10 + 0.9037846847406971 * fTemp11 + 0.9332565252573828 * fTemp12 + 0.7987014799113399 * fTemp13 + 0.9594789058101153 * fTemp14 + 0.9833506763787722 * fTemp15 + 0.9667013527575442 * fTemp16 + 0.6607028968480414 * fTemp17 + 0.997851700177906 * fTemp18 + 0.9914068007116243 * fTemp19 + 0.5754509101579716 * fTemp20 + 0.9997228000229557 * fTemp21 + 0.997782400183645 * fTemp22 + 0.5011991798150075 * fTemp23 + 0.9999642322610265 * fTemp24 + 0.9994277161764246 * fTemp25 + 0.40885959974189423 * fTemp26 + 0.9976114967078761 * fTemp27 + 0.5086597996216833 * fTemp28 + 0.9916403967057676 * fTemp29) + fTemp2 * fRec418[1];
			fRec417[0] = fConst0 * fTemp30 * fRec418[0] * fTemp3 + fTemp2 * fRec417[1];
			int iTemp616 = int(fRec417[0]);
			double fTemp617 = ((fRec413[1] != 0.0) ? (((fRec414[1] > 0.0) & (fRec414[1] < 1.0)) ? fRec413[1] : 0.0) : (((fRec414[1] == 0.0) & (iTemp616 != iRec415[1])) ? 0.00048828125 : (((fRec414[1] == 1.0) & (iTemp616 != iRec416[1])) ? -0.00048828125 : 0.0)));
			fRec413[0] = fTemp617;
			fRec414[0] = std::max<double>(0.0, std::min<double>(1.0, fRec414[1] + fTemp617));
			iRec415[0] = (((fRec414[1] >= 1.0) & (iRec416[1] != iTemp616)) ? iTemp616 : iRec415[1]);
			iRec416[0] = (((fRec414[1] <= 0.0) & (iRec415[1] != iTemp616)) ? iTemp616 : iRec416[1]);
			double fTemp618 = fRec17[0] * fRec418[0] * fTemp35 * (fTemp37 * (fTemp38 * (4.069010416666667e-06 * fTemp39 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec418[0] * fTemp43) + 1.0) + 8.138020833333335e-06 * fTemp44 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec418[0] * fTemp45) + 1.0)) + 8.138020833333335e-06 * fTemp46 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec418[0] * fTemp47) + 1.0)) + 4.069010416666667e-06 * fTemp48 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec418[0] * fTemp49) + 1.0));
			fVec159[IOTA0 & 262143] = fTemp618;
			double fTemp619 = fVec159[(IOTA0 - int(std::min<double>(fConst0, double(std::max<int>(0, iRec415[0]))))) & 262143];
			double fTemp620 = fRec414[0] * (fTemp619 - fVec159[(IOTA0 - int(std::min<double>(fConst0, double(std::max<int>(0, iRec416[0]))))) & 262143]) - fTemp619;
			fVec160[IOTA0 & 131071] = fTemp620;
			fRec419[0] = std::fmod(fRec419[1] + (10001.0 - std::pow(2.0, 0.08333333333333333 * fTemp67 * fRec418[0])), 1e+04);
			int iTemp621 = int(fRec419[0]);
			double fTemp622 = std::floor(fRec419[0]);
			double fTemp623 = std::min<double>(0.0001 * fRec419[0], 1.0);
			double fTemp624 = fRec419[0] + 1e+04;
			int iTemp625 = int(fTemp624);
			double fTemp626 = std::floor(fTemp624);
			double fTemp627 = (fVec160[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp621))) & 131071] * (fTemp622 + (1.0 - fRec419[0])) + (fRec419[0] - fTemp622) * fVec160[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp621 + 1))) & 131071]) * fTemp623 + (fVec160[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp625))) & 131071] * (fTemp626 + (-9999.0 - fRec419[0])) + (fRec419[0] + (1e+04 - fTemp626)) * fVec160[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp625 + 1))) & 131071]) * (1.0 - fTemp623);
			fVec161[0] = fTemp627;
			fRec412[0] = 0.995 * fRec412[1] + fTemp627 - fVec161[1];
			double fTemp628 = std::min<double>(std::max<double>(fRec404[0], -1.0), 1.0) + std::min<double>(std::max<double>(fRec412[0], -1.0), 1.0);
			fVec162[0] = fTemp628;
			fRec403[0] = 0.995 * fRec403[1] + 0.125 * (fTemp628 - fVec162[1]);
			output22[i0] = FAUSTFLOAT(std::min<double>(std::max<double>(fRec403[0], -1.0), 1.0));
			fRec427[0] = fTemp3 * (0.3870967741935484 * fTemp7 + 0.14984391259105098 * fTemp9 + 0.5712682150947923 * fTemp10 + 0.47206844431522277 * fTemp11 + 0.6221710168382552 * fTemp12 + 0.1792365587927237 * fTemp13 + 0.32634737357758986 * fTemp14 + 0.6243496357960459 * fTemp15 + 0.29968782518210196 * fTemp16 + 0.05800409519653586 * fTemp17 + 0.769762680004028 * fTemp18 + 0.23201638078614345 * fTemp19 + 0.022453198140594526 * fTemp20 + 0.8588868038734365 * fTemp21 + 0.1796255851247562 * fTemp22 + 0.008691560570552719 * fTemp23 + 0.9135112668901708 * fTemp24 + 0.1390649691288435 * fTemp25 + 0.014288197541675806 * fTemp26 + 0.9316525057025868 * fTemp27 + 0.0779609078737773 * fTemp28 + 0.7901579815429607 * fTemp29) + fTemp2 * fRec427[1];
			fRec426[0] = fConst0 * fTemp30 * fRec427[0] * fTemp3 + fTemp2 * fRec426[1];
			int iTemp629 = int(fRec426[0]);
			double fTemp630 = ((fRec422[1] != 0.0) ? (((fRec423[1] > 0.0) & (fRec423[1] < 1.0)) ? fRec422[1] : 0.0) : (((fRec423[1] == 0.0) & (iTemp629 != iRec424[1])) ? 0.00048828125 : (((fRec423[1] == 1.0) & (iTemp629 != iRec425[1])) ? -0.00048828125 : 0.0)));
			fRec422[0] = fTemp630;
			fRec423[0] = std::max<double>(0.0, std::min<double>(1.0, fRec423[1] + fTemp630));
			iRec424[0] = (((fRec423[1] >= 1.0) & (iRec425[1] != iTemp629)) ? iTemp629 : iRec424[1]);
			iRec425[0] = (((fRec423[1] <= 0.0) & (iRec424[1] != iTemp629)) ? iTemp629 : iRec425[1]);
			double fTemp631 = fRec17[0] * fRec427[0] * fTemp35 * (fTemp37 * (fTemp38 * (4.069010416666667e-06 * fTemp39 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec427[0] * fTemp43) + 1.0) + 8.138020833333335e-06 * fTemp44 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec427[0] * fTemp45) + 1.0)) + 8.138020833333335e-06 * fTemp46 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec427[0] * fTemp47) + 1.0)) + 4.069010416666667e-06 * fTemp48 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec427[0] * fTemp49) + 1.0));
			fVec163[IOTA0 & 262143] = fTemp631;
			double fTemp632 = fVec163[(IOTA0 - int(std::min<double>(fConst0, double(std::max<int>(0, iRec424[0]))))) & 262143];
			double fTemp633 = fRec423[0] * (fTemp632 - fVec163[(IOTA0 - int(std::min<double>(fConst0, double(std::max<int>(0, iRec425[0]))))) & 262143]) - fTemp632;
			fVec164[IOTA0 & 131071] = fTemp633;
			fRec428[0] = std::fmod(fRec428[1] + (10001.0 - std::pow(2.0, 0.08333333333333333 * fTemp67 * fRec427[0])), 1e+04);
			int iTemp634 = int(fRec428[0]);
			double fTemp635 = std::floor(fRec428[0]);
			double fTemp636 = std::min<double>(0.0001 * fRec428[0], 1.0);
			double fTemp637 = fRec428[0] + 1e+04;
			int iTemp638 = int(fTemp637);
			double fTemp639 = std::floor(fTemp637);
			double fTemp640 = (fVec164[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp634))) & 131071] * (fTemp635 + (1.0 - fRec428[0])) + (fRec428[0] - fTemp635) * fVec164[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp634 + 1))) & 131071]) * fTemp636 + (fVec164[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp638))) & 131071] * (fTemp639 + (-9999.0 - fRec428[0])) + (fRec428[0] + (1e+04 - fTemp639)) * fVec164[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp638 + 1))) & 131071]) * (1.0 - fTemp636);
			fVec165[0] = fTemp640;
			fRec421[0] = 0.995 * fRec421[1] + fTemp640 - fVec165[1];
			fRec435[0] = fTemp3 * (0.8870967741935484 * fTemp7 + 0.7869406867845994 * fTemp9 + 0.9843150237975342 * fTemp10 + 0.9161684091965294 * fTemp11 + 0.9418581497197699 * fTemp12 + 0.8235802337421916 * fTemp13 + 0.9688760660735403 * fTemp14 + 0.9872528616024974 * fTemp15 + 0.9745057232049948 * fTemp16 + 0.6980925447282736 * fTemp17 + 0.9985608069551207 * fTemp18 + 0.9942432278204827 * fTemp19 + 0.6192756445170169 * fTemp20 + 0.9998375104626749 * fTemp21 + 0.9987000837013993 * fTemp22 + 0.5493574265876763 * fTemp23 + 0.9999816544070762 * fTemp24 + 0.9997064705132191 * fTemp25 + 0.4572223213336258 * fTemp26 + 0.9978641408032058 * fTemp27 + 0.5384165154434134 * fTemp28 + 0.9936059891136413 * fTemp29) + fTemp2 * fRec435[1];
			fRec434[0] = fConst0 * fTemp30 * fRec435[0] * fTemp3 + fTemp2 * fRec434[1];
			int iTemp641 = int(fRec434[0]);
			double fTemp642 = ((fRec430[1] != 0.0) ? (((fRec431[1] > 0.0) & (fRec431[1] < 1.0)) ? fRec430[1] : 0.0) : (((fRec431[1] == 0.0) & (iTemp641 != iRec432[1])) ? 0.00048828125 : (((fRec431[1] == 1.0) & (iTemp641 != iRec433[1])) ? -0.00048828125 : 0.0)));
			fRec430[0] = fTemp642;
			fRec431[0] = std::max<double>(0.0, std::min<double>(1.0, fRec431[1] + fTemp642));
			iRec432[0] = (((fRec431[1] >= 1.0) & (iRec433[1] != iTemp641)) ? iTemp641 : iRec432[1]);
			iRec433[0] = (((fRec431[1] <= 0.0) & (iRec432[1] != iTemp641)) ? iTemp641 : iRec433[1]);
			double fTemp643 = fRec17[0] * fRec435[0] * fTemp35 * (fTemp37 * (fTemp38 * (4.069010416666667e-06 * fTemp39 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec435[0] * fTemp43) + 1.0) + 8.138020833333335e-06 * fTemp44 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec435[0] * fTemp45) + 1.0)) + 8.138020833333335e-06 * fTemp46 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec435[0] * fTemp47) + 1.0)) + 4.069010416666667e-06 * fTemp48 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec435[0] * fTemp49) + 1.0));
			fVec166[IOTA0 & 262143] = fTemp643;
			double fTemp644 = fVec166[(IOTA0 - int(std::min<double>(fConst0, double(std::max<int>(0, iRec432[0]))))) & 262143];
			double fTemp645 = fRec431[0] * (fTemp644 - fVec166[(IOTA0 - int(std::min<double>(fConst0, double(std::max<int>(0, iRec433[0]))))) & 262143]) - fTemp644;
			fVec167[IOTA0 & 131071] = fTemp645;
			fRec436[0] = std::fmod(fRec436[1] + (10001.0 - std::pow(2.0, 0.08333333333333333 * fTemp67 * fRec435[0])), 1e+04);
			int iTemp646 = int(fRec436[0]);
			double fTemp647 = std::floor(fRec436[0]);
			double fTemp648 = std::min<double>(0.0001 * fRec436[0], 1.0);
			double fTemp649 = fRec436[0] + 1e+04;
			int iTemp650 = int(fTemp649);
			double fTemp651 = std::floor(fTemp649);
			double fTemp652 = (fVec167[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp646))) & 131071] * (fTemp647 + (1.0 - fRec436[0])) + (fRec436[0] - fTemp647) * fVec167[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp646 + 1))) & 131071]) * fTemp648 + (fVec167[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp650))) & 131071] * (fTemp651 + (-9999.0 - fRec436[0])) + (fRec436[0] + (1e+04 - fTemp651)) * fVec167[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp650 + 1))) & 131071]) * (1.0 - fTemp648);
			fVec168[0] = fTemp652;
			fRec429[0] = 0.995 * fRec429[1] + fTemp652 - fVec168[1];
			double fTemp653 = std::min<double>(std::max<double>(fRec421[0], -1.0), 1.0) + std::min<double>(std::max<double>(fRec429[0], -1.0), 1.0);
			fVec169[0] = fTemp653;
			fRec420[0] = 0.995 * fRec420[1] + 0.125 * (fTemp653 - fVec169[1]);
			output23[i0] = FAUSTFLOAT(std::min<double>(std::max<double>(fRec420[0], -1.0), 1.0));
			fRec444[0] = fTemp3 * (0.4032258064516129 * fTemp7 + 0.16259105098855356 * fTemp9 + 0.5918770467870172 * fTemp10 + 0.4887471854618532 * fTemp11 + 0.6350006350009525 * fTemp12 + 0.19397173654599498 * fTemp13 + 0.350318438513321 * fTemp14 + 0.6438605619146722 * fTemp15 + 0.3251821019771071 * fTemp16 + 0.06556090765667483 * fTemp17 + 0.7874651740458527 * fTemp18 + 0.2622436306266993 * fTemp19 + 0.026435849861562426 * fTemp20 + 0.873164700640267 * fTemp21 + 0.2114867988924994 * fTemp22 + 0.010659616879662269 * fTemp23 + 0.924307966511127 * fTemp24 + 0.1705538700745963 * fTemp25 + 0.015978303681269818 * fTemp26 + 0.9388819664790354 * fTemp27 + 0.08489948693520755 * fTemp28 + 0.8024092234730806 * fTemp29) + fTemp2 * fRec444[1];
			fRec443[0] = fConst0 * fTemp30 * fRec444[0] * fTemp3 + fTemp2 * fRec443[1];
			int iTemp654 = int(fRec443[0]);
			double fTemp655 = ((fRec439[1] != 0.0) ? (((fRec440[1] > 0.0) & (fRec440[1] < 1.0)) ? fRec439[1] : 0.0) : (((fRec440[1] == 0.0) & (iTemp654 != iRec441[1])) ? 0.00048828125 : (((fRec440[1] == 1.0) & (iTemp654 != iRec442[1])) ? -0.00048828125 : 0.0)));
			fRec439[0] = fTemp655;
			fRec440[0] = std::max<double>(0.0, std::min<double>(1.0, fRec440[1] + fTemp655));
			iRec441[0] = (((fRec440[1] >= 1.0) & (iRec442[1] != iTemp654)) ? iTemp654 : iRec441[1]);
			iRec442[0] = (((fRec440[1] <= 0.0) & (iRec441[1] != iTemp654)) ? iTemp654 : iRec442[1]);
			double fTemp656 = fRec17[0] * fRec444[0] * fTemp35 * (fTemp37 * (fTemp38 * (8.138020833333335e-06 * fTemp44 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec444[0] * fTemp45) + 1.0) + 4.069010416666667e-06 * fTemp39 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec444[0] * fTemp43) + 1.0)) + 8.138020833333335e-06 * fTemp46 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec444[0] * fTemp47) + 1.0)) + 4.069010416666667e-06 * fTemp48 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec444[0] * fTemp49) + 1.0));
			fVec170[IOTA0 & 262143] = fTemp656;
			double fTemp657 = fVec170[(IOTA0 - int(std::min<double>(fConst0, double(std::max<int>(0, iRec441[0]))))) & 262143];
			double fTemp658 = fRec440[0] * (fTemp657 - fVec170[(IOTA0 - int(std::min<double>(fConst0, double(std::max<int>(0, iRec442[0]))))) & 262143]) - fTemp657;
			fVec171[IOTA0 & 131071] = fTemp658;
			fRec445[0] = std::fmod(fRec445[1] + (10001.0 - std::pow(2.0, 0.08333333333333333 * fTemp67 * fRec444[0])), 1e+04);
			int iTemp659 = int(fRec445[0]);
			double fTemp660 = std::floor(fRec445[0]);
			double fTemp661 = std::min<double>(0.0001 * fRec445[0], 1.0);
			double fTemp662 = fRec445[0] + 1e+04;
			int iTemp663 = int(fTemp662);
			double fTemp664 = std::floor(fTemp662);
			double fTemp665 = (fVec171[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp659))) & 131071] * (fTemp660 + (1.0 - fRec445[0])) + (fRec445[0] - fTemp660) * fVec171[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp659 + 1))) & 131071]) * fTemp661 + (fVec171[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp663))) & 131071] * (fTemp664 + (-9999.0 - fRec445[0])) + (fRec445[0] + (1e+04 - fTemp664)) * fVec171[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp663 + 1))) & 131071]) * (1.0 - fTemp661);
			fVec172[0] = fTemp665;
			fRec438[0] = 0.995 * fRec438[1] + fTemp665 - fVec172[1];
			fRec452[0] = fTemp3 * (0.9032258064516129 * fTemp7 + 0.8158168574401664 * fTemp9 + 0.9884683243281114 * fTemp10 + 0.9284467389749661 * fTemp11 + 0.9503819266229829 * fTemp12 + 0.8485722224954233 * fTemp13 + 0.9770696282000244 * fTemp14 + 0.9906347554630593 * fTemp15 + 0.9812695109261186 * fTemp16 + 0.7368668389782148 * fTemp17 + 0.9990936860125541 * fTemp18 + 0.9963747440502165 * fTemp19 + 0.6655571448835489 * fTemp20 + 0.9999122921947633 * fTemp21 + 0.9992983375581064 * fTemp22 + 0.6011483889270763 * fTemp23 + 0.9999915121478803 * fTemp24 + 0.9998641943660851 * fTemp25 + 0.5113057178006345 * fTemp26 + 0.9980900614524698 * fTemp27 + 0.5708343646564492 * fTemp28 + 0.9953063626155815 * fTemp29) + fTemp2 * fRec452[1];
			fRec451[0] = fConst0 * fTemp30 * fRec452[0] * fTemp3 + fTemp2 * fRec451[1];
			int iTemp666 = int(fRec451[0]);
			double fTemp667 = ((fRec447[1] != 0.0) ? (((fRec448[1] > 0.0) & (fRec448[1] < 1.0)) ? fRec447[1] : 0.0) : (((fRec448[1] == 0.0) & (iTemp666 != iRec449[1])) ? 0.00048828125 : (((fRec448[1] == 1.0) & (iTemp666 != iRec450[1])) ? -0.00048828125 : 0.0)));
			fRec447[0] = fTemp667;
			fRec448[0] = std::max<double>(0.0, std::min<double>(1.0, fRec448[1] + fTemp667));
			iRec449[0] = (((fRec448[1] >= 1.0) & (iRec450[1] != iTemp666)) ? iTemp666 : iRec449[1]);
			iRec450[0] = (((fRec448[1] <= 0.0) & (iRec449[1] != iTemp666)) ? iTemp666 : iRec450[1]);
			double fTemp668 = fRec17[0] * fRec452[0] * fTemp35 * (fTemp37 * (fTemp38 * (4.069010416666667e-06 * fTemp39 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec452[0] * fTemp43) + 1.0) + 8.138020833333335e-06 * fTemp44 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec452[0] * fTemp45) + 1.0)) + 8.138020833333335e-06 * fTemp46 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec452[0] * fTemp47) + 1.0)) + 4.069010416666667e-06 * fTemp48 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec452[0] * fTemp49) + 1.0));
			fVec173[IOTA0 & 262143] = fTemp668;
			double fTemp669 = fVec173[(IOTA0 - int(std::min<double>(fConst0, double(std::max<int>(0, iRec449[0]))))) & 262143];
			double fTemp670 = fRec448[0] * (fTemp669 - fVec173[(IOTA0 - int(std::min<double>(fConst0, double(std::max<int>(0, iRec450[0]))))) & 262143]) - fTemp669;
			fVec174[IOTA0 & 131071] = fTemp670;
			fRec453[0] = std::fmod(fRec453[1] + (10001.0 - std::pow(2.0, 0.08333333333333333 * fTemp67 * fRec452[0])), 1e+04);
			int iTemp671 = int(fRec453[0]);
			double fTemp672 = std::floor(fRec453[0]);
			double fTemp673 = std::min<double>(0.0001 * fRec453[0], 1.0);
			double fTemp674 = fRec453[0] + 1e+04;
			int iTemp675 = int(fTemp674);
			double fTemp676 = std::floor(fTemp674);
			double fTemp677 = (fVec174[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp671))) & 131071] * (fTemp672 + (1.0 - fRec453[0])) + (fRec453[0] - fTemp672) * fVec174[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp671 + 1))) & 131071]) * fTemp673 + (fVec174[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp675))) & 131071] * (fTemp676 + (-9999.0 - fRec453[0])) + (fRec453[0] + (1e+04 - fTemp676)) * fVec174[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp675 + 1))) & 131071]) * (1.0 - fTemp673);
			fVec175[0] = fTemp677;
			fRec446[0] = 0.995 * fRec446[1] + fTemp677 - fVec175[1];
			double fTemp678 = std::min<double>(std::max<double>(fRec438[0], -1.0), 1.0) + std::min<double>(std::max<double>(fRec446[0], -1.0), 1.0);
			fVec176[0] = fTemp678;
			fRec437[0] = 0.995 * fRec437[1] + 0.125 * (fTemp678 - fVec176[1]);
			output24[i0] = FAUSTFLOAT(std::min<double>(std::max<double>(fRec437[0], -1.0), 1.0));
			fRec461[0] = fTemp3 * (0.41935483870967744 * fTemp7 + 0.1758584807492196 * fTemp9 + 0.6121059825476628 * fTemp10 + 0.5052353082504222 * fTemp11 + 0.6475761258027333 * fTemp12 + 0.20922426306230146 * fTemp13 + 0.37467373387063974 * fTemp14 + 0.6628511966701354 * fTemp15 + 0.3517169614984392 * fTemp16 + 0.07374710483031789 * fTemp17 + 0.8042361787116915 * fTemp18 + 0.29498841932127157 * fTemp19 + 0.030926205251423634 * fTemp20 + 0.8863306844132403 * fTemp21 + 0.24740964201138907 * fTemp22 + 0.012969053815113136 * fTemp23 + 0.9339984619173654 * fTemp24 + 0.20750486104181018 * fTemp25 + 0.017868327183061663 * fTemp26 + 0.9453467305587657 * fTemp27 + 0.09217759487288468 * fTemp28 + 0.8141567396209008 * fTemp29) + fTemp2 * fRec461[1];
			fRec460[0] = fConst0 * fTemp30 * fRec461[0] * fTemp3 + fTemp2 * fRec460[1];
			int iTemp679 = int(fRec460[0]);
			double fTemp680 = ((fRec456[1] != 0.0) ? (((fRec457[1] > 0.0) & (fRec457[1] < 1.0)) ? fRec456[1] : 0.0) : (((fRec457[1] == 0.0) & (iTemp679 != iRec458[1])) ? 0.00048828125 : (((fRec457[1] == 1.0) & (iTemp679 != iRec459[1])) ? -0.00048828125 : 0.0)));
			fRec456[0] = fTemp680;
			fRec457[0] = std::max<double>(0.0, std::min<double>(1.0, fRec457[1] + fTemp680));
			iRec458[0] = (((fRec457[1] >= 1.0) & (iRec459[1] != iTemp679)) ? iTemp679 : iRec458[1]);
			iRec459[0] = (((fRec457[1] <= 0.0) & (iRec458[1] != iTemp679)) ? iTemp679 : iRec459[1]);
			double fTemp681 = fRec17[0] * fRec461[0] * fTemp35 * (fTemp37 * (fTemp38 * (4.069010416666667e-06 * fTemp39 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec461[0] * fTemp43) + 1.0) + 8.138020833333335e-06 * fTemp44 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec461[0] * fTemp45) + 1.0)) + 8.138020833333335e-06 * fTemp46 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec461[0] * fTemp47) + 1.0)) + 4.069010416666667e-06 * fTemp48 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec461[0] * fTemp49) + 1.0));
			fVec177[IOTA0 & 262143] = fTemp681;
			double fTemp682 = fVec177[(IOTA0 - int(std::min<double>(fConst0, double(std::max<int>(0, iRec458[0]))))) & 262143];
			double fTemp683 = fRec457[0] * (fTemp682 - fVec177[(IOTA0 - int(std::min<double>(fConst0, double(std::max<int>(0, iRec459[0]))))) & 262143]) - fTemp682;
			fVec178[IOTA0 & 131071] = fTemp683;
			fRec462[0] = std::fmod(fRec462[1] + (10001.0 - std::pow(2.0, 0.08333333333333333 * fTemp67 * fRec461[0])), 1e+04);
			double fTemp684 = fRec462[0] + 1e+04;
			int iTemp685 = int(fTemp684);
			double fTemp686 = std::floor(fTemp684);
			double fTemp687 = std::min<double>(0.0001 * fRec462[0], 1.0);
			double fTemp688 = (fVec178[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp685))) & 131071] * (fTemp686 + (-9999.0 - fRec462[0])) + (fRec462[0] + (1e+04 - fTemp686)) * fVec178[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp685 + 1))) & 131071]) * (1.0 - fTemp687);
			int iTemp689 = int(fRec462[0]);
			double fTemp690 = std::floor(fRec462[0]);
			double fTemp691 = (fVec178[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp689))) & 131071] * (fTemp690 + (1.0 - fRec462[0])) + (fRec462[0] - fTemp690) * fVec178[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp689 + 1))) & 131071]) * fTemp687;
			fVec179[0] = fTemp691 + fTemp688;
			fRec455[0] = fTemp688 + 0.995 * fRec455[1] + fTemp691 - fVec179[1];
			fRec469[0] = fTemp3 * (0.9193548387096774 * fTemp7 + 0.8452133194588969 * fTemp9 + 0.991987177050743 * fTemp10 + 0.9406214529210684 * fTemp11 + 0.9588299321097967 * fTemp12 + 0.8736614050778706 * fTemp13 + 0.984038559433102 * fTemp14 + 0.9934963579604579 * fTemp15 + 0.9869927159209158 * fTemp16 + 0.7770509549864052 * fTemp17 + 0.9994755127387466 * fTemp18 + 0.9979020509549864 * fTemp19 + 0.7143855553907273 * fTemp20 + 0.9999577026402215 * fTemp21 + 0.999661621121772 * fTemp22 + 0.6567738170527654 * fTemp23 + 0.9999965889225985 * fTemp24 + 0.9999454227615762 * fTemp25 + 0.5717864698579732 * fTemp26 + 0.9982920853299614 * fTemp27 + 0.6065706155596622 * fTemp28 + 0.9967428745471211 * fTemp29) + fTemp2 * fRec469[1];
			fRec468[0] = fConst0 * fTemp30 * fRec469[0] * fTemp3 + fTemp2 * fRec468[1];
			int iTemp692 = int(fRec468[0]);
			double fTemp693 = ((fRec464[1] != 0.0) ? (((fRec465[1] > 0.0) & (fRec465[1] < 1.0)) ? fRec464[1] : 0.0) : (((fRec465[1] == 0.0) & (iTemp692 != iRec466[1])) ? 0.00048828125 : (((fRec465[1] == 1.0) & (iTemp692 != iRec467[1])) ? -0.00048828125 : 0.0)));
			fRec464[0] = fTemp693;
			fRec465[0] = std::max<double>(0.0, std::min<double>(1.0, fRec465[1] + fTemp693));
			iRec466[0] = (((fRec465[1] >= 1.0) & (iRec467[1] != iTemp692)) ? iTemp692 : iRec466[1]);
			iRec467[0] = (((fRec465[1] <= 0.0) & (iRec466[1] != iTemp692)) ? iTemp692 : iRec467[1]);
			double fTemp694 = fRec17[0] * fRec469[0] * fTemp35 * (fTemp37 * (fTemp38 * (4.069010416666667e-06 * fTemp39 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec469[0] * fTemp43) + 1.0) + 8.138020833333335e-06 * fTemp44 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec469[0] * fTemp45) + 1.0)) + 8.138020833333335e-06 * fTemp46 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec469[0] * fTemp47) + 1.0)) + 4.069010416666667e-06 * fTemp48 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec469[0] * fTemp49) + 1.0));
			fVec180[IOTA0 & 262143] = fTemp694;
			double fTemp695 = fVec180[(IOTA0 - int(std::min<double>(fConst0, double(std::max<int>(0, iRec466[0]))))) & 262143];
			double fTemp696 = fRec465[0] * (fTemp695 - fVec180[(IOTA0 - int(std::min<double>(fConst0, double(std::max<int>(0, iRec467[0]))))) & 262143]) - fTemp695;
			fVec181[IOTA0 & 131071] = fTemp696;
			fRec470[0] = std::fmod(fRec470[1] + (10001.0 - std::pow(2.0, 0.08333333333333333 * fTemp67 * fRec469[0])), 1e+04);
			int iTemp697 = int(fRec470[0]);
			double fTemp698 = std::floor(fRec470[0]);
			double fTemp699 = std::min<double>(0.0001 * fRec470[0], 1.0);
			double fTemp700 = fRec470[0] + 1e+04;
			int iTemp701 = int(fTemp700);
			double fTemp702 = std::floor(fTemp700);
			double fTemp703 = (fVec181[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp697))) & 131071] * (fTemp698 + (1.0 - fRec470[0])) + (fRec470[0] - fTemp698) * fVec181[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp697 + 1))) & 131071]) * fTemp699 + (fVec181[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp701))) & 131071] * (fTemp702 + (-9999.0 - fRec470[0])) + (fRec470[0] + (1e+04 - fTemp702)) * fVec181[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp701 + 1))) & 131071]) * (1.0 - fTemp699);
			fVec182[0] = fTemp703;
			fRec463[0] = 0.995 * fRec463[1] + fTemp703 - fVec182[1];
			double fTemp704 = std::min<double>(std::max<double>(fRec455[0], -1.0), 1.0) + std::min<double>(std::max<double>(fRec463[0], -1.0), 1.0);
			fVec183[0] = fTemp704;
			fRec454[0] = 0.995 * fRec454[1] + 0.125 * (fTemp704 - fVec183[1]);
			output25[i0] = FAUSTFLOAT(std::min<double>(std::max<double>(fRec454[0], -1.0), 1.0));
			fRec478[0] = fTemp3 * (0.43548387096774194 * fTemp7 + 0.1896462018730489 * fTemp9 + 0.631942038446304 * fTemp10 + 0.5215371205795226 * fTemp11 + 0.6599120175960898 * fTemp12 + 0.22498434851654125 * fTemp13 + 0.39935073995566994 * fTemp14 + 0.6813215400624351 * fTemp15 + 0.3792924037460978 * fTemp16 + 0.08258786210600517 * fTemp17 + 0.8201008693900843 * fTemp18 + 0.3303514484240207 * fTemp19 + 0.03596568188487322 * fTemp20 + 0.8984440391718218 * fTemp21 + 0.2877254550789858 * fTemp22 + 0.015662474369218984 * fTemp23 + 0.9426700221131252 * fTemp24 + 0.25059958990750375 * fTemp25 + 0.019981915645726853 * fTemp26 + 0.9511276837859718 * fTemp27 + 0.09980346694349518 * fTemp28 + 0.825422037543483 * fTemp29) + fTemp2 * fRec478[1];
			fRec477[0] = fConst0 * fTemp30 * fRec478[0] * fTemp3 + fTemp2 * fRec477[1];
			int iTemp705 = int(fRec477[0]);
			double fTemp706 = ((fRec473[1] != 0.0) ? (((fRec474[1] > 0.0) & (fRec474[1] < 1.0)) ? fRec473[1] : 0.0) : (((fRec474[1] == 0.0) & (iTemp705 != iRec475[1])) ? 0.00048828125 : (((fRec474[1] == 1.0) & (iTemp705 != iRec476[1])) ? -0.00048828125 : 0.0)));
			fRec473[0] = fTemp706;
			fRec474[0] = std::max<double>(0.0, std::min<double>(1.0, fRec474[1] + fTemp706));
			iRec475[0] = (((fRec474[1] >= 1.0) & (iRec476[1] != iTemp705)) ? iTemp705 : iRec475[1]);
			iRec476[0] = (((fRec474[1] <= 0.0) & (iRec475[1] != iTemp705)) ? iTemp705 : iRec476[1]);
			double fTemp707 = fRec17[0] * fRec478[0] * fTemp35 * (fTemp37 * (fTemp38 * (4.069010416666667e-06 * fTemp39 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec478[0] * fTemp43) + 1.0) + 8.138020833333335e-06 * fTemp44 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec478[0] * fTemp45) + 1.0)) + 8.138020833333335e-06 * fTemp46 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec478[0] * fTemp47) + 1.0)) + 4.069010416666667e-06 * fTemp48 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec478[0] * fTemp49) + 1.0));
			fVec184[IOTA0 & 262143] = fTemp707;
			double fTemp708 = fVec184[(IOTA0 - int(std::min<double>(fConst0, double(std::max<int>(0, iRec475[0]))))) & 262143];
			double fTemp709 = fRec474[0] * (fTemp708 - fVec184[(IOTA0 - int(std::min<double>(fConst0, double(std::max<int>(0, iRec476[0]))))) & 262143]) - fTemp708;
			fVec185[IOTA0 & 131071] = fTemp709;
			fRec479[0] = std::fmod(fRec479[1] + (10001.0 - std::pow(2.0, 0.08333333333333333 * fTemp67 * fRec478[0])), 1e+04);
			int iTemp710 = int(fRec479[0]);
			double fTemp711 = std::floor(fRec479[0]);
			double fTemp712 = std::min<double>(0.0001 * fRec479[0], 1.0);
			double fTemp713 = fRec479[0] + 1e+04;
			int iTemp714 = int(fTemp713);
			double fTemp715 = std::floor(fTemp713);
			double fTemp716 = (fVec185[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp710))) & 131071] * (fTemp711 + (1.0 - fRec479[0])) + (fRec479[0] - fTemp711) * fVec185[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp710 + 1))) & 131071]) * fTemp712 + (fVec185[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp714))) & 131071] * (fTemp715 + (-9999.0 - fRec479[0])) + (fRec479[0] + (1e+04 - fTemp715)) * fVec185[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp714 + 1))) & 131071]) * (1.0 - fTemp712);
			fVec186[0] = fTemp716;
			fRec472[0] = 0.995 * fRec472[1] + fTemp716 - fVec186[1];
			fRec486[0] = fTemp3 * (0.9354838709677419 * fTemp7 + 0.8751300728407907 * fTemp9 + 0.9948693233918952 * fTemp10 + 0.9526942852216432 * fTemp11 + 0.9672041516493516 * fTemp12 + 0.8988316780125678 * fTemp13 + 0.9897649706262472 * fTemp14 + 0.9958376690946931 * fTemp15 + 0.991675338189386 * fTemp16 + 0.8186700681413849 * fTemp17 + 0.9997314625222382 * fTemp18 + 0.9989258500889531 * fTemp19 + 0.7658526443903277 * fTemp20 + 0.9999826750014347 * fTemp21 + 0.9998614000114778 * fTemp22 + 0.7164427963651452 * fTemp23 + 0.999998882258157 * fTemp24 + 0.9999821161305132 * fTemp25 + 0.6394213006632598 * fTemp26 + 0.9984727401183117 * fTemp27 + 0.646630608061183 * fTemp28 + 0.9979166644037433 * fTemp29) + fTemp2 * fRec486[1];
			fRec485[0] = fConst0 * fTemp30 * fRec486[0] * fTemp3 + fTemp2 * fRec485[1];
			int iTemp717 = int(fRec485[0]);
			double fTemp718 = ((fRec481[1] != 0.0) ? (((fRec482[1] > 0.0) & (fRec482[1] < 1.0)) ? fRec481[1] : 0.0) : (((fRec482[1] == 0.0) & (iTemp717 != iRec483[1])) ? 0.00048828125 : (((fRec482[1] == 1.0) & (iTemp717 != iRec484[1])) ? -0.00048828125 : 0.0)));
			fRec481[0] = fTemp718;
			fRec482[0] = std::max<double>(0.0, std::min<double>(1.0, fRec482[1] + fTemp718));
			iRec483[0] = (((fRec482[1] >= 1.0) & (iRec484[1] != iTemp717)) ? iTemp717 : iRec483[1]);
			iRec484[0] = (((fRec482[1] <= 0.0) & (iRec483[1] != iTemp717)) ? iTemp717 : iRec484[1]);
			double fTemp719 = fRec17[0] * fRec486[0] * fTemp35 * (fTemp37 * (fTemp38 * (4.069010416666667e-06 * fTemp39 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec486[0] * fTemp43) + 1.0) + 8.138020833333335e-06 * fTemp44 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec486[0] * fTemp45) + 1.0)) + 8.138020833333335e-06 * fTemp46 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec486[0] * fTemp47) + 1.0)) + 4.069010416666667e-06 * fTemp48 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec486[0] * fTemp49) + 1.0));
			fVec187[IOTA0 & 262143] = fTemp719;
			double fTemp720 = fVec187[(IOTA0 - int(std::min<double>(fConst0, double(std::max<int>(0, iRec483[0]))))) & 262143];
			double fTemp721 = fRec482[0] * (fTemp720 - fVec187[(IOTA0 - int(std::min<double>(fConst0, double(std::max<int>(0, iRec484[0]))))) & 262143]) - fTemp720;
			fVec188[IOTA0 & 131071] = fTemp721;
			fRec487[0] = std::fmod(fRec487[1] + (10001.0 - std::pow(2.0, 0.08333333333333333 * fTemp67 * fRec486[0])), 1e+04);
			int iTemp722 = int(fRec487[0]);
			double fTemp723 = std::floor(fRec487[0]);
			double fTemp724 = std::min<double>(0.0001 * fRec487[0], 1.0);
			double fTemp725 = fRec487[0] + 1e+04;
			int iTemp726 = int(fTemp725);
			double fTemp727 = std::floor(fTemp725);
			double fTemp728 = (fVec188[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp722))) & 131071] * (fTemp723 + (1.0 - fRec487[0])) + (fRec487[0] - fTemp723) * fVec188[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp722 + 1))) & 131071]) * fTemp724 + (fVec188[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp726))) & 131071] * (fTemp727 + (-9999.0 - fRec487[0])) + (fRec487[0] + (1e+04 - fTemp727)) * fVec188[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp726 + 1))) & 131071]) * (1.0 - fTemp724);
			fVec189[0] = fTemp728;
			fRec480[0] = 0.995 * fRec480[1] + fTemp728 - fVec189[1];
			double fTemp729 = std::min<double>(std::max<double>(fRec472[0], -1.0), 1.0) + std::min<double>(std::max<double>(fRec480[0], -1.0), 1.0);
			fVec190[0] = fTemp729;
			fRec471[0] = 0.995 * fRec471[1] + 0.125 * (fTemp729 - fVec190[1]);
			output26[i0] = FAUSTFLOAT(std::min<double>(std::max<double>(fRec471[0], -1.0), 1.0));
			fRec495[0] = fTemp3 * (0.45161290322580644 * fTemp7 + 0.2039542143600416 * fTemp9 + 0.6513724827222223 * fTemp10 + 0.5376567859427995 * fTemp11 + 0.672021505032247 * fTemp12 + 0.24124187730720903 * fTemp13 + 0.42428611124771165 * fTemp14 + 0.6992715920915713 * fTemp15 + 0.4079084287200832 * fTemp16 + 0.09210835487227685 * fTemp17 + 0.8350844214695714 * fTemp18 + 0.3684334194891074 * fTemp19 + 0.041597321555221806 * fTemp20 + 0.9095624246768618 * fTemp21 + 0.33277857244177445 * fTemp22 + 0.018785887153971136 * fTemp23 + 0.9504052006292467 * fTemp24 + 0.30057419446353817 * fTemp25 + 0.022345513868328965 * fTemp26 + 0.9562971562992734 * fTemp27 + 0.10778602026197859 * fTemp28 + 0.8362246062461756 * fTemp29) + fTemp2 * fRec495[1];
			fRec494[0] = fConst0 * fTemp30 * fRec495[0] * fTemp3 + fTemp2 * fRec494[1];
			int iTemp730 = int(fRec494[0]);
			double fTemp731 = ((fRec490[1] != 0.0) ? (((fRec491[1] > 0.0) & (fRec491[1] < 1.0)) ? fRec490[1] : 0.0) : (((fRec491[1] == 0.0) & (iTemp730 != iRec492[1])) ? 0.00048828125 : (((fRec491[1] == 1.0) & (iTemp730 != iRec493[1])) ? -0.00048828125 : 0.0)));
			fRec490[0] = fTemp731;
			fRec491[0] = std::max<double>(0.0, std::min<double>(1.0, fRec491[1] + fTemp731));
			iRec492[0] = (((fRec491[1] >= 1.0) & (iRec493[1] != iTemp730)) ? iTemp730 : iRec492[1]);
			iRec493[0] = (((fRec491[1] <= 0.0) & (iRec492[1] != iTemp730)) ? iTemp730 : iRec493[1]);
			double fTemp732 = fRec17[0] * fRec495[0] * fTemp35 * (fTemp37 * (fTemp38 * (4.069010416666667e-06 * fTemp39 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec495[0] * fTemp43) + 1.0) + 8.138020833333335e-06 * fTemp44 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec495[0] * fTemp45) + 1.0)) + 8.138020833333335e-06 * fTemp46 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec495[0] * fTemp47) + 1.0)) + 4.069010416666667e-06 * fTemp48 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec495[0] * fTemp49) + 1.0));
			fVec191[IOTA0 & 262143] = fTemp732;
			double fTemp733 = fVec191[(IOTA0 - int(std::min<double>(fConst0, double(std::max<int>(0, iRec492[0]))))) & 262143];
			double fTemp734 = fRec491[0] * (fTemp733 - fVec191[(IOTA0 - int(std::min<double>(fConst0, double(std::max<int>(0, iRec493[0]))))) & 262143]) - fTemp733;
			fVec192[IOTA0 & 131071] = fTemp734;
			fRec496[0] = std::fmod(fRec496[1] + (10001.0 - std::pow(2.0, 0.08333333333333333 * fTemp67 * fRec495[0])), 1e+04);
			int iTemp735 = int(fRec496[0]);
			double fTemp736 = std::floor(fRec496[0]);
			double fTemp737 = std::min<double>(0.0001 * fRec496[0], 1.0);
			double fTemp738 = fRec496[0] + 1e+04;
			int iTemp739 = int(fTemp738);
			double fTemp740 = std::floor(fTemp738);
			double fTemp741 = (fVec192[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp735))) & 131071] * (fTemp736 + (1.0 - fRec496[0])) + (fRec496[0] - fTemp736) * fVec192[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp735 + 1))) & 131071]) * fTemp737 + (fVec192[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp739))) & 131071] * (fTemp740 + (-9999.0 - fRec496[0])) + (fRec496[0] + (1e+04 - fTemp740)) * fVec192[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp739 + 1))) & 131071]) * (1.0 - fTemp737);
			fVec193[0] = fTemp741;
			fRec489[0] = 0.995 * fRec489[1] + fTemp741 - fVec193[1];
			fRec503[0] = fTemp3 * (0.9516129032258065 * fTemp7 + 0.9055671175858482 * fTemp9 + 0.9971129134476474 * fTemp10 + 0.9646669268877194 * fTemp11 + 0.9755064854862865 * fTemp12 + 0.9240668857747536 * fTemp13 + 0.9942341621640557 * fTemp14 + 0.9976586888657648 * fTemp15 + 0.9953173777315296 * fTemp16 + 0.8617493538316943 * fTemp17 + 0.9998867107515693 * fTemp18 + 0.999546843006277 * fTemp19 + 0.8200518044527414 * fTemp20 + 0.9999945182621727 * fTemp21 + 0.9999561460973817 * fTemp22 + 0.7803718784308346 * fTemp23 + 0.9999997347546212 * fTemp24 + 0.9999957560739402 * fTemp25 + 0.7150564437865264 * fTemp26 + 0.9986342861343523 * fTemp27 + 0.692700663173264 * fTemp28 + 0.9988286584123249 * fTemp29) + fTemp2 * fRec503[1];
			fRec502[0] = fConst0 * fTemp30 * fRec503[0] * fTemp3 + fTemp2 * fRec502[1];
			int iTemp742 = int(fRec502[0]);
			double fTemp743 = ((fRec498[1] != 0.0) ? (((fRec499[1] > 0.0) & (fRec499[1] < 1.0)) ? fRec498[1] : 0.0) : (((fRec499[1] == 0.0) & (iTemp742 != iRec500[1])) ? 0.00048828125 : (((fRec499[1] == 1.0) & (iTemp742 != iRec501[1])) ? -0.00048828125 : 0.0)));
			fRec498[0] = fTemp743;
			fRec499[0] = std::max<double>(0.0, std::min<double>(1.0, fRec499[1] + fTemp743));
			iRec500[0] = (((fRec499[1] >= 1.0) & (iRec501[1] != iTemp742)) ? iTemp742 : iRec500[1]);
			iRec501[0] = (((fRec499[1] <= 0.0) & (iRec500[1] != iTemp742)) ? iTemp742 : iRec501[1]);
			double fTemp744 = fRec17[0] * fRec503[0] * fTemp35 * (fTemp37 * (fTemp38 * (4.069010416666667e-06 * fTemp39 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec503[0] * fTemp43) + 1.0) + 8.138020833333335e-06 * fTemp44 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec503[0] * fTemp45) + 1.0)) + 8.138020833333335e-06 * fTemp46 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec503[0] * fTemp47) + 1.0)) + 4.069010416666667e-06 * fTemp48 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec503[0] * fTemp49) + 1.0));
			fVec194[IOTA0 & 262143] = fTemp744;
			double fTemp745 = fVec194[(IOTA0 - int(std::min<double>(fConst0, double(std::max<int>(0, iRec500[0]))))) & 262143];
			double fTemp746 = fRec499[0] * (fTemp745 - fVec194[(IOTA0 - int(std::min<double>(fConst0, double(std::max<int>(0, iRec501[0]))))) & 262143]) - fTemp745;
			fVec195[IOTA0 & 131071] = fTemp746;
			fRec504[0] = std::fmod(fRec504[1] + (10001.0 - std::pow(2.0, 0.08333333333333333 * fTemp67 * fRec503[0])), 1e+04);
			int iTemp747 = int(fRec504[0]);
			double fTemp748 = std::floor(fRec504[0]);
			double fTemp749 = std::min<double>(0.0001 * fRec504[0], 1.0);
			double fTemp750 = fRec504[0] + 1e+04;
			int iTemp751 = int(fTemp750);
			double fTemp752 = std::floor(fTemp750);
			double fTemp753 = (fVec195[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp747))) & 131071] * (fTemp748 + (1.0 - fRec504[0])) + (fRec504[0] - fTemp748) * fVec195[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp747 + 1))) & 131071]) * fTemp749 + (fVec195[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp751))) & 131071] * (fTemp752 + (-9999.0 - fRec504[0])) + (fRec504[0] + (1e+04 - fTemp752)) * fVec195[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp751 + 1))) & 131071]) * (1.0 - fTemp749);
			fVec196[0] = fTemp753;
			fRec497[0] = 0.995 * fRec497[1] + fTemp753 - fVec196[1];
			double fTemp754 = std::min<double>(std::max<double>(fRec489[0], -1.0), 1.0) + std::min<double>(std::max<double>(fRec497[0], -1.0), 1.0);
			fVec197[0] = fTemp754;
			fRec488[0] = 0.995 * fRec488[1] + 0.125 * (fTemp754 - fVec197[1]);
			output27[i0] = FAUSTFLOAT(std::min<double>(std::max<double>(fRec488[0], -1.0), 1.0));
			fRec512[0] = fTemp3 * (0.46774193548387094 * fTemp7 + 0.21878251821019767 * fTemp9 + 0.6703848439562785 * fTemp10 + 0.553598329811821 * fTemp11 + 0.6839166144230384 * fTemp12 + 0.2579864145490892 * fTemp13 + 0.4494158390062839 * fTemp14 + 0.7167013527575443 * fTemp15 + 0.43756503642039535 * fTemp16 + 0.10233375851767311 * fTemp17 + 0.8492120103386929 * fTemp18 + 0.40933503407069244 * fTemp19 + 0.047865790274395484 * fTemp20 + 0.9197418764705946 * fTemp21 + 0.38292632219516387 * fTemp22 + 0.022388837386410787 * fTemp23 + 0.9572819665085424 * fTemp24 + 0.3582213981825726 * fTemp25 + 0.024988694722393255 * fTemp26 + 0.9609198275120442 * fTemp27 + 0.11613491878578985 * fTemp28 + 0.846582159484562 * fTemp29) + fTemp2 * fRec512[1];
			fRec511[0] = fConst0 * fTemp30 * fRec512[0] * fTemp3 + fTemp2 * fRec511[1];
			int iTemp755 = int(fRec511[0]);
			double fTemp756 = ((fRec507[1] != 0.0) ? (((fRec508[1] > 0.0) & (fRec508[1] < 1.0)) ? fRec507[1] : 0.0) : (((fRec508[1] == 0.0) & (iTemp755 != iRec509[1])) ? 0.00048828125 : (((fRec508[1] == 1.0) & (iTemp755 != iRec510[1])) ? -0.00048828125 : 0.0)));
			fRec507[0] = fTemp756;
			fRec508[0] = std::max<double>(0.0, std::min<double>(1.0, fRec508[1] + fTemp756));
			iRec509[0] = (((fRec508[1] >= 1.0) & (iRec510[1] != iTemp755)) ? iTemp755 : iRec509[1]);
			iRec510[0] = (((fRec508[1] <= 0.0) & (iRec509[1] != iTemp755)) ? iTemp755 : iRec510[1]);
			double fTemp757 = fRec17[0] * fRec512[0] * fTemp35 * (fTemp37 * (fTemp38 * (4.069010416666667e-06 * fTemp39 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec512[0] * fTemp43) + 1.0) + 8.138020833333335e-06 * fTemp44 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec512[0] * fTemp45) + 1.0)) + 8.138020833333335e-06 * fTemp46 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec512[0] * fTemp47) + 1.0)) + 4.069010416666667e-06 * fTemp48 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec512[0] * fTemp49) + 1.0));
			fVec198[IOTA0 & 262143] = fTemp757;
			double fTemp758 = fVec198[(IOTA0 - int(std::min<double>(fConst0, double(std::max<int>(0, iRec509[0]))))) & 262143];
			double fTemp759 = fRec508[0] * (fTemp758 - fVec198[(IOTA0 - int(std::min<double>(fConst0, double(std::max<int>(0, iRec510[0]))))) & 262143]) - fTemp758;
			fVec199[IOTA0 & 131071] = fTemp759;
			fRec513[0] = std::fmod(fRec513[1] + (10001.0 - std::pow(2.0, 0.08333333333333333 * fTemp67 * fRec512[0])), 1e+04);
			int iTemp760 = int(fRec513[0]);
			double fTemp761 = std::floor(fRec513[0]);
			double fTemp762 = std::min<double>(0.0001 * fRec513[0], 1.0);
			double fTemp763 = fRec513[0] + 1e+04;
			int iTemp764 = int(fTemp763);
			double fTemp765 = std::floor(fTemp763);
			double fTemp766 = (fVec199[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp760))) & 131071] * (fTemp761 + (1.0 - fRec513[0])) + (fRec513[0] - fTemp761) * fVec199[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp760 + 1))) & 131071]) * fTemp762 + (fVec199[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp764))) & 131071] * (fTemp765 + (-9999.0 - fRec513[0])) + (fRec513[0] + (1e+04 - fTemp765)) * fVec199[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp764 + 1))) & 131071]) * (1.0 - fTemp762);
			fVec200[0] = fTemp766;
			fRec506[0] = 0.995 * fRec506[1] + fTemp766 - fVec200[1];
			fRec520[0] = fTemp3 * (0.967741935483871 * fTemp7 + 0.9365244536940688 * fTemp9 + 0.9987165071710528 * fTemp10 + 0.976541027176011 * fTemp11 + 0.9837387536759294 * fTemp12 + 0.949350831161287 * fTemp13 + 0.9974346616959475 * fTemp14 + 0.9989594172736732 * fTemp15 + 0.9979188345473465 * fTemp16 + 0.906313987445873 * fTemp17 + 0.9999664328152797 * fTemp18 + 0.9998657312611191 * fTemp19 + 0.8770780523669739 * fTemp20 + 0.9999989171875897 * fTemp21 + 0.9999913375007173 * fTemp22 + 0.8487852119680392 * fTemp23 + 0.9999999650705674 * fTemp24 + 0.9999994411290786 * fTemp25 + 0.7996382311165842 * fTemp26 + 0.9987787446097514 * fTemp27 + 0.7480564620675274 * fTemp28 + 0.9994795732148173 * fTemp29) + fTemp2 * fRec520[1];
			fRec519[0] = fConst0 * fTemp30 * fRec520[0] * fTemp3 + fTemp2 * fRec519[1];
			int iTemp767 = int(fRec519[0]);
			double fTemp768 = ((fRec515[1] != 0.0) ? (((fRec516[1] > 0.0) & (fRec516[1] < 1.0)) ? fRec515[1] : 0.0) : (((fRec516[1] == 0.0) & (iTemp767 != iRec517[1])) ? 0.00048828125 : (((fRec516[1] == 1.0) & (iTemp767 != iRec518[1])) ? -0.00048828125 : 0.0)));
			fRec515[0] = fTemp768;
			fRec516[0] = std::max<double>(0.0, std::min<double>(1.0, fRec516[1] + fTemp768));
			iRec517[0] = (((fRec516[1] >= 1.0) & (iRec518[1] != iTemp767)) ? iTemp767 : iRec517[1]);
			iRec518[0] = (((fRec516[1] <= 0.0) & (iRec517[1] != iTemp767)) ? iTemp767 : iRec518[1]);
			double fTemp769 = fRec17[0] * fRec520[0] * fTemp35 * (fTemp37 * (fTemp38 * (8.138020833333335e-06 * fTemp44 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec520[0] * fTemp45) + 1.0) + 4.069010416666667e-06 * fTemp39 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec520[0] * fTemp43) + 1.0)) + 8.138020833333335e-06 * fTemp46 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec520[0] * fTemp47) + 1.0)) + 4.069010416666667e-06 * fTemp48 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec520[0] * fTemp49) + 1.0));
			fVec201[IOTA0 & 262143] = fTemp769;
			double fTemp770 = fVec201[(IOTA0 - int(std::min<double>(fConst0, double(std::max<int>(0, iRec517[0]))))) & 262143];
			double fTemp771 = fRec516[0] * (fTemp770 - fVec201[(IOTA0 - int(std::min<double>(fConst0, double(std::max<int>(0, iRec518[0]))))) & 262143]) - fTemp770;
			fVec202[IOTA0 & 131071] = fTemp771;
			fRec521[0] = std::fmod(fRec521[1] + (10001.0 - std::pow(2.0, 0.08333333333333333 * fTemp67 * fRec520[0])), 1e+04);
			int iTemp772 = int(fRec521[0]);
			double fTemp773 = std::floor(fRec521[0]);
			double fTemp774 = std::min<double>(0.0001 * fRec521[0], 1.0);
			double fTemp775 = fRec521[0] + 1e+04;
			int iTemp776 = int(fTemp775);
			double fTemp777 = std::floor(fTemp775);
			double fTemp778 = (fVec202[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp772))) & 131071] * (fTemp773 + (1.0 - fRec521[0])) + (fRec521[0] - fTemp773) * fVec202[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp772 + 1))) & 131071]) * fTemp774 + (fVec202[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp776))) & 131071] * (fTemp777 + (-9999.0 - fRec521[0])) + (fRec521[0] + (1e+04 - fTemp777)) * fVec202[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp776 + 1))) & 131071]) * (1.0 - fTemp774);
			fVec203[0] = fTemp778;
			fRec514[0] = 0.995 * fRec514[1] + fTemp778 - fVec203[1];
			double fTemp779 = std::min<double>(std::max<double>(fRec506[0], -1.0), 1.0) + std::min<double>(std::max<double>(fRec514[0], -1.0), 1.0);
			fVec204[0] = fTemp779;
			fRec505[0] = 0.995 * fRec505[1] + 0.125 * (fTemp779 - fVec204[1]);
			output28[i0] = FAUSTFLOAT(std::min<double>(std::max<double>(fRec505[0], -1.0), 1.0));
			fRec529[0] = fTemp3 * (0.4838709677419355 * fTemp7 + 0.2341311134235172 * fTemp9 + 0.6889669190756865 * fTemp10 + 0.5693656456701377 * fTemp11 + 0.6956083436402524 * fTemp12 + 0.2752072127708799 * fTemp13 + 0.4746754155806435 * fTemp14 + 0.7336108220603539 * fTemp15 + 0.4682622268470344 * fTemp16 + 0.11328924843073412 * fTemp17 + 0.8625088113859891 * fTemp18 + 0.4531569937229365 * fTemp19 + 0.05481737827293587 * fTemp20 + 0.9290368058766395 * fTemp21 + 0.43853902618348695 * fTemp22 + 0.026524537874001226 * fTemp23 + 0.9633738352911688 * fTemp24 + 0.4243926059840196 * fTemp25 + 0.027944529161801766 * fTemp26 + 0.9650535353683865 * fTemp27 + 0.12486064733867508 * fTemp28 + 0.8565108417646293 * fTemp29) + fTemp2 * fRec529[1];
			fRec528[0] = fConst0 * fTemp30 * fRec529[0] * fTemp3 + fTemp2 * fRec528[1];
			int iTemp780 = int(fRec528[0]);
			double fTemp781 = ((fRec524[1] != 0.0) ? (((fRec525[1] > 0.0) & (fRec525[1] < 1.0)) ? fRec524[1] : 0.0) : (((fRec525[1] == 0.0) & (iTemp780 != iRec526[1])) ? 0.00048828125 : (((fRec525[1] == 1.0) & (iTemp780 != iRec527[1])) ? -0.00048828125 : 0.0)));
			fRec524[0] = fTemp781;
			fRec525[0] = std::max<double>(0.0, std::min<double>(1.0, fRec525[1] + fTemp781));
			iRec526[0] = (((fRec525[1] >= 1.0) & (iRec527[1] != iTemp780)) ? iTemp780 : iRec526[1]);
			iRec527[0] = (((fRec525[1] <= 0.0) & (iRec526[1] != iTemp780)) ? iTemp780 : iRec527[1]);
			double fTemp782 = fRec17[0] * fRec529[0] * fTemp35 * (fTemp37 * (fTemp38 * (4.069010416666667e-06 * fTemp39 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec529[0] * fTemp43) + 1.0) + 8.138020833333335e-06 * fTemp44 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec529[0] * fTemp45) + 1.0)) + 8.138020833333335e-06 * fTemp46 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec529[0] * fTemp47) + 1.0)) + 4.069010416666667e-06 * fTemp48 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec529[0] * fTemp49) + 1.0));
			fVec205[IOTA0 & 262143] = fTemp782;
			double fTemp783 = fVec205[(IOTA0 - int(std::min<double>(fConst0, double(std::max<int>(0, iRec526[0]))))) & 262143];
			double fTemp784 = fRec525[0] * (fTemp783 - fVec205[(IOTA0 - int(std::min<double>(fConst0, double(std::max<int>(0, iRec527[0]))))) & 262143]) - fTemp783;
			fVec206[IOTA0 & 131071] = fTemp784;
			fRec530[0] = std::fmod(fRec530[1] + (10001.0 - std::pow(2.0, 0.08333333333333333 * fTemp67 * fRec529[0])), 1e+04);
			int iTemp785 = int(fRec530[0]);
			double fTemp786 = std::floor(fRec530[0]);
			double fTemp787 = std::min<double>(0.0001 * fRec530[0], 1.0);
			double fTemp788 = fRec530[0] + 1e+04;
			int iTemp789 = int(fTemp788);
			double fTemp790 = std::floor(fTemp788);
			double fTemp791 = (fVec206[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp785))) & 131071] * (fTemp786 + (1.0 - fRec530[0])) + (fRec530[0] - fTemp786) * fVec206[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp785 + 1))) & 131071]) * fTemp787 + (fVec206[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp789))) & 131071] * (fTemp790 + (-9999.0 - fRec530[0])) + (fRec530[0] + (1e+04 - fTemp790)) * fVec206[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp789 + 1))) & 131071]) * (1.0 - fTemp787);
			fVec207[0] = fTemp791;
			fRec523[0] = 0.995 * fRec523[1] + fTemp791 - fVec207[1];
			fRec537[0] = fTemp3 * (0.9838709677419355 * fTemp7 + 0.9680020811654527 * fTemp9 + 0.9996790752964305 * fTemp10 + 0.9883181949523646 * fTemp11 + 0.9919027007433419 * fTemp12 + 0.9746672856868122 * fTemp13 + 0.9993582535855263 * fTemp14 + 0.9997398543184183 * fTemp15 + 0.9994797086368367 * fTemp16 + 0.9523891443724615 * fTemp17 + 0.99999580410191 * fTemp18 + 0.9999832164076399 * fTemp19 + 0.9370280291406476 * fTemp20 + 0.9999999323242243 * fTemp21 + 0.9999994585937948 * fTemp22 + 0.9219146738319275 * fTemp23 + 0.9999999989084553 * fTemp24 + 0.9999999825352837 * fTemp25 + 0.894224933177656 * fTemp26 + 0.9989079229802621 * fTemp27 + 0.8211203789288806 * fTemp28 + 0.9998699186986366 * fTemp29) + fTemp2 * fRec537[1];
			fRec536[0] = fConst0 * fTemp30 * fRec537[0] * fTemp3 + fTemp2 * fRec536[1];
			int iTemp792 = int(fRec536[0]);
			double fTemp793 = ((fRec532[1] != 0.0) ? (((fRec533[1] > 0.0) & (fRec533[1] < 1.0)) ? fRec532[1] : 0.0) : (((fRec533[1] == 0.0) & (iTemp792 != iRec534[1])) ? 0.00048828125 : (((fRec533[1] == 1.0) & (iTemp792 != iRec535[1])) ? -0.00048828125 : 0.0)));
			fRec532[0] = fTemp793;
			fRec533[0] = std::max<double>(0.0, std::min<double>(1.0, fRec533[1] + fTemp793));
			iRec534[0] = (((fRec533[1] >= 1.0) & (iRec535[1] != iTemp792)) ? iTemp792 : iRec534[1]);
			iRec535[0] = (((fRec533[1] <= 0.0) & (iRec534[1] != iTemp792)) ? iTemp792 : iRec535[1]);
			double fTemp794 = fRec17[0] * fRec537[0] * fTemp35 * (fTemp37 * (fTemp38 * (4.069010416666667e-06 * fTemp39 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec537[0] * fTemp43) + 1.0) + 8.138020833333335e-06 * fTemp44 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec537[0] * fTemp45) + 1.0)) + 8.138020833333335e-06 * fTemp46 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec537[0] * fTemp47) + 1.0)) + 4.069010416666667e-06 * fTemp48 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec537[0] * fTemp49) + 1.0));
			fVec208[IOTA0 & 262143] = fTemp794;
			double fTemp795 = fVec208[(IOTA0 - int(std::min<double>(fConst0, double(std::max<int>(0, iRec534[0]))))) & 262143];
			double fTemp796 = fRec533[0] * (fTemp795 - fVec208[(IOTA0 - int(std::min<double>(fConst0, double(std::max<int>(0, iRec535[0]))))) & 262143]) - fTemp795;
			fVec209[IOTA0 & 131071] = fTemp796;
			fRec538[0] = std::fmod(fRec538[1] + (10001.0 - std::pow(2.0, 0.08333333333333333 * fTemp67 * fRec537[0])), 1e+04);
			int iTemp797 = int(fRec538[0]);
			double fTemp798 = std::floor(fRec538[0]);
			double fTemp799 = std::min<double>(0.0001 * fRec538[0], 1.0);
			double fTemp800 = fRec538[0] + 1e+04;
			int iTemp801 = int(fTemp800);
			double fTemp802 = std::floor(fTemp800);
			double fTemp803 = (fVec209[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp797))) & 131071] * (fTemp798 + (1.0 - fRec538[0])) + (fRec538[0] - fTemp798) * fVec209[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp797 + 1))) & 131071]) * fTemp799 + (fVec209[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp801))) & 131071] * (fTemp802 + (-9999.0 - fRec538[0])) + (fRec538[0] + (1e+04 - fTemp802)) * fVec209[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp801 + 1))) & 131071]) * (1.0 - fTemp799);
			fVec210[0] = fTemp803;
			fRec531[0] = 0.995 * fRec531[1] + fTemp803 - fVec210[1];
			double fTemp804 = std::min<double>(std::max<double>(fRec523[0], -1.0), 1.0) + std::min<double>(std::max<double>(fRec531[0], -1.0), 1.0);
			fVec211[0] = fTemp804;
			fRec522[0] = 0.995 * fRec522[1] + 0.125 * (fTemp804 - fVec211[1]);
			output29[i0] = FAUSTFLOAT(std::min<double>(std::max<double>(fRec522[0], -1.0), 1.0));
			fRec546[0] = fTemp3 * (0.96875 * (fTemp24 + fTemp27) + 0.03125 * (fTemp23 + fTemp26) + 0.5 * (fTemp25 + fTemp22 + fTemp19 + fTemp16 + fTemp7 + fTemp14) + 0.2928932188134524 * fTemp13 + 0.7071067811865476 * fTemp12 + 0.5849625007211562 * fTemp11 + 0.25 * fTemp9 + 0.7071067811865475 * fTemp10 + 0.75 * fTemp15 + 0.125 * fTemp17 + 0.875 * fTemp18 + 0.0625 * fTemp20 + 0.9375 * fTemp21 + 0.1339745962155614 * fTemp28 + 0.8660254037844386 * fTemp29) + fTemp2 * fRec546[1];
			fRec545[0] = fConst0 * fTemp30 * fRec546[0] * fTemp3 + fTemp2 * fRec545[1];
			int iTemp805 = int(fRec545[0]);
			double fTemp806 = ((fRec541[1] != 0.0) ? (((fRec542[1] > 0.0) & (fRec542[1] < 1.0)) ? fRec541[1] : 0.0) : (((fRec542[1] == 0.0) & (iTemp805 != iRec543[1])) ? 0.00048828125 : (((fRec542[1] == 1.0) & (iTemp805 != iRec544[1])) ? -0.00048828125 : 0.0)));
			fRec541[0] = fTemp806;
			fRec542[0] = std::max<double>(0.0, std::min<double>(1.0, fRec542[1] + fTemp806));
			iRec543[0] = (((fRec542[1] >= 1.0) & (iRec544[1] != iTemp805)) ? iTemp805 : iRec543[1]);
			iRec544[0] = (((fRec542[1] <= 0.0) & (iRec543[1] != iTemp805)) ? iTemp805 : iRec544[1]);
			double fTemp807 = fRec17[0] * fRec546[0] * fTemp35 * (fTemp37 * (fTemp38 * (8.138020833333335e-06 * fTemp44 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec546[0] * fTemp45) + 1.0) + 4.069010416666667e-06 * fTemp39 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec546[0] * fTemp43) + 1.0)) + 8.138020833333335e-06 * fTemp46 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec546[0] * fTemp47) + 1.0)) + 4.069010416666667e-06 * fTemp48 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec546[0] * fTemp49) + 1.0));
			fVec212[IOTA0 & 262143] = fTemp807;
			double fTemp808 = fVec212[(IOTA0 - int(std::min<double>(fConst0, double(std::max<int>(0, iRec543[0]))))) & 262143];
			double fTemp809 = fRec542[0] * (fTemp808 - fVec212[(IOTA0 - int(std::min<double>(fConst0, double(std::max<int>(0, iRec544[0]))))) & 262143]) - fTemp808;
			fVec213[IOTA0 & 131071] = fTemp809;
			fRec547[0] = std::fmod(fRec547[1] + (10001.0 - std::pow(2.0, 0.08333333333333333 * fTemp67 * fRec546[0])), 1e+04);
			int iTemp810 = int(fRec547[0]);
			double fTemp811 = std::floor(fRec547[0]);
			double fTemp812 = std::min<double>(0.0001 * fRec547[0], 1.0);
			double fTemp813 = fRec547[0] + 1e+04;
			int iTemp814 = int(fTemp813);
			double fTemp815 = std::floor(fTemp813);
			double fTemp816 = (fVec213[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp810))) & 131071] * (fTemp811 + (1.0 - fRec547[0])) + (fRec547[0] - fTemp811) * fVec213[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp810 + 1))) & 131071]) * fTemp812 + (fVec213[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp814))) & 131071] * (fTemp815 + (-9999.0 - fRec547[0])) + (fRec547[0] + (1e+04 - fTemp815)) * fVec213[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp814 + 1))) & 131071]) * (1.0 - fTemp812);
			fVec214[0] = fTemp816;
			fRec540[0] = 0.995 * fRec540[1] + fTemp816 - fVec214[1];
			fRec554[0] = fTemp3 * (double(iTemp6 + iTemp8) + fTemp29 + fTemp28 + fTemp27 + fTemp26 + fTemp25 + fTemp24 + fTemp23 + fTemp22 + fTemp21 + fTemp20 + fTemp19 + fTemp18 + fTemp17 + fTemp16 + fTemp15 + fTemp14 + fTemp13 + fTemp12 + fTemp10 + fTemp11) + fTemp2 * fRec554[1];
			fRec553[0] = fConst0 * fTemp30 * fRec554[0] * fTemp3 + fTemp2 * fRec553[1];
			int iTemp817 = int(fRec553[0]);
			double fTemp818 = ((fRec549[1] != 0.0) ? (((fRec550[1] > 0.0) & (fRec550[1] < 1.0)) ? fRec549[1] : 0.0) : (((fRec550[1] == 0.0) & (iTemp817 != iRec551[1])) ? 0.00048828125 : (((fRec550[1] == 1.0) & (iTemp817 != iRec552[1])) ? -0.00048828125 : 0.0)));
			fRec549[0] = fTemp818;
			fRec550[0] = std::max<double>(0.0, std::min<double>(1.0, fRec550[1] + fTemp818));
			iRec551[0] = (((fRec550[1] >= 1.0) & (iRec552[1] != iTemp817)) ? iTemp817 : iRec551[1]);
			iRec552[0] = (((fRec550[1] <= 0.0) & (iRec551[1] != iTemp817)) ? iTemp817 : iRec552[1]);
			double fTemp819 = fRec17[0] * fRec554[0] * fTemp35 * (fTemp37 * (fTemp38 * (4.069010416666667e-06 * fTemp39 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec554[0] * fTemp43) + 1.0) + 8.138020833333335e-06 * fTemp44 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec554[0] * fTemp45) + 1.0)) + 8.138020833333335e-06 * fTemp46 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec554[0] * fTemp47) + 1.0)) + 4.069010416666667e-06 * fTemp48 * bbdmi_ambLagrangeMods31_faustpower2_f(std::sin(fRec554[0] * fTemp49) + 1.0));
			fVec215[IOTA0 & 262143] = fTemp819;
			double fTemp820 = fVec215[(IOTA0 - int(std::min<double>(fConst0, double(std::max<int>(0, iRec551[0]))))) & 262143];
			double fTemp821 = fRec550[0] * (fTemp820 - fVec215[(IOTA0 - int(std::min<double>(fConst0, double(std::max<int>(0, iRec552[0]))))) & 262143]) - fTemp820;
			fVec216[IOTA0 & 131071] = fTemp821;
			fRec555[0] = std::fmod(fRec555[1] + (10001.0 - std::pow(2.0, 0.08333333333333333 * fTemp67 * fRec554[0])), 1e+04);
			int iTemp822 = int(fRec555[0]);
			double fTemp823 = std::floor(fRec555[0]);
			double fTemp824 = std::min<double>(0.0001 * fRec555[0], 1.0);
			double fTemp825 = fRec555[0] + 1e+04;
			int iTemp826 = int(fTemp825);
			double fTemp827 = std::floor(fTemp825);
			double fTemp828 = (fVec216[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp822))) & 131071] * (fTemp823 + (1.0 - fRec555[0])) + (fRec555[0] - fTemp823) * fVec216[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp822 + 1))) & 131071]) * fTemp824 + (fVec216[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp826))) & 131071] * (fTemp827 + (-9999.0 - fRec555[0])) + (fRec555[0] + (1e+04 - fTemp827)) * fVec216[(IOTA0 - std::min<int>(65537, std::max<int>(0, iTemp826 + 1))) & 131071]) * (1.0 - fTemp824);
			fVec217[0] = fTemp828;
			fRec548[0] = 0.995 * fRec548[1] + fTemp828 - fVec217[1];
			double fTemp829 = std::min<double>(std::max<double>(fRec540[0], -1.0), 1.0) + std::min<double>(std::max<double>(fRec548[0], -1.0), 1.0);
			fVec218[0] = fTemp829;
			fRec539[0] = 0.995 * fRec539[1] + 0.125 * (fTemp829 - fVec218[1]);
			output30[i0] = FAUSTFLOAT(std::min<double>(std::max<double>(fRec539[0], -1.0), 1.0));
			iVec0[1] = iVec0[0];
			fRec8[1] = fRec8[0];
			fRec9[1] = fRec9[0];
			fRec11[1] = fRec11[0];
			fRec10[1] = fRec10[0];
			iRec14[1] = iRec14[0];
			fRec13[1] = fRec13[0];
			fRec12[1] = fRec12[0];
			fRec7[1] = fRec7[0];
			fRec15[1] = fRec15[0];
			fRec16[1] = fRec16[0];
			fRec6[1] = fRec6[0];
			fRec2[1] = fRec2[0];
			fRec3[1] = fRec3[0];
			iRec4[1] = iRec4[0];
			iRec5[1] = iRec5[0];
			fRec18[1] = fRec18[0];
			fRec19[1] = fRec19[0];
			fRec20[1] = fRec20[0];
			fRec17[1] = fRec17[0];
			fRec21[1] = fRec21[0];
			fRec22[1] = fRec22[0];
			fRec25[1] = fRec25[0];
			fRec26[1] = fRec26[0];
			fRec24[1] = fRec24[0];
			fRec27[1] = fRec27[0];
			fRec28[1] = fRec28[0];
			fRec29[1] = fRec29[0];
			fRec30[1] = fRec30[0];
			fRec31[1] = fRec31[0];
			fRec32[1] = fRec32[0];
			IOTA0 = IOTA0 + 1;
			fRec34[1] = fRec34[0];
			fRec35[1] = fRec35[0];
			fRec36[1] = fRec36[0];
			fRec37[1] = fRec37[0];
			fRec33[1] = fRec33[0];
			fVec4[1] = fVec4[0];
			fRec1[1] = fRec1[0];
			fRec44[1] = fRec44[0];
			fRec43[1] = fRec43[0];
			fRec39[1] = fRec39[0];
			fRec40[1] = fRec40[0];
			iRec41[1] = iRec41[0];
			iRec42[1] = iRec42[0];
			fRec45[1] = fRec45[0];
			fVec7[1] = fVec7[0];
			fRec38[1] = fRec38[0];
			fVec8[1] = fVec8[0];
			fRec0[1] = fRec0[0];
			fRec53[1] = fRec53[0];
			fRec52[1] = fRec52[0];
			fRec48[1] = fRec48[0];
			fRec49[1] = fRec49[0];
			iRec50[1] = iRec50[0];
			iRec51[1] = iRec51[0];
			fRec54[1] = fRec54[0];
			fVec11[1] = fVec11[0];
			fRec47[1] = fRec47[0];
			fRec61[1] = fRec61[0];
			fRec60[1] = fRec60[0];
			fRec56[1] = fRec56[0];
			fRec57[1] = fRec57[0];
			iRec58[1] = iRec58[0];
			iRec59[1] = iRec59[0];
			fRec62[1] = fRec62[0];
			fVec14[1] = fVec14[0];
			fRec55[1] = fRec55[0];
			fVec15[1] = fVec15[0];
			fRec46[1] = fRec46[0];
			fRec70[1] = fRec70[0];
			fRec69[1] = fRec69[0];
			fRec65[1] = fRec65[0];
			fRec66[1] = fRec66[0];
			iRec67[1] = iRec67[0];
			iRec68[1] = iRec68[0];
			fRec71[1] = fRec71[0];
			fVec18[1] = fVec18[0];
			fRec64[1] = fRec64[0];
			fRec78[1] = fRec78[0];
			fRec77[1] = fRec77[0];
			fRec73[1] = fRec73[0];
			fRec74[1] = fRec74[0];
			iRec75[1] = iRec75[0];
			iRec76[1] = iRec76[0];
			fRec79[1] = fRec79[0];
			fVec21[1] = fVec21[0];
			fRec72[1] = fRec72[0];
			fVec22[1] = fVec22[0];
			fRec63[1] = fRec63[0];
			fRec87[1] = fRec87[0];
			fRec86[1] = fRec86[0];
			fRec82[1] = fRec82[0];
			fRec83[1] = fRec83[0];
			iRec84[1] = iRec84[0];
			iRec85[1] = iRec85[0];
			fRec88[1] = fRec88[0];
			fVec25[1] = fVec25[0];
			fRec81[1] = fRec81[0];
			fRec95[1] = fRec95[0];
			fRec94[1] = fRec94[0];
			fRec90[1] = fRec90[0];
			fRec91[1] = fRec91[0];
			iRec92[1] = iRec92[0];
			iRec93[1] = iRec93[0];
			fRec96[1] = fRec96[0];
			fVec28[1] = fVec28[0];
			fRec89[1] = fRec89[0];
			fVec29[1] = fVec29[0];
			fRec80[1] = fRec80[0];
			fRec104[1] = fRec104[0];
			fRec103[1] = fRec103[0];
			fRec99[1] = fRec99[0];
			fRec100[1] = fRec100[0];
			iRec101[1] = iRec101[0];
			iRec102[1] = iRec102[0];
			fRec105[1] = fRec105[0];
			fVec32[1] = fVec32[0];
			fRec98[1] = fRec98[0];
			fRec112[1] = fRec112[0];
			fRec111[1] = fRec111[0];
			fRec107[1] = fRec107[0];
			fRec108[1] = fRec108[0];
			iRec109[1] = iRec109[0];
			iRec110[1] = iRec110[0];
			fRec113[1] = fRec113[0];
			fVec35[1] = fVec35[0];
			fRec106[1] = fRec106[0];
			fVec36[1] = fVec36[0];
			fRec97[1] = fRec97[0];
			fRec121[1] = fRec121[0];
			fRec120[1] = fRec120[0];
			fRec116[1] = fRec116[0];
			fRec117[1] = fRec117[0];
			iRec118[1] = iRec118[0];
			iRec119[1] = iRec119[0];
			fRec122[1] = fRec122[0];
			fVec39[1] = fVec39[0];
			fRec115[1] = fRec115[0];
			fRec129[1] = fRec129[0];
			fRec128[1] = fRec128[0];
			fRec124[1] = fRec124[0];
			fRec125[1] = fRec125[0];
			iRec126[1] = iRec126[0];
			iRec127[1] = iRec127[0];
			fRec130[1] = fRec130[0];
			fVec42[1] = fVec42[0];
			fRec123[1] = fRec123[0];
			fVec43[1] = fVec43[0];
			fRec114[1] = fRec114[0];
			fRec138[1] = fRec138[0];
			fRec137[1] = fRec137[0];
			fRec133[1] = fRec133[0];
			fRec134[1] = fRec134[0];
			iRec135[1] = iRec135[0];
			iRec136[1] = iRec136[0];
			fRec139[1] = fRec139[0];
			fVec46[1] = fVec46[0];
			fRec132[1] = fRec132[0];
			fRec146[1] = fRec146[0];
			fRec145[1] = fRec145[0];
			fRec141[1] = fRec141[0];
			fRec142[1] = fRec142[0];
			iRec143[1] = iRec143[0];
			iRec144[1] = iRec144[0];
			fRec147[1] = fRec147[0];
			fVec49[1] = fVec49[0];
			fRec140[1] = fRec140[0];
			fVec50[1] = fVec50[0];
			fRec131[1] = fRec131[0];
			fRec155[1] = fRec155[0];
			fRec154[1] = fRec154[0];
			fRec150[1] = fRec150[0];
			fRec151[1] = fRec151[0];
			iRec152[1] = iRec152[0];
			iRec153[1] = iRec153[0];
			fRec156[1] = fRec156[0];
			fVec53[1] = fVec53[0];
			fRec149[1] = fRec149[0];
			fRec163[1] = fRec163[0];
			fRec162[1] = fRec162[0];
			fRec158[1] = fRec158[0];
			fRec159[1] = fRec159[0];
			iRec160[1] = iRec160[0];
			iRec161[1] = iRec161[0];
			fRec164[1] = fRec164[0];
			fVec56[1] = fVec56[0];
			fRec157[1] = fRec157[0];
			fVec57[1] = fVec57[0];
			fRec148[1] = fRec148[0];
			fRec172[1] = fRec172[0];
			fRec171[1] = fRec171[0];
			fRec167[1] = fRec167[0];
			fRec168[1] = fRec168[0];
			iRec169[1] = iRec169[0];
			iRec170[1] = iRec170[0];
			fRec173[1] = fRec173[0];
			fVec60[1] = fVec60[0];
			fRec166[1] = fRec166[0];
			fRec180[1] = fRec180[0];
			fRec179[1] = fRec179[0];
			fRec175[1] = fRec175[0];
			fRec176[1] = fRec176[0];
			iRec177[1] = iRec177[0];
			iRec178[1] = iRec178[0];
			fRec181[1] = fRec181[0];
			fVec63[1] = fVec63[0];
			fRec174[1] = fRec174[0];
			fVec64[1] = fVec64[0];
			fRec165[1] = fRec165[0];
			fRec189[1] = fRec189[0];
			fRec188[1] = fRec188[0];
			fRec184[1] = fRec184[0];
			fRec185[1] = fRec185[0];
			iRec186[1] = iRec186[0];
			iRec187[1] = iRec187[0];
			fRec190[1] = fRec190[0];
			fVec67[1] = fVec67[0];
			fRec183[1] = fRec183[0];
			fRec197[1] = fRec197[0];
			fRec196[1] = fRec196[0];
			fRec192[1] = fRec192[0];
			fRec193[1] = fRec193[0];
			iRec194[1] = iRec194[0];
			iRec195[1] = iRec195[0];
			fRec198[1] = fRec198[0];
			fVec70[1] = fVec70[0];
			fRec191[1] = fRec191[0];
			fVec71[1] = fVec71[0];
			fRec182[1] = fRec182[0];
			fRec206[1] = fRec206[0];
			fRec205[1] = fRec205[0];
			fRec201[1] = fRec201[0];
			fRec202[1] = fRec202[0];
			iRec203[1] = iRec203[0];
			iRec204[1] = iRec204[0];
			fRec207[1] = fRec207[0];
			fVec74[1] = fVec74[0];
			fRec200[1] = fRec200[0];
			fRec214[1] = fRec214[0];
			fRec213[1] = fRec213[0];
			fRec209[1] = fRec209[0];
			fRec210[1] = fRec210[0];
			iRec211[1] = iRec211[0];
			iRec212[1] = iRec212[0];
			fRec215[1] = fRec215[0];
			fVec77[1] = fVec77[0];
			fRec208[1] = fRec208[0];
			fVec78[1] = fVec78[0];
			fRec199[1] = fRec199[0];
			fRec223[1] = fRec223[0];
			fRec222[1] = fRec222[0];
			fRec218[1] = fRec218[0];
			fRec219[1] = fRec219[0];
			iRec220[1] = iRec220[0];
			iRec221[1] = iRec221[0];
			fRec224[1] = fRec224[0];
			fVec81[1] = fVec81[0];
			fRec217[1] = fRec217[0];
			fRec231[1] = fRec231[0];
			fRec230[1] = fRec230[0];
			fRec226[1] = fRec226[0];
			fRec227[1] = fRec227[0];
			iRec228[1] = iRec228[0];
			iRec229[1] = iRec229[0];
			fRec232[1] = fRec232[0];
			fVec84[1] = fVec84[0];
			fRec225[1] = fRec225[0];
			fVec85[1] = fVec85[0];
			fRec216[1] = fRec216[0];
			fRec240[1] = fRec240[0];
			fRec239[1] = fRec239[0];
			fRec235[1] = fRec235[0];
			fRec236[1] = fRec236[0];
			iRec237[1] = iRec237[0];
			iRec238[1] = iRec238[0];
			fRec241[1] = fRec241[0];
			fVec88[1] = fVec88[0];
			fRec234[1] = fRec234[0];
			fRec248[1] = fRec248[0];
			fRec247[1] = fRec247[0];
			fRec243[1] = fRec243[0];
			fRec244[1] = fRec244[0];
			iRec245[1] = iRec245[0];
			iRec246[1] = iRec246[0];
			fRec249[1] = fRec249[0];
			fVec91[1] = fVec91[0];
			fRec242[1] = fRec242[0];
			fVec92[1] = fVec92[0];
			fRec233[1] = fRec233[0];
			fRec257[1] = fRec257[0];
			fRec256[1] = fRec256[0];
			fRec252[1] = fRec252[0];
			fRec253[1] = fRec253[0];
			iRec254[1] = iRec254[0];
			iRec255[1] = iRec255[0];
			fRec258[1] = fRec258[0];
			fVec95[1] = fVec95[0];
			fRec251[1] = fRec251[0];
			fRec265[1] = fRec265[0];
			fRec264[1] = fRec264[0];
			fRec260[1] = fRec260[0];
			fRec261[1] = fRec261[0];
			iRec262[1] = iRec262[0];
			iRec263[1] = iRec263[0];
			fRec266[1] = fRec266[0];
			fVec98[1] = fVec98[0];
			fRec259[1] = fRec259[0];
			fVec99[1] = fVec99[0];
			fRec250[1] = fRec250[0];
			fRec274[1] = fRec274[0];
			fRec273[1] = fRec273[0];
			fRec269[1] = fRec269[0];
			fRec270[1] = fRec270[0];
			iRec271[1] = iRec271[0];
			iRec272[1] = iRec272[0];
			fRec275[1] = fRec275[0];
			fVec102[1] = fVec102[0];
			fRec268[1] = fRec268[0];
			fRec282[1] = fRec282[0];
			fRec281[1] = fRec281[0];
			fRec277[1] = fRec277[0];
			fRec278[1] = fRec278[0];
			iRec279[1] = iRec279[0];
			iRec280[1] = iRec280[0];
			fRec283[1] = fRec283[0];
			fVec105[1] = fVec105[0];
			fRec276[1] = fRec276[0];
			fVec106[1] = fVec106[0];
			fRec267[1] = fRec267[0];
			fRec291[1] = fRec291[0];
			fRec290[1] = fRec290[0];
			fRec286[1] = fRec286[0];
			fRec287[1] = fRec287[0];
			iRec288[1] = iRec288[0];
			iRec289[1] = iRec289[0];
			fRec292[1] = fRec292[0];
			fVec109[1] = fVec109[0];
			fRec285[1] = fRec285[0];
			fRec299[1] = fRec299[0];
			fRec298[1] = fRec298[0];
			fRec294[1] = fRec294[0];
			fRec295[1] = fRec295[0];
			iRec296[1] = iRec296[0];
			iRec297[1] = iRec297[0];
			fRec300[1] = fRec300[0];
			fVec112[1] = fVec112[0];
			fRec293[1] = fRec293[0];
			fVec113[1] = fVec113[0];
			fRec284[1] = fRec284[0];
			fRec308[1] = fRec308[0];
			fRec307[1] = fRec307[0];
			fRec303[1] = fRec303[0];
			fRec304[1] = fRec304[0];
			iRec305[1] = iRec305[0];
			iRec306[1] = iRec306[0];
			fRec309[1] = fRec309[0];
			fVec116[1] = fVec116[0];
			fRec302[1] = fRec302[0];
			fRec316[1] = fRec316[0];
			fRec315[1] = fRec315[0];
			fRec311[1] = fRec311[0];
			fRec312[1] = fRec312[0];
			iRec313[1] = iRec313[0];
			iRec314[1] = iRec314[0];
			fRec317[1] = fRec317[0];
			fVec119[1] = fVec119[0];
			fRec310[1] = fRec310[0];
			fVec120[1] = fVec120[0];
			fRec301[1] = fRec301[0];
			fRec325[1] = fRec325[0];
			fRec324[1] = fRec324[0];
			fRec320[1] = fRec320[0];
			fRec321[1] = fRec321[0];
			iRec322[1] = iRec322[0];
			iRec323[1] = iRec323[0];
			fRec326[1] = fRec326[0];
			fVec123[1] = fVec123[0];
			fRec319[1] = fRec319[0];
			fRec333[1] = fRec333[0];
			fRec332[1] = fRec332[0];
			fRec328[1] = fRec328[0];
			fRec329[1] = fRec329[0];
			iRec330[1] = iRec330[0];
			iRec331[1] = iRec331[0];
			fRec334[1] = fRec334[0];
			fVec126[1] = fVec126[0];
			fRec327[1] = fRec327[0];
			fVec127[1] = fVec127[0];
			fRec318[1] = fRec318[0];
			fRec342[1] = fRec342[0];
			fRec341[1] = fRec341[0];
			fRec337[1] = fRec337[0];
			fRec338[1] = fRec338[0];
			iRec339[1] = iRec339[0];
			iRec340[1] = iRec340[0];
			fRec343[1] = fRec343[0];
			fVec130[1] = fVec130[0];
			fRec336[1] = fRec336[0];
			fRec350[1] = fRec350[0];
			fRec349[1] = fRec349[0];
			fRec345[1] = fRec345[0];
			fRec346[1] = fRec346[0];
			iRec347[1] = iRec347[0];
			iRec348[1] = iRec348[0];
			fRec351[1] = fRec351[0];
			fVec133[1] = fVec133[0];
			fRec344[1] = fRec344[0];
			fVec134[1] = fVec134[0];
			fRec335[1] = fRec335[0];
			fRec359[1] = fRec359[0];
			fRec358[1] = fRec358[0];
			fRec354[1] = fRec354[0];
			fRec355[1] = fRec355[0];
			iRec356[1] = iRec356[0];
			iRec357[1] = iRec357[0];
			fRec360[1] = fRec360[0];
			fVec137[1] = fVec137[0];
			fRec353[1] = fRec353[0];
			fRec367[1] = fRec367[0];
			fRec366[1] = fRec366[0];
			fRec362[1] = fRec362[0];
			fRec363[1] = fRec363[0];
			iRec364[1] = iRec364[0];
			iRec365[1] = iRec365[0];
			fRec368[1] = fRec368[0];
			fVec140[1] = fVec140[0];
			fRec361[1] = fRec361[0];
			fVec141[1] = fVec141[0];
			fRec352[1] = fRec352[0];
			fRec376[1] = fRec376[0];
			fRec375[1] = fRec375[0];
			fRec371[1] = fRec371[0];
			fRec372[1] = fRec372[0];
			iRec373[1] = iRec373[0];
			iRec374[1] = iRec374[0];
			fRec377[1] = fRec377[0];
			fVec144[1] = fVec144[0];
			fRec370[1] = fRec370[0];
			fRec384[1] = fRec384[0];
			fRec383[1] = fRec383[0];
			fRec379[1] = fRec379[0];
			fRec380[1] = fRec380[0];
			iRec381[1] = iRec381[0];
			iRec382[1] = iRec382[0];
			fRec385[1] = fRec385[0];
			fVec147[1] = fVec147[0];
			fRec378[1] = fRec378[0];
			fVec148[1] = fVec148[0];
			fRec369[1] = fRec369[0];
			fRec393[1] = fRec393[0];
			fRec392[1] = fRec392[0];
			fRec388[1] = fRec388[0];
			fRec389[1] = fRec389[0];
			iRec390[1] = iRec390[0];
			iRec391[1] = iRec391[0];
			fRec394[1] = fRec394[0];
			fVec151[1] = fVec151[0];
			fRec387[1] = fRec387[0];
			fRec401[1] = fRec401[0];
			fRec400[1] = fRec400[0];
			fRec396[1] = fRec396[0];
			fRec397[1] = fRec397[0];
			iRec398[1] = iRec398[0];
			iRec399[1] = iRec399[0];
			fRec402[1] = fRec402[0];
			fVec154[1] = fVec154[0];
			fRec395[1] = fRec395[0];
			fVec155[1] = fVec155[0];
			fRec386[1] = fRec386[0];
			fRec410[1] = fRec410[0];
			fRec409[1] = fRec409[0];
			fRec405[1] = fRec405[0];
			fRec406[1] = fRec406[0];
			iRec407[1] = iRec407[0];
			iRec408[1] = iRec408[0];
			fRec411[1] = fRec411[0];
			fVec158[1] = fVec158[0];
			fRec404[1] = fRec404[0];
			fRec418[1] = fRec418[0];
			fRec417[1] = fRec417[0];
			fRec413[1] = fRec413[0];
			fRec414[1] = fRec414[0];
			iRec415[1] = iRec415[0];
			iRec416[1] = iRec416[0];
			fRec419[1] = fRec419[0];
			fVec161[1] = fVec161[0];
			fRec412[1] = fRec412[0];
			fVec162[1] = fVec162[0];
			fRec403[1] = fRec403[0];
			fRec427[1] = fRec427[0];
			fRec426[1] = fRec426[0];
			fRec422[1] = fRec422[0];
			fRec423[1] = fRec423[0];
			iRec424[1] = iRec424[0];
			iRec425[1] = iRec425[0];
			fRec428[1] = fRec428[0];
			fVec165[1] = fVec165[0];
			fRec421[1] = fRec421[0];
			fRec435[1] = fRec435[0];
			fRec434[1] = fRec434[0];
			fRec430[1] = fRec430[0];
			fRec431[1] = fRec431[0];
			iRec432[1] = iRec432[0];
			iRec433[1] = iRec433[0];
			fRec436[1] = fRec436[0];
			fVec168[1] = fVec168[0];
			fRec429[1] = fRec429[0];
			fVec169[1] = fVec169[0];
			fRec420[1] = fRec420[0];
			fRec444[1] = fRec444[0];
			fRec443[1] = fRec443[0];
			fRec439[1] = fRec439[0];
			fRec440[1] = fRec440[0];
			iRec441[1] = iRec441[0];
			iRec442[1] = iRec442[0];
			fRec445[1] = fRec445[0];
			fVec172[1] = fVec172[0];
			fRec438[1] = fRec438[0];
			fRec452[1] = fRec452[0];
			fRec451[1] = fRec451[0];
			fRec447[1] = fRec447[0];
			fRec448[1] = fRec448[0];
			iRec449[1] = iRec449[0];
			iRec450[1] = iRec450[0];
			fRec453[1] = fRec453[0];
			fVec175[1] = fVec175[0];
			fRec446[1] = fRec446[0];
			fVec176[1] = fVec176[0];
			fRec437[1] = fRec437[0];
			fRec461[1] = fRec461[0];
			fRec460[1] = fRec460[0];
			fRec456[1] = fRec456[0];
			fRec457[1] = fRec457[0];
			iRec458[1] = iRec458[0];
			iRec459[1] = iRec459[0];
			fRec462[1] = fRec462[0];
			fVec179[1] = fVec179[0];
			fRec455[1] = fRec455[0];
			fRec469[1] = fRec469[0];
			fRec468[1] = fRec468[0];
			fRec464[1] = fRec464[0];
			fRec465[1] = fRec465[0];
			iRec466[1] = iRec466[0];
			iRec467[1] = iRec467[0];
			fRec470[1] = fRec470[0];
			fVec182[1] = fVec182[0];
			fRec463[1] = fRec463[0];
			fVec183[1] = fVec183[0];
			fRec454[1] = fRec454[0];
			fRec478[1] = fRec478[0];
			fRec477[1] = fRec477[0];
			fRec473[1] = fRec473[0];
			fRec474[1] = fRec474[0];
			iRec475[1] = iRec475[0];
			iRec476[1] = iRec476[0];
			fRec479[1] = fRec479[0];
			fVec186[1] = fVec186[0];
			fRec472[1] = fRec472[0];
			fRec486[1] = fRec486[0];
			fRec485[1] = fRec485[0];
			fRec481[1] = fRec481[0];
			fRec482[1] = fRec482[0];
			iRec483[1] = iRec483[0];
			iRec484[1] = iRec484[0];
			fRec487[1] = fRec487[0];
			fVec189[1] = fVec189[0];
			fRec480[1] = fRec480[0];
			fVec190[1] = fVec190[0];
			fRec471[1] = fRec471[0];
			fRec495[1] = fRec495[0];
			fRec494[1] = fRec494[0];
			fRec490[1] = fRec490[0];
			fRec491[1] = fRec491[0];
			iRec492[1] = iRec492[0];
			iRec493[1] = iRec493[0];
			fRec496[1] = fRec496[0];
			fVec193[1] = fVec193[0];
			fRec489[1] = fRec489[0];
			fRec503[1] = fRec503[0];
			fRec502[1] = fRec502[0];
			fRec498[1] = fRec498[0];
			fRec499[1] = fRec499[0];
			iRec500[1] = iRec500[0];
			iRec501[1] = iRec501[0];
			fRec504[1] = fRec504[0];
			fVec196[1] = fVec196[0];
			fRec497[1] = fRec497[0];
			fVec197[1] = fVec197[0];
			fRec488[1] = fRec488[0];
			fRec512[1] = fRec512[0];
			fRec511[1] = fRec511[0];
			fRec507[1] = fRec507[0];
			fRec508[1] = fRec508[0];
			iRec509[1] = iRec509[0];
			iRec510[1] = iRec510[0];
			fRec513[1] = fRec513[0];
			fVec200[1] = fVec200[0];
			fRec506[1] = fRec506[0];
			fRec520[1] = fRec520[0];
			fRec519[1] = fRec519[0];
			fRec515[1] = fRec515[0];
			fRec516[1] = fRec516[0];
			iRec517[1] = iRec517[0];
			iRec518[1] = iRec518[0];
			fRec521[1] = fRec521[0];
			fVec203[1] = fVec203[0];
			fRec514[1] = fRec514[0];
			fVec204[1] = fVec204[0];
			fRec505[1] = fRec505[0];
			fRec529[1] = fRec529[0];
			fRec528[1] = fRec528[0];
			fRec524[1] = fRec524[0];
			fRec525[1] = fRec525[0];
			iRec526[1] = iRec526[0];
			iRec527[1] = iRec527[0];
			fRec530[1] = fRec530[0];
			fVec207[1] = fVec207[0];
			fRec523[1] = fRec523[0];
			fRec537[1] = fRec537[0];
			fRec536[1] = fRec536[0];
			fRec532[1] = fRec532[0];
			fRec533[1] = fRec533[0];
			iRec534[1] = iRec534[0];
			iRec535[1] = iRec535[0];
			fRec538[1] = fRec538[0];
			fVec210[1] = fVec210[0];
			fRec531[1] = fRec531[0];
			fVec211[1] = fVec211[0];
			fRec522[1] = fRec522[0];
			fRec546[1] = fRec546[0];
			fRec545[1] = fRec545[0];
			fRec541[1] = fRec541[0];
			fRec542[1] = fRec542[0];
			iRec543[1] = iRec543[0];
			iRec544[1] = iRec544[0];
			fRec547[1] = fRec547[0];
			fVec214[1] = fVec214[0];
			fRec540[1] = fRec540[0];
			fRec554[1] = fRec554[0];
			fRec553[1] = fRec553[0];
			fRec549[1] = fRec549[0];
			fRec550[1] = fRec550[0];
			iRec551[1] = iRec551[0];
			iRec552[1] = iRec552[0];
			fRec555[1] = fRec555[0];
			fVec217[1] = fVec217[0];
			fRec548[1] = fRec548[0];
			fVec218[1] = fVec218[0];
			fRec539[1] = fRec539[0];
		}
	}

};

#ifdef FAUST_UIMACROS
	
	#define FAUST_FILE_NAME "bbdmi_ambLagrangeMods31.dsp"
	#define FAUST_CLASS_NAME "bbdmi_ambLagrangeMods31"
	#define FAUST_COMPILATION_OPIONS "-a /Applications/Faust-2.72.14/share/faust/max-msp/max-msp64.cpp -lang cpp -i -ct 1 -cn bbdmi_ambLagrangeMods31 -es 1 -mcd 16 -mdd 1024 -mdy 33 -uim -double -ftz 0"
	#define FAUST_INPUTS 0
	#define FAUST_OUTPUTS 31
	#define FAUST_ACTIVES 14
	#define FAUST_PASSIVES 0

	FAUST_ADDVERTICALSLIDER("LagrangeModSynth/lagrangeModulations/[00]Randomizer/dryWetRandom", fVslider2, 1.0, 0.0, 1.0, 0.001);
	FAUST_ADDBUTTON("LagrangeModSynth/lagrangeModulations/[00]Randomizer/newRandom", fButton0);
	FAUST_ADDVERTICALSLIDER("LagrangeModSynth/lagrangeModulations/[00]Randomizer/randomGetTime", fVslider0, 5.0, 0.1, 3e+02, 0.1);
	FAUST_ADDCHECKBOX("LagrangeModSynth/lagrangeModulations/[00]Randomizer/freqRandom", fCheckbox0);
	FAUST_ADDVERTICALSLIDER("LagrangeModSynth/lagrangeModulations/[01]Parameters/indexdistr", fVslider1, 0.0, 0.0, 21.0, 1.0);
	FAUST_ADDVERTICALSLIDER("LagrangeModSynth/lagrangeModulations/[01]Parameters/Freq", fVslider4, 1.1e+02, 1e+01, 1e+04, 0.01);
	FAUST_ADDVERTICALSLIDER("LagrangeModSynth/lagrangeModulations/[01]Parameters/retardFactor", fVslider3, 0.0, 0.0, 1.0, 0.001);
	FAUST_ADDVERTICALSLIDER("LagrangeModSynth/lagrangeModulations/[01]Parameters/maxtransp", fVslider10, 0.0, 0.0, 2e+01, 0.001);
	FAUST_ADDVERTICALSLIDER("LagrangeModSynth/lagrangeModulations/[01]Parameters/transpSide", fVslider11, 0.0, -1.0, 1.0, 0.001);
	FAUST_ADDVERTICALSLIDER("LagrangeModSynth/lagrangeModulations/[01]Parameters/modGlobalFreq", fVslider6, 5e+02, 0.01, 1e+04, 0.01);
	FAUST_ADDVERTICALSLIDER("LagrangeModSynth/lagrangeModulations/[01]Parameters/y2modFactor", fVslider5, 0.2, 0.0, 1e+01, 0.001);
	FAUST_ADDVERTICALSLIDER("LagrangeModSynth/lagrangeModulations/[01]Parameters/y3modFactor", fVslider7, 0.2, 0.0, 1e+01, 0.001);
	FAUST_ADDVERTICALSLIDER("LagrangeModSynth/lagrangeModulations/[01]Parameters/y4modFactor", fVslider8, 0.2, 0.0, 1e+01, 0.001);
	FAUST_ADDVERTICALSLIDER("LagrangeModSynth/lagrangeModulations/[01]Parameters/y5modFactor", fVslider9, 0.2, 0.0, 1e+01, 0.001);

	#define FAUST_LIST_ACTIVES(p) \
		p(VERTICALSLIDER, dryWetRandom, "LagrangeModSynth/lagrangeModulations/[00]Randomizer/dryWetRandom", fVslider2, 1.0, 0.0, 1.0, 0.001) \
		p(BUTTON, newRandom, "LagrangeModSynth/lagrangeModulations/[00]Randomizer/newRandom", fButton0, 0.0, 0.0, 1.0, 1.0) \
		p(VERTICALSLIDER, randomGetTime, "LagrangeModSynth/lagrangeModulations/[00]Randomizer/randomGetTime", fVslider0, 5.0, 0.1, 3e+02, 0.1) \
		p(CHECKBOX, freqRandom, "LagrangeModSynth/lagrangeModulations/[00]Randomizer/freqRandom", fCheckbox0, 0.0, 0.0, 1.0, 1.0) \
		p(VERTICALSLIDER, indexdistr, "LagrangeModSynth/lagrangeModulations/[01]Parameters/indexdistr", fVslider1, 0.0, 0.0, 21.0, 1.0) \
		p(VERTICALSLIDER, Freq, "LagrangeModSynth/lagrangeModulations/[01]Parameters/Freq", fVslider4, 1.1e+02, 1e+01, 1e+04, 0.01) \
		p(VERTICALSLIDER, retardFactor, "LagrangeModSynth/lagrangeModulations/[01]Parameters/retardFactor", fVslider3, 0.0, 0.0, 1.0, 0.001) \
		p(VERTICALSLIDER, maxtransp, "LagrangeModSynth/lagrangeModulations/[01]Parameters/maxtransp", fVslider10, 0.0, 0.0, 2e+01, 0.001) \
		p(VERTICALSLIDER, transpSide, "LagrangeModSynth/lagrangeModulations/[01]Parameters/transpSide", fVslider11, 0.0, -1.0, 1.0, 0.001) \
		p(VERTICALSLIDER, modGlobalFreq, "LagrangeModSynth/lagrangeModulations/[01]Parameters/modGlobalFreq", fVslider6, 5e+02, 0.01, 1e+04, 0.01) \
		p(VERTICALSLIDER, y2modFactor, "LagrangeModSynth/lagrangeModulations/[01]Parameters/y2modFactor", fVslider5, 0.2, 0.0, 1e+01, 0.001) \
		p(VERTICALSLIDER, y3modFactor, "LagrangeModSynth/lagrangeModulations/[01]Parameters/y3modFactor", fVslider7, 0.2, 0.0, 1e+01, 0.001) \
		p(VERTICALSLIDER, y4modFactor, "LagrangeModSynth/lagrangeModulations/[01]Parameters/y4modFactor", fVslider8, 0.2, 0.0, 1e+01, 0.001) \
		p(VERTICALSLIDER, y5modFactor, "LagrangeModSynth/lagrangeModulations/[01]Parameters/y5modFactor", fVslider9, 0.2, 0.0, 1e+01, 0.001) \

	#define FAUST_LIST_PASSIVES(p) \

#endif

/***************************END USER SECTION ***************************/

/*******************BEGIN ARCHITECTURE SECTION (part 2/2)***************/

/* Faust code wrapper ------- */

#include "ext.h"
#include "ext_obex.h"
#include "z_dsp.h"
#include "jpatcher_api.h"
#include <string.h>

#define ASSIST_INLET 	1
#define ASSIST_OUTLET 	2

#define EXTERNAL_VERSION    "0.87"
#define STR_SIZE            512

/************************** BEGIN MidiUI.h ****************************
FAUST Architecture File
Copyright (C) 2003-2022 GRAME, Centre National de Creation Musicale
---------------------------------------------------------------------
This program is free software; you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation; either version 2.1 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

EXCEPTION : As a special exception, you may create a larger work
that contains this FAUST architecture section and distribute
that work under terms of your choice, so long as this FAUST
architecture section is not modified.
************************************************************************/

#ifndef FAUST_MIDIUI_H
#define FAUST_MIDIUI_H

#include <vector>
#include <string>
#include <utility>
#include <cstdlib>
#include <cmath>

/************************** BEGIN MapUI.h ******************************
 FAUST Architecture File
 Copyright (C) 2003-2022 GRAME, Centre National de Creation Musicale
 ---------------------------------------------------------------------
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation; either version 2.1 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 
 EXCEPTION : As a special exception, you may create a larger work
 that contains this FAUST architecture section and distribute
 that work under terms of your choice, so long as this FAUST
 architecture section is not modified.
 ***********************************************************************/

#ifndef FAUST_MAPUI_H
#define FAUST_MAPUI_H

#include <vector>
#include <map>
#include <string>
#include <stdio.h>


/*******************************************************************************
 * MapUI : Faust User Interface.
 *
 * This class creates:
 * - a map of 'labels' and zones for each UI item.
 * - a map of unique 'shortname' (built so that they never collide) and zones for each UI item
 * - a map of complete hierarchical 'paths' and zones for each UI item
 *
 * Simple 'labels', 'shortname' and complete 'paths' (to fully discriminate between possible same
 * 'labels' at different location in the UI hierachy) can be used to access a given parameter.
 ******************************************************************************/

class FAUST_API MapUI : public UI, public PathBuilder
{
    
    protected:
    
        // Label zone map
        std::map<std::string, FAUSTFLOAT*> fLabelZoneMap;
    
        // Shortname zone map
        std::map<std::string, FAUSTFLOAT*> fShortnameZoneMap;
    
        // Full path map
        std::map<std::string, FAUSTFLOAT*> fPathZoneMap;
    
        void addZoneLabel(const std::string& label, FAUSTFLOAT* zone)
        {
            std::string path = buildPath(label);
            fFullPaths.push_back(path);
            fPathZoneMap[path] = zone;
            fLabelZoneMap[label] = zone;
        }
    
    public:
        
        MapUI() {}
        virtual ~MapUI() {}
        
        // -- widget's layouts
        void openTabBox(const char* label)
        {
            pushLabel(label);
        }
        void openHorizontalBox(const char* label)
        {
            pushLabel(label);
        }
        void openVerticalBox(const char* label)
        {
            pushLabel(label);
        }
        void closeBox()
        {
            if (popLabel()) {
                // Shortnames can be computed when all fullnames are known
                computeShortNames();
                // Fill 'shortname' map
                for (const auto& it : fFullPaths) {
                    fShortnameZoneMap[fFull2Short[it]] = fPathZoneMap[it];
                }
            }
        }
        
        // -- active widgets
        void addButton(const char* label, FAUSTFLOAT* zone)
        {
            addZoneLabel(label, zone);
        }
        void addCheckButton(const char* label, FAUSTFLOAT* zone)
        {
            addZoneLabel(label, zone);
        }
        void addVerticalSlider(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT fmin, FAUSTFLOAT fmax, FAUSTFLOAT step)
        {
            addZoneLabel(label, zone);
        }
        void addHorizontalSlider(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT fmin, FAUSTFLOAT fmax, FAUSTFLOAT step)
        {
            addZoneLabel(label, zone);
        }
        void addNumEntry(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT fmin, FAUSTFLOAT fmax, FAUSTFLOAT step)
        {
            addZoneLabel(label, zone);
        }
        
        // -- passive widgets
        void addHorizontalBargraph(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT fmin, FAUSTFLOAT fmax)
        {
            addZoneLabel(label, zone);
        }
        void addVerticalBargraph(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT fmin, FAUSTFLOAT fmax)
        {
            addZoneLabel(label, zone);
        }
    
        // -- soundfiles
        virtual void addSoundfile(const char* label, const char* filename, Soundfile** sf_zone) {}
        
        // -- metadata declarations
        virtual void declare(FAUSTFLOAT* zone, const char* key, const char* val)
        {}
    
        //-------------------------------------------------------------------------------
        // Public API
        //-------------------------------------------------------------------------------
    
        /**
         * Set the param value.
         *
         * @param str - the UI parameter label/shortname/path
         * @param value - the UI parameter value
         *
         */
        void setParamValue(const std::string& str, FAUSTFLOAT value)
        {
            const auto fPathZoneMapIter = fPathZoneMap.find(str);
            if (fPathZoneMapIter != fPathZoneMap.end()) {
                *fPathZoneMapIter->second = value;
                return;
            }
            
            const auto fShortnameZoneMapIter = fShortnameZoneMap.find(str);
            if (fShortnameZoneMapIter != fShortnameZoneMap.end()) {
                *fShortnameZoneMapIter->second = value;
                return;
            }
            
            const auto fLabelZoneMapIter = fLabelZoneMap.find(str);
            if (fLabelZoneMapIter != fLabelZoneMap.end()) {
                *fLabelZoneMapIter->second = value;
                return;
            }
            
            fprintf(stderr, "ERROR : setParamValue '%s' not found\n", str.c_str());
        }
        
        /**
         * Return the param value.
         *
         * @param str - the UI parameter label/shortname/path
         *
         * @return the param value.
         */
        FAUSTFLOAT getParamValue(const std::string& str)
        {
            const auto fPathZoneMapIter = fPathZoneMap.find(str);
            if (fPathZoneMapIter != fPathZoneMap.end()) {
                return *fPathZoneMapIter->second;
            }
            
            const auto fShortnameZoneMapIter = fShortnameZoneMap.find(str);
            if (fShortnameZoneMapIter != fShortnameZoneMap.end()) {
                return *fShortnameZoneMapIter->second;
            }
            
            const auto fLabelZoneMapIter = fLabelZoneMap.find(str);
            if (fLabelZoneMapIter != fLabelZoneMap.end()) {
                return *fLabelZoneMapIter->second;
            }
            
            fprintf(stderr, "ERROR : getParamValue '%s' not found\n", str.c_str());
            return 0;
        }
    
        // map access 
        std::map<std::string, FAUSTFLOAT*>& getFullpathMap() { return fPathZoneMap; }
        std::map<std::string, FAUSTFLOAT*>& getShortnameMap() { return fShortnameZoneMap; }
        std::map<std::string, FAUSTFLOAT*>& getLabelMap() { return fLabelZoneMap; }
            
        /**
         * Return the number of parameters in the UI.
         *
         * @return the number of parameters
         */
        int getParamsCount() { return int(fPathZoneMap.size()); }
        
        /**
         * Return the param path.
         *
         * @param index - the UI parameter index
         *
         * @return the param path
         */
        std::string getParamAddress(int index)
        {
            if (index < 0 || index > int(fPathZoneMap.size())) {
                return "";
            } else {
                auto it = fPathZoneMap.begin();
                while (index-- > 0 && it++ != fPathZoneMap.end()) {}
                return it->first;
            }
        }
        
        const char* getParamAddress1(int index)
        {
            if (index < 0 || index > int(fPathZoneMap.size())) {
                return nullptr;
            } else {
                auto it = fPathZoneMap.begin();
                while (index-- > 0 && it++ != fPathZoneMap.end()) {}
                return it->first.c_str();
            }
        }
    
        /**
         * Return the param shortname.
         *
         * @param index - the UI parameter index
         *
         * @return the param shortname
         */
        std::string getParamShortname(int index)
        {
            if (index < 0 || index > int(fShortnameZoneMap.size())) {
                return "";
            } else {
                auto it = fShortnameZoneMap.begin();
                while (index-- > 0 && it++ != fShortnameZoneMap.end()) {}
                return it->first;
            }
        }
        
        const char* getParamShortname1(int index)
        {
            if (index < 0 || index > int(fShortnameZoneMap.size())) {
                return nullptr;
            } else {
                auto it = fShortnameZoneMap.begin();
                while (index-- > 0 && it++ != fShortnameZoneMap.end()) {}
                return it->first.c_str();
            }
        }
    
        /**
         * Return the param label.
         *
         * @param index - the UI parameter index
         *
         * @return the param label
         */
        std::string getParamLabel(int index)
        {
            if (index < 0 || index > int(fLabelZoneMap.size())) {
                return "";
            } else {
                auto it = fLabelZoneMap.begin();
                while (index-- > 0 && it++ != fLabelZoneMap.end()) {}
                return it->first;
            }
        }
        
        const char* getParamLabel1(int index)
        {
            if (index < 0 || index > int(fLabelZoneMap.size())) {
                return nullptr;
            } else {
                auto it = fLabelZoneMap.begin();
                while (index-- > 0 && it++ != fLabelZoneMap.end()) {}
                return it->first.c_str();
            }
        }
    
        /**
         * Return the param path.
         *
         * @param zone - the UI parameter memory zone
         *
         * @return the param path
         */
        std::string getParamAddress(FAUSTFLOAT* zone)
        {
            for (const auto& it : fPathZoneMap) {
                if (it.second == zone) return it.first;
            }
            return "";
        }
    
        /**
         * Return the param memory zone.
         *
         * @param zone - the UI parameter label/shortname/path
         *
         * @return the param path
         */
        FAUSTFLOAT* getParamZone(const std::string& str)
        {
            const auto fPathZoneMapIter = fPathZoneMap.find(str);
            if (fPathZoneMapIter != fPathZoneMap.end()) {
                return fPathZoneMapIter->second;
            }
            
            const auto fShortnameZoneMapIter = fShortnameZoneMap.find(str);
            if (fShortnameZoneMapIter != fShortnameZoneMap.end()) {
                return fShortnameZoneMapIter->second;
            }
            
            const auto fLabelZoneMapIter = fLabelZoneMap.find(str);
            if (fLabelZoneMapIter != fLabelZoneMap.end()) {
                return fLabelZoneMapIter->second;
            }

            return nullptr;
        }
    
        /**
         * Return the param memory zone.
         *
         * @param zone - the UI parameter index
         *
         * @return the param path
         */
        FAUSTFLOAT* getParamZone(int index)
        {
            if (index < 0 || index > int(fPathZoneMap.size())) {
                return nullptr;
            } else {
                auto it = fPathZoneMap.begin();
                while (index-- > 0 && it++ != fPathZoneMap.end()) {}
                return it->second;
            }
        }
    
        static bool endsWith(const std::string& str, const std::string& end)
        {
            size_t l1 = str.length();
            size_t l2 = end.length();
            return (l1 >= l2) && (0 == str.compare(l1 - l2, l2, end));
        }
    
};

#endif // FAUST_MAPUI_H
/**************************  END  MapUI.h **************************/
/************************** BEGIN midi.h *******************************
FAUST Architecture File
Copyright (C) 2003-2022 GRAME, Centre National de Creation Musicale
---------------------------------------------------------------------
This program is free software; you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation; either version 2.1 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

EXCEPTION : As a special exception, you may create a larger work
that contains this FAUST architecture section and distribute
that work under terms of your choice, so long as this FAUST
architecture section is not modified.
************************************************************************/

#ifndef __midi__
#define __midi__

#include <vector>
#include <string>
#include <string.h>
#include <algorithm>
#include <assert.h>


class FAUST_API MapUI;

/**
 * A timestamped short MIDI message used with SOUL.
 */

// Force contiguous memory layout
#pragma pack (push, 1)
struct MIDIMessage
{
    uint32_t frameIndex;
    uint8_t byte0, byte1, byte2;
};
#pragma pack (pop)

/**
 * For timestamped MIDI messages (in usec).
 */
struct DatedMessage {
    
    double fDate;
    unsigned char fBuffer[3];
    size_t fSize;
    
    DatedMessage(double date, unsigned char* buffer, size_t size)
    :fDate(date), fSize(size)
    {
        assert(size <= 3);
        memcpy(fBuffer, buffer, size);
    }
    
    DatedMessage():fDate(0.0), fSize(0)
    {}
    
};

/**
 * MIDI processor definition.
 *
 * MIDI input or output handling classes will implement this interface,
 * so the same method names (keyOn, keyOff, ctrlChange...) will be used either
 * when decoding MIDI input or encoding MIDI output events.
 * MIDI channel is numbered in [0..15] in this layer.
 */
class midi {

    public:

        midi() {}
        virtual ~midi() {}

        // Additional timestamped API for MIDI input
        virtual MapUI* keyOn(double, int channel, int pitch, int velocity)
        {
            return keyOn(channel, pitch, velocity);
        }
        
        virtual void keyOff(double, int channel, int pitch, int velocity = 0)
        {
            keyOff(channel, pitch, velocity);
        }
    
        virtual void keyPress(double, int channel, int pitch, int press)
        {
            keyPress(channel, pitch, press);
        }
        
        virtual void chanPress(double date, int channel, int press)
        {
            chanPress(channel, press);
        }
    
        virtual void pitchWheel(double, int channel, int wheel)
        {
            pitchWheel(channel, wheel);
        }
           
        virtual void ctrlChange(double, int channel, int ctrl, int value)
        {
            ctrlChange(channel, ctrl, value);
        }
    
        virtual void ctrlChange14bits(double, int channel, int ctrl, int value)
        {
            ctrlChange14bits(channel, ctrl, value);
        }
    
        virtual void rpn(double, int channel, int ctrl, int value)
        {
            rpn(channel, ctrl, value);
        }

        virtual void progChange(double, int channel, int pgm)
        {
            progChange(channel, pgm);
        }
    
        virtual void sysEx(double, std::vector<unsigned char>& message)
        {
            sysEx(message);
        }

        // MIDI sync
        virtual void startSync(double date)  {}
        virtual void stopSync(double date)   {}
        virtual void clock(double date)  {}

        // Standard MIDI API
        virtual MapUI* keyOn(int channel, int pitch, int velocity)      { return nullptr; }
        virtual void keyOff(int channel, int pitch, int velocity)       {}
        virtual void keyPress(int channel, int pitch, int press)        {}
        virtual void chanPress(int channel, int press)                  {}
        virtual void ctrlChange(int channel, int ctrl, int value)       {}
        virtual void ctrlChange14bits(int channel, int ctrl, int value) {}
        virtual void rpn(int channel, int ctrl, int value)              {}
        virtual void pitchWheel(int channel, int wheel)                 {}
        virtual void progChange(int channel, int pgm)                   {}
        virtual void sysEx(std::vector<unsigned char>& message)         {}

        enum MidiStatus {
            // channel voice messages
            MIDI_NOTE_OFF = 0x80,
            MIDI_NOTE_ON = 0x90,
            MIDI_CONTROL_CHANGE = 0xB0,
            MIDI_PROGRAM_CHANGE = 0xC0,
            MIDI_PITCH_BEND = 0xE0,
            MIDI_AFTERTOUCH = 0xD0,         // aka channel pressure
            MIDI_POLY_AFTERTOUCH = 0xA0,    // aka key pressure
            MIDI_CLOCK = 0xF8,
            MIDI_START = 0xFA,
            MIDI_CONT = 0xFB,
            MIDI_STOP = 0xFC,
            MIDI_SYSEX_START = 0xF0,
            MIDI_SYSEX_STOP = 0xF7
        };

        enum MidiCtrl {
            ALL_NOTES_OFF = 123,
            ALL_SOUND_OFF = 120
        };
    
        enum MidiNPN {
            PITCH_BEND_RANGE = 0
        };

};

/**
 * A class to decode NRPN and RPN messages, adapted from JUCE forum message:
 * https://forum.juce.com/t/14bit-midi-controller-support/11517
 */
class MidiNRPN {
    
    private:
    
        bool ctrlnew;
        int ctrlnum;
        int ctrlval;
        
        int nrpn_lsb, nrpn_msb;
        int data_lsb, data_msb;
        
        enum
        {
            midi_nrpn_lsb = 98,
            midi_nrpn_msb = 99,
            midi_rpn_lsb  = 100,
            midi_rpn_msb  = 101,
            midi_data_lsb = 38,
            midi_data_msb = 6
        };
    
    public:
        
        MidiNRPN(): ctrlnew(false), nrpn_lsb(-1), nrpn_msb(-1), data_lsb(-1), data_msb(-1)
        {}
        
        // return true if the message has been filtered
        bool process(int data1, int data2)
        {
            switch (data1)
            {
                case midi_nrpn_lsb: nrpn_lsb = data2; return true;
                case midi_nrpn_msb: nrpn_msb = data2; return true;
                case midi_rpn_lsb: {
                    if (data2 == 127) {
                        nrpn_lsb = data_lsb = -1;
                    } else {
                        nrpn_lsb = 0;
                        data_lsb = -1;
                    }
                    return true;
                }
                case midi_rpn_msb: {
                    if (data2 == 127) {
                        nrpn_msb = data_msb = -1;
                    } else {
                        nrpn_msb = 0;
                        data_msb = -1;
                    }
                    return true;
                }
                case midi_data_lsb:
                case midi_data_msb:
                {
                    if (data1 == midi_data_msb) {
                        if (nrpn_msb < 0) {
                            return false;
                        }
                        data_msb = data2;
                    } else { // midi_data_lsb
                        if (nrpn_lsb < 0) {
                            return false;
                        }
                        data_lsb = data2;
                    }
                    if (data_lsb >= 0 && data_msb >= 0) {
                        ctrlnum = (nrpn_msb << 7) | nrpn_lsb;
                        ctrlval = (data_msb << 7) | data_lsb;
                        data_lsb = data_msb = -1;
                        nrpn_msb = nrpn_lsb = -1;
                        ctrlnew = true;
                    }
                    return true;
                }
                default: return false;
            };
        }
        
        bool hasNewNRPN() { bool res = ctrlnew; ctrlnew = false; return res; }
        
        // results in [0, 16383]
        int getCtrl() const { return ctrlnum; }
        int getVal() const { return ctrlval; }
    
};

/**
 * A pure interface for MIDI handlers that can send/receive MIDI messages to/from 'midi' objects.
 */
struct midi_interface {
    virtual void addMidiIn(midi* midi_dsp)      = 0;
    virtual void removeMidiIn(midi* midi_dsp)   = 0;
    virtual ~midi_interface() {}
};

/****************************************************
 * Base class for MIDI input handling.
 *
 * Shared common code used for input handling:
 * - decoding Real-Time messages: handleSync
 * - decoding one data byte messages: handleData1
 * - decoding two data byte messages: handleData2
 * - getting ready messages in polling mode
 ****************************************************/
class midi_handler : public midi, public midi_interface {

    protected:

        std::vector<midi*> fMidiInputs;
        std::string fName;
        MidiNRPN fNRPN;
    
        int range(int min, int max, int val) { return (val < min) ? min : ((val >= max) ? max : val); }
  
    public:

        midi_handler(const std::string& name = "MIDIHandler"):midi_interface(), fName(name) {}
        virtual ~midi_handler() {}

        void addMidiIn(midi* midi_dsp) { if (midi_dsp) fMidiInputs.push_back(midi_dsp); }
        void removeMidiIn(midi* midi_dsp)
        {
            std::vector<midi*>::iterator it = std::find(fMidiInputs.begin(), fMidiInputs.end(), midi_dsp);
            if (it != fMidiInputs.end()) {
                fMidiInputs.erase(it);
            }
        }

        // Those 2 methods have to be implemented by subclasses
        virtual bool startMidi() { return true; }
        virtual void stopMidi() {}
    
        void setName(const std::string& name) { fName = name; }
        std::string getName() { return fName; }
    
        // To be used in polling mode
        virtual int recvMessages(std::vector<MIDIMessage>* message) { return 0; }
        virtual void sendMessages(std::vector<MIDIMessage>* message, int count) {}
    
        // MIDI Real-Time
        void handleClock(double time)
        {
            for (unsigned int i = 0; i < fMidiInputs.size(); i++) {
                fMidiInputs[i]->clock(time);
            }
        }
        
        void handleStart(double time)
        {
            for (unsigned int i = 0; i < fMidiInputs.size(); i++) {
                fMidiInputs[i]->startSync(time);
            }
        }
        
        void handleStop(double time)
        {
            for (unsigned int i = 0; i < fMidiInputs.size(); i++) {
                fMidiInputs[i]->stopSync(time);
            }
        }
    
        void handleSync(double time, int type)
        {
            if (type == MIDI_CLOCK) {
                handleClock(time);
            // We can consider start and continue as identical messages
            } else if ((type == MIDI_START) || (type == MIDI_CONT)) {
                handleStart(time);
            } else if (type == MIDI_STOP) {
                handleStop(time);
            }
        }
    
        // MIDI 1 data
        void handleProgChange(double time, int channel, int data1)
        {
            for (unsigned int i = 0; i < fMidiInputs.size(); i++) {
                fMidiInputs[i]->progChange(time, channel, data1);
            }
        }
    
        void handleAfterTouch(double time, int channel, int data1)
        {
            for (unsigned int i = 0; i < fMidiInputs.size(); i++) {
                fMidiInputs[i]->chanPress(time, channel, data1);
            }
        }

        void handleData1(double time, int type, int channel, int data1)
        {
            if (type == MIDI_PROGRAM_CHANGE) {
                handleProgChange(time, channel, data1);
            } else if (type == MIDI_AFTERTOUCH) {
                handleAfterTouch(time, channel, data1);
            }
        }
    
        // MIDI 2 datas
        void handleKeyOff(double time, int channel, int data1, int data2)
        {
            for (unsigned int i = 0; i < fMidiInputs.size(); i++) {
                fMidiInputs[i]->keyOff(time, channel, data1, data2);
            }
        }
        
        void handleKeyOn(double time, int channel, int data1, int data2)
        {
            if (data2 == 0) {
                handleKeyOff(time, channel, data1, data2);
            } else {
                for (unsigned int i = 0; i < fMidiInputs.size(); i++) {
                    fMidiInputs[i]->keyOn(time, channel, data1, data2);
                }
            }
        }
    
        void handleCtrlChange(double time, int channel, int data1, int data2)
        {
            // Special processing for NRPN and RPN
            if (fNRPN.process(data1, data2)) {
                if (fNRPN.hasNewNRPN()) {
                    for (unsigned int i = 0; i < fMidiInputs.size(); i++) {
                        fMidiInputs[i]->rpn(time, channel, fNRPN.getCtrl(), fNRPN.getVal());
                    }
                }
            } else {
                for (unsigned int i = 0; i < fMidiInputs.size(); i++) {
                    fMidiInputs[i]->ctrlChange(time, channel, data1, data2);
                }
            }
        }
        
        void handlePitchWheel(double time, int channel, int data1, int data2)
        {
            for (unsigned int i = 0; i < fMidiInputs.size(); i++) {
                fMidiInputs[i]->pitchWheel(time, channel, (data2 << 7) + data1);
            }
        }
    
        void handlePitchWheel(double time, int channel, int bend)
        {
            for (unsigned int i = 0; i < fMidiInputs.size(); i++) {
                fMidiInputs[i]->pitchWheel(time, channel, bend);
            }
        }
        
        void handlePolyAfterTouch(double time, int channel, int data1, int data2)
        {
            for (unsigned int i = 0; i < fMidiInputs.size(); i++) {
                fMidiInputs[i]->keyPress(time, channel, data1, data2);
            }
        }
  
        void handleData2(double time, int type, int channel, int data1, int data2)
        {
            if (type == MIDI_NOTE_OFF) {
                handleKeyOff(time, channel,  data1, data2);
            } else if (type == MIDI_NOTE_ON) {
                handleKeyOn(time, channel, data1, data2);
            } else if (type == MIDI_CONTROL_CHANGE) {
                handleCtrlChange(time, channel, data1, data2);
            } else if (type == MIDI_PITCH_BEND) {
                handlePitchWheel(time, channel, data1, data2);
            } else if (type == MIDI_POLY_AFTERTOUCH) {
                handlePolyAfterTouch(time, channel, data1, data2);
            }
        }
    
        // SysEx
        void handleSysex(double time, std::vector<unsigned char>& message)
        {
            for (unsigned int i = 0; i < fMidiInputs.size(); i++) {
                fMidiInputs[i]->sysEx(time, message);
            }
        }
    
        void handleMessage(double time, int type, std::vector<unsigned char>& message)
        {
            if (type == MIDI_SYSEX_START) {
                handleSysex(time, message);
            }
        }
  
};

#define ucast(v) static_cast<unsigned char>(v)

#endif // __midi__
/**************************  END  midi.h **************************/

#ifdef _MSC_VER
#define gsscanf sscanf_s
#else
#define gsscanf sscanf
#endif

/**
 * Helper code for MIDI meta and polyphonic 'nvoices' parsing.
 */
struct MidiMeta : public Meta, public std::map<std::string, std::string> {
    
    void declare(const char* key, const char* value)
    {
        (*this)[key] = value;
    }
    
    const std::string get(const char* key, const char* def)
    {
        return (this->find(key) != this->end()) ? (*this)[key] : def;
    }
    
    static void analyse(dsp* mono_dsp, bool& midi_sync, int& nvoices)
    {
        JSONUI jsonui;
        mono_dsp->buildUserInterface(&jsonui);
        std::string json = jsonui.JSON();
        midi_sync = ((json.find("midi") != std::string::npos) &&
                     ((json.find("start") != std::string::npos) ||
                      (json.find("stop") != std::string::npos) ||
                      (json.find("clock") != std::string::npos) ||
                      (json.find("timestamp") != std::string::npos)));
    
    #if defined(NVOICES) && NVOICES!=NUM_VOICES
        nvoices = NVOICES;
    #else
        MidiMeta meta;
        mono_dsp->metadata(&meta);
        bool found_voices = false;
        // If "options" metadata is used
        std::string options = meta.get("options", "");
        if (options != "") {
            std::map<std::string, std::string> metadata;
            std::string res;
            MetaDataUI::extractMetadata(options, res, metadata);
            if (metadata.find("nvoices") != metadata.end()) {
                nvoices = std::atoi(metadata["nvoices"].c_str());
                found_voices = true;
            }
        }
        // Otherwise test for "nvoices" metadata
        if (!found_voices) {
            std::string numVoices = meta.get("nvoices", "0");
            nvoices = std::atoi(numVoices.c_str());
        }
        nvoices = std::max<int>(0, nvoices);
    #endif
    }
    
    static bool checkPolyphony(dsp* mono_dsp)
    {
        MapUI map_ui;
        mono_dsp->buildUserInterface(&map_ui);
        bool has_freq = false;
        bool has_gate = false;
        bool has_gain = false;
        for (int i = 0; i < map_ui.getParamsCount(); i++) {
            std::string path = map_ui.getParamAddress(i);
            has_freq |= MapUI::endsWith(path, "/freq");
            has_freq |= MapUI::endsWith(path, "/key");
            has_gate |= MapUI::endsWith(path, "/gate");
            has_gain |= MapUI::endsWith(path, "/gain");
            has_gain |= MapUI::endsWith(path, "/vel");
            has_gain |= MapUI::endsWith(path, "/velocity");
        }
        return (has_freq && has_gate && has_gain);
    }
    
};

/**
 * uiMidi : Faust User Interface
 * This class decodes MIDI meta data and maps incoming MIDI messages to them.
 * Currently ctrlChange, keyOn/keyOff, keyPress, progChange, chanPress, pitchWheel/pitchBend
 * start/stop/clock meta data is handled.
 * MIDI channel is numbered in [1..16] in this layer.
 * Channel 0 means "all channels" when receiving or sending.
 */
class uiMidi {
    
    friend class MidiUI;
    
    protected:
        
        midi* fMidiOut;
        bool fInputCtrl;
        int fChan;
    
        bool inRange(FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT v) { return (min <= v && v <= max); }
    
    public:
        
        uiMidi(midi* midi_out, bool input, int chan = 0):fMidiOut(midi_out), fInputCtrl(input), fChan(chan)
        {}
        virtual ~uiMidi()
        {}

};

/**
 * Base class for MIDI aware UI items.
 */
class uiMidiItem : public uiMidi, public uiItem {
    
    public:
        
        uiMidiItem(midi* midi_out, GUI* ui, FAUSTFLOAT* zone, bool input = true, int chan = 0)
            :uiMidi(midi_out, input, chan), uiItem(ui, zone)
        {}
        virtual ~uiMidiItem()
        {}
    
        virtual void reflectZone() {}
    
};

/**
 * Base class for MIDI aware UI items with timestamp support.
 */
class uiMidiTimedItem : public uiMidi, public uiTimedItem {
    
    public:
        
        uiMidiTimedItem(midi* midi_out, GUI* ui, FAUSTFLOAT* zone, bool input = true, int chan = 0)
            :uiMidi(midi_out, input, chan), uiTimedItem(ui, zone)
        {}
        virtual ~uiMidiTimedItem()
        {}
    
        virtual void reflectZone() {}
    
};

/**
 * MIDI sync.
 */
class uiMidiStart : public uiMidiTimedItem
{
  
    public:
    
        uiMidiStart(midi* midi_out, GUI* ui, FAUSTFLOAT* zone, bool input = true)
            :uiMidiTimedItem(midi_out, ui, zone, input)
        {}
        virtual ~uiMidiStart()
        {}
        
        virtual void reflectZone()
        {
            FAUSTFLOAT v = *fZone;
            fCache = v;
            if (v != FAUSTFLOAT(0)) {
                fMidiOut->startSync(0);
            }
        }
        void modifyZone(double date, FAUSTFLOAT v)
        {
            if (fInputCtrl) {
                uiItem::modifyZone(FAUSTFLOAT(v));
            }
        }
        
};

class uiMidiStop : public uiMidiTimedItem {
  
    public:
    
        uiMidiStop(midi* midi_out, GUI* ui, FAUSTFLOAT* zone, bool input = true)
            :uiMidiTimedItem(midi_out, ui, zone, input)
        {}
        virtual ~uiMidiStop()
        {}
        
        virtual void reflectZone()
        {
            FAUSTFLOAT v = *fZone;
            fCache = v;
            if (v != FAUSTFLOAT(1)) {
                fMidiOut->stopSync(0);
            }
        }
    
        void modifyZone(double date, FAUSTFLOAT v)
        {
            if (fInputCtrl) {
                uiItem::modifyZone(FAUSTFLOAT(v));
            }
        }
};

class uiMidiClock : public uiMidiTimedItem {

    private:
        
        bool fState;
  
    public:
    
        uiMidiClock(midi* midi_out, GUI* ui, FAUSTFLOAT* zone, bool input = true)
            :uiMidiTimedItem(midi_out, ui, zone, input), fState(false)
        {}
        virtual ~uiMidiClock()
        {}
    
        virtual void reflectZone()
        {
            FAUSTFLOAT v = *fZone;
            fCache = v;
            fMidiOut->clock(0);
        }
    
        void modifyZone(double date, FAUSTFLOAT v)
        {
            if (fInputCtrl) {
                fState = !fState;
                uiMidiTimedItem::modifyZone(date, FAUSTFLOAT(fState));
            }
        }

};

/**
 * Standard MIDI events.
 */

/**
 * uiMidiProgChange uses the [min...max] range.
 */
class uiMidiProgChange : public uiMidiTimedItem {
    
    public:
    
        FAUSTFLOAT fMin, fMax;
    
        uiMidiProgChange(midi* midi_out, GUI* ui, FAUSTFLOAT* zone,
                         FAUSTFLOAT min, FAUSTFLOAT max,
                         bool input = true, int chan = 0)
            :uiMidiTimedItem(midi_out, ui, zone, input, chan), fMin(min), fMax(max)
        {}
        virtual ~uiMidiProgChange()
        {}
        
        virtual void reflectZone()
        {
            FAUSTFLOAT v = *fZone;
            fCache = v;
            if (inRange(fMin, fMax, v)) {
                if (fChan == 0) {
                    // Send on [0..15] channels on the MIDI layer
                    for (int chan = 0; chan < 16; chan++) {
                        fMidiOut->progChange(chan, v);
                    }
                } else {
                    fMidiOut->progChange(fChan - 1, v);
                }
            }
        }
    
        void modifyZone(FAUSTFLOAT v)
        {
            if (fInputCtrl && inRange(fMin, fMax, v)) {
                uiItem::modifyZone(v);
            }
        }
    
        void modifyZone(double date, FAUSTFLOAT v)
        {
            if (fInputCtrl && inRange(fMin, fMax, v)) {
                uiMidiTimedItem::modifyZone(date, v);
            }
        }
        
};

/**
 * uiMidiChanPress.
 */
class uiMidiChanPress : public uiMidiTimedItem, public uiConverter {
    
    public:
    
        uiMidiChanPress(midi* midi_out, GUI* ui,
                        FAUSTFLOAT* zone,
                        FAUSTFLOAT min, FAUSTFLOAT max,
                        bool input = true,
                        MetaDataUI::Scale scale = MetaDataUI::kLin,
                        int chan = 0)
            :uiMidiTimedItem(midi_out, ui, zone, input, chan), uiConverter(scale, 0., 127., min, max)
        {}
        virtual ~uiMidiChanPress()
        {}
        
        virtual void reflectZone()
        {
            FAUSTFLOAT v = *fZone;
            fCache = v;
            int conv = std::round(fConverter->faust2ui(v));
            if (fChan == 0) {
                // Send on [0..15] channels on the MIDI layer
                for (int chan = 0; chan < 16; chan++) {
                    fMidiOut->chanPress(chan, conv);
                }
            } else {
                fMidiOut->chanPress(fChan - 1, conv);
            }
        }
    
        void modifyZone(FAUSTFLOAT v)
        {
            if (fInputCtrl) {
                uiItem::modifyZone(FAUSTFLOAT(fConverter->ui2faust(v)));
            }
        }
    
        void modifyZone(double date, FAUSTFLOAT v)
        {
            if (fInputCtrl) {
                uiMidiTimedItem::modifyZone(date, FAUSTFLOAT(fConverter->ui2faust(v)));
            }
        }
        
};

/**
 * uiMidiCtrlChange does scale (kLin/kLog/kExp) mapping.
 */
class uiMidiCtrlChange : public uiMidiTimedItem, public uiConverter {
    
    private:
    
        int fCtrl;
 
    public:

        uiMidiCtrlChange(midi* midi_out, int ctrl, GUI* ui,
                     FAUSTFLOAT* zone,
                     FAUSTFLOAT min, FAUSTFLOAT max,
                     bool input = true,
                     MetaDataUI::Scale scale = MetaDataUI::kLin,
                     int chan = 0)
            :uiMidiTimedItem(midi_out, ui, zone, input, chan), uiConverter(scale, 0., 127., min, max), fCtrl(ctrl)
        {}
        virtual ~uiMidiCtrlChange()
        {}
        
        virtual void reflectZone()
        {
            FAUSTFLOAT v = *fZone;
            fCache = v;
            int conv = std::round(fConverter->faust2ui(v));
            if (fChan == 0) {
                // Send on [0..15] channels on the MIDI layer
                for (int chan = 0; chan < 16; chan++) {
                    fMidiOut->ctrlChange(chan, fCtrl, conv);
                }
            } else {
                fMidiOut->ctrlChange(fChan - 1, fCtrl, conv);
            }
        }
        
        void modifyZone(FAUSTFLOAT v)
        {
            if (fInputCtrl) {
                uiItem::modifyZone(FAUSTFLOAT(fConverter->ui2faust(v)));
            }
        }
    
        void modifyZone(double date, FAUSTFLOAT v)
        {
            if (fInputCtrl) {
                uiMidiTimedItem::modifyZone(date, FAUSTFLOAT(fConverter->ui2faust(v)));
            }
        }
};

// Use a two segments linear converter
class uiMidiPitchWheel : public uiMidiTimedItem {

    private:
    
        LinearValueConverter2 fConverter;
    
    public:
    
        uiMidiPitchWheel(midi* midi_out, GUI* ui, FAUSTFLOAT* zone,
                         FAUSTFLOAT min, FAUSTFLOAT max,
                         bool input = true, int chan = 0)
            :uiMidiTimedItem(midi_out, ui, zone, input, chan)
        {
            if (min <= 0 && max >= 0) {
                fConverter = LinearValueConverter2(0., 8191., 16383., double(min), 0., double(max));
            } else {
                // Degenerated case...
                fConverter = LinearValueConverter2(0., 8191., 16383., double(min),double(min + (max - min)/FAUSTFLOAT(2)), double(max));
            }
        }
    
        virtual ~uiMidiPitchWheel()
        {}
        
        virtual void reflectZone()
        {
            FAUSTFLOAT v = *fZone;
            fCache = v;
            int conv = std::round(fConverter.faust2ui(v));
            if (fChan == 0) {
                // Send on [0..15] channels on the MIDI layer
                for (int chan = 0; chan < 16; chan++) {
                    fMidiOut->pitchWheel(chan, conv);
                }
            } else {
                fMidiOut->pitchWheel(fChan - 1, conv);
            }
        }
        
        void modifyZone(FAUSTFLOAT v)
        { 
            if (fInputCtrl) {
                uiItem::modifyZone(FAUSTFLOAT(fConverter.ui2faust(v)));
            }
        }
    
        void modifyZone(double date, FAUSTFLOAT v)
        {
            if (fInputCtrl) {
                uiMidiTimedItem::modifyZone(FAUSTFLOAT(fConverter.ui2faust(v)));
            }
        }
    
        void setRange(int val)
        {
            double semi = (val / 128) + ((val % 128) / 100.);
            fConverter.setMappingValues(0., 8191., 16383., -semi, 0., semi);
        }
 
};

/**
 * uiMidiKeyOn does scale (kLin/kLog/kExp) mapping for velocity.
 */
class uiMidiKeyOn : public uiMidiTimedItem, public uiConverter {

    private:
        
        int fKeyOn;
  
    public:
    
        uiMidiKeyOn(midi* midi_out, int key, GUI* ui,
                    FAUSTFLOAT* zone,
                    FAUSTFLOAT min, FAUSTFLOAT max,
                    bool input = true,
                    MetaDataUI::Scale scale = MetaDataUI::kLin,
                    int chan = 0)
            :uiMidiTimedItem(midi_out, ui, zone, input, chan), uiConverter(scale, 0., 127., min, max), fKeyOn(key)
        {}
        virtual ~uiMidiKeyOn()
        {}
        
        virtual void reflectZone()
        {
            FAUSTFLOAT v = *fZone;
            fCache = v;
            int conv = std::round(fConverter->faust2ui(v));
            if (fChan == 0) {
                // Send on [0..15] channels on the MIDI layer
                for (int chan = 0; chan < 16; chan++) {
                    fMidiOut->keyOn(chan, fKeyOn, conv);
                }
            } else {
                fMidiOut->keyOn(fChan - 1, fKeyOn, conv);
            }
        }
        
        void modifyZone(FAUSTFLOAT v)
        { 
            if (fInputCtrl) {
                uiItem::modifyZone(FAUSTFLOAT(fConverter->ui2faust(v)));
            }
        }
    
        void modifyZone(double date, FAUSTFLOAT v)
        {
            if (fInputCtrl) {
                uiMidiTimedItem::modifyZone(date, FAUSTFLOAT(fConverter->ui2faust(v)));
            }
        }
    
};

/**
 * uiMidiKeyOff does scale (kLin/kLog/kExp) mapping for velocity.
 */
class uiMidiKeyOff : public uiMidiTimedItem, public uiConverter {

    private:
        
        int fKeyOff;
  
    public:
    
        uiMidiKeyOff(midi* midi_out, int key, GUI* ui,
                     FAUSTFLOAT* zone,
                     FAUSTFLOAT min, FAUSTFLOAT max,
                     bool input = true,
                     MetaDataUI::Scale scale = MetaDataUI::kLin,
                     int chan = 0)
            :uiMidiTimedItem(midi_out, ui, zone, input, chan), uiConverter(scale, 0., 127., min, max), fKeyOff(key)
        {}
        virtual ~uiMidiKeyOff()
        {}
        
        virtual void reflectZone()
        {
            FAUSTFLOAT v = *fZone;
            fCache = v;
            int conv = std::round(fConverter->faust2ui(v));
            if (fChan == 0) {
                // Send on [0..15] channels on the MIDI layer
                for (int chan = 0; chan < 16; chan++) {
                    fMidiOut->keyOff(chan, fKeyOff, conv);
                }
            } else {
                fMidiOut->keyOff(fChan - 1, fKeyOff, conv);
            }
        }
        
        void modifyZone(FAUSTFLOAT v)
        { 
            if (fInputCtrl) {
                uiItem::modifyZone(FAUSTFLOAT(fConverter->ui2faust(v)));
            }
        }
    
        void modifyZone(double date, FAUSTFLOAT v)
        {
            if (fInputCtrl) {
                uiMidiTimedItem::modifyZone(date, FAUSTFLOAT(fConverter->ui2faust(v)));
            }
        }
    
};

/**
 * uiMidiKeyPress does scale (kLin/kLog/kExp) mapping for velocity.
 */
class uiMidiKeyPress : public uiMidiTimedItem, public uiConverter {

    private:
    
        int fKey;
  
    public:
    
        uiMidiKeyPress(midi* midi_out, int key, GUI* ui,
                       FAUSTFLOAT* zone,
                       FAUSTFLOAT min, FAUSTFLOAT max,
                       bool input = true,
                       MetaDataUI::Scale scale = MetaDataUI::kLin,
                       int chan = 0)
            :uiMidiTimedItem(midi_out, ui, zone, input, chan), uiConverter(scale, 0., 127., min, max), fKey(key)
        {}
        virtual ~uiMidiKeyPress()
        {}
        
        virtual void reflectZone()
        {
            FAUSTFLOAT v = *fZone;
            fCache = v;
            int conv = std::round(fConverter->faust2ui(v));
            if (fChan == 0) {
                // Send on [0..15] channels on the MIDI layer
                for (int chan = 0; chan < 16; chan++) {
                    fMidiOut->keyPress(chan, fKey, conv);
                }
            } else {
                fMidiOut->keyPress(fChan - 1, fKey, conv);
            }
        }
        
        void modifyZone(FAUSTFLOAT v)
        { 
            if (fInputCtrl) {
                uiItem::modifyZone(FAUSTFLOAT(fConverter->ui2faust(v)));
            }
        }
    
        void modifyZone(double date, FAUSTFLOAT v)
        {
            if (fInputCtrl) {
                uiMidiTimedItem::modifyZone(date, FAUSTFLOAT(fConverter->ui2faust(v)));
            }
        }
    
};

/******************************************************************************************
 * MidiUI : Faust User Interface
 * This class decodes MIDI metadata and maps incoming MIDI messages to them.
 * Currently ctrlChange, keyOn/keyOff, keyPress, progChange, chanPress, pitchWheel/pitchBend
 * start/stop/clock meta data are handled.
 *
 * Maps associating MIDI event ID (like each ctrl number) with all MIDI aware UI items
 * are defined and progressively filled when decoding MIDI related metadata.
 * MIDI aware UI items are used in both directions:
 *  - modifying their internal state when receving MIDI input events
 *  - sending their internal state as MIDI output events
 *******************************************************************************************/

class MidiUI : public GUI, public midi, public midi_interface, public MetaDataUI {

    // Add uiItem subclasses objects are deallocated by the inherited GUI class
    typedef std::map <int, std::vector<uiMidiCtrlChange*> > TCtrlChangeTable;
    typedef std::vector<uiMidiProgChange*>                  TProgChangeTable;
    typedef std::vector<uiMidiChanPress*>                   TChanPressTable;
    typedef std::map <int, std::vector<uiMidiKeyOn*> >      TKeyOnTable;
    typedef std::map <int, std::vector<uiMidiKeyOff*> >     TKeyOffTable;
    typedef std::map <int, std::vector<uiMidiKeyPress*> >   TKeyPressTable;
    typedef std::vector<uiMidiPitchWheel*>                  TPitchWheelTable;
    
    protected:
    
        TCtrlChangeTable fCtrlChangeTable;
        TProgChangeTable fProgChangeTable;
        TChanPressTable  fChanPressTable;
        TKeyOnTable      fKeyOnTable;
        TKeyOffTable     fKeyOffTable;
        TKeyOnTable      fKeyTable;
        TKeyPressTable   fKeyPressTable;
        TPitchWheelTable fPitchWheelTable;
        
        std::vector<uiMidiStart*> fStartTable;
        std::vector<uiMidiStop*>  fStopTable;
        std::vector<uiMidiClock*> fClockTable;
        
        std::vector<std::pair <std::string, std::string> > fMetaAux;
        
        midi_handler* fMidiHandler;
        bool fDelete;
        bool fTimeStamp;
    
        void addGenericZone(FAUSTFLOAT* zone, FAUSTFLOAT min, FAUSTFLOAT max, bool input = true)
        {
            if (fMetaAux.size() > 0) {
                for (size_t i = 0; i < fMetaAux.size(); i++) {
                    unsigned num;
                    unsigned chan;
                    if (fMetaAux[i].first == "midi") {
                        if (gsscanf(fMetaAux[i].second.c_str(), "ctrl %u %u", &num, &chan) == 2) {
                            fCtrlChangeTable[num].push_back(new uiMidiCtrlChange(fMidiHandler, num, this, zone, min, max, input, getScale(zone), chan));
                        } else if (gsscanf(fMetaAux[i].second.c_str(), "ctrl %u", &num) == 1) {
                            fCtrlChangeTable[num].push_back(new uiMidiCtrlChange(fMidiHandler, num, this, zone, min, max, input, getScale(zone)));
                        } else if (gsscanf(fMetaAux[i].second.c_str(), "keyon %u %u", &num, &chan) == 2) {
                            fKeyOnTable[num].push_back(new uiMidiKeyOn(fMidiHandler, num, this, zone, min, max, input, getScale(zone), chan));
                        } else if (gsscanf(fMetaAux[i].second.c_str(), "keyon %u", &num) == 1) {
                            fKeyOnTable[num].push_back(new uiMidiKeyOn(fMidiHandler, num, this, zone, min, max, input, getScale(zone)));
                        } else if (gsscanf(fMetaAux[i].second.c_str(), "keyoff %u %u", &num, &chan) == 2) {
                            fKeyOffTable[num].push_back(new uiMidiKeyOff(fMidiHandler, num, this, zone, min, max, input, getScale(zone), chan));
                        } else if (gsscanf(fMetaAux[i].second.c_str(), "keyoff %u", &num) == 1) {
                            fKeyOffTable[num].push_back(new uiMidiKeyOff(fMidiHandler, num, this, zone, min, max, input, getScale(zone)));
                        } else if (gsscanf(fMetaAux[i].second.c_str(), "key %u %u", &num, &chan) == 2) {
                            fKeyTable[num].push_back(new uiMidiKeyOn(fMidiHandler, num, this, zone, min, max, input, getScale(zone), chan));
                        } else if (gsscanf(fMetaAux[i].second.c_str(), "key %u", &num) == 1) {
                            fKeyTable[num].push_back(new uiMidiKeyOn(fMidiHandler, num, this, zone, min, max, input, getScale(zone)));
                        } else if (gsscanf(fMetaAux[i].second.c_str(), "keypress %u %u", &num, &chan) == 2) {
                            fKeyPressTable[num].push_back(new uiMidiKeyPress(fMidiHandler, num, this, zone, min, max, input, getScale(zone), chan));
                        } else if (gsscanf(fMetaAux[i].second.c_str(), "keypress %u", &num) == 1) {
                            fKeyPressTable[num].push_back(new uiMidiKeyPress(fMidiHandler, num, this, zone, min, max, input, getScale(zone)));
                        } else if (gsscanf(fMetaAux[i].second.c_str(), "pgm %u", &chan) == 1) {
                            fProgChangeTable.push_back(new uiMidiProgChange(fMidiHandler, this, zone, min, max, input, chan));
                        } else if (strcmp(fMetaAux[i].second.c_str(), "pgm") == 0) {
                            fProgChangeTable.push_back(new uiMidiProgChange(fMidiHandler, this, zone, min, max, input));
                        } else if (gsscanf(fMetaAux[i].second.c_str(), "chanpress %u", &chan) == 1) {
                            fChanPressTable.push_back(new uiMidiChanPress(fMidiHandler, this, zone, min, max, input, getScale(zone), chan));
                        } else if ((fMetaAux[i].second == "chanpress")) {
                            fChanPressTable.push_back(new uiMidiChanPress(fMidiHandler, this, zone, min, max, input, getScale(zone)));
                        } else if ((gsscanf(fMetaAux[i].second.c_str(), "pitchwheel %u", &chan) == 1) || (gsscanf(fMetaAux[i].second.c_str(), "pitchbend %u", &chan) == 1)) {
                            fPitchWheelTable.push_back(new uiMidiPitchWheel(fMidiHandler, this, zone, min, max, input, chan));
                        } else if ((fMetaAux[i].second == "pitchwheel") || (fMetaAux[i].second == "pitchbend")) {
                            fPitchWheelTable.push_back(new uiMidiPitchWheel(fMidiHandler, this, zone, min, max, input));
                        // MIDI sync
                        } else if (fMetaAux[i].second == "start") {
                            fStartTable.push_back(new uiMidiStart(fMidiHandler, this, zone, input));
                        } else if (fMetaAux[i].second == "stop") {
                            fStopTable.push_back(new uiMidiStop(fMidiHandler, this, zone, input));
                        } else if (fMetaAux[i].second == "clock") {
                            fClockTable.push_back(new uiMidiClock(fMidiHandler, this, zone, input));
                        // Explicit metadata to activate 'timestamp' mode
                        } else if (fMetaAux[i].second == "timestamp") {
                            fTimeStamp = true;
                        }
                    }
                }
            }
            fMetaAux.clear();
        }
    
        template <typename TABLE>
        void updateTable1(TABLE& table, double date, int channel, int val1)
        {
            for (size_t i = 0; i < table.size(); i++) {
                int channel_aux = table[i]->fChan;
                // channel_aux == 0 means "all channels"
                if (channel_aux == 0 || channel == channel_aux - 1) {
                    if (fTimeStamp) {
                        table[i]->modifyZone(date, FAUSTFLOAT(val1));
                    } else {
                        table[i]->modifyZone(FAUSTFLOAT(val1));
                    }
                }
            }
        }
        
        template <typename TABLE>
        void updateTable2(TABLE& table, double date, int channel, int val1, int val2)
        {
            if (table.find(val1) != table.end()) {
                for (size_t i = 0; i < table[val1].size(); i++) {
                    int channel_aux = table[val1][i]->fChan;
                    // channel_aux == 0 means "all channels"
                    if (channel_aux == 0 || channel == channel_aux - 1) {
                        if (fTimeStamp) {
                            table[val1][i]->modifyZone(date, FAUSTFLOAT(val2));
                        } else {
                            table[val1][i]->modifyZone(FAUSTFLOAT(val2));
                        }
                    }
                }
            }
        }
    
    public:
    
        MidiUI(midi_handler* midi_handler, bool delete_handler = false)
        {
            fMidiHandler = midi_handler;
            fMidiHandler->addMidiIn(this);
            // TODO: use shared_ptr based implementation
            fDelete = delete_handler;
            fTimeStamp = false;
        }
 
        virtual ~MidiUI() 
        {
            // Remove from fMidiHandler
            fMidiHandler->removeMidiIn(this);
            // TODO: use shared_ptr based implementation
            if (fDelete) delete fMidiHandler;
        }
    
        bool run() { return fMidiHandler->startMidi(); }
        void stop() { fMidiHandler->stopMidi(); }
        
        void addMidiIn(midi* midi_dsp) { fMidiHandler->addMidiIn(midi_dsp); }
        void removeMidiIn(midi* midi_dsp) { fMidiHandler->removeMidiIn(midi_dsp); }
      
        // -- active widgets
        
        virtual void addButton(const char* label, FAUSTFLOAT* zone)
        {
            addGenericZone(zone, FAUSTFLOAT(0), FAUSTFLOAT(1));
        }
        virtual void addCheckButton(const char* label, FAUSTFLOAT* zone)
        {
            addGenericZone(zone, FAUSTFLOAT(0), FAUSTFLOAT(1));
        }
        
        virtual void addVerticalSlider(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step)
        {
            addGenericZone(zone, min, max);
        }
        virtual void addHorizontalSlider(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step)
        {
            addGenericZone(zone, min, max);
        }
        virtual void addNumEntry(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step)
        {
            addGenericZone(zone, min, max);
        }

        // -- passive widgets

        virtual void addHorizontalBargraph(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT min, FAUSTFLOAT max) 
        {
            addGenericZone(zone, min, max, false);
        }
        virtual void addVerticalBargraph(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT min, FAUSTFLOAT max)
        {
            addGenericZone(zone, min, max, false);
        }

        // -- metadata declarations

        virtual void declare(FAUSTFLOAT* zone, const char* key, const char* val)
        {
            MetaDataUI::declare(zone, key, val);
            fMetaAux.push_back(std::make_pair(key, val));
        }
        
        // -- MIDI API
    
        void key(double date, int channel, int note, int velocity)
        {
            updateTable2<TKeyOnTable>(fKeyTable, date, channel, note, velocity);
        }
    
        MapUI* keyOn(double date, int channel, int note, int velocity)
        {
            updateTable2<TKeyOnTable>(fKeyOnTable, date, channel, note, velocity);
            // If note is in fKeyTable, handle it as a keyOn
            key(date, channel, note, velocity);
            return nullptr;
        }
        
        void keyOff(double date, int channel, int note, int velocity)
        {
            updateTable2<TKeyOffTable>(fKeyOffTable, date, channel, note, velocity);
            // If note is in fKeyTable, handle it as a keyOff with a 0 velocity
            key(date, channel, note, 0);
        }
        
        void ctrlChange(double date, int channel, int ctrl, int value)
        {
            updateTable2<TCtrlChangeTable>(fCtrlChangeTable, date, channel, ctrl, value);
        }
    
        void rpn(double date, int channel, int ctrl, int value)
        {
            if (ctrl == midi::PITCH_BEND_RANGE) {
                for (size_t i = 0; i < fPitchWheelTable.size(); i++) {
                    // channel_aux == 0 means "all channels"
                    int channel_aux = fPitchWheelTable[i]->fChan;
                    if (channel_aux == 0 || channel == channel_aux - 1) {
                        fPitchWheelTable[i]->setRange(value);
                    }
                }
            }
        }
    
        void progChange(double date, int channel, int pgm)
        {
            updateTable1<TProgChangeTable>(fProgChangeTable, date, channel, pgm);
        }
        
        void pitchWheel(double date, int channel, int wheel) 
        {
            updateTable1<TPitchWheelTable>(fPitchWheelTable, date, channel, wheel);
        }
        
        void keyPress(double date, int channel, int pitch, int press) 
        {
            updateTable2<TKeyPressTable>(fKeyPressTable, date, channel, pitch, press);
        }
        
        void chanPress(double date, int channel, int press)
        {
            updateTable1<TChanPressTable>(fChanPressTable, date, channel, press);
        }
        
        void ctrlChange14bits(double date, int channel, int ctrl, int value) {}
        
        // MIDI sync
        
        void startSync(double date)
        {
            for (size_t i = 0; i < fStartTable.size(); i++) {
                fStartTable[i]->modifyZone(date, FAUSTFLOAT(1));
            }
        }
        
        void stopSync(double date)
        {
            for (size_t i = 0; i < fStopTable.size(); i++) {
                fStopTable[i]->modifyZone(date, FAUSTFLOAT(0));
            }
        }
        
        void clock(double date)
        {
            for (size_t i = 0; i < fClockTable.size(); i++) {
                fClockTable[i]->modifyZone(date, FAUSTFLOAT(1));
            }
        }
};

#endif // FAUST_MIDIUI_H
/**************************  END  MidiUI.h **************************/
/************************** BEGIN mspUI.h **************************
 FAUST Architecture File
 Copyright (C) 2003-2022 GRAME, Centre National de Creation Musicale
 ---------------------------------------------------------------------
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation; either version 2.1 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 
 EXCEPTION : As a special exception, you may create a larger work
 that contains this FAUST architecture section and distribute
 that work under terms of your choice, so long as this FAUST
 architecture section is not modified.
 
 mspUI.h for static Max/MSP externals and faustgen~
 Created by Martin Di Rollo on 18/04/12.
 ********************************************************************/

#ifndef _mspUI_h
#define _mspUI_h

#include <math.h>
#include <assert.h>
#include <string>
#include <map>


#define STR_SIZE    512
#define MULTI_SIZE  256

#ifdef WIN32
#include <stdio.h>
#define snprintf _snprintf
#ifndef NAN
    static const unsigned long __nan[2] = {0xffffffff, 0x7fffffff};
    #define NAN (*(const float *) __nan)
#endif
#endif

struct Max_Meta1 : Meta
{
    int fCount;
    
    Max_Meta1():fCount(0)
    {}
    
    void declare(const char* key, const char* value)
    {
        if ((strcmp("name", key) == 0) || (strcmp("author", key) == 0)) {
            fCount++;
        }
    }
};

struct Max_Meta2 : Meta
{
    void declare(const char* key, const char* value)
    {
        if ((strcmp("name", key) == 0) || (strcmp("author", key) == 0)) {
            post("%s : %s", key, value);
        }
    }
};

struct Max_Meta3 : Meta
{
    std::string fName;
    
    bool endWith(const std::string& str, const std::string& suffix)
    {
        size_t i = str.rfind(suffix);
        return (i != std::string::npos) && (i == (str.length() - suffix.length()));
    }
    
    void declare(const char* key, const char* value)
    {
        if ((strcmp("filename", key) == 0)) {
            std::string val = value;
            if (endWith(value, ".dsp")) {
                fName = "com.grame." + val.substr(0, val.size() - 4) + "~";
            } else {
                fName = "com.grame." + val + "~";
            }
        }
    }
};

class mspUIObject {
    
    protected:
        
        std::string fLabel;
        FAUSTFLOAT* fZone;
        
        FAUSTFLOAT range(FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT val) {return (val < min) ? min : (val > max) ? max : val;}
        
    public:
        
        mspUIObject(const std::string& label, FAUSTFLOAT* zone):fLabel(label),fZone(zone) {}
        virtual ~mspUIObject() {}
        
        virtual void setValue(FAUSTFLOAT f) { *fZone = range(0.0, 1.0, f); }
        virtual FAUSTFLOAT getValue() { return *fZone; }
    
        virtual FAUSTFLOAT getInitValue() { return FAUSTFLOAT(0); }
        virtual FAUSTFLOAT getMinValue() { return FAUSTFLOAT(0); }
        virtual FAUSTFLOAT getMaxValue() { return FAUSTFLOAT(0); }
    
        virtual void toString(char* buffer) {}
        virtual std::string getName() { return fLabel; }
};

class mspCheckButton : public mspUIObject {
    
    public:
        
        mspCheckButton(const std::string& label, FAUSTFLOAT* zone):mspUIObject(label,zone) {}
        virtual ~mspCheckButton() {}
        
        void toString(char* buffer)
        {
            snprintf(buffer, STR_SIZE, "CheckButton(float): %s", fLabel.c_str());
        }
};

class mspButton : public mspUIObject {
    
    public:
        
        mspButton(const std::string& label, FAUSTFLOAT* zone):mspUIObject(label,zone) {}
        virtual ~mspButton() {}
        
        void toString(char* buffer)
        {
            snprintf(buffer, STR_SIZE, "Button(float): %s", fLabel.c_str());
        }
};

class mspSlider : public mspUIObject {
    
    private:
        
        FAUSTFLOAT fInit;
        FAUSTFLOAT fMin;
        FAUSTFLOAT fMax;
        FAUSTFLOAT fStep;
        
    public:
        
        mspSlider(const std::string& label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step)
        :mspUIObject(label,zone),fInit(init),fMin(min),fMax(max),fStep(step) {}
        virtual ~mspSlider() {}
        
        void toString(char* buffer)
        {
            std::stringstream str;
            str << "Slider(float): " << fLabel << " [init=" << fInit << ":min=" << fMin << ":max=" << fMax << ":step=" << fStep << ":cur=" << *fZone << "]";
            std::string res = str.str();
            snprintf(buffer, STR_SIZE, "%s", res.c_str());
        }
        
        void setValue(FAUSTFLOAT f) { *fZone = range(fMin, fMax, f); }
    
        virtual FAUSTFLOAT getInitValue() { return fInit; }
        virtual FAUSTFLOAT getMinValue() { return fMin; }
        virtual FAUSTFLOAT getMaxValue() { return fMax; }
    
};

class mspBargraph : public mspUIObject {
    
    private:
        
        FAUSTFLOAT fMin;
        FAUSTFLOAT fMax;
        FAUSTFLOAT fCurrent;
        
    public:
        
        mspBargraph(const std::string& label, FAUSTFLOAT* zone, FAUSTFLOAT min, FAUSTFLOAT max)
        :mspUIObject(label,zone), fMin(min), fMax(max), fCurrent(*zone) {}
        virtual ~mspBargraph() {}
        
        void toString(char* buffer)
        {
            std::stringstream str;
            str << "Bargraph(float): " << fLabel << " [min=" << fMin << ":max=" << fMax << ":cur=" << *fZone << "]";
            std::string res = str.str();
            snprintf(buffer, STR_SIZE, "%s", res.c_str());
        }
    
        // special version
        virtual FAUSTFLOAT getValue(bool& new_val)
        {
            if (*fZone != fCurrent) {
                fCurrent = *fZone;
                new_val = true;
            } else {
                new_val = false;
            }
            return fCurrent;
        }
    
        virtual FAUSTFLOAT getMinValue() { return fMin; }
        virtual FAUSTFLOAT getMaxValue() { return fMax; }
    
};

class mspUI : public UI, public PathBuilder
{
    private:
        
        std::map<std::string, mspUIObject*> fInputLabelTable;      // Input table using labels
        std::map<std::string, mspUIObject*> fInputShortnameTable;  // Input table using shortnames
        std::map<std::string, mspUIObject*> fInputPathTable;       // Input table using paths
        std::map<std::string, mspUIObject*> fOutputLabelTable;     // Table containing bargraph with labels
        std::map<std::string, mspUIObject*> fOutputShortnameTable; // Table containing bargraph with shortnames
        std::map<std::string, mspUIObject*> fOutputPathTable;      // Table containing bargraph with paths
        
        std::map<const char*, const char*> fDeclareTable;
        
        FAUSTFLOAT* fMultiTable[MULTI_SIZE];
        int fMultiIndex;
        int fMultiControl;
        
        std::string createLabel(const char* label)
        {
            std::map<const char*, const char*>::reverse_iterator it;
            if (fDeclareTable.size() > 0) {
                std::string res = std::string(label);
                char sep = '[';
                for (it = fDeclareTable.rbegin(); it != fDeclareTable.rend(); it++) {
                    res = res + sep + (*it).first + ":" + (*it).second;
                    sep = ',';
                }
                res += ']';
                fDeclareTable.clear();
                return res;
            } else {
                return std::string(label);
            }
        }
    
        void addSlider(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step)
        {
            mspUIObject* obj = new mspSlider(createLabel(label), zone, init, min, max, step);
            fInputLabelTable[std::string(label)] = obj;
            std::string path = buildPath(label);
            fInputPathTable[path] = obj;
            fFullPaths.push_back(path);
            fDeclareTable.clear();
        }
    
        void addBargraph(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT min, FAUSTFLOAT max)
        {
            mspUIObject* obj = new mspBargraph(createLabel(label), zone, min, max);
            fOutputLabelTable[std::string(label)] = obj;
            std::string path = buildPath(label);
            fOutputPathTable[path] = obj;
            fFullPaths.push_back(path);
            fDeclareTable.clear();
        }
    
    public:
        
        typedef std::map<std::string, mspUIObject*>::iterator iterator;
        
        mspUI()
        {
            for (int i = 0; i < MULTI_SIZE; i++) {
                fMultiTable[i] = 0;
            }
            fMultiIndex = fMultiControl = 0;
        }
    
        virtual ~mspUI()
        {
            clear();
        }
        
        void addButton(const char* label, FAUSTFLOAT* zone)
        {
            mspUIObject* obj = new mspButton(createLabel(label), zone);
            fInputLabelTable[std::string(label)] = obj;
            std::string path = buildPath(label);
            fInputPathTable[path] = obj;
            fFullPaths.push_back(path);
        }
        
        void addCheckButton(const char* label, FAUSTFLOAT* zone)
        {
            mspUIObject* obj = new mspCheckButton(createLabel(label), zone);
            fInputLabelTable[std::string(label)] = obj;
            std::string path = buildPath(label);
            fInputPathTable[path] = obj;
            fFullPaths.push_back(path);
        }
        
        void addVerticalSlider(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step)
        {
            addSlider(label, zone, init, min, max, step);
        }
        
        void addHorizontalSlider(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step)
        {
            addSlider(label, zone, init, min, max, step);
        }
        
        void addNumEntry(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step)
        {
            addSlider(label, zone, init, min, max, step);
        }
    
        void addHorizontalBargraph(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT min, FAUSTFLOAT max)
        {
            addBargraph(label, zone, min, max);
        }
    
        void addVerticalBargraph(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT min, FAUSTFLOAT max)
        {
            addBargraph(label, zone, min, max);
        }
        
        void addSoundfile(const char* label, const char* filename, Soundfile** sf_zone) {}
        
        void openTabBox(const char* label) { pushLabel(label); fDeclareTable.clear(); }
        void openHorizontalBox(const char* label) { pushLabel(label); fDeclareTable.clear(); }
        void openVerticalBox(const char* label) { pushLabel(label); fDeclareTable.clear(); }
        void closeBox()
        {
            fDeclareTable.clear();
            if (popLabel()) {
                // Shortnames can be computed when all fullnames are known
                computeShortNames();
                // Fill 'shortname' map
                for (const auto& path : fFullPaths) {
                    if (fInputPathTable.count(path)) {
                        fInputShortnameTable[fFull2Short[path]] = fInputPathTable[path];
                    } else if (fOutputPathTable.count(path)) {
                        fOutputShortnameTable[fFull2Short[path]] = fOutputPathTable[path];
                    } else {
                        assert(false);
                    }
                }
             }
        }
        
        virtual void declare(FAUSTFLOAT* zone, const char* key, const char* val)
        {
            if (strcmp(key, "multi") == 0) {
                int index = atoi(val);
                if (index >= 0 && index < MULTI_SIZE) {
                    fMultiTable[index] = zone;
                    fMultiControl++;
                } else {
                    post("Invalid multi index = %d", index);
                }
            }
            
            fDeclareTable[key] = val;
        }
        
        void setMultiValues(FAUSTFLOAT* multi, int buffer_size)
        {
            for (int read = 0; read < buffer_size; read++) {
                int write = (fMultiIndex + read) & (MULTI_SIZE - 1);
                if (fMultiTable[write]) {
                    *fMultiTable[write] = multi[read];
                }
            }
            fMultiIndex += buffer_size;
        }
        
        bool isMulti() { return fMultiControl > 0; }
        
        bool isValue(const std::string& name)
        {
            return (isOutputValue(name) || isInputValue(name));
        }
    
        bool isInputValue(const std::string& name)
        {
            return fInputLabelTable.count(name) || fInputShortnameTable.count(name) || fInputPathTable.count(name);
        }
    
        bool isOutputValue(const std::string& name)
        {
            return fOutputLabelTable.count(name) || fOutputShortnameTable.count(name) || fOutputPathTable.count(name);
        }
    
        bool setValue(const std::string& name, FAUSTFLOAT val)
        {
            if (fInputLabelTable.count(name)) {
                fInputLabelTable[name]->setValue(val);
                return true;
            } else if (fInputShortnameTable.count(name)) {
                fInputShortnameTable[name]->setValue(val);
                return true;
            } else if (fInputPathTable.count(name)) {
                fInputPathTable[name]->setValue(val);
                return true;
            } else {
                return false;
            }
        }
    
        FAUSTFLOAT getOutputValue(const std::string& name, bool& new_val)
        {
            return static_cast<mspBargraph*>(fOutputPathTable[name])->getValue(new_val);
        }
        
        iterator begin1() { return fInputLabelTable.begin(); }
        iterator end1() { return fInputLabelTable.end(); }
        
        iterator begin2() { return fInputPathTable.begin(); }
        iterator end2() { return fInputPathTable.end(); }
    
        iterator begin3() { return fOutputLabelTable.begin(); }
        iterator end3() { return fOutputLabelTable.end(); }
    
        iterator begin4() { return fOutputPathTable.begin(); }
        iterator end4() { return fOutputPathTable.end(); }
    
        int inputItemsCount() { return fInputLabelTable.size(); }
        int outputItemsCount() { return fOutputLabelTable.size(); }
    
        void clear()
        {
            for (const auto& it : fInputLabelTable) {
                delete it.second;
            }
            fInputLabelTable.clear();
            fInputShortnameTable.clear();
            fInputPathTable.clear();
            
            for (const auto& it : fOutputLabelTable) {
                delete it.second;
            }
            fOutputLabelTable.clear();
            fOutputShortnameTable.clear();
            fOutputPathTable.clear();
        }
        
        void displayControls()
        {
            post("------- Range, shortname and path ----------");
            for (const auto& it : fInputPathTable) {
                char param[STR_SIZE];
                it.second->toString(param);
                post(param);
                std::string shortname = "Shortname: " + fFull2Short[it.first];
                post(shortname.c_str());
                std::string path = "Complete path: " + it.first;
                post(path.c_str());
            }
            post("---------------------------------------------");
        }
    
        static bool checkDigit(const std::string& name)
        {
            for (int i = name.size() - 1; i >= 0; i--) {
                if (isdigit(name[i])) { return true; }
            }
            return false;
        }
        
        static int countDigit(const std::string& name)
        {
            int count = 0;
            for (int i = name.size() - 1; i >= 0; i--) {
                if (isdigit(name[i])) { count++; }
            }
            return count;
        }

};

//==============
// MIDI handler
//==============

struct max_midi : public midi_handler {
    
    void* m_midi_outlet = NULL;
    
    max_midi(void* midi_outlet = NULL):m_midi_outlet(midi_outlet)
    {}
    
    void sendMessage(std::vector<unsigned char>& message)
    {
        assert(m_midi_outlet);
        for (int i = 0; i < message.size(); i++) {
            outlet_int(m_midi_outlet, message[i]);
        }
    }
    
    // MIDI output API
    MapUI* keyOn(int channel, int pitch, int velocity)
    {
        std::vector<unsigned char> message = {ucast(MIDI_NOTE_ON + channel), ucast(pitch), ucast(velocity)};
        sendMessage(message);
        return NULL;
    }
    
    void keyOff(int channel, int pitch, int velocity)
    {
        std::vector<unsigned char> message = {ucast(MIDI_NOTE_OFF + channel), ucast(pitch), ucast(velocity)};
        sendMessage(message);
    }
    
    void ctrlChange(int channel, int ctrl, int val)
    {
        std::vector<unsigned char> message = {ucast(MIDI_CONTROL_CHANGE + channel), ucast(ctrl), ucast(val)};
        sendMessage(message);
    }
    
    void chanPress(int channel, int press)
    {
        std::vector<unsigned char> message = {ucast(MIDI_AFTERTOUCH + channel), ucast(press)};
        sendMessage(message);
    }
    
    void progChange(int channel, int pgm)
    {
        std::vector<unsigned char> message = {ucast(MIDI_PROGRAM_CHANGE + channel), ucast(pgm)};
        sendMessage(message);
    }
    
    void keyPress(int channel, int pitch, int press)
    {
        std::vector<unsigned char> message = {ucast(MIDI_POLY_AFTERTOUCH + channel), ucast(pitch), ucast(press)};
        sendMessage(message);
    }
    
    void pitchWheel(int channel, int wheel)
    {
        // lsb 7bit, msb 7bit
        std::vector<unsigned char> message = {ucast(MIDI_PITCH_BEND + channel), ucast(wheel & 0x7F), ucast((wheel >> 7) & 0x7F)};
        sendMessage(message);
    }
    
    void ctrlChange14bits(int channel, int ctrl, int value) {}
    
    void startSync(double date)
    {
        std::vector<unsigned char> message = {ucast(MIDI_START)};
        sendMessage(message);
    }
    
    void stopSync(double date)
    {
        std::vector<unsigned char> message = {ucast(MIDI_STOP)};
        sendMessage(message);
    }
    
    void clock(double date)
    {
        std::vector<unsigned char> message = {ucast(MIDI_CLOCK)};
        sendMessage(message);
    }
    
    void sysEx(double, std::vector<unsigned char>& message)
    {
        sendMessage(message);
    }
};

#endif
/**************************  END  mspUI.h **************************/
/************************** BEGIN poly-dsp.h *************************
FAUST Architecture File
Copyright (C) 2003-2022 GRAME, Centre National de Creation Musicale
---------------------------------------------------------------------
This program is free software; you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation; either version 2.1 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

EXCEPTION : As a special exception, you may create a larger work
that contains this FAUST architecture section and distribute
that work under terms of your choice, so long as this FAUST
architecture section is not modified.
*********************************************************************/

#ifndef __poly_dsp__
#define __poly_dsp__

#include <stdio.h>
#include <string>
#include <cmath>
#include <algorithm>
#include <functional>
#include <ostream>
#include <sstream>
#include <vector>
#include <limits.h>
#include <float.h>
#include <assert.h>

/************************** BEGIN proxy-dsp.h ***************************
FAUST Architecture File
Copyright (C) 2003-2022 GRAME, Centre National de Creation Musicale
---------------------------------------------------------------------
This program is free software; you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation; either version 2.1 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

EXCEPTION : As a special exception, you may create a larger work
that contains this FAUST architecture section and distribute
that work under terms of your choice, so long as this FAUST
architecture section is not modified.
***************************************************************************/

#ifndef __proxy_dsp__
#define __proxy_dsp__

#include <vector>
#include <map>

/************************** BEGIN JSONUIDecoder.h **************************
 FAUST Architecture File
 Copyright (C) 2003-2022 GRAME, Centre National de Creation Musicale
 ---------------------------------------------------------------------
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation; either version 2.1 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 
 EXCEPTION : As a special exception, you may create a larger work
 that contains this FAUST architecture section and distribute
 that work under terms of your choice, so long as this FAUST
 architecture section is not modified.
 *************************************************************************/

#ifndef __JSONUIDecoder__
#define __JSONUIDecoder__

#include <vector>
#include <map>
#include <utility>
#include <cstdlib>
#include <sstream>
#include <functional>

/************************** BEGIN CGlue.h *****************************
FAUST Architecture File
Copyright (C) 2003-2022 GRAME, Centre National de Creation Musicale
---------------------------------------------------------------------
This program is free software; you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation; either version 2.1 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

EXCEPTION : As a special exception, you may create a larger work
that contains this FAUST architecture section and distribute
that work under terms of your choice, so long as this FAUST
architecture section is not modified.
*************************************************************************/

#ifndef CGLUE_H
#define CGLUE_H

/************************** BEGIN CInterface.h **************************
 FAUST Architecture File
 Copyright (C) 2003-2022 GRAME, Centre National de Creation Musicale
 ---------------------------------------------------------------------
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation; either version 2.1 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 
 EXCEPTION : As a special exception, you may create a larger work
 that contains this FAUST architecture section and distribute
 that work under terms of your choice, so long as this FAUST
 architecture section is not modified.
 *************************************************************************/

#ifndef CINTERFACE_H
#define CINTERFACE_H

#ifndef FAUSTFLOAT
#define FAUSTFLOAT float
#endif

#include <stdlib.h>

#ifdef __cplusplus
extern "C" {
#endif
    
struct Soundfile;

/*******************************************************************************
 * UI, Meta and MemoryManager structures for C code.
 ******************************************************************************/

// -- widget's layouts

typedef void (* openTabBoxFun) (void* ui_interface, const char* label);
typedef void (* openHorizontalBoxFun) (void* ui_interface, const char* label);
typedef void (* openVerticalBoxFun) (void* ui_interface, const char* label);
typedef void (* closeBoxFun) (void* ui_interface);

// -- active widgets

typedef void (* addButtonFun) (void* ui_interface, const char* label, FAUSTFLOAT* zone);
typedef void (* addCheckButtonFun) (void* ui_interface, const char* label, FAUSTFLOAT* zone);
typedef void (* addVerticalSliderFun) (void* ui_interface, const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step);
typedef void (* addHorizontalSliderFun) (void* ui_interface, const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step);
typedef void (* addNumEntryFun) (void* ui_interface, const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step);

// -- passive widgets

typedef void (* addHorizontalBargraphFun) (void* ui_interface, const char* label, FAUSTFLOAT* zone, FAUSTFLOAT min, FAUSTFLOAT max);
typedef void (* addVerticalBargraphFun) (void* ui_interface, const char* label, FAUSTFLOAT* zone, FAUSTFLOAT min, FAUSTFLOAT max);

// -- soundfiles
    
typedef void (* addSoundfileFun) (void* ui_interface, const char* label, const char* url, struct Soundfile** sf_zone);

typedef void (* declareFun) (void* ui_interface, FAUSTFLOAT* zone, const char* key, const char* value);

typedef struct {

    void* uiInterface;

    openTabBoxFun openTabBox;
    openHorizontalBoxFun openHorizontalBox;
    openVerticalBoxFun openVerticalBox;
    closeBoxFun closeBox;
    addButtonFun addButton;
    addCheckButtonFun addCheckButton;
    addVerticalSliderFun addVerticalSlider;
    addHorizontalSliderFun addHorizontalSlider;
    addNumEntryFun addNumEntry;
    addHorizontalBargraphFun addHorizontalBargraph;
    addVerticalBargraphFun addVerticalBargraph;
    addSoundfileFun addSoundfile;
    declareFun declare;

} UIGlue;

typedef void (* metaDeclareFun) (void* ui_interface, const char* key, const char* value);

typedef struct {

    void* metaInterface;
    
    metaDeclareFun declare;

} MetaGlue;

/***************************************
 *  Interface for the DSP object
 ***************************************/

typedef char dsp_imp;
    
typedef dsp_imp* (* newDspFun) ();
typedef void (* destroyDspFun) (dsp_imp* dsp);
typedef int (* getNumInputsFun) (dsp_imp* dsp);
typedef int (* getNumOutputsFun) (dsp_imp* dsp);
typedef void (* buildUserInterfaceFun) (dsp_imp* dsp, UIGlue* ui);
typedef int (* getSampleRateFun) (dsp_imp* dsp);
typedef void (* initFun) (dsp_imp* dsp, int sample_rate);
typedef void (* classInitFun) (int sample_rate);
typedef void (* instanceInitFun) (dsp_imp* dsp, int sample_rate);
typedef void (* instanceConstantsFun) (dsp_imp* dsp, int sample_rate);
typedef void (* instanceResetUserInterfaceFun) (dsp_imp* dsp);
typedef void (* instanceClearFun) (dsp_imp* dsp);
typedef void (* computeFun) (dsp_imp* dsp, int len, FAUSTFLOAT** inputs, FAUSTFLOAT** outputs);
typedef void (* metadataFun) (MetaGlue* meta);
    
/***************************************
 * DSP memory manager functions
 ***************************************/

typedef void* (* allocateFun) (void* manager_interface, size_t size);
typedef void (* destroyFun) (void* manager_interface, void* ptr);

typedef struct {
    
    void* managerInterface;
    
    allocateFun allocate;
    destroyFun destroy;
    
} MemoryManagerGlue;

#ifdef __cplusplus
}
#endif

#endif
/**************************  END  CInterface.h **************************/

#ifdef __cplusplus
extern "C" {
#endif

/*******************************************************************************
 * UI glue code
 ******************************************************************************/
 
class UIFloat
{

    public:

        UIFloat() {}

        virtual ~UIFloat() {}

        // -- widget's layouts

        virtual void openTabBox(const char* label) = 0;
        virtual void openHorizontalBox(const char* label) = 0;
        virtual void openVerticalBox(const char* label) = 0;
        virtual void closeBox() = 0;

        // -- active widgets

        virtual void addButton(const char* label, float* zone) = 0;
        virtual void addCheckButton(const char* label, float* zone) = 0;
        virtual void addVerticalSlider(const char* label, float* zone, float init, float min, float max, float step) = 0;
        virtual void addHorizontalSlider(const char* label, float* zone, float init, float min, float max, float step) = 0;
        virtual void addNumEntry(const char* label, float* zone, float init, float min, float max, float step) = 0;

        // -- passive widgets

        virtual void addHorizontalBargraph(const char* label, float* zone, float min, float max) = 0;
        virtual void addVerticalBargraph(const char* label, float* zone, float min, float max) = 0;
    
        // -- soundfiles
    
        virtual void addSoundfile(const char* label, const char* filename, Soundfile** sf_zone) = 0;

        // -- metadata declarations

        virtual void declare(float* zone, const char* key, const char* val) {}
};

static void openTabBoxGlueFloat(void* cpp_interface, const char* label)
{
    UIFloat* ui_interface = static_cast<UIFloat*>(cpp_interface);
    ui_interface->openTabBox(label);
}

static void openHorizontalBoxGlueFloat(void* cpp_interface, const char* label)
{
    UIFloat* ui_interface = static_cast<UIFloat*>(cpp_interface);
    ui_interface->openHorizontalBox(label);
}

static void openVerticalBoxGlueFloat(void* cpp_interface, const char* label)
{
    UIFloat* ui_interface = static_cast<UIFloat*>(cpp_interface);
    ui_interface->openVerticalBox(label);
}

static void closeBoxGlueFloat(void* cpp_interface)
{
    UIFloat* ui_interface = static_cast<UIFloat*>(cpp_interface);
    ui_interface->closeBox();
}

static void addButtonGlueFloat(void* cpp_interface, const char* label, float* zone)
{
    UIFloat* ui_interface = static_cast<UIFloat*>(cpp_interface);
    ui_interface->addButton(label, zone);
}

static void addCheckButtonGlueFloat(void* cpp_interface, const char* label, float* zone)
{
    UIFloat* ui_interface = static_cast<UIFloat*>(cpp_interface);
    ui_interface->addCheckButton(label, zone);
}

static void addVerticalSliderGlueFloat(void* cpp_interface, const char* label, float* zone, float init, float min, float max, float step)
{
    UIFloat* ui_interface = static_cast<UIFloat*>(cpp_interface);
    ui_interface->addVerticalSlider(label, zone, init, min, max, step);
}

static void addHorizontalSliderGlueFloat(void* cpp_interface, const char* label, float* zone, float init, float min, float max, float step)
{
    UIFloat* ui_interface = static_cast<UIFloat*>(cpp_interface);
    ui_interface->addHorizontalSlider(label, zone, init, min, max, step);
}

static void addNumEntryGlueFloat(void* cpp_interface, const char* label, float* zone, float init, float min, float max, float step)
{
    UIFloat* ui_interface = static_cast<UIFloat*>(cpp_interface);
    ui_interface->addNumEntry(label, zone, init, min, max, step);
}

static void addHorizontalBargraphGlueFloat(void* cpp_interface, const char* label, float* zone, float min, float max)
{
    UIFloat* ui_interface = static_cast<UIFloat*>(cpp_interface);
    ui_interface->addHorizontalBargraph(label, zone, min, max);
}

static void addVerticalBargraphGlueFloat(void* cpp_interface, const char* label, float* zone, float min, float max)
{
    UIFloat* ui_interface = static_cast<UIFloat*>(cpp_interface);
    ui_interface->addVerticalBargraph(label, zone, min, max);
}
    
static void addSoundfileGlueFloat(void* cpp_interface, const char* label, const char* url, Soundfile** sf_zone)
{
    UIFloat* ui_interface = static_cast<UIFloat*>(cpp_interface);
    ui_interface->addSoundfile(label, url, sf_zone);
}

static void declareGlueFloat(void* cpp_interface, float* zone, const char* key, const char* value)
{
    UIFloat* ui_interface = static_cast<UIFloat*>(cpp_interface);
    ui_interface->declare(zone, key, value);
}

class UIDouble
{

    public:

        UIDouble() {}

        virtual ~UIDouble() {}

        // -- widget's layouts

        virtual void openTabBox(const char* label) = 0;
        virtual void openHorizontalBox(const char* label) = 0;
        virtual void openVerticalBox(const char* label) = 0;
        virtual void closeBox() = 0;

        // -- active widgets

        virtual void addButton(const char* label, double* zone) = 0;
        virtual void addCheckButton(const char* label, double* zone) = 0;
        virtual void addVerticalSlider(const char* label, double* zone, double init, double min, double max, double step) = 0;
        virtual void addHorizontalSlider(const char* label, double* zone, double init, double min, double max, double step) = 0;
        virtual void addNumEntry(const char* label, double* zone, double init, double min, double max, double step) = 0;

        // -- passive widgets

        virtual void addHorizontalBargraph(const char* label, double* zone, double min, double max) = 0;
        virtual void addVerticalBargraph(const char* label, double* zone, double min, double max) = 0;
    
        // -- soundfiles
    
        virtual void addSoundfile(const char* label, const char* filename, Soundfile** sf_zone) = 0;

        // -- metadata declarations

        virtual void declare(double* zone, const char* key, const char* val) {}
};

static void openTabBoxGlueDouble(void* cpp_interface, const char* label)
{
    UIDouble* ui_interface = static_cast<UIDouble*>(cpp_interface);
    ui_interface->openTabBox(label);
}

static void openHorizontalBoxGlueDouble(void* cpp_interface, const char* label)
{
    UIDouble* ui_interface = static_cast<UIDouble*>(cpp_interface);
    ui_interface->openHorizontalBox(label);
}

static void openVerticalBoxGlueDouble(void* cpp_interface, const char* label)
{
    UIDouble* ui_interface = static_cast<UIDouble*>(cpp_interface);
    ui_interface->openVerticalBox(label);
}

static void closeBoxGlueDouble(void* cpp_interface)
{
    UIDouble* ui_interface = static_cast<UIDouble*>(cpp_interface);
    ui_interface->closeBox();
}

static void addButtonGlueDouble(void* cpp_interface, const char* label, double* zone)
{
    UIDouble* ui_interface = static_cast<UIDouble*>(cpp_interface);
    ui_interface->addButton(label, zone);
}

static void addCheckButtonGlueDouble(void* cpp_interface, const char* label, double* zone)
{
    UIDouble* ui_interface = static_cast<UIDouble*>(cpp_interface);
    ui_interface->addCheckButton(label, zone);
}

static void addVerticalSliderGlueDouble(void* cpp_interface, const char* label, double* zone, double init, double min, double max, double step)
{
    UIDouble* ui_interface = static_cast<UIDouble*>(cpp_interface);
    ui_interface->addVerticalSlider(label, zone, init, min, max, step);
}

static void addHorizontalSliderGlueDouble(void* cpp_interface, const char* label, double* zone, double init, double min, double max, double step)
{
    UIDouble* ui_interface = static_cast<UIDouble*>(cpp_interface);
    ui_interface->addHorizontalSlider(label, zone, init, min, max, step);
}

static void addNumEntryGlueDouble(void* cpp_interface, const char* label, double* zone, double init, double min, double max, double step)
{
    UIDouble* ui_interface = static_cast<UIDouble*>(cpp_interface);
    ui_interface->addNumEntry(label, zone, init, min, max, step);
}

static void addHorizontalBargraphGlueDouble(void* cpp_interface, const char* label, double* zone, double min, double max)
{
    UIDouble* ui_interface = static_cast<UIDouble*>(cpp_interface);
    ui_interface->addHorizontalBargraph(label, zone, min, max);
}

static void addVerticalBargraphGlueDouble(void* cpp_interface, const char* label, double* zone, double min, double max)
{
    UIDouble* ui_interface = static_cast<UIDouble*>(cpp_interface);
    ui_interface->addVerticalBargraph(label, zone, min, max);
}
    
static void addSoundfileGlueDouble(void* cpp_interface, const char* label, const char* url, Soundfile** sf_zone)
{
    UIDouble* ui_interface = static_cast<UIDouble*>(cpp_interface);
    ui_interface->addSoundfile(label, url, sf_zone);
}

static void declareGlueDouble(void* cpp_interface, double* zone, const char* key, const char* value)
{
    UIDouble* ui_interface = static_cast<UIDouble*>(cpp_interface);
    ui_interface->declare(zone, key, value);
}

static void buildUIGlue(UIGlue* glue, UI* ui_interface, bool is_double)
{
    glue->uiInterface = ui_interface;
    
    if (is_double) {
        glue->openTabBox = reinterpret_cast<openTabBoxFun>(openTabBoxGlueDouble);
        glue->openHorizontalBox = reinterpret_cast<openHorizontalBoxFun>(openHorizontalBoxGlueDouble);
        glue->openVerticalBox = reinterpret_cast<openVerticalBoxFun>(openVerticalBoxGlueDouble);
        glue->closeBox = reinterpret_cast<closeBoxFun>(closeBoxGlueDouble);
        glue->addButton = reinterpret_cast<addButtonFun>(addButtonGlueDouble);
        glue->addCheckButton = reinterpret_cast<addCheckButtonFun>(addCheckButtonGlueDouble);
        glue->addVerticalSlider = reinterpret_cast<addVerticalSliderFun>(addVerticalSliderGlueDouble);
        glue->addHorizontalSlider = reinterpret_cast<addHorizontalSliderFun>(addHorizontalSliderGlueDouble);
        glue->addNumEntry = reinterpret_cast<addNumEntryFun>(addNumEntryGlueDouble);
        glue->addHorizontalBargraph = reinterpret_cast<addHorizontalBargraphFun>(addHorizontalBargraphGlueDouble);
        glue->addVerticalBargraph = reinterpret_cast<addVerticalBargraphFun>(addVerticalBargraphGlueDouble);
        glue->addSoundfile = reinterpret_cast<addSoundfileFun>(addSoundfileGlueDouble);
        glue->declare = reinterpret_cast<declareFun>(declareGlueDouble);
    } else {
        glue->openTabBox = reinterpret_cast<openTabBoxFun>(openTabBoxGlueFloat);
        glue->openHorizontalBox = reinterpret_cast<openHorizontalBoxFun>(openHorizontalBoxGlueFloat);
        glue->openVerticalBox = reinterpret_cast<openVerticalBoxFun>(openVerticalBoxGlueFloat);
        glue->closeBox = reinterpret_cast<closeBoxFun>(closeBoxGlueFloat);
        glue->addButton = reinterpret_cast<addButtonFun>(addButtonGlueFloat);
        glue->addCheckButton = reinterpret_cast<addCheckButtonFun>(addCheckButtonGlueFloat);
        glue->addVerticalSlider = reinterpret_cast<addVerticalSliderFun>(addVerticalSliderGlueFloat);
        glue->addHorizontalSlider = reinterpret_cast<addHorizontalSliderFun>(addHorizontalSliderGlueFloat);
        glue->addNumEntry = reinterpret_cast<addNumEntryFun>(addNumEntryGlueFloat);
        glue->addHorizontalBargraph = reinterpret_cast<addHorizontalBargraphFun>(addHorizontalBargraphGlueFloat);
        glue->addVerticalBargraph = reinterpret_cast<addVerticalBargraphFun>(addVerticalBargraphGlueFloat);
        glue->addSoundfile = reinterpret_cast<addSoundfileFun>(addSoundfileGlueFloat);
        glue->declare = reinterpret_cast<declareFun>(declareGlueFloat);
    }
}
    
// Base class
    
struct UIInterface
{
    virtual ~UIInterface() {}
    
    virtual int sizeOfFAUSTFLOAT() = 0;
    
    // -- widget's layouts
    
    virtual void openTabBox(const char* label) = 0;
    virtual void openHorizontalBox(const char* label) = 0;
    virtual void openVerticalBox(const char* label) = 0;
    virtual void closeBox() = 0;
    
    // float version
    
    // -- active widgets
    
    virtual void addButton(const char* label, float* zone) = 0;
    virtual void addCheckButton(const char* label, float* zone) = 0;
    
    virtual void addVerticalSlider(const char* label, float* zone, float init, float min, float max, float step) = 0;
    virtual void addHorizontalSlider(const char* label, float* zone, float init, float min, float max, float step) = 0;
    virtual void addNumEntry(const char* label, float* zone, float init, float min, float max, float step) = 0;
    
    // -- passive widgets
    
    virtual void addHorizontalBargraph(const char* label, float* zone, float min, float max) = 0;
    virtual void addVerticalBargraph(const char* label, float* zone, float min, float max) = 0;
    
    // -- metadata declarations
    
    virtual void declare(float* zone, const char* key, const char* val) = 0;
    
    // double version
    
    virtual void addButton(const char* label, double* zone) = 0;
    virtual void addCheckButton(const char* label, double* zone) = 0;
  
    virtual void addVerticalSlider(const char* label, double* zone, double init, double min, double max, double step) = 0;
    virtual void addHorizontalSlider(const char* label, double* zone, double init, double min, double max, double step) = 0;
    
    virtual void addNumEntry(const char* label, double* zone, double init, double min, double max, double step) = 0;
    
    // -- soundfiles
    
    virtual void addSoundfile(const char* label, const char* url, Soundfile** sf_zone) = 0;
    
    // -- passive widgets
    
    virtual void addHorizontalBargraph(const char* label, double* zone, double min, double max) = 0;
    virtual void addVerticalBargraph(const char* label, double* zone, double min, double max) = 0;
     
    // -- metadata declarations
    
    virtual void declare(double* zone, const char* key, const char* val) = 0;
    
};
    
struct UITemplate : public UIInterface
{
 
    void* fCPPInterface;

    UITemplate(void* cpp_interface):fCPPInterface(cpp_interface)
    {}
    virtual ~UITemplate() {}
    
    int sizeOfFAUSTFLOAT()
    {
        return reinterpret_cast<UI*>(fCPPInterface)->sizeOfFAUSTFLOAT();
    }
    
    // -- widget's layouts
    
    void openTabBox(const char* label)
    {
        openTabBoxGlueFloat(fCPPInterface, label);
    }
    void openHorizontalBox(const char* label)
    {
        openHorizontalBoxGlueFloat(fCPPInterface, label);
    }
    void openVerticalBox(const char* label)
    {
        openVerticalBoxGlueFloat(fCPPInterface, label);
    }
    void closeBox()
    {
        closeBoxGlueFloat(fCPPInterface);
    }
    
    // float version
    
    // -- active widgets
    
    void addButton(const char* label, float* zone)
    {
        addButtonGlueFloat(fCPPInterface, label, zone);
    }
    void addCheckButton(const char* label, float* zone)
    {
        addCheckButtonGlueFloat(fCPPInterface, label, zone);
    }
    
    void addVerticalSlider(const char* label, float* zone, float init, float min, float max, float step)
    {
        addVerticalSliderGlueFloat(fCPPInterface, label, zone, init, min, max, step);
    }
    
    void addHorizontalSlider(const char* label, float* zone, float init, float min, float max, float step)
    {
        addHorizontalSliderGlueFloat(fCPPInterface, label, zone, init, min, max, step);
    }
    
    void addNumEntry(const char* label, float* zone, float init, float min, float max, float step)
    {
        addNumEntryGlueFloat(fCPPInterface, label, zone, init, min, max, step);
    }
    
    // -- passive widgets
    
    void addHorizontalBargraph(const char* label, float* zone, float min, float max)
    {
        addHorizontalBargraphGlueFloat(fCPPInterface, label, zone, min, max);
    }
    
    void addVerticalBargraph(const char* label, float* zone, float min, float max)
    {
        addVerticalBargraphGlueFloat(fCPPInterface, label, zone, min, max);
    }

    // -- metadata declarations
    
    void declare(float* zone, const char* key, const char* val)
    {
        declareGlueFloat(fCPPInterface, zone, key, val);
    }
    
    // double version
    
    void addButton(const char* label, double* zone)
    {
        addButtonGlueDouble(fCPPInterface, label, zone);
    }
    void addCheckButton(const char* label, double* zone)
    {
        addCheckButtonGlueDouble(fCPPInterface, label, zone);
    }
    
    void addVerticalSlider(const char* label, double* zone, double init, double min, double max, double step)
    {
        addVerticalSliderGlueDouble(fCPPInterface, label, zone, init, min, max, step);
    }
    
    void addHorizontalSlider(const char* label, double* zone, double init, double min, double max, double step)
    {
        addHorizontalSliderGlueDouble(fCPPInterface, label, zone, init, min, max, step);
    }
    
    void addNumEntry(const char* label, double* zone, double init, double min, double max, double step)
    {
        addNumEntryGlueDouble(fCPPInterface, label, zone, init, min, max, step);
    }

    // -- soundfiles
    
    void addSoundfile(const char* label, const char* url, Soundfile** sf_zone)
    {
        addSoundfileGlueFloat(fCPPInterface, label, url, sf_zone);
    }

    // -- passive widgets
    
    void addHorizontalBargraph(const char* label, double* zone, double min, double max)
    {
        addHorizontalBargraphGlueDouble(fCPPInterface, label, zone, min, max);
    }
    
    void addVerticalBargraph(const char* label, double* zone, double min, double max)
    {
        addVerticalBargraphGlueDouble(fCPPInterface, label, zone, min, max);
    }

    // -- metadata declarations
    
    void declare(double* zone, const char* key, const char* val)
    {
        declareGlueDouble(fCPPInterface, zone, key, val);
    }

};
    
struct UIGlueTemplate : public UIInterface
{
    
    UIGlue* fGlue;
    
    UIGlueTemplate(UIGlue* glue):fGlue(glue)
    {}
    virtual ~UIGlueTemplate() {}
    
    virtual int sizeOfFAUSTFLOAT() { return sizeof(FAUSTFLOAT); }
    
    // -- widget's layouts
    
    void openTabBox(const char* label)
    {
        fGlue->openTabBox(fGlue->uiInterface, label);
    }
    void openHorizontalBox(const char* label)
    {
        fGlue->openHorizontalBox(fGlue->uiInterface, label);
    }
    void openVerticalBox(const char* label)
    {
        fGlue->openVerticalBox(fGlue->uiInterface, label);
    }
    void closeBox()
    {
        fGlue->closeBox(fGlue->uiInterface);
    }

    // float version
    
    // -- active widgets
    
    void addButton(const char* label, float* zone)
    {
        fGlue->addButton(fGlue->uiInterface, label, reinterpret_cast<FAUSTFLOAT*>(zone));
    }
    void addCheckButton(const char* label, float* zone)
    {
        fGlue->addCheckButton(fGlue->uiInterface, label, reinterpret_cast<FAUSTFLOAT*>(zone));
    }
    
    void addVerticalSlider(const char* label, float* zone, float init, float min, float max, float step)
    {
        fGlue->addVerticalSlider(fGlue->uiInterface, label, reinterpret_cast<FAUSTFLOAT*>(zone), init, min, max, step);
    }
    void addHorizontalSlider(const char* label, float* zone, float init, float min, float max, float step)
    {
        fGlue->addHorizontalSlider(fGlue->uiInterface, label, reinterpret_cast<FAUSTFLOAT*>(zone), init, min, max, step);
    }
    void addNumEntry(const char* label, float* zone, float init, float min, float max, float step)
    {
        fGlue->addNumEntry(fGlue->uiInterface, label, reinterpret_cast<FAUSTFLOAT*>(zone), init, min, max, step);
    }
    
    // -- passive widgets
    
    void addHorizontalBargraph(const char* label, float* zone, float min, float max)
    {
        fGlue->addHorizontalBargraph(fGlue->uiInterface, label, reinterpret_cast<FAUSTFLOAT*>(zone), min, max);
    }
    void addVerticalBargraph(const char* label, float* zone, float min, float max)
    {
        fGlue->addVerticalBargraph(fGlue->uiInterface, label, reinterpret_cast<FAUSTFLOAT*>(zone), min, max);
    }
    
    // -- metadata declarations
    
    void declare(float* zone, const char* key, const char* val)
    {
        fGlue->declare(fGlue->uiInterface, reinterpret_cast<FAUSTFLOAT*>(zone), key, val);
    }
    
    // double version
    
    void addButton(const char* label, double* zone)
    {
        fGlue->addButton(fGlue->uiInterface, label, reinterpret_cast<FAUSTFLOAT*>(zone));
    }
    void addCheckButton(const char* label, double* zone)
    {
        fGlue->addCheckButton(fGlue->uiInterface, label, reinterpret_cast<FAUSTFLOAT*>(zone));
    }
    
    void addVerticalSlider(const char* label, double* zone, double init, double min, double max, double step)
    {
        fGlue->addVerticalSlider(fGlue->uiInterface, label, reinterpret_cast<FAUSTFLOAT*>(zone), init, min, max, step);
    }
    void addHorizontalSlider(const char* label, double* zone, double init, double min, double max, double step)
    {
        fGlue->addHorizontalSlider(fGlue->uiInterface, label, reinterpret_cast<FAUSTFLOAT*>(zone), init, min, max, step);
    }
    void addNumEntry(const char* label, double* zone, double init, double min, double max, double step)
    {
        fGlue->addNumEntry(fGlue->uiInterface, label, reinterpret_cast<FAUSTFLOAT*>(zone), init, min, max, step);
    }
    // -- soundfiles
    
    void addSoundfile(const char* label, const char* url, Soundfile** sf_zone) {}
    
    // -- passive widgets
    
    void addHorizontalBargraph(const char* label, double* zone, double min, double max)
    {
        fGlue->addHorizontalBargraph(fGlue->uiInterface, label, reinterpret_cast<FAUSTFLOAT*>(zone), min, max);
    }
    void addVerticalBargraph(const char* label, double* zone, double min, double max)
    {
        fGlue->addVerticalBargraph(fGlue->uiInterface, label, reinterpret_cast<FAUSTFLOAT*>(zone), min, max);
    }
    
    // -- metadata declarations
    
    void declare(double* zone, const char* key, const char* val)
    {
        fGlue->declare(fGlue->uiInterface, reinterpret_cast<FAUSTFLOAT*>(zone), key, val);
    }
    
};

/*******************************************************************************
 * Meta glue code
 ******************************************************************************/

static void declareMetaGlue(void* cpp_interface, const char* key, const char* value)
{
    Meta* meta_interface = static_cast<Meta*>(cpp_interface);
    meta_interface->declare(key, value);
}

static void buildMetaGlue(MetaGlue* glue, Meta* meta)
{
    glue->metaInterface = meta;
    glue->declare = declareMetaGlue;
}
    
/*******************************************************************************
 * Memory manager glue code
 ******************************************************************************/

static void* allocateMemoryManagerGlue(void* cpp_interface, size_t size)
{
    dsp_memory_manager* manager_interface = static_cast<dsp_memory_manager*>(cpp_interface);
    return manager_interface->allocate(size);
}
    
static void destroyMemoryManagerGlue(void* cpp_interface, void* ptr)
{
    dsp_memory_manager* manager_interface = static_cast<dsp_memory_manager*>(cpp_interface);
    manager_interface->destroy(ptr);
}

static void buildManagerGlue(MemoryManagerGlue* glue, dsp_memory_manager* manager)
{
    glue->managerInterface = manager;
    glue->allocate = allocateMemoryManagerGlue;
    glue->destroy = destroyMemoryManagerGlue;
}

#ifdef __cplusplus
}
#endif

#endif
/**************************  END  CGlue.h **************************/

#ifdef _WIN32
#include <windows.h>
#define snprintf _snprintf
#define STRDUP _strdup
#else
#define STRDUP strdup
#endif

//------------------------------------------------------------------------------------------
//  Decode a dsp JSON description and implement 'buildUserInterface' and 'metadata' methods
//------------------------------------------------------------------------------------------

#define REAL_UI(ui_interface) reinterpret_cast<UIReal<REAL>*>(ui_interface)
#define REAL_ADR(index)      reinterpret_cast<REAL*>(&memory_block[index])
#define REAL_EXT_ADR(index)  reinterpret_cast<FAUSTFLOAT*>(&memory_block[index])
#define SOUNDFILE_ADR(index) reinterpret_cast<Soundfile**>(&memory_block[index])

typedef std::function<void(FAUSTFLOAT)> ReflectFunction;
typedef std::function<FAUSTFLOAT()> ModifyFunction;

struct FAUST_API ExtZoneParam {

    virtual void reflectZone() = 0;
    virtual void modifyZone() = 0;
    
    virtual void setReflectZoneFun(ReflectFunction reflect) = 0;
    virtual void setModifyZoneFun(ModifyFunction modify) = 0;
    
    virtual ~ExtZoneParam()
    {}
    
};

// Templated decoder

struct FAUST_API JSONUIDecoderBase
{
    virtual ~JSONUIDecoderBase()
    {}
    
    virtual void metadata(Meta* m) = 0;
    virtual void metadata(MetaGlue* glue) = 0;
    virtual int getDSPSize() = 0;
    virtual std::string getName() = 0;
    virtual std::string getLibVersion() = 0;
    virtual std::string getCompileOptions() = 0;
    virtual std::vector<std::string> getLibraryList() = 0;
    virtual std::vector<std::string> getIncludePathnames() = 0;
    virtual int getNumInputs() = 0;
    virtual int getNumOutputs() = 0;
    virtual int getSampleRate(char* memory_block) = 0;
    virtual void setReflectZoneFun(int index, ReflectFunction fun) = 0;
    virtual void setModifyZoneFun(int index, ModifyFunction fun) = 0;
    virtual void setupDSPProxy(UI* ui_interface, char* memory_block) = 0;
    virtual bool hasDSPProxy() = 0;
    virtual std::vector<ExtZoneParam*>& getInputControls() = 0;
    virtual std::vector<ExtZoneParam*>& getOutputControls() = 0;
    virtual void resetUserInterface() = 0;
    virtual void resetUserInterface(char* memory_block, Soundfile* defaultsound = nullptr) = 0;
    virtual void buildUserInterface(UI* ui_interface) = 0;
    virtual void buildUserInterface(UI* ui_interface, char* memory_block) = 0;
    virtual void buildUserInterface(UIGlue* ui_interface, char* memory_block) = 0;
    virtual bool hasCompileOption(const std::string& option) = 0;
    virtual std::string getCompileOption(const std::string& option) = 0;
};

template <typename REAL>
struct FAUST_API JSONUIDecoderReal : public JSONUIDecoderBase {
    
    struct ZoneParam : public ExtZoneParam {
        
        FAUSTFLOAT fZone;
        ReflectFunction fReflect;
        ModifyFunction fModify;
        
    #if defined(TARGET_OS_IPHONE) || defined(WIN32)
        ZoneParam(ReflectFunction reflect = nullptr, ModifyFunction modify = nullptr)
        :fReflect(reflect), fModify(modify)
        {}
        void reflectZone() { if (fReflect) fReflect(fZone); }
        void modifyZone() { if (fModify) fZone = fModify(); }
    #else
        ZoneParam(ReflectFunction reflect = [](FAUSTFLOAT value) {}, ModifyFunction modify = []() { return FAUSTFLOAT(-1); })
        :fReflect(reflect), fModify(modify)
        {}
        void reflectZone() { fReflect(fZone); }
        void modifyZone() { fZone = fModify(); }
    #endif
        
        void setReflectZoneFun(ReflectFunction reflect) { fReflect = reflect; }
        void setModifyZoneFun(ModifyFunction modify) { fModify = modify; }
        
    };
    
    typedef std::vector<ExtZoneParam*> controlMap;
  
    std::string fName;
    std::string fFileName;
    std::string fJSON;
    std::string fVersion;
    std::string fCompileOptions;
    
    std::map<std::string, std::string> fMetadata;
    std::vector<itemInfo> fUiItems;
    
    std::vector<std::string> fLibraryList;
    std::vector<std::string> fIncludePathnames;
    
    int fNumInputs, fNumOutputs, fSRIndex;
    int fDSPSize;
    bool fDSPProxy;
    
    controlMap fPathInputTable;     // [path, ZoneParam]
    controlMap fPathOutputTable;    // [path, ZoneParam]
    
    bool startWith(const std::string& str, const std::string& prefix)
    {
        return (str.substr(0, prefix.size()) == prefix);
    }

    bool isInput(const std::string& type)
    {
        return (type == "vslider" || type == "hslider" || type == "nentry" || type == "button" || type == "checkbox");
    }
    bool isOutput(const std::string& type) { return (type == "hbargraph" || type == "vbargraph"); }
    bool isSoundfile(const std::string& type) { return (type == "soundfile"); }
    
    std::string getString(std::map<std::string, std::pair<std::string, double> >& map, const std::string& key)
    {
        return (map.find(key) != map.end()) ? map[key].first : "";
    }
    
    int getInt(std::map<std::string, std::pair<std::string, double> >& map, const std::string& key)
    {
        return (map.find(key) != map.end()) ? int(map[key].second) : -1;
    }
    
    void setReflectZoneFun(int index, ReflectFunction fun)
    {
        fPathInputTable[index]->setReflectZoneFun(fun);
    }
    
    void setModifyZoneFun(int index, ModifyFunction fun)
    {
        fPathOutputTable[index]->setModifyZoneFun(fun);
    }

    JSONUIDecoderReal(const std::string& json)
    {
        fJSON = json;
        const char* p = fJSON.c_str();
        std::map<std::string, std::pair<std::string, double> > meta_data1;
        std::map<std::string, std::vector<std::string> > meta_data2;
        parseJson(p, meta_data1, fMetadata, meta_data2, fUiItems);
        
        // meta_data1 contains <name : val>, <inputs : val>, <ouputs : val> pairs etc...
        fName = getString(meta_data1, "name");
        fFileName = getString(meta_data1, "filename");
        fVersion = getString(meta_data1, "version");
        fCompileOptions = getString(meta_data1, "compile_options");
        
        if (meta_data2.find("library_list") != meta_data2.end()) {
            fLibraryList = meta_data2["library_list"];
        } else {
            // 'library_list' is coded as successive 'library_pathN' metadata
            for (const auto& it : fMetadata) {
                if (startWith(it.first, "library_path")) {
                    fLibraryList.push_back(it.second);
                }
            }
        }
        if (meta_data2.find("include_pathnames") != meta_data2.end()) {
            fIncludePathnames = meta_data2["include_pathnames"];
        }
        
        fDSPSize = getInt(meta_data1, "size");
        fNumInputs = getInt(meta_data1, "inputs");
        fNumOutputs = getInt(meta_data1, "outputs");
        fSRIndex = getInt(meta_data1, "sr_index");
        fDSPProxy = false;
        
        // Prepare the fPathTable and init zone
        for (const auto& it : fUiItems) {
            std::string type = it.type;
            // Meta data declaration for input items
            if (isInput(type)) {
                ZoneParam* param = new ZoneParam();
                fPathInputTable.push_back(param);
                param->fZone = it.init;
            }
            // Meta data declaration for output items
            else if (isOutput(type)) {
                ZoneParam* param = new ZoneParam();
                fPathOutputTable.push_back(param);
                param->fZone = REAL(0);
            }
        }
    }
    
    virtual ~JSONUIDecoderReal()
    {
        for (const auto& it : fPathInputTable) {
            delete it;
        }
        for (const auto& it : fPathOutputTable) {
            delete it;
        }
    }
    
    void metadata(Meta* m)
    {
        for (const auto& it : fMetadata) {
            m->declare(it.first.c_str(), it.second.c_str());
        }
    }
    
    void metadata(MetaGlue* m)
    {
        for (const auto& it : fMetadata) {
            m->declare(m->metaInterface, it.first.c_str(), it.second.c_str());
        }
    }
    
    void resetUserInterface()
    {
        int item = 0;
        for (const auto& it : fUiItems) {
            if (isInput(it.type)) {
                static_cast<ZoneParam*>(fPathInputTable[item++])->fZone = it.init;
            }
        }
    }
    
    void resetUserInterface(char* memory_block, Soundfile* defaultsound = nullptr)
    {
        for (const auto& it : fUiItems) {
            int index = it.index;
            if (isInput(it.type)) {
                *REAL_ADR(index) = it.init;
            } else if (isSoundfile(it.type)) {
                if (*SOUNDFILE_ADR(index) == nullptr) {
                    *SOUNDFILE_ADR(index) = defaultsound;
                }
            }
        }
    }
    
    int getSampleRate(char* memory_block)
    {
        return *reinterpret_cast<int*>(&memory_block[fSRIndex]);
    }
    
    void setupDSPProxy(UI* ui_interface, char* memory_block)
    {
        if (!fDSPProxy) {
            fDSPProxy = true;
            int countIn = 0;
            int countOut = 0;
            for (const auto& it : fUiItems) {
                std::string type = it.type;
                int index = it.index;
                if (isInput(type)) {
                    fPathInputTable[countIn++]->setReflectZoneFun([=](FAUSTFLOAT value) { *REAL_ADR(index) = REAL(value); });
                } else if (isOutput(type)) {
                    fPathOutputTable[countOut++]->setModifyZoneFun([=]() { return FAUSTFLOAT(*REAL_ADR(index)); });
                }
            }
        }
        
        // Setup soundfile in any case
        for (const auto& it : fUiItems) {
            if (isSoundfile(it.type)) {
                ui_interface->addSoundfile(it.label.c_str(), it.url.c_str(), SOUNDFILE_ADR(it.index));
            }
        }
    }
    
    bool hasDSPProxy() { return fDSPProxy; }
  
    void buildUserInterface(UI* ui_interface)
    {
        // MANDATORY: to be sure floats or double are correctly parsed
        char* tmp_local = setlocale(LC_ALL, nullptr);
        if (tmp_local != NULL) {
            tmp_local = STRDUP(tmp_local);
        }
        setlocale(LC_ALL, "C");
        
        int countIn = 0;
        int countOut = 0;
        int countSound = 0;
        
        for (const auto& it : fUiItems) {
            
            std::string type = it.type;
            REAL init = REAL(it.init);
            REAL min = REAL(it.fmin);
            REAL max = REAL(it.fmax);
            REAL step = REAL(it.step);
            
            // Meta data declaration for input items
            if (isInput(type)) {
                for (size_t i = 0; i < it.meta.size(); i++) {
                    ui_interface->declare(&static_cast<ZoneParam*>(fPathInputTable[countIn])->fZone, it.meta[i].first.c_str(), it.meta[i].second.c_str());
                }
            }
            // Meta data declaration for output items
            else if (isOutput(type)) {
                for (size_t i = 0; i < it.meta.size(); i++) {
                    ui_interface->declare(&static_cast<ZoneParam*>(fPathOutputTable[countOut])->fZone, it.meta[i].first.c_str(), it.meta[i].second.c_str());
                }
            }
            // Meta data declaration for group opening or closing
            else {
                for (size_t i = 0; i < it.meta.size(); i++) {
                    ui_interface->declare(0, it.meta[i].first.c_str(), it.meta[i].second.c_str());
                }
            }
            
            if (type == "hgroup") {
                ui_interface->openHorizontalBox(it.label.c_str());
            } else if (type == "vgroup") {
                ui_interface->openVerticalBox(it.label.c_str());
            } else if (type == "tgroup") {
                ui_interface->openTabBox(it.label.c_str());
            } else if (type == "vslider") {
                ui_interface->addVerticalSlider(it.label.c_str(), &static_cast<ZoneParam*>(fPathInputTable[countIn])->fZone, init, min, max, step);
            } else if (type == "hslider") {
                ui_interface->addHorizontalSlider(it.label.c_str(), &static_cast<ZoneParam*>(fPathInputTable[countIn])->fZone, init, min, max, step);
            } else if (type == "checkbox") {
                ui_interface->addCheckButton(it.label.c_str(), &static_cast<ZoneParam*>(fPathInputTable[countIn])->fZone);
            } else if (type == "soundfile") {
                // Nothing
            } else if (type == "hbargraph") {
                ui_interface->addHorizontalBargraph(it.label.c_str(), &static_cast<ZoneParam*>(fPathOutputTable[countOut])->fZone, min, max);
            } else if (type == "vbargraph") {
                ui_interface->addVerticalBargraph(it.label.c_str(), &static_cast<ZoneParam*>(fPathOutputTable[countOut])->fZone, min, max);
            } else if (type == "nentry") {
                ui_interface->addNumEntry(it.label.c_str(), &static_cast<ZoneParam*>(fPathInputTable[countIn])->fZone, init, min, max, step);
            } else if (type == "button") {
                ui_interface->addButton(it.label.c_str(), &static_cast<ZoneParam*>(fPathInputTable[countIn])->fZone);
            } else if (type == "close") {
                ui_interface->closeBox();
            }
            
            if (isInput(type)) {
                countIn++;
            } else if (isOutput(type)) {
                countOut++;
            } else if (isSoundfile(type)) {
                countSound++;
            }
        }
        
        if (tmp_local != NULL) {
            setlocale(LC_ALL, tmp_local);
            free(tmp_local);
        }
    }
    
    void buildUserInterface(UI* ui_interface, char* memory_block)
    {
        // MANDATORY: to be sure floats or double are correctly parsed
        char* tmp_local = setlocale(LC_ALL, nullptr);
        if (tmp_local != NULL) {
            tmp_local = STRDUP(tmp_local);
        }
        setlocale(LC_ALL, "C");
        
        for (const auto& it : fUiItems) {
            
            std::string type = it.type;
            int index = it.index;
            REAL init = REAL(it.init);
            REAL min = REAL(it.fmin);
            REAL max = REAL(it.fmax);
            REAL step = REAL(it.step);
            
            // Meta data declaration for input items
            if (isInput(type)) {
                for (size_t i = 0; i < it.meta.size(); i++) {
                    REAL_UI(ui_interface)->declare(REAL_ADR(index), it.meta[i].first.c_str(), it.meta[i].second.c_str());
                }
            }
            // Meta data declaration for output items
            else if (isOutput(type)) {
                for (size_t i = 0; i < it.meta.size(); i++) {
                    REAL_UI(ui_interface)->declare(REAL_ADR(index), it.meta[i].first.c_str(), it.meta[i].second.c_str());
                }
            }
            // Meta data declaration for group opening or closing
            else {
                for (size_t i = 0; i < it.meta.size(); i++) {
                    REAL_UI(ui_interface)->declare(0, it.meta[i].first.c_str(), it.meta[i].second.c_str());
                }
            }
            
            if (type == "hgroup") {
                REAL_UI(ui_interface)->openHorizontalBox(it.label.c_str());
            } else if (type == "vgroup") {
                REAL_UI(ui_interface)->openVerticalBox(it.label.c_str());
            } else if (type == "tgroup") {
                REAL_UI(ui_interface)->openTabBox(it.label.c_str());
            } else if (type == "vslider") {
                REAL_UI(ui_interface)->addVerticalSlider(it.label.c_str(), REAL_ADR(index), init, min, max, step);
            } else if (type == "hslider") {
                REAL_UI(ui_interface)->addHorizontalSlider(it.label.c_str(), REAL_ADR(index), init, min, max, step);
            } else if (type == "checkbox") {
                REAL_UI(ui_interface)->addCheckButton(it.label.c_str(), REAL_ADR(index));
            } else if (type == "soundfile") {
                REAL_UI(ui_interface)->addSoundfile(it.label.c_str(), it.url.c_str(), SOUNDFILE_ADR(index));
            } else if (type == "hbargraph") {
                REAL_UI(ui_interface)->addHorizontalBargraph(it.label.c_str(), REAL_ADR(index), min, max);
            } else if (type == "vbargraph") {
                REAL_UI(ui_interface)->addVerticalBargraph(it.label.c_str(), REAL_ADR(index), min, max);
            } else if (type == "nentry") {
                REAL_UI(ui_interface)->addNumEntry(it.label.c_str(), REAL_ADR(index), init, min, max, step);
            } else if (type == "button") {
                REAL_UI(ui_interface)->addButton(it.label.c_str(), REAL_ADR(index));
            } else if (type == "close") {
                REAL_UI(ui_interface)->closeBox();
            }
        }
        
        if (tmp_local != NULL) {
            setlocale(LC_ALL, tmp_local);
            free(tmp_local);
        }
    }
    
    void buildUserInterface(UIGlue* ui_interface, char* memory_block)
    {
        // MANDATORY: to be sure floats or double are correctly parsed
        char* tmp_local = setlocale(LC_ALL, nullptr);
        if (tmp_local != NULL) {
            tmp_local = STRDUP(tmp_local);
        }
        setlocale(LC_ALL, "C");
        
        for (const auto& it : fUiItems) {
            
            std::string type = it.type;
            int index = it.index;
            REAL init = REAL(it.init);
            REAL min = REAL(it.fmin);
            REAL max = REAL(it.fmax);
            REAL step = REAL(it.step);
            
            // Meta data declaration for input items
            if (isInput(type)) {
                for (size_t i = 0; i < it.meta.size(); i++) {
                    ui_interface->declare(ui_interface->uiInterface, REAL_EXT_ADR(index), it.meta[i].first.c_str(), it.meta[i].second.c_str());
                }
            }
            // Meta data declaration for output items
            else if (isOutput(type)) {
                for (size_t i = 0; i < it.meta.size(); i++) {
                    ui_interface->declare(ui_interface->uiInterface, REAL_EXT_ADR(index), it.meta[i].first.c_str(), it.meta[i].second.c_str());
                }
            }
            // Meta data declaration for group opening or closing
            else {
                for (size_t i = 0; i < it.meta.size(); i++) {
                    ui_interface->declare(ui_interface->uiInterface, 0, it.meta[i].first.c_str(), it.meta[i].second.c_str());
                }
            }
            
            if (type == "hgroup") {
                ui_interface->openHorizontalBox(ui_interface->uiInterface, it.label.c_str());
            } else if (type == "vgroup") {
                ui_interface->openVerticalBox(ui_interface->uiInterface, it.label.c_str());
            } else if (type == "tgroup") {
                ui_interface->openTabBox(ui_interface->uiInterface, it.label.c_str());
            } else if (type == "vslider") {
                ui_interface->addVerticalSlider(ui_interface->uiInterface, it.label.c_str(), REAL_EXT_ADR(index), init, min, max, step);
            } else if (type == "hslider") {
                ui_interface->addHorizontalSlider(ui_interface->uiInterface, it.label.c_str(), REAL_EXT_ADR(index), init, min, max, step);
            } else if (type == "checkbox") {
                ui_interface->addCheckButton(ui_interface->uiInterface, it.label.c_str(), REAL_EXT_ADR(index));
            } else if (type == "soundfile") {
                ui_interface->addSoundfile(ui_interface->uiInterface, it.label.c_str(), it.url.c_str(), SOUNDFILE_ADR(index));
            } else if (type == "hbargraph") {
                ui_interface->addHorizontalBargraph(ui_interface->uiInterface, it.label.c_str(), REAL_EXT_ADR(index), min, max);
            } else if (type == "vbargraph") {
                ui_interface->addVerticalBargraph(ui_interface->uiInterface, it.label.c_str(), REAL_EXT_ADR(index), min, max);
            } else if (type == "nentry") {
                ui_interface->addNumEntry(ui_interface->uiInterface, it.label.c_str(), REAL_EXT_ADR(index), init, min, max, step);
            } else if (type == "button") {
                ui_interface->addButton(ui_interface->uiInterface, it.label.c_str(), REAL_EXT_ADR(index));
            } else if (type == "close") {
                ui_interface->closeBox(ui_interface->uiInterface);
            }
        }
        
        if (tmp_local != NULL) {
            setlocale(LC_ALL, tmp_local);
            free(tmp_local);
        }
    }
    
    bool hasCompileOption(const std::string& option)
    {
        std::istringstream iss(fCompileOptions);
        std::string token;
        while (std::getline(iss, token, ' ')) {
            if (token == option) return true;
        }
        return false;
    }
    
    std::string getCompileOption(const std::string& option)
    {
        std::istringstream iss(fCompileOptions);
        std::string token;
        while (std::getline(iss, token, ' ')) {
            if (token == option) {
                std::string res;
                iss >> res;
                return res;
            }
        }
        return "";
    }
    
    int getDSPSize() { return fDSPSize; }
    std::string getName() { return fName; }
    std::string getLibVersion() { return fVersion; }
    std::string getCompileOptions() { return fCompileOptions; }
    std::vector<std::string> getLibraryList() { return fLibraryList; }
    std::vector<std::string> getIncludePathnames() { return fIncludePathnames; }
    int getNumInputs() { return fNumInputs; }
    int getNumOutputs() { return fNumOutputs; }
    
    std::vector<ExtZoneParam*>& getInputControls()
    {
        return fPathInputTable;
    }
    std::vector<ExtZoneParam*>& getOutputControls()
    {
        return fPathOutputTable;
    }
    
};

// FAUSTFLOAT templated decoder

struct FAUST_API JSONUIDecoder : public JSONUIDecoderReal<FAUSTFLOAT>
{
    JSONUIDecoder(const std::string& json):JSONUIDecoderReal<FAUSTFLOAT>(json)
    {}
};

// Generic factory

static JSONUIDecoderBase* createJSONUIDecoder(const std::string& json)
{
    JSONUIDecoder decoder(json);
    if (decoder.hasCompileOption("-double")) {
        return new JSONUIDecoderReal<double>(json);
    } else {
        return new JSONUIDecoderReal<float>(json);
    }
}

#endif
/**************************  END  JSONUIDecoder.h **************************/

/**
 * Proxy dsp definition created from the DSP JSON description.
 * This class allows a 'proxy' dsp to control a real dsp
 * possibly running somewhere else.
 */
class proxy_dsp : public dsp {

    protected:
    
        JSONUIDecoder* fDecoder;
        int fSampleRate;
    
        void init(const std::string& json)
        {
            fDecoder = new JSONUIDecoder(json);
            fSampleRate = -1;
        }
        
    public:
    
        proxy_dsp():fDecoder(nullptr), fSampleRate(-1)
        {}
    
        proxy_dsp(const std::string& json)
        {
            init(json);
        }
          
        proxy_dsp(dsp* dsp)
        {
            JSONUI builder(dsp->getNumInputs(), dsp->getNumOutputs());
            dsp->metadata(&builder);
            dsp->buildUserInterface(&builder);
            fSampleRate = dsp->getSampleRate();
            fDecoder = new JSONUIDecoder(builder.JSON());
        }
      
        virtual ~proxy_dsp()
        {
            delete fDecoder;
        }
    
        virtual int getNumInputs() { return fDecoder->fNumInputs; }
        virtual int getNumOutputs() { return fDecoder->fNumOutputs; }
        
        virtual void buildUserInterface(UI* ui) { fDecoder->buildUserInterface(ui); }
        
        // To possibly implement in a concrete proxy dsp 
        virtual void init(int sample_rate)
        {
            instanceInit(sample_rate);
        }
        virtual void instanceInit(int sample_rate)
        {
            instanceConstants(sample_rate);
            instanceResetUserInterface();
            instanceClear();
        }
        virtual void instanceConstants(int sample_rate) { fSampleRate = sample_rate; }
        virtual void instanceResetUserInterface() { fDecoder->resetUserInterface(); }
        virtual void instanceClear() {}
    
        virtual int getSampleRate() { return fSampleRate; }
    
        virtual proxy_dsp* clone() { return new proxy_dsp(fDecoder->fJSON); }
        virtual void metadata(Meta* m) { fDecoder->metadata(m); }
    
        virtual void compute(int count, FAUSTFLOAT** inputs, FAUSTFLOAT** outputs) {}
        virtual void compute(double date_usec, int count, FAUSTFLOAT** inputs, FAUSTFLOAT** outputs) {} 
        
};

#endif
/************************** END proxy-dsp.h **************************/

/************************** BEGIN JSONControl.h **************************
 FAUST Architecture File
 Copyright (C) 2003-2022 GRAME, Centre National de Creation Musicale
 ---------------------------------------------------------------------
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation; either version 2.1 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 
 EXCEPTION : As a special exception, you may create a larger work
 that contains this FAUST architecture section and distribute
 that work under terms of your choice, so long as this FAUST
 architecture section is not modified.
 *************************************************************************/

#ifndef __JSON_CONTROL__
#define __JSON_CONTROL__

#include <string>

#ifndef FAUSTFLOAT
#define FAUSTFLOAT float
#endif

struct FAUST_API JSONControl {
    
    virtual std::string getJSON() { return ""; }

    virtual void setParamValue(const std::string& path, FAUSTFLOAT value) {}

    virtual FAUSTFLOAT getParamValue(const std::string& path) { return 0; }
    
    virtual ~JSONControl()
    {}
    
};

#endif
/**************************  END  JSONControl.h **************************/

#define kActiveVoice    0
#define kFreeVoice     -1
#define kReleaseVoice  -2
#define kLegatoVoice   -3
#define kNoVoice       -4

#define VOICE_STOP_LEVEL  0.0005    // -70 db
#define MIX_BUFFER_SIZE   4096

/**
 * Allows to control zones in a grouped manner.
 */
class GroupUI : public GUI, public PathBuilder {

    private:

        // Map to associate labels with UI group items
        std::map<std::string, uiGroupItem*> fLabelZoneMap;

        // Insert a zone into the map based on the label folloing the freq/gain/gate polyphonic convention
        void insertMap(std::string label, FAUSTFLOAT* zone)
        {
            if (!MapUI::endsWith(label, "/gate")
                && !MapUI::endsWith(label, "/freq")
                && !MapUI::endsWith(label, "/key")
                && !MapUI::endsWith(label, "/gain")
                && !MapUI::endsWith(label, "/vel")
                && !MapUI::endsWith(label, "/velocity")) {

                // Groups all controllers except 'freq/key', 'gate', and 'gain/vel|velocity'
                if (fLabelZoneMap.find(label) != fLabelZoneMap.end()) {
                    fLabelZoneMap[label]->addZone(zone);
                } else {
                    fLabelZoneMap[label] = new uiGroupItem(this, zone);
                }
            }
        }

        uiCallbackItem* fPanic;

    public:

        GroupUI(FAUSTFLOAT* zone, uiCallback cb, void* arg)
        {
            fPanic = new uiCallbackItem(this, zone, cb, arg);
        }
    
        virtual ~GroupUI()
        {
            // 'fPanic' is kept and deleted in GUI, so do not delete here
        }

        // -- widget's layouts
        void openTabBox(const char* label)
        {
            pushLabel(label);
        }
        void openHorizontalBox(const char* label)
        {
            pushLabel(label);
        }
        void openVerticalBox(const char* label)
        {
            pushLabel(label);
        }
        void closeBox()
        {
            popLabel();
        }

        // -- active widgets
        void addButton(const char* label, FAUSTFLOAT* zone)
        {
            insertMap(buildPath(label), zone);
        }
        void addCheckButton(const char* label, FAUSTFLOAT* zone)
        {
            insertMap(buildPath(label), zone);
        }
        void addVerticalSlider(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT fmin, FAUSTFLOAT fmax, FAUSTFLOAT step)
        {
            insertMap(buildPath(label), zone);
        }
        void addHorizontalSlider(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT fmin, FAUSTFLOAT fmax, FAUSTFLOAT step)
        {
            insertMap(buildPath(label), zone);
        }
        void addNumEntry(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT fmin, FAUSTFLOAT fmax, FAUSTFLOAT step)
        {
            insertMap(buildPath(label), zone);
        }

        // -- passive widgets
        void addHorizontalBargraph(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT fmin, FAUSTFLOAT fmax)
        {
            insertMap(buildPath(label), zone);
        }
        void addVerticalBargraph(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT fmin, FAUSTFLOAT fmax)
        {
            insertMap(buildPath(label), zone);
        }

};

/**
 * One voice of polyphony.
 */
struct dsp_voice : public MapUI, public decorator_dsp {
    
    typedef std::function<double(int)> TransformFunction;
  
    // Convert MIDI note to frequency
    static double midiToFreq(double note)
    {
        return 440.0 * std::pow(2.0, (note-69.0)/12.0);
    }
    
    // Voice state and properties
    int fCurNote;                       // Current playing note pitch
    int fNextNote;                      // In kLegatoVoice state, next note to play
    int fNextVel;                       // In kLegatoVoice state, next velocity to play
    int fDate;                          // KeyOn date
    int fRelease;                       // Current number of samples used in release mode to detect end of note
    FAUSTFLOAT fLevel;                  // Last audio block level
    double fReleaseLengthSec;           // Maximum release length in seconds (estimated time to silence after note release)
    std::vector<std::string> fGatePath; // Paths of 'gate' control
    std::vector<std::string> fGainPath; // Paths of 'gain/vel|velocity' control
    std::vector<std::string> fFreqPath; // Paths of 'freq/key' control
    TransformFunction        fKeyFun;   // MIDI key to freq conversion function
    TransformFunction        fVelFun;   // MIDI velocity to gain conversion function
    
    FAUSTFLOAT** fInputsSlice;
    FAUSTFLOAT** fOutputsSlice;
 
    dsp_voice(dsp* dsp):decorator_dsp(dsp)
    {
        // Default conversion functions
        fVelFun = [](int velocity) { return double(velocity)/127.0; };
        fKeyFun = [](int pitch) { return midiToFreq(pitch); };
        dsp->buildUserInterface(this);
        fCurNote = kFreeVoice;
        fNextNote = fNextVel = -1;
        fLevel = FAUSTFLOAT(0);
        fDate = fRelease = 0;
        fReleaseLengthSec = 0.5;  // A half second is a reasonable default maximum release length.
        extractPaths(fGatePath, fFreqPath, fGainPath);
    }
    virtual ~dsp_voice()
    {}
    
    // Compute a slice of audio
    void computeSlice(int offset, int slice, FAUSTFLOAT** inputs, FAUSTFLOAT** outputs)
    {
        FAUSTFLOAT** inputsSlice = static_cast<FAUSTFLOAT**>(alloca(sizeof(FAUSTFLOAT*) * getNumInputs()));
        for (int chan = 0; chan < getNumInputs(); chan++) {
            inputsSlice[chan] = &(inputs[chan][offset]);
        }
        FAUSTFLOAT** outputsSlice = static_cast<FAUSTFLOAT**>(alloca(sizeof(FAUSTFLOAT*) * getNumOutputs()));
        for (int chan = 0; chan < getNumOutputs(); chan++) {
            outputsSlice[chan] = &(outputs[chan][offset]);
        }
        compute(slice, inputsSlice, outputsSlice);
    }
    
    // Compute audio in legato mode
    void computeLegato(int count, FAUSTFLOAT** inputs, FAUSTFLOAT** outputs)
    {
        int slice = count/2;
        
        // Reset envelops
        for (size_t i = 0; i < fGatePath.size(); i++) {
            setParamValue(fGatePath[i], FAUSTFLOAT(0));
        }
        
        // Compute current voice on half buffer
        computeSlice(0, slice, inputs, outputs);
         
        // Start next keyOn
        keyOn(fNextNote, fNextVel);
        
        // Compute on second half buffer
        computeSlice(slice, slice, inputs, outputs);
    }

    // Extract control paths from fullpath map
    void extractPaths(std::vector<std::string>& gate, std::vector<std::string>& freq, std::vector<std::string>& gain)
    {
        // Keep gain/vel|velocity, freq/key and gate labels
        for (const auto& it : getFullpathMap()) {
            std::string path = it.first;
            if (endsWith(path, "/gate")) {
                gate.push_back(path);
            } else if (endsWith(path, "/freq")) {
                fKeyFun = [](int pitch) { return midiToFreq(pitch); };
                freq.push_back(path);
            } else if (endsWith(path, "/key")) {
                fKeyFun = [](int pitch) { return pitch; };
                freq.push_back(path);
            } else if (endsWith(path, "/gain")) {
                fVelFun = [](int velocity) { return double(velocity)/127.0; };
                gain.push_back(path);
            } else if (endsWith(path, "/vel") || endsWith(path, "/velocity")) {
                fVelFun = [](int velocity) { return double(velocity); };
                gain.push_back(path);
            }
        }
    }
    
    // Reset voice
    void reset()
    {
        init(getSampleRate());
    }
 
    // Clear instance state
    void instanceClear()
    {
        decorator_dsp::instanceClear();
        fCurNote = kFreeVoice;
        fNextNote = fNextVel = -1;
        fLevel = FAUSTFLOAT(0);
        fDate = fRelease = 0;
    }
    
    // Keep 'pitch' and 'velocity' to fadeOut the current voice and start next one in the next buffer
    void keyOn(int pitch, int velocity, bool legato = false)
    {
        if (legato) {
            fNextNote = pitch;
            fNextVel = velocity;
        } else {
            keyOn(pitch, fVelFun(velocity));
        }
    }

    // KeyOn with normalized MIDI velocity [0..1]
    void keyOn(int pitch, double velocity)
    {
        for (size_t i = 0; i < fFreqPath.size(); i++) {
            setParamValue(fFreqPath[i], fKeyFun(pitch));
        }
        for (size_t i = 0; i < fGatePath.size(); i++) {
            setParamValue(fGatePath[i], FAUSTFLOAT(1));
        }
        for (size_t i = 0; i < fGainPath.size(); i++) {
            setParamValue(fGainPath[i], velocity);
        }
        
        fCurNote = pitch;
    }

    void keyOff(bool hard = false)
    {
        // No use of velocity for now...
        for (size_t i = 0; i < fGatePath.size(); i++) {
            setParamValue(fGatePath[i], FAUSTFLOAT(0));
        }
        
        if (hard) {
            // Immediately stop voice
            fCurNote = kFreeVoice;
        } else {
            // Release voice
            fRelease = fReleaseLengthSec * fDSP->getSampleRate();
            fCurNote = kReleaseVoice;
        }
    }
 
    // Change the voice release
    void setReleaseLength(double sec)
    {
        fReleaseLengthSec = sec;
    }

};

/**
 * A group of voices.
 */
struct dsp_voice_group {

    // GUI group for controlling voice parameters
    GroupUI fGroups;

    std::vector<dsp_voice*> fVoiceTable; // Individual voices
    dsp* fVoiceGroup;                    // Voices group to be used for GUI grouped control

    FAUSTFLOAT fPanic;  // Panic button value

    bool fVoiceControl; // Voice control mode
    bool fGroupControl; // Group control mode

    dsp_voice_group(uiCallback cb, void* arg, bool control, bool group)
        :fGroups(&fPanic, cb, arg),
        fVoiceGroup(0), fPanic(FAUSTFLOAT(0)),
        fVoiceControl(control), fGroupControl(group)
    {}

    virtual ~dsp_voice_group()
    {
        for (size_t i = 0; i < fVoiceTable.size(); i++) {
            delete fVoiceTable[i];
        }
        delete fVoiceGroup;
    }

    // Add a voice to the group
    void addVoice(dsp_voice* voice)
    {
        fVoiceTable.push_back(voice);
    }
        
    // Clear all voices from the group
    void clearVoices()
    {
        fVoiceTable.clear();
    }

    // Initialize the voice group
    void init()
    {
        // Groups all uiItem for a given path
        fVoiceGroup = new proxy_dsp(fVoiceTable[0]);
        fVoiceGroup->buildUserInterface(&fGroups);
        for (size_t i = 0; i < fVoiceTable.size(); i++) {
            fVoiceTable[i]->buildUserInterface(&fGroups);
        }
    }
    
    // Reset the user interface for each voice instance
    void instanceResetUserInterface()
    {
        for (size_t i = 0; i < fVoiceTable.size(); i++) {
            fVoiceTable[i]->instanceResetUserInterface();
        }
    }

    // Build the user interface for the voice group
    void buildUserInterface(UI* ui_interface)
    {
        if (fVoiceTable.size() > 1) {
            ui_interface->openTabBox("Polyphonic");

            // Grouped voices UI
            ui_interface->openVerticalBox("Voices");
            ui_interface->addButton("Panic", &fPanic);
            fVoiceGroup->buildUserInterface(ui_interface);
            ui_interface->closeBox();

            // If not grouped, also add individual voices UI
            if (!fGroupControl || dynamic_cast<SoundUIInterface*>(ui_interface)) {
                for (size_t i = 0; i < fVoiceTable.size(); i++) {
                    char buffer[32];
                    snprintf(buffer, 32, ((fVoiceTable.size() < 8) ? "Voice%ld" : "V%ld"), long(i+1));
                    ui_interface->openHorizontalBox(buffer);
                    fVoiceTable[i]->buildUserInterface(ui_interface);
                    ui_interface->closeBox();
                }
            }

            ui_interface->closeBox();
        } else {
            fVoiceTable[0]->buildUserInterface(ui_interface);
        }
    }

};

/**
 * Base class for MIDI controllable polyphonic DSP.
 */
#ifdef EMCC
#endif

class dsp_poly : public decorator_dsp, public midi, public JSONControl {

    protected:
    
    #ifdef EMCC
        MapUI fMapUI;               // Map for UI control
        std::string fJSON;          // JSON representation of the UI
        midi_handler fMidiHandler;  // MIDI handler for the UI
        MidiUI fMIDIUI;             // MIDI UI for the DSP
    #endif
    
    public:
    
    #ifdef EMCC
        dsp_poly(dsp* dsp):decorator_dsp(dsp), fMIDIUI(&fMidiHandler)
        {
            JSONUI jsonui(getNumInputs(), getNumOutputs());
            buildUserInterface(&jsonui);
            fJSON = jsonui.JSON(true);
            buildUserInterface(&fMapUI);
            buildUserInterface(&fMIDIUI);
        }
    #else
        dsp_poly(dsp* dsp):decorator_dsp(dsp)
        {}
    #endif
    
        virtual ~dsp_poly() {}
    
        // Reimplemented for EMCC
    #ifdef EMCC
        virtual int getNumInputs() { return decorator_dsp::getNumInputs(); }
        virtual int getNumOutputs() { return decorator_dsp::getNumOutputs(); }
        virtual void buildUserInterface(UI* ui_interface) { decorator_dsp::buildUserInterface(ui_interface); }
        virtual int getSampleRate() { return decorator_dsp::getSampleRate(); }
        virtual void init(int sample_rate) { decorator_dsp::init(sample_rate); }
        virtual void instanceInit(int sample_rate) { decorator_dsp::instanceInit(sample_rate); }
        virtual void instanceConstants(int sample_rate) { decorator_dsp::instanceConstants(sample_rate); }
        virtual void instanceResetUserInterface() { decorator_dsp::instanceResetUserInterface(); }
        virtual void instanceClear() { decorator_dsp::instanceClear(); }
        virtual dsp_poly* clone() { return new dsp_poly(fDSP->clone()); }
        virtual void metadata(Meta* m) { decorator_dsp::metadata(m); }
    
        // Additional API
        std::string getJSON()
        {
            return fJSON;
        }
    
        virtual void setParamValue(const std::string& path, FAUSTFLOAT value)
        {
            fMapUI.setParamValue(path, value);
            GUI::updateAllGuis();
        }
        
        virtual FAUSTFLOAT getParamValue(const std::string& path) { return fMapUI.getParamValue(path); }

        virtual void computeJS(int count, uintptr_t inputs, uintptr_t outputs)
        {
            decorator_dsp::compute(count, reinterpret_cast<FAUSTFLOAT**>(inputs),reinterpret_cast<FAUSTFLOAT**>(outputs));
        }
    #endif
    
        virtual MapUI* keyOn(int channel, int pitch, int velocity)
        {
            return midi::keyOn(channel, pitch, velocity);
        }
        virtual void keyOff(int channel, int pitch, int velocity)
        {
            midi::keyOff(channel, pitch, velocity);
        }
        virtual void keyPress(int channel, int pitch, int press)
        {
            midi::keyPress(channel, pitch, press);
        }
        virtual void chanPress(int channel, int press)
        {
            midi::chanPress(channel, press);
        }
        virtual void ctrlChange(int channel, int ctrl, int value)
        {
            midi::ctrlChange(channel, ctrl, value);
        }
        virtual void ctrlChange14bits(int channel, int ctrl, int value)
        {
            midi::ctrlChange14bits(channel, ctrl, value);
        }
        virtual void pitchWheel(int channel, int wheel)
        {
        #ifdef EMCC
            fMIDIUI.pitchWheel(0., channel, wheel);
            GUI::updateAllGuis();
        #else
            midi::pitchWheel(channel, wheel);
        #endif
        }
        virtual void progChange(int channel, int pgm)
        {
            midi::progChange(channel, pgm);
        }
    
        // Change the voice release
        virtual void setReleaseLength(double seconds)
        {}
    
};

/**
 * Polyphonic DSP: groups a set of DSP to be played together or triggered by MIDI.
 *
 * All voices are preallocated by cloning the single DSP voice given at creation time.
 * Dynamic voice allocation is done in 'getFreeVoice'
 */
class bbdmi_ambLagrangeMods31_poly : public dsp_voice_group, public dsp_poly {

    private:

        FAUSTFLOAT** fMixBuffer;        // Intermediate buffer for mixing voices
        FAUSTFLOAT** fOutBuffer;        // Intermediate buffer for output
        midi_interface* fMidiHandler;   // The midi_interface the DSP is connected to
        int fDate;                      // Current date for managing voices
    
        // Fade out the audio in the buffer
        void fadeOut(int count, FAUSTFLOAT** outBuffer)
        {
            // FadeOut on half buffer
            for (int chan = 0; chan < getNumOutputs(); chan++) {
                double factor = 1., step = 1./double(count);
                for (int frame = 0; frame < count; frame++) {
                    outBuffer[chan][frame] *= factor;
                    factor -= step;
                }
            }
        }
    
        // Mix the audio from the mix buffer to the output buffer, and also calculate the maximum level on the buffer
        FAUSTFLOAT mixCheckVoice(int count, FAUSTFLOAT** mixBuffer, FAUSTFLOAT** outBuffer)
        {
            FAUSTFLOAT level = 0;
            for (int chan = 0; chan < getNumOutputs(); chan++) {
                FAUSTFLOAT* mixChannel = mixBuffer[chan];
                FAUSTFLOAT* outChannel = outBuffer[chan];
                for (int frame = 0; frame < count; frame++) {
                    level = std::max<FAUSTFLOAT>(level, (FAUSTFLOAT)fabs(mixChannel[frame]));
                    outChannel[frame] += mixChannel[frame];
                }
            }
            return level;
        }
    
        // Mix the audio from the mix buffer to the output buffer
        void mixVoice(int count, FAUSTFLOAT** mixBuffer, FAUSTFLOAT** outBuffer)
        {
            for (int chan = 0; chan < getNumOutputs(); chan++) {
                FAUSTFLOAT* mixChannel = mixBuffer[chan];
                FAUSTFLOAT* outChannel = outBuffer[chan];
                for (int frame = 0; frame < count; frame++) {
                    outChannel[frame] += mixChannel[frame];
                }
            }
        }
    
        // Copy the audio from one buffer to another
        void copy(int count, FAUSTFLOAT** mixBuffer, FAUSTFLOAT** outBuffer)
        {
            for (int chan = 0; chan < getNumOutputs(); chan++) {
                memcpy(outBuffer[chan], mixBuffer[chan], count * sizeof(FAUSTFLOAT));
            }
        }

        // Clear the audio buffer
        void clear(int count, FAUSTFLOAT** outBuffer)
        {
            for (int chan = 0; chan < getNumOutputs(); chan++) {
                memset(outBuffer[chan], 0, count * sizeof(FAUSTFLOAT));
            }
        }
    
        // Get the index of a voice currently playing a specific pitch
        int getPlayingVoice(int pitch)
        {
            int voice_playing = kNoVoice;
            int oldest_date_playing = INT_MAX;
            
            for (size_t i = 0; i < fVoiceTable.size(); i++) {
                if (fVoiceTable[i]->fCurNote == pitch) {
                    // Keeps oldest playing voice
                    if (fVoiceTable[i]->fDate < oldest_date_playing) {
                        oldest_date_playing = fVoiceTable[i]->fDate;
                        voice_playing = int(i);
                    }
                }
            }
            
            return voice_playing;
        }
    
        // Allocate a voice with a given type
        int allocVoice(int voice, int type)
        {
            fVoiceTable[voice]->fDate++;
            fVoiceTable[voice]->fCurNote = type;
            return voice;
        }
    
        // Get a free voice for allocation, always returns a voice
        int getFreeVoice()
        {
            // Looks for the first available voice
            for (size_t i = 0; i < fVoiceTable.size(); i++) {
                if (fVoiceTable[i]->fCurNote == kFreeVoice) {
                    return allocVoice(i, kActiveVoice);
                }
            }

            // Otherwise steal one
            int voice_release = kNoVoice;
            int voice_playing = kNoVoice;
            int oldest_date_release = INT_MAX;
            int oldest_date_playing = INT_MAX;

            // Scan all voices
            for (size_t i = 0; i < fVoiceTable.size(); i++) {
                if (fVoiceTable[i]->fCurNote == kReleaseVoice) {
                    // Keeps oldest release voice
                    if (fVoiceTable[i]->fDate < oldest_date_release) {
                        oldest_date_release = fVoiceTable[i]->fDate;
                        voice_release = int(i);
                    }
                } else {
                    // Otherwise keeps oldest playing voice
                    if (fVoiceTable[i]->fDate < oldest_date_playing) {
                        oldest_date_playing = fVoiceTable[i]->fDate;
                        voice_playing = int(i);
                    }
                }
            }
        
            // Then decide which one to steal
            if (oldest_date_release != INT_MAX) {
                fprintf(stderr, "Steal release voice : voice_date = %d cur_date = %d voice = %d \n",
                        fVoiceTable[voice_release]->fDate,
                        fDate,
                        voice_release);
                return allocVoice(voice_release, kLegatoVoice);
            } else if (oldest_date_playing != INT_MAX) {
                fprintf(stderr, "Steal playing voice : voice_date = %d cur_date = %d voice = %d \n",
                        fVoiceTable[voice_playing]->fDate,
                        fDate,
                        voice_release);
                return allocVoice(voice_playing, kLegatoVoice);
            } else {
                assert(false);
                return kNoVoice;
            }
        }

        // Callback for panic button
        static void panic(FAUSTFLOAT val, void* arg)
        {
            if (val == FAUSTFLOAT(1)) {
                static_cast<bbdmi_ambLagrangeMods31_poly*>(arg)->allNotesOff(true);
            }
        }

        // Check if the DSP is polyphonic
        bool checkPolyphony()
        {
            if (fVoiceTable.size() > 0) {
                return true;
            } else {
                fprintf(stderr, "DSP is not polyphonic...\n");
                return false;
            }
        }

    public:
    
        /**
         * Constructor.
         *
         * @param dsp - the dsp to be used for one voice. Beware: bbdmi_ambLagrangeMods31_poly will use and finally delete the pointer.
         * @param nvoices - number of polyphony voices, should be at least 1
         * @param control - whether voices will be dynamically allocated and controlled (typically by a MIDI controler).
         *                If false all voices are always running.
         * @param group - if true, voices are not individually accessible, a global "Voices" tab will automatically dispatch
         *                a given control on all voices, assuming GUI::updateAllGuis() is called.
         *                If false, all voices can be individually controlled.
         *
         */
        bbdmi_ambLagrangeMods31_poly(dsp* dsp,
                   int nvoices,
                   bool control = false,
                   bool group = true)
        : dsp_voice_group(panic, this, control, group), dsp_poly(dsp) // dsp parameter is deallocated by ~dsp_poly
        {
            fDate = 0;
            fMidiHandler = nullptr;

            // Create voices
            assert(nvoices > 0);
            for (int i = 0; i < nvoices; i++) {
                addVoice(new dsp_voice(dsp->clone()));
            }

            // Init audio output buffers
            fMixBuffer = new FAUSTFLOAT*[getNumOutputs()];
            fOutBuffer = new FAUSTFLOAT*[getNumOutputs()];
            for (int chan = 0; chan < getNumOutputs(); chan++) {
                fMixBuffer[chan] = new FAUSTFLOAT[MIX_BUFFER_SIZE];
                fOutBuffer[chan] = new FAUSTFLOAT[MIX_BUFFER_SIZE];
            }

            dsp_voice_group::init();
        }

        virtual ~bbdmi_ambLagrangeMods31_poly()
        {
            // Remove from fMidiHandler
            if (fMidiHandler) fMidiHandler->removeMidiIn(this);
            for (int chan = 0; chan < getNumOutputs(); chan++) {
                delete[] fMixBuffer[chan];
                delete[] fOutBuffer[chan];
            }
            delete[] fMixBuffer;
            delete[] fOutBuffer;
            
        }

        // DSP API
        void buildUserInterface(UI* ui_interface)
        {
            // MidiUI ui_interface contains the midi_handler connected to the MIDI driver
            if (dynamic_cast<midi_interface*>(ui_interface)) {
                fMidiHandler = dynamic_cast<midi_interface*>(ui_interface);
                fMidiHandler->addMidiIn(this);
            }
            dsp_voice_group::buildUserInterface(ui_interface);
        }

        void init(int sample_rate)
        {
            decorator_dsp::init(sample_rate);
            fVoiceGroup->init(sample_rate);
            fPanic = FAUSTFLOAT(0);
            
            // Init voices
            for (size_t i = 0; i < fVoiceTable.size(); i++) {
                fVoiceTable[i]->init(sample_rate);
            }
        }
    
        void instanceInit(int samplingFreq)
        {
            instanceConstants(samplingFreq);
            instanceResetUserInterface();
            instanceClear();
        }

        void instanceConstants(int sample_rate)
        {
            decorator_dsp::instanceConstants(sample_rate);
            fVoiceGroup->instanceConstants(sample_rate);
            
            // Init voices
            for (size_t i = 0; i < fVoiceTable.size(); i++) {
                fVoiceTable[i]->instanceConstants(sample_rate);
            }
        }

        void instanceResetUserInterface()
        {
            decorator_dsp::instanceResetUserInterface();
            fVoiceGroup->instanceResetUserInterface();
            fPanic = FAUSTFLOAT(0);
            
            for (size_t i = 0; i < fVoiceTable.size(); i++) {
                fVoiceTable[i]->instanceResetUserInterface();
            }
        }

        void instanceClear()
        {
            decorator_dsp::instanceClear();
            fVoiceGroup->instanceClear();
            
            for (size_t i = 0; i < fVoiceTable.size(); i++) {
                fVoiceTable[i]->instanceClear();
            }
        }

        virtual bbdmi_ambLagrangeMods31_poly* clone()
        {
            return new bbdmi_ambLagrangeMods31_poly(fDSP->clone(), int(fVoiceTable.size()), fVoiceControl, fGroupControl);
        }

        void compute(int count, FAUSTFLOAT** inputs, FAUSTFLOAT** outputs)
        {
            assert(count <= MIX_BUFFER_SIZE);

            // First clear the intermediate fOutBuffer
            clear(count, fOutBuffer);

            if (fVoiceControl) {
                // Mix all playing voices
                for (size_t i = 0; i < fVoiceTable.size(); i++) {
                    dsp_voice* voice = fVoiceTable[i];
                    if (voice->fCurNote == kLegatoVoice) {
                        // Play from current note and next note
                        voice->computeLegato(count, inputs, fMixBuffer);
                        // FadeOut on first half buffer
                        fadeOut(count/2, fMixBuffer);
                        // Mix it in result
                        voice->fLevel = mixCheckVoice(count, fMixBuffer, fOutBuffer);
                    } else if (voice->fCurNote != kFreeVoice) {
                        // Compute current note
                        voice->compute(count, inputs, fMixBuffer);
                        // Mix it in result
                        voice->fLevel = mixCheckVoice(count, fMixBuffer, fOutBuffer);
                        // Check the level to possibly set the voice in kFreeVoice again
                        voice->fRelease -= count;
                        if ((voice->fCurNote == kReleaseVoice)
                            && (voice->fRelease < 0)
                            && (voice->fLevel < VOICE_STOP_LEVEL)) {
                            voice->fCurNote = kFreeVoice;
                        }
                    }
                }
            } else {
                // Mix all voices
                for (size_t i = 0; i < fVoiceTable.size(); i++) {
                    fVoiceTable[i]->compute(count, inputs, fMixBuffer);
                    mixVoice(count, fMixBuffer, fOutBuffer);
                }
            }
            
            // Finally copy intermediate buffer to outputs
            copy(count, fOutBuffer, outputs);
        }

        void compute(double date_usec, int count, FAUSTFLOAT** inputs, FAUSTFLOAT** outputs)
        {
            compute(count, inputs, outputs);
        }
    
        // Terminate all active voices, gently or immediately (depending of 'hard' value)
        void allNotesOff(bool hard = false)
        {
            for (size_t i = 0; i < fVoiceTable.size(); i++) {
                fVoiceTable[i]->keyOff(hard);
            }
        }
 
        // Additional polyphonic API
        MapUI* newVoice()
        {
            return fVoiceTable[getFreeVoice()];
        }

        void deleteVoice(MapUI* voice)
        {
            auto it = find(fVoiceTable.begin(), fVoiceTable.end(), reinterpret_cast<dsp_voice*>(voice));
            if (it != fVoiceTable.end()) {
                dsp_voice* voice = *it;
                voice->keyOff();
                voice->reset();
            } else {
                fprintf(stderr, "Voice not found\n");
            }
        }

        // MIDI API
        MapUI* keyOn(int channel, int pitch, int velocity)
        {
            if (checkPolyphony()) {
                int voice = getFreeVoice();
                fVoiceTable[voice]->keyOn(pitch, velocity, fVoiceTable[voice]->fCurNote == kLegatoVoice);
                return fVoiceTable[voice];
            } else {
                return 0;
            }
        }

        void keyOff(int channel, int pitch, int velocity = 127)
        {
            if (checkPolyphony()) {
                int voice = getPlayingVoice(pitch);
                if (voice != kNoVoice) {
                    fVoiceTable[voice]->keyOff();
                } else {
                    fprintf(stderr, "Playing pitch = %d not found\n", pitch);
                }
            }
        }

        void ctrlChange(int channel, int ctrl, int value)
        {
            if (ctrl == ALL_NOTES_OFF || ctrl == ALL_SOUND_OFF) {
                allNotesOff();
            }
        }

        // Change the voice release
        void setReleaseLength(double seconds)
        {
            for (size_t i = 0; i < fVoiceTable.size(); i++) {
                fVoiceTable[i]->setReleaseLength(seconds);
            }
        }

};

/**
 * Polyphonic DSP with an integrated effect.
 */
class dsp_poly_effect : public dsp_poly {
    
    private:
    
        // fPolyDSP will respond to MIDI messages.
        dsp_poly* fPolyDSP;
        
    public:
        
        dsp_poly_effect(dsp_poly* voice, dsp* combined)
        :dsp_poly(combined), fPolyDSP(voice)
        {}
        
        virtual ~dsp_poly_effect()
        {
            // dsp_poly_effect is also a decorator_dsp, which will free fPolyDSP
        }
    
        // MIDI API
        MapUI* keyOn(int channel, int pitch, int velocity)
        {
            return fPolyDSP->keyOn(channel, pitch, velocity);
        }
        void keyOff(int channel, int pitch, int velocity)
        {
            fPolyDSP->keyOff(channel, pitch, velocity);
        }
        void keyPress(int channel, int pitch, int press)
        {
            fPolyDSP->keyPress(channel, pitch, press);
        }
        void chanPress(int channel, int press)
        {
            fPolyDSP->chanPress(channel, press);
        }
        void ctrlChange(int channel, int ctrl, int value)
        {
            fPolyDSP->ctrlChange(channel, ctrl, value);
        }
        void ctrlChange14bits(int channel, int ctrl, int value)
        {
            fPolyDSP->ctrlChange14bits(channel, ctrl, value);
        }
        void pitchWheel(int channel, int wheel)
        {
            fPolyDSP->pitchWheel(channel, wheel);
        }
        void progChange(int channel, int pgm)
        {
            fPolyDSP->progChange(channel, pgm);
        }
    
        // Change the voice release
        void setReleaseLength(double sec)
        {
            fPolyDSP->setReleaseLength(sec);
        }
    
};

/**
 * Polyphonic DSP factory class. Helper code to support polyphonic DSP source with an integrated effect.
 */
struct dsp_poly_factory : public dsp_factory {
    
    dsp_factory* fProcessFactory;
    dsp_factory* fEffectFactory;
    
    dsp* adaptDSP(dsp* dsp, bool is_double)
    {
        return (is_double) ? new dsp_sample_adapter<double, float>(dsp) : dsp;
    }

    dsp_poly_factory(dsp_factory* process_factory = nullptr,
                     dsp_factory* effect_factory = nullptr):
    fProcessFactory(process_factory)
    ,fEffectFactory(effect_factory)
    {}

    virtual ~dsp_poly_factory()
    {}

    std::string getName() { return fProcessFactory->getName(); }
    std::string getSHAKey() { return fProcessFactory->getSHAKey(); }
    std::string getDSPCode() { return fProcessFactory->getDSPCode(); }
    std::string getCompileOptions() { return fProcessFactory->getCompileOptions(); }
    std::vector<std::string> getLibraryList() { return fProcessFactory->getLibraryList(); }
    std::vector<std::string> getIncludePathnames() { return fProcessFactory->getIncludePathnames(); }
    std::vector<std::string> getWarningMessages() { return fProcessFactory->getWarningMessages(); }
   
    std::string getEffectCode(const std::string& dsp_content)
    {
        std::stringstream effect_code;
        effect_code << "adapt(1,1) = _; adapt(2,2) = _,_; adapt(1,2) = _ <: _,_; adapt(2,1) = _,_ :> _;";
        effect_code << "adaptor(F,G) = adapt(outputs(F),inputs(G)); dsp_code = environment{ " << dsp_content << " };";
        effect_code << "process = adaptor(dsp_code.process, dsp_code.effect) : dsp_code.effect;";
        return effect_code.str();
    }

    virtual void setMemoryManager(dsp_memory_manager* manager)
    {
        fProcessFactory->setMemoryManager(manager);
        if (fEffectFactory) {
            fEffectFactory->setMemoryManager(manager);
        }
    }
    virtual dsp_memory_manager* getMemoryManager() { return fProcessFactory->getMemoryManager(); }

    /* Create a new polyphonic DSP instance with global effect, to be deleted with C++ 'delete'
     *
     * @param nvoices - number of polyphony voices, should be at least 1.
     * If -1 is used, the voice number found in the 'declare options "[nvoices:N]";' section will be used.
     * @param control - whether voices will be dynamically allocated and controlled (typically by a MIDI controler).
     *                If false all voices are always running.
     * @param group - if true, voices are not individually accessible, a global "Voices" tab will automatically dispatch
     *                a given control on all voices, assuming GUI::updateAllGuis() is called.
     *                If false, all voices can be individually controlled.
     * @param is_double - if true, internally allocated DSPs will be adapted to receive 'double' samples.
     */
    dsp_poly* createPolyDSPInstance(int nvoices, bool control, bool group, bool is_double = false)
    {
        if (nvoices == -1) {
            // Get 'nvoices' from the metadata declaration
            dsp* dsp = fProcessFactory->createDSPInstance();
            bool midi_sync;
            MidiMeta::analyse(dsp, midi_sync, nvoices);
            delete dsp;
        }
        dsp_poly* dsp_poly = new bbdmi_ambLagrangeMods31_poly(adaptDSP(fProcessFactory->createDSPInstance(), is_double), nvoices, control, group);
        if (fEffectFactory) {
            // the 'dsp_poly' object has to be controlled with MIDI, so kept separated from new dsp_sequencer(...) object
            return new dsp_poly_effect(dsp_poly, new dsp_sequencer(dsp_poly, adaptDSP(fEffectFactory->createDSPInstance(), is_double)));
        } else {
            return new dsp_poly_effect(dsp_poly, dsp_poly);
        }
    }

    /* Create a new DSP instance, to be deleted with C++ 'delete' */
    dsp* createDSPInstance()
    {
        return fProcessFactory->createDSPInstance();
    }

};

#endif // __poly_dsp__
/************************** END poly-dsp.h **************************/

list<GUI*> GUI::fGuiList;
ztimedmap GUI::gTimedZoneMap;

static t_class* faust_class;

/*--------------------------------------------------------------------------*/
static const char* getCodeSize()
{
    int tmp;
    return (sizeof(&tmp) == 8) ? "64 bits" : "32 bits";
}

/*--------------------------------------------------------------------------*/
typedef struct faust
{
    t_pxobject m_ob;
    t_atom *m_seen, *m_want;
    map<string, vector<t_object*> > m_output_table;
    short m_where;
    bool m_mute;
    void** m_args;
    mspUI* m_dspUI;
    dsp* m_dsp;
    void* m_control_outlet;
    char* m_json;
    t_systhread_mutex m_mutex;    
    int m_Inputs;
    int m_Outputs;
    SaveUI* m_savedUI;
#ifdef MIDICTRL
    MidiUI* m_midiUI;
    max_midi* m_midiHandler;
    void* m_midi_outlet;
#endif
#ifdef SOUNDFILE
    SoundUI* m_soundInterface;
#endif
#ifdef OSCCTRL
    OSCUI* m_oscInterface;
#endif
} t_faust;

void faust_create_jsui(t_faust* x);
void faust_make_json(t_faust* x);

/*--------------------------------------------------------------------------*/
void faust_allocate(t_faust* x, int nvoices)
{
    // Delete old
    delete x->m_dsp;
    delete x->m_dspUI;
    x->m_dspUI = new mspUI();
    
    if (nvoices > 0) {
    #ifdef POST
        post("polyphonic DSP voices = %d", nvoices);
    #endif
        x->m_dsp = new bbdmi_ambLagrangeMods31_poly(new bbdmi_ambLagrangeMods31(), nvoices, true, true);
    #ifdef POLY2
        x->m_dsp = new dsp_sequencer(x->m_dsp, new effect());
    #endif
    } else {
    #ifdef POST
        post("monophonic DSP");
    #endif
        // Create a DS/US + Filter adapted DSP
        std::string error;
        x->m_dsp = createSRAdapter<FAUSTFLOAT>(new bbdmi_ambLagrangeMods31(), error, DOWN_SAMPLING, UP_SAMPLING, FILTER_TYPE);
    }
    
#ifdef MIDICTRL
    x->m_dsp->buildUserInterface(x->m_midiUI);
#endif
  
    // Possible sample adaptation
    if (sizeof(FAUSTFLOAT) == 4) {
        x->m_dsp = new dsp_sample_adapter<FAUSTFLOAT, double>(x->m_dsp);
    }
    
    x->m_Inputs = x->m_dsp->getNumInputs();
    x->m_Outputs = x->m_dsp->getNumOutputs();
    
    // Initialize at the system's sampling rate
    x->m_dsp->init(long(sys_getsr()));
    
    // Initialize User Interface (here connnection with controls)
    x->m_dsp->buildUserInterface(x->m_dspUI);
    
#ifdef SOUNDFILE
    x->m_dsp->buildUserInterface(x->m_soundInterface);
#endif
    
    // Prepare JSON
    faust_make_json(x);
    
    // Send JSON to JS script
    faust_create_jsui(x);
    
#ifdef OSCCTRL
    x->m_oscInterface = NULL;
#endif
    
    // Load old controller state
    x->m_dsp->buildUserInterface(x->m_savedUI);
}

/*--------------------------------------------------------------------------*/
void faust_anything(t_faust* obj, t_symbol* s, short ac, t_atom* av)
{
    bool res = false;
    string name = string((s)->s_name);
    
    // If no argument is there, consider it as a toggle message for a button
    if (ac == 0 && obj->m_dspUI->isValue(name)) {
        
        FAUSTFLOAT off = FAUSTFLOAT(0.0);
        FAUSTFLOAT on = FAUSTFLOAT(1.0);
        obj->m_dspUI->setValue(name, off);
        obj->m_dspUI->setValue(name, on);
        
        av[0].a_type = A_FLOAT;
        av[0].a_w.w_float = off;
        faust_anything(obj, s, 1, av);
        
    } else if (mspUI::checkDigit(name)) { // List of values
        
        int pos, ndigit = 0;
        for (pos = name.size() - 1; pos >= 0; pos--) {
            if (isdigit(name[pos]) || name[pos] == ' ') {
                ndigit++;
            } else {
                break;
            }
        }
        pos++;
        
        string prefix = name.substr(0, pos);
        string num_base = name.substr(pos);
        int num = atoi(num_base.c_str());
        
        int i;
        t_atom* ap;
       
        // Increment ap each time to get to the next atom
        for (i = 0, ap = av; i < ac; i++, ap++) {
            FAUSTFLOAT value;
            switch (atom_gettype(ap)) {
                case A_LONG:
                    value = FAUSTFLOAT(ap[0].a_w.w_long);
                    break;
                case A_FLOAT:
                    value = FAUSTFLOAT(ap[0].a_w.w_float);
                    break;
                default:
                    post("Invalid argument in parameter setting"); 
                    return;         
            }
            
            string num_val = to_string(num + i);
            stringstream param_name; param_name << prefix;
            for (int i = 0; i < ndigit - mspUI::countDigit(num_val); i++) {
                param_name << ' ';
            }
            param_name << num_val;
              
            // Try special naming scheme for list of parameters
            res = obj->m_dspUI->setValue(param_name.str(), value);
            
            // Otherwise try standard name
            if (!res) {
                res = obj->m_dspUI->setValue(name, value);
            }
            if (!res) {
                post("Unknown parameter : %s", (s)->s_name);
            }
        }
        
    } else {
        // Standard parameter name
        FAUSTFLOAT value = (av[0].a_type == A_LONG) ? FAUSTFLOAT(av[0].a_w.w_long) : FAUSTFLOAT(av[0].a_w.w_float);
        res = obj->m_dspUI->setValue(name, value);
        if (!res) {
            post("Unknown parameter : %s", (s)->s_name);
        }
    }
}

/*--------------------------------------------------------------------------*/
void faust_polyphony(t_faust* x, t_symbol* s, short ac, t_atom* av)
{
    if (systhread_mutex_lock(x->m_mutex) == MAX_ERR_NONE) {
        faust_allocate(x, av[0].a_w.w_long);
        systhread_mutex_unlock(x->m_mutex);
    } else {
        post("Mutex lock cannot be taken...");
    }
}

/*--------------------------------------------------------------------------*/
#ifdef MIDICTRL
void faust_midievent(t_faust* x, t_symbol* s, short ac, t_atom* av) 
{
    if (ac > 0) {
        int type = (int)av[0].a_w.w_long & 0xf0;
        int channel = (int)av[0].a_w.w_long & 0x0f;
                
        if (ac == 1) {
            x->m_midiHandler->handleSync(0.0, av[0].a_w.w_long);
        } else if (ac == 2) {
            x->m_midiHandler->handleData1(0.0, type, channel, av[1].a_w.w_long);
        } else if (ac == 3) {
            x->m_midiHandler->handleData2(0.0, type, channel, av[1].a_w.w_long, av[2].a_w.w_long);
        }
    }
}
#endif

/*--------------------------------------------------------------------------*/
void faust_create_jsui(t_faust* x)
{
    t_object *patcher, *box, *obj;
    object_obex_lookup((t_object*)x, gensym("#P"), &patcher);
    
    for (box = jpatcher_get_firstobject(patcher); box; box = jbox_get_nextobject(box)) {
        obj = jbox_get_object(box);
        // Notify JSON
        if (obj && strcmp(object_classname(obj)->s_name, "js") == 0) {
            t_atom json;
            atom_setsym(&json, gensym(x->m_json));
            object_method_typed(obj, gensym("anything"), 1, &json, 0);
        }
    }
        
    // Keep all outputs to be notified in update_outputs
    x->m_output_table.clear();
    for (box = jpatcher_get_firstobject(patcher); box; box = jbox_get_nextobject(box)) {
        obj = jbox_get_object(box);
        t_symbol* scriptingname = jbox_get_varname(obj); // scripting name
        // Keep control outputs
        if (scriptingname && x->m_dspUI->isOutputValue(scriptingname->s_name)) {
            x->m_output_table[scriptingname->s_name].push_back(obj);
        }
    }
}

/*--------------------------------------------------------------------------*/
void faust_update_outputs(t_faust* x)
{
    for (const auto& it1 : x->m_output_table) {
        bool new_val = false;
        FAUSTFLOAT value = x->m_dspUI->getOutputValue(it1.first, new_val);
        if (new_val) {
            t_atom at_value;
            atom_setfloat(&at_value, value);
            for (const auto& it2 : it1.second) {
                object_method_typed(it2, gensym("float"), 1, &at_value, 0);
            }
        }
    }
}

/*--------------------------------------------------------------------------*/
void faust_make_json(t_faust* x)
{
    // Prepare JSON
    if (x->m_json) free(x->m_json);
    JSONUI builder(x->m_dsp->getNumInputs(), x->m_dsp->getNumOutputs());
    x->m_dsp->metadata(&builder);
    x->m_dsp->buildUserInterface(&builder);
    x->m_json = strdup(builder.JSON().c_str());
}

/*--------------------------------------------------------------------------*/
void* faust_new(t_symbol* s, short ac, t_atom* av)
{
    bool midi_sync = false;
    int nvoices = 0;
    t_faust* x = (t_faust*)object_alloc(faust_class);
    
    bbdmi_ambLagrangeMods31* tmp_dsp = new bbdmi_ambLagrangeMods31();
    MidiMeta::analyse(tmp_dsp, midi_sync, nvoices);
 #ifdef SOUNDFILE
    Max_Meta3 meta3;
    tmp_dsp->metadata(&meta3);
    string bundle_path_str = SoundUI::getBinaryPathFrom(meta3.fName);
    if (bundle_path_str == "") {
        post("Bundle_path '%s' cannot be found!", meta3.fName.c_str());
    }
    x->m_soundInterface = new SoundUI(bundle_path_str, -1, nullptr, sizeof(FAUSTFLOAT) == 8);
 #endif
    delete tmp_dsp;
    
    x->m_savedUI = new SaveLabelUI();
    // allocation is done in faust_allocate
    x->m_dspUI = NULL;
    x->m_dsp = NULL;
    x->m_json = NULL;
    x->m_mute = false;
    x->m_control_outlet = outlet_new((t_pxobject*)x, (char*)"list");
    
#ifdef MIDICTRL
    x->m_midi_outlet = outlet_new((t_pxobject*)x, NULL);
    x->m_midiHandler = new max_midi(x->m_midi_outlet);
    x->m_midiUI = new MidiUI(x->m_midiHandler);
#endif
    
    faust_allocate(x, nvoices);
   
    if (systhread_mutex_new(&x->m_mutex, SYSTHREAD_MUTEX_NORMAL) != MAX_ERR_NONE) {
        post("Cannot allocate mutex...");
    }
    
    int num_input;
    if (x->m_dspUI->isMulti()) {
        num_input = x->m_dsp->getNumInputs() + 1;
    } else {
        num_input = x->m_dsp->getNumInputs();
    }
    
    x->m_args = (void**)calloc((num_input + x->m_dsp->getNumOutputs()) + 2, sizeof(void*));
    
    /* Multi in */
    dsp_setup((t_pxobject*)x, num_input);

    /* Multi out */
    for (int i = 0; i < x->m_dsp->getNumOutputs(); i++) {
        outlet_new((t_pxobject*)x, (char*)"signal");
    }

    ((t_pxobject*)x)->z_misc = Z_NO_INPLACE; // To assure input and output buffers are actually different
 
    // Display controls
#ifdef POST
    x->m_dspUI->displayControls();
#endif
    
    // Get attributes values
    attr_args_process(x, ac, av);
    return x;
}

#ifdef OSCCTRL
// osc 'IP inport outport xmit bundle'
/*--------------------------------------------------------------------------*/
void faust_osc(t_faust* x, t_symbol* s, short ac, t_atom* av)
{
    if (ac == 5) {
        if (systhread_mutex_lock(x->m_mutex) == MAX_ERR_NONE) {
            
            delete x->m_oscInterface;
            
            const char* argv1[32];
            int argc1 = 0;
            
            argv1[argc1++] = "Faust";
            
            argv1[argc1++]  = "-desthost";
            argv1[argc1++]  = atom_getsym(&av[0])->s_name;
            
            char inport[32];
            snprintf(inport, 32, "%ld", long(av[1].a_w.w_long));
            argv1[argc1++] = "-port";
            argv1[argc1++] = inport;
            
            char outport[32];
            snprintf(outport, 32, "%ld", long(av[2].a_w.w_long));
            argv1[argc1++] = "-outport";
            argv1[argc1++] = outport;
            
            char xmit[32];
            snprintf(xmit, 32, "%ld", long(av[3].a_w.w_long));
            argv1[argc1++] = "-xmit";
            argv1[argc1++] = xmit;
            
            char bundle[32];
            snprintf(bundle, 32, "%ld", long(av[4].a_w.w_long));
            argv1[argc1++] = "-bundle";
            argv1[argc1++] = bundle;
            
            x->m_oscInterface = new OSCUI("Faust", argc1, (char**)argv1);
            x->m_dsp->buildUserInterface(x->m_oscInterface);
            x->m_oscInterface->run();

            post(x->m_oscInterface->getInfos().c_str());

            systhread_mutex_unlock(x->m_mutex);
        } else {
            post("Mutex lock cannot be taken...");
        }
    } else {
        post("Should be : osc 'IP inport outport xmit(0|1|2) bundle(0|1)'");
    }
}
#endif

/*--------------------------------------------------------------------------*/
// Reset controllers to init value and send [path, init, min, max]
void faust_init(t_faust* x, t_symbol* s, short ac, t_atom* av)
{
    // Reset internal state
    x->m_savedUI->reset();
    
    // Input controllers
    for (mspUI::iterator it = x->m_dspUI->begin2(); it != x->m_dspUI->end2(); it++) {
        t_atom myList[4];
        atom_setsym(&myList[0], gensym((*it).first.c_str()));
        atom_setfloat(&myList[1], (*it).second->getInitValue());    // init value
        atom_setfloat(&myList[2], (*it).second->getMinValue());
        atom_setfloat(&myList[3], (*it).second->getMaxValue());
        outlet_list(x->m_control_outlet, 0, 4, myList);
    }
    // Output controllers
    for (mspUI::iterator it = x->m_dspUI->begin4(); it != x->m_dspUI->end4(); it++) {
        t_atom myList[4];
        atom_setsym(&myList[0], gensym((*it).first.c_str()));
        atom_setfloat(&myList[1], (*it).second->getInitValue());    // init value
        atom_setfloat(&myList[2], (*it).second->getMinValue());
        atom_setfloat(&myList[3], (*it).second->getMaxValue());
        outlet_list(x->m_control_outlet, 0, 4, myList);
    }
 }

/*--------------------------------------------------------------------------*/
void faust_dump_inputs(t_faust* x)
{
    // Input controllers
    for (mspUI::iterator it = x->m_dspUI->begin2(); it != x->m_dspUI->end2(); it++) {
        t_atom myList[4];
        atom_setsym(&myList[0], gensym((*it).first.c_str()));
        atom_setfloat(&myList[1], (*it).second->getValue());    // cur value
        atom_setfloat(&myList[2], (*it).second->getMinValue());
        atom_setfloat(&myList[3], (*it).second->getMaxValue());
        outlet_list(x->m_control_outlet, 0, 4, myList);
    }
}

/*--------------------------------------------------------------------------*/
void faust_dump_outputs(t_faust* x)
{
    // Output controllers
    for (mspUI::iterator it = x->m_dspUI->begin4(); it != x->m_dspUI->end4(); it++) {
        t_atom myList[4];
        atom_setsym(&myList[0], gensym((*it).first.c_str()));
        atom_setfloat(&myList[1], (*it).second->getValue());    // cur value
        atom_setfloat(&myList[2], (*it).second->getMinValue());
        atom_setfloat(&myList[3], (*it).second->getMaxValue());
        outlet_list(x->m_control_outlet, 0, 4, myList);
    }
}

/*--------------------------------------------------------------------------*/
// Dump controllers as list of [path, cur, min, max]
void faust_dump(t_faust* x, t_symbol* s, short ac, t_atom* av)
{
    faust_dump_inputs(x);
    faust_dump_outputs(x);
}

/*--------------------------------------------------------------------------*/
void faust_dblclick(t_faust* x, long inlet)
{
    x->m_dspUI->displayControls();
}

/*--------------------------------------------------------------------------*/
//11/13/2015 : faust_assist is actually called at each click in the patcher, so we now use 'faust_dblclick' to display the parameters...
void faust_assist(t_faust* x, void* b, long msg, long a, char* dst)
{
    if (msg == ASSIST_INLET) {
        if (a == 0) {
            if (x->m_dsp->getNumInputs() == 0) {
                snprintf(dst, 512, "(messages)");
            } else {
                snprintf(dst, 512, "(messages/signal) : Audio Input %ld", (a+1));
            }
        } else if (a < x->m_dsp->getNumInputs()) {
            snprintf(dst, 512, "(signal) : Audio Input %ld", (a+1));
        }
    } else if (msg == ASSIST_OUTLET) {
        if (a < x->m_dsp->getNumOutputs()) {
            snprintf(dst, 512, "(signal) : Audio Output %ld", (a+1));
        } else if (a == x->m_dsp->getNumOutputs()) {
            snprintf(dst, 512, "(list) : [path, cur|init, min, max]*");
        } else {
            snprintf(dst, 512, "(int) : raw MIDI bytes*");
        }
    }
}

/*--------------------------------------------------------------------------*/
void faust_mute(t_faust* obj, t_symbol* s, short ac, t_atom* at)
{
    if (atom_gettype(at) == A_LONG) {
        obj->m_mute = atom_getlong(at);
    }
}

/*--------------------------------------------------------------------------*/
void faust_free(t_faust* x)
{
    dsp_free((t_pxobject*)x);
    delete x->m_dsp;
    delete x->m_dspUI;
    delete x->m_savedUI;
    if (x->m_args) free(x->m_args);
    if (x->m_json) free(x->m_json);
    systhread_mutex_free(x->m_mutex);
#ifdef MIDICTRL
    // m_midiUI *must* be deleted before m_midiHandler
    delete x->m_midiUI;
    delete x->m_midiHandler;
#endif
#ifdef SOUNDFILE
    delete x->m_soundInterface;
#endif
#ifdef OSCCTRL
    delete x->m_oscInterface;
#endif
}

/*--------------------------------------------------------------------------*/
void faust_perform64(t_faust* x, t_object* dsp64, double** ins, long numins, double** outs, long numouts, long sampleframes, long flags, void* userparam)
{
    AVOIDDENORMALS;
    if (!x->m_mute && systhread_mutex_trylock(x->m_mutex) == MAX_ERR_NONE) {
        if (x->m_dsp) {
            if (x->m_dspUI->isMulti()) {
                x->m_dspUI->setMultiValues(reinterpret_cast<FAUSTFLOAT*>(ins[0]), sampleframes);
                x->m_dsp->compute(sampleframes, reinterpret_cast<FAUSTFLOAT**>(++ins), reinterpret_cast<FAUSTFLOAT**>(outs));
            } else {
                x->m_dsp->compute(sampleframes, reinterpret_cast<FAUSTFLOAT**>(ins), reinterpret_cast<FAUSTFLOAT**>(outs));
            }
        #ifdef OSCCTRL
            if (x->m_oscInterface) x->m_oscInterface->endBundle();
        #endif
            //faust_update_outputs(x);
            // Use the right outlet to output messages
            faust_dump_outputs(x);
        }
    #if defined(MIDICTRL) || defined(OSCCTRL)
        GUI::updateAllGuis();
    #endif
        systhread_mutex_unlock(x->m_mutex);
    } else {
        // Write null buffers to outs
        for (int i = 0; i < numouts; i++) {
             memset(outs[i], 0, sizeof(double) * sampleframes);
        }
    }
}

/*--------------------------------------------------------------------------*/
void faust_dsp64(t_faust* x, t_object* dsp64, short* count, double samplerate, long maxvectorsize, long flags)
{
    object_method(dsp64, gensym("dsp_add64"), x, faust_perform64, 0, NULL);
}

/*--------------------------------------------------------------------------*/
t_max_err faust_attr_set(t_faust* x, t_object* attr, long ac, t_atom* av)
{
    if (ac && av) {
        t_symbol* attrname = (t_symbol*)object_method(attr, gensym("getname"));
        // Redirect on the generic message handling method
        faust_anything(x, attrname, ac, av);
    }
    return MAX_ERR_NONE;
}

/*--------------------------------------------------------------------------*/
#ifdef _WIN32
extern "C" int main(void)
#else
void ext_main(void* r)
#endif
{
    string file_name = string(FAUST_FILE_NAME);
    // Remove ".dsp" ending
    string class_name = file_name.erase(file_name.size()-4) + "~";
    t_class* c = class_new(class_name.c_str(), (method)faust_new, (method)faust_free, sizeof(t_faust), 0L, A_GIMME, 0);
    
    class_addmethod(c, (method)faust_anything, "anything", A_GIMME, 0);
    class_addmethod(c, (method)faust_polyphony, "polyphony", A_GIMME, 0);
#ifdef OSCCTRL
    class_addmethod(c, (method)faust_osc, "osc", A_GIMME, 0);
#endif
    class_addmethod(c, (method)faust_init, "init", A_GIMME, 0);
    class_addmethod(c, (method)faust_dump, "dump", A_GIMME, 0);
#ifdef MIDICTRL
    class_addmethod(c, (method)faust_midievent, "midievent", A_GIMME, 0);
#endif
    class_addmethod(c, (method)faust_dsp64, "dsp64", A_CANT, 0);
    class_addmethod(c, (method)faust_dblclick, "dblclick", A_CANT, 0);
    class_addmethod(c, (method)faust_assist, "assist", A_CANT, 0);
    class_addmethod(c, (method)faust_mute, "mute", A_GIMME, 0);
    
    dsp* tmp_dsp = new bbdmi_ambLagrangeMods31();
    mspUI tmp_UI;
    tmp_dsp->buildUserInterface(&tmp_UI);
    
    // Setup attributes
    if (sizeof(FAUSTFLOAT) == 4) {
        for (mspUI::iterator it = tmp_UI.begin1(); it != tmp_UI.end1(); it++) {
            CLASS_ATTR_FLOAT(c, (*it).first.c_str(), 0, t_faust, m_ob);
            CLASS_ATTR_ACCESSORS(c, (*it).first.c_str(), NULL, (method)faust_attr_set);
        }
    } else {
        for (mspUI::iterator it = tmp_UI.begin1(); it != tmp_UI.end1(); it++) {
            CLASS_ATTR_DOUBLE(c, (*it).first.c_str(), 0, t_faust, m_ob);
            CLASS_ATTR_ACCESSORS(c, (*it).first.c_str(), NULL, (method)faust_attr_set);
        }
    }
    
    class_dspinit(c);
    class_register(CLASS_BOX, c);
    faust_class = c;
#ifdef POST
    post((char*)"Faust DSP object v%s (sample = %s bits code = %s)", EXTERNAL_VERSION, ((sizeof(FAUSTFLOAT) == 4) ? "32" : "64"), getCodeSize());
    post((char*)"Copyright (c) 2012-2024 Grame");
#endif
    Max_Meta1 meta1;
    tmp_dsp->metadata(&meta1);
    if (meta1.fCount > 0) {
    #ifdef POST
        Max_Meta2 meta2;
        post("------------------------------");
        tmp_dsp->metadata(&meta2);
        post("------------------------------");
    #endif
    }
    delete(tmp_dsp);
#ifdef _WIN32
    return 0;
#endif
}

/******************* END max-msp64.cpp ****************/


#endif
