//--------------------------------------------------------------------------------------//
//-----------------------------------------bbdmilib-------------------------------------//
//
//----------------------------FAUST CODE FOR BBDMI ANR PROJECT--------------------------//
//
//--------------------------------------------------------------------------------------//

declare name "BBDMI Faust Lib";
declare author "Alain Bonardi";
declare author "Francesco Di Maggio";
declare author "David Fierro";
declare author "Anne Sedes";
declare author "Atau Tanaka";
declare author "Stephen Whitmarsch";
declare copyright "2022-2025 BBDMI TEAM";
declare name "bbdmi_50hz";
//
process = library("bbdmi.lib").bbdmi_electric_noise(50);
