declare filename "lagrangeSynth.dsp";
declare name "lagrangeSynth";
import("stdfaust.lib");
//NN controlling the lagrange parameters? to make them change faster in intresting ways??
bbdmi_lagrangeSynth = hgroup("LagrangeSynth",lagrangeInterpolation)  :> /(echoVoices) : fi.dcblocker : clipper//<: bbdmi_multiTranspEcho_ui(transpOrder) :> /(transpOrder) : fi.dcblocker : clipper : *(outputGain)
with{
    //outputGain = vslider("outputGain", 1, 0,1,0.01):si.smoo;
    lagrangeInterpolation = par(i,echoVoices, x, yCoords(i) : it.lagrangeInterpolation(N, (0,40,80,120,160,200)) ) :> /(echoVoices);// :> _;
    clipper = _, -1 : max : _, 1 : min;
    N = 5;//Lagrange order
    //transpOrder = 4;
//de.sdelay(10 * ma.SR,1024,delsample+delsample*indexfactor)
    xCoordsList = (x1, x2, x3, x4, x5,x6);// (x1, x2, x3, x4, x5,x6);//(0, 20, 60, 100, 120,160);
    yCoords(i) = (y1, y2(i)*indexfactor(indexdistr,i), y3(i)*indexfactor(indexdistr,i), y4(i)*indexfactor(indexdistr,i), y5(i)*indexfactor(indexdistr,i), y6);//(y1, y2, y3, y4, y5,y6);//0, 1, -0.3, 0.1, -0.8, 0;
    x = os.phasor(200, freq);
    freq = vslider("Freq", 40, 10,2000,0.01):si.smoo;

    echoVoices = 4;
    getindfactor(tf,index) = (indexdistr == tf) * th(tf, index, echoVoices);
    indexfactor(tf,index) = sum(tf, 22, getindfactor(tf,index));

    indexdistr = vslider("indexdistr ",0,0,21,1);

    xmod = os.osc(modGlobalFreq)*200;
    modGlobalFreq = vslider("modGlobalFreq", 10, 0.01,1000,0.01):si.smoo;
    modfreq = (xmod/200)*2*ma.PI*((x/100):sin:+(1):/(2));
    //y coordinates
    y1 = 0;
    //
    y2(i) = y2mod(i): +~(/(2)) : *(y2mod(i));//y2mod(i): +~(/(2) : *(y2mod(i)));
    y2mod(i) = ((modfreq*y2modFactor*indexfactor(indexdistr,i)):sin:+(1):/(2));
    y2modFactor = vslider("y2modFactor", 0.1, 0,10,0.001):si.smoo;
    y3(i) = -1*y3mod(i): +~(/(2)): *(y3mod(i));
    y3mod(i) = ((modfreq*y3modFactor*indexfactor(indexdistr,i)):sin:+(1):/(2));
    y3modFactor = vslider("y3modFactor", 0.1, 0,10,0.001):si.smoo;
    y4(i) = y4mod(i): +~(/(2)): *(y4mod(i));
    y4mod(i) = ((modfreq*y4modFactor*indexfactor(indexdistr,i)):sin:+(1):/(2));
    y4modFactor = vslider("y4modFactor", 0.1, 0,10,0.001):si.smoo;
    y5(i) = -1*y5mod(i): +~(/(2)): *(y5mod(i));
    y5mod(i) = ((modfreq*y5modFactor*indexfactor(indexdistr,i)):sin:+(1):/(2));
    y5modFactor = vslider("y5modFactor", 0.1, 0,10,0.001):si.smoo;
    //
    y6 = 0;
};


process = bbdmi_lagrangeSynth;





th(0, i, p) = (i+1) / p;
    th(1, i, p) = ((i+1) / p)^2;
    th(2, i, p) = sin(ma.PI * 0.5 * (i+1) / p);
    th(3, i, p) = log10(1 + (i+1) / p) / log10(2);
    th(4, i, p) = sqrt((i+1) / p);
    th(5, i, p) = 1 - cos(ma.PI * 0.5 * (i+1) / p);
    th(6, i, p) = (1 - cos(ma.PI * (i+1) / p)) * 0.5;
    th(7, i, p) = 1 - (1 - (i+1) / p )^2;
    th(8, i, p) = ((i+1) / p < 0.5) * 2 * ((i+1) / p)^2 + ((i+1) / p >= 0.5) * (1 - (-2 * (i+1) / p + 2)^2 * 0.5);
    th(9, i, p) = ((i+1) / p)^3;
    th(10, i, p) = 1 - (1 - (i+1) / p)^3;
    th(11, i, p) = ((i+1) / p < 0.5) * 4 * ((i+1) / p)^3 + ((i+1) / p >= 0.5) * (1 - (-2 * (i+1) / p + 2)^3 * 0.5);
    th(12, i, p) = ((i+1) / p)^4;
    th(13, i, p) = 1 - (1 - (i+1) / p)^4;
    th(14, i, p) = ((i+1) / p < 0.5) * 8 * ((i+1) / p)^4 + ((i+1) / p >= 0.5) * (1 - (-2 * (i+1) / p + 2)^4 * 0.5);
    th(15, i, p) = ((i+1) / p)^5;
    th(16, i, p) = 1 - (1 - (i+1) / p)^5;
    th(17, i, p) = ((i+1) / p < 0.5) * 16 * ((i+1) / p)^5 + ((i+1) / p >= 0.5) * (1 - (-2 * (i+1) / p + 2)^5 * 0.5);
    th(18, i, p) = 2^(10 * (i+1) / p - 10);
    th(19, i, p) = ((i+1) / p < 1) * (1 - 2^(-10 * (i+1) / p)) + ((i+1) / p == 1);
    th(20, i, p) = 1 - sqrt(1 - ((i+1) / p)^2);
    th(21, i, p) = sqrt(1 - ((i+1) / p - 1)^2);
