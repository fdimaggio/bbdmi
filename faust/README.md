# BBDMI Faust repository

## About

DSP files created for sonifying and processing electromyogram (EMG) and electroencephalogram (EEG) data can be found in this folder. The [EAVI board](https://gitlab.huma-num.fr/bbdmi/bbdmi/-/tree/main/faust/eavi_board/) folder has patches designed for the board running an [OpenWare](https://github.com/RebelTechnology/OpenWare) firmware codebase. DSP files for running on any compatible host may be found in the [sound_synthesis](https://gitlab.huma-num.fr/bbdmi/bbdmi/-/tree/main/faust/eavi_board/sound_synthesis) folder.

## Installation

If you have [Git](http://git-scm.com/) or [GitHub Desktop](https://desktop.github.com/) installed, you can clone this repo via Terminal using the following commands:

```
cd ~/Documents/GitHub
git clone https://gitlab.huma-num.fr/bbdmi/bbdmi
```

You can also [download the latest release here](https://gitlab.huma-num.fr/bbdmi/bbdmi).