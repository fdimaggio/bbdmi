# eavi~

```bbdmi.eavi~``` acquires and outputs ExG and motion data from the EAVI board.

## Use

It outputs (```mc~```) audio signals.

### MESSAGES

(```onoff``` toggle) *enable/disable output stream*

(```refresh``` bang) *refresh midi ports*

### OUTPUT

>[outlet] (```message```, type) *description*

[1] (mc~ signal) *ExG*

[2] (mc~ signal) *gyroscope*