# eavi

```bbdmi.eavi``` acquires and outputs ExG and motion data from the EAVI board.

## Use

It outputs a (```list```) of control messages.

### MESSAGES

(```onoff``` toggle) *enable/disable output stream*

(```refresh``` bang) *refresh midi ports*

### OUTPUT

>[outlet] (```message```, type) *description*

[1] (list) *ExG [range: -1,1]*

[2] (list) *gyroscope [range: -1,1]*