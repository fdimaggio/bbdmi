{
	"patcher" : 	{
		"fileversion" : 1,
		"appversion" : 		{
			"major" : 8,
			"minor" : 5,
			"revision" : 2,
			"architecture" : "x64",
			"modernui" : 1
		}
,
		"classnamespace" : "box",
		"rect" : [ 34.0, 79.0, 495.0, 671.0 ],
		"openrect" : [ 34.0, 79.0, 0.0, 0.0 ],
		"bglocked" : 0,
		"openinpresentation" : 0,
		"default_fontsize" : 12.0,
		"default_fontface" : 0,
		"default_fontname" : "Arial",
		"gridonopen" : 1,
		"gridsize" : [ 15.0, 15.0 ],
		"gridsnaponopen" : 1,
		"objectsnaponopen" : 1,
		"statusbarvisible" : 2,
		"toolbarvisible" : 1,
		"lefttoolbarpinned" : 2,
		"toptoolbarpinned" : 2,
		"righttoolbarpinned" : 0,
		"bottomtoolbarpinned" : 0,
		"toolbars_unpinned_last_save" : 3,
		"tallnewobj" : 0,
		"boxanimatetime" : 200,
		"enablehscroll" : 1,
		"enablevscroll" : 1,
		"devicewidth" : 0.0,
		"description" : "",
		"digest" : "",
		"tags" : "",
		"style" : "",
		"subpatcher_template" : "Default Max 7",
		"assistshowspatchername" : 0,
		"boxes" : [ 			{
				"box" : 				{
					"id" : "obj-8",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 255.5, 138.398857115564283, 83.0, 22.0 ],
					"text" : "bbdmi.~2list 4",
					"varname" : "bbdmi.calibrate[3]"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-7",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 255.5, 223.196571346692849, 216.0, 22.0 ],
					"text" : "bbdmi.rms~ 4 @winsize 0.5 @clock 10",
					"varname" : "bbdmi.calibrate[2]"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-5",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 255.5, 371.196571346692849, 155.0, 22.0 ],
					"text" : "bbdmi.calibrate 4 @mode 1",
					"varname" : "bbdmi.calibrate[1]"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-11",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 255.5, 53.0, 67.0, 22.0 ],
					"text" : "bbdmi.eavi",
					"varname" : "bbdmi.eavi[1]"
				}

			}
, 			{
				"box" : 				{
					"args" : [ 4 ],
					"bgmode" : 0,
					"border" : 1,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-2",
					"lockeddragscroll" : 0,
					"lockedsize" : 0,
					"maxclass" : "bpatcher",
					"name" : "bbdmi.list2~.maxpat",
					"numinlets" : 2,
					"numoutlets" : 2,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "multichannelsignal", "" ],
					"patching_rect" : [ 19.0, 131.898857115564283, 199.0, 35.0 ],
					"varname" : "bbdmi.list2~",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"args" : [ 4, "@winsize", 0.5, "@clock", 10 ],
					"bgmode" : 0,
					"border" : 1,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-1",
					"lockeddragscroll" : 0,
					"lockedsize" : 0,
					"maxclass" : "bpatcher",
					"name" : "bbdmi.rms~.maxpat",
					"numinlets" : 2,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "" ],
					"patching_rect" : [ 19.0, 190.797714231128566, 198.0, 87.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 20.0, 245.682142857142708, 198.0, 87.0 ],
					"varname" : "bbdmi.rms~",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.168627450980392, 0.168627450980392, 0.168627450980392, 1.0 ],
					"candycane" : 8,
					"contdata" : 1,
					"id" : "obj-20",
					"maxclass" : "multislider",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 19.0, 486.595428462257132, 199.0, 163.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 38.480896085500717, 219.0, 194.0, 200.0 ],
					"setminmax" : [ 0.0, 1.0 ],
					"setstyle" : 3,
					"signed" : 1,
					"size" : 4,
					"slidercolor" : [ 0.850980392156863, 0.784313725490196, 0.207843137254902, 1.0 ],
					"spacing" : 2
				}

			}
, 			{
				"box" : 				{
					"annotation" : "",
					"args" : [ 4, "@mode", 1 ],
					"bgmode" : 0,
					"border" : 1,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"hint" : "",
					"id" : "obj-3",
					"lockeddragscroll" : 0,
					"lockedsize" : 0,
					"maxclass" : "bpatcher",
					"name" : "bbdmi.calibrate.maxpat",
					"numinlets" : 2,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "" ],
					"patching_rect" : [ 19.0, 301.696571346692849, 199.0, 161.0 ],
					"varname" : "bbdmi.calibrate",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"annotation" : "",
					"bgmode" : 0,
					"border" : 1,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"hint" : "",
					"id" : "obj-4",
					"lockeddragscroll" : 0,
					"lockedsize" : 0,
					"maxclass" : "bpatcher",
					"name" : "bbdmi.eavi.maxpat",
					"numinlets" : 1,
					"numoutlets" : 2,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 19.0, 20.0, 198.999999999999972, 88.0 ],
					"varname" : "bbdmi.eavi",
					"viewvisibility" : 1
				}

			}
 ],
		"lines" : [ 			{
				"patchline" : 				{
					"destination" : [ "obj-3", 0 ],
					"midpoints" : [ 28.5, 279.0, 28.5, 279.0 ],
					"source" : [ "obj-1", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-8", 0 ],
					"midpoints" : [ 265.0, 78.0, 265.0, 78.0 ],
					"source" : [ "obj-11", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-1", 0 ],
					"midpoints" : [ 28.5, 168.0, 28.5, 168.0 ],
					"source" : [ "obj-2", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-20", 0 ],
					"midpoints" : [ 28.5, 465.0, 28.5, 465.0 ],
					"source" : [ "obj-3", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-2", 0 ],
					"midpoints" : [ 28.5, 111.0, 28.5, 111.0 ],
					"source" : [ "obj-4", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-5", 0 ],
					"midpoints" : [ 265.0, 246.0, 265.0, 246.0 ],
					"source" : [ "obj-7", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-7", 0 ],
					"midpoints" : [ 265.0, 162.0, 265.0, 162.0 ],
					"source" : [ "obj-8", 0 ]
				}

			}
 ],
		"parameters" : 		{
			"obj-3::obj-107::obj-33" : [ "tab[33]", "tab[1]", 0 ],
			"obj-3::obj-123::obj-33" : [ "tab[466]", "tab[1]", 0 ],
			"obj-3::obj-34::obj-33" : [ "tab[34]", "tab[1]", 0 ],
			"obj-3::obj-36::obj-33" : [ "tab[35]", "tab[1]", 0 ],
			"obj-3::obj-40::obj-33" : [ "tab[36]", "tab[1]", 0 ],
			"obj-3::obj-41::obj-33" : [ "tab[37]", "tab[1]", 0 ],
			"obj-3::obj-42::obj-33" : [ "tab[38]", "tab[1]", 0 ],
			"obj-3::obj-43::obj-33" : [ "tab[39]", "tab[1]", 0 ],
			"obj-3::obj-44::obj-33" : [ "tab[40]", "tab[1]", 0 ],
			"obj-3::obj-45::obj-33" : [ "tab[41]", "tab[1]", 0 ],
			"obj-3::obj-46::obj-33" : [ "tab[42]", "tab[1]", 0 ],
			"obj-3::obj-47::obj-33" : [ "tab[43]", "tab[1]", 0 ],
			"obj-3::obj-48::obj-33" : [ "tab[44]", "tab[1]", 0 ],
			"obj-3::obj-49::obj-33" : [ "tab[45]", "tab[1]", 0 ],
			"obj-3::obj-50::obj-33" : [ "tab[46]", "tab[1]", 0 ],
			"obj-3::obj-74::obj-33" : [ "tab[32]", "tab[1]", 0 ],
			"obj-5::obj-107::obj-33" : [ "tab[2]", "tab[1]", 0 ],
			"obj-5::obj-123::obj-33" : [ "tab[18]", "tab[1]", 0 ],
			"obj-5::obj-34::obj-33" : [ "tab[3]", "tab[1]", 0 ],
			"obj-5::obj-36::obj-33" : [ "tab[4]", "tab[1]", 0 ],
			"obj-5::obj-40::obj-33" : [ "tab[5]", "tab[1]", 0 ],
			"obj-5::obj-41::obj-33" : [ "tab[6]", "tab[1]", 0 ],
			"obj-5::obj-42::obj-33" : [ "tab[7]", "tab[1]", 0 ],
			"obj-5::obj-43::obj-33" : [ "tab[8]", "tab[1]", 0 ],
			"obj-5::obj-44::obj-33" : [ "tab[9]", "tab[1]", 0 ],
			"obj-5::obj-45::obj-33" : [ "tab[10]", "tab[1]", 0 ],
			"obj-5::obj-46::obj-33" : [ "tab[11]", "tab[1]", 0 ],
			"obj-5::obj-47::obj-33" : [ "tab[12]", "tab[1]", 0 ],
			"obj-5::obj-48::obj-33" : [ "tab[13]", "tab[1]", 0 ],
			"obj-5::obj-49::obj-33" : [ "tab[14]", "tab[1]", 0 ],
			"obj-5::obj-50::obj-33" : [ "tab[15]", "tab[1]", 0 ],
			"obj-5::obj-74::obj-33" : [ "tab[1]", "tab[1]", 0 ],
			"parameterbanks" : 			{
				"0" : 				{
					"index" : 0,
					"name" : "",
					"parameters" : [ "-", "-", "-", "-", "-", "-", "-", "-" ]
				}

			}
,
			"inherited_shortname" : 1
		}
,
		"dependency_cache" : [ 			{
				"name" : "bbdmi.calibrate.maxpat",
				"bootpath" : "~/Documents/GitHub/bbdmi/max/control_processing/calibrate",
				"patcherrelativepath" : "../../../control_processing/calibrate",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bbdmi.eavi.maxpat",
				"bootpath" : "~/Documents/GitHub/bbdmi/max/input/eavi",
				"patcherrelativepath" : "..",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bbdmi.list2~.maxpat",
				"bootpath" : "~/Documents/GitHub/bbdmi/max/utilities/list2~",
				"patcherrelativepath" : "../../../utilities/list2~",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bbdmi.rms~.maxpat",
				"bootpath" : "~/Documents/GitHub/bbdmi/max/feature_extraction/rms~",
				"patcherrelativepath" : "../../../feature_extraction/rms~",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bbdmi.~2list.maxpat",
				"bootpath" : "~/Documents/GitHub/bbdmi/max/utilities/~2list",
				"patcherrelativepath" : "../../../utilities/~2list",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "name.js",
				"bootpath" : "~/Documents/GitHub/bbdmi/max/source/js",
				"patcherrelativepath" : "../../../source/js",
				"type" : "TEXT",
				"implicit" : 1
			}
, 			{
				"name" : "p.calibrate.maxpat",
				"bootpath" : "~/Documents/GitHub/bbdmi/max/control_processing/calibrate",
				"patcherrelativepath" : "../../../control_processing/calibrate",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "p.exposer.maxpat",
				"bootpath" : "~/Documents/GitHub/bbdmi/max/source/patchers",
				"patcherrelativepath" : "../../../source/patchers",
				"type" : "JSON",
				"implicit" : 1
			}
 ],
		"autosave" : 0
	}

}
