# myo

```bbdmi.myo``` acquires and streams data from the Myo armband.

## Use

It outputs a (```list```) of control messages (EMG, QUATERNIONS).

### MESSAGES

(```onoff``` toggle) *enable/disable output stream*

(```refresh``` bang) *refresh midi ports*

### OUTPUT

>[outlet] (```message```, type) *description*

[1] (list) *EMG*

[2] (list) *QUATERNIONS*

## Dependencies

It requires [Myo object for Max](https://github.com/JulesFrancoise/myo-for-max).