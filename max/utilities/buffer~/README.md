# buffer~

```bbdmi.buffer~``` allows to upload and playback an audio sample (mono or stereo), or record from live audio input. 

## Use

Samples, or a folder of samples, can be dragged and dropped in the relative spaces. Similarly, clicking on either spaces will open up a dialog window to ask for a file (or folder). In alternative, one input (```mc.list```) of multichannel audio signal can be sent on the first inlet, recorded and played back. The buffer length must be defined beforehand, and the recording toggle will stop automatically once the recording time has reached the end. Users can find an example in the bbdmi.granulator~ help file.

### ATTRIBUTES

(```message```) *buffer (name)*

### MESSAGES

(mc.~ list) *to record*

### OUTPUT

(mc.~ list) *buffer playback*