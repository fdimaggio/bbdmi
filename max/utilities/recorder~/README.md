# recorder~

```bbdmi.recorder~``` records and plays back a multichannel (```mc.* list```) of audio signals.

## Use

It takes one multichannel (```mc.~ list```) of audio signals and outputs a recorded version. Recordings can be saved in a .wav file, as well as recalled from disk. 

### ARGUMENTS

(int) *number of channels*

### MESSAGES

>[inlet] (```message```, type) *description*

[1] (mc.~ signal) *to record*

[2] (```record ``` toggle) *record messages*

[2] (```playback``` toggle) *playback messages*

[2] (```loop``` toggle) *loop recorded messages*

[2] (```clear``` bang) *clear recording*

[2] (```read``` bang) *recall a recording from disk (.json file)*

[2] (```write``` bang) *save recording to disk (.json file)*
 
### OUTPUT

(mc.~ signal) *recorded*