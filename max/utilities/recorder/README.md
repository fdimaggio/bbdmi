# recorder

```bbdmi.recorder``` records and plays back a (```list```) of messages.

## Use

It takes one input (```list```) and outputs a recorded version. Recordings can be saved in a json file, as well as recalled from disk. 

### ARGUMENTS

(int) *number of channels*

### MESSAGES

>[inlet] (```message```, type) *description*

[1] (list) *to record*

[2] (```record ``` toggle) *record messages*

[2] (```playback``` toggle) *playback messages*

[2] (```loop``` toggle) *loop recorded messages*

[2] (```clear``` bang) *clear recording*

[2] (```read``` bang) *recall a recording from disk (.json file)*

[2] (```write``` bang) *save recording to disk (.json file)*
 
### OUTPUT

(list) *recorded*