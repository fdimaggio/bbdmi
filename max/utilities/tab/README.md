# tab

```bbdmi.tab``` visualises and outputs a tab value.

## Use

It takes one input (```int```) and outputs the same (```int```) content.

### ATTRIBUTES

(int) *number of channels*

### MESSAGES

>[inlet] (```message```, type) *description*

[1] (int) *tab number* 

[2] (```rand``` bang) *output a random number*

[2] (```reset``` bang) *set list to zero*
 
### OUTPUT

(int) ```tab number```