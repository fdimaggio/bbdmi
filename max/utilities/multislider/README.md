# multislider

```bbdmi.multislider``` visualises and outputs a (```list```) of control messages.

## Use

It takes one input (```list```) and outputs the same (```list```) content.

### ATTRIBUTES

>[order] (```message```, type) *description*

[1] (int) *number of channels*

[2] (```polarity``` toggle) *unipolar [range: 0,1] or bipolar [range, -1,1]*

### ARGUMENTS

(```@polarity``` toggle) *unipolar or bipolar*

### MESSAGES

>[inlet] (```message```, type) *description*

[1] (list) *to visualize*

[2] (```rand``` bang) *generates random numbers*

[2] (```reset``` bang) *set list to zero*
 
### OUTPUT

(list) *messages [range: 0,1]*