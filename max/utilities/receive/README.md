# receive

## Use

Give the module a name, and it will receive a (```list```) from the relative <send> object.

### ATTRIBUTES

(symbol) *receive name*

### OUTPUT

(list) *received*