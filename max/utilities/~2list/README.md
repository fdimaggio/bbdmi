# ~2list

```bbdmi.~2list``` converts multichannel audio signals to list.

## Use

It takes one input (```mc.~ signal```) and outputs a (```list```) of control messages. 

### ARGUMENTS

(int) *number of channels*

### MESSAGES

(mc.~ signal) *to convert*

### OUTPUT

(list) *converted [range: -1,1]*