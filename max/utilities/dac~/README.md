# dac~

```bbdmi.dac~``` plays back multichannel signals.

## Use

It takes one input (```mc.~ list```) and plays back a multichannel version.

### ATTRIBUTES

(```@mute``` int) *mute/unmute output signal*

(```@dB``` int) *signal amplitude [range: -70, 6]*

### MESSAGES

>[inlet] (```message```, type) *description*

[1] (mc.~ signal) *to convert*

[2] (```mute``` int) *mute/unmute output signal*

[2] (```dB``` int) *signal amplitude [range: -70, 6]*