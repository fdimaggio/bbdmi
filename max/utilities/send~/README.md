# send~

## Use

Give the module a name, and it will send a (```mc.~ list```) to the relative <mc.receive> object.

### ATTRIBUTES

(symbol) *send (name)*

### ARGUMENTS

(```@chans``` int) *number of channels*

### MESSAGES

(mc.~ signal) *to send*