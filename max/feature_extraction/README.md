# Feature Extraction

The [Feature extraction](https://gitlab.huma-num.fr/bbdmi/bbdmi/-/tree/main/max/feature_extraction) module contains abstractions that extracts meaningful data from incoming signals, such as bandpassed power, or a root-mean-square. These abstractions expect signal (audio) input, and output data (list messages).