# template

```bbdmi.template``` is an empty patcher made to aid the process of creating a new module while maintaining the system architecture requirements. Here you can explain how a module works.

## Use

Here you can explain how to use the module.

### ARGUMENTS

>[order] (```message```, type) *description*

List of arguments.

### ATTRIBUTES

List of attributes.

### MESSAGES

>[inlet] (```message```, type) *description*

List of attributes.

### OUTPUT

>[outlet] (```message```, type) *description*

List of outlets.