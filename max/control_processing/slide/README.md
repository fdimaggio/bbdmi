# slide

```bbdmi.slide``` smooth a list of control signals logarithmically. It is useful for lowpass filtering to smooth a stream of continuous data.

## Use

It takes one input (```list```) and outputs a smoothed version (```list```). 

### ATTRIBUTES

(int) *number of channels*

### ARGUMENTS

(```@up``` float) *slide up (ms)*

(```@down``` float) *slide down (ms)*

### MESSAGES

>[inlet] (```message```, type) *description*

[1] (list) *to slide*

[2] (```up``` float) *slide up (ms)*

[2]	(```down``` float) *slide down (ms)*

### OUTPUT

(list) *slided [range: 0,1]*
