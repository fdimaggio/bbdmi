# schmitt

```bbdmi.schmitt``` triggers a 1 when a high threshold is passed, a 0 when a lower threshold is reached.

## Use

It takes one input (```float, int```) and triggers a boolean value (```0-1``

### ATTRIBUTES

(```@threshold``` list) *low and high threshold ([range: 0,1]*

### MESSAGES

>[inlet] (```message```, type) *description*

[1] (float, int) *to schmitt*

[2] (```threshold``` int) *low and high threshold [range: 0,1]*

### OUTPUT
(int) *trigger (0-1)*