# latch

```bbdmi.latch``` outputs the higher value from a (```list```) of two control messages.

## Use

It takes one input (```list```) of two values, and outputs the higher value (```int, float```).

### MESSAGES

(list) *to latch*

### OUTPUT

(int, float) *latched [range: 0,1]*