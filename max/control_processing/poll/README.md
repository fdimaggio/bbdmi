# poll

```bbdmi.poll``` outputs messages at an adjustable time throughput.

## Use

It takes one input (```list```) and outputs a polled version (```list```). 

### ATTRIBUTS

(int) *number of channels*

### ARGUMENTS

(```@time``` int) *time throughput (ms)*

### MESSAGES

>[inlet] (```message```, type) *description*

[1] (list) *to poll*

[2] (```time``` int) *time throughput (ms)*

### OUTPUT

(list) *polled [range: 0,1]*