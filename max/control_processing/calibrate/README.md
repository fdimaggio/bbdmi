# calibrate

```bbdmi.calibrate``` dynamically calibrates minimum and maximum input of a list of control signals over an adjustable period of time (defined in seconds).

## Use

It takes one input (```list```) and outputs a calibrated version over time (```list```). 

### ARGUMENTS

(int) *number of channels*

### ATTRIBUTES

(```@freeze``` toggle) *calibration status (0 = disabled, 1 = enabled)*

(```@time``` int, float) *time window (seconds)*

(```@polarity``` toggle) *minimum output value (unipolar = 0, bipolar = -1)*

### MESSAGES

>[inlet] (```message```, type) *description*

[1] (list) *to calibrate*

[2] (```freeze``` toggle) *calibration status (0 = disabled, 1 = enabled)*

[2] (```time``` int, float) *time window (seconds)*

[2] (```polarity``` toggle) *minimum output value (unipolar = 0, bipolar = -1)*

### OUTPUT

(list) *calibrated [range: 0,1]*