# speedlim

```bbdmi.speedlim``` limits the speed throughput of a list of control messages.

## Use

It takes one input (```list```) and outputs a speed-limited version (```list```).

### ARGUMENTS

(int) *number of channels*

### ATTRIBUTES

(```@speedlim``` int) *speed limit throughput (ms)*

### MESSAGES

>[inlet] (```message```, type) *description*

[1] (list) *to speedlim*

[2] (```speedlim``` int) *speed limit throughput (ms)*

### OUTPUT
(list, float) *speedlimed [range: 0,1]*