# steer

```bbdmi.steer``` calculates the direction from a (```list```) of two control messages, keeping the origin position at half (offset) when both inputs are equal to zero.

## Use

It takes one input (```list```) of two values, and calculates in order the difference (-), the invention (* -1.), the offset (+ 1.) and the average (/ 2.) of two values to output one single value, i.e. the direction (```int, float```). 

### MESSAGES

(list) *to steer (two values)*

### OUTPUT

(int, float) *steered (direction) [range: 0,1]*