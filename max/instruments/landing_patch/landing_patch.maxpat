{
	"patcher" : 	{
		"fileversion" : 1,
		"appversion" : 		{
			"major" : 8,
			"minor" : 5,
			"revision" : 6,
			"architecture" : "x64",
			"modernui" : 1
		}
,
		"classnamespace" : "box",
		"rect" : [ 34.0, 66.0, 1370.0, 655.0 ],
		"bglocked" : 0,
		"openinpresentation" : 0,
		"default_fontsize" : 12.0,
		"default_fontface" : 0,
		"default_fontname" : "Arial",
		"gridonopen" : 1,
		"gridsize" : [ 15.0, 15.0 ],
		"gridsnaponopen" : 1,
		"objectsnaponopen" : 1,
		"statusbarvisible" : 2,
		"toolbarvisible" : 1,
		"lefttoolbarpinned" : 0,
		"toptoolbarpinned" : 0,
		"righttoolbarpinned" : 0,
		"bottomtoolbarpinned" : 0,
		"toolbars_unpinned_last_save" : 2,
		"tallnewobj" : 0,
		"boxanimatetime" : 200,
		"enablehscroll" : 1,
		"enablevscroll" : 1,
		"devicewidth" : 0.0,
		"description" : "",
		"digest" : "",
		"tags" : "",
		"style" : "",
		"subpatcher_template" : "Default Max 7",
		"showrootpatcherontab" : 0,
		"showontab" : 0,
		"assistshowspatchername" : 0,
		"globalpatchername" : "bbdmi.preset[1][1][1]",
		"boxes" : [ 			{
				"box" : 				{
					"id" : "obj-1",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 0,
					"patcher" : 					{
						"fileversion" : 1,
						"appversion" : 						{
							"major" : 8,
							"minor" : 5,
							"revision" : 6,
							"architecture" : "x64",
							"modernui" : 1
						}
,
						"classnamespace" : "box",
						"rect" : [ 0.0, 26.0, 1370.0, 629.0 ],
						"bglocked" : 0,
						"openinpresentation" : 0,
						"default_fontsize" : 12.0,
						"default_fontface" : 0,
						"default_fontname" : "Arial",
						"gridonopen" : 1,
						"gridsize" : [ 15.0, 15.0 ],
						"gridsnaponopen" : 1,
						"objectsnaponopen" : 1,
						"statusbarvisible" : 2,
						"toolbarvisible" : 1,
						"lefttoolbarpinned" : 0,
						"toptoolbarpinned" : 0,
						"righttoolbarpinned" : 0,
						"bottomtoolbarpinned" : 0,
						"toolbars_unpinned_last_save" : 0,
						"tallnewobj" : 0,
						"boxanimatetime" : 200,
						"enablehscroll" : 1,
						"enablevscroll" : 1,
						"devicewidth" : 0.0,
						"description" : "",
						"digest" : "",
						"tags" : "",
						"style" : "",
						"subpatcher_template" : "Default Max 7",
						"showontab" : 2,
						"assistshowspatchername" : 0,
						"boxes" : [ 							{
								"box" : 								{
									"annotation" : "",
									"bgmode" : 0,
									"border" : 1,
									"clickthrough" : 0,
									"enablehscroll" : 0,
									"enablevscroll" : 0,
									"hint" : "",
									"id" : "obj-13",
									"lockeddragscroll" : 0,
									"lockedsize" : 0,
									"maxclass" : "bpatcher",
									"name" : "bbdmi.recorder~.maxpat",
									"numinlets" : 2,
									"numoutlets" : 2,
									"offset" : [ 0.0, 0.0 ],
									"outlettype" : [ "multichannelsignal", "" ],
									"patching_rect" : [ 602.0, 43.0, 198.0, 140.549999999999955 ],
									"viewvisibility" : 1
								}

							}
, 							{
								"box" : 								{
									"annotation" : "",
									"args" : [ 4 ],
									"bgmode" : 0,
									"border" : 1,
									"clickthrough" : 0,
									"enablehscroll" : 0,
									"enablevscroll" : 0,
									"hint" : "",
									"id" : "obj-9",
									"lockeddragscroll" : 0,
									"lockedsize" : 0,
									"maxclass" : "bpatcher",
									"name" : "bbdmi_eeg_simulator~.maxpat",
									"numinlets" : 1,
									"numoutlets" : 2,
									"offset" : [ 0.0, 0.0 ],
									"outlettype" : [ "multichannelsignal", "" ],
									"patching_rect" : [ 389.5, 43.0, 198.0, 140.549999999999955 ],
									"varname" : "bbdmi_eeg_simulator~",
									"viewvisibility" : 1
								}

							}
, 							{
								"box" : 								{
									"bubble" : 1,
									"bubbleside" : 2,
									"id" : "obj-7",
									"linecount" : 2,
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 419.75, 1285.925000000000182, 141.0, 52.0 ],
									"text" : "automatically scales 0,1 \nvalues range to 0,127",
									"textjustification" : 1
								}

							}
, 							{
								"box" : 								{
									"args" : [ 4 ],
									"bgmode" : 0,
									"border" : 1,
									"clickthrough" : 0,
									"enablehscroll" : 0,
									"enablevscroll" : 0,
									"id" : "obj-23",
									"lockeddragscroll" : 0,
									"lockedsize" : 0,
									"maxclass" : "bpatcher",
									"name" : "bbdmi.~2list.maxpat",
									"numinlets" : 2,
									"numoutlets" : 2,
									"offset" : [ 0.0, 0.0 ],
									"outlettype" : [ "", "" ],
									"patching_rect" : [ 293.0, 284.5, 199.0, 35.0 ],
									"varname" : "bbdmi.~2list",
									"viewvisibility" : 1
								}

							}
, 							{
								"box" : 								{
									"args" : [ 4 ],
									"bgmode" : 0,
									"border" : 1,
									"clickthrough" : 0,
									"enablehscroll" : 0,
									"enablevscroll" : 0,
									"id" : "obj-2",
									"lockeddragscroll" : 0,
									"lockedsize" : 0,
									"maxclass" : "bpatcher",
									"name" : "bbdmi.list2~.maxpat",
									"numinlets" : 2,
									"numoutlets" : 2,
									"offset" : [ 0.0, 0.0 ],
									"outlettype" : [ "multichannelsignal", "" ],
									"patching_rect" : [ 814.5, 381.5, 199.0, 35.0 ],
									"varname" : "bbdmi.list2~",
									"viewvisibility" : 1
								}

							}
, 							{
								"box" : 								{
									"annotation" : "",
									"args" : [ 4 ],
									"bgmode" : 0,
									"border" : 1,
									"clickthrough" : 0,
									"enablehscroll" : 0,
									"enablevscroll" : 0,
									"hint" : "",
									"id" : "obj-57",
									"lockeddragscroll" : 0,
									"lockedsize" : 0,
									"maxclass" : "bpatcher",
									"name" : "bbdmi.eavi.maxpat",
									"numinlets" : 1,
									"numoutlets" : 2,
									"offset" : [ 0.0, 0.0 ],
									"outlettype" : [ "", "" ],
									"patching_rect" : [ 814.5, 43.0, 199.0, 88.0 ],
									"varname" : "bbdmi.eavi",
									"viewvisibility" : 1
								}

							}
, 							{
								"box" : 								{
									"bubble" : 1,
									"id" : "obj-47",
									"linecount" : 2,
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 860.5, 145.0, 126.0, 37.0 ],
									"text" : "choose ExG input and turn on/off"
								}

							}
, 							{
								"box" : 								{
									"bgcolor" : [ 1.0, 0.788235, 0.470588, 1.0 ],
									"fontname" : "Arial Bold",
									"hint" : "",
									"id" : "obj-52",
									"ignoreclick" : 1,
									"legacytextcolor" : 1,
									"maxclass" : "textbutton",
									"numinlets" : 1,
									"numoutlets" : 3,
									"outlettype" : [ "", "", "int" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 834.0, 153.5, 20.0, 20.0 ],
									"rounded" : 60.0,
									"text" : "3",
									"textcolor" : [ 0.34902, 0.34902, 0.34902, 1.0 ]
								}

							}
, 							{
								"box" : 								{
									"fontsize" : 12.0,
									"id" : "obj-8",
									"linecount" : 2,
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 34.0, 43.0, 128.0, 33.0 ],
									"text" : "• Simulate EMG signal\n  (e.g. 4 CHANNELS)"
								}

							}
, 							{
								"box" : 								{
									"fontsize" : 14.0,
									"id" : "obj-4",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 34.0, 21.0, 50.0, 22.0 ],
									"text" : "INPUT",
									"underline" : 1
								}

							}
, 							{
								"box" : 								{
									"arrows" : 2,
									"border" : 2.0,
									"id" : "obj-80",
									"maxclass" : "live.line",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 27.0, 43.0, 11.0, 88.0 ]
								}

							}
, 							{
								"box" : 								{
									"annotation" : "",
									"args" : [ 4 ],
									"bgmode" : 0,
									"border" : 1,
									"clickthrough" : 0,
									"enablehscroll" : 0,
									"enablevscroll" : 0,
									"hint" : "",
									"id" : "obj-6",
									"lockeddragscroll" : 0,
									"lockedsize" : 0,
									"maxclass" : "bpatcher",
									"name" : "bbdmi_emg_simulator~.maxpat",
									"numinlets" : 1,
									"numoutlets" : 2,
									"offset" : [ 0.0, 0.0 ],
									"outlettype" : [ "multichannelsignal", "" ],
									"patching_rect" : [ 177.0, 43.0, 198.0, 140.549999999999955 ],
									"varname" : "bbdmi.rand~",
									"viewvisibility" : 1
								}

							}
, 							{
								"box" : 								{
									"bubble" : 1,
									"id" : "obj-58",
									"linecount" : 3,
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 381.0, 540.5, 149.0, 51.0 ],
									"text" : "(optional) adjust window size for more or less responsiveness"
								}

							}
, 							{
								"box" : 								{
									"bubble" : 1,
									"id" : "obj-31",
									"linecount" : 4,
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 868.0, 1417.0, 229.0, 64.0 ],
									"text" : "(otional) set /adjust sound synthesis \nparameters and forward back to Max\n(MIDI and/or OSC)\n(toggle a number to freeze parameter)"
								}

							}
, 							{
								"box" : 								{
									"bubble" : 1,
									"id" : "obj-15",
									"linecount" : 2,
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 383.0, 1164.5, 140.0, 37.0 ],
									"text" : "(optional) set / adjust parameters range"
								}

							}
, 							{
								"box" : 								{
									"fontsize" : 12.0,
									"id" : "obj-42",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 854.375, 1344.924999999999955, 216.0, 20.0 ],
									"text" : " • Forward values to Max, MIDI or OSC"
								}

							}
, 							{
								"box" : 								{
									"fontsize" : 14.0,
									"id" : "obj-43",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 854.375, 1320.924999999999955, 66.0, 22.0 ],
									"text" : "OUTPUT",
									"underline" : 1
								}

							}
, 							{
								"box" : 								{
									"arrows" : 2,
									"border" : 2.0,
									"id" : "obj-44",
									"maxclass" : "live.line",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 847.5, 1344.924999999999955, 11.0, 162.099999999999909 ]
								}

							}
, 							{
								"box" : 								{
									"annotation" : "",
									"args" : [ 4 ],
									"bgmode" : 0,
									"border" : 1,
									"clickthrough" : 0,
									"enablehscroll" : 0,
									"enablevscroll" : 0,
									"hint" : "",
									"id" : "obj-41",
									"lockeddragscroll" : 0,
									"lockedsize" : 0,
									"maxclass" : "bpatcher",
									"name" : "bbdmi.2osc.maxpat",
									"numinlets" : 2,
									"numoutlets" : 0,
									"offset" : [ 0.0, 0.0 ],
									"patching_rect" : [ 606.5, 1344.924999999999955, 199.0, 162.099999999999909 ],
									"varname" : "bbdmi.2osc",
									"viewvisibility" : 1
								}

							}
, 							{
								"box" : 								{
									"annotation" : "",
									"args" : [ 4 ],
									"bgmode" : 0,
									"border" : 1,
									"clickthrough" : 0,
									"enablehscroll" : 0,
									"enablevscroll" : 0,
									"hint" : "",
									"id" : "obj-40",
									"lockeddragscroll" : 0,
									"lockedsize" : 0,
									"maxclass" : "bpatcher",
									"name" : "bbdmi.2midi.maxpat",
									"numinlets" : 2,
									"numoutlets" : 0,
									"offset" : [ 0.0, 0.0 ],
									"patching_rect" : [ 390.75, 1344.924999999999955, 199.0, 162.099999999999909 ],
									"varname" : "bbdmi.2midi",
									"viewvisibility" : 1
								}

							}
, 							{
								"box" : 								{
									"annotation" : "",
									"args" : [ 4 ],
									"bgmode" : 0,
									"border" : 1,
									"clickthrough" : 0,
									"enablehscroll" : 0,
									"enablevscroll" : 0,
									"hint" : "",
									"id" : "obj-37",
									"lockeddragscroll" : 0,
									"lockedsize" : 0,
									"maxclass" : "bpatcher",
									"name" : "bbdmi.2max.maxpat",
									"numinlets" : 1,
									"numoutlets" : 0,
									"offset" : [ 0.0, 0.0 ],
									"patching_rect" : [ 177.0, 1344.924999999999955, 199.0, 162.099999999999909 ],
									"varname" : "bbdmi.2max",
									"viewvisibility" : 1
								}

							}
, 							{
								"box" : 								{
									"candycane" : 8,
									"id" : "obj-29",
									"maxclass" : "multislider",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 293.0, 346.0, 199.0, 106.0 ],
									"setstyle" : 3,
									"size" : 4
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-28",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 390.75, 1533.0, 150.0, 20.0 ]
								}

							}
, 							{
								"box" : 								{
									"bubble" : 1,
									"id" : "obj-18",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 1154.5, 266.975000000000364, 85.0, 24.0 ],
									"text" : "load preset"
								}

							}
, 							{
								"box" : 								{
									"bgcolor" : [ 1.0, 0.788235, 0.470588, 1.0 ],
									"fontname" : "Arial Bold",
									"hint" : "",
									"id" : "obj-25",
									"ignoreclick" : 1,
									"legacytextcolor" : 1,
									"maxclass" : "textbutton",
									"numinlets" : 1,
									"numoutlets" : 3,
									"outlettype" : [ "", "", "int" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 1127.0, 268.975000000000364, 20.0, 20.0 ],
									"rounded" : 60.0,
									"text" : "2",
									"textcolor" : [ 0.34902, 0.34902, 0.34902, 1.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-19",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1018.0, 194.475000000000364, 63.0, 22.0 ],
									"text" : "writeagain"
								}

							}
, 							{
								"box" : 								{
									"bubblesize" : 20,
									"id" : "obj-17",
									"maxclass" : "preset",
									"numinlets" : 1,
									"numoutlets" : 5,
									"outlettype" : [ "preset", "int", "preset", "int", "" ],
									"patching_rect" : [ 1018.0, 264.475000000000364, 100.0, 29.0 ],
									"pattrstorage" : "landing-2a"
								}

							}
, 							{
								"box" : 								{
									"autorestore" : "landing-2a.json",
									"id" : "obj-3",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1018.0, 227.5, 313.0, 22.0 ],
									"saved_object_attributes" : 									{
										"client_rect" : [ 100, 100, 500, 600 ],
										"parameter_enable" : 0,
										"parameter_mappable" : 0,
										"storage_rect" : [ 200, 200, 800, 500 ]
									}
,
									"text" : "pattrstorage landing-2a @autorestore 1 @changemode 1",
									"varname" : "landing-2a"
								}

							}
, 							{
								"box" : 								{
									"bubble" : 1,
									"id" : "obj-27",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 1157.5, 75.0, 72.0, 24.0 ],
									"text" : "start dsp"
								}

							}
, 							{
								"box" : 								{
									"bgcolor" : [ 1.0, 0.788235, 0.470588, 1.0 ],
									"fontname" : "Arial Bold",
									"hint" : "",
									"id" : "obj-35",
									"ignoreclick" : 1,
									"legacytextcolor" : 1,
									"maxclass" : "textbutton",
									"numinlets" : 1,
									"numoutlets" : 3,
									"outlettype" : [ "", "", "int" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 1130.0, 77.0, 20.0, 20.0 ],
									"rounded" : 60.0,
									"text" : "1",
									"textcolor" : [ 0.34902, 0.34902, 0.34902, 1.0 ]
								}

							}
, 							{
								"box" : 								{
									"arrows" : 2,
									"border" : 2.0,
									"id" : "obj-24",
									"maxclass" : "live.line",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 634.5, 508.682142857142708, 10.0, 87.0 ]
								}

							}
, 							{
								"box" : 								{
									"fontsize" : 12.0,
									"id" : "obj-12",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 641.5, 1076.05714285714248, 312.0, 20.0 ],
									"text" : " • Scale parameteres to adapt to sound synthesis module"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-11",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 641.5, 759.803571428571331, 370.0, 20.0 ],
									"text" : " • Calibrate dynamic minimum–maximum input over a window of 10''"
								}

							}
, 							{
								"box" : 								{
									"fontsize" : 12.0,
									"id" : "obj-10",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 641.5, 508.682142857142708, 310.0, 20.0 ],
									"text" : "• Calculate root-mean-square (RMS) on a sliding window"
								}

							}
, 							{
								"box" : 								{
									"fontsize" : 14.0,
									"id" : "obj-1",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 641.5, 484.682142857142708, 167.0, 22.0 ],
									"text" : "FEATURE EXTRACTION",
									"underline" : 1
								}

							}
, 							{
								"box" : 								{
									"fontsize" : 14.0,
									"id" : "obj-36",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 641.5, 735.803571428571331, 174.0, 22.0 ],
									"text" : "CONTROL PROCESSING",
									"underline" : 1
								}

							}
, 							{
								"box" : 								{
									"annotation" : "",
									"args" : [ 4 ],
									"bgmode" : 0,
									"border" : 1,
									"clickthrough" : 0,
									"enablehscroll" : 0,
									"enablevscroll" : 0,
									"hint" : "",
									"id" : "obj-83",
									"lockeddragscroll" : 0,
									"lockedsize" : 0,
									"maxclass" : "bpatcher",
									"name" : "bbdmi.scale.maxpat",
									"numinlets" : 1,
									"numoutlets" : 1,
									"offset" : [ 0.0, 0.0 ],
									"outlettype" : [ "" ],
									"patching_rect" : [ 177.0, 1076.924999999999955, 199.0, 162.099999999999909 ],
									"varname" : "bbdmi.scale",
									"viewvisibility" : 1
								}

							}
, 							{
								"box" : 								{
									"arrows" : 2,
									"border" : 2.0,
									"id" : "obj-90",
									"maxclass" : "live.line",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 634.5, 759.803571428571331, 10.8125, 479.221428571428532 ]
								}

							}
, 							{
								"box" : 								{
									"candycane" : 8,
									"id" : "obj-87",
									"maxclass" : "multislider",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 293.0, 622.242857142857019, 199.0, 106.0 ],
									"setminmax" : [ 0.0, 1.0 ],
									"setstyle" : 3,
									"size" : 4
								}

							}
, 							{
								"box" : 								{
									"candycane" : 8,
									"contdata" : 1,
									"id" : "obj-82",
									"maxclass" : "multislider",
									"numinlets" : 1,
									"numoutlets" : 2,
									"orientation" : 0,
									"outlettype" : [ "", "" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 289.5, 948.364285714285643, 199.0, 106.0 ],
									"setminmax" : [ 0.0, 1.0 ],
									"setstyle" : 1,
									"signed" : 1,
									"size" : 4,
									"spacing" : 2
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-213",
									"local" : 1,
									"maxclass" : "ezdac~",
									"numinlets" : 2,
									"numoutlets" : 0,
									"patching_rect" : [ 1063.0, 59.75, 54.5, 54.5 ]
								}

							}
, 							{
								"box" : 								{
									"annotation" : "",
									"args" : [ 4, "@time", 10, "@polarity", 0 ],
									"bgmode" : 0,
									"border" : 1,
									"clickthrough" : 0,
									"enablehscroll" : 0,
									"enablevscroll" : 0,
									"hint" : "",
									"id" : "obj-14",
									"lockeddragscroll" : 0,
									"lockedsize" : 0,
									"maxclass" : "bpatcher",
									"name" : "bbdmi.calibrate.maxpat",
									"numinlets" : 2,
									"numoutlets" : 1,
									"offset" : [ 0.0, 0.0 ],
									"outlettype" : [ "" ],
									"patching_rect" : [ 177.0, 759.803571428571331, 199.0, 162.0 ],
									"varname" : "bbdmi.calibrate",
									"viewvisibility" : 1
								}

							}
, 							{
								"box" : 								{
									"args" : [ 4, "@winsize", 0.5, "@clock", 10 ],
									"bgmode" : 0,
									"border" : 1,
									"clickthrough" : 0,
									"enablehscroll" : 0,
									"enablevscroll" : 0,
									"id" : "obj-5",
									"lockeddragscroll" : 0,
									"lockedsize" : 0,
									"maxclass" : "bpatcher",
									"name" : "bbdmi.rms~.maxpat",
									"numinlets" : 2,
									"numoutlets" : 1,
									"offset" : [ 0.0, 0.0 ],
									"outlettype" : [ "" ],
									"patching_rect" : [ 177.0, 508.682142857142708, 198.0, 87.0 ],
									"varname" : "bbdmi.rms",
									"viewvisibility" : 1
								}

							}
 ],
						"lines" : [ 							{
								"patchline" : 								{
									"destination" : [ "obj-23", 0 ],
									"midpoints" : [ 611.5, 270.0, 302.5, 270.0 ],
									"order" : 1,
									"source" : [ "obj-13", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-5", 0 ],
									"midpoints" : [ 611.5, 270.0, 186.5, 270.0 ],
									"order" : 0,
									"source" : [ "obj-13", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-82", 0 ],
									"midpoints" : [ 186.5, 933.0, 299.0, 933.0 ],
									"order" : 0,
									"source" : [ "obj-14", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-83", 0 ],
									"midpoints" : [ 186.5, 924.0, 186.5, 924.0 ],
									"order" : 1,
									"source" : [ "obj-14", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-3", 0 ],
									"midpoints" : [ 1027.5, 219.0, 1027.5, 219.0 ],
									"source" : [ "obj-19", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-5", 0 ],
									"midpoints" : [ 824.0, 471.0, 186.5, 471.0 ],
									"source" : [ "obj-2", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-29", 0 ],
									"midpoints" : [ 302.5, 321.0, 302.5, 321.0 ],
									"source" : [ "obj-23", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-14", 0 ],
									"midpoints" : [ 186.5, 597.0, 186.5, 597.0 ],
									"order" : 1,
									"source" : [ "obj-5", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-87", 0 ],
									"midpoints" : [ 186.5, 609.0, 302.5, 609.0 ],
									"order" : 0,
									"source" : [ "obj-5", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-2", 0 ],
									"midpoints" : [ 824.0, 132.0, 824.0, 132.0 ],
									"order" : 1,
									"source" : [ "obj-57", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-29", 0 ],
									"midpoints" : [ 824.0, 333.0, 302.5, 333.0 ],
									"order" : 0,
									"source" : [ "obj-57", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-23", 0 ],
									"midpoints" : [ 186.5, 270.0, 302.5, 270.0 ],
									"order" : 1,
									"source" : [ "obj-6", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-5", 0 ],
									"midpoints" : [ 186.5, 186.0, 186.5, 186.0 ],
									"order" : 0,
									"source" : [ "obj-6", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-37", 0 ],
									"midpoints" : [ 186.5, 1242.0, 186.5, 1242.0 ],
									"order" : 2,
									"source" : [ "obj-83", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-40", 0 ],
									"midpoints" : [ 186.5, 1329.0, 400.25, 1329.0 ],
									"order" : 1,
									"source" : [ "obj-83", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-41", 0 ],
									"midpoints" : [ 186.5, 1272.0, 616.0, 1272.0 ],
									"order" : 0,
									"source" : [ "obj-83", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-23", 0 ],
									"midpoints" : [ 399.0, 270.0, 302.5, 270.0 ],
									"order" : 1,
									"source" : [ "obj-9", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-5", 0 ],
									"midpoints" : [ 399.0, 270.0, 186.5, 270.0 ],
									"order" : 0,
									"source" : [ "obj-9", 0 ]
								}

							}
 ]
					}
,
					"patching_rect" : [ 32.0, 65.6875, 71.0, 22.0 ],
					"saved_object_attributes" : 					{
						"description" : "",
						"digest" : "",
						"globalpatchername" : "",
						"tags" : ""
					}
,
					"text" : "p bpatchers",
					"varname" : "demo[1]"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-6",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 0,
					"patcher" : 					{
						"fileversion" : 1,
						"appversion" : 						{
							"major" : 8,
							"minor" : 5,
							"revision" : 6,
							"architecture" : "x64",
							"modernui" : 1
						}
,
						"classnamespace" : "box",
						"rect" : [ 34.0, 92.0, 1370.0, 629.0 ],
						"bglocked" : 0,
						"openinpresentation" : 0,
						"default_fontsize" : 12.0,
						"default_fontface" : 0,
						"default_fontname" : "Arial",
						"gridonopen" : 1,
						"gridsize" : [ 15.0, 15.0 ],
						"gridsnaponopen" : 1,
						"objectsnaponopen" : 1,
						"statusbarvisible" : 2,
						"toolbarvisible" : 1,
						"lefttoolbarpinned" : 0,
						"toptoolbarpinned" : 2,
						"righttoolbarpinned" : 0,
						"bottomtoolbarpinned" : 0,
						"toolbars_unpinned_last_save" : 0,
						"tallnewobj" : 0,
						"boxanimatetime" : 200,
						"enablehscroll" : 1,
						"enablevscroll" : 1,
						"devicewidth" : 0.0,
						"description" : "",
						"digest" : "",
						"tags" : "",
						"style" : "",
						"subpatcher_template" : "Default Max 7",
						"showontab" : 2,
						"assistshowspatchername" : 0,
						"boxes" : [ 							{
								"box" : 								{
									"id" : "obj-23",
									"maxclass" : "button",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 591.5, 111.936054421768745, 24.0, 24.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-26",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 591.75, 145.0, 49.0, 22.0 ],
									"text" : "read $1"
								}

							}
, 							{
								"box" : 								{
									"checkedcolor" : [ 0.131302371621132, 0.99969744682312, 0.023593800142407, 1.0 ],
									"id" : "obj-30",
									"maxclass" : "toggle",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 512.75, 111.936054421768745, 24.0, 24.0 ],
									"uncheckedcolor" : [ 0.986251831054688, 0.007236152887344, 0.027423052117229, 1.0 ],
									"varname" : "textbutton"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-31",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 512.75, 145.0, 73.0, 22.0 ],
									"text" : "playback $1"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 12.0,
									"id" : "obj-22",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "multichannelsignal", "" ],
									"patching_rect" : [ 514.75, 194.0, 96.0, 22.0 ],
									"text" : "bbdmi.recorder~",
									"varname" : "bbdmi.rand~[3]"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-8",
									"maxclass" : "button",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 428.296192902326538, 111.936054421768745, 24.0, 24.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-13",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 356.75, 145.0, 52.0, 22.0 ],
									"text" : "onoff $1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-14",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 428.296192902326538, 145.0, 49.0, 22.0 ],
									"text" : "rand $1"
								}

							}
, 							{
								"box" : 								{
									"checkedcolor" : [ 0.131302371621132, 0.99969744682312, 0.023593800142407, 1.0 ],
									"id" : "obj-16",
									"maxclass" : "toggle",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 356.75, 111.936054421768745, 24.0, 24.0 ],
									"uncheckedcolor" : [ 0.986251831054688, 0.007236152887344, 0.027423052117229, 1.0 ],
									"varname" : "textbutton[2]"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 12.0,
									"id" : "obj-20",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "multichannelsignal", "" ],
									"patching_rect" : [ 356.75, 194.0, 140.0, 22.0 ],
									"text" : "bbdmi_eeg_simulator~ 4",
									"varname" : "bbdmi.rand~[2]"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-10",
									"maxclass" : "button",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 260.296192902326538, 111.936054421768745, 24.0, 24.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-4",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 0,
									"patching_rect" : [ 376.842385804653077, 565.125, 80.0, 22.0 ],
									"text" : "bbdmi.2osc 4",
									"varname" : "bbdmi.2max[2]"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-1",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 0,
									"patching_rect" : [ 283.75, 565.125, 83.0, 22.0 ],
									"text" : "bbdmi.2midi 4",
									"varname" : "bbdmi.2max[1]"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-7",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 188.75, 145.0, 52.0, 22.0 ],
									"text" : "onoff $1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-74",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 834.489029037952491, 145.0, 60.0, 22.0 ],
									"text" : "device $1"
								}

							}
, 							{
								"box" : 								{
									"fontsize" : 12.0,
									"id" : "obj-78",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 801.5, 47.436054421768745, 70.0, 22.0 ],
									"text" : "loadmess 0"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 12.0,
									"id" : "obj-167",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 769.25, 79.686054421768745, 52.0, 22.0 ],
									"text" : "midiinfo"
								}

							}
, 							{
								"box" : 								{
									"bgcolor" : [ 0.168627450980392, 0.168627450980392, 0.168627450980392, 1.0 ],
									"bgfillcolor_angle" : 270.0,
									"bgfillcolor_autogradient" : 0.0,
									"bgfillcolor_color" : [ 0.168627450980392, 0.168627450980392, 0.168627450980392, 1.0 ],
									"bgfillcolor_color1" : [ 0.301961, 0.301961, 0.301961, 1.0 ],
									"bgfillcolor_color2" : [ 0.2, 0.2, 0.2, 1.0 ],
									"bgfillcolor_proportion" : 0.5,
									"bgfillcolor_type" : "color",
									"fontsize" : 12.0,
									"id" : "obj-169",
									"items" : [ "IAC Driver Bus 1", ",", "to Max 1", ",", "to Max 2" ],
									"maxclass" : "umenu",
									"numinlets" : 1,
									"numoutlets" : 3,
									"outlettype" : [ "int", "", "" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 769.25, 111.936054421768745, 149.478058075904983, 22.0 ],
									"presentation" : 1,
									"presentation_rect" : [ 21.649765342473984, 45.371006045395916, 161.33571882545948, 22.0 ]
								}

							}
, 							{
								"box" : 								{
									"checkedcolor" : [ 0.131302371621132, 0.99969744682312, 0.023593800142407, 1.0 ],
									"id" : "obj-79",
									"maxclass" : "toggle",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 685.75, 111.936054421768745, 24.0, 24.0 ],
									"uncheckedcolor" : [ 0.986251831054688, 0.007236152887344, 0.027423052117229, 1.0 ],
									"varname" : "textbutton[3]"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-2",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 685.75, 145.0, 52.0, 22.0 ],
									"text" : "onoff $1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-82",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "multichannelsignal", "" ],
									"patching_rect" : [ 685.75, 228.0, 83.0, 22.0 ],
									"text" : "bbdmi.list2~ 4",
									"varname" : "bbdmi.list2~[1]"
								}

							}
, 							{
								"box" : 								{
									"color" : [ 0.059509769082069, 0.501929938793182, 0.998454749584198, 1.0 ],
									"fontname" : "Arial",
									"fontsize" : 12.0,
									"id" : "obj-25",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "" ],
									"patching_rect" : [ 685.75, 194.0, 67.0, 22.0 ],
									"text" : "bbdmi.eavi",
									"varname" : "bbdmi.rand~[1]"
								}

							}
, 							{
								"box" : 								{
									"bubble" : 1,
									"bubbleside" : 2,
									"id" : "obj-21",
									"linecount" : 2,
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 114.5, 47.436054421768745, 109.0, 52.0 ],
									"text" : "choose ExG input and turn on/off",
									"textjustification" : 1
								}

							}
, 							{
								"box" : 								{
									"bgcolor" : [ 1.0, 0.788235, 0.470588, 1.0 ],
									"fontname" : "Arial Bold",
									"hint" : "",
									"id" : "obj-52",
									"ignoreclick" : 1,
									"legacytextcolor" : 1,
									"maxclass" : "textbutton",
									"numinlets" : 1,
									"numoutlets" : 3,
									"outlettype" : [ "", "", "int" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 159.0, 113.936054421768745, 20.0, 20.0 ],
									"rounded" : 60.0,
									"text" : "3",
									"textcolor" : [ 0.34902, 0.34902, 0.34902, 1.0 ]
								}

							}
, 							{
								"box" : 								{
									"fontsize" : 12.0,
									"id" : "obj-84",
									"linecount" : 2,
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 40.0, 195.0, 128.0, 33.0 ],
									"text" : "• Simulate EMG signal\n  (e.g. 4 CHANNELS)"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-28",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 260.296192902326538, 145.0, 49.0, 22.0 ],
									"text" : "rand $1"
								}

							}
, 							{
								"box" : 								{
									"checkedcolor" : [ 0.131302371621132, 0.99969744682312, 0.023593800142407, 1.0 ],
									"id" : "obj-114",
									"maxclass" : "toggle",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 188.75, 111.936054421768745, 24.0, 24.0 ],
									"uncheckedcolor" : [ 0.986251831054688, 0.007236152887344, 0.027423052117229, 1.0 ],
									"varname" : "textbutton[1]"
								}

							}
, 							{
								"box" : 								{
									"arrows" : 2,
									"border" : 2.0,
									"id" : "obj-89",
									"maxclass" : "live.line",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 27.0, 195.0, 11.0, 37.75 ]
								}

							}
, 							{
								"box" : 								{
									"fontsize" : 14.0,
									"id" : "obj-92",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 40.0, 175.0, 50.0, 22.0 ],
									"text" : "INPUT",
									"underline" : 1
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 12.0,
									"id" : "obj-18",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "multichannelsignal", "" ],
									"patching_rect" : [ 188.75, 194.0, 144.0, 22.0 ],
									"text" : "bbdmi_emg_simulator~ 4",
									"varname" : "bbdmi.rand~"
								}

							}
, 							{
								"box" : 								{
									"angle" : 270.0,
									"bgcolor" : [ 0.662745098039216, 0.737254901960784, 1.0, 0.5 ],
									"border" : 1,
									"bordercolor" : [ 0.0, 0.0, 0.0, 1.0 ],
									"id" : "obj-93",
									"maxclass" : "panel",
									"mode" : 0,
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 672.25, 32.6875, 260.0, 230.0 ],
									"proportion" : 0.5,
									"rounded" : 0
								}

							}
, 							{
								"box" : 								{
									"fontsize" : 12.0,
									"id" : "obj-70",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 970.0, 565.125, 216.0, 20.0 ],
									"text" : " • Forward values to Max, MIDI or OSC"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-67",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 188.75, 565.125, 84.0, 22.0 ],
									"text" : "bbdmi.2max 4",
									"varname" : "bbdmi.2max"
								}

							}
, 							{
								"box" : 								{
									"fontsize" : 12.0,
									"id" : "obj-12",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 970.0, 514.03125, 312.0, 20.0 ],
									"text" : " • Scale parameteres to adapt to sound synthesis module"
								}

							}
, 							{
								"box" : 								{
									"fontsize" : 12.0,
									"id" : "obj-66",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 970.0, 411.25, 310.0, 20.0 ],
									"text" : "• Calculate root-mean-square (RMS) on a sliding window"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-64",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 970.0, 463.541666666666742, 370.0, 20.0 ],
									"text" : " • Calibrate dynamic minimum–maximum input over a window of 10''"
								}

							}
, 							{
								"box" : 								{
									"bubble" : 1,
									"bubbleside" : 3,
									"id" : "obj-58",
									"linecount" : 3,
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 218.75, 321.262500000000045, 158.0, 51.0 ],
									"text" : "(optional) adjust window size for more or less responsiveness"
								}

							}
, 							{
								"box" : 								{
									"bubble" : 1,
									"id" : "obj-39",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 1093.5, 210.75, 72.0, 24.0 ],
									"text" : "start dsp"
								}

							}
, 							{
								"box" : 								{
									"bgcolor" : [ 1.0, 0.788235, 0.470588, 1.0 ],
									"fontname" : "Arial Bold",
									"hint" : "",
									"id" : "obj-48",
									"ignoreclick" : 1,
									"legacytextcolor" : 1,
									"maxclass" : "textbutton",
									"numinlets" : 1,
									"numoutlets" : 3,
									"outlettype" : [ "", "", "int" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 1066.0, 212.75, 20.0, 20.0 ],
									"rounded" : 60.0,
									"text" : "1",
									"textcolor" : [ 0.34902, 0.34902, 0.34902, 1.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-213",
									"local" : 1,
									"maxclass" : "ezdac~",
									"numinlets" : 2,
									"numoutlets" : 0,
									"patching_rect" : [ 1003.5, 195.5, 54.5, 54.5 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-47",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 4,
									"outlettype" : [ "", "", "", "" ],
									"patching_rect" : [ 1183.0, 32.6875, 56.0, 22.0 ],
									"restore" : 									{
										"interval" : [ 10 ],
										"textbutton" : [ 0 ],
										"textbutton[1]" : [ 0 ],
										"textbutton[2]" : [ 0 ],
										"textbutton[3]" : [ 0 ],
										"winsize" : [ 0.5 ]
									}
,
									"text" : "autopattr",
									"varname" : "u097009114"
								}

							}
, 							{
								"box" : 								{
									"bubble" : 1,
									"id" : "obj-42",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 1093.5, 110.436054421768745, 85.0, 24.0 ],
									"text" : "load preset"
								}

							}
, 							{
								"box" : 								{
									"bgcolor" : [ 1.0, 0.788235, 0.470588, 1.0 ],
									"fontname" : "Arial Bold",
									"hint" : "",
									"id" : "obj-43",
									"ignoreclick" : 1,
									"legacytextcolor" : 1,
									"maxclass" : "textbutton",
									"numinlets" : 1,
									"numoutlets" : 3,
									"outlettype" : [ "", "", "int" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 1066.0, 112.436054421768745, 20.0, 20.0 ],
									"rounded" : 60.0,
									"text" : "2",
									"textcolor" : [ 0.34902, 0.34902, 0.34902, 1.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-44",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 958.0, 32.6875, 63.0, 22.0 ],
									"text" : "writeagain"
								}

							}
, 							{
								"box" : 								{
									"bubblesize" : 20,
									"id" : "obj-45",
									"maxclass" : "preset",
									"numinlets" : 1,
									"numoutlets" : 5,
									"outlettype" : [ "preset", "int", "preset", "int", "" ],
									"patching_rect" : [ 958.0, 107.936054421768745, 100.0, 28.0 ]
								}

							}
, 							{
								"box" : 								{
									"autorestore" : "landing-1a.json",
									"id" : "obj-46",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 958.0, 64.9375, 313.0, 22.0 ],
									"saved_object_attributes" : 									{
										"client_rect" : [ 100, 100, 500, 600 ],
										"parameter_enable" : 0,
										"parameter_mappable" : 0,
										"storage_rect" : [ 200, 200, 800, 500 ]
									}
,
									"text" : "pattrstorage landing-1a @autorestore 1 @changemode 1",
									"varname" : "landing-1a"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-36",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 535.75, 369.762500000000045, 53.0, 22.0 ],
									"text" : "clock $1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-35",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 385.75, 369.762500000000045, 65.0, 22.0 ],
									"text" : "winsize $1"
								}

							}
, 							{
								"box" : 								{
									"fontface" : 0,
									"id" : "obj-32",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 589.785871398448762, 336.69855442176879, 101.0, 20.0 ],
									"text" : "clock inteval (ms)"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-34",
									"maxclass" : "number",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 535.75, 334.69855442176879, 50.0, 22.0 ],
									"varname" : "interval"
								}

							}
, 							{
								"box" : 								{
									"format" : 6,
									"id" : "obj-33",
									"maxclass" : "flonum",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 386.416666666666629, 335.69855442176879, 50.0, 22.0 ],
									"varname" : "winsize"
								}

							}
, 							{
								"box" : 								{
									"fontface" : 0,
									"id" : "obj-76",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 438.416666666666629, 336.69855442176879, 91.0, 20.0 ],
									"text" : "window size (s)"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-24",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 188.75, 513.833333333333371, 83.0, 22.0 ],
									"text" : "bbdmi.scale 4",
									"varname" : "bbdmi.scale"
								}

							}
, 							{
								"box" : 								{
									"arrows" : 2,
									"border" : 2.0,
									"id" : "obj-9",
									"maxclass" : "live.line",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 958.0, 387.25, 10.0, 199.875 ]
								}

							}
, 							{
								"box" : 								{
									"fontsize" : 14.0,
									"id" : "obj-11",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 970.0, 545.1875, 66.0, 22.0 ],
									"text" : "OUTPUT",
									"underline" : 1
								}

							}
, 							{
								"box" : 								{
									"fontsize" : 14.0,
									"id" : "obj-15",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 970.0, 442.366666666666788, 174.0, 22.0 ],
									"text" : "CONTROL PROCESSING",
									"underline" : 1
								}

							}
, 							{
								"box" : 								{
									"fontsize" : 14.0,
									"id" : "obj-6",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 970.0, 387.25, 167.0, 22.0 ],
									"text" : "FEATURE EXTRACTION",
									"underline" : 1
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-17",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 188.75, 462.541666666666742, 219.0, 22.0 ],
									"text" : "bbdmi.calibrate 4 @time 10 @polarity 0",
									"varname" : "bbdmi.autocalibrate"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-19",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 188.75, 411.25, 216.0, 22.0 ],
									"text" : "bbdmi.rms~ 4 @winsize 0.5 @clock 10",
									"varname" : "bbdmi.rms~"
								}

							}
, 							{
								"box" : 								{
									"angle" : 270.0,
									"bgcolor" : [ 1.0, 0.992156862745098, 0.717647058823529, 0.5 ],
									"border" : 1,
									"bordercolor" : [ 0.0, 0.0, 0.0, 1.0 ],
									"id" : "obj-3",
									"maxclass" : "panel",
									"mode" : 0,
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 176.75, 309.116666666666788, 522.583333333333485, 288.645833333333258 ],
									"proportion" : 0.5,
									"rounded" : 0
								}

							}
 ],
						"lines" : [ 							{
								"patchline" : 								{
									"destination" : [ "obj-28", 0 ],
									"midpoints" : [ 269.796192902326538, 138.0, 269.796192902326538, 138.0 ],
									"source" : [ "obj-10", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-7", 0 ],
									"midpoints" : [ 198.25, 138.0, 198.25, 138.0 ],
									"source" : [ "obj-114", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-20", 0 ],
									"midpoints" : [ 366.25, 168.0, 366.25, 168.0 ],
									"source" : [ "obj-13", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-20", 0 ],
									"midpoints" : [ 437.796192902326538, 180.0, 366.25, 180.0 ],
									"source" : [ "obj-14", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-13", 0 ],
									"midpoints" : [ 366.25, 138.0, 366.25, 138.0 ],
									"source" : [ "obj-16", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-169", 0 ],
									"midpoints" : [ 778.75, 102.0, 778.75, 102.0 ],
									"source" : [ "obj-167", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-74", 0 ],
									"midpoints" : [ 843.989029037952491, 135.0, 843.989029037952491, 135.0 ],
									"source" : [ "obj-169", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-24", 0 ],
									"midpoints" : [ 198.25, 486.0, 198.25, 486.0 ],
									"source" : [ "obj-17", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-19", 0 ],
									"midpoints" : [ 198.25, 219.0, 198.25, 219.0 ],
									"source" : [ "obj-18", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-17", 0 ],
									"midpoints" : [ 198.25, 435.0, 198.25, 435.0 ],
									"source" : [ "obj-19", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-25", 0 ],
									"midpoints" : [ 695.25, 168.0, 695.25, 168.0 ],
									"source" : [ "obj-2", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-19", 0 ],
									"midpoints" : [ 366.25, 306.0, 198.25, 306.0 ],
									"source" : [ "obj-20", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-19", 0 ],
									"midpoints" : [ 524.25, 306.0, 198.25, 306.0 ],
									"source" : [ "obj-22", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-26", 0 ],
									"midpoints" : [ 601.0, 138.0, 601.25, 138.0 ],
									"source" : [ "obj-23", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-1", 0 ],
									"midpoints" : [ 198.25, 552.0, 293.25, 552.0 ],
									"order" : 1,
									"source" : [ "obj-24", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-4", 0 ],
									"midpoints" : [ 198.25, 552.0, 386.342385804653077, 552.0 ],
									"order" : 0,
									"source" : [ "obj-24", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-67", 0 ],
									"midpoints" : [ 198.25, 537.0, 198.25, 537.0 ],
									"order" : 2,
									"source" : [ "obj-24", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-82", 0 ],
									"midpoints" : [ 695.25, 219.0, 695.25, 219.0 ],
									"source" : [ "obj-25", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-22", 1 ],
									"midpoints" : [ 601.25, 168.0, 601.25, 168.0 ],
									"source" : [ "obj-26", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-18", 0 ],
									"midpoints" : [ 269.796192902326538, 180.0, 198.25, 180.0 ],
									"source" : [ "obj-28", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-31", 0 ],
									"midpoints" : [ 522.25, 138.0, 522.25, 138.0 ],
									"source" : [ "obj-30", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-22", 1 ],
									"midpoints" : [ 522.25, 180.0, 601.25, 180.0 ],
									"source" : [ "obj-31", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-35", 0 ],
									"midpoints" : [ 395.916666666666629, 360.0, 395.25, 360.0 ],
									"source" : [ "obj-33", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-36", 0 ],
									"midpoints" : [ 545.25, 357.0, 545.25, 357.0 ],
									"source" : [ "obj-34", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-19", 1 ],
									"midpoints" : [ 395.25, 393.0, 395.25, 393.0 ],
									"source" : [ "obj-35", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-19", 1 ],
									"midpoints" : [ 545.25, 405.0, 395.25, 405.0 ],
									"source" : [ "obj-36", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-46", 0 ],
									"midpoints" : [ 967.5, 57.0, 967.5, 57.0 ],
									"source" : [ "obj-44", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-18", 0 ],
									"midpoints" : [ 198.25, 168.0, 198.25, 168.0 ],
									"source" : [ "obj-7", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-25", 0 ],
									"midpoints" : [ 843.989029037952491, 180.0, 695.25, 180.0 ],
									"source" : [ "obj-74", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-167", 1 ],
									"midpoints" : [ 811.0, 72.0, 811.75, 72.0 ],
									"source" : [ "obj-78", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-2", 0 ],
									"midpoints" : [ 695.25, 138.0, 695.25, 138.0 ],
									"source" : [ "obj-79", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-14", 0 ],
									"midpoints" : [ 437.796192902326538, 138.0, 437.796192902326538, 138.0 ],
									"source" : [ "obj-8", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-19", 0 ],
									"midpoints" : [ 695.25, 306.0, 198.25, 306.0 ],
									"source" : [ "obj-82", 0 ]
								}

							}
 ]
					}
,
					"patching_rect" : [ 32.0, 35.0, 83.0, 22.0 ],
					"saved_object_attributes" : 					{
						"description" : "",
						"digest" : "",
						"globalpatchername" : "",
						"tags" : ""
					}
,
					"text" : "p abstractions",
					"varname" : "demo"
				}

			}
 ],
		"lines" : [  ],
		"parameters" : 		{
			"obj-1::obj-14::obj-107::obj-33" : [ "tab[70]", "tab[1]", 0 ],
			"obj-1::obj-14::obj-123::obj-33" : [ "tab[38]", "tab[1]", 0 ],
			"obj-1::obj-14::obj-34::obj-33" : [ "tab[86]", "tab[1]", 0 ],
			"obj-1::obj-14::obj-36::obj-33" : [ "tab[95]", "tab[1]", 0 ],
			"obj-1::obj-14::obj-40::obj-33" : [ "tab[112]", "tab[1]", 0 ],
			"obj-1::obj-14::obj-41::obj-33" : [ "tab[87]", "tab[1]", 0 ],
			"obj-1::obj-14::obj-42::obj-33" : [ "tab[96]", "tab[1]", 0 ],
			"obj-1::obj-14::obj-43::obj-33" : [ "tab[97]", "tab[1]", 0 ],
			"obj-1::obj-14::obj-44::obj-33" : [ "tab[98]", "tab[1]", 0 ],
			"obj-1::obj-14::obj-45::obj-33" : [ "tab[106]", "tab[1]", 0 ],
			"obj-1::obj-14::obj-46::obj-33" : [ "tab[107]", "tab[1]", 0 ],
			"obj-1::obj-14::obj-47::obj-33" : [ "tab[99]", "tab[1]", 0 ],
			"obj-1::obj-14::obj-48::obj-33" : [ "tab[113]", "tab[1]", 0 ],
			"obj-1::obj-14::obj-49::obj-33" : [ "tab[114]", "tab[1]", 0 ],
			"obj-1::obj-14::obj-50::obj-33" : [ "tab[115]", "tab[1]", 0 ],
			"obj-1::obj-14::obj-74::obj-33" : [ "tab[69]", "tab[1]", 0 ],
			"obj-1::obj-37::obj-107::obj-27::obj-18" : [ "toggle[5]", "toggle", 0 ],
			"obj-1::obj-37::obj-107::obj-48" : [ "SendTo-TXT[2]", "SendTo-TXT", 0 ],
			"obj-1::obj-37::obj-107::obj-8" : [ "tab[101]", "tab[1]", 0 ],
			"obj-1::obj-37::obj-123::obj-27::obj-18" : [ "toggle[64]", "toggle", 0 ],
			"obj-1::obj-37::obj-123::obj-48" : [ "SendTo-TXT[43]", "SendTo-TXT", 0 ],
			"obj-1::obj-37::obj-123::obj-8" : [ "tab[141]", "tab[1]", 0 ],
			"obj-1::obj-37::obj-34::obj-27::obj-18" : [ "toggle[9]", "toggle", 0 ],
			"obj-1::obj-37::obj-34::obj-48" : [ "SendTo-TXT[3]", "SendTo-TXT", 0 ],
			"obj-1::obj-37::obj-34::obj-8" : [ "tab[102]", "tab[1]", 0 ],
			"obj-1::obj-37::obj-36::obj-27::obj-18" : [ "toggle[10]", "toggle", 0 ],
			"obj-1::obj-37::obj-36::obj-48" : [ "SendTo-TXT[4]", "SendTo-TXT", 0 ],
			"obj-1::obj-37::obj-36::obj-8" : [ "tab[142]", "tab[1]", 0 ],
			"obj-1::obj-37::obj-40::obj-27::obj-18" : [ "toggle[11]", "toggle", 0 ],
			"obj-1::obj-37::obj-40::obj-48" : [ "SendTo-TXT[5]", "SendTo-TXT", 0 ],
			"obj-1::obj-37::obj-40::obj-8" : [ "tab[37]", "tab[1]", 0 ],
			"obj-1::obj-37::obj-41::obj-27::obj-18" : [ "toggle[12]", "toggle", 0 ],
			"obj-1::obj-37::obj-41::obj-48" : [ "SendTo-TXT[6]", "SendTo-TXT", 0 ],
			"obj-1::obj-37::obj-41::obj-8" : [ "tab[42]", "tab[1]", 0 ],
			"obj-1::obj-37::obj-42::obj-27::obj-18" : [ "toggle[13]", "toggle", 0 ],
			"obj-1::obj-37::obj-42::obj-48" : [ "SendTo-TXT[7]", "SendTo-TXT", 0 ],
			"obj-1::obj-37::obj-42::obj-8" : [ "tab[43]", "tab[1]", 0 ],
			"obj-1::obj-37::obj-43::obj-27::obj-18" : [ "toggle[14]", "toggle", 0 ],
			"obj-1::obj-37::obj-43::obj-48" : [ "SendTo-TXT[8]", "SendTo-TXT", 0 ],
			"obj-1::obj-37::obj-43::obj-8" : [ "tab[143]", "tab[1]", 0 ],
			"obj-1::obj-37::obj-44::obj-27::obj-18" : [ "toggle[15]", "toggle", 0 ],
			"obj-1::obj-37::obj-44::obj-48" : [ "SendTo-TXT[9]", "SendTo-TXT", 0 ],
			"obj-1::obj-37::obj-44::obj-8" : [ "tab[144]", "tab[1]", 0 ],
			"obj-1::obj-37::obj-45::obj-27::obj-18" : [ "toggle[16]", "toggle", 0 ],
			"obj-1::obj-37::obj-45::obj-48" : [ "SendTo-TXT[10]", "SendTo-TXT", 0 ],
			"obj-1::obj-37::obj-45::obj-8" : [ "tab[145]", "tab[1]", 0 ],
			"obj-1::obj-37::obj-46::obj-27::obj-18" : [ "toggle[17]", "toggle", 0 ],
			"obj-1::obj-37::obj-46::obj-48" : [ "SendTo-TXT[11]", "SendTo-TXT", 0 ],
			"obj-1::obj-37::obj-46::obj-8" : [ "tab[146]", "tab[1]", 0 ],
			"obj-1::obj-37::obj-47::obj-27::obj-18" : [ "toggle[18]", "toggle", 0 ],
			"obj-1::obj-37::obj-47::obj-48" : [ "SendTo-TXT[12]", "SendTo-TXT", 0 ],
			"obj-1::obj-37::obj-47::obj-8" : [ "tab[147]", "tab[1]", 0 ],
			"obj-1::obj-37::obj-48::obj-27::obj-18" : [ "toggle[19]", "toggle", 0 ],
			"obj-1::obj-37::obj-48::obj-48" : [ "SendTo-TXT[13]", "SendTo-TXT", 0 ],
			"obj-1::obj-37::obj-48::obj-8" : [ "tab[148]", "tab[1]", 0 ],
			"obj-1::obj-37::obj-49::obj-27::obj-18" : [ "toggle[20]", "toggle", 0 ],
			"obj-1::obj-37::obj-49::obj-48" : [ "SendTo-TXT[14]", "SendTo-TXT", 0 ],
			"obj-1::obj-37::obj-49::obj-8" : [ "tab[45]", "tab[1]", 0 ],
			"obj-1::obj-37::obj-50::obj-27::obj-18" : [ "toggle[21]", "toggle", 0 ],
			"obj-1::obj-37::obj-50::obj-48" : [ "SendTo-TXT[15]", "SendTo-TXT", 0 ],
			"obj-1::obj-37::obj-50::obj-8" : [ "tab[149]", "tab[1]", 0 ],
			"obj-1::obj-37::obj-74::obj-27::obj-18" : [ "toggle[4]", "toggle", 0 ],
			"obj-1::obj-37::obj-74::obj-48" : [ "SendTo-TXT[1]", "SendTo-TXT", 0 ],
			"obj-1::obj-37::obj-74::obj-8" : [ "tab[100]", "tab[1]", 0 ],
			"obj-1::obj-40::obj-107::obj-27::obj-18" : [ "toggle[24]", "toggle", 0 ],
			"obj-1::obj-40::obj-107::obj-8" : [ "tab[152]", "tab[1]", 0 ],
			"obj-1::obj-40::obj-123::obj-27::obj-18" : [ "toggle[22]", "toggle", 0 ],
			"obj-1::obj-40::obj-123::obj-8" : [ "tab[150]", "tab[1]", 0 ],
			"obj-1::obj-40::obj-34::obj-27::obj-18" : [ "toggle[25]", "toggle", 0 ],
			"obj-1::obj-40::obj-34::obj-8" : [ "tab[153]", "tab[1]", 0 ],
			"obj-1::obj-40::obj-37::obj-27::obj-18" : [ "toggle[37]", "toggle", 0 ],
			"obj-1::obj-40::obj-37::obj-8" : [ "tab[161]", "tab[1]", 0 ],
			"obj-1::obj-40::obj-38::obj-27::obj-18" : [ "toggle[35]", "toggle", 0 ],
			"obj-1::obj-40::obj-38::obj-8" : [ "tab[158]", "tab[1]", 0 ],
			"obj-1::obj-40::obj-39::obj-27::obj-18" : [ "toggle[1]", "toggle", 0 ],
			"obj-1::obj-40::obj-39::obj-8" : [ "tab[1]", "tab[1]", 0 ],
			"obj-1::obj-40::obj-40::obj-27::obj-18" : [ "toggle[27]", "toggle", 0 ],
			"obj-1::obj-40::obj-40::obj-8" : [ "tab[155]", "tab[1]", 0 ],
			"obj-1::obj-40::obj-41::obj-27::obj-18" : [ "toggle[28]", "toggle", 0 ],
			"obj-1::obj-40::obj-41::obj-8" : [ "tab[156]", "tab[1]", 0 ],
			"obj-1::obj-40::obj-44::obj-27::obj-18" : [ "toggle[31]", "toggle", 0 ],
			"obj-1::obj-40::obj-44::obj-8" : [ "tab[159]", "tab[1]", 0 ],
			"obj-1::obj-40::obj-45::obj-27::obj-18" : [ "toggle[32]", "toggle", 0 ],
			"obj-1::obj-40::obj-45::obj-8" : [ "tab[160]", "tab[1]", 0 ],
			"obj-1::obj-40::obj-46::obj-27::obj-18" : [ "toggle[33]", "toggle", 0 ],
			"obj-1::obj-40::obj-46::obj-8" : [ "tab[163]", "tab[1]", 0 ],
			"obj-1::obj-40::obj-47::obj-27::obj-18" : [ "toggle[34]", "toggle", 0 ],
			"obj-1::obj-40::obj-47::obj-8" : [ "tab[39]", "tab[1]", 0 ],
			"obj-1::obj-40::obj-48::obj-27::obj-18" : [ "toggle[38]", "toggle", 0 ],
			"obj-1::obj-40::obj-48::obj-8" : [ "tab[40]", "tab[1]", 0 ],
			"obj-1::obj-40::obj-49::obj-27::obj-18" : [ "toggle[36]", "toggle", 0 ],
			"obj-1::obj-40::obj-49::obj-8" : [ "tab[162]", "tab[1]", 0 ],
			"obj-1::obj-40::obj-50::obj-27::obj-18" : [ "toggle[39]", "toggle", 0 ],
			"obj-1::obj-40::obj-50::obj-8" : [ "tab[164]", "tab[1]", 0 ],
			"obj-1::obj-40::obj-74::obj-27::obj-18" : [ "toggle[23]", "toggle", 0 ],
			"obj-1::obj-40::obj-74::obj-8" : [ "tab[151]", "tab[1]", 0 ],
			"obj-1::obj-41::obj-107::obj-8" : [ "tab[166]", "tab[1]", 0 ],
			"obj-1::obj-41::obj-123::obj-8" : [ "tab[165]", "tab[1]", 0 ],
			"obj-1::obj-41::obj-34::obj-8" : [ "tab[167]", "tab[1]", 0 ],
			"obj-1::obj-41::obj-37::obj-8" : [ "tab[171]", "tab[1]", 0 ],
			"obj-1::obj-41::obj-38::obj-8" : [ "tab[170]", "tab[1]", 0 ],
			"obj-1::obj-41::obj-39::obj-8" : [ "tab[157]", "tab[1]", 0 ],
			"obj-1::obj-41::obj-40::obj-8" : [ "tab[168]", "tab[1]", 0 ],
			"obj-1::obj-41::obj-41::obj-8" : [ "tab[169]", "tab[1]", 0 ],
			"obj-1::obj-41::obj-44::obj-8" : [ "tab[172]", "tab[1]", 0 ],
			"obj-1::obj-41::obj-45::obj-8" : [ "tab[173]", "tab[1]", 0 ],
			"obj-1::obj-41::obj-46::obj-8" : [ "tab[174]", "tab[1]", 0 ],
			"obj-1::obj-41::obj-47::obj-8" : [ "tab[175]", "tab[1]", 0 ],
			"obj-1::obj-41::obj-48::obj-8" : [ "tab[176]", "tab[1]", 0 ],
			"obj-1::obj-41::obj-49::obj-8" : [ "tab[177]", "tab[1]", 0 ],
			"obj-1::obj-41::obj-50::obj-8" : [ "tab[178]", "tab[1]", 0 ],
			"obj-1::obj-41::obj-74::obj-8" : [ "tab[154]", "tab[1]", 0 ],
			"obj-1::obj-41::obj-95" : [ "number[162]", "number", 0 ],
			"obj-1::obj-83::obj-107::obj-33" : [ "tab[81]", "tab[1]", 0 ],
			"obj-1::obj-83::obj-123::obj-33" : [ "tab[116]", "tab[1]", 0 ],
			"obj-1::obj-83::obj-34::obj-33" : [ "tab[72]", "tab[1]", 0 ],
			"obj-1::obj-83::obj-36::obj-33" : [ "tab[82]", "tab[1]", 0 ],
			"obj-1::obj-83::obj-40::obj-33" : [ "tab[117]", "tab[1]", 0 ],
			"obj-1::obj-83::obj-41::obj-33" : [ "tab[118]", "tab[1]", 0 ],
			"obj-1::obj-83::obj-42::obj-33" : [ "tab[84]", "tab[1]", 0 ],
			"obj-1::obj-83::obj-43::obj-33" : [ "tab[119]", "tab[1]", 0 ],
			"obj-1::obj-83::obj-44::obj-33" : [ "tab[120]", "tab[1]", 0 ],
			"obj-1::obj-83::obj-45::obj-33" : [ "tab[85]", "tab[1]", 0 ],
			"obj-1::obj-83::obj-46::obj-33" : [ "tab[121]", "tab[1]", 0 ],
			"obj-1::obj-83::obj-47::obj-33" : [ "tab[122]", "tab[1]", 0 ],
			"obj-1::obj-83::obj-48::obj-33" : [ "tab[108]", "tab[1]", 0 ],
			"obj-1::obj-83::obj-49::obj-33" : [ "tab[123]", "tab[1]", 0 ],
			"obj-1::obj-83::obj-50::obj-33" : [ "tab[124]", "tab[1]", 0 ],
			"obj-1::obj-83::obj-74::obj-33" : [ "tab[71]", "tab[1]", 0 ],
			"obj-6::obj-17::obj-107::obj-33" : [ "tab[274]", "tab[1]", 0 ],
			"obj-6::obj-17::obj-123::obj-33" : [ "tab[227]", "tab[1]", 0 ],
			"obj-6::obj-17::obj-34::obj-33" : [ "tab[275]", "tab[1]", 0 ],
			"obj-6::obj-17::obj-36::obj-33" : [ "tab[230]", "tab[1]", 0 ],
			"obj-6::obj-17::obj-40::obj-33" : [ "tab[231]", "tab[1]", 0 ],
			"obj-6::obj-17::obj-41::obj-33" : [ "tab[205]", "tab[1]", 0 ],
			"obj-6::obj-17::obj-42::obj-33" : [ "tab[206]", "tab[1]", 0 ],
			"obj-6::obj-17::obj-43::obj-33" : [ "tab[189]", "tab[1]", 0 ],
			"obj-6::obj-17::obj-44::obj-33" : [ "tab[276]", "tab[1]", 0 ],
			"obj-6::obj-17::obj-45::obj-33" : [ "tab[277]", "tab[1]", 0 ],
			"obj-6::obj-17::obj-46::obj-33" : [ "tab[278]", "tab[1]", 0 ],
			"obj-6::obj-17::obj-47::obj-33" : [ "tab[279]", "tab[1]", 0 ],
			"obj-6::obj-17::obj-48::obj-33" : [ "tab[280]", "tab[1]", 0 ],
			"obj-6::obj-17::obj-49::obj-33" : [ "tab[281]", "tab[1]", 0 ],
			"obj-6::obj-17::obj-50::obj-33" : [ "tab[282]", "tab[1]", 0 ],
			"obj-6::obj-17::obj-74::obj-33" : [ "tab[273]", "tab[1]", 0 ],
			"obj-6::obj-1::obj-107::obj-27::obj-18" : [ "toggle[109]", "toggle", 0 ],
			"obj-6::obj-1::obj-107::obj-8" : [ "tab[4]", "tab[1]", 0 ],
			"obj-6::obj-1::obj-123::obj-27::obj-18" : [ "toggle[107]", "toggle", 0 ],
			"obj-6::obj-1::obj-123::obj-8" : [ "tab[2]", "tab[1]", 0 ],
			"obj-6::obj-1::obj-34::obj-27::obj-18" : [ "toggle[110]", "toggle", 0 ],
			"obj-6::obj-1::obj-34::obj-8" : [ "tab[5]", "tab[1]", 0 ],
			"obj-6::obj-1::obj-37::obj-27::obj-18" : [ "toggle[115]", "toggle", 0 ],
			"obj-6::obj-1::obj-37::obj-8" : [ "tab[104]", "tab[1]", 0 ],
			"obj-6::obj-1::obj-38::obj-27::obj-18" : [ "toggle[114]", "toggle", 0 ],
			"obj-6::obj-1::obj-38::obj-8" : [ "tab[103]", "tab[1]", 0 ],
			"obj-6::obj-1::obj-39::obj-27::obj-18" : [ "toggle[111]", "toggle", 0 ],
			"obj-6::obj-1::obj-39::obj-8" : [ "tab[6]", "tab[1]", 0 ],
			"obj-6::obj-1::obj-40::obj-27::obj-18" : [ "toggle[112]", "toggle", 0 ],
			"obj-6::obj-1::obj-40::obj-8" : [ "tab[7]", "tab[1]", 0 ],
			"obj-6::obj-1::obj-41::obj-27::obj-18" : [ "toggle[113]", "toggle", 0 ],
			"obj-6::obj-1::obj-41::obj-8" : [ "tab[8]", "tab[1]", 0 ],
			"obj-6::obj-1::obj-44::obj-27::obj-18" : [ "toggle[116]", "toggle", 0 ],
			"obj-6::obj-1::obj-44::obj-8" : [ "tab[73]", "tab[1]", 0 ],
			"obj-6::obj-1::obj-45::obj-27::obj-18" : [ "toggle[117]", "toggle", 0 ],
			"obj-6::obj-1::obj-45::obj-8" : [ "tab[74]", "tab[1]", 0 ],
			"obj-6::obj-1::obj-46::obj-27::obj-18" : [ "toggle[118]", "toggle", 0 ],
			"obj-6::obj-1::obj-46::obj-8" : [ "tab[207]", "tab[1]", 0 ],
			"obj-6::obj-1::obj-47::obj-27::obj-18" : [ "toggle[119]", "toggle", 0 ],
			"obj-6::obj-1::obj-47::obj-8" : [ "tab[232]", "tab[1]", 0 ],
			"obj-6::obj-1::obj-48::obj-27::obj-18" : [ "toggle[120]", "toggle", 0 ],
			"obj-6::obj-1::obj-48::obj-8" : [ "tab[233]", "tab[1]", 0 ],
			"obj-6::obj-1::obj-49::obj-27::obj-18" : [ "toggle[121]", "toggle", 0 ],
			"obj-6::obj-1::obj-49::obj-8" : [ "tab[225]", "tab[1]", 0 ],
			"obj-6::obj-1::obj-50::obj-27::obj-18" : [ "toggle[122]", "toggle", 0 ],
			"obj-6::obj-1::obj-50::obj-8" : [ "tab[234]", "tab[1]", 0 ],
			"obj-6::obj-1::obj-74::obj-27::obj-18" : [ "toggle[108]", "toggle", 0 ],
			"obj-6::obj-1::obj-74::obj-8" : [ "tab[3]", "tab[1]", 0 ],
			"obj-6::obj-24::obj-107::obj-33" : [ "tab[264]", "tab[1]", 0 ],
			"obj-6::obj-24::obj-123::obj-33" : [ "tab[262]", "tab[1]", 0 ],
			"obj-6::obj-24::obj-34::obj-33" : [ "tab[265]", "tab[1]", 0 ],
			"obj-6::obj-24::obj-36::obj-33" : [ "tab[266]", "tab[1]", 0 ],
			"obj-6::obj-24::obj-40::obj-33" : [ "tab[267]", "tab[1]", 0 ],
			"obj-6::obj-24::obj-41::obj-33" : [ "tab[268]", "tab[1]", 0 ],
			"obj-6::obj-24::obj-42::obj-33" : [ "tab[291]", "tab[1]", 0 ],
			"obj-6::obj-24::obj-43::obj-33" : [ "tab[292]", "tab[1]", 0 ],
			"obj-6::obj-24::obj-44::obj-33" : [ "tab[293]", "tab[1]", 0 ],
			"obj-6::obj-24::obj-45::obj-33" : [ "tab[294]", "tab[1]", 0 ],
			"obj-6::obj-24::obj-46::obj-33" : [ "tab[295]", "tab[1]", 0 ],
			"obj-6::obj-24::obj-47::obj-33" : [ "tab[296]", "tab[1]", 0 ],
			"obj-6::obj-24::obj-48::obj-33" : [ "tab[297]", "tab[1]", 0 ],
			"obj-6::obj-24::obj-49::obj-33" : [ "tab[298]", "tab[1]", 0 ],
			"obj-6::obj-24::obj-50::obj-33" : [ "tab[299]", "tab[1]", 0 ],
			"obj-6::obj-24::obj-74::obj-33" : [ "tab[263]", "tab[1]", 0 ],
			"obj-6::obj-4::obj-107::obj-8" : [ "tab[183]", "tab[1]", 0 ],
			"obj-6::obj-4::obj-123::obj-8" : [ "tab[181]", "tab[1]", 0 ],
			"obj-6::obj-4::obj-34::obj-8" : [ "tab[184]", "tab[1]", 0 ],
			"obj-6::obj-4::obj-37::obj-8" : [ "tab[11]", "tab[1]", 0 ],
			"obj-6::obj-4::obj-38::obj-8" : [ "tab[10]", "tab[1]", 0 ],
			"obj-6::obj-4::obj-39::obj-8" : [ "tab[185]", "tab[1]", 0 ],
			"obj-6::obj-4::obj-40::obj-8" : [ "tab[186]", "tab[1]", 0 ],
			"obj-6::obj-4::obj-41::obj-8" : [ "tab[9]", "tab[1]", 0 ],
			"obj-6::obj-4::obj-44::obj-8" : [ "tab[12]", "tab[1]", 0 ],
			"obj-6::obj-4::obj-45::obj-8" : [ "tab[13]", "tab[1]", 0 ],
			"obj-6::obj-4::obj-46::obj-8" : [ "tab[14]", "tab[1]", 0 ],
			"obj-6::obj-4::obj-47::obj-8" : [ "tab[15]", "tab[1]", 0 ],
			"obj-6::obj-4::obj-48::obj-8" : [ "tab[16]", "tab[1]", 0 ],
			"obj-6::obj-4::obj-49::obj-8" : [ "tab[17]", "tab[1]", 0 ],
			"obj-6::obj-4::obj-50::obj-8" : [ "tab[18]", "tab[1]", 0 ],
			"obj-6::obj-4::obj-74::obj-8" : [ "tab[182]", "tab[1]", 0 ],
			"obj-6::obj-4::obj-95" : [ "number[1]", "number", 0 ],
			"obj-6::obj-67::obj-107::obj-27::obj-18" : [ "toggle[60]", "toggle", 0 ],
			"obj-6::obj-67::obj-107::obj-48" : [ "SendTo-TXT[18]", "SendTo-TXT", 0 ],
			"obj-6::obj-67::obj-107::obj-8" : [ "tab[257]", "tab[1]", 0 ],
			"obj-6::obj-67::obj-123::obj-27::obj-18" : [ "toggle[58]", "toggle", 0 ],
			"obj-6::obj-67::obj-123::obj-48" : [ "SendTo-TXT[16]", "SendTo-TXT", 0 ],
			"obj-6::obj-67::obj-123::obj-8" : [ "tab[213]", "tab[1]", 0 ],
			"obj-6::obj-67::obj-34::obj-27::obj-18" : [ "toggle[61]", "toggle", 0 ],
			"obj-6::obj-67::obj-34::obj-48" : [ "SendTo-TXT[19]", "SendTo-TXT", 0 ],
			"obj-6::obj-67::obj-34::obj-8" : [ "tab[258]", "tab[1]", 0 ],
			"obj-6::obj-67::obj-36::obj-27::obj-18" : [ "toggle[65]", "toggle", 0 ],
			"obj-6::obj-67::obj-36::obj-48" : [ "SendTo-TXT[20]", "SendTo-TXT", 0 ],
			"obj-6::obj-67::obj-36::obj-8" : [ "tab[202]", "tab[1]", 0 ],
			"obj-6::obj-67::obj-40::obj-27::obj-18" : [ "toggle[66]", "toggle", 0 ],
			"obj-6::obj-67::obj-40::obj-48" : [ "SendTo-TXT[21]", "SendTo-TXT", 0 ],
			"obj-6::obj-67::obj-40::obj-8" : [ "tab[269]", "tab[1]", 0 ],
			"obj-6::obj-67::obj-41::obj-27::obj-18" : [ "toggle[67]", "toggle", 0 ],
			"obj-6::obj-67::obj-41::obj-48" : [ "SendTo-TXT[22]", "SendTo-TXT", 0 ],
			"obj-6::obj-67::obj-41::obj-8" : [ "tab[259]", "tab[1]", 0 ],
			"obj-6::obj-67::obj-42::obj-27::obj-18" : [ "toggle[62]", "toggle", 0 ],
			"obj-6::obj-67::obj-42::obj-48" : [ "SendTo-TXT[23]", "SendTo-TXT", 0 ],
			"obj-6::obj-67::obj-42::obj-8" : [ "tab[270]", "tab[1]", 0 ],
			"obj-6::obj-67::obj-43::obj-27::obj-18" : [ "toggle[63]", "toggle", 0 ],
			"obj-6::obj-67::obj-43::obj-48" : [ "SendTo-TXT[24]", "SendTo-TXT", 0 ],
			"obj-6::obj-67::obj-43::obj-8" : [ "tab[271]", "tab[1]", 0 ],
			"obj-6::obj-67::obj-44::obj-27::obj-18" : [ "toggle[68]", "toggle", 0 ],
			"obj-6::obj-67::obj-44::obj-48" : [ "SendTo-TXT[25]", "SendTo-TXT", 0 ],
			"obj-6::obj-67::obj-44::obj-8" : [ "tab[179]", "tab[1]", 0 ],
			"obj-6::obj-67::obj-45::obj-27::obj-18" : [ "toggle[69]", "toggle", 0 ],
			"obj-6::obj-67::obj-45::obj-48" : [ "SendTo-TXT[26]", "SendTo-TXT", 0 ],
			"obj-6::obj-67::obj-45::obj-8" : [ "tab[215]", "tab[1]", 0 ],
			"obj-6::obj-67::obj-46::obj-27::obj-18" : [ "toggle[70]", "toggle", 0 ],
			"obj-6::obj-67::obj-46::obj-48" : [ "SendTo-TXT[27]", "SendTo-TXT", 0 ],
			"obj-6::obj-67::obj-46::obj-8" : [ "tab[216]", "tab[1]", 0 ],
			"obj-6::obj-67::obj-47::obj-27::obj-18" : [ "toggle[71]", "toggle", 0 ],
			"obj-6::obj-67::obj-47::obj-48" : [ "SendTo-TXT[28]", "SendTo-TXT", 0 ],
			"obj-6::obj-67::obj-47::obj-8" : [ "tab[203]", "tab[1]", 0 ],
			"obj-6::obj-67::obj-48::obj-27::obj-18" : [ "toggle[72]", "toggle", 0 ],
			"obj-6::obj-67::obj-48::obj-48" : [ "SendTo-TXT[29]", "SendTo-TXT", 0 ],
			"obj-6::obj-67::obj-48::obj-8" : [ "tab[217]", "tab[1]", 0 ],
			"obj-6::obj-67::obj-49::obj-27::obj-18" : [ "toggle[73]", "toggle", 0 ],
			"obj-6::obj-67::obj-49::obj-48" : [ "SendTo-TXT[30]", "SendTo-TXT", 0 ],
			"obj-6::obj-67::obj-49::obj-8" : [ "tab[180]", "tab[1]", 0 ],
			"obj-6::obj-67::obj-50::obj-27::obj-18" : [ "toggle[74]", "toggle", 0 ],
			"obj-6::obj-67::obj-50::obj-48" : [ "SendTo-TXT[31]", "SendTo-TXT", 0 ],
			"obj-6::obj-67::obj-50::obj-8" : [ "tab[218]", "tab[1]", 0 ],
			"obj-6::obj-67::obj-74::obj-27::obj-18" : [ "toggle[59]", "toggle", 0 ],
			"obj-6::obj-67::obj-74::obj-48" : [ "SendTo-TXT[17]", "SendTo-TXT", 0 ],
			"obj-6::obj-67::obj-74::obj-8" : [ "tab[214]", "tab[1]", 0 ],
			"parameterbanks" : 			{
				"0" : 				{
					"index" : 0,
					"name" : "",
					"parameters" : [ "-", "-", "-", "-", "-", "-", "-", "-" ]
				}

			}
,
			"inherited_shortname" : 1
		}
,
		"dependency_cache" : [ 			{
				"name" : "bbdmi.2max.maxpat",
				"bootpath" : "~/Documents/GitHub/bbdmi/max/output/2max",
				"patcherrelativepath" : "../../output/2max",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bbdmi.2midi.maxpat",
				"bootpath" : "~/Documents/GitHub/bbdmi/max/output/2midi",
				"patcherrelativepath" : "../../output/2midi",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bbdmi.2osc.maxpat",
				"bootpath" : "~/Documents/GitHub/bbdmi/max/output/2osc",
				"patcherrelativepath" : "../../output/2osc",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bbdmi.calibrate.maxpat",
				"bootpath" : "~/Documents/GitHub/bbdmi/max/control_processing/calibrate",
				"patcherrelativepath" : "../../control_processing/calibrate",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bbdmi.eavi.maxpat",
				"bootpath" : "~/Documents/GitHub/bbdmi/max/input/eavi",
				"patcherrelativepath" : "../../input/eavi",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bbdmi.list2~.maxpat",
				"bootpath" : "~/Documents/GitHub/bbdmi/max/utilities/list2~",
				"patcherrelativepath" : "../../utilities/list2~",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bbdmi.recorder~.maxpat",
				"bootpath" : "~/Documents/GitHub/bbdmi/max/utilities/recorder~",
				"patcherrelativepath" : "../../utilities/recorder~",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bbdmi.rms~.maxpat",
				"bootpath" : "~/Documents/GitHub/bbdmi/max/feature_extraction/rms~",
				"patcherrelativepath" : "../../feature_extraction/rms~",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bbdmi.scale.maxpat",
				"bootpath" : "~/Documents/GitHub/bbdmi/max/control_processing/scale",
				"patcherrelativepath" : "../../control_processing/scale",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bbdmi.~2list.maxpat",
				"bootpath" : "~/Documents/GitHub/bbdmi/max/utilities/~2list",
				"patcherrelativepath" : "../../utilities/~2list",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bbdmi_eeg_simulator_4~.mxo",
				"type" : "iLaX"
			}
, 			{
				"name" : "bbdmi_eeg_simulator~.maxpat",
				"bootpath" : "~/Documents/GitHub/bbdmi/max/input/eeg_simulator~",
				"patcherrelativepath" : "../../input/eeg_simulator~",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bbdmi_emg_pulse_4~.mxo",
				"type" : "iLaX"
			}
, 			{
				"name" : "bbdmi_emg_simulator~.maxpat",
				"bootpath" : "~/Documents/GitHub/bbdmi/max/input/emg_simulator~",
				"patcherrelativepath" : "../../input/emg_simulator~",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "landing-1a.json",
				"bootpath" : "~/Documents/GitHub/bbdmi/max/instruments/landing_patch/presets",
				"patcherrelativepath" : "./presets",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "landing-2a.json",
				"bootpath" : "~/Documents/GitHub/bbdmi/max/instruments/landing_patch/presets",
				"patcherrelativepath" : "./presets",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "name.js",
				"bootpath" : "~/Documents/GitHub/bbdmi/max/source/js",
				"patcherrelativepath" : "../../source/js",
				"type" : "TEXT",
				"implicit" : 1
			}
, 			{
				"name" : "p.2max.maxpat",
				"bootpath" : "~/Documents/GitHub/bbdmi/max/output/2max",
				"patcherrelativepath" : "../../output/2max",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "p.2midi.maxpat",
				"bootpath" : "~/Documents/GitHub/bbdmi/max/output/2midi",
				"patcherrelativepath" : "../../output/2midi",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "p.2osc.maxpat",
				"bootpath" : "~/Documents/GitHub/bbdmi/max/output/2osc",
				"patcherrelativepath" : "../../output/2osc",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "p.calibrate.maxpat",
				"bootpath" : "~/Documents/GitHub/bbdmi/max/control_processing/calibrate",
				"patcherrelativepath" : "../../control_processing/calibrate",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "p.exposer.maxpat",
				"bootpath" : "~/Documents/GitHub/bbdmi/max/source/patchers",
				"patcherrelativepath" : "../../source/patchers",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "p.scale.maxpat",
				"bootpath" : "~/Documents/GitHub/bbdmi/max/control_processing/scale",
				"patcherrelativepath" : "../../control_processing/scale",
				"type" : "JSON",
				"implicit" : 1
			}
 ],
		"autosave" : 0
	}

}
