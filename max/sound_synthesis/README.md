# Sound Synthesis

The [Sound synthesis](https://gitlab.huma-num.fr/bbdmi/bbdmi/-/tree/main/max/sound_synthesis) module contains all abstractions developed by the BBDMI project that deal with the production and modulations of sound. These are typically combined into a complex digital synthesizer that responds to data control data derived from the ExG signals.