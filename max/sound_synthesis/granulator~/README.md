# granulator~

```bbdmi.granulator~``` processes a sound input (or buffer) through an 8-phase shifted granular synthesiser.

## Use

Record live audio input or load a buffer (/folder), and start granulating the sound. It takes one (```list```) of control messages in its second inlet (must be scaled to accommodate each sound parameter), and a series of (```message```) names on its third inlet (in order as below). In addition, massages can be sent remotely through a <send> object named with the same module ID.

### Note

* Playback and randomness position expect as input range ```0,1``` as they are internally adjusted to fit the buffer’s length.
* When recording, remember to first set the length of the buffer (e.g. 2000 ms).

### ATTRIBUTES

(```message```) *buffer (name)*

### MESSAGES

>[inlet] (```message```, type) *description*

[1] (mc.~ signal) *to record and granulate*

[2] (list) *nine scaled values: in order, ```playpos```, ```randpos```, ```stretch```, ```graindur```, ```transp```, ```aleaoct```, ```quant```, ```randpan```, ```gain```*

[3] (```playpos``` float) *playback position [range: 0,1]*

[3] (```randpos``` float) *position randomness [range: 0,1]*

[3] (```stretch``` float) *playback speed*

[3] (```graindur``` float) *grain duration*

[3] (```transp``` int) *pitch transposition (cents, i.e. 100 = 1 semitone)*

[3] (```aleaoct``` float) pitch random octave [range: 0,3]*

[3] (```quant``` float) *pitch quantisation [range: 0,1]*

[3] (```randpan``` float) *random panning [range: 0,0.5]*

[3] (```gain``` float) *amplitude  [range: 0,1]*

[3] (```playstop``` toggle) *play / stop*

[3] (```mode``` toggle) *freeze / playback*

[3] (```read```) *load a buffer from disk*

[3] (```write```) *save a buffer into disk*

[3] (```file``` name) *load a named buffer*

[3] (```folder``` name) *load a set of named buffers*

[3] (```clear```) *empty buffer and reset granulator~*

### OUTPUT

(mc.~ signal) *granulated stereo signal*