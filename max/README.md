# BBDMI Max repository

## About

This part of the repository contains Max patches, bpatchers and abstractions developed within the [BBDMI](https://bbdmi.nakala.fr/) project for the purpose of creating a Body Brain Digital Musical Instrument. The repository is organized in a directory structure that roughly followes a Brain Computer Music Interface (BCMI) data flow. Directories can be seen as modules sharing a common goal and operation within the BCMI data flow.

## Module organization

- The [Input](https://gitlab.huma-num.fr/bbdmi/bbdmi/-/tree/main/max/input) module deals with diverse ExG devices and acquistion software, making sure that the data becomes available as an (audio) signal for further processing in Max (e.g. in the [Feature extraction](https://gitlab.huma-num.fr/bbdmi/bbdmi/-/tree/main/max/feature_extraction) module). The module also contains some basic signal processing for conditioning the data, such as a notch filter.

- The [Feature extraction](https://gitlab.huma-num.fr/bbdmi/bbdmi/-/tree/main/max/feature_extraction) module contains abstractions that extracts meaningful data from incoming signals, such as bandpassed power, or a root-mean-square. These abstractions generally expect signal (audio) input, and generally output data (list messages). Several machine learning algoriths are included as well, such as regression and classification.

- The [Control processing](https://gitlab.huma-num.fr/bbdmi/bbdmi/-/tree/main/max/control_processing) module allows creative, musical manipulation of data (list messages). These abstractions are typically used to take data (messages) from the [Feature extraction](https://gitlab.huma-num.fr/bbdmi/bbdmi/-/tree/main/max/feature_extraction) module, and map those in intersting ways to the [Sound synthesis](https://gitlab.huma-num.fr/bbdmi/bbdmi/-/tree/main/max/sound_synthesis) module.

- The [Sound synthesis](https://gitlab.huma-num.fr/bbdmi/bbdmi/-/tree/main/max/sound_synthesis) module deals with the production and modulations of sound. These are typically combined into a complex digital synthesizer that responds to data control data derived from the ExG signals.

- The [Output](https://gitlab.huma-num.fr/bbdmi/bbdmi/-/tree/main/max/output) module provides communication with either an audio sound device, protocols such as MIDI and OSC, or other Max patches using internal message routing (send/receive). It also contains utilities for routing and mixing signals to the output devices.

- The [Utilities](https://gitlab.huma-num.fr/bbdmi/bbdmi/-/tree/main/max/utilities) module contains functionality in the form of abstractions, that are more generally employed than in a specific module. These are typically small abstractions that make patching within other abstractions, or in patches, easier.

- The [Instruments](https://gitlab.huma-num.fr/bbdmi/bbdmi/-/tree/main/max/instruments) module contains patches that combine BBDMI and other Max abstractions into instruments. Naturally, these are often more individualized and user-specific. This module also acts as documentation of past performances and instrument ideas, and can hopefully function as examples for building your own.

## README.md organization

Each abstraction or patch is located in the directories described above that include a README.md explaining their function, user cases and examples. Users can delve into variable names, module's description, while consulting a list of information that includes what type of data an inlet/outlet is expecting, which range, messages and arguments are allowed, as follows.

## Architecture

### Modularisation of Max objects

We propose a modular approach of patching Max objects, allowing the user to flexibly interchange modules for creating different instruments with different ExG devices. We provide you with a detailed article explaining the main strategies and design choices of our system architecture: [An Interactive Modular System for Electrophysiological DMIs](http://camps.aptaracorp.com/ACM_PMS/PMS/ACM/AM23/31/feabd036-40e5-11ee-b37c-16bb50361d1f/OUT/am23-31.html). 

### Multiple channels due to different input devices

Since each device permits a different set of input channels, each module is designed to automatically adapt to the number of input channels. 

### Control signals vs. Audio signals

Modules can stream either control signals (numeric data) or audio signals. The latter can be distinguished in their name by the appended ```/~``` (tilde) symbol. Each module (/patch) accepts either a number list of data (control signals), or a multichannel list of audio signals (using the ```mc.``` Max objects). 

### "One-cable" patching

Both types of modules share the logic of "one-cable" patching (i.e. multiple channels are streamed though one and one only cable, avoiding the process of connecting multiple outputs cables to multiple input cables, and vice versa). Modules automatically adapt to the number of input channels. Please note that Max’s DSP engine has to be refreshed (turned off and on) to update the number of channels.

### One vs. Multiple channels parameters modulation

Some modules process single channels separately (e.g. bbdmi.calibrate, bbdmi.scale and bbdmi.2osc) so that parameters can be set for each channel individually. Others apply the same value(s) for all the channels (e.g. bbdmi.rms/~, bbdmi.speedlim and bbdmi./~2list). This is why you may find a ``` "p." abstraction ``` in some modules (e.g in p.calibrate), repeating the same function over all channels. 

### "Modes" of interaction: virtual patching (send receive), versus cabling

Two different modes (methods) of patching are implemented:

1. *Abstractions* (with objects exposed): Each module is instantiated in the main patcher window as an abstraction, where the main parameters can be modified using messages (e.g. ```onoff 1```) (see image).

![abstractions](max/source/images/abstractions.png "Abstractions (with objects exposed)”) *Abstractions (with objects exposed)*

2. *Bpatchers* (with GUI exposed): Each module is instantiated as bpatchers, which exposes parameters in a graphical user interface.

![bpacthers](max/source/images/bpatchers.png "Bpatchers (with GUI exposed)”) *Bpatchers (with GUI exposed)*

Some modules can send or receive messages, using the module’s ID (e.g. bbdmi.granulator~). The ID can be set either with an argument directly or using the Inspector when using modules as abstractions. When using modules as bpatchers it can be set in the GUI directly.

You can use the <bbdmi.send> with the <bbdmi.receive> object to implement similar solutions in your patch. 

### Saving and recalling presets

Each parameter in a module GUI is “bound" to a <pattr> object, using a consistent namespace  in the GUI, abstraction and help files. This means that you can use the <preset> object in conjunction with <pattrstorage> to save and recall a snapshot of all parameters in a patcher window. For more information see the <pattrstorage> help file.

## Installation

If you have [Git](http://git-scm.com/) or [GitHub Desktop](https://desktop.github.com/) installed, you can clone this repo via Terminal using the following commands:

```
cd ~/Documents/GitHub
git clone https://gitlab.huma-num.fr/bbdmi/bbdmi

```
You can also [download the latest release here](https://gitlab.huma-num.fr/bbdmi/bbdmi). After decompressing the zip archive, the resulting folder must be placed in the Max search path such as:

```
~/Documents/Max 8/Packages
~/Documents/GitHub

```

## Dependencies

Some abstractions and patches use external libraries that need to be installed, and placed in the Max Packages folder:

- [rapid](https://github.com/mzed/rapid)
- [myo-for-max](https://github.com/JulesFrancoise/myo-for-max)
- [CNMAT-Externs](https://github.com/CNMAT/CNMAT-Externs)