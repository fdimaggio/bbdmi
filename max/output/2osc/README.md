# 2osc

```bbdmi.2osc``` sends out control signals as Open Sound Control (OSC) messages.

## Use

It takes one input (```list```) and sends each individual value using the OSC format (/output value). Host, port and output messages can be set/adjusted using GUI elements. The message can contain as many words as needed. The first word is used to address the ```udpreceive``` object, the rest of the words can be routed using the ```OSC-route``` object. Here are some examples of format:

- ```/tempo``` value
- ```/sampler/speed``` value
- ```/synth1/gran1/dur``` value
- ```/synth2/filter/eq1/freq``` value

### ATTRIBUTES

(int) *number of channels*

### ARGUMENTS

(```@port``` int) *output port*

(```@host``` int) *host*

### MESSAGES

>[inlet] (```message```, type) *description*

[1] (list) *to forward*

[2] (```port``` int) *output port*

[2] (```host``` int) *host*