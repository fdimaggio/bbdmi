# Output

The [Output](https://gitlab.huma-num.fr/bbdmi/bbdmi/-/tree/main/max/output) module contains abstractions that provide communication with either an audio sound device, protocols such as MIDI and OSC, or other Max patches using internal message routing (send/receive). It also contains utilities for routing and mixing signals to the output devices.