# 2max

```bbdmi.2max``` forwards a list of control signals internally to Max for further processing.

## Use

It takes one input (```list```) and sends each individual value internally using the send/receive method. The message can contain as many words as needed. The first word is used to address the ```receive``` object, the rest of the words can be routed using the ```route``` object. Examples of formats are:

- ```tempo``` value
- ```sampler speed``` value
- ```synth1 gran1 dur``` value
- ```synth2 filter eq1 freq``` value

### ATTRIBUTES

(int) *number of channels*

### MESSAGES

(list) *to forward*